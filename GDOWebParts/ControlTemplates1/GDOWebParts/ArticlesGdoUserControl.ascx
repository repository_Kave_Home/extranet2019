﻿<%@ Assembly Name="GDOWebParts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=2a2904cb1bfec2c2" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticlesGdoUserControl.ascx.cs"
    Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.ArticlesGdoUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<script type="text/javascript" src="/Style Library/Julia/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript">
    function OpenFile(url) {
        $.blockUI({ message: 'En Proceso' });
        window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
        $.unblockUI();
    }

    $(document).ready(function () {
        disableQtyEdit();
    });

    function disableQtyEdit() {
        $(".Qty_input .riTextBox.riEnabled").attr("disabled", "disabled");
    }

    function FocusArticle(codArticle) {
        $(function () {
            $(window).scrollTo($("[id='" + codArticle + "']"), 0);
        });
    }
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    function HideSearchBoxTopMenu() {
        $("#searchBox").hide();
        $(".megamenuCategoryStyle").hide();
    }
    function filterClick() {
        var elem = document.getElementById("<%= ibClearFilter.ClientID %>").click();
    }
    function onLoadTreeView(sender, args) {
        var search = $("#" + '<%=SearchProduct.ClientID %>');
        var provider = $find('<%=Proveidros_Combo.ClientID %>');
        var grupProv = $("#" + '<%=GrupProveidorFilter.ClientID %>').val();
        var program = $find('<%=Programa.ClientID %>');
        var treeView = $find('<%=CategoriaTreeView.ClientID %>');
        var estance = $("#estanceGdo").val();
        var style = $("#styleGdo").val();
        var productEsEx = $find('<%=ProductoExEs.ClientID %>');
        if (search != null && provider != null && grupProv != null && program != null && treeView != null && productEsEx != null) {
            JuliaClients.Service.PrepareTreeView(treeView, search.val(), null, null, program.get_value(), provider.get_value(), grupProv, estance, style, null, null, null, null, null, productEsEx.get_value());
        }
    };
</script>
<style type="text/css">
    .printView
    {
        margin-left: 15;
    }
    .loadingPanel
    {
        position: absolute;
        width: 100%;
        height: 100%;
        float: left;
    }
    .ui-effects-transfer
    {
        border: 2px solid black;
    }
    
    .sliderCarousel
    {
        /*forzar separacion entre catalogos y tabla de productos*/
        min-height: 100px !important;
    }
    .ColorItemProduct
    {
        width: 60px !important;
        height: 60px !important;
        border-color: #e3e3e3;
    }
    .ColorItemMore
    {
        width: 10px !important;
        height: 60px !important;
        margin-left: 2px;
        margin-bottom: 4px;
        padding: 2px;
        background-image: url('/Style Library/julia/img/agmore.png');
        border: 0;
        top: 0;
        left: 0;
        background-position: left center;
        display: inline-block;
        background-repeat: no-repeat;
    }
    .riTextBox.riEnabled.LblCantidad
    {
        padding-left: 2px;
    }
</style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" OnNavigate="ScriptManager1_Navigate">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerPedidoUserControl" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadListView1">
        </telerik:AjaxSetting>
    </AjaxSettings>
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PrintCurentView">
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">
        function onRequestStart(ajaxManager, eventArgs) {
            if (eventArgs.EventTarget.indexOf("PrintCurentView") != -1) {
                eventArgs.EnableAjax = false;
            }
        }
    </script>
</telerik:RadCodeBlock>
<asp:Label runat="server" CssClass="LblFilterStates" ID="EstadoFiltros"></asp:Label>
<asp:Label runat="server" CssClass="LblFilterStates" ID="EstadoDivisa"></asp:Label>
<div class="NewGroupProductsCatalogTitle">
    <div class="headerText" style="padding-bottom: 1rem">
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SeeResultsFor") %>:</div>
    <div id="Div4" class="NewFilterCatalog">
        <div class="NewgroupProductsCatalogSearch">
            <telerik:RadTextBox ID="SearchProduct" runat="server" AutoPostBack="false" Skin="Silk"
                Width="175px" CssClass="RadComboBox RadComboBox_Default" OnClientLoad="onLoadTreeView" />
            <asp:ImageButton ID="ibSearchProduct" OnClick="ibSearchProduct_Click" CssClass="NewimgProductsCatalogSearch"
                ImageUrl="/Style%20Library/Julia/img/icona_buscador.PNG" runat="server" onmouseover="this.src=this.src.replace('icona_buscador.','icona_buscador_on.');"
                onmouseout="this.src=this.src.replace('icona_buscador_on.','icona_buscador.');" />
        </div>
    </div>
    <div id="Div8" class="NewFilterCatalog" style="width: 100%;">
        <div>
            <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectCategory") %>:</div>
        <telerik:RadTreeView RenderMode="Lightweight" ID="CategoriaTreeView" runat="server"
            Style="white-space: normal" OnNodeClick="NodeClick" Skin="Silk" OnClientLoad="onLoadTreeView">
            <DataBindings>
                <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
            </DataBindings>
        </telerik:RadTreeView>
    </div>
    <div id="Div0" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Proveidros_Combo" runat="server" Width="200px" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Proveedor"
            OnSelectedIndexChanged="Proveidros_Combo_SelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            RenderMode="Lightweight" Skin="Silk" OnClientLoad="onLoadTreeView" />
        <asp:HiddenField runat="server" ID="GrupProveidorFilter" />
    </div>
    <div id="Div1" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Familia" Visible="false" runat="server" Width="200px" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona una familia"
            OnSelectedIndexChanged="FamiliaSelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            RenderMode="Lightweight" Skin="Silk" />
    </div>
    <div id="Div2" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Subfamilia" Visible="false" runat="server" Width="200px"
            DataTextField="value" AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona una Subfamilia"
            OnSelectedIndexChanged="SubFamiliaSelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            RenderMode="Lightweight" Skin="Silk" />
    </div>
    <div id="Div3" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Programa" runat="server" Width="200px" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Programa"
            OnSelectedIndexChanged="ProgramaSelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            RenderMode="Lightweight" Skin="Silk" OnClientLoad="onLoadTreeView" />
    </div>
     <div id="divProductoExEs" class="NewFilterCatalog">
        <telerik:RadComboBox ID="ProductoExEs" runat="server" Width="200px" Skin="Silk" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Tipo de Producto" OnSelectedIndexChanged="ProductoExEsSelectedIndexChanged" />
    </div>
    <asp:ImageButton ID="ibClearFilter" Style="display: none" runat="server" CssClass="ClearFilter"
        OnClick="ibClearFilter_Click" />
    <div id="deleteAll" class="deleteButton clearFilterImg" runat="server" onclick="filterClick()">
        <span><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteFilters")%></span>
    </div>
</div>
<div class="ProductGridContainer">
    <telerik:RadListView ID="RadListView1" CssClass="RadListView" runat="server" ItemPlaceholderID="PlaceHolder1"
        OnItemCommand="RadListView1_ItemCommand" AllowPaging="true" OnLoad="RadListView1_Load"
        OnPageIndexChanged="RadListView1_PageIndexChanged" AllowCustomPaging="true" Width="100%">
        <%-- OnDataBound="RadListView1_DataBound" --%>
        <LayoutTemplate>
            <div class="CatalogPagination">
                <div class="CatalogNumberPagination">
                    <telerik:RadDataPager ID="RadDataPager2" CssClass="RadDataPager" PageSize="<%# PageSize %>"
                        runat="server" OnFieldCreated="RadDataPager_FieldCreated" RenderMode="Lightweight"
                        Skin="Metro">
                        <ClientEvents OnPageIndexChanging="PageIndexChanging" />
                        <Fields>
                            <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                            <telerik:RadDataPagerButtonField FieldType="NextLast" />
                            <telerik:RadDataPagerGoToPageField />
                            <telerik:RadDataPagerPageSizeField />
                        </Fields>
                    </telerik:RadDataPager>
                    <asp:ImageButton ID="PrintCurentView" OnClick="PrintView_Click" ImageUrl="/Style%20Library/Julia/img/imprimir.png"
                        CssClass="ClearFilter printView" runat="server" onmouseover="this.src=this.src.replace('imprimir.','imprimir_on.');"
                        onmouseout="this.src=this.src.replace('imprimir_on.','imprimir.');" />
                </div>
                <div class="clearFloat">
                </div>
            </div>
            <div class="contenedorProductos">
                <asp:Panel ID="productPanel" runat="server">
                    <div>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </div>
                </asp:Panel>
            </div>
            <div class="clearFloat">
            </div>
            <div class="CatalogPagination">
                <div class="CatalogNumberPagination">
                    <telerik:RadDataPager ID="RadDataPager1" runat="server" OnFieldCreated="RadDataPager_FieldCreated"
                        BackColor="White" RenderMode="Lightweight" Skin="Metro">
                        <ClientEvents OnPageIndexChanging="PageIndexChanging" />
                        <Fields>
                            <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                            <telerik:RadDataPagerButtonField FieldType="NextLast" />
                            <telerik:RadDataPagerGoToPageField />
                            <telerik:RadDataPagerPageSizeField />
                        </Fields>
                    </telerik:RadDataPager>
                </div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="NewitemProduct Container">
                <div id="imgProducto1" style="text-align: center;">
                    <a href='<%# Eval("NavigateUrl") %>'>
                        <asp:Panel ID="ImgDiscount" runat="server" Width="66px" Height="60px" CssClass="ImgDiscountPos"
                            Visible="true" BackImageUrl='<%# Eval("ImgDiscountUrl") %>'>
                            <asp:Label ID="Label5" runat="server" CssClass="Label_Discount" Text='<%# Eval("DiscountLabel") %>'></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="ImgNewProduct" runat="server" BackImageUrl="/gdo/Style Library/Julia/img/NewProduct.png"
                            Width="225px" Height="225px" CssClass="ImgNewProductPos" Visible='<%#Eval("NewProduct")%>'>
                        </asp:Panel>
                        <asp:Panel ID="ImgComingSoon" runat="server" BackImageUrl="/Style Library/Julia/img/ComingSoonProduct.png"
                            Width="225px" Height="225px" CssClass="ImgComingSoonPos" Visible='<%#Eval("ComingSoon")%>'>
                        </asp:Panel>
                        <%--   <asp:Image ID="ImgDiscount"  runat="server" CssClass="ImgDiscountPos" Visible="true" ImageUrl='<%# Eval("ImgDiscountUrl") %>' />--%>
                        <img alt="" src="<%# Eval("ImageUrl") %>" id="<%# Eval("Code") %>" class="NewimgProduct"
                            width="225px" height="225px" border="0" onerror="<%# Eval("ImgErrorUrl") %>" />
                    </a>
                </div>
                <div class="bloq_info_front" style="float: left;">
                    <div class="NewtxtProductRef">
                        <asp:Literal runat="server" Text='<%#Eval("Code")%>' ID="productCode"></asp:Literal>
                    </div>
                    <div class="NewtxtProductName">
                        <%#Eval("Description")%>
                    </div>
                    <div class="NewtxtProductPrice">
                        <asp:Panel ID="Panel_Price" runat="server" CssClass="Price_Panel">
                            <asp:Label ID="Label_Price" Text='<%#Eval("Price")%>' runat="server"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="Panel_PriceWithDiscount" runat="server" CssClass="PriceWithoutDiscount_Panel">
                            <asp:Label ID="Label_Discount" Text='<%#Eval("PriceWithDiscount")%>' runat="server"></asp:Label>
                        </asp:Panel>
                    </div>
                </div>
                <div class="Contenido">
                    <div id='<%#Eval("CodeWS")%>'>
                        <div class="MeasuresBox">
                            <span style="display: none;">
                                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMeasures")%></span>
                            <table cellspacing="0" cellpadding="0" class="tableMeasures">
                                <tr>
                                    <td>
                                        <img height="25px" src="/gdo/Style Library/Julia/img/Height.png" />
                                    </td>
                                    <td>
                                        <img height="25px" src="/gdo/Style Library/Julia/img/Width.png" />
                                    </td>
                                    <td>
                                        <img height="25px" src="/gdo/Style Library/Julia/img/Depth.png" />
                                    </td>
                                </tr>
                                <tr class="ValuestableMeasuresTr">
                                    <td>
                                        <%#Eval("Height")%>
                                    </td>
                                    <td>
                                        <%#Eval("Width")%>
                                    </td>
                                    <td>
                                        <%#Eval("Length")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="QtyAndBtn">
                            <div class="NewitemQty">
                                <div class="Qty_input">
                                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                        CssClass="LblCantidad" IncrementSettings-InterceptMouseWheel="true" MaxLength="3"
                                        runat="server" ID="itemQty" Width="50px" MinValue='<%#Convert.ToDouble(Eval("UnidadMinima"))%>'
                                        MaxValue="9999" NumberFormat-DecimalDigits="0" Value='<%#Convert.ToDouble(Eval("UnidadMinima"))%>'
                                        IncrementSettings-Step='<%# Convert.ToDouble(Eval("UnidadMedida")) %>' AllowRounding="true"
                                        KeepNotRoundedValue="true" />
                                </div>
                            </div>
                            <div class="Btn_AddToCar">
                                <asp:ImageButton ID="ImageButton1" CommandName="AddItemToCart" runat="server" CssClass="NewimgProductAdd"
                                    BorderStyle="None" BackColor="Transparent" ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/add_to_car.gif%>"
                                    onmouseover="this.src=this.src.replace('add_to_car.','add_to_car_on.');" onmouseout="this.src=this.src.replace('add_to_car_on.','add_to_car.');" />
                            </div>
                        </div>
                        <div id="Div1" class="PanelColor" visible='<%#Eval("ShowAgrupColor")%>' runat="server">
                            <asp:Literal runat="server" ID="ColorsAgrup" Text='<%#Eval("ColorsAgrup")%>'></asp:Literal>
                        </div>
                        <div id="Div2" class="PanelMeasure" visible='<%#Eval("ShowAgrupMeasures")%>' runat="server">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("MeasuresAgrupLink") %>'
                                Text='<%#Eval("MeasuresAgrupText") %>'>
                                
                            </asp:HyperLink>
                        </div>
                    </div>
                </div>
                <%--<div class="NewBtnAdd">
                    <asp:HyperLink ID="btAddProd" CssClass="NewimgProductAdd" NavigateUrl='<%#Eval("NavigateUrl") %>'
                        BorderStyle="None" BackColor="Transparent" runat="server" />
                </div>--%>
            </div>
        </ItemTemplate>
    </telerik:RadListView>
</div>
<style type="text/css">
    /* to top */
    #to-top
    {
        display: block;
        position: fixed;
        text-align: center;
        line-height: 12px !important;
        right: 17px;
        bottom: -30px;
        color: #fff;
        cursor: pointer;
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        -o-border-radius: 2px;
        z-index: 10000;
        height: 29px;
        width: 29px;
        background-color: rgba(0,0,0,0.4);
        background-repeat: no-repeat;
        background-position: center;
        transition: background-color 0.1s linear;
        -moz-transition: background-color 0.1s linear;
        -webkit-transition: background-color 0.1s linear;
        -o-transition: background-color 0.1s linear;
    }
    
    #to-top i
    {
        line-height: 29px !important;
        width: 29px !important;
        height: 29px !important;
        font-size: 14px !important;
        top: 0px !important;
        left: 0px !important;
        text-align: center !important;
        background-color: transparent !important;
    }
    
    
    #to-top:hover, #to-top.dark:hover
    {
        background-color: #27CFC3;
    }
    
    #to-top.dark
    {
        background-color: rgba(0,0,0,0.87);
    }
    
    [class^="icon-"], [class*=" icon-"]
    {
        background-color: #27CFC3;
        border-radius: 999px 999px 999px 999px;
        -moz-border-radius: 999px 999px 999px 999px;
        -webkit-border-radius: 999px 999px 999px 999px;
        -o-border-radius: 999px 999px 999px 999px;
        color: #fff;
        display: inline-block;
        font-size: 16px;
        height: 32px;
        line-height: 32px;
        max-width: 100%;
        position: relative;
        text-align: center;
        vertical-align: middle;
        width: 32px;
        top: -2px;
        word-spacing: 1px;
    }
    
    [class^="icon-"].icon-3x, [class*=" icon-"].icon-3x
    {
        background-color: #eeedec !important;
    }
    
    body [class^="icon-"].icon-3x.alt-style, body [class*=" icon-"].icon-3x.alt-style
    {
        background-color: #27CFC3 !important;
        color: #fff !important;
    }
    
    .icon-angle-up:before
    {
        content: "\f01b";
        font-family: "Fontawesome";
        font-style: normal;
        font-size: 25px !important;
    }
    
    [class^="icon-"]:before, [class*=" icon-"]:before
    {
        text-decoration: inherit;
        display: inline-block;
        speak: none;
    }
</style>
<%--<a id="to-top" style="bottom: 17px; display: none;" onclick="$(window).scrollTo(0, 500); return false;"
    href="javascript:totop();"><i class="icon-angle-up"></i></a>--%>
<a id="to-top" style="bottom: 17px; display: none;" onclick="$('html, body').animate({ scrollTop: 0 }, 500);"><i class="icon-angle-up"></i></a>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        //Check to see if the window is top if not then display button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#to-top').fadeIn();
            } else {
                $('#to-top').fadeOut();
            }
        });
    });
    //$("#to-top").click(function () { $(window).scrollTo(0, 500); });

    var sourceArticleId = getParameterByName('artId');
    if (sourceArticleId != 'undefined' && sourceArticleId != '') {
        FocusArticle(sourceArticleId);
    }
</script>
<telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
    AutoCloseDelay="3000" OffsetY="-100" EnableShadow="true" Position="MiddleLeft"
    ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"
    Skin="Silk" Text="Producto Añadido">
</telerik:RadNotification>
<input type="hidden" id="estanceGdo" value="<%= this.estance %>" />
<input type="hidden" id="styleGdo" value="<%= this.style %>" />
