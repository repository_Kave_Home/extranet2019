﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using JuliaGrupUtils.Utils;
using Telerik.Web.UI;
using JuliaGrupUtils.ErrorHandler;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Globalization;
using System.Web;


namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class ArticlesGdoUserControl : UserControl
    {
        private static int _defaultPageSize = 64;  //64;
        public int PageSize = _defaultPageSize;
        User currentUser;
        Client currentClient;
        Order CurrentOrder;
        Boolean AsociatedProveitor = false;
        List<Article> Products = new List<Article>();
        ArticleDataAccessObject articleDAO = null;
        private string CodiProveidor = string.Empty;
        private string CodiProveidorAsociats = string.Empty;

        private bool getCachedData = false;


        string fprov = String.Empty;
        string fgprov = String.Empty;
        string fCatalog = String.Empty;
        string fFamily = String.Empty;
        string fSubFamily = String.Empty;
        string fProgram = String.Empty;
        string fSearchTxt = String.Empty;
        string fCategory = String.Empty;
        string fProductoExEs = String.Empty;
        string fEstance = String.Empty;
        string fStyle = String.Empty;
        int fPageNo = 0;

        internal class TreeViewDataItem
        {
            private string text;
            private int id;
            private int parentId;
            private string valueCode;

            public string Text
            {
                get { return text; }
                set { text = value; }
            }
            public int ID
            {
                get { return id; }
                set { id = value; }
            }
            public int ParentID
            {
                get { return parentId; }
                set { parentId = value; }
            }
            public string ValueCode
            {
                get { return valueCode; }
                set { valueCode = value; }
            }
            public TreeViewDataItem(int id, int parentId, string text, string valueCode)
            {
                this.id = id;
                this.parentId = parentId;
                this.text = text;
                this.valueCode = valueCode;
            }
        }

        public string currentUsername
        {
            get
            {
                LoadCurrentUserandClientData();
                return currentUser.UserName;
            }
        }

        public string currentClientCode
        {
            get
            {
                LoadCurrentUserandClientData();
                return currentClient.Code;
            }
        }

        public string estance
        {
            get
            {
                return (Session["fEstanceGdo"] != null) ? Session["fEstanceGdo"].ToString() : String.Empty;
            }
        }

        public string style
        {
            get
            {
                return (Session["fStyleGdo"] != null) ? Session["fStyleGdo"].ToString() : String.Empty;
            }
        }

        protected override void CreateChildControls()
        {
            try
            {
                if (Session["ItemsPerPageGdo"] != null)
                {
                    this.PageSize = string.IsNullOrEmpty(Session["ItemsPerPageGdo"].ToString()) ? _defaultPageSize : int.Parse(Session["ItemsPerPageGdo"].ToString());
                }
                if (!this.Page.IsPostBack)
                {
                    string fprov = (!string.IsNullOrEmpty(Request.QueryString["proveidor"])) ? Request.QueryString["proveidor"] : ((Session["ProveedorFilter"] != null) ? Session["ProveedorFilter"].ToString() : String.Empty);
                    string gprov = (!string.IsNullOrEmpty(Request.QueryString["Grupproveidors"])) ? Request.QueryString["Grupproveidors"] : ((Session["GrupoProveedoresFilter"] != null) ? Session["GrupoProveedoresFilter"].ToString() : String.Empty);
                    this.ProductoExEs.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProExEs");
                    if (!string.IsNullOrEmpty(fprov))
                    {
                        this.Proveidros_Combo.SelectedValue = fprov;
                        this.Proveidros_Combo.Text = fprov;

                        var CogGrupProveedor = Products.Where(p => p.GDONoVendedor == fprov).Select(p => p.GDOTipoUnion).Distinct().First();
                        GrupProveidorFilter.Value = CogGrupProveedor;
                    }
                    else if (!string.IsNullOrEmpty(gprov))
                    {
                        //Solo lo compruebo si no me estan filtrado por un proveedor especifico
                        this.Proveidros_Combo.SelectedValue = "AllAssociatedProducts";
                        this.Proveidros_Combo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AllAssociatedProducts");
                        this.GrupProveidorFilter.Value = gprov;
                    }
                    this.CurrentOrder = (Order)Session["OrderGdo"];
                    if (this.CurrentOrder.ListOrderProducts.Count != 0)
                    {
                        this.GrupProveidorFilter.Value = this.CurrentOrder.ListOrderProducts.First().Product.GDOTipoUnion;
                    }
                    //this.RadListView1.DataSource = LoadData();
                    //this.RadListView1.DataBind();
                    ArticleDataAccessObject articleDAO = Session["ArticleGdoDAO"] as ArticleDataAccessObject;
                }
                else
                {
                    if (!Page.IsCallback)
                    {
                        //this.RadListView1.DataSource = LoadData();
                        //ReLoadVariableFilters();
                        //this.RadListView1.DataBind();
                        RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                        ajaxManager.ResponseScripts.Add("disableQtyEdit();");
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void LoadFilters(List<Article> articles, string selectedProv)
        {
            try
            {
                //Obtenemos todos los artículos para montar los filtros de familia, subfamilia y programa
                List<Article> Allarticles = Session["ArticlesGdo"] as List<Article>;
                if (Allarticles != null)
                {
                    var families = (from p in Allarticles
                                    orderby p.Family
                                    select p.Family).Distinct();
                    //var catalogtype = (from p in articles
                    //                   orderby p.CatalogType
                    //                   select p.CatalogType).Distinct();
                    //string FamilyFilterOldValue = this.Familia.SelectedValue;
                    string FamilyFilterOldValue = fFamily;
                    this.Familia.Items.Clear();
                    this.Familia.Items.Add(new RadComboBoxItem("", ""));
                    foreach (string family in families)
                    {
                        this.Familia.Items.Add(new RadComboBoxItem(family, family));
                    }
                    this.Familia.SelectedValue = FamilyFilterOldValue;
                    ReLoadVariableFilters();
                    //var subFamilies = (from p in Allarticles
                    //                   orderby p.SubFamily
                    //                   select p.SubFamily).Distinct();
                    //this.Subfamilia.Items.Clear();
                    ////this.Subfamilia.Items.Add(new RadComboBoxItem("", ""));
                    //foreach (string subfamilia in subFamilies)
                    //{
                    //    this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                    //
                    //load Programa

                    var Programas = (from p in Allarticles
                                     orderby p.Programa
                                     select p.Programa).Distinct();

                    //string ProgramaFilterOldValue = this.Programa.SelectedValue;
                    string ProgramaFilterOldValue = fProgram;
                    this.Programa.Items.Clear();
                    this.Programa.Items.Add(new RadComboBoxItem("", ""));
                    foreach (string programa in Programas)
                    {
                        this.Programa.Items.Add(new RadComboBoxItem(programa, programa));
                    }
                    this.Programa.SelectedValue = ProgramaFilterOldValue;

                    string ProductoExEsFilterOldValue = fProductoExEs;
                    this.ProductoExEs.Items.Clear();
                    this.ProductoExEs.Items.Add(new RadComboBoxItem("", ""));

                    string ProductoExEs = "L";
                    string text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsEstandar");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));
                    ProductoExEs = "K";
                    text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsExclusivo");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));
                    //--> ALB 20190402 - Afegir productes Exclusivos Plus al filtre del catàleg de productes
                    ProductoExEs = "K+";
                    text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsExclusivoPlus");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));
                    this.ProductoExEs.SelectedValue = ProductoExEsFilterOldValue;
                    //load Categoria                    
                    SetCategoriesTreeView();
                }
                if (articles != null)
                {
                    //Load Proveidors. Los proveedores los cargamos en base a los artículos que el cliente puede comprar
                    this.Proveidros_Combo.ClearSelection();
                    this.Proveidros_Combo.Items.Clear();

                    var proveedores = (from p in articles
                                       orderby p.GDONoVendedor
                                       select p.GDONoVendedor).Distinct();
                    if (!string.IsNullOrEmpty(GrupProveidorFilter.Value))
                        this.Proveidros_Combo.Items.Add(new RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AllAssociatedProducts"), "AllAssociatedProducts"));

                    foreach (string prov in proveedores)
                    {
                        this.Proveidros_Combo.Items.Add(new RadComboBoxItem(prov, prov));
                    }
                    if (this.Proveidros_Combo.Items.FindItemByValue(selectedProv) == null)
                    {
                        this.Proveidros_Combo.Items.Add(new RadComboBoxItem(selectedProv, selectedProv));
                    }
                    this.Proveidros_Combo.SelectedValue = selectedProv;
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private List<TopMenuItem> GetMenuItems()
        {
            List<TopMenuItem> Categorias = new List<TopMenuItem>();
            List<TopMenuItem> topMenuItems;
            var tipoCompra = 0; //LaForma

            TopMenuDAO topMenuDAO = new TopMenuDAO(this.currentUsername, this.currentClientCode, tipoCompra, this.currentUser.Language);
            topMenuItems = topMenuDAO.MenuItems;
            return topMenuItems;
        }
        private List<TopMenuItem> loadCategoryList(string type, List<TopMenuItem> topMenuItems)
        {
            List<TopMenuItem> Categorias;
            Categorias = topMenuItems.Where(p => p.Tipo == type).ToList();
            return Categorias;
        }
        private List<TopMenuItem> GetSubMenuItems(string key, string tipo, List<TopMenuItem> topMenuItems)
        {
            List<TopMenuItem> SubCategorias;
            SubCategorias = topMenuItems.Where(p => p.Tipo == tipo && p.ParentCode.ToUpper() == key.ToUpper()).ToList();
            return SubCategorias;
        }
        private void SetCategoriesTreeView()
        {
            List<TopMenuItem> Categorias = loadCategoryList("0", GetMenuItems());
            List<TopMenuItem> SubCategorias = loadCategoryList("1", GetMenuItems());
            List<TopMenuItem> SubsubCategorias = loadCategoryList("4", GetMenuItems());

            this.CategoriaTreeView.Nodes.Clear();
            List<TreeViewDataItem> treeViewDataItemList = new List<TreeViewDataItem>();
            foreach (TopMenuItem categoria in Categorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(categoria.Codigo.Replace(" ", "")) + 10000, 0, categoria.Descripcion, categoria.Codigo.Replace(" ", "")));
            }
            foreach (TopMenuItem subCategoria in SubCategorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(subCategoria.Codigo.Replace(" ", "")) + 10000, Int32.Parse(subCategoria.ParentCode.Replace(" ", "")) + 10000, subCategoria.Descripcion, subCategoria.Codigo.Replace(" ", "")));
            }
            foreach (TopMenuItem subsubCategoria in SubsubCategorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(subsubCategoria.Codigo.Replace(" ", "")) + 10000, Int32.Parse(subsubCategoria.ParentCode.Replace(" ", "")) + 10000, subsubCategoria.Descripcion, subsubCategoria.Codigo.Replace(" ", "")));
            }
            this.CategoriaTreeView.DataTextField = "text";
            this.CategoriaTreeView.DataFieldID = "id";
            this.CategoriaTreeView.DataFieldParentID = "parentId";
            this.CategoriaTreeView.DataValueField = "valueCode";
            this.CategoriaTreeView.DataSource = treeViewDataItemList;
            this.CategoriaTreeView.DataBind();

            RadTreeNode nodeCategory = this.CategoriaTreeView.FindNodeByValue(fCategory);
            if (nodeCategory != null)
            {
                nodeCategory.Selected = true;
                if (nodeCategory.Level != 0) { nodeCategory.ParentNode.Expanded = true; }
                else { nodeCategory.Expanded = true; }
                if (nodeCategory.Level == 2) nodeCategory.ParentNode.ParentNode.Expanded = true;
            }
        }
        private void ReLoadVariableFilters()
        {
            try
            {
                List<Article> articles = Session["ArticlesGdo"] as List<Article>;
                if (articles != null)
                {
                    this.Subfamilia.Items.Clear();
                    this.Subfamilia.Items.Add(new RadComboBoxItem("", ""));
                    //load subfamilies
                    string SubFamilyFilterOldValue = fSubFamily;
                    if (this.Familia.SelectedValue != String.Empty)
                    {
                        var subFamilies = (from p in articles
                                           where p.Family == this.Familia.SelectedValue
                                           orderby p.SubFamily
                                           select p.SubFamily).Distinct();

                        foreach (string subfamilia in subFamilies)
                        {
                            this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                        }
                    }
                    else
                    {
                        var subFamilies = (from p in articles
                                           orderby p.SubFamily
                                           select p.SubFamily).Distinct();

                        foreach (string subfamilia in subFamilies)
                        {
                            this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                        }
                    }
                    this.Subfamilia.SelectedValue = SubFamilyFilterOldValue;
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Inicialice();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ClientID, "ProductEffects();", true);
        }
        public DataTable LoadData()
        {

            fCatalog = Request.QueryString["fCatalogGdo"];
            Session["fCatalogGdo"] = fCatalog;
            fFamily = (Session["fFamilyGdo"] != null) ? Session["fFamilyGdo"].ToString() : this.Familia.SelectedValue;
            fSubFamily = (Session["fSubFamilyGdo"] != null) ? Session["fSubFamilyGdo"].ToString() : this.Subfamilia.SelectedValue;
            fProgram = (Session["fProgramGdo"] != null) ? Session["fProgramGdo"].ToString() : this.Programa.SelectedValue;
            fSearchTxt = (Session["fSearchTxtGdo"] != null) ? Session["fSearchTxtGdo"].ToString() : this.SearchProduct.Text;
            fPageNo = (Session["fPageNoGdo"] != null) ? Convert.ToInt32(Session["fPageNoGdo"].ToString()) : this.RadListView1.CurrentPageIndex;
            fCategory = (Session["fCategoryGdo"] != null) ? Session["fCategoryGdo"].ToString() : this.CategoriaTreeView.SelectedValue;
            fEstance = (Session["fEstanceGdo"] != null) ? Session["fEstanceGdo"].ToString() : String.Empty;
            fStyle = (Session["fStyleGdo"] != null) ? Session["fStyleGdo"].ToString() : String.Empty;
            fProductoExEs = (Session["fProductoExEs"] != null) ? Session["fProductoExEs"].ToString() : this.ProductoExEs.SelectedValue;

            return LoadData(fPageNo, PageSize);  //int.MaxValue);
        }
        public DataTable LoadData(int pageNo, int pageItems)
        {
            DataTable table = null;
            try
            {

                string filter = fSearchTxt; // this.SearchProduct.Text.Trim().ToUpper();
                filter = filter.Trim().ToUpper();

                table = new DataTable();
                Boolean CheckFilters = false;
                List<string> CodRepeat = new List<string>();
                List<Article> articles = new List<Article>();

                articles = (List<Article>)Session["ArticlesGdo"];
                if (Session["OrderGdo"] != null)
                {
                    CurrentOrder = (Order)Session["OrderGdo"];
                }

                if (articles != null)
                {
                    #region Add Table Columns
                    table.Columns.Add("Code");
                    table.Columns.Add("Description");
                    table.Columns.Add("Price");
                    table.Columns.Add("NavigateUrl");
                    table.Columns.Add("ImageUrl");
                    table.Columns.Add("Height");
                    table.Columns.Add("Length");
                    table.Columns.Add("Width");
                    table.Columns.Add("CodeWS");
                    table.Columns.Add("UnidadMedida");
                    table.Columns.Add("UnidadMinima");
                    table.Columns.Add("ImgErrorUrl");
                    table.Columns.Add("ImgDiscountUrl");
                    table.Columns.Add("PriceWithDiscount");
                    table.Columns.Add("DiscountLabel");

                    table.Columns.Add("Proveidor");
                    table.Columns.Add("GrupProveidor");

                    table.Columns.Add("ShowAgrupColor", typeof(bool));
                    table.Columns.Add("ShowAgrupMeasures", typeof(bool));
                    table.Columns.Add("ColorsAgrup");
                    table.Columns.Add("MeasuresAgrupLink");
                    table.Columns.Add("MeasuresAgrupText");
                    table.Columns.Add("NewProduct", typeof(bool));
                    table.Columns.Add("ComingSoon", typeof(bool));

                    #endregion Add Table Columns
                    LoadCurrentUserandClientData();
                    //Get current UI Culture
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    #region Apply filters to Articles (catalogo, familia, subfamilia, programa)
                    List<Article> collection = articles;

                    bool accesoComingSoon = this.currentUser.AccesoComingSoon;

                    if (!accesoComingSoon)
                        collection = collection.Where(p => p.ComingSoon == "No").ToList<Article>();

                    if (!(string.IsNullOrEmpty(filter)))
                    {
                        collection = collection.Where(p => p.Code.ToUpper().Contains(filter) ||
                                       p.Description.ToUpper().Contains(filter)).ToList<Article>();
                        CheckFilters = true;
                    }
                    else
                    {
                        //filter by Principal, Agrupacions
                        collection = collection.Where(p => p.PrincipalAgrup == "Sí").ToList<Article>();
                    }
                    //if (this.Familia.SelectedValue != String.Empty)
                    if (fFamily != String.Empty)
                    {
                        collection = collection.Where(p => p.Family == fFamily).ToList<Article>();
                        CheckFilters = true;
                    }
                    //filter for the subfamily
                    //if (this.Subfamilia.SelectedValue != String.Empty)
                    if (fSubFamily != String.Empty)
                    {
                        collection = collection.Where(p => p.SubFamily == fSubFamily).ToList<Article>();
                        CheckFilters = true;
                    }
                    //filter for the program
                    //if (this.Programa.SelectedValue != String.Empty)
                    if (fProgram != String.Empty)
                    {
                        collection = collection.Where(p => p.Programa == fProgram).ToList<Article>();
                        CheckFilters = true;
                    }
                    if (fCategory != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fCategory)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    if (fEstance != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fEstance)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    if (fStyle != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fStyle)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }

                    //filter for the product type
                    if (fProductoExEs != String.Empty)
                    {
                        collection = collection.Where(p => p.ProductType == fProductoExEs).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }

                    #endregion Apply filters to Articles (catalogo, familia, subfamilia, programa)

                    EstadoFiltros.Visible = false;
                    EstadoDivisa.Visible = false;

                    #region Apply Proveidor filters
                    string pfilter = this.Proveidros_Combo.SelectedValue;
                    string gfilter = this.GrupProveidorFilter.Value;

                    List<Article> UnionProvcollection = collection;
                    if (this.CurrentOrder.ListOrderProducts.Count == 0)
                    {
                        //No tenemos compra iniciada
                        if (pfilter == string.Empty)
                        {
                            //es mostren tots els productes iel combo te el todos los productos
                            //Solo debe ocurrir en la primera carga de la pagina o en paginaciones
                            this.EstadoFiltros.Text = "";
                        }
                        else
                        {
                            //Si el proveedor es agrupable, filtramos los productos de toda la union de proveedores
                            if (!String.IsNullOrEmpty(gfilter))
                            {
                                UnionProvcollection = collection.Where(p => p.GDOTipoUnion == gfilter).ToList<Article>();
                            }
                            else
                            {
                                //productos de ese proveeodr
                                UnionProvcollection = collection.Where(p => p.GDONoVendedor == pfilter).ToList<Article>();
                            }
                            //Ya hemos obtenido todos los productos del grupo. Ahora obtenemos los que vamos a mostrar por pantalla
                            if (pfilter != "AllAssociatedProducts")
                            {
                                //Si han seleccionado un proveedor en el combo, mostramos solo los de ese proveedor
                                collection = UnionProvcollection.Where(p => p.GDONoVendedor == pfilter).ToList<Article>();
                            }
                            else
                            {
                                //Si han seleccionado en el combo Todos los proveedores asociados.
                                collection = UnionProvcollection;
                            }


                            //combo de proveedores con los asociados y el todos los productos de los asociados
                            EstadoFiltros.Visible = true;
                            //label indicando estado
                            this.EstadoFiltros.Text = string.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblProveidorFiltratSinCompra"), Proveidros_Combo.Text);

                        }

                    }
                    else
                    {
                        //Tenemos compra iniciada
                        //el filtro de grupo viene marcado por la compra
                        gfilter = CurrentOrder.ListOrderProducts.First().Product.GDOTipoUnion;
                        if (String.IsNullOrEmpty(pfilter))
                        {//Si no han seleccionado un proveedor en el combo obtenemos el primero

                            pfilter = CurrentOrder.ListOrderProducts.First().Product.GDONoVendedor;
                            //Si quisiesemos mostrar todos los asociados dariamos valor a p filter. Descomentar la linia siguiente
                            pfilter = (String.IsNullOrEmpty(gfilter)) ? pfilter : "AllAssociatedProducts";
                            this.Proveidros_Combo.SelectedValue = pfilter;
                        }
                        if (String.IsNullOrEmpty(gfilter))
                        {
                            //Si el producto NO tiene proveedores asociados. Filtramos la coleccion por el proveedor de la compra
                            pfilter = CurrentOrder.ListOrderProducts.First().Product.GDONoVendedor;
                            UnionProvcollection = UnionProvcollection.Where(p => p.GDONoVendedor == pfilter).ToList<Article>();
                            collection = UnionProvcollection;
                        }
                        else
                        {
                            //Si el producto tiene proveedores asociados. Filtramos por el grupo de proveedores y por el valor del selector de proveedor (Si tiene valor)
                            UnionProvcollection = collection.Where(p => p.GDOTipoUnion == gfilter).ToList<Article>();
                            //Ya hemos obtenido todos los productos del grupo. Ahora obtenemos los que vamos a mostrar por pantalla
                            if (pfilter != "AllAssociatedProducts")
                            {
                                //Si han seleccionado un proveedor en el combo, mostramos solo los de ese proveedor
                                collection = UnionProvcollection.Where(p => p.GDONoVendedor == pfilter).ToList<Article>();
                            }
                            else
                            {
                                //Si han seleccionado en el combo Todos los proveedores asociados.
                                collection = UnionProvcollection;
                            }
                        }

                        EstadoFiltros.Visible = true;
                        this.EstadoFiltros.Text = string.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblProveidorFiltratConCompra"), Proveidros_Combo.Text);
                    }
                    #endregion Apply Proveidor filters

                    

                    if ((string)Session["Divisa"] == "1")
                    {
                        this.EstadoDivisa.Visible = true;
                        this.EstadoDivisa.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblDivisaNotUSD");
                    }
                    else
                    {
                        this.EstadoDivisa.Visible = false;
                        this.EstadoDivisa.Text = "";
                    }


                    //if(this.currentClient.
                    //filter for the proveidor
                    int rowId = 0;
                    int TotalItems = collection.Count();

                    RadListView1.VirtualItemCount = TotalItems;
                    collection = collection.Skip(pageNo * pageItems).ToList();

                    foreach (Article a in collection)
                    {
                        #region Transformación de datos
                        DataRow row = table.NewRow();

                        row["Code"] = a.Code;
                        row["CodeWS"] = a.Code.Replace(' ', '_');

                        if (a.Description.Length > 50)
                        {
                            row["Description"] = a.Description.Substring(0, 50) + "...";
                        }
                        else
                        {
                            row["Description"] = a.Description;
                        }

                        row["NavigateUrl"] = "/gdo/Pages/productGdoInfo.aspx?code=" + a.Code + "&catalog=" + a.InsertcatalogNo + "&version=" + a.ArticleVersion;
                        row["ImageUrl"] = a.SmallImgUrl;
                        row["Height"] = a.Height + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        row["Width"] = a.Width + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        row["Length"] = a.Length + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        if ((string)Session["Divisa"] == "0")
                        {
                            row["Price"] = a.Price.ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars");
                        }
                        else if ((string)Session["Divisa"] == "2")
                        {
                            row["Price"] = (a.Price * Convert.ToDouble(currentUser.CoeficientGDOPVP)).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars");
                        }
                        else
                        {
                            row["Price"] = (a.Price * Convert.ToDouble(currentUser.DolarCoef)).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        }

                        row["UnidadMedida"] = a.UnidadMedida;
                        row["UnidadMinima"] = a.GDOMinQty;

                        row["ImgDiscountUrl"] = setImgDiscountURL(a);
                        row["ImgErrorUrl"] = "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg';";
                        row["PriceWithDiscount"] = setDiscount(a);
                        /*Descompte foramtejat*/
                        if (!(a.Discount == "0,00"))
                        {
                            row["DiscountLabel"] = Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N0") + "%";
                        }
                        else
                        {
                            row["DiscountLabel"] = "";
                        }
                        row["Proveidor"] = a.GDONoVendedor;
                        row["GrupProveidor"] = a.GDOTipoUnion;

                        //Agrupacions

                        if (a.TipoAgrup == 1 && a.DetalleAgrup != string.Empty)
                        {
                            //Color

                            row["ShowAgrupColor"] = true;
                            row["ShowAgrupMeasures"] = false;
                            row["ColorsAgrup"] = ColorItemsProductImage(a); // ColorItems(a);
                        }
                        else if (a.TipoAgrup == 2 && a.DetalleAgrup != string.Empty)
                        {
                            //Mesures

                            row["ShowAgrupColor"] = false;
                            row["ShowAgrupMeasures"] = true;
                            row["MeasuresAgrupLink"] = "/gdo/Pages/productGdoInfo.aspx?code=" + a.Code + "&catalog=" + a.InsertcatalogNo + "&version=" + a.ArticleVersion;
                            row["MeasuresAgrupText"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("VerMasMedidas");
                        }
                        else
                        {
                            row["ShowAgrupColor"] = false;
                            row["ShowAgrupMeasures"] = false;
                        }
                        if (a.ProductNew == "No")
                        {
                            row["NewProduct"] = false;
                        }
                        else
                        {
                            row["NewProduct"] = true;
                        }
                        if (a.ComingSoon == "No")
                        {
                            row["ComingSoon"] = false;
                        }
                        else
                        {
                            row["ComingSoon"] = true;
                        }

                        #endregion Transformación de datos

                        #region Adicion de datos
                        table.Rows.Add(row);
                        rowId++;
                        #endregion Adicion de datos

                        if (rowId >= pageItems) break;
                    }
                    LoadFilters(UnionProvcollection, pfilter);
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            //Añadimos los datos a una cache para utilizarla en paginaciones (ya que no cambian los datos)
            HttpContext.Current.Cache.Remove("ArticlesGdo_Paging_cache_" + currentUser.UserName + currentClient.Code);
            HttpContext.Current.Cache.Add("ArticlesGdo_Paging_cache_" + currentUser.UserName + currentClient.Code, table, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);

            return table;
        }

        private DataTable loadComboMeasuersValue(Article a)
        {
            DataTable table = new DataTable();
            try
            {
                table.Columns.Add("Name");
                table.Columns.Add("Value");

                List<string> ListMeasures = Products.Where(p => p.PatronAgrup == a.PatronAgrup).Select(p => p.CodMedida).ToList();

                foreach (string artMed in ListMeasures)
                {
                    DataRow row = table.NewRow();
                    row["Name"] = artMed;
                    row["Value"] = a.PatronAgrup + ";" + artMed;
                    table.Rows.Add(row);
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return table;
        }
        private void Inicialice()
        {
            //Default Values from filters
            this.Familia.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectFamily");
            this.Subfamilia.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectSubFamily");
            this.Programa.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProgram");
            this.Proveidros_Combo.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProveidor");
            this.ProductoExEs.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProExEs");
            if (Session["ArticleGdoDAO"] != null)
            {
                articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
            }
            Products = articleDAO.GetAllProducts();
        }
        public void RadListView1_PageIndexChanged(object sender, RadListViewPageChangedEventArgs e)
        {
            LoadCurrentUserandClientData();
            getCachedData = false;

            SetHistoryPoint(e.NewPageIndex);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "ScrollPage", "ResetScrollPosition();", true);


        }

        public void LoadCurrentUserandClientData()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                this.currentClient = (Client)Session["Client"];
            }
            if (Session["OrderGdo"] != null)
            {
                this.CurrentOrder = (Order)Session["OrderGdo"]; ;
            }
        }

        public void RadListView1_Load(object sender, EventArgs e)
        {

        }

        public void NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            try
            {
                this.RadListView1.CurrentPageIndex = 0;
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void FamiliaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Familia.SelectedValue = e.Value;
                this.Subfamilia.ClearSelection();

                //this.Programa.ClearSelection();
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;

                ReLoadVariableFilters();
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void ibSearchProduct_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                this.RadListView1.CurrentPageIndex = 0;
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void ClearFilters()
        {
            //Session.Remove("fprovGdo"); //Esborrem el filtre de proveidor especific
            //Session.Remove("fgprovGdo"); //Esborrem el filtre de proveidor especific
            Session.Remove("fSearchTxtGdo");
            Session.Remove("fFamilyGdo");
            Session.Remove("fSubFamilyGdo");
            Session.Remove("fProgramGdo");
            Session.Remove("fCategoryGdo");
            Session.Remove("fEstanceGdo");
            Session.Remove("fStyleGdo");
            Session.Remove("fPageNoGdo");
            Session.Remove("fCatalogGdo");
            Session.Remove("fCatalogGdo");
            Session.Remove("fProductoExEs");
            this.ProductoExEs.ClearSelection();
            this.Proveidros_Combo.ClearSelection();
            this.GrupProveidorFilter.Value = String.Empty;
            this.Familia.ClearSelection();
            this.Subfamilia.ClearSelection();
            this.Programa.ClearSelection();
            this.CategoriaTreeView.UnselectAllNodes();
            this.SearchProduct.Text = "";
            this.RadListView1.CurrentPageIndex = 0;
        }

        public void ibClearFilter_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearFilters();

                CurrentOrder = (Order)Session["OrderGdo"];
                if (CurrentOrder.ListOrderProducts.Count == 0)
                {
                    Session["GrupoProveedoresFilter"] = string.Empty;
                }
                Session["ProveedorFilter"] = string.Empty;
                Response.Redirect("/gdo/Pages/Home.aspx");

            }
            catch (ThreadAbortException)
            {
                // Do nothing. ASP.NET is redirecting.
                // Always comment this so other developers know why the exception 
                // is being swallowed.
                //Capturamos la excepción que por diseño ASP.Net lanza para Response.End o para Response.Redirect
                //Otra alternativa es mover el redirect fuera del catch
                //http://stackoverflow.com/questions/4368640/response-redirect-and-thread-was-being-aborted-error
                //http://briancaos.wordpress.com/2010/11/30/response-redirect-throws-an-thread-was-being-aborted/
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetHistoryPoint()
        {
            SetHistoryPoint(this.RadListView1.CurrentPageIndex);
        }
        private void SetHistoryPoint(int newPageNo)
        {
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("Family", this.Familia.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("SubFamily", this.Subfamilia.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("Program", this.Programa.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("SearchTxt", this.SearchProduct.Text);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("PageNo", newPageNo.ToString());
            Session["fFamilyGdo"] = this.Familia.SelectedValue;
            Session["fSubFamilyGdo"] = this.Subfamilia.SelectedValue;
            Session["fProgramGdo"] = this.Programa.SelectedValue;
            Session["fCategoryGdo"] = this.CategoriaTreeView.SelectedValue;
            Session["fSearchTxtGdo"] = this.SearchProduct.Text;
            Session["fPageNoGdo"] = newPageNo.ToString();
            Session["fCatalogGdo"] = fCatalog;
            Session["fProductoExEs"] = this.ProductoExEs.SelectedValue;
        }


        public void SubFamiliaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Subfamilia.SelectedValue = e.Value;
                this.Programa.ClearSelection();
                this.RadListView1.CurrentPageIndex = 0;

                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private String ColorItems(Article a)
        {
            string ProductItemTmp = string.Empty;
            string url = string.Empty;
            string Imageurl = string.Empty;
            string[] ArtCol = a.DetalleAgrup.Split(';');

            for (int i = 0; i < ArtCol.Length; i++)
            {
                Imageurl = "/Style Library/Color Images/" + ArtCol[i] + ".jpg";
                url = "/gdo/Pages/RedirectTo.aspx?Colorcode=" + ArtCol[i] + "&PatronAgrup=" + a.PatronAgrup;
                ProductItemTmp += "<a href='" + url + "'><img alt='" + ArtCol[i] + "' title='" + JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtCol[i]) + "' src='" + Imageurl + "' class='ColorItem' /></a>";
            }

            return ProductItemTmp;
        }

        ///<summary>
        ///Funcion que devuelve el código HTML para mostrar las imagenes de los productos de la misma 
        ///agrupación de color
        ///</summary>
        ///<remarks>
        ///Devuelve hasta 3 imágenes y una marca de más en caso que la agrupación esté formada por más de 4 artículos
        ///Importante Article.DetalleAgrup y Article.DetalleAgrupColor deben estar en el mismo orden
        ///</remarks>
        private String ColorItemsProductImage(Article a)
        {
            string ProductItemTmp = string.Empty;
            string url = string.Empty;
            string Imageurl = string.Empty;

            string[] ArtColorCodeCol = a.DetalleAgrup.Split(';');    //Contiene todos los códigos de color de la agrupación
            string[] ArtColorProductCodeCol = a.DetalleAgrupColor.Split(';');   //Contiene hasta 4 códigos de producto de la agrupación
            int iAgrupCount = ArtColorProductCodeCol.Length;

            for (int i = 0; ((i < iAgrupCount) && (i < 3)); i++)
            {

                url = "/gdo/Pages/RedirectTo.aspx?Colorcode=" + ArtColorCodeCol[i] + "&PatronAgrup=" + a.PatronAgrup;
                Imageurl = a.SmallImgUrl.Replace(a.Code, ArtColorProductCodeCol[i]);    //Mostramos la imagen del artículo
                ProductItemTmp += "<a href='" + url + "'><img alt='" + ArtColorCodeCol[i] + "' title='" + JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtColorCodeCol[i]) + "' src='" + Imageurl + "' class='ColorItem ColorItemProduct' /></a>";
            }
            if (iAgrupCount > 3)
            {
                string pUrl = "/gdo/Pages/productGdoInfo.aspx?code=" + a.Code + "&catalog=" + a.InsertcatalogNo + "&version=" + a.ArticleVersion;
                ProductItemTmp += "<a href='" + pUrl + "'><div class='ColorItemMore'></div></a>";
            }

            return ProductItemTmp;
        }

        public void ProgramaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Programa.SelectedValue = e.Value;
                this.RadListView1.CurrentPageIndex = 0;

                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadListView1_ItemCommand(object sender, RadListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "AddItemToCart":
                        RadNumericTextBox num = (RadNumericTextBox)e.ListViewItem.FindControl("itemQty");
                        Literal code = (Literal)e.ListViewItem.FindControl("productCode");
                        if (num != null && code != null)
                        {
                            this.AddItemToCart(code.Text, (int)num.Value);
                        }
                        break;
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.scripClientScript.RegisterClientScriptBlock(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void AddItemToCart(string articleCode, int qtt)
        {
            try
            {
                Order currentOrder = Session["OrderGdo"] as Order;

                Client cl = (Client)Session["Client"];
                string code = string.Empty;
                if (!string.IsNullOrEmpty(articleCode) && qtt > 0)
                {
                    List<Article> articles = Session["ArticlesGdo"] as List<Article>;
                    articles = articles.Where(p => p.Code == articleCode).ToList<Article>();
                    code = articles[0].Code;

                    if (articles.Count > 0)
                        currentOrder.AddProduct(articles[0], qtt, "", cl.UserName);


                    Session["ProveedorFilter"] = articles[0].GDONoVendedor;
                    Session["GrupoProveedoresFilter"] = articles[0].GDOTipoUnion;
                    this.Proveidros_Combo.SelectedValue = articles[0].GDONoVendedor;

                }
                ContenedorUserControl contenedor = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                contenedor.Refresh();

                Session["OrderGdo"] = currentOrder;

                //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));                
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "HideSearchBoxTopMenu", "HideSearchBoxTopMenu();", true);

                //this.RadListView1.DataSource = LoadData();
                //this.RadListView1.DataBind();
            }
            catch (NavException ex)
            {
                //string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "radalert", radalertscripts, true);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);


            }
        }
        public Control FindControlRecursive(Control Root, string Id)
        {
            try
            {
                if (Root.ID == Id)
                    return Root;
                foreach (Control Ctl in Root.Controls)
                {
                    Control FoundCtl = FindControlRecursive(Ctl, Id);
                    if (FoundCtl != null)
                        return FoundCtl;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return null;
        }
        //    protected void RadListView1_DataBound(object sender, EventArgs e)
        //    {
        ////        foreach (RadListViewDataItem item in RadListView1.Items)
        ////        {
        ////            Label Label_Price = (Label)item.FindControl("Label_Price");
        ////            Label Label_Discount = (Label)item.FindControl("Label_Discount");
        ////            Panel Price_Panel = (Panel)item.FindControl("Panel_Price");
        ////            Panel Price_Discount = (Panel)item.FindControl("Panel_PriceWithDiscount");

        ////            if (Price_Discount.ID == "Panel_PriceWithDiscount")
        ////            {
        ////                Price_Discount.Style.Add("color", "green");
        ////                Price_Discount.Style.Add("text-decoracion", "none");

        ////            }
        ////            if (Label_Discount.Text != "")
        ////            {
        ////                Price_Panel.Style.Add("text-decoration", "line-through");
        ////                Price_Panel.Style.Add("color", "rgb(154,46,47)");
        ////            }
        ////            else
        ////            {
        ////                Price_Panel.Style.Add("text-decoration", "none");
        ////                Price_Panel.Style.Add("color", "rgb(154,46,47)");
        ////            }
        ////        }

        //   }

        private object setDiscount(Article a)
        {
            string Output = string.Empty;
            string oldprice = a.Price.ToString("N2");


            if (!(a.Discount == "0,00"))
            {
                if ((int)Session["PriceSelector"] == 0)
                {
                    Output = "  " + a.DiscountNet + " " + LanguageManager.GetLocalizedString("Dollars");

                }
                else if ((int)Session["PriceSelector"] == 1)
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        Output = String.Format(a.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                    }
                }
                else
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        //REZ 04052013 - Esta calculant malament
                        //Output = String.Format(a.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        Output = ((Convert.ToDouble(a.DiscountNet, new CultureInfo("es-Es", false))) * (Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                    }
                }
            }
            return Output;
        }
        private object setImgDiscountURL(Article a)
        {
            //Image Imagen = (Image)this.RadListView1.Page.FindControl("ImgDiscount");

            string output = string.Empty;

            if (a.Discount.ToString() == "0,00")
            {
                output = "/Style%20Library/Julia/img/null_pixel.png";
                //Imagen.Visible = false;

            }
            else
            {
                //REZ 13052015 - Producte en liquidació
                //if (a.CatalogNo.StartsWith("PRELIQ"))
                if (a.TipusIcona == 2)
                {
                    output = "/Style%20Library/Julia/img/dto_liquidaciones.png";
                }
                else
                {
                    output = "/Style%20Library/Julia/img/dto_promocion.png";
                }
                //Imagen.Visible = true;
            }

            return output;
        }
        protected override void OnPreRender(EventArgs e)
        {

            RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
            ajaxManager.ClientEvents.OnRequestStart = "onRequestStart";

            if (ajaxManager == null)
            {
                RadAjaxManager manager = new RadAjaxManager();
                manager.ID = "RadAjaxManager1";
                manager.ClientEvents.OnRequestStart = "onRequestStart";
                Page.Items.Add(typeof(RadAjaxManager), manager);
                Page.Form.Controls.Add(manager);
            }

            DataTable cachedArticles = null;
            if (getCachedData)
            {
                LoadCurrentUserandClientData();
                cachedArticles = (DataTable)HttpContext.Current.Cache.Get("ArticlesGdo_Paging_cache_" + currentUser.UserName + currentClient.Code);
            }
            if (cachedArticles == null)
            {
                cachedArticles = LoadData();
            }
            this.RadListView1.DataSource = cachedArticles;
            this.RadListView1.DataBind();
            this.RadListView1.CurrentPageIndex = fPageNo;
            this.SearchProduct.Text = fSearchTxt;

            base.OnPreRender(e);
        }
        protected void Proveidros_Combo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (Proveidros_Combo.SelectedValue == "")
                    CodiProveidor = string.Empty;

                if (e.Value == "AllAssociatedProducts")
                {
                    //EN el cas que volguem mostrar tots els productes dels proveidors asscoiats...
                    //Session.Remove("fprovGdo"); //Esborrem el filtre de proveidor especific
                    string strprov = Session["ProveedorFilter"].ToString();
                    var CogGrupProveedor = Products.Where(p => p.GDONoVendedor == strprov).Select(p => p.GDOTipoUnion).Distinct().First();
                    this.Proveidros_Combo.SelectedValue = e.Value;

                    GrupProveidorFilter.Value = CogGrupProveedor;
                    //Session["fgprovGdo"] = CogGrupProveedor;
                    Session["GrupoProveedoresFilter"] = CogGrupProveedor;
                    Session.Remove("ProveedorFilter");
                }
                else if (e.Value != "")
                {

                    var CogGrupProveedor = Products.Where(p => p.GDONoVendedor == e.Value).Select(p => p.GDOTipoUnion).Distinct().First();
                    this.Proveidros_Combo.SelectedValue = e.Value;

                    GrupProveidorFilter.Value = CogGrupProveedor;
                    //Session["fgprovGdo"] = CogGrupProveedor;
                    Session["GrupoProveedoresFilter"] = CogGrupProveedor;
                    //Session["fprovGdo"] = e.Value;
                    Session["ProveedorFilter"] = e.Value;
                }
                else
                {
                    GrupProveidorFilter.Value = string.Empty;
                    //Session.Remove("fprovGdo"); //Esborrem el filtre de proveidor especific
                    Session.Remove("ProveedorFilter");
                    //Session.Remove("fgprovGdo"); //Esborrem el filtre de proveidor especific
                    //Solo si no hay compra...
                    CurrentOrder = (Order)Session["OrderGdo"];
                    if (CurrentOrder.ListOrderProducts.Count == 0)
                    {
                        Session["GrupoProveedoresFilter"] = string.Empty;
                    }
                }

                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void ProductoExEsSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.ProductoExEs.SelectedValue = e.Value;
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        void PageSizeComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Session.Add("ItemsPerPageGdo", e.Value);
            this.PageSize = int.Parse(e.Value);
            getCachedData = true;

            this.RadListView1.CurrentPageIndex = 0;

            SetHistoryPoint();
        }

        protected void RadDataPager_FieldCreated(object sender, RadDataPagerFieldCreatedEventArgs e)
        {
            try
            {
                if (e.Item.Field is RadDataPagerPageSizeField)
                {
                    var combo = e.Item.FindControl("PageSizeComboBox") as RadComboBox;

                    if (combo != null)
                    {
                        combo.Items.Clear();
                        AddComboItem(combo, "64");
                        AddComboItem(combo, "128");
                        AddComboItem(combo, "256");
                        //carreguem el valor de Session (CASE)
                        if (Session["ItemsPerPageGdo"] != null)
                        {
                            combo.SelectedValue = Session["ItemsPerPageGdo"].ToString();
                        }
                        combo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(PageSizeComboBox_SelectedIndexChanged);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        protected void AddComboItem(RadComboBox combo, string value)
        {
            try
            {
                if (combo.FindItemByValue(value) == null)
                    combo.Items.Add(new RadComboBoxItem(value, value));
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void ComboMeasures_Change(object sender, EventArgs e)
        {

            RadComboBox ComboMeasures = (RadComboBox)this.RadListView1.FindControl("ComboMeasures") as RadComboBox;
            string[] dataNewArt = ComboMeasures.SelectedValue.Split(';');
            Article newProduct = Products.Where(p => p.PatronAgrup == dataNewArt[0] && p.CodMedida == dataNewArt[1]).First() as Article;
            string Url = "/gdo/Pages/productGdoInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion; ;
            Response.Redirect(Url);
        }

        protected void ScriptManager1_Navigate(object sender, HistoryEventArgs e) // Event handler for restoring state
        {
            //this.Familia.SelectedValue = e.State["Family"];
            //this.Subfamilia.SelectedValue = e.State["SubFamily"];
            //this.Programa.SelectedValue = e.State["Programa"];
            //this.SearchProduct.Text = e.State["SearchTxt"];
            //this.RadListView1.CurrentPageIndex = Convert.ToInt32(e.State["PageNo"]);
        }

        public void PrintView_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);
            }
            catch (NavException ex)
            {
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
        }

        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                LoadCurrentUserandClientData();

                string codCatalog = string.Empty;
                string codprov = (this.Proveidros_Combo.SelectedValue == "AllAssociatedProducts") ? String.Empty : this.Proveidros_Combo.SelectedValue;
                string familia = this.Familia.SelectedValue;
                string subfamilia = this.Subfamilia.SelectedValue;
                string programa = this.Programa.SelectedValue;
                string categoria = this.CategoriaTreeView.SelectedValue;
                string txtSearch = this.SearchProduct.Text;
                string strProExEs = this.ProductoExEs.SelectedValue;
                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=informevista&UserCode=" + currentUser.UserName + "&ClientCode=" + currentClient.Code + "&prov=" + codprov + "&familia=" + familia + "&subfamilia=" + subfamilia + "&programa=" + programa + "&categoria" + categoria + "&filtre=" + txtSearch + "&Divisa=" + Session["Divisa"].ToString() + "&TipoUnion=" + Session["GrupoProveedoresFilter"].ToString() + "&Gdo=1";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            return output;
        }

    }
}
