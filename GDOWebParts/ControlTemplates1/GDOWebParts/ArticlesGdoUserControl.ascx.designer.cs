﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace GDOWebParts.ControlTemplates1.GDOWebParts {
    
    
    public partial class ArticlesGdoUserControl {
        
        /// <summary>
        /// Control ScriptManagerProxy1.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.ScriptManagerProxy ScriptManagerProxy1;
        
        /// <summary>
        /// Control RadAjaxManagerPedidoUserControl.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadAjaxManagerProxy RadAjaxManagerPedidoUserControl;
        
        /// <summary>
        /// Control RadCodeBlock2.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadCodeBlock RadCodeBlock2;
        
        /// <summary>
        /// Control EstadoFiltros.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label EstadoFiltros;
        
        /// <summary>
        /// Control EstadoDivisa.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label EstadoDivisa;
        
        /// <summary>
        /// Control SearchProduct.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTextBox SearchProduct;
        
        /// <summary>
        /// Control ibSearchProduct.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ibSearchProduct;
        
        /// <summary>
        /// Control CategoriaTreeView.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTreeView CategoriaTreeView;
        
        /// <summary>
        /// Control Proveidros_Combo.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox Proveidros_Combo;
        
        /// <summary>
        /// Control GrupProveidorFilter.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField GrupProveidorFilter;
        
        /// <summary>
        /// Control Familia.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox Familia;
        
        /// <summary>
        /// Control Subfamilia.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox Subfamilia;
        
        /// <summary>
        /// Control Programa.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox Programa;
        
        /// <summary>
        /// Control ProductoExEs.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox ProductoExEs;
        
        /// <summary>
        /// Control ibClearFilter.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ibClearFilter;
        
        /// <summary>
        /// Control deleteAll.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl deleteAll;
        
        /// <summary>
        /// Control RadListView1.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadListView RadListView1;
        
        /// <summary>
        /// Control RadNotification1.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Telerik.Web.UI.RadNotification RadNotification1;
    }
}
