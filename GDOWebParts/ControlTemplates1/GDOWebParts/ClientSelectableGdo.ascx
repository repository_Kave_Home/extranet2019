﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientSelectableGdo.ascx.cs"
    Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.ClientSelectableGdo" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Import Namespace="Microsoft.SharePoint.Publishing" %>
<style type="text/css">
    .ComboClients
    {
        margin-right: 10px;
    }
    .valign_middle
    {
        vertical-align: middle;
    }
</style>
<script type="text/javascript">

    var imgUrl = null;
    function alertCallBackFn(arg) {
        radalert("<strong>radalert</strong> returned the following result: <h3 style='color: #ff0000;'>" + arg + "</h3>", null, null, "Result");
    }

    function confirmCallBackFn(arg) {
        radalert("<strong>radconfirm</strong> returned the following result: <h3 style='color: #ff0000;'>" + arg + "</h3>", null, null, "Result");
    }

    function promptCallBackFn(arg) {
        radalert("After 7.5 million years, <strong>Deep Thought</strong> answers:<h3 style='color: #ff0000;'>" + arg + "</h3>", null, null, "Deep Thought");
    }

    function pageLoad() {
        //attach a handler to readion buttons to update global variable holding image url
        var $ = $telerik.$;
        $('input:radio').bind('click', function () {
            imgUrl = $(this).val();
        });
    }

    function OnClientSelectedIndexChanged(sender, args) {
        /*$.blockUI({ message: '<img src="/Style Library/Julia/img/loading.gif" width="66px" height="66px"/>' });*/
        $.blockUI({ message: $('#loadingPnl') });
    }
    function OnPricePointsSelectedIndexChanged(sender, args) {
        /*$.blockUI({ message: '<img src="/Style Library/Julia/img/loading.gif" width="66px" height="66px" />' });*/
        $.blockUI({ message: $('#loadingPnl') });
    }
    function OnLanguageSelectedIndexChanged(sender, args) {
        $.blockUI({ message: $('#loadingPnl') });

        var combo = $find("<%= LanguageSelect.ClientID %>");
        var itm = combo.get_selectedItem();
        var langvalue = itm.get_value()
        var url = itm.get_attributes().getAttribute("NewsUrl");

        <%if (SPContext.Current != null && PublishingWeb.IsPublishingWeb(SPContext.Current.Web))
        {
            PublishingWeb publishingWeb = PublishingWeb.GetPublishingWeb(SPContext.Current.Web);
            if (publishingWeb.Label != null)
            {
            %>
                 OnSelectionChange(langvalue, url);
            <%
            }
            else
            {
            %>
                OnSelectionChange(langvalue);
            <%
            }
        }%>
    }                                                                                          
</script>
<script type="text/javascript">
// <![CDATA[
    function OnSelectionChange(value, newurl) {
        /*$.blockUI({ message: '<img src=\'/Style Library/Julia/img/loading.gif\' width=\'66px\' height=\'66px\'/>' });*/
        $.blockUI({ message: $('#loadingPnl') });
        var today = new Date();
        var oneYear = new Date(today.getTime() + 365 * 24 * 60 * 60 * 1000);
        var url = window.location.href;
        document.cookie = "lcid=" + value + ";path=/;expires=" + oneYear.toGMTString();
        window.location.href = newurl || url;
    }
    /*ExecuteOrDelayUntilScriptLoaded(setSelectedCurrentLanguage, "core.js");*/
    function setSelectedCurrentLanguage() {
        var combo = $find("<%= LanguageSelect.ClientID %>");
        var itm = combo.findItemByValue(_spPageContextInfo.currentLanguage);
        itm.select();
    }
// ]]>
</script>
<div id="loadingPnl" style="display: none;">
    <img src="/Style Library/Julia/img/loading.gif" width="66px" height="66px" />
</div>
<asp:Panel ID="Panel1" runat="server">
    <telerik:RadWindowManager ID="RadWindowManager1" Skin="Silk" RenderMode="Lightweight"
        runat="server" EnableShadow="false" AutoSize="true">
    </telerik:RadWindowManager>
    <%--<span class="valign_middle">
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Client_WelComelbl")%></span>
    <asp:Label ID="lblUsername" runat="server" Text="UserName" CssClass="valign_middle"></asp:Label>
    <asp:Label ID="lblClient" runat="server" Text="- Cliente: " CssClass="valign_middle"></asp:Label>--%>
    <i class="fa fa-user fa-lg moveElementLeft" style="color: #777" title="<%= this.currentUsername %>">
    </i>
    <telerik:RadComboBox ID="RadComboBox" runat="server" Enabled="false" EnableTextSelection="true"
        MarkFirstMatch="true" Width="300px" CssClass="ComboClients valign_middle" Filter="Contains"
        DataTextField="Name" DataValueField="Code" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
        AutoPostBack="true" RenderMode="Lightweight" Skin="Material">
    </telerik:RadComboBox>
    <telerik:RadComboBox runat="server" CssClass="valign_middle" ID="DivisasCombo" AutoPostBack="true"
        OnSelectedIndexChanged="DivisasCombo_IndexChanged" RenderMode="Lightweight" Skin="Material">
    </telerik:RadComboBox>
    <i class="fa fa-globe fa-lg" style="color: #777"></i>
    <telerik:RadComboBox RenderMode="Lightweight" ID="LanguageSelect" Skin="Material"
        runat="server" Width="130px" DataTextField="Text" DataValueField="Value" AutoPostBack="false"
        OnClientSelectedIndexChanged="OnLanguageSelectedIndexChanged" CssClass="lateralPadding verticalSeparatorOnRight">
        <Items>
            <telerik:RadComboBoxItem Value="1027" NewsUrl="/News/ca-es/" Text="Català"></telerik:RadComboBoxItem>
            <telerik:RadComboBoxItem Value="3082" NewsUrl="/News/es-es/" Text="Castellano"></telerik:RadComboBoxItem>
            <telerik:RadComboBoxItem Value="1036" NewsUrl="/News/fr-fr/" Text="Français"></telerik:RadComboBoxItem>
            <telerik:RadComboBoxItem Value="1040" NewsUrl="/News/it-it/" Text="Italiano"></telerik:RadComboBoxItem>
            <telerik:RadComboBoxItem Value="1033" NewsUrl="/News/en-us/" Text="English"></telerik:RadComboBoxItem>
            <telerik:RadComboBoxItem Value="1031" NewsUrl="/News/de-de/" Text="Deutsch"></telerik:RadComboBoxItem>
        </Items>
    </telerik:RadComboBox>
    <%--<asp:LinkButton ID="lnkbtnclose" runat="server" CssClass="valign_middle" OnClick="lnkbtnclose_click"><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Client_SessionClose")%></asp:LinkButton>--%>
    <asp:HyperLink runat="server" CssClass="valign_middle" ID="LinkToManual" Target="_blank"><i class="fa fa-question-circle fa-lg headerIcons" alt='<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("HelpIcon")%>' title='<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("HelpIcon")%>'></i></asp:HyperLink>
    <%--<asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="/Pages/AccountAdministration.aspx" ><img src="/Style Library/Julia/img/config.png" style="vertical-align:middle;" width="15px" height="15px" alt='<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ConfigIcon")%>' title='<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ConfigIcon")%>' border="0" /></asp:HyperLink>--%>
    <%--<telerik:RadAjaxPanel ID="pnl1" runat="server">--%>
    <%--<asp:Button ID="btnAlert" Width="150" runat="server"   
        Text="radalert from server" OnCommand="Btn_OnCommand" 
        CommandArgument="radalert" />--%>
    <%-- </telerik:RadAjaxPanel>--%>
</asp:Panel>
<input type="hidden" id="currentUserName" value="<%= this.currentUsername %>" />
<input type="hidden" id="currentClientCode" value="<%= this.currentClientCode %>" />
<input type="hidden" id="currentPriceSelId" value="<%= this.currentPriceSelId %>" />
<input type="hidden" id="dollarCoef" value="<%= this.dollarCoef %>" />