﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Data;
using System.Linq;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Threading;
using JuliaGrupUtils.ErrorHandler;
using Telerik.Web.UI;
using System.Collections.Generic;


namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class ClientSelectableGdo : UserControl
    {
        private static int _defaultPageSize = 64;
        User currentUser;
        Client currentClient;
        Client Cli;
        int currentPriceSel;

        public string currentUsername
        {
            get
            {
                loaduserdata();
                return currentUser.UserName;
            }
        }
        public string currentClientCode
        {
            get
            {
                loaduserdata();
                return currentClient.Code;
            }
        }
        public int currentPriceSelId
        {
            get
            {
                loaduserdata();
                return currentPriceSel;
            }
        }
        public decimal dollarCoef
        {
            get
            {
                loaduserdata();
                return currentUser.DolarCoef;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            //REZ:: Mantenimiento de variables de Session

            base.OnInit(e);
        }
        protected void LoadSessionVars()
        {
            Sesion SesionJulia = new Sesion();

            /* REZ:: Porque carga Article, Order y Ventas todo el rato??? -- Lo quito a ver que pasa*/
            //Carrego Article

            if (Session["Order"] == null)
            {
                //Carrego Order
                Order Order;
                Order = SesionJulia.GetOrder(this.currentClient);
                if (Order == null)
                    throw new Exception("Error NULL en el GetOrder del cliente seleccionado");
                Session.Add("Order", Order);
            }
            if (Session["OrderGdo"] == null)
            {
                //Carrego Order
                Order Order;
                Order = SesionJulia.GetOrderGdo(this.currentClient);
                if (Order == null)
                    throw new Exception("Error NULL en el GetOrder del cliente seleccionado");
                Session.Add("OrderGdo", Order);
            }
            if (Session["GrupoProveedoresFilter"] == null)
            {
                Session.Add("GrupoProveedoresFilter", string.Empty);
            }
            // SI la order no conte producte resetegem la variable de grup provediors per tal de moficiar el filtratge de product List.

            if (Session["ProveedorFilter"] == null)
            {
                Session.Add("ProveedorFilter", string.Empty);
            }

            //if (Session["VentasDAO"] == null)
            //{
            //    //Carrego Ventas
            //    VentaDataAccessObject VentasDAO;
            //    VentasDAO = SesionJulia.GetVentas(cl.Code);
            //    if (VentasDAO == null)
            //        throw new Exception("Error NULL en el GetVentas del cliente seleccionado");
            //    Session.Add("VentasDAO", VentasDAO);
            //}

            if (Session["ItemsPerPageGdo"] == null)
            {
                Session.Add("ItemsPerPageGdo", (int)_defaultPageSize);
            }
            
            if (Session["PriceSelector"] == null)
            {
                //Euros per defecte
                Session.Add("PriceSelector", (int)1);
                this.currentPriceSel = 1;
            }
            if (Session["Divisa"] == null)
            {
                //Session.Add("Divisa", "2");
                if (((currentUser is Client) && (currentUser.AccessType == "Basic")) && (currentUser.GdoPVPUser))
                    Session.Add("Divisa", "2");
                else
                {
                    //Euros per defecte
                    Session.Add("Divisa", "1");
                }
            }
            else
            {
                this.currentPriceSel = Int32.Parse((String)Session["Divisa"]);
            }
            if (Session["ArticleGdoDAO"] == null)
            {
                ArticleDataAccessObject ArticleGdoDAO;
                ArticleGdoDAO = SesionJulia.GetArticleGdo(currentUser.UserName, this.currentClient.Code, currentUser.Language, this.currentClient.TipoCambioDivisa);
                if (ArticleGdoDAO == null)
                    throw new Exception("Error NULL en el GetArticleGdo del cliente seleccionado");
                Session.Add("ArticleGdoDAO", ArticleGdoDAO);
            }
            if (Session["ArticlesGdo"] == null)
            {
                List<Article> articles = new List<Article>();
                ArticleDataAccessObject ArticleGdoDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                articles = ArticleGdoDAO.GetCatalogProducts();
                if (articles == null)
                    throw new Exception("Error NULL en la lista de articulos del cliente seleccionado");
                Session.Add("ArticlesGdo", articles);
            }

        }

        private void loaduserdata()
        {
            Sesion SesionJulia = new Sesion();

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];

            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
            }

            if (Session["Client"] == null)
            {
                if (this.currentUser is Agent)
                {
                    Agent agent = (Agent)this.currentUser;
                    //RTP: Carrer el primer client de la llista d'agents que és ell mateix
                    Client cli = SesionJulia.GetClient(agent.ListClients[0].Code, currentUser.UserName);
                    if (cli == null)
                        throw new Exception("Error NULL en el GetClient del cliente seleccionado");

                    //RTP: S'ha de posar el username del client amb el currentuser
                    cli.UserName = agent.UserName;
                    cli.Code = agent.Code;
                    cli.Language = agent.Language;
                    this.currentClient = cli;
                    Session["Client"] = cli;
                }
                else if (this.currentUser is Client)
                {
                    this.currentClient = (Client)this.currentUser;
                    Session["Client"] = currentUser;
                }
                else
                { //CLIENTGROUP
                    ClientGroup clientgroup = (ClientGroup)this.currentUser;
                    //RTP: Carrer el primer client de la llista d'agents que és ell mateix
                    Client cli = SesionJulia.GetClient(clientgroup.ListClients[0].Code, currentUser.UserName);
                    if (cli == null)
                        throw new Exception("Error NULL en el GetClient del cliente seleccionado");

                    //RTP: S'ha de posar el username del client amb el currentuser
                    cli.UserName = clientgroup.UserName;
                    cli.Code = clientgroup.Code;
                    cli.Language = clientgroup.Language;
                    this.currentClient = cli;
                    Session["Client"] = cli;
                }                
            }
            else
            {
                this.currentClient = (Client)Session["Client"];
            }

            Client cl = (Client)Session["Client"];

            if (Session["Divisa"] == null)
            {
                if (((currentUser is Client) && (currentUser.AccessType == "Basic")) && (currentUser.GdoPVPUser))
                {
                    this.currentPriceSel = 2;
                }
                else { 
                    //Euros per defecte
                    this.currentPriceSel = 1;
                }
            }
            else
            {
                this.currentPriceSel = Int32.Parse((string)Session["Divisa"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (!Page.IsPostBack)
                {
                    loaduserdata();

                    if (currentUser.Loaded && currentUser.FechaCaducidadPassword < DateTime.Today)
                    {
                        Response.Redirect("/_layouts/login/changepass.aspx?outdate=true&u=" + currentUser.UserName);
                    }
                    //lblUsername.Text = currentUser.UserName;
                    int currentLang = LoadLanguage(currentUser.Language);

                    if (currentUser.Loaded && currentLang != Thread.CurrentThread.CurrentUICulture.LCID)
                    {

                        Sesion SesionJulia = new Sesion();
                        Client cl = (Client)Session["Client"];
                        currentLang = Thread.CurrentThread.CurrentUICulture.LCID;
                        currentUser.Language = SetLanguage(Thread.CurrentThread.CurrentUICulture.LCID.ToString());
                        SesionJulia.UpdateClientLanguage(currentUser);
                        Session["User"] = currentUser;
                        // ---- Bloc per actualitzar la llista de productos amb l'idioma sel.leccionat
                        SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
                        ArticleDataAccessObject ArticleDAO;
                        ArticleDAO = SesionJulia.GetArticleGdo(currentUser.UserName, cl.Code, currentUser.Language, cl.TipoCambioDivisa);
                        //Session.Add("ArticleDAO", ArticleDAO);
                        Session.Add("ArticleGdoDAO", ArticleDAO);

                    }
                    //salvas a NAV
                    if (currentUser.Loaded == false)
                    {
                        currentUser.Loaded = true;
                        if (currentLang != Thread.CurrentThread.CurrentUICulture.LCID)
                        {
                            SetDisplayLanguage(currentLang);
                        }
                    }
                    LoadSessionVars();
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    if (this.currentUser is Client)
                    {
                        RadComboBox.Visible = false;
                        //lblClient.Visible = false;
                        this.LinkToManual.NavigateUrl = "/Documents/" + strCultureName + "/Manual.pdf";

                    }
                    else
                    {
                        //lblClient.Visible = true;
                        RadComboBox.Visible = true;
                        //lblClient.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Client_ClientListText");
                        this.LinkToManual.NavigateUrl = "/Documents/" + strCultureName + "/Agent_Manual.pdf";

                        //this.LoadDropList();
                        this.Loadcombolist();

                    }
                    //seleccioname el valor de divisa correcte
                    //this.Divisa.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaLbl");


                    if (((currentUser is Client) && (currentUser.AccessType == "Basic")) && (currentUser.GdoPVPUser))
                    {
                        this.DivisasCombo.Items.Add(new RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsSelectorGdo"), "2"));
                    }
                    else
                    {
                        this.DivisasCombo.Items.Add(new RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsSelectorGdo"), "0"));
                        this.DivisasCombo.Items.Add(new RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosSelectorGdo"), "1"));
                    }
                    this.DivisasCombo.SelectedValue = (string)Session["Divisa"];


                    //Populate language dropdown and select currentuser language
                    this.LanguageSelect.SelectedValue = currentLang.ToString();


                    string language = "<script type='text/javascript' src='/_layouts/ScriptResx.ashx?culture=" + strCultureName + "&name=JuliaGrupPortalClientesResources'></script>";
                    Page.ClientScript.RegisterStartupScript(GetType(), "Language", language);

                }
                else
                {
                    //Codigo para ver que control hace postback
                    //    if (IsPostBack)
                    //    {

                    //        string CtrlID = string.Empty;

                    //        if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"] != string.Empty)
                    //        {
                    //            CtrlID = Request.Form["__EVENTTARGET"];
                    //        }
                    //        else
                    //        {
                    //            //Buttons and ImageButtons
                    //            foreach (string ctrlid in Request.Form)
                    //            {
                    //                if (Request.Form[ctrlid] != null && Request.Form[ctrlid] != string.Empty)
                    //                {
                    //                    CtrlID = Request.Form[ctrlid];
                    //                }
                    //            }
                    //            Control foundControl;
                    //            foreach (string ctl in Request.Form)
                    //            {
                    //                // handle ImageButton they having an additional "quasi-property" 
                    //                // in their Id which identifies mouse x and y coordinates
                    //                if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    //                {
                    //                    CtrlID = ctl.Substring(0, ctl.Length - 2);
                    //                    foundControl = Page.FindControl(CtrlID);
                    //                }
                    //                else
                    //                {
                    //                    foundControl = Page.FindControl(ctl);
                    //                }

                    //                if (!(foundControl is Button || foundControl is ImageButton)) continue;

                    //                CtrlID = foundControl.ClientID;
                    //                break;
                    //            }


                    //        }

                    //        Page.ClientScript.RegisterStartupScript(this.GetType(),

                    //            "sourceofpostback",

                    //            "<script type='text/javascript'>" +

                    //            "window.onload=new function(){" +

                    //            "alert('Control ID " + CtrlID +

                    //" caused postback.');}" +

                    //            "</script>");

                    //    } 

                }

            }
            catch (Exception ex)
            {

                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }

        }
        private void SetDisplayLanguage(int lcid)
        {
            string script = "<script language='javascript'>OnSelectionChange('" + lcid + "')</script>";
            Page.ClientScript.RegisterStartupScript(GetType(), "Register", script);
        }
        private int LoadLanguage(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "CAT":
                    ret = "1027";
                    break;
                case "ESP":
                    ret = "3082";
                    break;
                case "FRA":
                    ret = "1036";
                    break;
                case "ITA":
                    ret = "1040";
                    break;
                case "ENU":
                    ret = "1033";
                    break;
                case "DEU":
                    ret = "1031";
                    break;
                default:
                    ret = "3082";
                    break;
            }
            return Convert.ToInt32(ret);
        }
        private string SetLanguage(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "1027":
                    ret = "CAT";
                    break;
                case "3082":
                    ret = "ESP";
                    break;
                case "1036":
                    ret = "FRA";
                    break;
                case "1040":
                    ret = "ITA";
                    break;
                case "1033":
                    ret = "ENU";
                    break;
                case "1031":
                    ret = "DEU";
                    break;
                default:
                    ret = "ESP";
                    break;
            }
            return ret;
        }
        private void Loadcombolist()
        {
            try
            {

                if (RadComboBox.DataSource == null)
                {
                    //this.RadComboBox.Items.Add(new RadComboBoxItem(((Client)Session["Client"]).Code + " - " + ((Client)Session["Client"]).Name,((Client)Session["Client"]).Code));

                    if (this.currentUser is Agent)
                    {
                        Agent agent = (Agent)this.currentUser;
                        this.RadComboBox.DataSource = agent.ListClientsName;
                    }
                    else if (this.currentUser is ClientGroup)//CLIENTGROUP
                    {
                        ClientGroup clientgroup = (ClientGroup)this.currentUser;
                        this.RadComboBox.DataSource = clientgroup.ListClientsName;
                    }
                    this.RadComboBox.SelectedValue = null;
                    this.RadComboBox.DataBind();
                    this.RadComboBox.SelectedValue = ((Client)Session["Client"]).Code;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void lnkbtnclose_click(object sender, EventArgs e)
        {
            Sesion SesionJulia = new Sesion();
            this.currentUser = (User)Session["User"];
            Client cl = (Client)Session["Client"];
            SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
            Session.Clear();
            Page.Response.Redirect("/_layouts/SignOut.aspx");
        }

        protected void DivisasCombo_IndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Session.Remove("Divisa");

            //Divisa siempre es un string??
            //Session.Add("Divisa", Convert.ToInt32(this.DivisasCombo.SelectedValue));
            Session.Add("Divisa", this.DivisasCombo.SelectedValue);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);

        }
    }
}