﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using Microsoft.SharePoint;
using System.Diagnostics;
using Telerik.Web.UI;

namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class ContenedorUserControl : UserControl
    {
        Order CurrentOrder;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CurrentOrder = (Order)Session["OrderGdo"];
                this.Contenedor_value.Text = CurrentOrder.GdoVolumenBox.ToString("F") + " %";

                //REZ 19082013 -- En lugar de con timer lo hacemos directamente para evitar el triple postback...

            }
            
            
        }
        public void SetContenedorValue(Decimal Value)
        {
            this.Contenedor_value.Text = Value.ToString("F") +" %";
        }
        public void Refresh()
        {
            try
            {
                this.Timer1.Enabled = true;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.Timer1.Enabled = false;
                CurrentOrder = (Order)Session["OrderGdo"];

                this.Contenedor_value.Text = CurrentOrder.GdoVolumenBox.ToString("F") + " %";

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void Panel_PreRender(object sender, EventArgs e)
        {
            try
            {
                RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);


                manager.AjaxSettings.AddAjaxSetting(Timer1, Contenedor_value, loadingPanel);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
    }
}
