﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageGalleryGdoUserControl.ascx.cs" Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.ImageGalleryGdoUserControl" %>


 <link rel="stylesheet" href="/Style%20Library/Julia/magiczoomplus.css" type="text/css"
    media="screen" />
 <script src="/Style%20Library/Julia/js/magiczoomplus.js" type="text/javascript"></script>

<style>

.image_carousel_pInfo
{
    width:84px;
    }
    
.image_carousel_pInfo img {
    border: 1px solid #ccc;
    background-color: white;
    display: block;
    float: left;
}

</style>

<script type="text/javascript" type="text/javascript">
    function initCarousel_productInfo() {
        jQuery(".image_carousel_pInfo").carouFredSel({
            width: "84px",
            height: "100%",
            /*responsive: true,*/
            direction: "up",
            circular: true,
            auto: false,
            align: "top",
            scroll: {
                items: 1
            },
            prev: {
                button: "#productInfo_prev",
                key: "left"
            },
            next: {
                button: "#productInfo_next",
                key: "right"
            }
        });
    }
    $(document).ready(function () {
        initCarousel_productInfo();
    });
</script>

<div id="product_images">
    <div class="image_carousel_pInfo">
            <div id="thumbs">
                <asp:Literal ID="listItems" runat="server"></asp:Literal>
            </div>
            <div class="clearfix">
            <a class="prev" id="productInfo_prev" href="#"></a>
            <a class="next" id="productInfo_next" href="#"></a>
            </div>
    </div>
</div>

<div class="ZoomImageContainer" style="width:70%;height:100%;display:table;margin:0 auto;">
    <a id="Zoomer" href="<%= this.ProductCode.Replace("·1", "·3").Replace("NODISPONIBLE_1", "NODISPONIBLE_3") %>" style="width:720px!important;height:720px!important"
        class="MagicZoomPlus" rel="zoom-position: inner; expand-position: center; expand-align: image;buttons: autohide; buttons-position: top right;pan-zoom: false" >
            <img alt="" src="<%= this.ProductCode.Replace("·1", "·2").Replace("NODISPONIBLE_1", "NODISPONIBLE_2") %>" width="720px" height="720px"/>
    </a>
<br />    
</div>

