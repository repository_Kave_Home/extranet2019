﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;



namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class ImageGalleryGdoUserControl : UserControl
    {
        public List<string> ImageUrls;
        public string ProductCode = string.Empty;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ImageUrls != null)
                {
                    string text = string.Empty;

                    foreach (string url in ImageUrls)
                    {
                        //string BigUrl = url.Replace("type=1", "type=3");
                        //string MidUrl = url.Replace("type=1", "type=2");
                        //REZ 19082013 - Cargamos directamente de intranet para evitar problemas de lentitud de carga
                        string encodedurl = url.Replace(" ", "%20");
                        string BigUrl = encodedurl.Replace("·1", "·3");
                        string MidUrl = encodedurl.Replace("·1", "·2");

                        text += "<div><a href='" + BigUrl + "' rel='zoom-id: Zoomer' rev='" + MidUrl + "'><img src='" + encodedurl + "' width='82px' height='82px'/></a></div>";
                        //text += "<img src=" + url + " width='100px' height='47px'/>";
                    }
                    this.listItems.Text = text;
                    //if (ImageUrls.Count <= 3)
                    //{
                    //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallHideArrowFunction", "HideArroy('Hide');", true);
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallHideArrowFunction", "HideArroy('Show');", true);
                    //}
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
