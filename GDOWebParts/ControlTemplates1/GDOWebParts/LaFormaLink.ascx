﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LaFormaLink.ascx.cs" Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.LaFormaLink" %>

<style>
    .gdolink 
    {
        float: right;
    }
    .static.menu-item.gdolink
    {
        background-color: #a12830!important;
    }
</style>
<asp:HyperLink ID="GDOButton" runat="server" Enabled="false">
    <div class="s4-tn gdolink">
        <div class="menu horizontal menu-horizontal">
            <ul class="root static">
                <li class="static">
                    <a class="static menu-item gdolink" href="/">
                        <span class="additional-background">
                            <span class="menu-item-text"><img height="15" style="top: 3px; position: relative;" src="/Style Library/Julia/img/GoTo.png">&nbsp;LaForma</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</asp:HyperLink>