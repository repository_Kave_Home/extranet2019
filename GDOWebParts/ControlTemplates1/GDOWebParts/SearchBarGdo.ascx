﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchBarGdo.ascx.cs"
    Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.SearchBarGdo" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<script type="text/javascript">
    function setSearchBar() {
        $('#search').on('keyup', function () {
            if (this.value.length >= 3) {
                JuliaClients.Service.searchProducts(this.value, $('#currentUserName').val(), $('#currentClientCode').val(), 1);
                $('#searchProducts').css("visibility", "visible");
                $('#searchProducts').css("display", "block");
            } else {
                $('#searchProducts').css("visibility", "hidden");
                $('#searchProducts').css("display", "none");
            }
        });
        $('#search').on('search', function () {
            $('#searchProducts').css("visibility", "hidden");
            $('#searchProducts').css("display", "none");
        });
        $("#search").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "/gdo/Pages/RedirectTo.aspx?r=products&t=" + this.value;
                event.preventDefault();
            }
        });

    }

    _spBodyOnLoadFunctionNames.push("setSearchBar");

    $(document).mouseup(function (e) {
        var search = $("#search");
        var container = $("#searchProducts");

        if (search.is(e.target) && search.val().length >= 3) {
            $('#searchProducts').css("visibility", "visible");
            $('#searchProducts').css("display", "block");
        }

        if (!container.is(e.target) && container.has(e.target).length === 0 && !search.is(e.target)) {
            $('#searchProducts').css("visibility", "hidden");
            $('#searchProducts').css("display", "none");
        }
    });
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerMainPedidoCart3" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="Timer1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="headerSearch" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<div id="headerSearch" class="headerSearch" runat="server">
    <!-- form id="search_mini_form" action="/Pages/RedirectTo.aspx?r=products&t=" method="get" -->
    <span class="fa fa-search fa-lg searchIcon"></span>
    <input id="search" type="search" placeholder="<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Search") %>..."
        name="q" value="" class="input-text required-entry" maxlength="128" placeholder=""
        autocomplete="off" />
    <!-- /form -->
</div>
<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="100" Enabled="false" />
