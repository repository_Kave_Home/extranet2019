﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using Microsoft.SharePoint;
using System.Diagnostics;

namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class SearchBarGdo : UserControl
    {
        Order CurrentOrder;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CurrentOrder = (Order)Session["OrderGdo"];
            if (this.CurrentOrder != null && this.CurrentOrder.ListOrderProducts.Count != 0)
            {
                this.headerSearch.Visible = false;
                this.headerSearch.Disabled = true;
            }
            else
            {
                this.headerSearch.Visible = true;
                this.headerSearch.Disabled = false;
            }
        }
        public void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.Timer1.Enabled = false;
                CurrentOrder = (Order)Session["OrderGdo"];

                if (this.CurrentOrder != null && this.CurrentOrder.ListOrderProducts.Count != 0)
                {
                    this.headerSearch.Visible = false;
                    this.headerSearch.Disabled = true;
                    Page.FindControl("JuliaTopMenu").Visible = false;
                }
                else
                {
                    this.headerSearch.Visible = true;
                    this.headerSearch.Disabled = false;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
