﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation"
    Assembly="Microsoft.SharePoint.Publishing, Version=12.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="julia" TagName="LaFormaLink" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/LaFormaLink.ascx" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenuGdo.ascx.cs" Inherits="GDOWebParts.ControlTemplates1.GDOWebParts.TopMenuGdo" %>

<%--Megamenu--%>
<div class="menu-container">
    <div class="menu">
        <ul>
            <asp:Repeater ID="JuliaTopMenu" runat="server" OnItemDataBound="JuliaTopMenu_ItemDataBound">
                <ItemTemplate>
                    <li class="megamenuCategoryStyle"><a href="/gdo/Pages/RedirectTo.aspx?r=products&c=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo%>">
                        <%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Descripcion%>
                    </a>
                        <ul>
                            <asp:Repeater ID="SubcategoriesMenuRepeater" runat="server" OnItemDataBound="JuliaSubTopMenu_ItemDataBound">
                                <HeaderTemplate>
                                    <li class="megamenuOnly">
                                        <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li id="<%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo.Trim()%>" class="megamenuCategoryStyle megamenuBorder"><a href="/gdo/Pages/RedirectTo.aspx?r=products&c=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).ParentCode%>&sc=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo%>">
                                        <%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Descripcion%></a>
                                        <ul id= "<%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo.Trim()%>" style="display:none;">

                                            <asp:Repeater ID="SubsubcategoriesMenuRepeater" runat="server">
                                                <HeaderTemplate>
                                                    <li>
                                                        <ul>
                                                    </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li class="megamenuCategoryStyle"><a href="/gdo/Pages/RedirectTo.aspx?r=products&c=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).ParentCode%>&sc=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo%>">
                                                        <%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Descripcion%></a>
                                                        </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul></li>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul></li></FooterTemplate>
                            </asp:Repeater>
                             <li id="subsubcategory" class="megamenuCategoryStyle">
                               
                                 <a style="border-bottom:none;"></a>
                            </li>
                            <asp:Repeater ID="EstilosMenuRepeater" runat="server">
                                <HeaderTemplate>
                                    <li><a style="text-transform: uppercase;">
                                        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Styles") %></a>
                                        <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><a href="/gdo/Pages/RedirectTo.aspx?r=products&c=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).ParentCode%>&es=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo%>">
                                        <%# ((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Descripcion%></a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul></li></FooterTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="EstanciasMenuRepeater" runat="server">
                                <HeaderTemplate>
                                    <li><a style="text-transform: uppercase;">
                                        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Estancias")%></a>
                                        <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><a href="/gdo/Pages/RedirectTo.aspx?r=products&c=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).ParentCode%>&e=<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo%>">
                                        <%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Descripcion%></a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul></li></FooterTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="CategoryFavoriteProduct" runat="server">
                                <HeaderTemplate>
                                    <div class="itemRightPanel">
                                        <p class="itemRightPanelSection">
                                            <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TopMenuHighligtproduct_title")%></p>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <img class="itemRightPanelImage" src="<%#Eval("ImageUrl")%>" id="TpMnFav_<%#Eval("Code")%>"
                                        onerror="<%#Eval("ImgErrorUrl")%>" />
                                    <p class="itemRightPanelTitle">
                                        <%#Eval("Description")%></p>
                                    <p class="itemRightPanelPrice" <%# String.IsNullOrEmpty(Eval("PriceWithDiscount").ToString())?"":"style='text-decoration:line-through;margin:0;color:#A12830;font-family:Brown Light;'" %>>
                                        <%#Eval("PriceString")%></p>
                                    <p class="itemRightPanelPrice">
                                        <%#Eval("PriceWithDiscount")%></p>
                                    <a href="<%#Eval("NavigateUrl")%>">
                                        <div class="mainButton">
                                            <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TopMenuHighligtproduct_linktext")%></div>
                                    </a>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <img src="/ImagenesTopMenu/<%#((JuliaGrupUtils.Business.TopMenuItem)Container.DataItem).Codigo.Trim()%>.png"
                                onerror="this.style='display:none;';" style="width: 75%; position: absolute;
                                bottom: 0; left: 0;" />
                            <li style="width: 75%; height: 150px;"></li>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:Repeater>   
            <%--Opciones de usuario--%>
            <li id="myaccountmenu" style="float: right; background: #f0f0f0; border-radius: 5px 5px 0 0;">
                <a id="myaccountmenuTitle" style="font-family: 'Brown Regular'">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("MyAccount")%></a>
                <a class="fa fa-user userStickyOff"></a>
                <ul class="userOptionsMenu">
                    <li>
                        <ul>
                            <asp:Repeater runat="server" ID="MenuRepeater" DataSourceID="GlobalNavDataSource">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <SeparatorTemplate>
                                </SeparatorTemplate>
                                <ItemTemplate>
                                    <li><a href="<%#Eval("Url")%>">
                                        <%#Eval("Title")%></a></li>
                                    <asp:Repeater runat="server" ID="ChildMenuRepeater" DataSource='<%#((SiteMapNode)Container.DataItem).ChildNodes%>'>
                                        <%-- <HeaderTemplate>
                                            <ul class="categoryitems">
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <li><a href="<%# Eval("Url")%>">
                                                <%#Eval("Title")%></a></li>
                                        </ItemTemplate>
                                        <%--<FooterTemplate>
                                            </ul></FooterTemplate>--%>
                                    </asp:Repeater>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                            <publishingnavigation:portalsitemapdatasource id="globalNavDS" sitemapprovider="CombinedNavSiteMapProvider"
                                showstartingnode="false" startfromcurrentnode="false" runat="server" />
                            <publishingnavigation:portalsitemapdatasource id="GlobalNavDataSource" runat="server"
                                sitemapprovider="CombinedNavSiteMapProvider" showstartingnode="false" startfromcurrentnode="true"
                                startingnodeoffset="0" trimnoncurrenttypes="Heading" treatstartingnodeascurrent="false" />
                            <julia:LaFormaLink ID="LaFormaLink" runat="server"></julia:LaFormaLink>
                            <li class="logoutOption">
                                <asp:LinkButton ID="lnkbtnclose" runat="server" CssClass="valign_middle" OnClick="lnkbtnclose_click"><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Client_SessionClose")%></asp:LinkButton>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <%--Carrito y buscador escondidos para el stiky menu--%>
            <li id="searchIconSticky" class="searchStickyOff"><a><i class="fa fa-search"></i></a>
            </li>
            <li class="cartStickyOff"><a href="/Pages/advancedOrder.aspx"><i class="fa fa-shopping-cart fa-lg">
            </i></a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    function setTopMenu() {
        $('.megamenuCategoryStyle:has(ul)').doubleTapToGo();
        $("#myaccountmenu:has(ul)").doubleTapToGo();
        init_megamenu();
    }
    _spBodyOnLoadFunctionNames.push("setTopMenu");            
</script>
