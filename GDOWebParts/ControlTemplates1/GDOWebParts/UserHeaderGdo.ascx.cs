﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SharePoint;
using System.Diagnostics;


namespace GDOWebParts.ControlTemplates1.GDOWebParts
{
    public partial class UserHeaderGdo : UserControl
    {
        private User currentUser;
        private Client currentClient;
        private ArticleDataAccessObject articleDAO;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Client"] != null)
                {
                    this.currentClient = (Client)Session["Client"];
                    getClientLogo();
                }
            }

        }
        private void getClientLogo()
        {

            string URLImage = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ClientLogo&ClientCode=" + currentClient.Code;
            /*if (string.IsNullOrEmpty(this.ClientLogo.ImageUrl))
            {
                this.ClientLogo.ImageUrl = URLImage;
            }
            this.ClientLogo.Attributes["onerror"] = "$(this).css('display', 'none');";*/
        }
    
    }
    
}
