﻿$(document).ready(function () {
    $('.Container').mouseenter(function (evento) {
        $(this).children('.Contenido').css('position', 'relative');
        $(this).children('.bloq_info_front').css('position', 'relative');
        $(this).children('.Contenido, .Contenido div').css('display', 'inline-block');
        $(this).children('.bloq_info_front').css('z-index', '99');
        $(this).children('.Contenido').css('z-index', '99');
        $(this).children('#imgProducto1').css('border-top', '1px solid rgb(224,224,224)');
        $(this).children('#imgProducto1').css('border-left', '1px solid rgb(224,224,224)');
        $(this).children('#imgProducto1').css('z-index', '99');
        $(this).children('.Contenido,.bloq_info_front').css('border-left', '1px solid rgb(224,224,224)');
        $(this).children('div').css('box-shadow', 'gray 0.7em 0.7em 0.7em ');
        $(this).children('.Contenido').css('box-shadow', 'gray 0.7em 0.7em 0.7em ');
    });
    $('.Container').mouseleave(function (evento) {
        $(this).children('.Contenido').css('position', 'static');
        $(this).children('.bloq_info_front').css('position', 'static');
        $(this).children('.Contenido, .Contenido div').css('display', 'none');
        $(this).children('.Contenido, #imgProducto1, .Container').css('box-shadow', 'none');
        $(this).children('.Contenido, #imgProducto1,.bloq_info_front').css('border', 'none');
        $(this).children('.bloq_info_front').css('z-index', '0');
        $(this).children('#imgProducto1').css('z-index', '0');
        $(this).children('.Contenido').css('z-index', '0');
        $(this).children('.Contenido').css('box-shadow', 'none');
        $(this).children('div').css('box-shadow', 'none');
    });
})