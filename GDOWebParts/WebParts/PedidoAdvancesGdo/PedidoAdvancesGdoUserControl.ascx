﻿<%@ Assembly Name="GDOWebParts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=2a2904cb1bfec2c2" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidoAdvancesGdoUserControl.ascx.cs"
    Inherits="GDOWebParts.WebParts.PedidoAdvancesGdo.aspx.PedidoAdvancesGdoUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<SharePoint:CssRegistration ID="CssRegistrationPedido" Name="/gdo/Style Library/Julia/PedidoAdvanced_Gdo.css"
    After="corev4.css" runat="server" />
<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<script type="text/javascript">
    function disableQtyEdit() {

        $(".UnitPerBoxEdit").attr("disabled", true);
    }
    function enableQtyEdit() {

        $(".UnitPerBoxEdit").attr("disabled", false);
    }
     

</script>
<style type="text/css">
    .ms-WPBody td
    {
        font-size: inherit;
    }
    .rgMasterTable td
    {
        white-space: nowrap !important;
    }
    
    .left-box-cart
    {
        display: none !important;
    }
    .headerSearch
    {
        right: 0;
    }
    #userselector
    {
        margin-right: 0;
    }
    #ctl00_ClientSelector_Panel1
    {
        right: 0;
    }
    .PrintView
    {
        float: right;
    }
</style>
<script type="text/javascript">
    function OpenFile(url) {
        $.blockUI({ message: 'En Proceso' });
        window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
        $.unblockUI({ message: 'En Proceso' });
    }
    function callConfirm(msg) {
        radconfirm(msg, confirmCallBackFn);
    }
    function confirmCallBackFn(arg) {
        var ajaxManager = $find("<%=RadAjaxManager1.ClientID%>");
        if (arg) {
            ajaxManager.ajaxRequest('OK');
        }
        else {
            ajaxManager.ajaxRequest('Cancelar');
        }
    }
    function alertRetorno(arg) {
        window.location = "/gdo/Pages/home.aspx";
    }

    $(document).ready(function () {
        $(".LblCantidad").keydown(function () {
            return false;
        });
    });
    function callConfirmDelOrder(msg) {
        radconfirm(msg, confirmCallBackFnOrder);
    }

    function confirmCallBackFnOrder(arg) {
        var ajaxManager = $find("<%=RadAjaxManager1.ClientID%>");
        if (arg) {
            ajaxManager.ajaxRequest('OKOrder');
        }
        else {
            ajaxManager.ajaxRequest('Cancelar');
        }
    }
    function Redirect() {
        window.location = "/gdo/Pages/advancedOrderGdo.aspx";
    }

</script>
<div id="container">
    <div class="title" id="titol">
        <asp:Label class="Pedido_ConfirmacionpedidoLbl" ID="Pedido_ConfirmacionpedidoLbl"
            runat="server"></asp:Label>:
        <asp:Label ID="PedidoNo" class="PedidoNo" runat="server"></asp:Label>
        <asp:ImageButton ID="PrintCurentView" OnClick="PrintPDF_Click" ImageUrl="/Style%20Library/Julia/img/imprimir.png"
            CssClass="PrintView" runat="server" onmouseover="this.src=this.src.replace('imprimir.','imprimir_on.');"
            onmouseout="this.src=this.src.replace('imprimir_on.','imprimir.');" />
    </div>
    <div class="ContainerTop" id="ContainerTop">
        <div id="ContainerLeft" class="ContainerLeft">
            <div id="numeroPedido" class="numeroPedido">
                <div class="numeroPedidolbl" id="numeroPedidolbl">
                    <asp:Label ID="Pedido_ClientLabel" runat="server"></asp:Label>
                </div>
                <div id="clientevalue">
                    <asp:Label ID="clienteValue" runat="server" Width="335px"></asp:Label>
                </div>
            </div>
            <div id="direccionentrega">
                <div id="entregalbl">
                    <asp:Label ID="Pedido_direccionentregaLbl" runat="server"></asp:Label>
                </div>
                <div id="entregavalue">
                    <telerik:RadComboBox ID="entregaValue" runat="server" Width="340px" MaxHeight="200px"
                        DataTextField="value" DataValueField="id" RenderMode="Lightweight" />
                </div>
            </div>
            <div id="ciudad">
                <div id="ciudadLbl">
                    <asp:Label ID="Pedido_CiudadLbl" runat="server"></asp:Label>
                </div>
                <div id="ciudadvalue">
                    <asp:Label ID="ciudadValue" runat="server" Width="335px"></asp:Label>
                </div>
            </div>
            <div id="pais">
                <div id="paisLbl">
                    <asp:Label ID="Pedido_PaisLbl" runat="server"></asp:Label>
                </div>
                <div id="paisvalue">
                    <asp:Label ID="paisValue" runat="server" Width="335px"></asp:Label>
                </div>
            </div>
            <div id="formadepago">
                <div id="formadepagoLbl">
                    <asp:Label ID="Pedido_formapagoLbl" runat="server"></asp:Label>
                </div>
                <div id="formadepagovalue">
                    <telerik:RadComboBox ID="formadepagoValue" runat="server" Enabled="false" Width="340px"
                        DataTextField="value" DataValueField="id" RenderMode="Lightweight" />
                </div>
            </div>
            <div id="referencia">
                <div id="referenciaLbl">
                    <asp:Label ID="Pedido_referenciaLbl" runat="server"></asp:Label>
                </div>
                <div id="referenciavalue">
                    <asp:TextBox ID="referenciaValue" runat="server" Width="335px" MaxLength="30" onkeydown="return (event.keyCode!=13);"></asp:TextBox></div>
            </div>
        </div>
        <div id="ContainerRight" class="ContainerRight">
            <div id="TipoContenedor">
                <div id="TipoContenedorLbl">
                    <asp:Label ID="Pedido_TipoContendorLbl" runat="server"></asp:Label></div>
                <div id="TipoContenedorValue">
                    <telerik:RadComboBox runat="server" Width="183px" DataTextField="UserDescription"
                        AutoPostBack="true" RenderMode="Lightweight" OnSelectedIndexChanged="Pedido_TipoContendorLbl_Change"
                        DataValueField="Code" ID="TipoContendor_Combo">
                    </telerik:RadComboBox>
                </div>
            </div>
            <div id="fechaRequerida">
                <div id="fechaRequeridalbl">
                    <asp:Label ID="Pedido_fechaRequeridalbl" runat="server"></asp:Label></div>
                <div id="fechaRequeridavalue">
                    <telerik:RadDatePicker runat="server" ID="FechaRequeridaValue" Culture="Spanish (Spain)"
                        Width="130px" onkeydown="return (event.keyCode!=13);">
                        <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                        </Calendar>
                    </telerik:RadDatePicker>
                </div>
            </div>
            <div id="observaciones">
                <div id="observacionesLbl">
                    <asp:Label ID="Pedido_observacionesLbl" runat="server"></asp:Label></div>
                <div id="observacionesvalue">
                    <telerik:RadTextBox ID="observacionesValue" runat="server" MaxLength="160" TextMode="MultiLine"
                        CssClass="observacionesValue" Width="200px">
                    </telerik:RadTextBox></div>
            </div>
        </div>
    </div>
    <div id="Header_Advanced">
        <div id="titol2">
            <asp:Label ID="Pedido_detallespedidoLbl" runat="server"></asp:Label>
        </div>
        <div class="Guardar_sin_enviar">
            <asp:ImageButton runat="server" ID="Button_save_without_send" OnClick="PlaceOrder"
                ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/guardar_NoSend.gif%>" onmouseover="this.src=this.src.replace('guardar_NoSend.','guardar_NoSend_on.');"
                onmouseout="this.src=this.src.replace('guardar_NoSend_on.','guardar_NoSend.');" />
        </div>
    </div>
    <telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="450px" Width="900px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None" Title="Unsaved changes pending">
        <ContentTemplate>
            <div style="padding: 20px">
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                <br></br>
                <asp:Label ID="lblConfirm" runat="server" Text=""></asp:Label>
                <br></br>
                <asp:Label ID="lblDesc1" runat="server" Text=""></asp:Label>
                <br></br>
                <asp:Label ID="lblDesc2" runat="server" Text=""></asp:Label>
                <br></br>
                <asp:Label ID="lblDesc3" runat="server" Text=""></asp:Label>
                <br />
                <div style="padding: 20px" align="right">
                    <asp:Button ID="btnOk" runat="server" Text="Ok" Width="50px" OnClick="btnOk_Click" />
                </div>
            </div>
        </ContentTemplate>
    </telerik:RadWindow>
    <div id="ContainerBottom">
        <asp:Panel ID="PedidoPanel" runat="server">
            <div class="tableOrderList">
                <telerik:RadGrid ID="RadGrid1" Width="100%" AllowSorting="False" OnNeedDataSource="RadGrid1_NeedDataSource"
                    HeaderStyle-CssClass="PedidoAdvancedHeaderClass" CssClass="Grid" AutoGenerateColumns="false"
                    PageSize="9999" AllowPaging="True" OnItemCommand="RadGrid1_ItemCommand" OnItemDeleted="RadGrid1_ItemDeleted"
                    OnItemInserted="RadGrid1_ItemInserted" OnItemUpdated="RadGrid1_ItemUpdated" OnDataBound="RadGrid1_DataBound"
                    EnableAjaxSkinRendering="true" AllowMultiRowSelection="True" runat="server" GridLines="None"
                    EnableLinqExpressions="false" OnItemDataBound="RadGrid1_ItemDataBound" AllowAutomaticUpdates="true"
                    ShowFooter="True" Style="width: 100%;" Skin="Metro">
                    <ClientSettings EnableRowHoverStyle="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <MasterTableView Width="100%" Summary="RadGrid table" UseAllDataFields="true">
                        <Columns>
                            <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                            <telerik:GridBoundColumn Visible="false" DataField="LineNo" HeaderText="LineNo" UniqueName="LineNo" />
                            <telerik:GridHTMLEditorColumn UniqueName="SmallImage" DataField="SmallImage" />
                            <telerik:GridBoundColumn DataField="ArticleCode" UniqueName="ArticleCode" />
                            <telerik:GridBoundColumn DataField="Description" UniqueName="Description" ReadOnly="true" />
                            <telerik:GridBoundColumn DataField="Num" HeaderText="Num." DataType="System.Int32"
                                UniqueName="Num">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn Aggregate="Sum" FooterText=" " DataField="Cubic" HeaderText="Cubic."
                                DataFormatString="{0:N3}" UniqueName="Cubic">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Weight" Aggregate="Sum" FooterText=" " HeaderText="Weight."
                                DataFormatString="{0:N1}" UniqueName="Weight">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Price" HeaderText="€" UniqueName="Price" ReadOnly="true">
                                <ItemStyle CssClass="ColumnPrice" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="TotalPrice" HeaderText="€" UniqueName="TotalPrice"
                                ReadOnly="true">
                                <ItemStyle CssClass="ColumnPrice" />
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn UniqueName="EditColumn" CommandName="Edit" ButtonType="ImageButton" />
                            <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ButtonType="ImageButton"
                                ConfirmDialogType="RadWindow">
                            </telerik:GridButtonColumn>
                        </Columns>
                        <EditFormSettings EditFormType="Template">
                            <FormTemplate>
                                <asp:Panel runat="server" ID="EditFormTemplate" CssClass="EditFormTemplate">
                                    <asp:Panel runat="server" ID="EditFormTemplateFieldsGdo" CssClass="EditFormTemplateFieldsGdo">
                                        <telerik:RadTextBox runat="server" ID="LineNoEdit" Visible="false" Text='<%# Bind("LineNo") %>'>
                                        </telerik:RadTextBox>
                                        <telerik:RadTextBox runat="server" ID="CodeEdit" Visible="false" Text='<%# Bind("ArticleCode") %>'>
                                        </telerik:RadTextBox>
                                        <asp:Label runat="server" ID="Edit_LblQUantity" CssClass="LblCantidadEditForm"></asp:Label>
                                        <telerik:RadNumericTextBox Height="22px" Visible="true" MaxLength="3" CssClass="UnitPerBoxEdit"
                                            LabelWidth="70px" ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                            IncrementSettings-InterceptMouseWheel="true" runat="server" ID="qtyEdit" Width="70px"
                                            MinValue="1" NumberFormat-DecimalDigits="0" DbValue='<%# Eval("Num") %>'>
                                            <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                        </telerik:RadNumericTextBox>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="EditFormTemplateButtons" CssClass="EditFormTemplateButtons">
                                        <div class="addItemButton">
                                            <asp:LinkButton OnClientClick="enableQtyEdit()" ID="LinkButton1" Width="11px" Height="11px"
                                                runat="server" CommandName='<%#((bool)DataBinder.Eval(Container, "OwnerTableView.IsItemInserted")) ? "SaveLine" : "UpdateLine" %>'
                                                CssClass="savebut"></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancel" Width="11px" Height="11px" runat="server" CausesValidation="False"
                                                CommandName="Cancel" CssClass="raditemButton cancelEditAdd"></asp:LinkButton>
                                            <asp:Label runat="server" ID="ErrorTxt" Visible="false"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </FormTemplate>
                        </EditFormSettings>
                        <EditItemStyle ForeColor="Gray"></EditItemStyle>
                    </MasterTableView>
                    <PagerStyle Mode="NextPrev" />
                </telerik:RadGrid>
                <div class="RadGrid RadGrid_Metro Grid" style="width: 100%;">
                    <table class="rgCommandRow" style="width: 100%;">
                        <tbody>
                            <tr class="rgCommandRow" style="border-top: none;">
                                <td>
                                    <table class="table_source" style="width: 100%;" cellpadding="0px" cellspacing="0px">
                                        <tr>
                                            <td>
                                                <asp:ImageButton runat="server" ID="SeguiComprando" OnClick="RedirectToHome" ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/seguir-comprando.png%>"
                                                    onmouseover="this.src=this.src.replace('seguir-comprando.','seguir-comprando_on.');"
                                                    onmouseout="this.src=this.src.replace('seguir-comprando_on.','seguir-comprando.');" />
                                                <asp:ImageButton runat="server" CssClass="BorrarCarrito" Width="26px" ID="Button_Delete_Order"
                                                    OnClick="DeleteOrder" onmouseover="this.src='/gdo/Style Library/Julia/img/Borrar_carritoHover.png'"
                                                    onmouseout="this.src='/gdo/Style Library/Julia/img/Borrar_carrito.png'" ImageUrl="/gdo/Style Library/Julia/img/Borrar_carrito.png" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="width: auto;">
                    <table class="rgCommandRow" style="float: right; width: 50%;">
                        <tbody>
                            <tr class="rgCommandRow" style="border-top: none; border">
                                <td>
                                    <table class="table_source" style="width: 100%;" cellpadding="0px" cellspacing="0px">
                                        <tr>
                                            <td class="caption_sourceTable">
                                                <div class="lblfinal_field3">
                                                    <asp:Label ID="LblTotal" runat="server"></asp:Label></div>
                                            </td>
                                            <td>
                                                <div class="lblfinal_field4">
                                                    <asp:Label ID="LblTotalValue" CssClass="TotalValue" runat="server" Text=""></asp:Label></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="ComercialConditions">
        <asp:Label runat="server" ID="ComercialConditions" CssClass="LblCondicionesComerciales"></asp:Label>
    </div>
    <asp:CheckBox CssClass="CheckCondiciones" ID="Aceptascondiciones" runat="server" />
</div>
<%-- <telerik:RadWindowManager ID="RadWindowManager1" Skin="Silk" RenderMode="Lightweight" runat="server" /> --%>
<div style="float: right" id="EnviarPedido">
    <asp:ImageButton ID="SendPedido" OnClick="SendPedido_Click" runat="server" ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/confirmar pre.gif%>"
        onmouseover="this.src=this.src.replace('confirmar pre.','confirmar pre_on.');" onmouseout="this.src=this.src.replace('confirmar pre_on.','confirmar pre.');" />
</div>
