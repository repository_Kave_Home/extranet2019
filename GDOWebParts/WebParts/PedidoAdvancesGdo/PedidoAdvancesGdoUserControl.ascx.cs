﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using Microsoft.SharePoint;
using System.Diagnostics;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.ErrorHandler;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using GDOWebParts.ControlTemplates1.GDOWebParts;
using System.Threading;

namespace GDOWebParts.WebParts.PedidoAdvancesGdo.aspx
{
    public partial class PedidoAdvancesGdoUserControl : UserControl
    {
        private Order CurrentOrder;
        private User currentUser;
        private Client CurrentClient;
        string imgURL = "";
        private static string QtyEditVar = string.Empty;

        public Article currentArticle;
        protected void Page_Load(object sender, EventArgs e)
        {

            EnsureChildControls();

        }
        protected override void CreateChildControls()
        {

            if (this.CurrentOrder == null)
                this.CurrentOrder = (Order)Session["OrderGdo"];
            if (!IsPostBack)
            {
                this.CurrentClient = this.CurrentOrder.OrderClient;
                this.Initialize();
            }
            else
            {
                if (!Page.IsCallback)
                {

                    RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                    ajaxManager.ResponseScripts.Add("disableQtyEdit();");

                }
            }
        }
        private void Initialize()
        {
            try
            {
                Sesion SesionJulia = new Sesion();
                CurrentOrder = (Order)Session["OrderGdo"];
                //load title
                this.referenciaValue.Text = this.CurrentOrder.ReferenciaPedido;
                //load forma de pago
                this.LoadPayMethodsAndTherms(CurrentOrder);
                //load direccion de entrega
                this.LoadAddresses(this.CurrentOrder);
                this.LoadTipoContenedor(this.CurrentOrder.GdoBox);
                //load tipo de entrega
                this.Pedido_ConfirmacionpedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ConfirmacionpedidoLbl");
                //load observaciones
                this.observacionesValue.Text = this.CurrentOrder.OrderObservations;
                //load client name
                this.clienteValue.Text = this.CurrentClient.Code + " - " + this.CurrentOrder.CustomerName;
                this.PedidoNo.Text = this.CurrentOrder.OrderNumber;
                this.FechaRequeridaValue.MinDate = System.DateTime.Now;
                if (CurrentOrder.FechaRequerida >= System.DateTime.Now)
                {
                    this.FechaRequeridaValue.SelectedDate = CurrentOrder.FechaRequerida;
                }




                this.TipoContendor_Combo.DataSource = CurrentOrder.ListaContenedores;
                this.TipoContendor_Combo.DataBind();
                this.TipoContendor_Combo.SelectedValue = CurrentOrder.GdoBox;
                //load all selected articles
                //this.LoadSelectedArticles();
                //RadComboBox combo = (RadComboBox)RadGrid1.FindControl("RadComboBox1");
                //ArticleDataAccessObject articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                //combo.DataSource = articleDAO.GetAllProducts();
                //combo.DataBind();

                this.Aceptascondiciones.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_AceptasCondiciones2");
                this.ComercialConditions.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ComercialConditions");
                this.Pedido_ClientLabel.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ClientLabel");
                this.Pedido_referenciaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_referenciaLbl");
                this.Pedido_formapagoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_formapagoLbl");
                this.Pedido_direccionentregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_direccionentregaLbl");
                this.Pedido_CiudadLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_City");
                this.Pedido_PaisLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_Pais");
                this.Pedido_observacionesLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_observacionesLbl");

                this.Pedido_TipoContendorLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_TipoContenedor");


                this.Pedido_fechaRequeridalbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_FechaRequerida");


                this.RadGrid1.Columns.FindByUniqueNameSafe("SmallImage").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SmallImageOrder");
                this.RadGrid1.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Num").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Cubic").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CubicOrder");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Weight").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("WeightOrder");
                string strDivisa = (string)Session["Divisa"];
                if (strDivisa == "0" || strDivisa == "2")   // Si es vista dolares o dolares PVP
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UnitariPriceOrderDollar");
                    this.RadGrid1.Columns.FindByUniqueNameSafe("TotalPrice").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TotalPriceOrderDollar");
                }
                else
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UnitariPriceOrder");
                    this.RadGrid1.Columns.FindByUniqueNameSafe("TotalPrice").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TotalPriceOrder");
                }




                //this.RadGrid1.Columns.FindByUniqueNameSafe("Discount").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Discount");
                ////this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price");
                //this.RadGrid1.Columns.FindByUniqueNameSafe("LineReference").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblLineDescription");
                //this.RadGrid1.Columns.FindByUniqueNameSafe("Volumen").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Volumen");
                //this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
                ((GridButtonColumn)RadGrid1.MasterTableView.GetColumnSafe("DeleteColumn")).ConfirmText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteProduct");

            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void LoadTipoContenedor(string Box)
        {
            try
            {
                OrderDataAccesObject orderDao = new OrderDataAccesObject();
                orderDao.GetTipoContenedores();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void LoadAddresses(Order order)
        {
            try
            {
                int i = 0;
                foreach (string[] addresses in this.CurrentClient.Addresses)
                {
                    RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);

                    if (this.CurrentOrder.DeliveryAddress == addresses[0] || (String.IsNullOrEmpty(this.CurrentOrder.DeliveryAddress) && i == 0))
                    {
                        aux.Selected = true;
                        this.ciudadValue.Text = addresses[2];
                        this.paisValue.Text = addresses[3];
                    }

                    this.entregaValue.Items.Add(aux);

                    i++;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void LoadPayMethodsAndTherms(Order order)
        {
            try
            {
                if (Session["User"] != null)
                {
                    currentUser = (User)Session["User"];
                }

                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                string PaymentMathodTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "PaymentMethod");
                string ThermsPaymentTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "ThermsPayment");
                formadepagoValue.Items.Add(new RadComboBoxItem(PaymentMathodTxt, order.CodFormaPago));


            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void PrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);



            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                string doc = string.Empty;
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];

                }
                if (Session["Client"] != null)
                {
                    this.CurrentClient = (Client)Session["Client"];

                }



                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ofertapdf&ClientCode=" + currentUser.UserName + "&ofertano=" + CurrentOrder.NavOferta.No + "&selector=" + Session["PriceSelector"].ToString() + "&TipoCompra=1";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


            return output;
        }
        public void SendPedido_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Aceptascondiciones.Checked)
                {
                    OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                    this.CurrentOrder.ReferenciaPedido = this.referenciaValue.Text;

                    if (!String.IsNullOrEmpty(this.formadepagoValue.SelectedValue))
                    {
                        this.CurrentOrder.PaymentMethod = this.formadepagoValue.SelectedValue;
                    }
                    if (!String.IsNullOrEmpty(this.entregaValue.SelectedValue))
                    {
                        this.CurrentOrder.DeliveryAddress = this.entregaValue.SelectedValue;
                    }


                    this.CurrentOrder.OrderObservations = this.observacionesValue.Text;

                    if (FechaRequeridaValue.IsEmpty)
                    {
                        CurrentOrder.FechaRequerida = new DateTime();
                    }
                    else
                    {
                        CurrentOrder.FechaRequerida = (DateTime)FechaRequeridaValue.SelectedDate;
                    }

                    //get Condiciones de contenedor superado

                    Sesion SesionJulia = new Sesion();
                    currentUser = (User)Session["User"];
                    Client cl = (Client)Session["Client"];


                    string checkContenedor = orderDAO.CheckTotalVolumenGDO(currentUser.UserName, CurrentOrder.OrderNumber, cl.Code);
                    if (checkContenedor != "")
                    {

                        string radalertscript = "<script language='javascript'>function f(){callConfirm('" + checkContenedor + "'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);


                        //RadWindowManager1.RadConfirm("Server radconfirm: Are you sure?", "confirmCallBackFn", 330, 180, null, "Server RadConfirm");

                    }
                    else
                    {


                        bool result = orderDAO.SendOrderToNav(this.CurrentOrder, cl.UserName, 1);
                        if (result)
                        {
                            //RTP


                            ////netegem les orders en memoria

                            Session.Remove("OrderDAO");
                            //netegem la llista de pedidos oberts
                            Session.Remove("VentasDAO");

                            //Obtenemos una Order Nueva
                            SesionJulia = new Sesion();
                            currentUser = (User)Session["User"];
                            cl = (Client)Session["Client"];
                            Order Order;
                            Order = SesionJulia.GetOrderGdo(cl);
                            Session.Add("OrderGdo", Order);


                            //Mostramos el mensaje de confirmación de envio de pedido
                            radwindowPopup.Title = CurrentOrder.OrderClient.Name;
                            radwindowPopup.VisibleOnPageLoad = true;

                            lblMessage.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation");
                            lblConfirm.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck");
                            lblDesc1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView");
                            lblDesc2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1") + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2");


                        }
                        else
                        {
                            radwindowPopup.VisibleOnPageLoad = true;
                            lblMessage.Text = "Order Error";

                        }
                    }

                }
                else
                {
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AceptarCondicionesError") + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                }



            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,''); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            try
            {
                Order Order;
                Sesion SesionJulia = new Sesion();
                currentUser = (User)Session["User"];
                Client cl = (Client)Session["Client"];

                Order = SesionJulia.GetOrderGdo(cl);
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                if (e.Argument.ToString() == "OK")
                {
                    bool result = false;

                    result = orderDAO.SendOrderToNav(this.CurrentOrder, cl.UserName, 1);

                    if (result)
                    {

                        Session.Remove("OrderGdoDAO");
                        //netegem la llista de pedidos oberts


                        //Obtenemos una Order Nueva
                        SesionJulia = new Sesion();
                        currentUser = (User)Session["User"];
                        cl = (Client)Session["Client"];
                        Order = SesionJulia.GetOrderGdo(cl);
                        Session.Add("OrderGdo", Order);


                        radwindowPopup.Title = CurrentOrder.OrderClient.Name;
                        radwindowPopup.VisibleOnPageLoad = true;

                        lblMessage.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation");
                        lblConfirm.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck");
                        lblDesc1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView");
                        lblDesc2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1") + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2");
                        string messageToShow = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation") + " <br><br> " +
                            JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck") + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView") + " <br><br> " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1")
                             + " <br><br> " + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2") + " <br><br> ";


                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "radalert('" + messageToShow + "', 700, 250,'" + radwindowPopup.Title + "',alertRetorno);", true);
                    }
                }
                else if (e.Argument.ToString() == "OKOrder")
                {
                    try
                    {
                        CurrentOrder = (Order)Session["OrderGdo"];
                        if (CurrentOrder != null)
                        {
                            this.currentUser = (User)Session["User"];
                            this.CurrentOrder.DeleteAllProducts(currentUser.UserName);
                            Session["GrupoProveedoresFilter"] = string.Empty;
                            Session["ProveedorFilter"] = string.Empty;
                            Session["OrderGdo"] = this.CurrentOrder;
                            LoadSelectedArticles();
                            RadGrid1.DataBind();
                            //Refrescamos el carrito
                            ContenedorUserControl contenedor = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                            contenedor.Refresh();
                        }
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "Redirect();", true);
                    }
                    catch (NavException ex)
                    {
                        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

                    }
                    catch (Exception ex)
                    {
                        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
                    }
                }
                else
                {
                    //Si cancela, No hacemos nada
                }
            }
            catch (NavException ex)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "radalert('" + ex.Message + "', 330, 100);", true);
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("/gdo/Pages/Home.aspx");
        }
        //RadGrid Functions
        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadSelectedArticles();
            //this.RadGrid1.DataBind();
        }
        private void LoadSelectedArticles()
        {
            if (Session["User"] != null)
            {
                currentUser = (User)Session["User"];
            }

            try
            {
                DataTable data = new DataTable();
                data.Columns.Add("LineNo");
                data.Columns.Add("SmallImage");
                data.Columns.Add("ArticleCode");
                data.Columns.Add("Description");
                data.Columns.Add("Num", typeof(Int32));
                data.Columns.Add("Cubic", typeof(Decimal));
                data.Columns.Add("Weight", typeof(Decimal));
                data.Columns.Add("Price");
                data.Columns.Add("Type");
                data.Columns.Add("TotalPrice");
                data.Columns.Add("GrupProveidor");

                if (this.CurrentOrder != null)
                {
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    foreach (OrderLine ol in this.CurrentOrder.ListOrderProducts)
                    {
                        DataRow row = data.NewRow();
                        row["LineNo"] = ol.LineNo;
                        row["SmallImage"] = "<a href='" + "/Gdo/Pages/productGdoInfo.aspx?code=" + ol.Product.Code + "&catalog=" + ol.Product.InsertcatalogNo + "&version=" + ol.Product.ArticleVersion + "'><img src='" + ol.Product.SmallImgUrl + "' alt='' width='50px' height='50px' onerror='this.src=\"" + "/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg\"" + "' /></a>";//falta dada
                        row["ArticleCode"] = ol.Product.Code;
                        row["Description"] = ol.Product.Description;
                        row["Num"] = ol.Quantity;
                        row["Cubic"] = ol.Volumen;
                        row["Type"] = ol.Type;
                        row["Weight"] = ol.PesoNeto;
                        string strDivisa = (string)Session["Divisa"];
                        switch (strDivisa)
                        {
                            case "0":   //Dolares
                                row["Price"] = ol.UnitPrice.ToString("N2", new CultureInfo("es-Es", false));
                                row["TotalPrice"] = ol.Price.ToString("N2", new CultureInfo("es-Es", false));
                                break;
                            case "2":   //DolaresPVP
                                row["Price"] = (ol.UnitPrice * currentUser.CoeficientGDOPVP).ToString("N2", new CultureInfo("es-Es", false));
                                row["TotalPrice"] = (ol.Price * Convert.ToDouble(currentUser.CoeficientGDOPVP)).ToString("N2", new CultureInfo("es-Es", false));
                                break;

                            default:   //Euros
                                row["Price"] = (ol.UnitPrice * currentUser.DolarCoef).ToString("N2", new CultureInfo("es-Es", false));
                                row["TotalPrice"] = (ol.Price * Convert.ToDouble(currentUser.DolarCoef)).ToString("N2", new CultureInfo("es-Es", false));
                                break;

                        }

                        row["GrupProveidor"] = ol.Product.GDOTipoUnion;

                        data.Rows.Add(row);
                    }

                }

                this.RadGrid1.DataSource = data;

            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.EditCommandName)
                {
                    RadGrid1.MasterTableView.IsItemInserted = false;


                    //RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                    //ajaxManager.ResponseScripts.Add("disableQtyEdit();");
                }
                if (e.CommandName == RadGrid.InitInsertCommandName)
                {
                    RadGrid1.MasterTableView.ClearEditItems();
                }
                ArticleDataAccessObject articleDAO = null;
                Article articleSelected = null;
                CurrentOrder = (Order)Session["OrderGdo"];
                CurrentClient = (Client)Session["Client"];
                string code = string.Empty;
                string catalog = string.Empty;
                string version = string.Empty;
                switch (e.CommandName)
                {
                    case ("LoadArticle"):

                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        code = ((RadTextBox)e.Item.FindControl("codiArticleForm")).Text;
                        version = ((RadTextBox)e.Item.FindControl("VersionArticleForm")).Text;
                        catalog = ((RadTextBox)e.Item.FindControl("CatalogArticleForm")).Text;
                        //articleSelected = articleDAO.GetProductByCode(code);
                        articleSelected = articleDAO.GetProductByCodeAndCatalog(code, version, catalog);
                        if (articleSelected != null)
                        {
                            ((Label)e.Item.FindControl("productDescriptionForm")).Text = articleSelected.Description;
                        }
                        break;
                    case ("SaveLine"):

                        if ((Label)e.Item.FindControl("ErrorTxt") != null)
                        {
                            ((Label)e.Item.FindControl("ErrorTxt")).Visible = false;
                        }


                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        RadComboBox combo = ((RadComboBox)e.Item.FindControl("RadComboBox1"));
                        string[] value = combo.SelectedValue.Split(';');

                        if (value.Length > 2)
                        {
                            code = value[0];
                            version = value[1];
                            catalog = value[2];
                        }
                        articleSelected = articleDAO.GetProductByCodeAndCatalog(code, version, catalog);
                        if (articleSelected != null)
                        {
                            string lineDescription = ((RadTextBox)e.Item.FindControl("lineReferenceForm")).Text;

                            //int numItems = (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value;

                            string Qtyvalue = ((RadNumericTextBox)e.Item.FindControl("numForm")).Value.ToString();
                            if (string.IsNullOrEmpty(Qtyvalue))
                            {
                                throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoArticle"));
                            }
                            else
                            {
                                int numItems = Convert.ToInt32(Qtyvalue);
                                this.CurrentOrder.AddProduct(articleSelected, numItems, lineDescription, CurrentClient.UserName);
                            }
                            //int numItems = string.IsNullOrEmpty((((RadNumericTextBox)e.Item.FindControl("numForm")).Value).ToString()) ? (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value : 1;

                        }
                        else
                        {

                            throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ArticleInvalid"));
                        }

                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;


                    case ("Cancel"):
                        break;
                    case ("Delete"):


                        //GridDataItem item = (GridDataItem)e.Item;
                        //string LineNo = item["LineNo"].Text;
                        //this.CurrentOrder.DeleteProductByLineNo(int.Parse(LineNo), CurrentClient.UserName);

                        this.CurrentOrder.DeleteProduct(e.Item.DataSetIndex, CurrentClient.UserName);   //REZ10042016 - Lo de arriba peta si no mostramos la columna LineNo

                        ContenedorUserControl contenedor = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                        contenedor.Refresh();
                        //this.CurrentOrder.DeleteProduct(e.Item.DataSetIndex, CurrentClient.UserName);

                        //esborrem variables de sessio si no hi ha articles  a la order.

                        if (CurrentOrder.ListOrderProducts.Count == 0)
                        {
                            Session["GrupoProveedoresFilter"] = string.Empty;
                            Session["ProveedorFilter"] = string.Empty;
                        }
                        RadGrid1.Rebind();
                        break;
                    case ("UpdateLine"):
                        RadNumericTextBox Qty = (RadNumericTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("qtyEdit");
                        Qty.Attributes.Remove("disabled");
                        articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                        RadTextBox CodeEdit = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("CodeEdit");

                        code = CodeEdit.Text;

                        articleSelected = articleDAO.GetProductByCode(code);
                        if (articleSelected != null)
                        {

                            Qty.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Numbers");

                            string Qtyvalue = Qty.Value.ToString();
                            if (string.IsNullOrEmpty(Qtyvalue))
                            {
                                string radalertscript = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("QtyErrorMsg") + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                                break;
                            }
                            else
                            {
                                int ID = Convert.ToInt32(((RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("LineNoEdit")).Text);
                                int numItems = Convert.ToInt32(Qtyvalue);
                                //this.CurrentOrder.SetProduct(articleSelected, numItems,"", CurrentClient.UserName, ID);
                                this.CurrentOrder.SetProductByLineNo(ID, int.Parse(Qtyvalue), "", CurrentClient.UserName);
                                ContenedorUserControl contenedorr = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                                contenedorr.Refresh();
                            }

                        }
                        else
                        {
                            if ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt") != null)
                            {
                                ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt")).Text = "Article not found";
                                ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt")).Visible = true;
                            }
                        }

                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                    case (RadGrid.EditCommandName):


                        break;



                    default:
                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                }

            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public Control FindControlRecursive(Control Root, string Id)
        {
            try
            {
                if (Root.ID == Id)
                    return Root;
                foreach (Control Ctl in Root.Controls)
                {
                    Control FoundCtl = FindControlRecursive(Ctl, Id);
                    if (FoundCtl != null)
                        return FoundCtl;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return null;
        }
        protected void RadComboBox1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }
        private void QtyTxtbox_SetValues(Article article, GridEditableItem editedItem)
        {
            try
            {
                RadNumericTextBox QtyTxtbox = (RadNumericTextBox)editedItem.FindControl("numForm");
                QtyTxtbox.MinValue = 1;
                QtyTxtbox.IncrementSettings.Step = Convert.ToInt32(article.UnidadMedida);
                QtyTxtbox.Value = Convert.ToInt32(article.UnidadMedida);
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                e.KeepInInsertMode = false;
            }
        }
        public void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        {
            try
            {

                //int i = e.Item.DataSetIndex;
                //this.CurrentOrder.DeleteProduct(i, CurrentClient.UserName);

                GridDataItem item = (GridDataItem)e.Item;
                string LineNo = item["LineNo"].Text;
                this.CurrentOrder.DeleteProductByLineNo(int.Parse(LineNo), CurrentClient.UserName);

                Session["OrderGdo"] = this.CurrentOrder;

                //this.LoadSelectedArticles();
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadGrid1_ItemUpdated(object source, GridUpdatedEventArgs e)
        {
        }
        protected void DeleteOrder(object sender, EventArgs e)
        {
            string radalertscript = "<script language='javascript'>function f(){callConfirmDelOrder('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("callConfirmDelOrder") + "'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "radalertDelOrder", radalertscript);


        }
        protected void PlaceOrder(object sender, EventArgs e)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                this.CurrentOrder.ReferenciaPedido = this.referenciaValue.Text;

                if (!String.IsNullOrEmpty(this.formadepagoValue.SelectedValue))
                {
                    this.CurrentOrder.PaymentMethod = this.formadepagoValue.SelectedValue;
                }
                if (!String.IsNullOrEmpty(this.entregaValue.SelectedValue))
                {
                    this.CurrentOrder.DeliveryAddress = this.entregaValue.SelectedValue;
                }


                this.CurrentOrder.OrderObservations = this.observacionesValue.Text;

                if (FechaRequeridaValue.IsEmpty)
                {
                    CurrentOrder.FechaRequerida = new DateTime();
                }
                else
                {
                    CurrentOrder.FechaRequerida = (DateTime)FechaRequeridaValue.SelectedDate;
                }
                if (!string.IsNullOrEmpty(this.TipoContendor_Combo.SelectedValue))
                {
                    CurrentOrder.GdoBox = this.TipoContendor_Combo.SelectedValue;
                }


                Sesion SesionJulia = new Sesion();
                Client CurrentClient = (Client)Session["Client"];
                Order Order;
                Order = SesionJulia.GetOrderGdo(CurrentClient);
                Session.Add("OrderGdo", Order);
                Session["OrderGdo"] = this.CurrentOrder;

                this.CurrentOrder = (Order)Session["OrderGdo"];
                if (CurrentOrder != null)
                {
                    this.CurrentOrder.Update(CurrentClient.UserName);
                }
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void RedirectToHome(object sender, EventArgs e)
        {
            //Response.Redirect("/gdo/Pages/Home.aspx");
            Response.Redirect("/gdo/Pages/RedirectTo.aspx?r=back");
        }
        public List<Article> GetAllArticles()
        {
            try
            {
                ArticleDataAccessObject articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                return articleDAO.GetAllProducts();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            try
            {


                if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                {
                    GridEditableItem item = (GridEditableItem)e.Item;
                    Label label = (Label)item.FindControl("Edit_LblQUantity");
                    label.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");

                }
                if (!(e.Item is GridEditFormInsertItem) && e.Item.IsInEditMode)
                {

                    GridEditFormItem Form = (e.Item as GridEditFormItem);

                    RadTextBox codiArticle = (RadTextBox)Form.FindControl("CodeEdit");
                    codiArticle.Enabled = false;

                    ArticleDataAccessObject articleDAO = null;
                    if (Session["ArticleGdoDAO"] != null)
                    {
                        articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                    }
                    else
                    {
                        throw new SPException("Error: ArticleGdoDAO session var is Null");
                    }
                    this.currentArticle = articleDAO.GetProductByCode(codiArticle.Text);
                    RadNumericTextBox QtyTxtbox = (RadNumericTextBox)Form.FindControl("qtyEdit");
                    QtyTxtbox.MinValue = Convert.ToInt32(this.currentArticle.GDOMinQty);
                    QtyTxtbox.MaxLength = 3;
                    QtyTxtbox.IncrementSettings.Step = Convert.ToInt32(this.currentArticle.UnidadMedida);
                    //QtyTxtbox.Value = Convert.ToInt32(this.currentArticle.GDOMinQty);
                }
                if (e.Item is GridEditableItem && e.Item.IsDataBound && !(e.Item is GridEditFormInsertItem))
                {
                    if (((DataRowView)e.Item.DataItem)["Type"].ToString() != "Item")
                    {
                        GridDataItem item = e.Item as GridDataItem;
                        if (item != null)
                        {
                            ImageButton ImgBtnEdit = item["EditColumn"].Controls[0] as ImageButton;
                            ImgBtnEdit.Visible = false;

                            ImageButton ImgBtnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                            ImgBtnDelete.Visible = false;

                            GridTableCell image = item["SmallImage"] as GridTableCell;
                            image.Text = "";

                        }
                    }
                }
                if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                {

                    GridEditableItem item = (GridEditableItem)e.Item;
                    LinkButton Linkbut = (LinkButton)item.FindControl("LinkButton1");
                }
                if (e.Item is GridDataItem)
                {
                    foreach (GridColumn col in RadGrid1.MasterTableView.Columns)
                    {
                        if (col.UniqueName == "DeleteColumn")
                        {
                            (col as GridButtonColumn).ConfirmText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ConfirmDeleteMsg");
                        }
                    }
                }
            }

            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            try
            {
                //---------------Actualitzo la Order------------------
                Sesion SesionJulia = new Sesion();
                //Carrego Order
                Order p;
                p = SesionJulia.GetOrderGdo(CurrentOrder.OrderClient);
                Session.Add("OrderGdo", p);
                this.CurrentOrder = (Order)Session["OrderGdo"];
                //----------------------------------------------------


                this.LblTotal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Total_Import");

                string strDivisa = Session["Divisa"].ToString();
                switch (strDivisa)
                {
                    case "0":   //Dolares
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? (p.NavOferta.TotalImportesConIVA).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : "-";
                        break;
                    case "2":   //DolaresPVP
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? (p.NavOferta.TotalImportesConIVA * currentUser.CoeficientGDOPVP).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : "-";
                        break;

                    default:   //Euros
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? (p.NavOferta.TotalImportesConIVA * currentUser.DolarCoef).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") : "-";
                        break;

                }



            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void Pedido_TipoContendorLbl_Change(object sender, EventArgs e)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                List<TipoContenedor> lista = orderDAO.GetTipoContenedores();

                string code = this.TipoContendor_Combo.SelectedValue;
                string message = !String.IsNullOrEmpty((lista.Find(p => p.Code == code).MessageContenedor)) ? lista.Find(p => p.Code == code).MessageContenedor : "";
                if (!string.IsNullOrEmpty(message))
                {
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + message + "', 460, 150,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                }

                PlaceOrder(sender, e);
                //Refrescamos el carrito
                ContenedorUserControl contenedor = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                contenedor.Refresh();
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
    }
}
