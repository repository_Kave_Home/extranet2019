﻿<%@ Assembly Name="GDOWebParts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=2a2904cb1bfec2c2" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductGdoInfoUserControl.ascx.cs"
    Inherits="GDOWebParts.WebParts.ProductGdoInfo.aspx.ProductGdoInfoUserControl" %>
<%@ Register TagPrefix="julia" TagName="ImageGalleryGdoUserControl" Src="~/_CONTROLTEMPLATES/GDOWebParts/ImageGalleryGdoUserControl.ascx" %>
<%@ Register TagPrefix="julia" TagName="RecentlyViewedGdoUserControl" Src="~/_CONTROLTEMPLATES/GDOWebParts/RecentlyViewedGdo.ascx" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<SharePoint:CssRegistration ID="CssRegistrationPedido" Name="/gdo/Style Library/Julia/ProductInfo_Gdo.css"
    runat="server" />
<style type="text/css">
    .tabs a
    {
        border-left: none !important;
    }
    .ValuestableMeasuresTr td
    {
        padding: 0px 5px;
        white-space: nowrap;
    }
</style>
<script type="text/javascript">
<% if (this.currentArticle != null) { %>
    $(document).ready(function () {
        //modify the href of download Image
        $("#DownloadImage").click(function () {
            $("#DownloadImage").attr("href", $("#Zoomer").attr("href"));
        });

        $(".tab_content").hide(); //Hide all content
        if ($("#tabs").children().length == 1) {
            $("#tabs li:first").attr("class", "active");
            $("#tabs li:first").show();
            $(".tab_container div:first").show();
        } else {
            //            $(".SecondTab").addClass("active");
            //            $(".SecondTab li").show();
            //            $(".tab_container div:first + div").show();
            //Mostramos por defecto el tercer tab
            $(".ThirdTab").addClass("active");
            $(".ThirdTab li").show();
            $(".tab_container div:first + div + div").show();
        }
        //modify the href of download Image
        $("ul.tabs li").click(function () {
            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active content
            $("html body").animate({
                scrollTop: $(document).height()
            }, "slow");
        });
    });
    function GoBack() {
        window.location.href = "/gdo/Pages/RedirectTo.aspx?r=back&artId=<%= this.currentArticle.Code %>";
    }

    function disableQtyEdit() {

        $(".UnitPerBoxEdit").attr("disabled", true);
    }
    function enableQtyEdit() {

        $(".UnitPerBoxEdit").attr("disabled", false);
    }
    function HideSearchBoxTopMenu() {
        $("#searchBox").hide();
        $(".megamenuCategoryStyle").hide();
    }
<% } %>
</script>
<asp:Panel ID="ProductInfoPanel" runat="server">
    <div>
        <div>
            <img alt="" class="BackBtn" onclick="GoBack()" height="20px" src="/gdo/Style Library/Julia/img/Back_ProductInfo.png" />
            <asp:Panel ID="TIttleDownload" CssClass="CaptionTitle" runat="server">
            </asp:Panel>
        </div>
        <div class="productDetailZone">
            <div class="productLeftZone">
                <asp:Panel runat="server" ID="ProductImage" />
                <julia:ImageGalleryGdoUserControl ID="imageGallery" runat="server"></julia:ImageGalleryGdoUserControl>
            </div>
            <div class="productRightZone">
                <asp:Panel ID="ProductTitle" runat="server" CssClass="productInfoBlock">
                    <asp:Panel ID="ImgNewProd" runat="server">
                        <span>
                            <img src="/gdo/Style Library/Julia/img/new_ficha.png" /></span>

                    </asp:Panel>
                    <asp:Panel ID="ImgComingSoon" runat="server">
                        <span>
                            <img src="/Style Library/Julia/img/comingsoon_ficha.png" /></span>

                    </asp:Panel>
                    <asp:Label runat="server" CssClass="productInfoTitle" ID="CaptionTitle"></asp:Label>
                </asp:Panel>
                <br />
                <br />
                <div class="productInfoBlock">
                    <asp:Label runat="server" ID="CaptionMeasures" CssClass="productInfoLabelText"></asp:Label>
                    <table cellspacing="0" class="tableMeasures" style="width: 100%; max-width: 200px;
                        margin: 0;">
                        <tr>
                            <td>
                                <img alt="" height="16px" src="/Style Library/Julia/img/Height.png" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="ProductHeight" CssClass="productInfoValueText"></asp:Label>
                                <td>
                                </td>
                                <td>
                                    <img alt="" height="16px" src="/Style Library/Julia/img/Width.png" />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="ProductWidth" CssClass="productInfoValueText"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <img alt="" height="16px" src="/Style Library/Julia/img/Depth.png" />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="ProductDepth" CssClass="productInfoValueText"></asp:Label>
                                </td>
                        </tr>
                    </table>
                    <br />
                    <table cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="CaptionCode" CssClass="productInfoLabelText"></asp:Label>
                                <asp:Label runat="server" ID="ValueCode" CssClass="productInfoValueText" Style="background-color: #eee;
                                    border-radius: 15px; padding: 3px 10px;"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="horizontalSeparator">
                </div>
                <div class="productInfoBlock">
                    <asp:Label runat="server" ID="CaptionPrice" CssClass="productInfoLabelText"></asp:Label>
                    <asp:Label runat="server" ID="ValuePrice" CssClass="productInfoPriceText"></asp:Label>
                </div>
                <div class="productInfoBlock">
                    <div style="float: left; margin-right: 20px; padding-top: 5px;">
                        <telerik:RadNumericTextBox ShowSpinButtons="true" LabelCssClass="field_info_lbl LblCantidad"
                            IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true"
                            runat="server" MaxLength="5" ID="itemQty" MaxValue="99999" CssClass="UnitPerBoxEdit"
                            NumberFormat-DecimalDigits="0" Value="1" Skin="Metro" />
                    </div>
                    <div>
                        <asp:ImageButton ID="ImageButton1" OnClientClick="enableQtyEdit()" OnClick="AddToOrder"
                            runat="server" ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/add_to_car.gif%>"
                            onmouseover="this.src=this.src.replace('add_to_car.','add_to_car_on.');" onmouseout="this.src=this.src.replace('add_to_car_on.','add_to_car.');" />
                    </div>
                </div>
                <div class="productInfoBlock">
                    <asp:Label runat="server" ID="CaptionUnidades"></asp:Label>
                    <asp:Label ID="ValueUnidades" runat="server"></asp:Label>
                </div>
                <div class="productInfoBlock">
                    <asp:Label runat="server" ID="CaptionReference"></asp:Label>
                    <telerik:RadTextBox TextMode="MultiLine" MaxLength="30" ButtonCssClass="lineInfoTxt"
                        runat="server" ID="ValueDescriptionArea" CssClass="txtareareference">
                    </telerik:RadTextBox>
                </div>
                <div class="productInfoBlock">
                    <julia:RecentlyViewedGdoUserControl runat="server"></julia:RecentlyViewedGdoUserControl>
                </div>
            </div>
        </div>
    </div>
    <%--
<a class="DownloadImage" id="DownloadImage" href="#" target="_blank" download>
        <img alt="" src="/Style Library/Julia/img/descargar.png" 
                        onmouseover="this.src=this.src.replace('descargar.','descargar_on.');"  
                        onmouseout="this.src=this.src.replace('descargar_on.','descargar.');"
	/>
    </a>
    --%>
    <%--
    <div id="photo" class="catalogPhoto">
        <asp:Panel runat="server" CssClass="BasicInformation" ID="BasicInformation">
            <asp:Panel runat="server" CssClass="BasicInfoLeft" ID="BasicInfoLeft">
                <div class="Product_code">
                    
                </div>
                <div class="Product_Measures">
                    
                    <table cellspacing="0" class="tableMeasures">
                        <tr>
                            <td>
                                <img alt="" height="25px" src="/gdo/Style Library/Julia/img/Height.png" />
                            </td>
                            <td>
                                <img alt="" height="25px" src="/gdo/Style Library/Julia/img/Width.png" />
                            </td>
                            <td>
                                <img alt="" height="25px" src="/gdo/Style Library/Julia/img/Depth.png" />
                            </td>
                        </tr>
                        <tr class="ValuestableMeasuresTr">
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ProductPrice">
                    
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="BasicInfoRight" CssClass="BasicInfoRight">
                <div id="orderButton" class="orderButton">
                    <asp:ImageButton ID="realizar_pedido" OnClientClick="enableQtyEdit()" OnClick="AddToOrder" runat="server" 
                        ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/add_to_car.gif%>" 
                        onmouseover="this.src=this.src.replace('add_to_car.','add_to_car_on.');"  
                        onmouseout="this.src=this.src.replace('add_to_car_on.','add_to_car.');"
                    />
                </div>
                <div id="UnidadVenta" class="UnidadVenta">
                    
                </div>
                <div id="quantity" class="quantity">
                    
                </div>
                <div id="Line_referencelbl_input" class="reference">
                    
                </div>
            </asp:Panel>
        </asp:Panel>
    </div>
    --%>
    <asp:Panel runat="server" CssClass="PanelCompatibleCon" ID="PanelCompatibleCon">
        <asp:Label runat="server" ID="LblProductCompatible" CssClass="CaptionLbl LblProveedoresCompatibles LblProductCompatible">
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCompatibleWith")%></asp:Label>
        <asp:Table CellPadding="0" CellSpacing="0" runat="server" CssClass="AssociatedProveidorTable"
            ID="AssociatedProveidorTable">
        </asp:Table>
    </asp:Panel>
    <div class="container">
        <ul id="tabs" class="tabs">
            <li class="firsttab"><a href="#tab1" onclick="return false;">
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DescripciónProductoTab")%></a></li>
            <asp:Panel runat="server" ID="ColorMeasureTab">
                <li class="SecondTab"><a href="#tab2" onclick="return false;">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OpcionesMedidasColoresTab")%></a></li></asp:Panel>
            <asp:Panel runat="server" ID="ArticlesDisponibleTab">
                <li class="ThirdTab"><a href="#tab3" onclick="return false;">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ColeccióncompletaTab")%></a></li></asp:Panel>
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="Catalog">
                    <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionProveidor"></asp:Label>
                    <asp:HyperLink ID="ProveidorReturn" CssClass="LinkProveidor" runat="server">
                        <asp:Label runat="server" ID="ValueProveidor"></asp:Label>
                    </asp:HyperLink>
                </div>
                <div class="Description">
                    <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionDescriptiion"></asp:Label>
                    <asp:Label runat="server" ID="ValueDescription"></asp:Label>
                </div>
                <div class="ColorMeasuresAndOthers">
                    <div class="ColorsAndMore">
                        <div class="color">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionColor"></asp:Label>
                            <asp:Label runat="server" ID="ValueColor"></asp:Label>
                        </div>
                        <div class="materials">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMaterials"></asp:Label>
                            <asp:Label runat="server" ID="ValueMaterials"></asp:Label>
                        </div>
                        <div class="Mantain">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMantain"></asp:Label>
                            <asp:Label runat="server" ID="ValueMantain"></asp:Label>
                        </div>
                        <div class="build">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionBuild"></asp:Label>
                            <asp:Label runat="server" ID="ValueBuild"></asp:Label>
                        </div>
                        <div class="build">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionApilable"></asp:Label>
                            <asp:Label runat="server" ID="ValueApilable"></asp:Label>
                        </div>
                        <div class="cubic">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionCubic"></asp:Label>
                            <asp:Label runat="server" ID="ValueCubic"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionWeight"></asp:Label>
                            <asp:Label runat="server" ID="ValueWeight"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionEAN"></asp:Label>
                            <asp:Label runat="server" ID="ValueEAN"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionCodArancel"></asp:Label>
                            <asp:Label runat="server" ID="ValueCodArancel"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionPaisOrigen"></asp:Label>
                            <asp:Label runat="server" ID="ValuePaisOrigen"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMaxContainerUnits"></asp:Label>
                            <asp:Label runat="server" ID="ValueMaxContainerUnits"></asp:Label>
                        </div>
                    </div>
                    <div class="NumberAndMeasures">
                        <div class="NumberofBultos">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionNumbers"></asp:Label>
                            <asp:Label runat="server" ID="ValueNumbers"></asp:Label>
                        </div>
                        <asp:Panel ID="PanelMeasures" runat="server">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMeasuresBulto"></asp:Label>
                        </asp:Panel>
                    </div>
                </div>
                <div class="DocumentationPanel">
                    <asp:Panel runat="server" ID="Documentation" CssClass="DocumentationPanelDocumentacionTenica">
                        <div class="DocsHeader">
                            <asp:Label runat="server" ID="DocsHeaderLbl" CssClass="CaptionLbl"></asp:Label>
                        </div>
                        <div class="documents">
                            <asp:PlaceHolder ID="DocumentsPlace" runat="server"></asp:PlaceHolder>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="Certificados" CssClass="CertificadosPanel">
                        <div class="DocsHeader">
                            <asp:Label runat="server" ID="DocsHeaderLblCert" CssClass="CaptionLbl"></asp:Label>
                        </div>
                        <div class="documents">
                            <asp:PlaceHolder ID="DocumentsPlaceCerts" runat="server"></asp:PlaceHolder>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div id="tab2" class="tab_content">
                <asp:Panel runat="server" ID="Panel_Colors" CssClass="PanelColorsAgrup">
                    <asp:Label runat="server" ID="AvaliableColors_Lbl" CssClass="LblAgrupCaption CaptionLbl LblColorAgrupCaption LblProductCompatible"></asp:Label>
                    <telerik:RadListView ID="RadListViewColors" CssClass="RadListView" runat="server"
                        ItemPlaceholderID="PlaceHolder1">
                        <LayoutTemplate>
                            <asp:Panel ID="productPanel" runat="server" CssClass="ProductPanelColorAgrup">
                                <div>
                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                </div>
                            </asp:Panel>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Url") %>' CssClass="LynkColor"
                                ID="LynkColor">
                                <img alt="<%# Eval("Description") %>" src="<%# Eval("ImageUrl") %>" class="ColorItem" />
                            </asp:HyperLink>
                            <telerik:RadToolTip runat="server" ID="ToolTipColor" TargetControlID='LynkColor'
                                IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                Width="100px" RelativeTo="Mouse" Animation="None">
                                <asp:Label ID="LblTollTIpCOlor" Text='<%#Eval("ToolTipText")%>' Width="200px" runat="server" />
                            </telerik:RadToolTip>
                        </ItemTemplate>
                    </telerik:RadListView>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="Panel_Measures" ID="Panel_Measures">
                    <div>
                        <asp:Label runat="server" ID="Measures_Lbl" CssClass="CaptionLbl LblProductCompatible CaptionAgrupMeasure"></asp:Label>
                        <telerik:RadComboBox runat="server" CssClass="Combomeasures" ID="ComboMeasures" OnSelectedIndexChanged="ComboMeasures_SelectedIndexChanged"
                            AutoPostBack="true">
                        </telerik:RadComboBox>
                    </div>
                </asp:Panel>
            </div>
            <div id="tab3" class="tab_content">
                <asp:Panel runat="server" CssClass="Panel_Disponibilidad" ID="Panel_Disponibilidad">
                    <asp:Label runat="server" ID="Disponibilidad_Lbl" CssClass="CaptionLbl LblProductCompatible"></asp:Label>
                    <telerik:RadGrid ID="RagridDisponiblitat" runat="server" Width="97%" CssClass="Grid"
                        AutoGenerateColumns="false" EnableAjaxSkinRendering="true" AllowMultiRowSelection="True"
                        GridLines="None" AllowAutomaticUpdates="true" OnNeedDataSource="RagridDisponiblitat_NeedDataSource"
                        OnItemCommand="RagridDisponiblitat_ItemCommand">
                        <ClientSettings EnableRowHoverStyle="true">
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <MasterTableView Summary="RadGrid table" UseAllDataFields="true">
                            <Columns>
                                <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                                <telerik:GridHTMLEditorColumn UniqueName="SmallImage" DataField="SmallImage" />
                                <telerik:GridBoundColumn DataField="ArticleCode" UniqueName="ArticleCode" />
                                <telerik:GridBoundColumn DataField="Description" UniqueName="Description" ReadOnly="true" />
                                <telerik:GridBoundColumn DataField="Color" UniqueName="Color" ReadOnly="true" />
                                <telerik:GridBoundColumn DataField="Measures" UniqueName="Measures" ReadOnly="true" />
                                <telerik:GridBoundColumn DataField="Price" HeaderText="Price" UniqueName="Price"
                                    ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Stock" Display="false" UniqueName="Stock" ReadOnly="true">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditItemStyle ForeColor="Gray"></EditItemStyle>
                        </MasterTableView>
                        <PagerStyle Mode="NextPrev" />
                    </telerik:RadGrid>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="DetailedInformation" CssClass="DetailedInformation">
    </asp:Panel>
</asp:Panel>
<telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
    AutoCloseDelay="3000" OffsetY="-100" EnableShadow="true" Position="MiddleLeft"
    ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"
    Skin="Silk" Text="Producto Añadido">
</telerik:RadNotification>
<asp:Panel ID="debugPanel" runat="server">
    <asp:Literal ID="DegubInfoText" runat="server"></asp:Literal>
</asp:Panel>
