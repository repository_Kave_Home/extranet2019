﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Microsoft.SharePoint;
using System.Collections.Generic;
using GDOWebParts.WebParts.ProductGdoInfo;
using System.Diagnostics;
using GDOWebParts.ControlTemplates1.GDOWebParts;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Linq;
using JuliaGrupUtils.ErrorHandler;
using Telerik.Web.UI;
using System.Text;
using System.Web;
using System.Globalization;
using System.Threading;

namespace GDOWebParts.WebParts.ProductGdoInfo.aspx
{
    public partial class ProductGdoInfoUserControl : UserControl
    {
        Order CurrentOrder;
        public Article currentArticle;
        List<Article> Products = new List<Article>();
        private Order currentOrder;
        private User CurrentUser;
        ArticleDataAccessObject articleDAO = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos parámetros de query string para ver si podemos cargar la página
                string productCode = Request.QueryString["code"];
                string catalog = Request.QueryString["catalog"];
                string Version = Request.QueryString["version"];

                if (string.IsNullOrEmpty(productCode) || string.IsNullOrEmpty(catalog)) {
                    //No podemos mostrar la pagina
                    DegubInfoText.Text = "Product not found";
                    ProductInfoPanel.Visible = false;
                    debugPanel.Visible = true;
                    return; //End
                }

                    RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add("disableQtyEdit();");
              
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio Page_Load" + "<br/>");
                    if (Session["ArticleGdoDAO"] != null)
                    {
                        this.articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                    }
                    else
                    {
                        throw new SPException("Error: ArticleDAO session var is Null");
                    }
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio GetAllProducts" + "<br/>");
                    Products = articleDAO.GetAllProducts();
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin GetAllProducts" + "<br/>");
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio GetProductByCodeAndCatalog" + "<br/>");
                    this.currentArticle = articleDAO.GetProductByCodeAndCatalog(productCode, Version, catalog);
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin GetProductByCodeAndCatalog" + "<br/>");
                    this.CurrentOrder = (Order)Session["OrderGdo"];
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio inicialice" + "<br/>");
                    this.inicialice();
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin inicialice" + "<br/>");



                    if (!String.IsNullOrEmpty(productCode))
                    {
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio GetProductByCode" + "<br/>");
                        this.currentArticle = articleDAO.GetProductByCode(productCode);
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin GetProductByCode" + "<br/>");
                        //this.currentArticle = articleDAO.GetProductByCode(productCode, productVersion);
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio SetArticleDescriptionValues" + "<br/>");
                        if (!Page.IsPostBack)
                        {
                        this.SetArticleDescriptionValues(this.currentArticle);
                        }
                        LoadAssociatedProveitors(this.currentArticle.GDOTipoUnion);
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin SetArticleDescriptionValues" + "<br/>");
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio GetProductImagesByCode" + "<br/>");

                        //List<SPFile> images = new List<SPFile>();
                        List<SPFile> images = articleDAO.GetProductImagesByCode(productCode, Version, "·1");
                        
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio SetPreviewImages" + "<br/>");
                        SetPreviewImages(images);
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio GetProductDocumentsByCode" + "<br/>");
                        List<SPListItem> alldocuments = articleDAO.GetProductDocumentsByCode(productCode, Version);
                        List<SPListItem> documents = alldocuments.Where(p => p["ContentType"].ToString() != "Certificates").ToList();

                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Inicio Creacion Documentos" + "<br/>");
                        string urlDoc = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=document&id=";
                        if (documents.Count == 0)
                        {
                            this.Documentation.Visible = false;
                        }
                        else
                        {
                            foreach (SPListItem doc in documents)
                            {
                                HtmlGenericControl div = getHtmlDocumentItem(urlDoc + doc.Properties["_dlc_DocId"], doc.Name, doc);
                                DocumentsPlace.Controls.Add(div);
                            }
                        }
                        //List<SPListItem> certificats = articleDAO.GetProductCertificateByCode(productCode, Version);
                        List<SPListItem> certificats = alldocuments.Where(p => p["ContentType"].ToString() == "Certificates").ToList();
                        if (certificats.Count == 0)
                        {
                            this.Certificados.Visible = false;
                        }
                        else
                        {
                            foreach (SPListItem cert in certificats)
                            {
                                HtmlGenericControl div = getHtmlDocumentItem(urlDoc + cert.Properties["_dlc_DocId"], cert.Name, cert);
                                Certificados.Controls.Add(div);
                            }
                        }
                        //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin Creacion Documentos" + "<br/>");
                    }
                    //debugtext.Append(DateTime.Now.ToLongTimeString() + " :: Fin Page_Load" + "<br/>");
                
               
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
          

        }

        private void SetPreviewImages(List<SPFile> images)
        {
            try
            {
                string url = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=imageid&id={0}&type={1}&p={2}";
                ((ImageGalleryGdoUserControl)this.imageGallery).ImageUrls = new List<string>();

                bool first = true;
                int imgorder = 1;
                foreach (SPFile item in images)
                {
                    //if (item.Properties["_dlc_DocId"] != null)
                    //{
                    //    ((ImageGalleryUserControl)this.imageGallery).ImageUrls.Add(string.Format(url, item.Properties["_dlc_DocId"].ToString(), "1"));
                    //    if (first)
                    //        ((ImageGalleryUserControl)this.imageGallery).ProductCode = item.Properties["_dlc_DocId"].ToString();
                    //}
                    ////Lo intento por Nombre de documento
                    if (item.Name != null)
                    {
                        //((ImageGalleryGdoUserControl)this.imageGallery).ImageUrls.Add(string.Format(url, item.Name.ToString(), "1", "1"));
                        //REZ 19082013 - Cargamos directamente de intranet para evitar problemas de lentitud de carga
                        ((ImageGalleryGdoUserControl)this.imageGallery).ImageUrls.Add(JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                                JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                item.Url);
                        if (first)
                        {
                            //((ImageGalleryGdoUserControl)this.imageGallery).ProductCode = item.Name.ToString();
                            ((ImageGalleryGdoUserControl)this.imageGallery).ProductCode = JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                                JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                    item.Url;
                            first = false;
                        }
                    }

                    //((ImageGalleryUserControl)this.imageGallery).ImageUrls.Add(string.Format(url, this.productCode, "1", imgorder.ToString()));

                    //if (imgorder == 1)
                    //        ((ImageGalleryUserControl)this.imageGallery).ProductCode = this.productCode;
                    //imgorder++;
                }
                if (string.IsNullOrEmpty(((ImageGalleryGdoUserControl)this.imageGallery).ProductCode))
                {
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    ((ImageGalleryGdoUserControl)this.imageGallery).ProductCode = "/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE" + "_1" + ".jpg";
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private HtmlGenericControl getHtmlDocumentItem(string link, string text, SPListItem item)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            try
            {

                div.Attributes.Add("class", "documentItemGdo"); 
                HtmlGenericControl icon = new HtmlGenericControl("img");
                icon.Attributes.Add("class", "pdf_ico");
                icon.Attributes.Add("alt", "");

                string docIcon = SPUtility.ConcatUrls("/_layouts/images/",
                SPUtility.MapToIcon(item.Web, SPUtility.ConcatUrls(item.Web.Url, item.Url), "", IconSize.Size16));
                icon.Attributes.Add("src", "/gdo/Style Library/Julia/Img/pdf_ico.gif");


                HtmlGenericControl a = new HtmlGenericControl("a");
                a.Attributes.Add("href", link);
                a.InnerHtml = text;

                div.Controls.Add(icon);
                div.Controls.Add(a);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return div;
        }
        private void SetArticleDescriptionValues(Article a)
        {
            try
            {
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                this.CaptionTitle.Text = a.Code + " - " + a.Description;
                this.ValueDescription.Text = a.DescripcionLarga;
                this.ValueColor.Text = a.Color;
                this.ValueMaterials.Text = a.ItemMateriales;
                this.ValueMantain.Text = a.ItemMantenimiento;
                this.ValueBuild.Text = a.ItemMontaje;
                this.ValueCubic.Text = a.Volume;
                this.ValueWeight.Text = a.Weight;
                this.ValueEAN.Text = a.EANcode;
                this.ValueCodArancel.Text = a.CodigoArancelario;
                this.ValuePaisOrigen.Text = a.PaisOrigen;
                this.ValueMaxContainerUnits.Text = a.MaxQtyContenedor;
                this.ValueApilable.Text = a.Apilable;
                this.ValueNumbers.Text = (a.Numitems.IndexOf(",") >= 0) ? a.Numitems.Substring(0, a.Numitems.IndexOf(",")) : a.Numitems;
                this.ValueCode.Text = a.Code;
                if ((string)Session["Divisa"] == "0")
                {
                    this.ValuePrice.Text = a.Price.ToString("N2", new CultureInfo("es-Es", false))+ " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars");
                }
                else if ((string)Session["Divisa"] == "2")
                {
                    this.ValuePrice.Text = (a.Price * Convert.ToDouble(CurrentUser.CoeficientGDOPVP)).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars");
                }
                else
                {
                    this.ValuePrice.Text = (a.Price * Convert.ToDouble(CurrentUser.DolarCoef)).ToString("N2", new CultureInfo("es-Es", false))+ " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                }
                
                this.ValueUnidades.Text = a.UnidadMedida.ToString("N0"); 
                this.ValueProveidor.Text = a.GDONoVendedor;
                //this.ProveidorReturn.NavigateUrl = "/gdo/Pages/Home.aspx?proveidor=" + a.GDONoVendedor;
                this.ProveidorReturn.NavigateUrl = "/gdo/Pages/RedirectTo.aspx?r=products&proveidor=" + a.GDONoVendedor;
                SetTableMeasures(a.Measures);
                this.ProductWidth.Text = a.Width +" "  +  JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                this.ProductHeight.Text = a.Height+ " "  + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                this.ProductDepth.Text = a.Length + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                this.itemQty.MinValue = a.GDOMinQty;
                this.itemQty.MaxLength = 5;
                this.itemQty.IncrementSettings.Step = (Double)a.UnidadMedida;
                this.itemQty.NumberFormat.DecimalDigits = 0;
                if (a.ProductNew == "No")
                {
                    this.ImgNewProd.Visible = false;
                }
                else
                {
                      this.ImgNewProd.Visible  = true;
                }
                if (a.ComingSoon == "No")
                {
                    this.ImgComingSoon.Visible = false;
                }
                else
                {
                    this.ImgComingSoon.Visible = true;
                }
                if (a.GDOMinQty < 1 || a.GDOMinQty > 9999)
                {
                    throw new NavException("Error In Min Qty");
                }

                this.itemQty.Value = (Double)a.GDOMinQty;

                if (a.TipoAgrup != 0 )
                {
                    RagridDisponiblitat.Visible = true;
                    if (a.TipoAgrup == 1 && a.DetalleAgrup != string.Empty)
                    {
                        ColorMeasureTab.Visible = true;
                        ArticlesDisponibleTab.Visible = true;
                        Panel_Colors.Visible = true;
                        Panel_Measures.Visible = false;
                        this.RadListViewColors.DataSource = ColorItems(a);
                        (RagridDisponiblitat.MasterTableView.GetColumn("Measures") as GridBoundColumn).Display = false;
                    }
                    else if (a.TipoAgrup == 2 && a.DetalleAgrup != string.Empty)
                    {
                        ColorMeasureTab.Visible = true;
                        ArticlesDisponibleTab.Visible = true;
                        Measures_Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Product_Measures_Lbl");
                        Panel_Colors.Visible = false;
                        Panel_Measures.Visible = true;
                        (RagridDisponiblitat.MasterTableView.GetColumn("Color") as GridBoundColumn).Display = false;
                        loadComboMeasuersValue(a);
                    }
                    this.RagridDisponiblitat.DataSource = LoadArticlesDisponibles(a);
                }
                else
                {
                    ColorMeasureTab.Visible = false;
                    ArticlesDisponibleTab.Visible = false;
                    Panel_Colors.Visible = false;
                    Panel_Measures.Visible = false;
                    RagridDisponiblitat.Visible = false;
                }
                
               
             

                  


            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadAssociatedProveitors(string tipoUnion)
        {
            try
            {
                if (tipoUnion == "")
                {
                    PanelCompatibleCon.Visible = false;
                }
                else
                {
                    PanelCompatibleCon.Visible = true;
                    List<string> Proveitors = Products.Where(p => p.GDOTipoUnion == tipoUnion).Select(p => p.GDONoVendedor).Distinct().ToList();
                    //String CurrentProvider = Products.Where(p => p.Code == Request.QueryString["code"]).Select(p => p.GDONoVendedor).First();
                    if (Proveitors.Count == 1)
                    {
                        PanelCompatibleCon.Visible = false;
                    }
                    else
                    {
                        TableRow row = new TableRow();
                        foreach (string cod in Proveitors)
                        {
                            //if (cod != CurrentProvider)
                            //{
                                TableCell cell = new TableCell();
                                //cell.Style.Add("width", "50px");
                                cell.Style.Add("text-align", "center");
                               
                                HyperLink link = new HyperLink();
                                //link.NavigateUrl = "/gdo/Pages/Home.aspx?proveidor=" + cod;
                                link.NavigateUrl = "/gdo/Pages/RedirectTo.aspx?r=products&proveidor=" + cod;
                                Label Lbl = new Label();
                                Lbl.Text = cod;
                                link.Controls.Add(Lbl);
                                cell.Controls.Add(link);
                                row.Cells.Add(cell);
                            //}
                        }
                        TableCell cellLast = new TableCell();
                        //cellLast.Style.Add("width", "50px");
                        
                        cellLast.Style.Add("text-align", "center");
                        HyperLink linkLast = new HyperLink();
                        //linkLast.NavigateUrl = "/gdo/Pages/Home.aspx?Grupproveidors=" + tipoUnion;
                        linkLast.NavigateUrl = "/gdo/Pages/RedirectTo.aspx?r=products&Grupproveidors=" + tipoUnion;
                        Label LblLast = new Label();
                        LblLast.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("vertodos"); 
                        linkLast.Controls.Add(LblLast);
                        cellLast.Controls.Add(linkLast);
                        row.Cells.Add(cellLast);

                        AssociatedProveidorTable.Rows.Add(row);
                    }
                }
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void loadComboMeasuersValue(Article a)
        {
            this.ComboMeasures.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ComboMeasuresEmptyMessage");
            List<string> ListMeasures = Products.Where(p => p.PatronAgrup == a.PatronAgrup).Select(p => p.CodMedida).ToList();
            this.ComboMeasures.Items.Clear();
           foreach(string artMed in ListMeasures)
            {
                if (artMed != a.CodMedida)
                {
                    this.ComboMeasures.Items.Add(new Telerik.Web.UI.RadComboBoxItem(artMed, a.PatronAgrup + ";" + artMed));
                }
            }
        }

      
        private DataTable ColorItems(Article a) {

            
            DataTable table = new DataTable();
            table.Columns.Add("Code");
            table.Columns.Add("ImageUrl");
            table.Columns.Add("Description");
            table.Columns.Add("Url");
            table.Columns.Add("ToolTipText");
            string[] ArtCol = a.DetalleAgrup.Split(';');
            for (int i = 0; i < ArtCol.Length; i++)
            {
                Article newProduct = Products.Where(p => p.PatronAgrup == a.PatronAgrup && p.CodColor == ArtCol[i]).SingleOrDefault() as Article ;
                if (newProduct != null)
                {
                    DataRow row = table.NewRow();
                    row["Code"] = a.Code;
                    row["ImageUrl"] = "/Style Library/Color Images/" + ArtCol[i] + ".jpg";
                    row["Description"] = ArtCol[i];
                    row["Url"] = "/gdo/Pages/productGdoInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion;
                    row["ToolTipText"] = JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtCol[i]);
                   
                    
                   
                    table.Rows.Add(row);
                }
            }
    
            return table;
        }
      
        public void RagridDisponiblitat_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            
            //this.RadGrid1.DataBind();

        }
        private DataTable LoadArticlesDisponibles(Article a)
        {
            DataTable table = new DataTable();

            table.Columns.Add("Id");
            table.Columns.Add("SmallImage");
            table.Columns.Add("ArticleCode");
            table.Columns.Add("Description");
            table.Columns.Add("Color");
            table.Columns.Add("Measures");
            table.Columns.Add("Price");
            table.Columns.Add("Stock");

            List<Article> ProductList = Products.Where(c => c.PatronAgrup == a.PatronAgrup).ToList();
            foreach (Article art in ProductList)
            {
                DataRow row = table.NewRow();
                
                row["SmallImage"] = "<a href='/gdo/Pages/productGdoInfo.aspx?code=" + art.Code + "&catalog=" + art.InsertcatalogNo + "&version=" + art.ArticleVersion + "'><img src="+ art.SmallImgUrl + " alt='' width='50px' height='50px' /></a>";
                row["ArticleCode"] = art.Code;
                row["Description"] = art.Description;
                row["Color"] = art.Color;
                row["Measures"] = art.CodMedida;
                if ((string)Session["Divisa"] == "0")
                {
                    row["Price"] = art.Price.ToString("N2", new CultureInfo("es-Es", false));
                }
                else if ((string)Session["Divisa"] == "2") {
                    row["Price"] = (art.Price * Convert.ToDouble(CurrentUser.CoeficientGDOPVP)).ToString("N2", new CultureInfo("es-Es", false));
                }
                else
                {
                    row["Price"] = (art.Price * Convert.ToDouble(CurrentUser.DolarCoef)).ToString("N2", new CultureInfo("es-Es", false));
                } 
                
                row["Stock"] = "HOla que ase";
                table.Rows.Add(row);
            }
            return table;

        }
        private void SetTableMeasures(string measures)
        {
            try
            {
                if (!string.IsNullOrEmpty(measures))
                { 
                    //“Alto1” + “;” + “Ancho1” + “;” + “Fondo1” + “;” + “Alto2” + “;” + “Ancho2” + “:” + “Fondo2”
                  
                    Table table = new Table();
                    table.Style.Add("text-align", "center");
                    //Add headers
                    TableRow row = new TableRow();
                        TableHeaderCell headerCell = new TableHeaderCell();
                        headerCell.Text = "";
                        row.Cells.Add(headerCell);
                        headerCell = new TableHeaderCell();
                        headerCell.Text = "<img height='25px' src='/gdo/style library/julia/img/Height.png' />";
                        row.Cells.Add(headerCell);
                        headerCell = new TableHeaderCell();
                        headerCell.Text = "<img height='25px' src='/gdo/style library/julia/img/Width.png' />";
                        row.Cells.Add(headerCell);    
                        headerCell = new TableHeaderCell();
                        headerCell.Text = "<img height='25px' src='/gdo/style library/julia/img/Depth.png' />";
                        row.Cells.Add(headerCell);
                    table.Rows.Add(row);
                    string[] Measures = measures.Split('#');

                    //Add the Column values
                    for (int i = 0; i < Measures.Length; i++)
                    {
                        string[] values = Measures[i].Split(';');
                         row = new TableRow();
                         TableCell cellBulto = new TableCell();
                         cellBulto.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("bulto") + " " + (i+1)+ ":";
                         row.Cells.Add(cellBulto);
                         for (int j = 0; j < values.Length;j++)
                         {
                             TableCell cell = new TableCell();
                             cell.Text = values[j] + " "  + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                             cell.Style.Add("padding", "0px 5px");
                             row.Cells.Add(cell);
                         }
                            table.Rows.Add(row);
                    }
                    // Add the the Table in the Form
                    PanelMeasures.Controls.Add(table);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public bool IsDivisble(int x, int n)
        {
            return (x % n) == 0;
        }
        private void inicialice()
        {
            try
            {
                if (Session["OrderGdo"] != null)
                {
                    CurrentOrder = (Order)Session["OrderGdo"];
                }

                //this.CaptionTitle.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(""); 
                this.CaptionProveidor.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProveidorLbl") + " :";
                this.CaptionDescriptiion.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription") + " :";
                this.CaptionColor.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Color") + " :";
                this.CaptionMaterials.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMaterials") + " :";
                this.CaptionMantain.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMantain") + " :";
                this.CaptionBuild.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductBuilt") + " :";
                this.CaptionCubic.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductVolume") + " :";
                this.CaptionWeight.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductWeight") + " :";
                this.CaptionNumbers.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NumberOfPorducts") + " :";

                this.CaptionCode.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
                this.CaptionMeasures.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasProducto");
                this.CaptionPrice.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price");
                this.CaptionMeasuresBulto.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasBulto");
                this.CaptionUnidades.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblUnidadesVenta");
                this.itemQty.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");
                this.CaptionReference.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblLineDescription");
                this.DocsHeaderLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DocumentacionTecnica");
                this.DocsHeaderLblCert.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Certificaciones");
                this.AvaliableColors_Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Product_AvaliableColors");
                this.Disponibilidad_Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Product_ArticlesRelacionats");

                //REZ 25072014 - Nous atributs
                this.CaptionEAN.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionEAN") + " : ";
                this.CaptionCodArancel.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionCodArancel") + " : ";
                this.CaptionPaisOrigen.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionPaisOrigen") + " : ";
                this.CaptionMaxContainerUnits.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionMaxContainerUnits") + " : ";
                this.CaptionApilable.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionApilable") + " : ";

                //headers

                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("SmallImage").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SmallImageOrder");
                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
                if (((string)Session["Divisa"] == "0") || ((string)Session["Divisa"] == "2"))
                {
                    this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UnitariPriceOrderDollar");
                }
                else
                {
                    this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UnitariPriceOrder");
                }
                
                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Color").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Color");
                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Measures").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasProducto");
                this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Stock").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TotalPriceOrder");

            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            
        }
        protected void ComboMeasures_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                string[] dataNewArt = ComboMeasures.SelectedValue.Split(';');
                Article newProduct = Products.Where(p => p.PatronAgrup == dataNewArt[0] && p.CodMedida == dataNewArt[1]).SingleOrDefault() as Article;
                if (newProduct != null)
                {
                    string Url = "/gdo/Pages/productGdoInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion; ;
                    Response.Redirect(Url,true);
                }
               
               
                
                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
         
        }    
        public void AddToOrder(object sender, EventArgs e)
        {
            try
            {
                int qty = Int32.Parse(this.itemQty.Text);

                string linedescription = this.ValueDescriptionArea.Text;
                if (Session["OrderGdo"] != null)
                {
                    CurrentOrder = (Order)Session["OrderGdo"];
                }


                this.CurrentOrder.AddProduct(this.currentArticle, qty, linedescription, CurrentOrder.OrderClient.UserName);
                //refresquem la order que tenim a la sessio
                Session["OrderGdo"] = this.CurrentOrder;
                
                //refresquem el contenidor
                ContenedorUserControl contenedor = (ContenedorUserControl)this.FindControlRecursive(this.Page.Master, "Contenedor");
                contenedor.Refresh();

                //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "HideSearchBoxTopMenu", "HideSearchBoxTopMenu();", true);

                //Volvemos al listado de productos
                //string script = "<script type='text/javascript'>" +
                //        "setTimeout(function(){window.location.href = '/gdo';}, 500);" +
                //        "</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "AddToOrder", script);
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);  
            }
        }
        protected void RagridDisponiblitat_ItemCommand(object source, GridCommandEventArgs e)
        {
            
            if (e.CommandName == "RowClick")
            {
            GridDataItem item = (GridDataItem)e.Item;
            string code = item["ArticleCode"].Text;
            Article art = (Article)Products.Where(p => p.Code == code).SingleOrDefault();
                Response.Redirect("/gdo/Pages/productGdoInfo.aspx?code=" + art.Code + "&catalog=" + art.InsertcatalogNo + "&version=" + art.ArticleVersion);
            }
        }
        public Control FindControlRecursive(Control Root, string Id)
        {
            try
            {
                if (Root.ID == Id)
                    return Root;
                foreach (Control Ctl in Root.Controls)
                {
                    Control FoundCtl = FindControlRecursive(Ctl, Id);
                    if (FoundCtl != null)
                        return FoundCtl;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return null;
        }
 
    }
}
