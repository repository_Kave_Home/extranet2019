﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsUserControl.ascx.cs" Inherits="GDOWebParts.WebParts.Products.ProductsUserControl" %>
<%@ Register TagPrefix="julia" TagName="ArticlesGdoUserControl" Src="~/_CONTROLTEMPLATES/GDOWebParts/ArticlesGdoUserControl.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<SharePoint:CssRegistration ID="CssRegistrationPedido" name="/gdo/Style Library/Julia/ProductsListsGdo.css" runat="server"/>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerArticlesCatalogUserGdo" runat="server" >
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="Panel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="Panel" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="false" BackColor="White" Transparency="50">
        <img src="/Style Library/Julia/img/loading.gif" style="margin:0 auto;position:fixed;top:48%" />
</telerik:RadAjaxLoadingPanel>

<asp:Panel ID="Panel" runat ="server" OnPreRender ="catalogPanel_PreRender"  CssClass="displaytable">

    <julia:ArticlesGdoUserControl ID="ArticlesGdoUserControl" runat="server">
    </julia:ArticlesGdoUserControl>

</asp:Panel>