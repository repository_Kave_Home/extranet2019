﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Collections.Generic;
using Microsoft.SharePoint;
using System.Diagnostics;
using Telerik.Web.UI;

namespace GDOWebParts.WebParts.Products
{
    public partial class ProductsUserControl : UserControl
    {
        private ArticleDataAccessObject articleDAO;
        public Order order;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void CreateChildControls()
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    this.SetCatalogProducts(Request.QueryString["catalog"]);
                }
            }
            catch (Exception ex)
            {
                Logger.WiteLog(ex.Message, ex.StackTrace);
            }
        }
        private void SetCatalogProducts(string catalogNo)
        {
            try
            {
                if (articleDAO == null && Session["ArticleGdoDAO"] != null)
                {
                    this.articleDAO = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                    List<Article> articles;
                 
                    articles = this.articleDAO.GetCatalogProducts();
                    Session["ArticlesGdo"] = articles;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void catalogPanel_PreRender(object sender, EventArgs e)
        {
            try
            {
                RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
                manager.AjaxSettings.AddAjaxSetting(Panel, Panel, RadAjaxLoadingPanel1);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
