﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Linq;

namespace GDOWebParts.WebParts.RedirectTo
{
    public partial class RedirectToUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = string.Empty;
            string RedirMode = Request.QueryString["r"];
            string artId = Request.QueryString["artId"];

            if ((!String.IsNullOrEmpty(RedirMode)) && (RedirMode.Trim().ToUpper() == "PRODUCTS"))
            {
                //Venimos del menú superior o de un nuevo filtro de proveedor o grupo (productInfoGdo)
                ClearFilters();
                string filters = SetFilters();

                url = "/gdo/Pages/Home.aspx" + filters;
                Response.Redirect(url);
            }
            else if ((!String.IsNullOrEmpty(RedirMode)) && (RedirMode.Trim().ToUpper() == "BACK"))
            {
                //Volvemos de ProductInfo o de Pedido Advanced. No borramos filtros
                url = "/gdo/Pages/Home.aspx";
                string urlConn = (url.Contains("?")) ? "&" : "?";
                url = (!String.IsNullOrEmpty(artId)) ? url + urlConn + "artId=" + artId : url;
                Response.Redirect(url);
            }
            else
            {
                //Redirección de color

                string Colorcode = Request.QueryString["Colorcode"];

                string patronagrup = Request.QueryString["PatronAgrup"];
                ArticleDataAccessObject articledao = (ArticleDataAccessObject)Session["ArticleGdoDAO"];
                List<Article> Products = articledao.GetAllProducts();
                Article newProduct = Products.Where(p => p.PatronAgrup == patronagrup && p.CodColor == Colorcode).SingleOrDefault() as Article;

                if (newProduct != null)
                {

                    url = "/gdo/Pages/productGdoInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion;
                    Response.Redirect(url);
                }
                else
                {
                    this.PanelRedirectError.Visible = true;
                    this.ErrorLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ErroLblArtNotFound");
                }

            }


        }

        private void ClearFilters()
        {
            Session.Remove("fSearchTxtGdo");
            Session.Remove("fFamilyGdo");
            Session.Remove("fSubFamilyGdo");
            Session.Remove("fProgramGdo");
            Session.Remove("fSearchTxtGdo");
            Session.Remove("fPageNoGdo");
            Session.Remove("fCatalogGdo");
            Session.Remove("fCategoryGdo");
            Session.Remove("fSubCategoryGdo");
            Session.Remove("fEstanceGdo");
            Session.Remove("fStyleGdo");

            Order CurrentOrder = (Order)Session["OrderGdo"];
            if (CurrentOrder.ListOrderProducts.Count == 0)
            {
                Session["GrupoProveedoresFilter"] = string.Empty;
            }
            Session["ProveedorFilter"] = string.Empty;

        }

        private string SetFilters()
        {
            string filters = String.Empty;

            string fprov = Request.QueryString["proveidor"];
            string gprov = Request.QueryString["Grupproveidors"];
            string c = Request.QueryString["c"];
            string sc = Request.QueryString["sc"];
            string e = Request.QueryString["e"];
            string es = Request.QueryString["es"];
            string t = Request.QueryString["t"];

            if ((!String.IsNullOrEmpty(fprov)) || (!String.IsNullOrEmpty(gprov)) || (!String.IsNullOrEmpty(c)) || (!String.IsNullOrEmpty(sc)) || (!String.IsNullOrEmpty(t)))
            {
                filters = "?";
                if (!String.IsNullOrEmpty(fprov))
                {
                    filters += "proveidor=" + fprov;
                    Session["ProveedorFilter"] = fprov;
                }
                if (!String.IsNullOrEmpty(gprov))
                {
                    SetAmp(filters, gprov);
                    filters += "Grupproveidors=" + gprov;
                    Session["GrupoProveedoresFilter"] = gprov;
                }
                if (!String.IsNullOrEmpty(c))
                {
                    SetAmp(filters, c);
                    filters += "c=" + c;
                    Session["fCategoryGdo"] = c;
                }
                if (!String.IsNullOrEmpty(sc))
                {
                    SetAmp(filters, sc);
                    filters += "sc=" + sc;
                    Session["fSubCategoryGdo"] = sc;
                }
                if (!String.IsNullOrEmpty(e))
                {
                    SetAmp(filters, e);
                    filters += "e=" + e;
                    Session["fEstanceGdo"] = e;
                }
                if (!String.IsNullOrEmpty(es))
                {
                    SetAmp(filters, es);
                    filters += "es=" + es;
                    Session["fStyleGdo"] = es;
                }
                if (!String.IsNullOrEmpty(t))
                {
                    SetAmp(filters, t);
                    filters += "t=" + t;
                    Session["fSearchTxtGdo"] = t;
                }
            }

            return filters;
        }

        private string SetAmp(string filters, string field)
        {
            if ((filters != "?") && (!String.IsNullOrEmpty(field)))
            {
                filters += "&";
            }
            return filters;
        }

    }
}
