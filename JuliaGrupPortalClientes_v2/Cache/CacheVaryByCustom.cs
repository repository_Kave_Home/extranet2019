﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.ApplicationRuntime;
using System.Web;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;


namespace JuliaGrupPortalClientes_v2.Cache
{
    public class CacheVaryByCustom : SPHttpApplication, IVaryByCustomHandler
    {
        private const string VaryByUserString = "VaryByUser";

        public override void Init()
        {
            base.Init();

            // register our own VaryByCustomStringHandler 
            this.RegisterGetVaryByCustomStringHandler((Microsoft.SharePoint.ApplicationRuntime.IVaryByCustomHandler)this);
        }

        public string GetVaryByCustomString(HttpApplication app, HttpContext context, string custom)
        {
            // This code handles custom vary parameters specified in the cache profile 
            string[] strings = custom.Split(';');

            // verify if our parameter is configured 
            foreach (string str in strings)
            {
                // if yes, lets return a custom variation string which contains the ID of the user 
                if (str == VaryByUserString)
                {
                    // No se puede usar session en Global-.asax, por tanto para Outputcaching a nivel de pagina no sirve
                    //podemos usar cookies en su lugar, o bien cache a nivel de control
                    //Cookies: http://www.hawkfield.be/articles.php/VaryByCustom_Output_Caching
                    //http://stackoverflow.com/questions/6781155/output-caching-using-both-varybyparam-and-varybycustom

                    string output = String.Empty;
                    /* //Codigo que no funciona a nivel de pagina porque usa session
                    User currentUser;
                    string lang = String.Empty;
                    Client currentClient;
                    string price = String.Empty;
                    string username = String.Empty;
                    string clientcode = String.Empty;
                    

                        if (Session["User"] != null)
                        {
                            currentUser = (User)Session["User"];
                            lang = currentUser.Language;
                            username = currentUser.UserName;
                        }
                        if (Session["Client"] != null)
                        {
                            currentClient = (Client)Session["Client"];
                            clientcode = currentClient.Code;
                        }
                        if (Session["PriceSelector"] != null)
                        {
                            price = Session["PriceSelector"].ToString();
                        }
                        output = username + "_" + clientcode + "_" + price + "_" + lang;
                    */

                        HttpCookie cookie = context.Request.Cookies["UserSettings"];
                        //We have no cookie ?
                        if (cookie != null)
                            //Return what is contained inside the cookie
                            output = context.Request.Browser.Browser + "_" + cookie.Value;
                        else output = new Guid().ToString();
                    
                    return output;
                }
            }

            // if our parameter has not been provided just return an empty string. 
            return String.Empty;
        }
    } 
}
