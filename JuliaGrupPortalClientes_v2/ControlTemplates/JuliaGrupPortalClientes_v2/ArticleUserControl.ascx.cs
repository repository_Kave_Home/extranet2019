﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Threading;
using System.Diagnostics;
using Microsoft.SharePoint;


namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class ArticleUserControl : UserControl
    {
        private Article article;

        public Article Article
        {
            get { return article; }
            set { article = value; }
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (article != null)
                {
                    this.lblPrecio.Text = article.PriceInPoints + " " + LanguageManager.GetLocalizedString("Points");
                    this.lblroductoCatalogo.Text = article.Description;
                    this.lblTxtreferencia.Text = article.Code;
                    this.prodImage.ImageUrl = article.ImgUrl;

                    string ImageType = string.IsNullOrEmpty(Request.QueryString["type"]) ? "1" : Request.QueryString["type"];
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;

                    this.prodImage.Attributes["onerror"] = "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_" + ImageType + ".jpg';";

                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
