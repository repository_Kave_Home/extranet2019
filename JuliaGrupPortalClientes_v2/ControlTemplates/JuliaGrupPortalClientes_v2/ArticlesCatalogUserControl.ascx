﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticlesCatalogUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.ArticlesCatalogUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%-- @ OutputCache Duration="1" VaryByControl="*" % --%>
<%-- Catalog; Familia; Subfamilia; Programa; SearchProduct; ibSearchProduct; PrintCurentView; RadDataPager2; RadDataPager1; ImageButton1 --%>
<%-- @ OutputCache Duration="1" Location="None" % --%>
<%-- Lo ponemos en la master --%>
<%-- 
<script type="text/javascript" src="/Style Library/Julia/js/handlebars.js" ></script> 
<script type="text/javascript" src="/Style Library/Julia/js/JuliaClients.js" ></script>
--%>
<link href="/Style Library/Julia/kendo.common.min.css" rel="stylesheet">
<link href="/Style Library/Julia/kendo.default.min.css" rel="stylesheet">
<script src="/Style Library/Julia/js/kendo.web.min.js"></script>
<script language="javascript" type="text/javascript">

    function OpenFile(url) {
        $.blockUI({ message: 'En Proceso' });
        window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
        $.unblockUI();
    }
    function OpenProductInfoDialog1(code, version, catalog) {

        var options = {
            url: "/Pages/ProductInfo.aspx?code=" + code + "&version=" + version + "&catalog=" + catalog,
            width: 900,
            height: 600,

            title: '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductInfo") %>',
            dialogReturnValueCallback: DialogCallback1
        };

        SP.UI.ModalDialog.showModalDialog(options);
    }

    function DialogCallback1(dialogResult, returnValue) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            RefreshTable();
            var notification = $find("<%= RadNotification1.ClientID %>");
            var message =  '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct")%>';           
            notification.set_text(message);
            notification.show();
        }
    }

    function PageIndexChanging(sender, eventArgs) {
        var oldPageIndex = eventArgs.get_oldPageIndex();
        var newPageIndex = eventArgs.get_newPageIndex();
        eventArgs.set_newPageIndex(newPageIndex);
        
    }

    <%--function setQttProduct(codeWS, code) {
        $('#<%= this.qttNumber.ClientID%>').val($('#' + codeWS).children('.NewitemQty').children('span').children('input[type="text"]').val());
        $('#<%= this.codeProduct.ClientID%>').val(code);
        return true;
    }--%>

    function ResetScrollPosition() {
        setTimeout("window.scrollTo(0,0)", 0);
    }

    <%--function RefreshCartTable() {
        document.aspnetForm.ctl00_PlaceHolderCarritoVolador_Carrito_mainPedidoCart_RefreshButton.click();
    }--%>

    function sendTextSearch(sender, eventArgs) {
        var c = eventArgs.get_keyCode();
        if (c == 13) {
            /*alert(sender.get_textBoxValue());*/
            $('#<%= this.ibSearchProduct.ClientID%>').click();
            eventArgs.set_cancel(true);
        }
    }

    function pageLoad() {
        $(function () {
            $(".QttyNum").kendoNumericTextBox({
                format: "#",
                decimals: 0,
                min: 1,
                max: 999
            });
            $(".k-numerictextbox").css("width","50px"); 
            $(".QttyNum").keypress(function () {
                var qtt = this.value;
                if ((qtt != undefined) && (!isNaN(qtt)) && (qtt > 999)) {
                    return false;
                }
            });
            $('.gstock').attr('title', '<%= JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Verde") %>');
            $('.astock').attr('title', '<%= JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja") %>');
            $('.rstock').attr('title', '<%= JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo") %>');
            $('.bstock').attr('title', '<%= JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Negro") %>');
        });
     }

     function FocusArticle(codArticle) {
        $(function () {
            $(window).scrollTo($("[id='" + codArticle + "']"), 0);
        });
    }
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    function filterClick() {
        var elem = document.getElementById("<%= ibClearFilter.ClientID %>").click();
    }
    function onLoadTreeView(sender, args) {
        var search = $("#" + '<%=SearchProduct.ClientID %>').val();
        var color = $find('<%=Color.ClientID %>');
        var program = $find('<%=Programa.ClientID %>');
        var catalog = getParameterByName("catalog")
        var treeView = $find('<%=CategoriaTreeView.ClientID %>');
        var estance = $("#estance").val();
        var style = $("#style").val();
        var price = $find('<%=RadSlider_Price.ClientID %>');
        var filterMin = $("#" + '<%=minSlider.ClientID %>').val();
        var filterMax = $("#" + '<%=maxSlider.ClientID %>').val();
        var dto = $find('<%=DtoExclusivo.ClientID %>');
        var productEsEx = $find('<%=ProductoExEs.ClientID %>');
        if (search != null && color != null && program != null && treeView != null && price != null && dto != null && productEsEx != null) {
            JuliaClients.Service.PrepareTreeView(treeView, search, color.get_value(), catalog, program.get_value(), null, null, estance, style, price.get_selectionStart(), price.get_selectionEnd(), filterMin, filterMax, dto.get_value(), productEsEx.get_value());
        }
    };
    (function (global) {
        function OnClientValueChanged(slider, args) {
            // Show the tooltip only while the slider handle is sliding. In case the user simply clicks on the track of the slider to change the value
            // the change will be quick and the tooltip will show and hide too quickly.
            if (!isSliding) {
                return;
            }
            var tooltip = getTooltip();
            global.setTimeout(function () {
                updateToolTipText(tooltip, slider);
            }, 30);
        };
        var isSliding = false;
        function OnClientSlideStart(slider, args) {
            isSliding = true;
            showRadToolTip(slider);
        };
        function OnClientSlide(slider, args) {
            resetToolTipLocation(getTooltip());
        };
        function OnClientSlideRangeStart(slider, args) {
            isSliding = true;
            showRadToolTip(slider);
        };
        function OnClientSlideRange(slider, args) {
            resetToolTipLocation(getTooltip());
        }; 
        function OnClientSlideEnd(slider, args) {
            isSliding = false;
            getTooltip().hide();
        }; 
        function OnClientSlideRangeEnd(slider, args) {
            isSliding = false;
            getTooltip().hide();
        };
        function showRadToolTip(slider) {
            var tooltip = getTooltip();
            tooltip.set_targetControl($get("RadSliderSelected_" + slider.get_id()));
            resetToolTipLocation(tooltip);
            global.setTimeout(function () {
                updateToolTipText(tooltip, slider);
            }, 30);
        };
        function resetToolTipLocation(tooltip) {
            if (!tooltip.isVisible()) {
                global.setTimeout(function () {
                    tooltip.show();
                }, 20);
            }
            else {
                tooltip.updateLocation();
            }
        }; 
        function updateToolTipText(tooltip, slider) {
            var div = document.createElement("div");
            div.style.whiteSpace = "nowrap";
            if (slider.get_itemType() == Telerik.Web.UI.SliderItemType.Item) {
                div.innerHTML = (slider.get_selectedItems()[0].get_text() + " / " + slider.get_selectedItems()[1].get_text());
            }
            else {
                div.innerHTML = (slider.get_selectionStart() + " / " + slider.get_selectionEnd());
            }
            tooltip.set_contentElement(div);
        };
        global.OnClientValueChanged = OnClientValueChanged;
        global.OnClientSlideStart = OnClientSlideStart;
        global.OnClientSlide = OnClientSlide;
        global.OnClientSlideRangeStart = OnClientSlideRangeStart;
        global.OnClientSlideRange = OnClientSlideRange;
        global.OnClientSlideEnd = OnClientSlideEnd;
        global.OnClientSlideRangeEnd = OnClientSlideRangeEnd;
 
    })(window);
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        //Check to see if the window is top if not then display button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#to-top').fadeIn();
            } else {
                $('#to-top').fadeOut();
            }
        });
    });
    //$("#to-top").click(function () { $(window).scrollTo(0, 500); });

    var sourceArticleId = getParameterByName('artId');
    if (sourceArticleId != 'undefined' && sourceArticleId != '') {
        FocusArticle(sourceArticleId);
    }
        
</script>
<style type="text/css">
    .sliderCarousel
    {
        /*forzar separacion entre catalogos y tabla de productos*/
        min-height: 100px !important;
    }
    .QttyNum
    {
        width: 50px;
    }
    .NewimgProductAdd
    {
        background-image: url('/Style Library/Julia/img/<%=(string.IsNullOrEmpty(System.Threading.Thread.CurrentThread.CurrentUICulture.Name)) ? "es-ES" : System.Threading.Thread.CurrentThread.CurrentUICulture.Name %>/add_to_car.gif');
    }
    .NewimgProductAdd:hover
    {
        background-image: url('/Style Library/Julia/img/<%=(string.IsNullOrEmpty(System.Threading.Thread.CurrentThread.CurrentUICulture.Name)) ? "es-ES" : System.Threading.Thread.CurrentThread.CurrentUICulture.Name %>/add_to_car_on.gif');
    }
    .PagOrder
    {
        float: left;
        margin-top: 5px;
        width: 170px;
    }
    .RadDataPager
    {
        display: inline-block !important;
    }
    .s4-wpTopTable
    {
        margin: 0;
    }
    .ColorItemProduct
    {
        width: 60px !important;
        height: 60px !important;
        border-color: #e3e3e3;
    }
    .ColorItemMore
    {
        width: 10px !important;
        height: 60px !important;
        margin-left: 2px;
        margin-bottom: 4px;
        padding: 2px;
        background-image: url('/Style Library/julia/img/agmore.png');
        border: 0;
        top: 0;
        left: 0;
        background-position: left center;
        display: inline-block;
        background-repeat: no-repeat;
    }
</style>
<style type="text/css">
    /* to top */
    #to-top
    {
        display: block;
        position: fixed;
        text-align: center;
        line-height: 12px !important;
        right: 17px;
        bottom: -30px;
        color: #fff;
        cursor: pointer;
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        -o-border-radius: 2px;
        z-index: 10000;
        height: 29px;
        width: 29px;
        background-color: rgba(0,0,0,0.4);
        background-repeat: no-repeat;
        background-position: center;
        transition: background-color 0.1s linear;
        -moz-transition: background-color 0.1s linear;
        -webkit-transition: background-color 0.1s linear;
        -o-transition: background-color 0.1s linear;
    }
    
    #to-top i
    {
        line-height: 29px !important;
        width: 29px !important;
        height: 29px !important;
        font-size: 14px !important;
        top: 0px !important;
        left: 0px !important;
        text-align: center !important;
        background-color: transparent !important;
    }
    
    
    #to-top:hover, #to-top.dark:hover
    {
        background-color: #A12830;
    }
    
    #to-top.dark
    {
        background-color: rgba(0,0,0,0.87);
    }
    
    [class^="icon-"], [class*=" icon-"]
    {
        background-color: #27CFC3;
        border-radius: 999px 999px 999px 999px;
        -moz-border-radius: 999px 999px 999px 999px;
        -webkit-border-radius: 999px 999px 999px 999px;
        -o-border-radius: 999px 999px 999px 999px;
        color: #fff;
        display: inline-block;
        font-size: 16px;
        height: 32px;
        line-height: 32px;
        max-width: 100%;
        position: relative;
        text-align: center;
        vertical-align: middle;
        width: 32px;
        top: -2px;
        word-spacing: 1px;
    }
    
    [class^="icon-"].icon-3x, [class*=" icon-"].icon-3x
    {
        background-color: #eeedec !important;
    }
    
    body [class^="icon-"].icon-3x.alt-style, body [class*=" icon-"].icon-3x.alt-style
    {
        background-color: #27CFC3 !important;
        color: #fff !important;
    }
    
    .icon-angle-up:before
    {
        content: "\f01b";
        font-family: "Fontawesome";
        font-style: normal;
        font-size: 25px !important;
    }
    
    [class^="icon-"]:before, [class*=" icon-"]:before
    {
        text-decoration: inherit;
        display: inline-block;
        speak: none;
    }
</style>
<!-- content start -->
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" OnNavigate="ScriptManager1_Navigate">
</asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerPedidoUserControl" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadListView1">
        </telerik:AjaxSetting>
    </AjaxSettings>
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="PrintCurentView">
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">
        function onRequestStart(ajaxManager, eventArgs) {
            if (eventArgs.EventTarget.indexOf("PrintCurentView") != -1) {
                eventArgs.EnableAjax = false;
            }

        }
    </script>
</telerik:RadCodeBlock>
<%--telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

    <script type="text/javascript">

        function radCombobox_onLoad(sender) {

            sender._oSelectItemOnBlur = sender._selectItemOnBlur;

            sender._selectItemOnBlur = function (e) {

                if (!this.get_enableLoadOnDemand())

                    this._oSelectItemOnBlur(e);

            };

        }

    </script>

</telerik:RadScriptBlock --%>
<%-- Client Side Binding 
<%-- 
<script type="text/javascript" language="javascript">
    var loading = false;
    $(window).scroll(function () {
        // Modify to adjust trigger point. You may want to add content
        // a little before the end of the page is reached. You may also want
        // to make sure you can't retrigger the end of page condition while
        // content is still loading.
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            if (($('#currentPos').data("end") === false) && (!loading)) {
                loading = true;
                JuliaClients.Service.getArticles($('#currentPos').data('currentpos'), 64, '<%= this.currentUsername %>');
                loading = false;
            }
        }
    });
    //El Refresh del carrito se lo carga...
    $(document).ready(function () {
        JuliaClients.Service.getArticles($('#currentPos').data('currentpos'), 64, '<%= this.currentUsername %>');
    });
    /*function pageLoad() {
        $(function () {
            JuliaClients.Service.getArticles($('#currentPos').data('currentpos'), 64, '<%= this.currentUsername %>');
        });*/

    
</script>


<span id="tblStaff"></span>

<input type="button" value="Load Articles" onclick="javascript:JuliaClients.Service.getArticles($('#currentPos').data('currentpos'),64, '<%= this.currentUsername %>');return false;" />
<input type="button" value="Load Order Lines" onclick="javascript:JuliaClients.Service.getOrderLines(0,5);return false;" />


<span id="currentPos" data-currentpos="0" data-end="false"></span>
<input type="hidden" id="currentItemQtty" value="" />
--%>
<div class="NewGroupProductsCatalogTitle">
    <div class="headerText">
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SeeResultsFor") %>:</div>
    <div class="NewtxtTitleCatalog" style="display: none;">
        <asp:Label ID="titleCatalog" runat="server"></asp:Label>
    </div>
    <div id="Div0" class="NewFilterCatalog" style="display: none;">
        <telerik:RadComboBox ID="Catalog" Visible="false" runat="server" Width="200px" Skin="Silk"
            DataTextField="value" CssClass="Filter_Catalog" AutoPostBack="true" DataValueField="id"
            EmptyMessage="Selecciona un Catalogo" />
        <%-- OnSelectedIndexChanged="CatalogSelectedIndexChanged" />--%>
    </div>
    <div class="clearFloat">
    </div>
    <div id="Div4" class="NewFilterCatalog">
        <div>
            <telerik:RadTextBox ID="SearchProduct" runat="server" AutoPostBack="false" Skin="Silk"
                Width="175px" CssClass="RadComboBox RadComboBox_Default" OnClientLoad="onLoadTreeView">
                <ClientEvents OnKeyPress="sendTextSearch" />
            </telerik:RadTextBox>
            <asp:ImageButton ID="ibSearchProduct" OnClick="ibSearchProduct_Click" CssClass="NewimgProductsCatalogSearch"
                ImageUrl="/Style%20Library/Julia/img/icona_buscador.PNG" runat="server" onmouseover="this.src=this.src.replace('icona_buscador','icona_buscador_on');"
                onmouseout="this.src=this.src.replace('icona_buscador_on','icona_buscador');" />
        </div>
    </div>
    <div id="Div1" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Familia" Visible="false" runat="server" Width="200px" Skin="Silk"
            DataTextField="value" AutoPostBack="true" DataValueField="id" OnSelectedIndexChanged="FamiliaSelectedIndexChanged"
            EnableTextSelection="true" MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })" />
    </div>
    <div id="Div2" class="NewFilterCatalog">
        <telerik:RadComboBox ID="Subfamilia" Visible="false" runat="server" Width="200px"
            Skin="Silk" DataTextField="value" AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona una Subfamilia"
            OnSelectedIndexChanged="SubFamiliaSelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })" />
    </div>
    <div id="Div8" class="NewFilterCatalog" style="width: 100%">
        <div>
            <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectCategory") %>:</div>
        <telerik:RadTreeView RenderMode="Lightweight" ID="CategoriaTreeView" runat="server"
            Style="white-space: normal;" OnNodeClick="NodeClick" Skin="Silk" OnClientLoad="onLoadTreeView">
            <DataBindings>
                <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
            </DataBindings>
        </telerik:RadTreeView>
    </div>
    <div id="Div10" class="NewFilterCatalog" style="width: 100%">
        <telerik:RadComboBox ID="Color" runat="server" Width="200px" Skin="Silk" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Color" OnSelectedIndexChanged="ColorSelectedIndexChanged"
            EnableTextSelection="true" MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            OnClientLoad="onLoadTreeView" />
    </div>
    <div id="Div3" class="NewFilterCatalog" style="width: 100%">
        <telerik:RadComboBox ID="Programa" runat="server" Width="200px" Skin="Silk" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Programa"
            OnSelectedIndexChanged="ProgramaSelectedIndexChanged" EnableTextSelection="true"
            MarkFirstMatch="true" Filter="Contains" OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
            OnClientLoad="onLoadTreeView" />
    </div>
    <div id="divFiltroDto" style="float: left; width: 100%; padding-bottom:1rem">
        <telerik:RadComboBox ID="DtoExclusivo" runat="server" Width="200px" Skin="Silk" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Dto" OnSelectedIndexChanged="DtoSelectedIndexChanged" />
    </div>
    <div id="divProductoExEs" style="float: left; width: 100%;">
        <telerik:RadComboBox ID="ProductoExEs" runat="server" Width="200px" Skin="Silk" DataTextField="value"
            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Tipo de Producto" OnSelectedIndexChanged="ProductoExEsSelectedIndexChanged" />
    </div>
    <div style="margin-bottom: 0.7rem; margin-top: 0.8rem; float: left; width: 100%">
        <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip2" runat="server" OffsetY="20"
            Position="TopCenter" ShowCallout="false" ShowEvent="FromCode" HideEvent="FromCode">
        </telerik:RadToolTip>
        <telerik:RadSlider RenderMode="Lightweight" ID="RadSlider_Price" runat="server" ItemType="tick"
            AnimationDuration="400" CssClass="TicksSlider" ThumbsInteractionMode="Free" IsSelectionRangeEnabled="true"
            OnClientValueChanged="OnClientValueChanged" OnClientSlideStart="OnClientSlideStart"
            OnClientSlide="OnClientSlide" OnClientSlideEnd="OnClientSlideEnd" OnClientSlideRangeStart="OnClientSlideRangeStart"
            OnClientSlideRange="OnClientSlideRange" OnClientSlideRangeEnd="OnClientSlideRangeEnd"
            Height="45px" TrackPosition="TopLeft" EnableDragRange="true" ShowDecreaseHandle="false"
            ShowIncreaseHandle="false" Skin="Silk" OnClientLoad="onLoadTreeView" OnValueChanged="RadSlider_ValueChanged"
            AutoPostBack="true" SmallChange="1">
        </telerik:RadSlider>
        <input id="minSlider" type="text" runat="server" value="" class="InputHidden" />
        <input id="maxSlider" type="text" runat="server" value="" class="InputHidden" />
        <div class="module">
            <asp:Label ID="LabelSelectionStart" runat="server" Text="None" />
            -
            <asp:Label ID="LabelSelectionEnd" runat="server" Text="None" />
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function getTooltip() {
                    return $find("<%= RadToolTip2.ClientID %>");
                }
            </script>
        </telerik:RadCodeBlock>
    </div>
    <asp:ImageButton ID="ibClearFilter" Style="display: none" runat="server" CssClass="ClearFilter"
        OnClick="Clear_Filter_Click" />
    <div id="deleteAll" class="deleteButton clearFilterImg" runat="server" onclick="filterClick()">
        <span>
            <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteFilters")%>
        </span>
    </div>
</div>
<div class="ProductGridContainer">
    <telerik:RadListView ID="RadListView1" runat="server" ItemPlaceholderID="PlaceHolder1"
        OnItemCommand="RadListView1_ItemCommand" AllowPaging="true" OnLoad="RadListView1_Load"
        OnPageIndexChanged="RadListView1_PageIndexChanged" OnDataBound="RadListView1_DataBound"
        AllowCustomPaging="true">
        <%-- EnableViewState="false" --%>
        <LayoutTemplate>
            <div class="CatalogPaginationTop">
                <div class="CatalogNumberPagination" style="display: inline-block;">
                    <telerik:RadDataPager ID="RadDataPager2" CssClass="RadDataPager" PageSize="<%# PageSize %>"
                        runat="server" OnFieldCreated="RadDataPager_FieldCreated" RenderMode="Lightweight"
                        Skin="Metro">
                        <ClientEvents OnPageIndexChanging="PageIndexChanging" />
                        <Fields>
                            <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                            <telerik:RadDataPagerButtonField FieldType="NextLast" />
                            <telerik:RadDataPagerGoToPageField />
                            <telerik:RadDataPagerPageSizeField />
                        </Fields>
                    </telerik:RadDataPager>
                    <div style="float: right; width: 14rem; margin-left: 3rem;">
                        <telerik:RadComboBox ID="rbOrden" runat="server" CssClass="PagOrder" DataTextField="value"
                            AutoPostBack="true" DataValueField="id" EmptyMessage="Selecciona un Orden" OnSelectedIndexChanged="OrdenSelectedIndexChanged"
                            RenderMode="Lightweight" Skin="Metro" />
                        <asp:ImageButton ID="PrintCurentView" OnClick="PrintView_Click" ImageUrl="/Style%20Library/Julia/img/imprimir.png"
                            CssClass="ClearFilter printView" runat="server" onmouseover="this.src=this.src.replace('imprimir','imprimir_on');"
                            onmouseout="this.src=this.src.replace('imprimir_on','imprimir');" />
                    </div>
                </div>
                <!-- div class="clearFloat">
            </div
-->
            </div>
            <div class="contenedorProductos">
                <asp:Panel ID="productPanel" runat="server">
                    <div>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </div>
                </asp:Panel>
            </div>
            <div class="clearFloat">
            </div>
            <div class="CatalogPagination">
                <div class="CatalogNumberPagination CatalogNumberPaginationBottom">
                    <telerik:RadDataPager ID="RadDataPager1" runat="server" OnFieldCreated="RadDataPager_FieldCreated"
                        BackColor="White" RenderMode="Lightweight" Skin="Metro">
                        <ClientEvents OnPageIndexChanging="PageIndexChanging" />
                        <Fields>
                            <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                            <telerik:RadDataPagerButtonField FieldType="NextLast" />
                            <telerik:RadDataPagerGoToPageField />
                            <telerik:RadDataPagerPageSizeField />
                        </Fields>
                    </telerik:RadDataPager>
                </div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="NewitemProduct
Container">
                <div id="imgProducto1">
                    <a href='<%# Eval("NavigateUrl") %>'>
                        <asp:Panel ID="ImgDiscount" runat="server" Width="66px" Height="60px" CssClass="ImgDiscountPos"
                            Visible='<%#Eval("ShowDiscount")%>' BackImageUrl='<%# Eval("ImgDiscountUrl") %>'>
                            <span class="Label_Discount">
                                <%# Eval("DiscountLabel") %></span>
                        </asp:Panel>
                        <asp:Panel ID="ImgNewProduct" runat="server" BackImageUrl="/Style Library/Julia/img/NewProduct.png"
                            Width="225px" Height="225px" CssClass="ImgNewProductPos" Visible='<%#Eval("NewProduct")%>'>
                        </asp:Panel>
                        <img src="<%# Eval("ImageUrl") %>" id="<%# Eval("Code") %>" class="NewimgProduct"
                            onerror="<%# Eval("ImgErrorUrl") %>" />
                    </a>
                </div>
                <div class="bloq_info_front">
                    <div class="NewtxtProductRef">
                        <asp:Literal runat="server" Text='<%#Eval("Code")%>' ID="productCode"></asp:Literal>
                        <asp:Literal runat="server" Text='<%#Eval("productVersion")%>' ID="productVersion"
                            Visible="false"></asp:Literal>
                        <asp:Literal runat="server" Text='<%#Eval("productInsertCatalogNo")%>' ID="productInsertCatalogNo"
                            Visible="false"></asp:Literal>
                    </div>
                    <div style="width: 48%; float: right; height: 1.5rem;">
                        <i class="fa fa-heart-o addToWishlistIcon" id="searchHeart-<%# Eval("Code") %>" onclick="addToWishlist(<%# Eval("EANcode")
%>, '<%# Eval("Code") %>', '<%# Eval("CatalogNo") %>')"></i>
                    </div>
                    <div class="NewtxtProductName">
                        <%#Eval("Description")%></div>
                    <div class="NewtxtProductPrice">
                        <asp:Panel ID="Panel_Price" runat="server" CssClass="Price_Panel">
                            <asp:Label ID="Label_Price" Text='<%#Eval("Price")%>' runat="server"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="Panel_PriceWithDiscount" runat="server" CssClass="PriceWithoutDiscount_Panel">
                            <asp:Label ID="Label_Discount" Text='<%#Eval("PriceWithDiscount")%>' runat="server"></asp:Label>
                        </asp:Panel>
                    </div>
                    <div class="NewtxtProductStock">
                        <span class="stock_lbl">Stock:</span>
                        <%-- STOCK
FOR REPRESENTANTES y CLIENTGROUP --%>
                        <div id="Div5" class="DivStockImages" runat="server" visible='<%#Eval("VisibleStockControlRepresentante")%>'>
                            <label runat="server" visible='<%#Eval("VisibleStockNoNull")%>' class="LblCantdidades">
                                <div class="StockBox" runat="server" visible='<%#Eval("VisibilidadCantidadVerde")%>'>
                                    <%#Eval("CantidadVerde")%>&nbsp;<div class="gstock">
                                    </div>
                                </div>
                                <div class="StockBox" visible='<%#Eval("VisibilidadCantidadAzul")%>' runat="server">
                                    <%#Eval("CantidadAzul")%>&nbsp;<div class="astock">
                                    </div>
                                </div>
                                <div class="StockBox" runat="server" visible='<%#Eval("VisibilidadCantidadRoja")%>'>
                                    <%#Eval("CantidadRoja")%>&nbsp;<div class="rstock">
                                    </div>
                                </div>
                                <div class="StockBox" runat="server" visible='<%#Eval("VisibilidadCantidadGris")%>'>
                                    <%#Eval("CantidadGris")%>&nbsp;<div class="gastock" title='<%#Eval("StockTooltipGris")%>'>
                                    </div>
                                </div>
                                <asp:Image ID="ImgStockNoNullNivelServicio" runat="server" ImageUrl='<%#Eval("ImgNivelServicio")%>'
                                    Width="19px" Height="19px" CssClass="ImgNivelServicio" Visible='<%#Eval("NivelDeServicioVIsible")%>'
                                    ToolTip='<%#Eval("ToolTipNivelServicio")%>' />
                            </label>
                            <div runat="server" visible='<%#Eval("VisibleStockNull")%>'>
                                <div class="StockBox">
                                    <div class="bstock">
                                    </div>
                                </div>
                                <asp:Image Visible='<%#Eval("NivelDeServicioVIsible")%>' ID="ImgStockNullNivelServicio"
                                    CssClass="ImgNivelServicio" Width="19px" Height="19px" ImageUrl='<%#Eval("ImgNivelServicio")%>'
                                    runat="server" ToolTip='<%#Eval("ToolTipNivelServicio")%>' />
                            </div>
                        </div>
                        <%-- STOCK FOR CLIENTES --%>
                        <div class="DivStockImages" runat="server" visible='<%#Eval("VisibleStockControlClient")%>'>
                            <asp:Image class="StockBox" runat="server" ID="ImgStock" ImageUrl='<%#Eval("Stock")%>'
                                Width="16px" Height="16px" />
                            <asp:Image class="StockBox" ID="ImgNivelDeServicioClient" CssClass="ImgNivelServicio"
                                ImageUrl='<%#Eval("ImgNivelServicio")%>' Visible='<%#Eval("NivelDeServicioVIsible")%>'
                                runat="server" Width="19px" Height="19px" />
                            <telerik:RadToolTip runat="server" ID="RadToolTip1" TargetControlID="ImgStock" IsClientID="false"
                                ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight" Width="100px"
                                RelativeTo="Mouse" Animation="None">
                                <asp:Label ID="LblStock" Text='<%#Eval("StockTooltip")%>' Width="160px" runat="server" />
                            </telerik:RadToolTip>
                            <telerik:RadToolTip runat="server" ID="RadToolTipClientNivelServicio" TargetControlID="ImgNivelDeServicioClient"
                                IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                Width="100px" RelativeTo="Mouse" Animation="None">
                                <asp:Label ID="LblToolClientlServicio" Text='<%#Eval("ToolTipNivelServicio")%>' Width="160px"
                                    runat="server" />
                            </telerik:RadToolTip>
                        </div>
                    </div>
                </div>
                <div class="Contenido">
                    <div id='<%#Eval("CodeWS")%>'>
                        <div class="MeasuresBox">
                            <span style="display: none;">
                                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMeasures")%></span>
                            <table cellspacing="0" cellpadding="0" class="tableMeasures">
                                <tr>
                                    <td>
                                        <div class="pheight">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="pwidth">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="pdepth">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="ValuestableMeasuresTr">
                                    <td>
                                        <%#Eval("Height")%>
                                    </td>
                                    <td>
                                        <%#Eval("Width")%>
                                    </td>
                                    <td>
                                        <%#Eval("Length")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="QtyAndBtn">
                            <div class="NewitemQty">
                                <div class="Qty_input">
                                    <%-- telerik:RadNumericTextBox
ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true"
MaxLength="3" runat="server" ID="itemQty" Width="50px" MinValue="1" MaxValue="999"
NumberFormat-DecimalDigits="0" Value='<%# Convert.ToDouble(Eval("UnidadMedida"))
%>' IncrementSettings-Step='<%# Convert.ToDouble(Eval("UnidadMedida")) %>' / --%>
                                    <input class="QttyNum" id="itemQty" value='<%# Convert.ToDouble(Eval("UnidadMedida"))
%>' step='<%# Convert.ToDouble(Eval("UnidadMedida")) %>' runat="server" />
                                </div>
                                <%--asp:RequiredFieldValidator ID="itemQtyValidator" runat="server" ErrorMessage="*
Required" ControlToValidate="itemQty"></asp:RequiredFieldValidator --%>
                            </div>
                            <div class="Btn_AddToCar">
                                <asp:LinkButton ID="Add" CommandName="AddItemToCart" runat="server" CssClass="NewimgProductAdd" />
                                <%-- BorderStyle="None" BackColor="Transparent" ImageUrl="<%$SPUrl:~sitecollection/Style
Library/Julia/img/~language/add_to_car.gif%>" /--%>
                            </div>
                        </div>
                        <div id="Div6" class="PanelColor" visible='<%#Eval("ShowAgrupColor")%>' runat="server">
                            <asp:Literal runat="server" ID="ColorsAgrup" Text='<%#Eval("ColorsAgrup")%>'></asp:Literal>
                        </div>
                        <div id="Div7" class="PanelMeasure" visible='<%#Eval("ShowAgrupMeasures")%>' runat="server">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("MeasuresAgrupLink")
%>' Text='<%#Eval("MeasuresAgrupText") %>'> </asp:HyperLink>
                        </div>
                    </div>
                </div>
                <%--<div class="NewBtnAdd"> <asp:HyperLink ID="btAddProd" CssClass="NewimgProductAdd"
NavigateUrl='<%#Eval("NavigateUrl") %>' BorderStyle="None" BackColor="Transparent"
runat="server" /> </div>--%>
            </div>
        </ItemTemplate>
    </telerik:RadListView>
</div>
<%--<a id="to-top" style="bottom: 17px; display: none;" onclick="$(window).scrollTo(0, 500); return false;"
    href="javascript:totop();"><i class="icon-angle-up"></i></a>--%>
<a id="to-top" style="bottom: 17px; display: none;" onclick="$('html, body').animate({ scrollTop: 0 }, 500);"><i class="icon-angle-up"></i></a>
<telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
    AutoCloseDelay="3000" OffsetY="-100" EnableShadow="true" Position="MiddleLeft"
    ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"
    Skin="Silk" Text="Producto Añadido">
</telerik:RadNotification>
<asp:Literal ID="RefreshLiteral" runat="server"></asp:Literal>
<asp:Literal ID="ProductEffectsLiteral" runat="server"></asp:Literal>
<asp:HiddenField ID="qttNumber" runat="server" Value="0" />
<asp:HiddenField ID="codeProduct" runat="server" Value="0" />
<input type="hidden" id="estance" value="<%= this.estance %>" />
<input type="hidden" id="style" value="<%= this.style %>" />
<asp:Timer ID="Timer1" runat="server" Interval="100" OnTick="Timer1_Tick" Enabled="false" />
