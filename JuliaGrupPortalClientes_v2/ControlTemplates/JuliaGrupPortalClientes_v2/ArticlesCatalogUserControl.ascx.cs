﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using System.Data;
using System.Linq;
using Telerik.Web.UI;
using JuliaGrupUtils;
using Microsoft.SharePoint;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using Microsoft.SharePoint.WebPartPages;
using JuliaGrupPortalClientes_v2.Web_Parts.Carrito;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using JuliaGrupUtils.ErrorHandler;
using System.IO;
using System.Web;
using System.Web.UI.HtmlControls;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class ArticlesCatalogUserControl : UserControl
    {
        private static int _defaultPageSize = 64;  //64;
        public int PageSize = _defaultPageSize;
        public User currentUser;
        Client currentClient;
        Order currentOrder;
        List<Article> Products = new List<Article>();
        List<Article> currentArticles = new List<Article>();
        ArticleDataAccessObject articleDAO = null;
        int min = 0;
        int max = 0;

        private bool getCachedData = false;

        string fCatalog = String.Empty;
        string fFamily = String.Empty;
        string fSubFamily = String.Empty;
        string fProgram = String.Empty;
        string fSearchTxt = String.Empty;
        string fOrden = String.Empty;
        string fDto = String.Empty;
        int fPageNo = 0;
        string fCategory = String.Empty;
        string fProductoExEs = String.Empty;
        string fEstance = String.Empty;
        string fStyle = String.Empty;
        string fColor = String.Empty;
        decimal fGlobalMinPrice = 0;
        decimal fGlobalMaxPrice = 9999;
        decimal fMinPrice = 0;
        decimal fMaxPrice = 0;

        internal class TreeViewDataItem
        {
            private string text;
            private int id;
            private int parentId;
            private string valueCode;

            public string Text
            {
                get { return text; }
                set { text = value; }
            }
            public int ID
            {
                get { return id; }
                set { id = value; }
            }
            public int ParentID
            {
                get { return parentId; }
                set { parentId = value; }
            }
            public string ValueCode
            {
                get { return valueCode; }
                set { valueCode = value; }
            }
            public TreeViewDataItem(int id, int parentId, string text, string valueCode)
            {
                this.id = id;
                this.parentId = parentId;
                this.text = text;
                this.valueCode = valueCode;
            }
        }

        public string currentUsername
        {
            get
            {
                LoadCurrentUserandClientData();
                return currentUser.UserName;
            }
        }

        public string currentOrderNo
        {
            get
            {
                LoadCurrentUserandClientData();
                return currentOrder.NavOferta.No;
            }
        }

        public string currentClientCode
        {
            get
            {
                LoadCurrentUserandClientData();
                return currentClient.Code;
            }
        }

        public string estance
        {
            get
            {
                return (Session["fEstance"] != null) ? Session["fEstance"].ToString() : String.Empty;
            }
        }

        public string style
        {
            get
            {
                return (Session["fStyle"] != null) ? Session["fStyle"].ToString() : String.Empty;
            }
        }

        public void Refresh()
        {
            try
            {
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                //LoadFilters();

                this.Timer1.Enabled = false;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void Timer1_Tick(object sender, EventArgs e)
        {
            //return;   //Disable Refresh for Cliet Side Binding
            try
            {
                this.Refresh();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected override void CreateChildControls()
        {

            //return;   //Disable Refresh for Cliet Side Binding

            try
            {
                if (Session["ItemsPerPage"] != null)
                {
                    this.PageSize = string.IsNullOrEmpty(Session["ItemsPerPage"].ToString()) ? _defaultPageSize : int.Parse(Session["ItemsPerPage"].ToString());
                }
                if (!this.Page.IsPostBack)
                {
                    //this.RadListView1.DataSource = LoadData(PageSize);
                    //this.RadListView1.DataSource = LoadData();
                    //this.RadListView1.CurrentPageIndex = 0;
                    //this.RadListView1.DataBind();
                    //LoadFilters();

                    //this.Timer1.Enabled = true;   //Disabled
                    RadComboBox rbOrden = this.RadListView1.Controls[0].FindControl("rbOrden") as RadComboBox;

                    rbOrden.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrder");
                    rbOrden.Items.Add(new RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrderDefaultOrder"), "0"));
                    RadComboBoxItem item1 = new RadComboBoxItem();
                    item1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrderPriceAsc");
                    item1.Value = "1";
                    rbOrden.Items.Add(item1);
                    RadComboBoxItem item2 = new RadComboBoxItem();
                    item2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrderPriceDesc");
                    item2.Value = "2";
                    rbOrden.Items.Add(item2);
                    RadComboBoxItem item3 = new RadComboBoxItem();
                    item3.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrderStockAsc");
                    item3.Value = "3";
                    rbOrden.Items.Add(item3);
                    RadComboBoxItem item4 = new RadComboBoxItem();
                    item4.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectOrderStockDesc");
                    item4.Value = "4";
                    rbOrden.Items.Add(item4);

                    if (Session["prodOrder"] != null)
                    {
                        rbOrden.SelectedValue = Session["prodOrder"].ToString();
                    }
                    this.ProductoExEs.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProExEs");
                    //REZ 25072014 - Dto Exclusivo
                    this.DtoExclusivo.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectDto");
                    //DtoExclusivo.Items.Add(new RadComboBoxItem("", ""));                    
                    RadComboBoxItem ditem1 = new RadComboBoxItem();
                    ditem1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectDtoNone");
                    ditem1.Value = "";
                    DtoExclusivo.Items.Add(ditem1);
                    if (Products.Where(p => p.TieneDtoExclusivo == true).Count() > 0)
                    {
                        RadComboBoxItem ditem4 = new RadComboBoxItem();
                        ditem4.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectDtoTodos");
                        ditem4.Value = "3";
                        DtoExclusivo.Items.Add(ditem4);

                        RadComboBoxItem ditem2 = new RadComboBoxItem();
                        ditem2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectDtoExclusive");
                        ditem2.Value = "1";
                        DtoExclusivo.Items.Add(ditem2);
                    }

                    RadComboBoxItem ditem3 = new RadComboBoxItem();
                    ditem3.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectDtoGeneral");
                    ditem3.Value = "2";
                    DtoExclusivo.Items.Add(ditem3);

                    if (Session["DtoExclusivo"] != null)
                    {
                        DtoExclusivo.SelectedValue = Session["DtoExclusivo"].ToString();
                    }


                    this.titleCatalog.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("catalog");
                    //this.RadListView1.DataBind();
                    ArticleDataAccessObject articleDAO = Session["ArticleDAO"] as ArticleDataAccessObject;

                    if (articleDAO != null)
                        SelectCatalog(articleDAO);
                }
                else
                {
                    if (!Page.IsCallback)
                    {

                        //this.RadListView1.DataSource = LoadData();
                        ////ReLoadVariableFilters();
                        //this.RadListView1.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                LoadCurrentUserandClientData();

                string codCatalog = Request.QueryString["catalog"];
                string familia = this.Familia.SelectedValue;
                string subfamilia = this.Subfamilia.SelectedValue;
                //===========================================================================
                string programa = this.Programa.SelectedValue;
                string color = this.Color.SelectedValue;
                string categoria = this.CategoriaTreeView.SelectedValue;
                /*string programa = this.ProgramTreeView.SelectedValue;                
                string color = this.ColorTreeView.SelectedValue;*/
                //===========================================================================
                int minPrice = (int)this.RadSlider_Price.SelectionStart;
                int maxPrice = (int)this.RadSlider_Price.SelectionEnd;
                string txtSearch = this.SearchProduct.Text;

                if ((string.IsNullOrEmpty(codCatalog)))
                {
                    codCatalog = string.Empty;
                }

                //REZ 25/03/2013 : Cambiamos currentUser.Code por currentUser.Username...
                //output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=informevista&UserCode=" + currentUser.Code + "&ClientCode=" + currentClient.Code + "&codCatalog=" + codCatalog + "&familia=" + familia + "&subfamilia=" + subfamilia + "&programa=" + programa + "&filtre=" + txtSearch + "&Unidad=" + Session["PriceSelector"];
                string strDto = (Session["DtoExclusivo"] != null) ? Session["DtoExclusivo"].ToString() : "";
                string strProExEs = this.ProductoExEs.SelectedValue;
                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=informevista&UserCode=" + currentUser.UserName + "&ClientCode=" + currentClient.Code + "&codCatalog=" + codCatalog + "&familia=" + familia + "&subfamilia=" + subfamilia + "&programa=" + programa + "&categoria" + categoria + "&price" + minPrice + "_" + maxPrice + "&filtre=" + txtSearch + "&Unidad=" + Session["PriceSelector"].ToString() + "&Dto=" + strDto + "&ProExEs=" + strProExEs;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return output;
        }
        private void SelectCatalog(ArticleDataAccessObject articleDAO)
        {
            try
            {
                string catalog = Request.QueryString["catalog"];

                if (!string.IsNullOrEmpty(catalog))
                {
                    catalog = catalog.Replace("%20", " ");
                    string catalogname = articleDAO.GetCatalogName(catalog);
                    if (!string.IsNullOrEmpty(catalogname))
                    {
                        RadComboBoxItem item = this.Catalog.FindItemByValue(catalogname);
                        if (item != null)
                            item.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Inicialice();

            //EnsureChildControls();
            //ProductEffectsLiteral.Text = @"<script type=""text/javascript""> ProductEffects(); </script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ClientID, "ProductEffects();", true);
        }

        private void Inicialice()
        {
            //Default Values from filters
            this.Familia.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectFamily");
            this.Subfamilia.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectSubFamily");
            //===========================================================================
            this.Programa.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProgram");
            this.Color.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectColor");
            //===========================================================================
            //TODO: Falta Price

            if (Session["ArticleDAO"] != null)
            {
                articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
            }
            Products = articleDAO.GetAllProducts();
            this.currentArticles = Session["Articles"] as List<Article>;
            this.ProductoExEs.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectProExEs");
            //REZ 25072014 - Filtros exclusivos. Si hay algun item con filtro exclusivo mostramos el panel de filtros
            //if (Products.Where(p => p.TieneDtoExclusivo == true).Count() > 0){
            //    this.divFiltroDto.Visible = true;
            //}
        }

        public void OrdenSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Session["prodOrder"] = e.Value;
            getCachedData = false;
            this.RadListView1.CurrentPageIndex = 0;
            SetHistoryPoint();
        }

        //REZ 25072014 - 
        public void DtoSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Session["DtoExclusivo"] = e.Value;
            getCachedData = false;
            this.RadListView1.CurrentPageIndex = 0;
            SetHistoryPoint();
        }

        public void RadListView1_PageIndexChanged(object sender, RadListViewPageChangedEventArgs e)
        {
            LoadCurrentUserandClientData();
            //getCachedData = true;

            getCachedData = false;

            //DataTable cachedArticles = (DataTable)HttpContext.Current.Cache.Get("ArticlesCatalog_Paging_cache_" + currentUser.UserName + currentClient.Code);
            ////DataTable selectedArticles;
            //if (cachedArticles == null)
            //{
            //    cachedArticles = LoadData();
            //}

            //selectedArticles = GetSubData(cachedArticles, e.NewPageIndex, this.PageSize);   //Solo funciona con AllowCustomPaging, de momento no lo usamos
            //this.RadListView1.DataSource = selectedArticles;
            //this.RadListView1.VirtualItemCount = cachedArticles.Rows.Count;

            //Sin allowCustomPaging. Parece que no afecta tanto al rendimiento
            //this.RadListView1.DataSource = cachedArticles;
            //this.RadListView1.DataBind();
            SetHistoryPoint(e.NewPageIndex);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "ScrollPage", "ResetScrollPosition();", true);
        }

        private DataTable GetSubData(DataTable sourceData, int pageindex, int pagesize)
        {
            DataTable output = sourceData.Clone();

            int iniPos = pageindex * pagesize;
            int endPos = iniPos + pagesize;
            int sourceCount = sourceData.Rows.Count;
            endPos = (sourceCount < endPos) ? sourceCount : endPos;

            for (int i = iniPos; i < endPos; i++)
            {
                output.ImportRow(sourceData.Rows[i]);
            }
            return output;
        }

        //public void SearchProduct_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.RadListView1.DataSource = LoadData();
        //        this.RadListView1.CurrentPageIndex = 0;
        //        this.RadListView1.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        public void ibSearchProduct_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Session["fSearchTxt"] = this.SearchProduct.Text;
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();

                SetHistoryPoint();

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void ClearFilters()
        {
            Session.Remove("fSearchTxt");
            Session.Remove("fFamily");
            Session.Remove("fSubFamily");
            Session.Remove("fProgram");
            Session.Remove("fPageNo");
            Session.Remove("fCatalog");
            Session.Remove("prodOrder");
            Session.Remove("DtoExclusivo");
            Session.Remove("fCategory");
            Session.Remove("fEstance");
            Session.Remove("fStyle");
            Session.Remove("fColor");
            Session.Remove("fProductoExEs");
            Session.Remove("fPrice");
            Session.Remove("fMinPrice");
            Session.Remove("fMaxPrice");

            this.Familia.ClearSelection();
            this.Subfamilia.ClearSelection();
            //===========================================================================
            this.Programa.ClearSelection();
            this.Color.ClearSelection();
            this.CategoriaTreeView.UnselectAllNodes();
            this.ProductoExEs.ClearSelection();
            /*this.ProgramTreeView.UnselectAllNodes();            
            this.ColorTreeView.UnselectAllNodes();*/
            //===========================================================================
            this.RadSlider_Price.SelectionStart = this.RadSlider_Price.MinimumValue;
            this.RadSlider_Price.SelectionEnd = this.RadSlider_Price.MaximumValue;
            this.LabelSelectionStart.Text = this.RadSlider_Price.MinimumValue.ToString();
            this.LabelSelectionEnd.Text = this.RadSlider_Price.MaximumValue.ToString();
            this.minSlider.Value = "";
            this.maxSlider.Value = "";
            this.SearchProduct.Text = "";
            this.RadListView1.CurrentPageIndex = 0;
            RadComboBox rbOrden = this.RadListView1.Controls[0].FindControl("rbOrden") as RadComboBox;
            rbOrden.ClearSelection();
            this.DtoExclusivo.ClearSelection();
        }
        public void Clear_Filter_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ClearFilters();

                //Response.Redirect("/Pages/Products.aspx", true);
                //Familia.ClearSelection();

                //Subfamilia.ClearSelection();
                //Programa.ClearSelection();


                //SearchProduct.Text = "";

                //this.RadListView1.DataSource = LoadData();
                //this.RadListView1.DataBind();

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void RadListView1_Load(object sender, EventArgs e)
        {

        }

        private void LoadFilters()  //List<Article> currentArticles)
        {
            try
            {
                //
                if (currentArticles != null)
                {
                    //string codCatalog = Request.QueryString["catalog"];
                    //Si no filtramos por un catalogo, no mostraremos productos de Inspirate ni de Promociones
                    //if (string.IsNullOrEmpty(codCatalog))   
                    //{
                    //    articles = (from p in articles where !p.CatalogType.Equals("Inspirate") select p).ToList();
                    //    articles = (from p in articles where !p.CatalogType.Equals("Promociones") select p).ToList();
                    //}
                    //load catalogs

                    //***El combo de cataleg esta ocult ja que es filtra a traves de les fotos per querystring***//
                    //var catalogs = (from p in articles
                    //                orderby p.Catalog
                    //                select p.Catalog).Distinct();

                    //foreach (string catalog in catalogs)
                    //{
                    //    var currcatalog = articles.First(i => i.Catalog.Equals(catalog));
                    //    this.Catalog.Items.Add(new RadComboBoxItem(currcatalog.Catalog, currcatalog.CatalogNo));
                    //}

                    //load families


                    var families = (from p in currentArticles
                                    orderby p.Family
                                    select p.Family).Distinct();


                    /* 2019-06-04 - Aida Lucha --- no es fa servir
                        var catalogtype = (from p in currentArticles
                                           orderby p.CatalogType
                                           select p.CatalogType).Distinct();
                    */
                    //string FamilyFilterOldValue = this.Familia.SelectedValue;
                    string FamilyFilterOldValue = fFamily;
                    this.Familia.Items.Clear();
                    this.Familia.Items.Add(new RadComboBoxItem("", ""));
                    foreach (string family in families)
                    {
                        this.Familia.Items.Add(new RadComboBoxItem(family, family));
                    }
                    this.Familia.SelectedValue = FamilyFilterOldValue;
                    //this.Familia.Items.Clear();


                    //var subFamilies = (from p in currentArticles
                    //                   orderby p.SubFamily
                    //                   select p.SubFamily).Distinct();

                    //this.Subfamilia.Items.Clear();  //Que pasa si tinc familia seleccionada?
                    //foreach (string subfamilia in subFamilies)
                    //{
                    //    this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                    //}

                    //===========================================================================
                    //load Categoria                    
                    SetCategoriesTreeView();
                    /*//load Programa
                    SetProgramTreeView();                    
                    //load Color
                    SetColorTreeView();*/
                    //load Programa                    
                    var Programas = (from p in currentArticles
                                     orderby p.Programa
                                     select p.Programa).Distinct();

                    //string ProgramaFilterOldValue = this.Programa.SelectedValue;
                    string ProgramaFilterOldValue = fProgram;
                    this.Programa.Items.Clear();
                    this.Programa.Items.Add(new RadComboBoxItem("", ""));
                    foreach (string programa in Programas)
                    {
                        this.Programa.Items.Add(new RadComboBoxItem(programa, programa));
                    }
                    this.Programa.SelectedValue = ProgramaFilterOldValue;

                    //load Color
                    var Colors = (from p in currentArticles
                                  orderby p.Color
                                  select p.Color).Distinct();

                    //string ProgramaFilterOldValue = this.Programa.SelectedValue;
                    string ColorFilterOldValue = fColor;
                    this.Color.Items.Clear();
                    this.Color.Items.Add(new RadComboBoxItem("", ""));
                    foreach (string color in Colors)
                    {
                        this.Color.Items.Add(new RadComboBoxItem(color, color));
                    }
                    this.Color.SelectedValue = ColorFilterOldValue;
                    //===========================================================================

                   
                    string ProductoExEsFilterOldValue = fProductoExEs;
                    this.ProductoExEs.Items.Clear();
                    this.ProductoExEs.Items.Add(new RadComboBoxItem("", ""));
                    
                    string ProductoExEs = "L";
                    string text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsEstandar");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));
                    ProductoExEs = "K";
                    text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsExclusivo");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));

                    // --> ALB 20190402 - Afegir productes Exclusivos Plus al filtre del catàleg de productes
                    ProductoExEs = "K+";
                    text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProExEsExclusivoPlus");
                    this.ProductoExEs.Items.Add(new RadComboBoxItem(text, ProductoExEs));
                    this.ProductoExEs.SelectedValue = ProductoExEsFilterOldValue;

                    //Article minPriceProd = currentArticles.OrderBy(p => p.PriceInt).FirstOrDefault();
                    //Article maxPriceProd = currentArticles.OrderBy(p => p.PriceInt).LastOrDefault();

                    //if (minPriceProd.PriceInt < fMinPrice) fMinPrice = minPriceProd.PriceInt;
                    //if (maxPriceProd.PriceInt > fMaxPrice) fMaxPrice = maxPriceProd.PriceInt;

                    /*this.MinProdPrice.Value = (minPriceProd != null) ? minPriceProd.PriceInt : 0.0;    // fMinPrice;
                    this.MaxProdPrice.Value = (maxPriceProd != null) ? maxPriceProd.PriceInt : 0.0;    //fMaxPrice;*/

                    ReLoadVariableFilters();

                    if (String.IsNullOrEmpty(this.minSlider.Value) && String.IsNullOrEmpty(this.maxSlider.Value))//!this.Page.IsPostBack)
                    {
                        this.RadSlider_Price.MinimumValue = currentArticles.OrderBy(i => i.PriceInt).Select(x => x.PriceInt).FirstOrDefault();
                        this.RadSlider_Price.MaximumValue = currentArticles.OrderByDescending(i => i.PriceInt).Select(x => x.PriceInt).FirstOrDefault();
                        this.RadSlider_Price.SelectionStart = this.RadSlider_Price.MinimumValue;
                        this.RadSlider_Price.SelectionEnd = this.RadSlider_Price.MaximumValue;
                    }
                    else
                    {
                        this.RadSlider_Price.MinimumValue = Decimal.Parse(this.minSlider.Value);
                        this.RadSlider_Price.MaximumValue = Decimal.Parse(this.maxSlider.Value);
                        this.RadSlider_Price.SelectionStart = this.RadSlider_Price.MinimumValue;
                        this.RadSlider_Price.SelectionEnd = this.RadSlider_Price.MaximumValue;
                    }
                    //this.minSlider.Value = fMinPrice.ToString();
                    //this.maxSlider.Value = fMaxPrice.ToString();
                    this.RadSlider_Price.LargeChange = Math.Ceiling((this.RadSlider_Price.MaximumValue - this.RadSlider_Price.MinimumValue) / 4);

                    //this.fGlobalMinPrice = this.RadSlider_Price.MinimumValue;
                    //this.fGlobalMaxPrice = this.RadSlider_Price.MaximumValue;
                    this.LabelSelectionStart.Text = RadSlider_Price.SelectionStart.ToString();
                    this.LabelSelectionEnd.Text = RadSlider_Price.SelectionEnd.ToString();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void BindToArray(RadListBox listbox, List<TopMenuItem> listitems)
        {
            List<string> itemsList = new List<string>();
            foreach (TopMenuItem item in listitems)
            {
                itemsList.Add(item.Descripcion);
            }
            listbox.DataSource = itemsList;
            listbox.DataBind();
        }

        private List<TopMenuItem> GetMenuItems()
        {
            List<TopMenuItem> Categorias = new List<TopMenuItem>();
            List<TopMenuItem> topMenuItems;
            var tipoCompra = 0; //LaForma

            TopMenuDAO topMenuDAO = new TopMenuDAO(this.currentUsername, this.currentClientCode, tipoCompra, this.currentUser.Language);
            topMenuItems = topMenuDAO.MenuItems;
            return topMenuItems;
        }

        private List<TopMenuItem> loadCategoryList(string type, List<TopMenuItem> topMenuItems)
        {
            List<TopMenuItem> Categorias;
            Categorias = topMenuItems.Where(p => p.Tipo == type).ToList();
            return Categorias;
        }

        //===========================================================================
        private void SetCategoriesTreeView()
        {
            List<TopMenuItem> Categorias = loadCategoryList("0", GetMenuItems());
            List<TopMenuItem> SubCategorias = loadCategoryList("1", GetMenuItems());
            List<TopMenuItem> SubsubCategorias = loadCategoryList("4", GetMenuItems());

            this.CategoriaTreeView.Nodes.Clear();
            List<TreeViewDataItem> treeViewDataItemList = new List<TreeViewDataItem>();
            foreach (TopMenuItem categoria in Categorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(categoria.Codigo.Replace(" ", "")) + 10000, 0, categoria.Descripcion, categoria.Codigo.Replace(" ", "")));
            }
            foreach (TopMenuItem subCategoria in SubCategorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(subCategoria.Codigo.Replace(" ", "")) + 10000, Int32.Parse(subCategoria.ParentCode.Replace(" ", "")) + 10000, subCategoria.Descripcion, subCategoria.Codigo.Replace(" ", "")));
            }
            foreach (TopMenuItem subsubCategoria in SubsubCategorias)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(Int32.Parse(subsubCategoria.Codigo.Replace(" ", "")) + 10000, Int32.Parse(subsubCategoria.ParentCode.Replace(" ", "")) + 10000, subsubCategoria.Descripcion, subsubCategoria.Codigo.Replace(" ", "")));
            }
            this.CategoriaTreeView.DataTextField = "text";
            this.CategoriaTreeView.DataFieldID = "id";
            this.CategoriaTreeView.DataFieldParentID = "parentId";
            this.CategoriaTreeView.DataValueField = "valueCode";
            this.CategoriaTreeView.DataSource = treeViewDataItemList;
            this.CategoriaTreeView.DataBind();

            RadTreeNode nodeCategory = this.CategoriaTreeView.FindNodeByValue(fCategory);
            if (nodeCategory != null)
            {
                nodeCategory.Selected = true;
                if (nodeCategory.Level != 0) nodeCategory.ParentNode.Expanded = true;
                else nodeCategory.Expanded = true;
                if (nodeCategory.Level == 2) nodeCategory.ParentNode.ParentNode.Expanded = true;
            }
        }

        /*private void SetColorTreeView()
        {
            var Colors = (from p in currentArticles
                          orderby p.Color
                          select p.Color).Distinct();

            this.ColorTreeView.Nodes.Clear();
            List<TreeViewDataItem> treeViewDataItemList = new List<TreeViewDataItem>();
            int cont = 1;
            foreach (String color in Colors)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(cont, 0, color, color));
                cont++;
            }
            this.ColorTreeView.DataTextField = "text";
            this.ColorTreeView.DataFieldID = "id";
            this.ColorTreeView.DataFieldParentID = "parentId";
            this.ColorTreeView.DataValueField = "valueCode";
            this.ColorTreeView.DataSource = treeViewDataItemList;
            this.ColorTreeView.DataBind();

            RadTreeNode nodeCategory = this.ColorTreeView.FindNodeByValue(fColor);
            if (nodeCategory != null) nodeCategory.Selected = true;
        }

        private void SetProgramTreeView()
        {
            var Programas = (from p in currentArticles
                             orderby p.Programa
                             select p.Programa).Distinct();

            this.ProgramTreeView.Nodes.Clear();
            List<TreeViewDataItem> treeViewDataItemList = new List<TreeViewDataItem>();
            int cont = 1;
            foreach (String programa in Programas)
            {
                treeViewDataItemList.Add(new TreeViewDataItem(cont, 0, programa, programa));
                cont++;
            }
            this.ProgramTreeView.DataTextField = "text";
            this.ProgramTreeView.DataFieldID = "id";
            this.ProgramTreeView.DataFieldParentID = "parentId";
            this.ProgramTreeView.DataValueField = "valueCode";
            this.ProgramTreeView.DataSource = treeViewDataItemList;
            this.ProgramTreeView.DataBind();

            RadTreeNode nodeCategory = this.ProgramTreeView.FindNodeByValue(fProgram);
            if (nodeCategory != null) nodeCategory.Selected = true;
        }*/
        //===========================================================================

        private void ReLoadVariableFilters()
        {
            try
            {
                List<Article> articles = this.currentArticles;//Session["Articles"] as List<Article>;
                if (articles != null)
                {
                    this.Subfamilia.Items.Clear();
                    this.Subfamilia.Items.Add(new RadComboBoxItem("", ""));
                    //load subfamilies
                    string SubFamilyFilterOldValue = fSubFamily;
                    if (this.Familia.SelectedValue != String.Empty)
                    {

                        var subFamilies = (from p in articles
                                           where p.Family == this.Familia.SelectedValue
                                           orderby p.SubFamily
                                           select p.SubFamily).Distinct();

                        foreach (string subfamilia in subFamilies)
                        {
                            this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                        }
                    }
                    else
                    {
                        var subFamilies = (from p in articles
                                           orderby p.SubFamily
                                           select p.SubFamily).Distinct();

                        foreach (string subfamilia in subFamilies)
                        {
                            this.Subfamilia.Items.Add(new RadComboBoxItem(subfamilia, subfamilia));
                        }
                    }

                    this.Subfamilia.SelectedValue = SubFamilyFilterOldValue;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void LoadCurrentUserandClientData()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                this.currentClient = (Client)Session["Client"];
            }
            if (Session["Order"] != null)
            {
                this.currentOrder = (Order)Session["Order"];
            }
        }

        public DataTable LoadData()
        {
            
            fCatalog = Request.QueryString["catalog"];
            Session["fCatalog"] = fCatalog;
            fFamily = (Session["fFamily"] != null) ? Session["fFamily"].ToString() : this.Familia.SelectedValue;
            fSubFamily = (Session["fSubFamily"] != null) ? Session["fSubFamily"].ToString() : this.Subfamilia.SelectedValue;
            //===========================================================================
            fProgram = (Session["fProgram"] != null) ? Session["fProgram"].ToString() : this.Programa.SelectedValue;
            fCategory = (Session["fCategory"] != null) ? fCategory = Session["fCategory"].ToString() : this.CategoriaTreeView.SelectedValue;
            fColor = (Session["fColor"] != null) ? Session["fColor"].ToString() : this.Color.SelectedValue;
            /*fProgram = (Session["fProgram"] != null) ? Session["fProgram"].ToString() : this.ProgramTreeView.SelectedValue;
            fColor = (Session["fColor"] != null) ? Session["fColor"].ToString() : this.ColorTreeView.SelectedValue;*/
            fProductoExEs = (Session["fProductoExEs"] != null) ? Session["fProductoExEs"].ToString() : this.ProductoExEs.SelectedValue;
            //===========================================================================
            fSearchTxt = (Session["fSearchTxt"] != null) ? Session["fSearchTxt"].ToString() : this.SearchProduct.Text;
            fPageNo = (Session["fPageNo"] != null) ? Convert.ToInt32(Session["fPageNo"].ToString()) : this.RadListView1.CurrentPageIndex;
            RadComboBox rbOrden = this.RadListView1.Controls[0].FindControl("rbOrden") as RadComboBox;
            fOrden = (Session["prodOrder"] != null) ? Session["prodOrder"].ToString() : rbOrden.SelectedValue;
            fDto = (Session["DtoExclusivo"] != null) ? Session["DtoExclusivo"].ToString() : this.DtoExclusivo.SelectedValue;
            fEstance = (Session["fEstance"] != null) ? Session["fEstance"].ToString() : String.Empty;
            fStyle = (Session["fStyle"] != null) ? Session["fStyle"].ToString() : String.Empty;
            //fMinPrice = ((Session["fMinPrice"] != null) ? int.Parse(Session["fMinPrice"].ToString()) : (int)this.RadSlider_Price.SelectionStart);
            //fMaxPrice = ((Session["fMaxPrice"] != null) ? int.Parse(Session["fMaxPrice"].ToString()) : (int)this.RadSlider_Price.SelectionEnd);
            fMinPrice = ((Session["fMinPrice"] != null) ? int.Parse(Session["fMinPrice"].ToString()) : (int)fGlobalMinPrice);
            fMaxPrice = ((Session["fMaxPrice"] != null) ? int.Parse(Session["fMaxPrice"].ToString()) : (int)fGlobalMaxPrice);

            return LoadData(fPageNo, PageSize);  //int.MaxValue);
        }
        public DataTable LoadData(int pageNo, int pageItems)
        {
            
            DataTable table = null;
            //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception("Inicio LoadData"), new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            try
            {
                this.currentArticles = new List<Article>();
                string filter = (Session["fSearchTxt"] != null) ? Session["fSearchTxt"].ToString() : this.SearchProduct.Text;
                filter = filter.Trim().ToUpper();

                table = new DataTable();
                List<string> CodRepeat = new List<string>();
                Boolean CheckFilters = false;
                string codeRepeated = string.Empty;
                List<Article> articles = Session["Articles"] as List<Article>;
                //Si no filtramos por un catalogo, no mostraremos productos de Inspirate ni de Promociones
                string codCatalog = fCatalog;   //Request.QueryString["catalog"];
                if (string.IsNullOrEmpty(codCatalog))
                {
                    //ALB 06052019 - ja no tenim catalegs 'Inspirate'
                    //articles = (from p in articles where !p.CatalogType.Equals("Inspirate") select p).ToList();
                    //REZ 19012015 - Debemos mostrar articulos de promociones también en el listado de productos
                    //articles = (from p in articles where !p.CatalogType.Equals("Promociones") select p).ToList();
                    //REZ 09022016 - Volvemos a ocultar los articulos de promociones del listado sin catalogo excepto los de liquidacion
                    articles = (from p in articles
                                where
                                    !p.CatalogType.Equals("Promociones")
                                    ||
                                    p.CatalogNo.StartsWith("PRELIQ")
                                select p).ToList();
                }

                
                if (articles != null)
                {
                    #region Add Table Columns
                    table.Columns.Add("RowId");
                    table.Columns.Add("Code");
                    table.Columns.Add("EANcode");
                    table.Columns.Add("CatalogNo");
                    table.Columns.Add("Description");
                    table.Columns.Add("Price");
                    table.Columns.Add("NavigateUrl");
                    table.Columns.Add("ImageUrl");
                    table.Columns.Add("Height");
                    table.Columns.Add("Length");
                    table.Columns.Add("Width");
                    table.Columns.Add("CodeWS");
                    table.Columns.Add("UnidadMedida");
                    table.Columns.Add("Stock");
                    table.Columns.Add("StockTooltip");
                    table.Columns.Add("StockTooltipRojo");
                    table.Columns.Add("StockTooltipVerde");
                    table.Columns.Add("StockTooltipAzul");
                    table.Columns.Add("ImgErrorUrl");
                    table.Columns.Add("ImgDiscountUrl");
                    table.Columns.Add("PriceWithDiscount");
                    table.Columns.Add("DiscountLabel");
                    table.Columns.Add("ShowDiscount", typeof(bool));
                    table.Columns.Add("CatalegPerDefecte");
                    table.Columns.Add("VisibleStockControlRepresentante", typeof(bool));
                    table.Columns.Add("VisibleStockControlClient", typeof(bool));
                    table.Columns.Add("CantidadRoja");
                    table.Columns.Add("VisibilidadCantidadRoja", typeof(bool));
                    table.Columns.Add("CantidadAzul");
                    table.Columns.Add("VisibilidadCantidadAzul", typeof(bool));
                    table.Columns.Add("CantidadVerde");
                    table.Columns.Add("VisibilidadCantidadVerde", typeof(bool));
                    //REZ 21032014 - Stock Gris
                    table.Columns.Add("CantidadGris");
                    table.Columns.Add("VisibilidadCantidadGris", typeof(bool));
                    table.Columns.Add("StockTooltipGris");

                    table.Columns.Add("StockTooltipNegro");
                    table.Columns.Add("VisibleStockNull", typeof(bool));
                    table.Columns.Add("VisibleStockNoNull", typeof(bool));
                    table.Columns.Add("ImgNivelServicio");
                    table.Columns.Add("ToolTipNivelServicio");
                    table.Columns.Add("NivelDeServicioVIsible", typeof(bool));
                    table.Columns.Add("ShowAgrupColor", typeof(bool));
                    table.Columns.Add("ShowAgrupMeasures", typeof(bool));
                    table.Columns.Add("ColorsAgrup");
                    table.Columns.Add("MeasuresAgrupLink");
                    table.Columns.Add("MeasuresAgrupText");
                    table.Columns.Add("NewProduct", typeof(bool));
                    //REZ 24/10/2013 - Añadimos InsertcatalogNo y ProductVersion para comprar el articulo exacto
                    table.Columns.Add("productVersion");
                    table.Columns.Add("productInsertCatalogNo");
                    #endregion Add Table Columns

                    //Get current UI Culture
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;

                    #region Apply filters to Articles (catalogo, familia, subfamilia, programa)
                    List<Article> collection = articles;
                    
                    //Si nos han pasado un filtro, filtramos la colección
                    if (!String.IsNullOrEmpty(filter))
                    {
                        collection = collection.Where(a => a.Description.ToUpper().Contains(filter) ||
                                                              a.Code.ToUpper().Contains(filter)).ToList<Article>();
                        CheckFilters = true;  //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    else if (
                                (!(String.IsNullOrEmpty(fDto)) && !(fDto == "0"))
                                ||
                                (!(String.IsNullOrEmpty(fOrden)) && !(fOrden == "0"))
                            )
                    {
                        //Hemos ordenado o filtrado por Descuentos, en este caso no agrupamos artículos.

                    }
                    else
                    {
                        //Si no hay filtro de text solo mostramos los principales de la agrupación. Ni ordenamos ni filtramos por descuento
                        collection = collection.Where(p => p.PrincipalAgrup == "Sí").ToList<Article>();
                    }
                    //if (string.IsNullOrEmpty(codCatalog))
                    //{
                    //    //Penaliza demasiado el rendimiento. Si estamos mostrando toda la lista dejamos duplicados porque no se veran juntos...
                    //    //Añadimos control de duplicados en caso de aplicar filtros
                    //    //CheckFilters = true;  
                    //}
                    

                    if (fFamily != String.Empty)
                    {
                        collection = collection.Where(p => p.Family == fFamily).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    //filter for the subfamily
                    if (fSubFamily != String.Empty)
                    {
                        collection = collection.Where(p => p.SubFamily == fSubFamily).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    //filter for the program
                    if (fProgram != String.Empty)
                    {
                        collection = collection.Where(p => p.Programa == fProgram).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    //filter for the product type
                    if (fProductoExEs != String.Empty)
                    {
                        collection = collection.Where(p => p.ProductType == fProductoExEs).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    
                    //Filter per Dto
                    switch (fDto)
                    {
                        case "0":
                            //No filter
                            break;
                        case "1":
                            collection = collection.Where(p => p.Discount != "0,00").ToList<Article>();
                            collection = collection.Where(p => p.TieneDtoExclusivo == true).ToList<Article>();
                            CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                            break;
                        case "2":
                            collection = collection.Where(p => p.Discount != "0,00").ToList<Article>();
                            collection = collection.Where(p => p.TieneDtoExclusivo == false).ToList<Article>();
                            CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                            break;
                        case "3":
                            //Todos los productos con descuento
                            collection = collection.Where(p => p.Discount != "0,00").ToList<Article>();
                            CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                            break;
                    }
                    
                    //REZ22032016 - Filtro por nueva categorizacion
                    if (fCategory != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fCategory)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    //ALB05062019 - Ja no es fa servir
                    /*
                    if (fEstance != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fEstance)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    if (fStyle != String.Empty)
                    {
                        collection = collection.Where(p => p.CategoriesList.Contains(fStyle)).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }*/
                    if (fColor != String.Empty)
                    {
                        collection = collection.Where(p => p.Color == fColor).ToList<Article>();
                        CheckFilters = true; //Aplicamos control de duplicados para que no aparezcan juntos en caso de filtro
                    }
                    
                    //TODO: Falta fPrice
                    if (!String.IsNullOrEmpty(this.minSlider.Value) && !String.IsNullOrEmpty(this.maxSlider.Value))
                    {
                        collection = collection.Where(p => p.PriceInt >= fMinPrice).ToList<Article>();
                        collection = collection.Where(p => p.PriceInt <= fMaxPrice).ToList<Article>();
                        CheckFilters = true;
                    }
                    
                    //Ordenacion de resultados
                    switch (fOrden)
                    {
                        case "1":
                            collection = collection.OrderBy(p => p.Price).ToList<Article>();
                            break;
                        case "2":
                            collection = collection.OrderByDescending(p => p.Price).ToList<Article>();
                            break;
                        case "3":
                            collection = collection.OrderBy(p => p.CantidadVerde).ThenBy(p => p.CantidadAzul).ThenBy(p => p.CantidadRojo).ThenBy(p => p.CantidadGris).ToList<Article>();
                            break;
                        case "4":
                            collection = collection.OrderByDescending(p => p.CantidadVerde).ThenByDescending(p => p.CantidadAzul).ThenByDescending(p => p.CantidadRojo).ThenByDescending(p => p.CantidadGris).ToList<Article>();
                            break;
                    }

                    #endregion
                    
                    LoadCurrentUserandClientData();
                    
                    int rowId = 0;
                    int TotalItems = collection.Count();
                    RadListView1.VirtualItemCount = TotalItems;

                    int iPriceSel = (int)Session["PriceSelector"];
                    CultureInfo culture = new CultureInfo("es-Es", false);

                    //ALB06052019 - Ja ho fa al foreach de sota
                    /* foreach (Article a in collection)
                    {
                        a.CalculatePriceWithDiscount(iPriceSel.ToString(), currentClient.CoefDivisaWEB, currentClient.LiteralDivisaWEB, currentUser.Coeficient, (uint)Thread.CurrentThread.CurrentUICulture.LCID, 0, default(decimal));
                        this.currentArticles.Add(a);
                    }*/


                    collection = collection.Skip(pageNo * pageItems).ToList();
                   
                    foreach (Article a in collection)
                    {

                        #region Control de duplicados
                        //Check if duplicate
                        int RowInsertId = -1;
                        if (CheckFilters)
                        {
                            DataRow[] duplicatedRows = table.Select("Code = '" + a.Code.ToString() + "'");
                            if (duplicatedRows.Count() > 0)
                            {
                                //CodRepeat.Add(a.Code);
                                //codeRepeated = a.Code;
                                //No añadimos el nuevo artículo. Deberíamos ver si es el de catalogopordefecto, y en ese caso substituirlo
                                if (duplicatedRows[0]["CatalegPerDefecte"].ToString() == "true")
                                {
                                    //La que tengo añadida ya es el catalogo por defecto
                                    continue;   //Saltamos esta iteración
                                }
                                else
                                {
                                    //El producto que tenemos añadido no tiene el catálogo por defecto, miramos si el actual lo tiene
                                    if (a.CatalogPorDefecto == "true")
                                    {
                                        //necesito añadir un ID al datatable para poder machacarlo en caso de duplicado
                                        RowInsertId = int.Parse(duplicatedRows[0]["RowId"].ToString());
                                    }
                                    else
                                    {
                                        //Lo dejamos como está
                                        continue;
                                    }
                                }
                            }
                        }
                        #endregion Control de duplicados

                        #region Transformación de datos
                        DataRow row = table.NewRow();
                        row["RowId"] = (RowInsertId > -1) ? RowInsertId : rowId;
                        row["Code"] = a.Code;
                        row["EANcode"] = a.EANcode;
                        row["CatalogNo"] = a.CatalogNo;
                        row["CodeWS"] = a.Code.Replace(' ', '_');
                        //REZ - 24/10/2013
                        row["productVersion"] = a.ArticleVersion;
                        row["productInsertCatalogNo"] = a.InsertcatalogNo;

                        if (a.Description.Length > 50)
                        {
                            row["Description"] = a.Description.Substring(0, 50) + "...";
                        }
                        else
                        {
                            row["Description"] = a.Description;
                        }
                        //REZ 29072014 - Gestión de Divisas personalizadas
                        switch (iPriceSel)
                        {
                            case 0:
                                row["Price"] = a.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                                break;
                            case 1:
                                row["Price"] = a.Price.ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                                break;
                            case 2:
                                row["Price"] = ((Convert.ToDouble(a.PriceInPoints, culture)) *
                                                (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture) +
                                                " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                                break;
                            case 3:
                                row["Price"] = ((Convert.ToDouble(a.Price.ToString("N2", culture), culture)) *
                                                (Convert.ToDouble(currentClient.CoefDivisaWEB, culture))).ToString("N2", culture)
                                                + " " + currentClient.LiteralDivisaWEB;
                                break;
                            case 4:
                                row["Price"] = (((Convert.ToDouble(a.PriceInPoints, culture)) *
                                                (Convert.ToDouble(currentClient.CoefDivisaWEB, culture))) *
                                                (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture)
                                                + " " + currentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                                break;
                            default:
                                row["Price"] = a.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                                break;
                        }

                        //row["NavigateUrl"] = "javascript:OpenProductInfoDialog1(\"" + a.Code + "\",\"" + a.ArticleVersion + "\",\"" + a.InsertcatalogNo + "\");";
                        row["NavigateUrl"] = "/Pages/ProductInfo.aspx?code=" + a.Code + "&version=" + a.ArticleVersion + "&catalog=" + a.InsertcatalogNo;
                        row["ImageUrl"] = a.SmallImgUrl;
                        row["Height"] = a.Height + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        row["Width"] = a.Width + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        row["Length"] = a.Length + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                        row["UnidadMedida"] = a.UnidadMedida;
                        if (this.currentUser.VerCantidades)
                        {
                            row["VisibleStockControlRepresentante"] = true;
                            row["VisibleStockControlClient"] = false;
                            row["VisibilidadCantidadRoja"] = a.CantidadRojo == 0 ? false : true;
                            row["CantidadRoja"] = a.CantidadRojo.ToString("N0");
                            row["VisibilidadCantidadAzul"] = a.CantidadAzul == 0 ? false : true;
                            row["CantidadAzul"] = a.CantidadAzul.ToString("N0");
                            row["VisibilidadCantidadVerde"] = a.CantidadVerde == 0 ? false : true;
                            row["CantidadVerde"] = a.CantidadVerde.ToString("N0");
                            //REZ 21032014 - Stock Gris
                            row["VisibilidadCantidadGris"] = a.CantidadGris == 0 ? false : true;
                            row["CantidadGris"] = a.CantidadGris.ToString("N0");
                            if (a.Stock == 3)
                            {
                                row["VisibleStockNull"] = true;
                                row["VisibleStockNoNull"] = false;
                            }
                            else
                            {
                                row["VisibleStockNull"] = false;
                                row["VisibleStockNoNull"] = true;
                            }

                            row["StockTooltipRojo"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                            row["StockTooltipVerde"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                            row["StockTooltipAzul"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                            row["StockTooltipNegro"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                            //REZ 21032014 - Stock Gris
                            row["StockTooltipGris"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), a.DescripcionGris);

                            row["StockTooltip"] = "";
                        }
                        else
                        {
                            row["VisibilidadCantidadRoja"] = false;
                            row["VisibilidadCantidadAzul"] = false;
                            row["VisibilidadCantidadVerde"] = false;
                            //REZ 21032014 - Stock Gris
                            row["VisibilidadCantidadGris"] = false;

                            row["VisibleStockControlRepresentante"] = false;
                            row["VisibleStockControlClient"] = true;
                            row["Stock"] = setStock(a.Stock);
                            row["StockTooltip"] = setStockTooltip(a.Stock, a.DescripcionGris);
                            row["StockTooltipRojo"] = "";
                            row["StockTooltipVerde"] = "";
                            row["StockTooltipAzul"] = "";
                            row["StockTooltipNegro"] = "";
                            row["StockTooltipGris"] = "";
                            row["VisibleStockNull"] = false;
                            row["VisibleStockNoNull"] = false;
                        }



                        row["ImgErrorUrl"] = "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg';";
                        row["CatalegPerDefecte"] = a.CatalogPorDefecto;


                        /*Descompte foramtejat*/
                        if (!(a.Discount == "0,00"))
                        {
                            row["ShowDiscount"] = (a.TieneDtoExclusivo) ? false : true;
                            row["ImgDiscountUrl"] = setImgDiscountURL(a);
                            //row["DiscountLabel"] = "-"+Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N0") + "%";
                            row["DiscountLabel"] = "-" + Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N2", culture).Replace(",00", "") + "%";
                            row["PriceWithDiscount"] = setDiscount(a);
                        }
                        else
                        {
                            row["ShowDiscount"] = false;
                            row["ImgDiscountUrl"] = "/Style%20Library/Julia/img/null_pixel.png";
                            row["DiscountLabel"] = "";
                            row["PriceWithDiscount"] = string.Empty;
                        }


                        /*Nivell de Servei*/
                        if (a.NiveldeServicio == 0)
                        {
                            row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                            row["ToolTipNivelServicio"] = "";
                            row["NivelDeServicioVIsible"] = false;
                        }
                        else if (a.NiveldeServicio == 1)
                        {
                            row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                            row["ToolTipNivelServicio"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio1"), a.NiveldeservicioDescripcion);
                            row["NivelDeServicioVIsible"] = true;
                        }
                        else
                        {
                            row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                            row["ToolTipNivelServicio"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio2"), a.NiveldeservicioDescripcion);
                            row["NivelDeServicioVIsible"] = true;
                        }
                        //Agrupacions

                        if (a.TipoAgrup == 1 && a.DetalleAgrup != string.Empty)
                        {
                            //Color

                            row["ShowAgrupColor"] = true;
                            row["ShowAgrupMeasures"] = false;
                            row["ColorsAgrup"] = ColorItemsProductImage(a);     //ColorItems(a);
                        }
                        else if (a.TipoAgrup == 2 && a.DetalleAgrup != string.Empty)
                        {
                            //Mesures

                            row["ShowAgrupColor"] = false;
                            row["ShowAgrupMeasures"] = true;
                            row["MeasuresAgrupLink"] = "/Pages/ProductInfo.aspx?code=" + a.Code + "&catalog=" + a.InsertcatalogNo + "&version=" + a.ArticleVersion;
                            row["MeasuresAgrupText"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("VerMasMedidas");

                        }
                        else
                        {
                            row["ShowAgrupColor"] = false;
                            row["ShowAgrupMeasures"] = false;
                        }

                        if (a.ProductNew == "No")
                        {
                            row["NewProduct"] = false;
                        }
                        else
                        {
                            row["NewProduct"] = true;
                        }

                        //if (min == 0 && max == 0)
                        //{
                        //    min = a.PriceInt;
                        //    max = a.PriceInt;
                        //}
                        //else
                        //{
                        //    if (a.PriceInt < min) min = a.PriceInt;
                        //    if (a.PriceInt > max) max = a.PriceInt;
                        //}

                        a.CalculatePriceWithDiscount(iPriceSel.ToString(), currentClient.CoefDivisaWEB, currentClient.LiteralDivisaWEB, currentUser.Coeficient, (uint)Thread.CurrentThread.CurrentUICulture.LCID, 0, default(decimal), currentClient.ShopInShop);
                        
                        /* 2019-06-04 - Aida Lucha -  - Afegit per no fer 2 vegades el bucle */
                        this.currentArticles.Add(a);
                        
                        #endregion Transformación de datos

                        #region Adicion de datos
                        if (RowInsertId > -1)
                        {//Si es un elemento duplicado que hay que substituir
                            table.Rows.RemoveAt(RowInsertId);
                            table.Rows.InsertAt(row, RowInsertId);
                        }
                        else
                        {//Si no es un elemento duplicado lo añadimos normalmente
                            table.Rows.Add(row);
                            rowId++;
                        }
                        #endregion Adicion de datos
                        if (rowId >= pageItems) break;
                    }
                    //LoadFilters(collection);
                    
                }


            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception("Fin LoadData"), new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            //Añadimos los datos a una cache para utilizarla en paginaciones (ya que no cambian los datos)
            HttpContext.Current.Cache.Remove("ArticlesCatalog_Paging_cache_" + currentUser.UserName + currentClient.Code);
            HttpContext.Current.Cache.Add("ArticlesCatalog_Paging_cache_" + currentUser.UserName + currentClient.Code, table, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);

            return table;
        }

        ///<summary>
        ///Funcion que devuelve el código HTML para mostrar las imagenes de los colores de agrupación de color
        ///</summary>
        ///<remarks>
        ///
        ///</remarks>
        private String ColorItems(Article a)
        {
            string ProductItemTmp = string.Empty;
            string url = string.Empty;
            string Imageurl = string.Empty;
            string[] ArtCol = a.DetalleAgrup.Split(';');

            int iAgrupCount = ArtCol.Length;

            for (int i = 0; i < iAgrupCount; i++)
            {
                Imageurl = "/Style Library/Color Images/" + ArtCol[i] + ".jpg";
                url = "/Pages/RedirectTo.aspx?Colorcode=" + ArtCol[i] + "&PatronAgrup=" + a.PatronAgrup;
                ProductItemTmp += "<a href='" + url + "'><img alt='" + ArtCol[i] + "' title='" + JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtCol[i]) + "' src='" + Imageurl + "' class='ColorItem' /></a>";
            }

            return ProductItemTmp;
        }

        ///<summary>
        ///Funcion que devuelve el código HTML para mostrar las imagenes de los productos de la misma 
        ///agrupación de color
        ///</summary>
        ///<remarks>
        ///Devuelve hasta 3 imágenes y una marca de más en caso que la agrupación esté formada por más de 4 artículos
        ///Importante Article.DetalleAgrup y Article.DetalleAgrupColor deben estar en el mismo orden
        ///</remarks>
        private String ColorItemsProductImage(Article a)
        {
            string ProductItemTmp = string.Empty;
            string url = string.Empty;
            string Imageurl = string.Empty;
            //string[] ArtCol = a.DetalleAgrup.Split(';');
            //Importante DetalleAgrup y DetalleAgrupColor deben estar en el mismo orden
            string[] ArtColorCodeCol = a.DetalleAgrup.Split(';');    //Contiene todos los códigos de color de la agrupación
            string[] ArtColorProductCodeCol = a.DetalleAgrupColor.Split(';');   //Contiene hasta 4 códigos de producto de la agrupación
            int iAgrupCount = ArtColorProductCodeCol.Length;

            for (int i = 0; ((i < iAgrupCount) && (i < 3)); i++)
            {

                //Imageurl = "/Style Library/Color Images/" + ArtCol[i] + ".jpg";
                url = "/Pages/RedirectTo.aspx?Colorcode=" + ArtColorCodeCol[i] + "&PatronAgrup=" + a.PatronAgrup;
                //ProductItemTmp += "<a href='" + url + "'><img alt='" + ArtCol[i] + "' title='" + JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtCol[i]) + "' src='" + Imageurl + "' class='ColorItem' /></a>";
                Imageurl = a.SmallImgUrl.Replace(a.Code, ArtColorProductCodeCol[i]);    //Mostramos la imagen del artículo
                //url = a.NavigateUrl.Replace(a.Code, ArtCol[i]);   //No funciona porque puede canviar la versión y quizá el catálogo
                ProductItemTmp += "<a href='" + url + "'><img alt='" + ArtColorCodeCol[i] + "' title='" + JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtColorCodeCol[i]) + "' src='" + Imageurl + "' class='ColorItem ColorItemProduct' /></a>";
            }
            if (iAgrupCount > 3)
            {
                ProductItemTmp += "<a href='" + a.NavigateUrl + "'><div class='ColorItemMore'></div></a>";
            }

            return ProductItemTmp;
        }
        private string setVisible(string a)
        {
            string visible = string.Empty;

            if (!(a == "0,00"))
            {
                visible = "true";
            }
            else
            {
                visible = "false";
            }

            return visible;
        }

        private object setDiscount(Article a)
        {
            string Output = string.Empty;
            string oldprice = a.Price.ToString("N2");
            int iPriceSel = (int)Session["PriceSelector"];
            CultureInfo culture = new CultureInfo("es-Es", false);

            if (!(a.Discount == "0,00"))
            {
                if (iPriceSel == 0) //Puntos
                {
                    Output = "  " + a.DiscountNet + " " + LanguageManager.GetLocalizedString("Points");
                }
                else if (iPriceSel == 1)    //Euros
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        Output = String.Format(a.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                    }
                }
                else if (iPriceSel == 2)    //Euros PVP
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        /* 2019-06-04 - Aida Lucha - Shop in shop*/
                        if (!currentClient.ShopInShop)
                        {
                            Output = ((Convert.ToDouble(a.DiscountNet, culture)) * (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        }
                        else {
                            Output = ((Convert.ToDouble(a.DiscountNetDecimal, culture)) * (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        }
                    }
                }
                else if (iPriceSel == 3)    //Divisa Cliente
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        Output = (
                                    (Convert.ToDouble(a.DiscountPriceNet, culture)) *
                                    (Convert.ToDouble(currentClient.CoefDivisaWEB, culture))
                                 ).ToString("N2", culture)
                                 + " " + currentClient.LiteralDivisaWEB;
                    }
                }
                else if (iPriceSel == 4)    //Divisa Cliente PVP
                {
                    if (!(a.DiscountNet == "0,00"))
                    {
                        Output = ((Convert.ToDouble(a.DiscountNet, culture)) *
                                    (Convert.ToDouble(currentUser.Coeficient, culture)) *
                                    (Convert.ToDouble(currentClient.CoefDivisaWEB, culture))
                                 ).ToString("N2", culture)
                                 + " " + currentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                    }
                }


            }
            return Output;
        }

        private object setImgDiscountURL(Article a)
        {
            //Image Imagen = (Image)this.RadListView1.Page.FindControl("ImgDiscount");

            string output = string.Empty;


            if (a.Discount.ToString() == "0,00")
            {
                output = "/Style%20Library/Julia/img/null_pixel.png";
                //Imagen.Visible = false;

            }
            else
            {
                if (a.TieneDtoExclusivo)
                {
                    output = "/Style%20Library/Julia/img/dto_exclusive.png";
                }
                else
                {
                    //REZ 13052015 - Producte en liquidació
                    //if (a.CatalogNo.StartsWith("PRELIQ"))
                    if (a.TipusIcona == 2)
                    {
                        output = "/Style%20Library/Julia/img/dto_liquidaciones.png";
                    }
                    else
                    {
                        output = "/Style%20Library/Julia/img/dto_promocion.png";
                    }
                }
                //Imagen.Visible = true;
            }

            return output;
        }

        private String setStock(decimal p)
        {
            string imgURL = string.Empty;

            int Stock = 0;
            Stock = Convert.ToInt32(p);
            switch (Stock)
            {
                case 0:
                    imgURL = "/Style%20Library/Julia/img/Red_Stock.png";
                    break;
                case 1:
                    imgURL = "/Style%20Library/Julia/img/Orange_Stock.png";
                    break;
                case 2:
                    imgURL = "/Style%20Library/Julia/img/Green_Stock.png";
                    break;
                case 3:
                    imgURL = "/Style%20Library/Julia/img/Black_Stock.png";
                    break;
                case 4:
                    imgURL = "/Style%20Library/Julia/img/Gray_Stock.png";
                    break;
            }
            return (imgURL);
        }
        private String setStockTooltip(decimal p, string GrisDesc)
        {
            string output = string.Empty;

            int Stock = 0;
            Stock = Convert.ToInt32(p);
            switch (Stock)
            {
                case 0:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                    break;
                case 1:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                    break;
                case 2:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                    break;
                case 3:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                    break;
                case 4:
                    output = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), GrisDesc);
                    break;
            }
            return (output);
        }

        //public void CatalogSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    try
        //    {
        //        LoadFilters();   
        //        this.RadListView1.DataSource = LoadData();

        //        this.RadListView1.CurrentPageIndex = 0;
        //        this.RadListView1.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}
        public void FamiliaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Familia.SelectedValue = e.Value;
                this.Subfamilia.ClearSelection();
                //this.Programa.ClearSelection();
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;

                ReLoadVariableFilters();
                //this.RadListView1.DataBind();

                SetHistoryPoint();

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetHistoryPoint()
        {
            SetHistoryPoint(this.RadListView1.CurrentPageIndex);
        }
        private void SetHistoryPoint(int newPageNo)
        {
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("Family", this.Familia.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("SubFamily", this.Subfamilia.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("Program", this.Programa.SelectedValue);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("SearchTxt", this.SearchProduct.Text);
            //ScriptManager.GetCurrent(this.Page).AddHistoryPoint("PageNo", newPageNo.ToString());
            Session["fFamily"] = this.Familia.SelectedValue;
            Session["fSubFamily"] = this.Subfamilia.SelectedValue;
            //===========================================================================
            Session["fProgram"] = this.Programa.SelectedValue;
            Session["fProductoExEs"] = this.ProductoExEs.SelectedValue;
            Session["fCategory"] = this.CategoriaTreeView.SelectedValue;
            Session["Color"] = this.Color.SelectedValue;
            /*Session["fProgram"] = this.ProgramTreeView.SelectedValue;
            Session["fColor"] = this.ColorTreeView.SelectedValue;*/
            //===========================================================================
            //Session["fMinPrice"] = this.RadSlider_Price.SelectionStart;
            //Session["fMaxPrice"] = this.RadSlider_Price.SelectionEnd;
            Session["fSearchTxt"] = this.SearchProduct.Text;
            Session["fPageNo"] = newPageNo.ToString();
            Session["fCatalog"] = fCatalog;
        }

        public void SubFamiliaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Subfamilia.SelectedValue = e.Value;
                this.Programa.ClearSelection();
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();

                SetHistoryPoint();

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadSlider_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.minSlider.Value = RadSlider_Price.SelectionStart.ToString();
                this.maxSlider.Value = RadSlider_Price.SelectionEnd.ToString();
                LabelSelectionStart.Text = RadSlider_Price.SelectionStart.ToString();
                LabelSelectionEnd.Text = RadSlider_Price.SelectionEnd.ToString();
                this.RadListView1.CurrentPageIndex = 0;

                Session["fMinPrice"] = this.RadSlider_Price.SelectionStart;
                Session["fMaxPrice"] = this.RadSlider_Price.SelectionEnd;

                SetHistoryPoint();

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        //===========================================================================
        public void ProgramaSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Programa.SelectedValue = e.Value;
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void ProductoExEsSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.ProductoExEs.SelectedValue = e.Value;
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void ColorSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                this.Color.SelectedValue = e.Value;
                //this.RadListView1.DataSource = LoadData();
                this.RadListView1.CurrentPageIndex = 0;
                //this.RadListView1.DataBind();
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            try
            {
                //Al cambiar de categoria eliminem el filtre de preu
                //Session.Remove("fPrice");
                //Session.Remove("fMinPrice");
                //Session.Remove("fMaxPrice");

                this.RadListView1.CurrentPageIndex = 0;
                SetHistoryPoint();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        //===========================================================================

        public void RadListView1_ItemCommand(object sender, RadListViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "AddItemToCart":
                        //RadNumericTextBox num = (RadNumericTextBox)e.ListViewItem.FindControl("itemQty");
                        HtmlInputText num = (HtmlInputText)e.ListViewItem.FindControl("itemQty");
                        Literal code = (Literal)e.ListViewItem.FindControl("productCode");
                        Literal version = (Literal)e.ListViewItem.FindControl("productVersion");
                        Literal insertCatalog = (Literal)e.ListViewItem.FindControl("productInsertCatalogNo");
                        if (num != null && code != null)
                        {
                            //this.AddItemToCart(code.Text, (int)num.Value);
                            //this.AddItemToCart(code.Text, (int)num.Value, version.Text, insertCatalog.Text);
                            this.AddItemToCart(code.Text, int.Parse(num.Value), version.Text, insertCatalog.Text);
                        }

                        break;
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.scripClientScript.RegisterClientScriptBlock(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void AddItemToCart(string articleCode, int qtt, string version, string insertCatalog)
        {
            try
            {

                currentOrder = Session["Order"] as Order;
                Client cl = (Client)Session["Client"];
                string code = string.Empty;
                if (!string.IsNullOrEmpty(articleCode) && qtt > 0)
                {
                    if (Session["ArticleDAO"] != null)
                    {
                        this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];

                    }
                    else
                    {
                        throw new SPException("Error: ArticleDAO session var is Null");
                    }
                    Products = articleDAO.GetAllProducts();
                    Article iArticle = articleDAO.GetProductByCodeAndCatalog(articleCode, version, insertCatalog);

                    //List<Article> articles = Session["Articles"] as List<Article>;
                    //articles = articles.Where(p => p.Code == articleCode).ToList<Article>();
                    //code = articles[0].Code;
                    code = iArticle.Code;
                    //if (articles.Count > 0)
                    currentOrder.AddProduct(iArticle, qtt, "", cl.UserName);
                }


                //CarritoUserControl carrito = (CarritoUserControl)this.FindControlRecursive(this.Page.Master, "Carrito");
                //carrito.Refresh();
                RefreshCarrito();

                //REZ04042016 - Quitamos los popUps, debe mostrarse el aumento de número en el sticky
                //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));

            }
            catch (NavException ex)
            {

                //string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);


            }
        }

        protected void AddItemToCart(string articleCode, int qtt)
        {
            try
            {

                currentOrder = Session["Order"] as Order;
                Client cl = (Client)Session["Client"];
                string code = string.Empty;
                if (!string.IsNullOrEmpty(articleCode) && qtt > 0)
                {
                    List<Article> articles = Session["Articles"] as List<Article>;
                    articles = articles.Where(p => p.Code == articleCode).ToList<Article>();
                    code = articles[0].Code;
                    if (articles.Count > 0)
                        currentOrder.AddProduct(articles[0], qtt, "", cl.UserName);
                }


                //CarritoUserControl carrito = (CarritoUserControl)this.FindControlRecursive(this.Page.Master, "Carrito");
                //carrito.Refresh();
                RefreshCarrito();

                //REZ04042016 - Quitamos los popUps, debe mostrarse el aumento de número en el sticky
                //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "test", "prova(" + code + ");", true);


                //Page.ClientScript.RegisterStartupScript(this.GetType(),"test","prova(" + code + ");",true);

                //Response.Redirect(Request.RawUrl);

            }
            catch (NavException ex)
            {

                //string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);


            }
        }

        private void RefreshCarrito()
        {
            RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
            ajaxManager.ResponseScripts.Add(@"RefreshTable();");
        }

        public Control FindControlRecursive(Control Root, string Id)
        {
            try
            {
                if (Root.ID == Id)
                    return Root;
                foreach (Control Ctl in Root.Controls)
                {
                    Control FoundCtl = FindControlRecursive(Ctl, Id);
                    if (FoundCtl != null)
                        return FoundCtl;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return null;
        }



        protected override void OnPreRender(EventArgs e)
        {

            RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
            ajaxManager.ClientEvents.OnRequestStart = "onRequestStart";

            if (ajaxManager == null)
            {
                RadAjaxManager manager = new RadAjaxManager();
                manager.ID = "RadAjaxManager1";
                manager.ClientEvents.OnRequestStart = "onRequestStart";
                Page.Items.Add(typeof(RadAjaxManager), manager);
                Page.Form.Controls.Add(manager);
            }

            LoadCurrentUserandClientData();

            DataTable cachedArticles = null;
            if (getCachedData)
            {
                cachedArticles = (DataTable)HttpContext.Current.Cache.Get("ArticlesCatalog_Paging_cache_" + currentUser.UserName + currentClient.Code);
            }
            //DataTable selectedArticles;
            if (cachedArticles == null)
            {
                cachedArticles = LoadData();
            }

            LoadFilters();


            this.RadListView1.DataSource = cachedArticles;
            this.RadListView1.DataBind();
            this.RadListView1.CurrentPageIndex = fPageNo;
            this.SearchProduct.Text = fSearchTxt;

            base.OnPreRender(e);
        }

        void PageSizeComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            Session.Add("ItemsPerPage", e.Value);
            this.PageSize = int.Parse(e.Value);

            //LoadCurrentUserandClientData();

            //DataTable cachedArticles = (DataTable)HttpContext.Current.Cache.Get("ArticlesCatalog_Paging_cache_" + currentUser.UserName + currentClient.Code);
            ////DataTable selectedArticles;
            //if (cachedArticles == null)
            //{
            //    cachedArticles = LoadData();
            //}

            getCachedData = false;

            //selectedArticles = GetSubData(cachedArticles, e.NewPageIndex, this.PageSize);   //Solo funciona con AllowCustomPaging, de momento no lo usamos
            //this.RadListView1.DataSource = selectedArticles;
            //this.RadListView1.VirtualItemCount = cachedArticles.Rows.Count;

            //Sin allowCustomPaging. Parece que no afecta tanto al rendimiento
            //this.RadListView1.DataSource = cachedArticles;
            this.RadListView1.CurrentPageIndex = 0;
            //this.RadListView1.DataBind();
            SetHistoryPoint();
            ////REZ 21032014 - Borramos cookie para que no coja la pagina de cache???
            //HttpCookie cookie = HttpContext.Current.Request.Cookies["UserSettings"];
            //cookie.Value = null;
        }

        protected void RadDataPager_FieldCreated(object sender, RadDataPagerFieldCreatedEventArgs e)
        {
            try
            {
                if (e.Item.Field is RadDataPagerPageSizeField)
                {
                    var combo = e.Item.FindControl("PageSizeComboBox") as RadComboBox;

                    if (combo != null)
                    {
                        combo.Items.Clear();
                        //AddComboItem(combo, "16");
                        //AddComboItem(combo, "32");
                        AddComboItem(combo, "64");
                        AddComboItem(combo, "128");
                        AddComboItem(combo, "256");
                        //carreguem el valor de Session (CASE)
                        if (Session["ItemsPerPage"] != null)
                        {
                            combo.SelectedValue = Session["ItemsPerPage"].ToString();
                        }
                        combo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(PageSizeComboBox_SelectedIndexChanged);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        protected void AddComboItem(RadComboBox combo, string value)
        {
            try
            {
                if (combo.FindItemByValue(value) == null)
                    combo.Items.Add(new RadComboBoxItem(value, value));
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void RadListView1_DataBound(object sender, EventArgs e)
        {
            foreach (RadListViewDataItem item in RadListView1.Items)
            {
                Label Label_Price = (Label)item.FindControl("Label_Price");
                Label Label_Discount = (Label)item.FindControl("Label_Discount");
                Panel Price_Panel = (Panel)item.FindControl("Panel_Price");
                Panel Price_Discount = (Panel)item.FindControl("Panel_PriceWithDiscount");

                if (Price_Discount.ID == "Panel_PriceWithDiscount")
                {
                    Price_Discount.Style.Add("color", "green");
                    Price_Discount.Style.Add("text-decoracion", "none");

                }
                if (Label_Discount.Text != "")
                {
                    Price_Panel.Style.Add("text-decoration", "line-through");
                    Price_Panel.Style.Add("margin-right", "1.5rem");
                    Price_Panel.Style.Add("color", "rgb(154,46,47)");
                }
                else
                {
                    Price_Discount.Visible = false;
                    Price_Panel.Style.Add("text-decoration", "none");
                    Price_Panel.Style.Add("color", "rgb(154,46,47)");
                }
            }

        }

        public void PrintView_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);
            }
            catch (NavException ex)
            {

                //string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");



            }


        }
        private void muestraArchivo(byte[] documento)
        {
            string extension = ".pdf";
            string contentType = "";
            switch (extension)
            {
                case ".xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".doc":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/msword";
                    break;
                case ".ppt":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
            }
            //MUESTRA VÍA WEB


            //string fileUrl = GenerateDownloadLink(fileContent, "test" +  extension);
            Response.Clear();
            MemoryStream ms = new MemoryStream(documento);
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=products.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();


        }

        protected void ScriptManager1_Navigate(object sender, HistoryEventArgs e) // Event handler for restoring state
        {
            //this.Familia.SelectedValue = e.State["Family"];
            //this.Subfamilia.SelectedValue = e.State["SubFamily"];
            //this.Programa.SelectedValue = e.State["Programa"];
            //this.SearchProduct.Text = e.State["SearchTxt"];
            //this.RadListView1.CurrentPageIndex = Convert.ToInt32(e.State["PageNo"]);
        }



    }
}
