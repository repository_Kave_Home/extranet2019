﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Data;
using System.Linq;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Threading;
using JuliaGrupUtils.ErrorHandler;
using Telerik.Web.UI;
using System.Web;
using System.Text;


namespace JuliaGrupPortalClientes_v2.ControlTemplates
{
    public partial class ClientSelectable : UserControl
    {
        private static int _defaultPageSize = 64;
        User currentUser;
        Client currentClient;
        Order currentOrder;
        int currentPriceSel;

        #region properties
        public string currentUsername
        {
            get
            {
                loaduserdata();
                return currentUser.UserName;
            }
        }

        public string currentOrderNo
        {
            get
            {
                loaduserdata();
                return currentOrder.NavOferta.No;
            }
        }

        public string currentClientCode
        {
            get
            {
                loaduserdata();
                return currentClient.Code;
            }
        }

        public int currentPriceSelId
        {
            get
            {
                loaduserdata();
                return currentPriceSel;
            }
        }

        public string currentUserVerCantidades
        {
            get
            {
                loaduserdata();
                return currentUser.VerCantidades.ToString();
            }
        }

        public string currentUserLanguage
        {
            get
            {
                loaduserdata();
                return currentUser.Language;
            }
        }

        public decimal coeficient
        {
            get
            {
                loaduserdata();
                return currentUser.Coeficient;
            }
        }

        public decimal coefDivisaWEB
        {
            get
            {
                loaduserdata();
                return currentClient.CoefDivisaWEB;
            }
        }

        public string literalDivisaWEB
        {
            get
            {
                loaduserdata();
                return currentClient.LiteralDivisaWEB;
            }
        }

        
        #endregion properties

        protected void LoadSessionVars()
        {
            Sesion SesionJulia = new Sesion();

            /* REZ:: Porque carga Article, Order y Ventas todo el rato??? -- Lo quito a ver que pasa*/
            //Carrego Article

            

            if (Session["ArticleDAO"] == null)
            {
                ArticleDataAccessObject ArticleDAO;
                ArticleDAO = SesionJulia.GetArticle(currentUser.UserName, this.currentClient.Code, currentUser.Language);
                if (ArticleDAO == null)
                    throw new Exception("Error NULL en el GetArticle del cliente seleccionado");
                Session.Add("ArticleDAO", ArticleDAO);
            }
            

            if (Session["Order"] == null)
            {
                //Carrego Order
                Order Order;
                Order = SesionJulia.GetOrder(this.currentClient);
                if (Order == null)
                    throw new Exception("Error NULL en el GetOrder del cliente seleccionado");
                Session.Add("Order", Order);
            }

            if (Session["PriceSelector"] == null)
            {
                //Carrego el valor del selector per defecte que son punts              
                //TRUE =  Points
                //FALSE = Euros                 

                /* 2019-06-04 - Aida Lucha - Shop in shop */
                Session.Add("PriceSelector", (int)0);
                
            }
            if (Session["ItemsPerPage"] == null)
            {
                //Carrego el valor del selector per defecte que son 64                  
                Session.Add("ItemsPerPage", (int)_defaultPageSize);
            }

            //REZ 25022014 - Generamos una Cookie para poder usar OutputCache
            if (HttpContext.Current.Request.Cookies["UserSettings"] == null)
            {
                string cookievalue = currentUser.UserName + "_" + currentClient.Code + "_" + Session["PriceSelector"].ToString() + "_" + currentUser.Language + "_" + Session["ItemsPerPage"].ToString();

                //Deberia ser response, pero entonces no lo hace bien porque no vamos actualizando y borrando la cookie... Los redirect con EndResponse hacen que no se borre
                //HttpContext.Current.Response.Cookies.Add(new HttpCookie("UserSettings", cookievalue));
                HttpContext.Current.Request.Cookies.Add(new HttpCookie("UserSettings", cookievalue));
            }
            //El problema vendra al borrar la cookie, que hara cache y no cogera el cambio de usuario
        }

        private void SetDisplayLanguage(int lcid)
        {
            string script = "<script language='javascript'>OnSelectionChange('" + lcid + "')</script>";
            Page.ClientScript.RegisterStartupScript(GetType(), "Register", script);
        }

        private void loaduserdata()
        {
            Sesion SesionJulia = new Sesion();
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
                
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);
                
                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
            }
            
            if (Session["Client"] == null)
            {
                if (this.currentUser is Agent)
                {
                    Agent agent = (Agent)this.currentUser;
                    //RTP: Carrer el primer client de la llista d'agents que és ell mateix
                    Client cli = SesionJulia.GetClient(agent.ListClients[0].Code, currentUser.UserName);
                    if (cli == null)
                        throw new Exception("Error NULL en el GetClient del cliente seleccionado");

                    //RTP: S'ha de posar el username del client amb el currentuser
                    cli.UserName = agent.UserName;
                    cli.Code = agent.Code;
                    cli.Language = agent.Language;
                    this.currentClient = cli;
                    Session["Client"] = cli;

                    

                }
                else if (this.currentUser is Client)
                {
                    this.currentClient = (Client)this.currentUser;
                    Session["Client"] = (Client)currentUser;
                    

                }
                else
                { //CLIENTGROUP
                    ClientGroup clientgroup = (ClientGroup)this.currentUser;
                    //RTP: Carrer el primer client de la llista d'agents que és ell mateix
                    Client cli = SesionJulia.GetClient(clientgroup.ListClients[0].Code, currentUser.UserName);
                    if (cli == null)
                        throw new Exception("Error NULL en el GetClient del cliente seleccionado");

                    //RTP: S'ha de posar el username del client amb el currentuser
                    cli.UserName = clientgroup.UserName;
                    cli.Code = clientgroup.Code;
                    cli.Language = clientgroup.Language;
                    this.currentClient = cli;
                    Session["Client"] = cli;
                    

                }

                

            }
            else
            {
                this.currentClient = (Client)Session["Client"];
                

            }



            if (Session["Order"] != null)
            {
                this.currentOrder = (Order)Session["Order"];

            }
            else
            {
                //Carrego Order
                this.currentOrder = SesionJulia.GetOrder(this.currentClient);
                if (this.currentOrder == null)
                    throw new Exception("Error NULL en el GetOrder del cliente seleccionado");
                Session.Add("Order", this.currentOrder);
            }
            
            if (Session["PriceSelector"] == null)
            {             
                currentPriceSel = 0;  
            }
            else
            {
                currentPriceSel = (int)Session["PriceSelector"];
            }
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO PAGELOAD [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                string strCultureName = "es-ES";

                if (!Page.IsPostBack)
                {
                    //LoadSessionVars();
                    //REZ 29072013 - Cargamos solo la información del usuario
                    loaduserdata();
                    
                    if (!currentUser.UserHasLaFormaAcces)
                    {
                        Response.Redirect("/gdo", true);
                    }

                    if (currentUser.Loaded && currentUser.FechaCaducidadPassword < DateTime.Today)
                    {
                        //Response.Redirect("/_layouts/login/changepass.aspx?outdate=true&u=" + currentUser.UserName);
                        //REZ 29072013 - No recargamos el resto de funciones
                        Session.Clear();    //Borramos la sesion y redirigimos a la página de cambio de password
                        Response.Redirect("/_layouts/login/changepass.aspx?outdate=true&u=" + currentUser.UserName, true);
                        //Response.End();
                    }

                    //lblUsername.Text = currentUser.UserName; //Nombre de usuario


                    //aplica el idioma que ve de NAV al portal 
                    //Recupero el idioma de NAV
                    //aplico el idioma al portal
                    
                    int currentLang = LoadLanguage(currentUser.Language);
                    
                    if (currentUser.Loaded && currentLang != Thread.CurrentThread.CurrentUICulture.LCID)
                    {

                        Sesion SesionJulia = new Sesion();
                        Client cl = (Client)Session["Client"];
                        currentLang = Thread.CurrentThread.CurrentUICulture.LCID;
                        currentUser.Language = SetLanguage(Thread.CurrentThread.CurrentUICulture.LCID.ToString());
                        SesionJulia.UpdateClientLanguage(currentUser);
                        Session["User"] = currentUser;

                    

                        // ---- Bloc per actualitzar la llista de productos amb l'idioma sel.leccionat
                        SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
                        ArticleDataAccessObject ArticleDAO;
                        ArticleDAO = SesionJulia.GetArticle(currentUser.UserName, cl.Code, currentUser.Language);
                        Session.Add("ArticleDAO", ArticleDAO);
                    }
                    //salvas a NAV
                    if (currentUser.Loaded == false)
                    {
                        currentUser.Loaded = true;
                        if (currentLang != Thread.CurrentThread.CurrentUICulture.LCID)
                        {//REZ 29072013 - Solo cambiamos el idioma si el idioma de iterficie es diferente del del usuario
                            SetDisplayLanguage(currentLang);
                        }
                    }

                    JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO LOAD SESSION VARS [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                    LoadSessionVars();
                    //Cargamos el combo de selector de euros/puntos
                    JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - FIN LOAD SESSION VARS [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                    
                    this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("PointsSelector"), "0"));
                        //REZ 29072014 - Añadimos items de Divisa Web
                    

                    if (currentClient.VerDivisaWEB)
                    {
                        if (!((currentUser is Client) && (currentUser.AccessType == "Basic")))
                        {
                            //this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(currentClient.LiteralDivisaWEB, "3"));
                            //REZ 11082014 - Siempre mostramos Euros, en caso de ver divisa esta aplica solo a PVP
                            this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosSelector"), "1"));
                        }
                        this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(currentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo"), "4"));
                    }
                    else
                    {
                        if (!((currentUser is Client) && (currentUser.AccessType == "Basic")))
                        {
                            this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosSelector"), "1"));
                        }
                        this.PricePoints.Items.Add(new Telerik.Web.UI.RadComboBoxItem(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPCombo"), "2"));
                    }

                    
                    this.PricePoints.SelectedValue = Session["PriceSelector"].ToString();

                    strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    if (this.currentUser is Client)
                    {
                        RadComboBox.Visible = false;
                        this.LinkToManual.NavigateUrl = "/Documents/" + strCultureName + "/Manual.pdf";

                    }
                    else
                    {
                        RadComboBox.Visible = true;
                        this.LinkToManual.NavigateUrl = "/Documents/" + strCultureName + "/Agent_Manual.pdf";

                        //this.LoadDropList();
                        this.Loadcombolist();

                    }

                   
                    //Populate language dropdown and select currentuser language
                    this.LanguageSelect.SelectedValue = currentLang.ToString();

                    ///* en caso que haya elementos en la wishlist, pintamos el corazon de color rojo -- Esto es un error de rendimiento muy grave --> Pasarlo a javascript async */
                //JuliaGrupUtils.Business.Wishlist wishlist = new JuliaGrupUtils.Business.Wishlist();
                //wishlist.AssignUserAndCode(currentUser.UserName, currentClient.Code);
                //wishlist.GetWishlist();

                //if (wishlist.WishlistLines.Count != 0)
                //{
                //    iconWL.Style.Add("color", "#A12830");
                //}
                //else
                //{
                //    iconWL.Style.Add("color", "#777777");
                //}
            }
            else
                {


                }
                //Cargamos el fichero de recursos en JS
                string language = "<script type='text/javascript' src='/_layouts/ScriptResx.ashx?culture=" + strCultureName + "&name=JuliaGrupPortalClientesResources'></script>";
                Page.ClientScript.RegisterStartupScript(GetType(), "Language", language);
                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - FIN PAGELOAD [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
        }

        private void Loadcombolist()
        {
            try
            {
                if (RadComboBox.DataSource == null)
                {
                    if (this.currentUser is Agent)
                    {
                        Agent agent = (Agent)this.currentUser;
                        this.RadComboBox.DataSource = agent.ListClientsName;
                    }
                    else if (this.currentUser is ClientGroup)//CLIENTGROUP
                    {
                        ClientGroup clientgroup = (ClientGroup)this.currentUser;
                        this.RadComboBox.DataSource = clientgroup.ListClientsName;
                    }
                    this.RadComboBox.SelectedValue = null;
                    this.RadComboBox.DataBind();
                    this.RadComboBox.SelectedValue = this.currentClient.Code;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void RadComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                loaduserdata();

                //Neteja la sessio pq s'ha canviat de client, cal netejar-ho tot.
                Session.Clear();

                Sesion SesionJulia = new Sesion();
                SesionJulia.CleanArticleCache(currentUser.UserName, currentClient.Code);

                Client cli = SesionJulia.GetClient(RadComboBox.SelectedValue, currentUser.UserName);
                if (cli == null)
                    throw new Exception("Error NULL en el GetClient del cliente seleccionado");
                Session["Client"] = cli;
                this.currentClient = cli;

                string returnUrl = String.Empty;
                ////Full Absolute Url
                //returnUrl = Page.Request.Url.AbsoluteUri;
                ////Full Relative Url including QueryString
                //returnUrl = Page.Request.Url.PathAndQuery;
                //Relative Url without QueryString
                returnUrl = Page.Request.Url.AbsolutePath;

                Page.Response.Redirect(returnUrl, true);        //Si dejamos false se ejecuta antes slider y peta todo...

            }
            catch (ThreadAbortException)
            {
                // Do nothing. ASP.NET is redirecting.
                // Always comment this so other developers know why the exception 
                // is being swallowed.
                //Capturamos la excepción que por diseño ASP.Net lanza para Response.End o para Response.Redirect
                //Otra alternativa es mover el redirect fuera del catch
                //http://stackoverflow.com/questions/4368640/response-redirect-and-thread-was-being-aborted-error
                //http://briancaos.wordpress.com/2010/11/30/response-redirect-throws-an-thread-was-being-aborted/
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private int LoadLanguage(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "CAT":
                    ret = "1027";
                    break;
                case "ESP":
                    ret = "3082";
                    break;
                case "FRA":
                    ret = "1036";
                    break;
                case "ITA":
                    ret = "1040";
                    break;
                case "ENU":
                    ret = "1033";
                    break;
                case "DEU":
                    ret = "1031";
                    break;
                default:
                    ret = "3082";
                    break;
            }
            return Convert.ToInt32(ret);
        }

        private string SetLanguage(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "1027":
                    ret = "CAT";
                    break;
                case "3082":
                    ret = "ESP";
                    break;
                case "1036":
                    ret = "FRA";
                    break;
                case "1040":
                    ret = "ITA";
                    break;
                case "1033":
                    ret = "ENU";
                    break;
                case "1031":
                    ret = "DEU";
                    break;
                default:
                    ret = "ESP";
                    break;
            }
            return ret;
        }
        /* Movido a TopMenu */
        /*protected void lnkbtnclose_click(object sender, EventArgs e)
        {
            Sesion SesionJulia = new Sesion();
            this.currentUser = (User)Session["User"];
            Client cl = (Client)Session["Client"];
            SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
            Session.Clear();
            Page.Response.Redirect("/_layouts/SignOut.aspx");
        }*/

        protected void lnkbtnrefreshData_click(object sender, EventArgs e)
        {
            Sesion SesionJulia = new Sesion();
            this.currentUser = (User)Session["User"];
            Client cl = (Client)Session["Client"];
            Session.Remove("ArticleDAO");
            Session.Remove("Articles");
            SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void PricePoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session.Remove("PriceSelector");
            //HttpContext.Current.Response.Cookies.Remove("UserSettings");
            Session.Add("PriceSelector", Convert.ToInt32(this.PricePoints.SelectedValue));

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        private void ClearFilters()
        {
            Session.Remove("fSearchTxt");
            Session.Remove("fFamily");
            Session.Remove("fSubFamily");
            Session.Remove("fProgram");
            Session.Remove("fPageNo");
            Session.Remove("fCatalog");
            Session.Remove("prodOrder");
            Session.Remove("DtoExclusivo");
        }
    }
}
