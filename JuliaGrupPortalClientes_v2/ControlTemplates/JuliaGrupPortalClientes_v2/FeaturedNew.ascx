﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedNew.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.FeaturedNew" %>

<!-- Destacado -->
<div class="newsItem featured">
    <div class="newsItemContent">
        <div class="divImageSuput"><asp:Literal id="newsItemThumb" runat="server" /> </div>
       
        <div class="art-postheadericons art-metadata-icons">
          
            <span class="art-postcategoryicon" id="categories" runat="server">
                <span class="categories"><asp:Literal ID="PublicadoEnLbl" runat="server"/></span> 
            </span>
        </div>
        <div class="newsItempreview">
            <h2><a id="linktitle" href="#" runat="server"></a></h2>
            <p id="newsummarytext" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id justo erat, quis interdum ligul...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id justo erat, quis interdum ligul...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id justo erat, quis interdum ligul...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id justo erat, quis interdum ligul... </p>
            <%--  
            <ul class="socialFeedback">
                <li id="sociallikes" class="sociallikes" runat="server" style="cursor:hand;"></li>                
                <li id="socialComments" class="socialComments" runat="server" style="cursor:hand;"></li>                
            </ul>
            <div id="listOfLikeFacesContainer" class="listOfLikeFacesContainer" runat="server" style="display:none"></div>
            <div id="listOfCommentFacesContainer" class="listOfCommentFacesContainer" runat="server" style="display:none"></div>
            <p class="time">
                <span class="dots">...</span> </br><asp:Literal id="lblnewdate" runat="server" />
            </p>
            --%>
            <div class="moreinfo"><a id="MoreInfoLynk" href="#" runat="server"><img src="/Style%20Library/Julia/img/more.jpg" alt="" /><span><asp:Literal id="lblmoreinfo" runat="server" /></span></a></div>
        </div>
    </div>
</div>
