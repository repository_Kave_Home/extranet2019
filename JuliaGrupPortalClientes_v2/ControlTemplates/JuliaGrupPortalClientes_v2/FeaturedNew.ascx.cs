﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Utilities;
using System.Web;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class FeaturedNew : UserControl
    {
        #region Properties
        public string Url { get; set; }
        public string Title { get; set; }
        public string ImgUrl { get; set; }
        public string SummaryText { get; set; }
        public int LikesNum { get; set; }
        //public List<Like> LikesList { get; set; }
        //public List<Comment> CommentsList { get; set; }
        public int CommentsNum { get; set; }
        public string[] Categories { get; set; }
        public int Id { get; set; }
        public string SiteCollectionUrl { get; set; }
        public DateTime NewDate { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PublicadoEnLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("PublishedIn");
            EnsureChildControls();

            linktitle.InnerText = Title;
            linktitle.HRef = Url;
            MoreInfoLynk.HRef = Url;
            //this.lblnewdate.Text = NewDate.ToShortDateString();
            this.lblmoreinfo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NewsMoreInfoLink"); 

            //delete width and height of url            
            string urlToTransform = ImgUrl;
            if (urlToTransform.ToUpper().IndexOf("WIDTH=\"") != -1)
            {
                int iniWidth = urlToTransform.ToUpper().IndexOf("WIDTH=\"");
                int posEndWith = urlToTransform.ToUpper().IndexOf("\"", iniWidth + 8);
                int numPositions = urlToTransform.Length - posEndWith;
                string urlToTransformIni = urlToTransform.Substring(0, iniWidth);
                string urlToTransformEnd = urlToTransform.Substring(posEndWith + 1, numPositions - 1);
                urlToTransform = urlToTransformIni + " " + urlToTransformEnd;
            }

            if (urlToTransform.ToUpper().IndexOf("HEIGHT=\"") != -1)
            {
                int iniWidth = urlToTransform.ToUpper().IndexOf("HEIGHT=\"");
                int posEndWith = urlToTransform.ToUpper().IndexOf("\"", iniWidth + 9);
                int numPositions = urlToTransform.Length - posEndWith;
                string urlToTransformIni = urlToTransform.Substring(0, iniWidth);
                string urlToTransformEnd = urlToTransform.Substring(posEndWith + 1, numPositions - 1);
                urlToTransform = urlToTransformIni + " " + urlToTransformEnd;
            }

            newsItemThumb.Text = urlToTransform;
            //class="newsItemThumb" alt="" _moz_resizing="true"
            newsummarytext.InnerText = SPHttpUtility.ConvertSimpleHtmlToText(SummaryText, -1);
            //int numLikes = LikesNum;
            //int numComments = CommentsNum;
            //if (numLikes > 0)
            //    sociallikes.Attributes.Add("onclick", "javascript:ShowDivLikes(this," + this.Id + ");");

            //sociallikes.InnerText = numLikes + "";
            //if (numComments > 0)
            //    socialComments.Attributes.Add("onclick", "javascript:ShowDivComments(this," + this.Id + ");");

            //socialComments.InnerText = numComments + "";

            //ListOfFaces likesFaces = (ListOfFaces)LoadControl(_ListOffacesascxPath);
            //likesFaces.ListOfLikes = this.LikesList;
            //this.listOfLikeFacesContainer.Controls.Add(likesFaces);

            //ListOfFaces commentFaces = (ListOfFaces)LoadControl(_ListOffacesascxPath);
            //commentFaces.ListOfComments = this.CommentsList;
            //this.listOfCommentFacesContainer.Controls.Add(commentFaces);


            int categoriescounter = 0;
            foreach (string cat in Categories)
            {
                var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                nameValues.Remove("page");  //REZ - 27/11/2012 -- Al canviar de categoria no te sentit mantenir la pagina perque cal reindexar
                nameValues.Remove("category");
                nameValues.Set("category", cat);

                string aux = "";

                if (this.SiteCollectionUrl != null && this.SiteCollectionUrl != "")
                    aux = ((categoriescounter > 0) ? ", " : "") + "<a href=\"" + SiteCollectionUrl + "/?" + nameValues.ToString() + "\">" + cat + "</a>";
                else
                    aux = ((categoriescounter > 0) ? ", " : "") + "<a href=\"" + Page.Request.Url.AbsolutePath + "?" + nameValues.ToString() + "\">" + cat + "</a>";

                categories.Controls.Add(new LiteralControl()
                {
                    Text = aux
                });

                categoriescounter++;
            }
        }
    }
}
