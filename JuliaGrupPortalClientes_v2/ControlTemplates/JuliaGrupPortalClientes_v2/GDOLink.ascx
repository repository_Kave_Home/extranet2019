﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GDOLink.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.GDOLink" %>


<li ID="GDOButton" runat="server" Visible="false">
    <a href="/gdo" style="color:#A12830">
        <i class="fa fa-arrow-circle-right fa-lg" style="padding-right:5px;"></i>
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("GDO_LinkText")%>
    </a>
</li>

<%-- 
<asp:HyperLink ID="GDOButton" runat="server" Visible="false">
    <div class="s4-tn" style="float: right!important;">
        <div class="menu horizontal menu-horizontal">
            <ul class="root static">
                <li class="static">
                    <a class="static menu-item" style="background-color: #a12830!important;" href="/gdo">
                        <span class="additional-background">
                            <span class="menu-item-text">
                            <img height="15" style="top: 3px; position: relative;" src="/Style Library/Julia/img/GoTo.png">&nbsp;<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("GDO_LinkText")%>
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</asp:HyperLink>
--%>