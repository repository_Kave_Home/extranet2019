﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageDescSlideUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.ImageDescSlideUserControl" %>

<div class="<%# DataBinder.Eval(this, "cssClass") %>" style="height:220px;">
    <a href="<%# DataBinder.Eval(this, "link") %>">
        <img src="<%# DataBinder.Eval(this, "image_url") %>" 
            width="<%# DataBinder.Eval(this, "width") %>" 
            height="<%# DataBinder.Eval(this, "height") %>" 
             onerror = "<%# DataBinder.Eval(this, "imgErrorUrl") %>" 
            border="0px"
            alt=""/>
    </a>
    <div class="clearFloat"></div>
    <div class="productModel">
        <%# DataBinder.Eval(this, "productModel") %>
    </div>
    <div class="productDescription">
        <%# DataBinder.Eval(this, "productDescription") %>
    </div>
    <div class="productPrice">
        <%# DataBinder.Eval(this, "productPrice") %>
    </div>
</div>