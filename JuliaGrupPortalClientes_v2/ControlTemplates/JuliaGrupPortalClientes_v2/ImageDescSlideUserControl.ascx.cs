﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.Threading;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class ImageDescSlideUserControl : UserControl
    {
        #region Private_Properties

        private string height;
        private string width;
        private string link;
        private string image_url;

        private string productModel;
        private string productDescription;
        private string productPrice;
        private string cssClass;
        private string imgErrorUrl;
        #endregion

        #region Public_Properties

        public string Height
        {
            get { return height; }
            set { height = value; }
        }
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        public string Link
        {
            get { return link; }
            set { link = value; }
        }
        public string Image_url
        {
            get { return image_url; }
            set { image_url = value; }
        }
        public string ProductModel
        {
            get { return productModel; }
            set { productModel = value; }
        }
        public string ProductDescription
        {
            get { return productDescription; }
            set { productDescription = value; }
        }
        public string ProductPrice
        {
            get { return productPrice; }
            set { productPrice = value; }
        }
        public string CssClass
        {
            get { return cssClass; }
            set { cssClass = value; }
        }
        public string ImgErrorUrl
        {
            get { return imgErrorUrl; }
            set { imgErrorUrl = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
            try
            {
                if (string.IsNullOrEmpty(height))
                    height = "140px";
                if (string.IsNullOrEmpty(link))
                    link = "";
                if (string.IsNullOrEmpty(image_url))
                    image_url = "";
                if (string.IsNullOrEmpty(productModel))
                    productModel = "";
                if (string.IsNullOrEmpty(productDescription))
                    productDescription = "";
                if (string.IsNullOrEmpty(productPrice))
                    productPrice = "";

                imgErrorUrl = "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg';";
                this.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
