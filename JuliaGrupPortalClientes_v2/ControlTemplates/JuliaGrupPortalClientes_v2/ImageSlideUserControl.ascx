﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageSlideUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.ImageSlideUserControl" %>

<div class="sliderIndentation" style="display:block; float:left;
    overflow:hidden;
    width: <%# int.Parse(DataBinder.Eval(this, "width").ToString().Replace("px","")) + 18 %>px;
    height: <%# int.Parse(DataBinder.Eval(this, "height").ToString().Replace("px","")) + 22 %>px;
">
<a class="<%# DataBinder.Eval(this, "cssClass") %>" href="<%# DataBinder.Eval(this, "link") %>">
    <img src="<%# DataBinder.Eval(this, "image_url") %>" 
        width="<%# DataBinder.Eval(this, "Width") %>" 
        height="<%# DataBinder.Eval(this, "height") %>"        
        border="0px"
        onerror = "<%# DataBinder.Eval(this, "imgErrorUrl") %>" 
        alt=""/>
</a>
<% if (!String.IsNullOrEmpty(this.Document_url))  {%>
    <a style="position: relative;
            top: -22px;
            left: 9px;
            width: <%# DataBinder.Eval(this, "width") %>;
	        margin: 0;padding:0;text-align:center;"
        class="pdfcatalog <%# DataBinder.Eval(this, "cssClass") %>" target="_blank" 
        href="<%# DataBinder.Eval(this, "document_url") %>"><img src="/Style%20Library/Julia/img/pdf_ico16.png" style="vertical-align: middle; margin-right: 
5px;"/>pdf</a>
        
<% } %>
</div>