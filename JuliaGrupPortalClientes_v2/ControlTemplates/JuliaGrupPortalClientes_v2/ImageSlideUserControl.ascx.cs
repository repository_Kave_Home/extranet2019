﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.Threading;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class ImageSlideUserControl : UserControl
    {
        #region Private_Properties

        private string height;
        private string width;
        private string link;
        private string image_url;
        private string cssClass;
        private string imgErrorUrl;
        private string document_url;

        #endregion

        #region Public_Properties

        public string Height
        {
            get { return height; }
            set { height = value; }
        }
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        public string Link
        {
            get { return link; }
            set { link = value; }
        }
        public string Image_url
        {
            get { return image_url; }
            set { image_url = value; }
        }
        public string CssClass
        {
            get { return cssClass; }
            set { cssClass = value; }
        }
        public string ImgErrorUrl
        {
            get { return imgErrorUrl; }
            set { imgErrorUrl = value; }
        }
        public string Document_url
        {
            get { return document_url; }
            set { document_url = value; }
        }
        

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                if (string.IsNullOrEmpty(height))
                    height = "140px";
                if (string.IsNullOrEmpty(link))
                    link = "";
                if (string.IsNullOrEmpty(image_url))
                    image_url = "";

                imgErrorUrl = "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg'";

                this.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
