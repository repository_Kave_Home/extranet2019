﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using System.Diagnostics;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class LaFormaLink : UserControl
    {
        User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    this.currentUser = (User)Session["User"];
                    Client cl = (Client)Session["Client"];

                    //if ((this.currentUser.AccessType == "Admin") && (cl.AccesoGDO))
                    if ((this.currentUser.UserHasLaFormaAcces))
                    {
                        LaFormaButton.Visible = true;
                        //lblClient.Visible = false;
                        //this.LinkToManual.NavigateUrl = "/Documents/" + strCultureName + "/Manual.pdf";

                    }
                    else
                    {
                        LaFormaButton.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            }
        }
    }
}
