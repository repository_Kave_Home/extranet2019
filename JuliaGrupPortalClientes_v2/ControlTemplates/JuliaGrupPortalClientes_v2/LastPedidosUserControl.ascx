﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LastPedidosUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.LastPedidosUserControl" %>
<!-- link href="/Style%20Library/Julia/LastPedidos.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationLastPedidos" name="/Style Library/Julia/LastPedidos.css" runat="server"/>
<%--Webpart Pedidos en curso--%>

<script language="javascript" type="text/javascript">
    function RowClick(code, type) {
        OpenOrderInfoDialog(code, type);
    }
    function OpenOrderInfoDialog(code, type) {
        var options = {
            url: "/Pages/OrderInfo.aspx?code=" + code + "&type=" + type + "&IsDlg=1",
            width: 900,
            height: 600,      
            dialogReturnValueCallback: DialogCallback
        };

        SP.UI.ModalDialog.showModalDialog(options);
    }

    function DialogCallback(dialogResult, returnValue) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            RefreshTable();
        }
    }
</script>

<div style="display:block">
<div class="titlepedidoCurso">
<%= this.Title %>
</div>
<div class="salto"/>
<div class="subtitlepedidoCurso">
<%= this.Subtitle %>
</div>
<asp:PlaceHolder ID="LastOrdersHolder" runat="server">
</asp:PlaceHolder>
<div class="linkViewDetails" onclick="location.href='/Pages/OrdersList.aspx'">
    <asp:Label ID="lblLinkViewDetails" CssClass="txtViewDetails" runat="server" />
    <div class="imgViewDetails">
    </div>
</div>
</div>
<%--<div class="containerCurrentOrders">
    <div class="headerCurrentOrders" style="width:100%">
        <div class="txtCurrentOrdersTitle">
            <asp:Label ID="lblCurrentOrdersNum" runat="server" /><asp:Label ID="lblCurrentOrdersTitle"
                runat="server" /></div>
        <div class="txtCurrentOrdersSubtitle">
            <asp:Label ID="lblCurrentOrdersSubtitle" runat="server" /></div>
    </div>
    
    <%--<div class="linesCurrentOrders">
        <div class="itemCurrentOrder">
            <div class="txtCurrentOrder_NameStatus">
                <asp:Label ID="lblCurrentOrderName" CssClass="txtCurrentOrderName" runat="server" />
                <asp:Label ID="lblCurrentOrderStatus" CssClass="txtCurrentOrderStatusPending" runat="server" />
            </div>
            <div class="txtCurrentOrder_DescriptionDate">
                <asp:Label ID="lblCurrentOrderDescription" CssClass="txtCurrentOrderDescription"
                    runat="server" />
                <asp:Label ID="lblCurrentOrderDate" CssClass="txtCurrentOrderDate" runat="server" />
            </div>
        </div>
    </div>
    <asp:Panel ID="pedidosContainer" runat="server">
    </asp:Panel>
    <div class="linkViewDetails" onclick="location.href='/Pages/OrdersList.aspx'">
        <asp:Label ID="lblLinkViewDetails" CssClass="txtViewDetails" runat="server" />
        <div class="imgViewDetails">
        </div>
    </div>
</div>--%>