﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using System.Web.UI.HtmlControls;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class LastPedidosUserControl : UserControl
    {
        User CurrentUser;
        private string title = "title";

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        private string subtitle = "subtitle";

        public string Subtitle
        {
            get { return subtitle; }
            set { subtitle = value; }
        }
        private string details = "details";

        public string Details
        {
            get { return details; }
            set { details = value; }
        }

        private VentaDataAccessObject ventasDAO;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.title = getText("lastOrdersTitle");
            this.subtitle = getText("FollowDetails");
            this.details = getText("seeDetails");
            lblLinkViewDetails.Text = this.details;

            try
            {
                if (!IsPostBack)
                {
                    if (Session["VentasDAO"] == null)
                    {
                        //Carrego Ventas

                        Sesion SesionJulia = new Sesion();
                        ventasDAO = SesionJulia.GetVentas(((Client)Session["Client"]).Code);
                        if (ventasDAO == null)
                            throw new Exception("Error NULL en el GetVentas del cliente seleccionado");
                        Session.Add("VentasDAO", ventasDAO);
                    }
                    else
                    {
                        ventasDAO = (VentaDataAccessObject)Session["VentasDAO"];
                    }
                    List<Venta> ventas = ventasDAO.GetLastVentas(5);
                    LoadData(ventas);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Utils.Logger.WiteLog("LastPedidosUserControl->" + ex.Message, ex.StackTrace);
            }
            //base.OnPreRender(e);
        }

        private void LoadData(List<Venta> ventas)
        {
            //string[] tdclass = new string[3];
            //tdclass[2] = "pedidoStatusCell";
            //foreach (Venta venta in ventas)
            //{
            //    string[] p = new string[3];

            //    HtmlGenericControl a_link = new HtmlGenericControl("a");
            //    a_link.Attributes.Add("class", "LastPedidoLink");
            //    p[0] = string.IsNullOrEmpty(venta.Number) ? "" : venta.Number;
            //    p[1] = string.IsNullOrEmpty(venta.Reference) ? getText("Reference") + " s/n" : getText("Reference") + ": " + venta.Reference;
            //    p[2] = string.IsNullOrEmpty(venta.Puntos) ? "" : venta.Puntos + " €";
            //    a_link.Controls.Add(getPedidoLine(p, "lastPedidosTableLine1", "firstLine", tdclass));

            //    p[0] = (venta.Fecha == null) ? "" : getText("OrderDate") + ": " + getFormattedDate(venta.Fecha);
            //    p[1] = string.IsNullOrEmpty(venta.Volumen) ? "" : venta.Volumen;
            //    p[2] = string.IsNullOrEmpty(venta.Estado) ? "" : venta.Estado;
            //    a_link.Controls.Add(getPedidoLine(p, "lastPedidosTableLine2", "secondLine", tdclass));
                
            //    a_link.Attributes.Add("onClick", String.Format("javascript:RowClick('{0}','{1}')",venta.Number, venta.VentaType()));
            //    this.pedidosContainer.Controls.Add(a_link);
            //}
            try
            {
                foreach (Venta venta in ventas)
                {
                    LastOrdersHolder.Controls.Add(GetOrderLine(venta));
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private HtmlGenericControl GetOrderLine(Venta venta)
        {
           
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes.Add("class", "pedidoCurso");
                div.Attributes.Add("OnClick", String.Format("javascript:RowClick('{0}','{1}')", venta.Number, venta.VentaType()));

                HtmlGenericControl div_img = new HtmlGenericControl("div");
                div_img.Attributes.Add("class", "imgPedidoCurso");

                HtmlGenericControl img = new HtmlGenericControl("img");
                img.Attributes.Add("border", "0px");
                img.Attributes.Add("src", "");
                div_img.Controls.Add(img);

                HtmlGenericControl table = new HtmlGenericControl("table");
                table.Attributes.Add("class", "infoPedidoCurso");

                HtmlGenericControl tr = new HtmlGenericControl("tr");
                HtmlGenericControl td = new HtmlGenericControl("td");
                HtmlGenericControl div_title = new HtmlGenericControl("div");
                div_title.Attributes.Add("class", "titlePedidoCurso");
                div_title.InnerHtml = string.IsNullOrEmpty(venta.Number) ? "" : venta.Number;
                td.Controls.Add(div_title);
                tr.Controls.Add(td);

                HtmlGenericControl td_2 = new HtmlGenericControl("td");
                HtmlGenericControl div_estado = new HtmlGenericControl("div");
                if (venta.Estado == "Pediente")
                    div_estado.Attributes.Add("class", "pendientePedidoCurso");
                else
                    div_estado.Attributes.Add("class", "enviadoPedidoCurso");
                div_estado.InnerHtml = string.IsNullOrEmpty(venta.Estado) ? "" : venta.Estado;
                td_2.Controls.Add(div_estado);

                tr.Controls.Add(td_2);
                table.Controls.Add(tr);

                HtmlGenericControl tr_2 = new HtmlGenericControl("tr");
                HtmlGenericControl td_3 = new HtmlGenericControl("td");
                HtmlGenericControl div_date = new HtmlGenericControl("div");
                div_date.Attributes.Add("class", "datePedidoCurso");
                div_date.InnerHtml = (venta.Fecha == null) ? "" : getText("OrderDate") + ": " + getFormattedDate(venta.Fecha);
                td_3.Controls.Add(div_date);
                tr_2.Controls.Add(td_3);

                HtmlGenericControl td_4 = new HtmlGenericControl("td");
                HtmlGenericControl div_desc = new HtmlGenericControl("div");
                div_desc.Attributes.Add("class", "descPedidoCurso");
                

            if(Session["User"]!= null)
            {
            CurrentUser = (User)Session["User"];
            }

               if ((int)Session["PriceSelector"]==0)
                {
                   div_desc.InnerHtml = venta.Puntos.ToString() +  JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                    
                }
                //REZ 29042013 -- Esta mal la condicion
                //else if ((int)Session["PriceSelector"] == 1)
                else if ((int)Session["PriceSelector"] == 1)
                {
                      div_desc.InnerHtml = venta.Importe.ToString() +  JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                }
                else
                {
                    //REZ 29042013 -- Esta mal la multiplicación
                     //div_desc.InnerHtml = (venta.Importe* CurrentUser.Coeficient) + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                    div_desc.InnerHtml = (venta.Puntos * CurrentUser.Coeficient) + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                }
                            


              
                td_4.Controls.Add(div_desc);

                tr_2.Controls.Add(td_4);
                table.Controls.Add(tr_2);
                div.Controls.Add(table);

                return div;
            
        }

        private string getFormattedDate(DateTime date)
        {
            string fecha = string.Empty;
            fecha = date.Day.ToString() + "." + date.Month + "." + date.Year;

            return fecha;
        }

        private string getText(string text)
        {
            return JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(text);
        }

        /*
         <div class="pedidoCurso" OnClick="javascript: window.open('http://www.google.es')">
		        <div class="imgPedidoCurso">
			        <img border="0px" src="D:\Users\adria.marzo\Documents\Julia\sofa.png"/>
		        </div>
		        <table class="infoPedidoCurso">
			        <tr>
				        <td>
					        <div class="titlePedidoCurso">
						        Chaiselongues Kibuc
					        </div>
				        </td>
				        <td>
					        <div class="enviadoPedidoCurso">
						        Enviado
					        </div>
				        </td>
			        </tr>
			        <tr>
				        <td>
					        <div class="descPedidoCurso">
						        Kibuc Mataró
					        </div>
				        </td>
				        <td>
					        <div class="datePedidoCurso">
						        22.09.2011
					        </div>
				        </td>
			        </tr>
		        </table>
        </div>
         */

        //private HtmlGenericControl getPedidoLine(string[] content, string tableclass, string trclass, string[] tdclass)
        //{
        //    HtmlGenericControl table = new HtmlGenericControl("table");
        //    if (!string.IsNullOrEmpty(tableclass))
        //        table.Attributes.Add("class", tableclass);

        //    HtmlGenericControl tr = new HtmlGenericControl("tr");
        //    if (!string.IsNullOrEmpty(trclass))
        //        tr.Attributes.Add("class", trclass);

        //    for (int i = 0; i < content.Length; i++)
        //    {
        //        HtmlGenericControl td = new HtmlGenericControl("td");
        //        if(tdclass != null && !string.IsNullOrEmpty(tdclass[i]))
        //            td.Attributes.Add("class", tdclass[i]);
        //        td.InnerText = content[i];
        //        tr.Controls.Add(td);
        //    }
        //    table.Controls.Add(tr);

        //    return table;
        //}
    }
}
