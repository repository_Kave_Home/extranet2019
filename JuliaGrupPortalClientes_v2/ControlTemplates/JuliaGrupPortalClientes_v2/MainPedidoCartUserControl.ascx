﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainPedidoCartUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.MainPedidoCartUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<asp:Literal ID="RefreshTableScript" runat="server"></asp:Literal>

<script type="text/javascript">
 


</script>

<!-- content start -->
<%-- Old Code - Carrito Volador --%>
<%-- 
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerMainPedidoCart3" runat="server">
    <AjaxSettings>
     <telerik:AjaxSetting AjaxControlID="Panel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="loadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
       <telerik:AjaxSetting AjaxControlID="RefreshButton">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="loadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGrid1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="loadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="Timer1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="loadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel runat="server" ID="loadingPanel" Skin="Default"></telerik:RadAjaxLoadingPanel>

<asp:Panel runat ="server"  ID="Panel" OnPreRender = "Panel_PreRender">


    <telerik:RadGrid ID="RadGrid1" OnSortCommand="RadGrid1_SortCommand" OnPageIndexChanged="RadGrid1_PageIndexChanged"
        Width="100%" OnPageSizeChanged="RadGrid1_PageSizeChanged" AllowSorting="false" AutoGenerateColumns="false" 
        OnDeleteCommand="RadGrid1_ItemCommand" Culture="es-Es"
        PageSize="5" AllowPaging="True" AllowMultiRowSelection="false" runat="server"  
        GridLines="None">
        <MasterTableView Caption="" CssClass="radGridCarrito" Width="100%" Summary="RadGrid table" >
            <Columns>
                <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id"/>
                <telerik:GridBoundColumn DataField="ArticleCode"  UniqueName="Code" />
                <telerik:GridBoundColumn DataField="Description" UniqueName="Description">
                    <ItemStyle CssClass="DescriptionFIeld" />   
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Num" UniqueName="Num"  />
                <telerik:GridBoundColumn DataField="Price" UniqueName="Price"  />        
                <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ButtonType="ImageButton" />
            </Columns>        
        </MasterTableView>
        <PagerStyle Mode="NumericPages"/>
    </telerik:RadGrid>
    <asp:Timer ID="Timer1" runat="server" Interval="100" OnTick="Timer1_Tick" Enabled="false" /> 
    <asp:Button ID="RefreshButton" runat="server" style="display:none;" OnClick="RefreshButton_Click" CssClass="refreshCarrito"/>
</asp:Panel>
--%>

<asp:Button ID="RefreshButton" runat="server" style="display:none;" OnClick="RefreshButton_Click" CssClass="refreshCarrito"/>

<!-- content end -->
