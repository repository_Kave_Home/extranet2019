﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using Telerik.Web.UI;
using JuliaGrupUtils.Business;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.Collections.Generic;
using JuliaGrupUtils.ErrorHandler;
using System.Globalization;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class MainPedidoCartUserControl : UserControl
    {
        private Order order;
        private User currentUser;
        int iPriceSelector;

        protected void Page_Load(object sender, EventArgs e)
        { }

        public void Refresh()
        {
        }

        public void RefreshButton_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }


        #region old Code - Carrito volador
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    //Cargamos el carrito asincronamente
        //    //LoadData();
        //     iPriceSelector = (Session["PriceSelector"] != null) ? Convert.ToInt32(Session["PriceSelector"]) : 0;

        //    if (!Page.IsPostBack)
        //    {
        //        //Pruebo a cargarlo en cliente
        //        //this.Timer1.Enabled = true;

        //        this.RadGrid1.Columns.FindByUniqueNameSafe("Code").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
        //        this.RadGrid1.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
        //        this.RadGrid1.Columns.FindByUniqueNameSafe("Num").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");

        //        if (iPriceSelector == 0)
        //        {
        //            this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + " (" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") + ")";
        //        }
        //        else if (iPriceSelector == 1)
        //        {
        //            this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + " (" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
        //        }
        //        else
        //        {
        //            this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + " (" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") + ")";
        //        }

        //        //De momento lo dejamos aqui para evitar triple postback. Provar de cargarlo en cliente
        //        this.order = (Order)Session["Order"];
        //        //LoadData();
        //        //Session["Order"] = this.order;
        //        //this.RadGrid1.Rebind();
        //        DataTable data = new DataTable();
        //        data.Columns.Add("ArticleCode");
        //        data.Columns.Add("Description");
        //        data.Columns.Add("Num");
        //        data.Columns.Add("Price");

        //        this.RadGrid1.DataSource = data;
        //        this.RadGrid1.DataBind();
        //    }

        //}


        


//        protected override void CreateChildControls()
//        {
//            try
//            {
//                this.RefreshTableScript.Text = @"<script type=""text/javascript""> function RefreshTable() {
//                                           document.aspnetForm." + this.RefreshButton.ClientID + ".click();} </script>";
//                this.order = (Order)Session["Order"];
//                if (Session["User"] != null)
//                {
//                    this.currentUser = (User)Session["User"];
//                }
//                //RTP :)
//                this.order.OrderClient.UserName = currentUser.UserName ;

//                //LoadData();   //Lo cargamos con Timer
//                //this.RadGrid1.DataBind();
//            }
//            catch (Exception ex)
//            {
//                JuliaGrupUtils.Utils.Logger.WiteLog("MainPedidoCartUserControl->" + ex.Message, ex.StackTrace);
//            }
//            base.CreateChildControls();
//        }
        
        //public void Refresh()
        //{
        //    try
        //    {
        //        /* Actualizamos la order */
        //        this.order = (Order)Session["Order"];
        //        Sesion SesionJulia = new Sesion();
        //        this.order = SesionJulia.GetOrder(this.order.OrderClient);
                
        //        LoadData();
        //        Session["Order"] = this.order;
        //        //this.RadGrid1.Rebind();
        //        this.RadGrid1.DataBind();
        //        //this.Timer1.Enabled = true;

           
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}
        

        //public void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        //{
        //    try
        //    {
        //        //int id = e.Item.ItemIndex;    //02122012 - REZ:: Bug. Al borrar item del carrito de una página que no sea la primera, borra el item de la primera página
        //        int id = e.Item.DataSetIndex;

        //        this.order = (Order)Session["Order"];
        //        this.order.DeleteProduct(id, currentUser.UserName);
        //        LoadData();
        //        Session["Order"] = this.order;
        //        this.RadGrid1.DataBind();
        //    }
        //    catch (NavException ex)
        //    {
        //        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

  
        //protected void RadGrid1_PageSizeChanged(object source, GridPageSizeChangedEventArgs e)
        //{
        //    LoadData();
        //    this.RadGrid1.DataBind();
        //}

        //private void LoadData()
        //{
        //    try
        //    {
        //        DataTable data = new DataTable();
        //        data.Columns.Add("ArticleCode");
        //        data.Columns.Add("Description");
        //        data.Columns.Add("Num");
        //        data.Columns.Add("Price");

        //        if (this.order != null)
        //        {
        //            List<OrderLine> lineas = this.order.ListOrderProducts;
        //            foreach (OrderLine ol in lineas)
        //            {
        //                DataRow row = data.NewRow();
        //                if (!(ol.Product == null))
        //                {
        //                    row["ArticleCode"] = ol.Product.Code;
        //                    if (ol.Product.Description.Length > 24)
        //                    {
        //                        row["Description"] = ol.Product.Description.Substring(0 , 24) + "...";
        //                    }
        //                    else 
        //                    {
        //                        row["Description"] = ol.Product.Description;
        //                    }
        //                    row["Num"] = ol.Quantity;

        //                    if (iPriceSelector == 0)
        //                    {
        //                        row["Price"] = ol.Points;
        //                    }
        //                    else if (iPriceSelector == 1)
        //                    {
        //                        row["Price"] = ol.Price.ToString("N2", new CultureInfo("es-Es", false));
        //                    }
        //                    else
        //                    {
        //                        row["Price"] = (Convert.ToDecimal(ol.Points) * currentUser.Coeficient).ToString("N2", new CultureInfo("es-Es", false)) ;
        //                    }
                            
        //                    data.Rows.Add(row);
        //                }
        //            }
        //        }

        //        this.RadGrid1.DataSource = data;
        //    }
        //    catch (NavException ex)
        //    {
        //        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        //protected void RadGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        //{
        //    LoadData();
        //}

        //protected void RadGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        //{
        //    LoadData();            
        //}

        //public void Timer1_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.order = (Order)Session["Order"];
        //        LoadData();
        //        Session["Order"] = this.order;
        //        this.RadGrid1.Rebind();
        //        this.Timer1.Enabled = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        
        //protected void Panel_PreRender(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);

        //        manager.AjaxSettings.AddAjaxSetting(RadGrid1, RadGrid1, loadingPanel);
        //        manager.AjaxSettings.AddAjaxSetting(Timer1, RadGrid1, loadingPanel);
        //        manager.AjaxSettings.AddAjaxSetting(RefreshButton, RadGrid1, loadingPanel);
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }

        //}

        #endregion

    }
}
