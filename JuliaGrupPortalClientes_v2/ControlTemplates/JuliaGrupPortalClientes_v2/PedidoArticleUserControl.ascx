﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidoArticleUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.PedidoArticleUserControl" %>
    <!-- link href="/Style%20Library/Julia/PedidoAdvanced.css" rel="stylesheet" type="text/css" / -->
    <SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/PedidoAdvanced.css" runat="server"/>
<div class="itemProduct">
    <asp:Image ID="imgProd" CssClass="imgProduct" ImageUrl="/Style%20Library/Julia/img/catalogoProducto1.png"
        runat="server" />
    <div class="txtProductRef">
        <asp:Label ID="lblProductRef" runat="server"></asp:Label></div>
    <div class="txtProductName">
        <asp:Label ID="lblProductName" runat="server"></asp:Label></div>
    <div class="txtProductPrice">
        <asp:Label ID="lblProductPrice" runat="server"></asp:Label></div>
    <asp:Button ID="btAddProd" CssClass="imgProductAdd" BorderStyle="None" BackColor="Transparent"
        runat="server" />
</div>
