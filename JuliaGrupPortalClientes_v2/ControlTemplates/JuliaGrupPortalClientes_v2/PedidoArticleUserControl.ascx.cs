﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class PedidoArticleUserControl : UserControl
    {
        private Article article;

        public Article Article
        {
            get { return article; }
            set { article = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            if (article != null)
            {
                this.lblProductPrice.Text = article.PriceInPoints + " " + LanguageManager.GetLocalizedString("Points");
                this.lblProductName.Text = article.Description;
                this.lblProductRef.Text = article.Code;
            }
        }
    }
}
