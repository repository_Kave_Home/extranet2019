﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentlyViewed.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.RecentlyViewed" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<style>
    .recentprod
    {
        width: 84px;
    }
    
    .recentprod img
    {
        border: 1px solid #ccc;
        background-color: white;
        width: 82px;
        height: 82px;
        display: block;
        float: left;
        margin: 3px;
        margin-right: 2px;
    }
</style>
<script type="text/javascript">
    function checkHistory_jquery(targetId, maxitems) {
        var cookiename = 'LaForma_history' + $("#currentClientCode").val() + '_' + $("#currentUserName").val();
        var imagesPath = '<%=JuliaGrupUtils.Utils.ConstantManager.IntranetURL%>' +
                            '<%=JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb%>' + '/' +
                            '<%=JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl%>'
                            + '/';
        var history = Cookies.get(cookiename);
        var htmlContent = '';

        if (history != "" && history != null) {
            var insert = true;
            var sp = history.toString().split(",");
            var count = 0;
            for (var i = sp.length - 1; i >= 0; i--) {
                var productcode = searchParameterByName('code', sp[i].substring(sp[i].lastIndexOf('?')));
                htmlContent += '<a class="recentprod" href="'
                    + sp[i]
                    + '">'
                    + '<img src="' + imagesPath
                    + productcode
                    + '/'
                    + productcode
                    + '·1A.jpg"/>'
                    + '</a>';
                if (sp[i] == document.URL) {
                    insert = false;
                }
                $('#' + targetId).html(htmlContent);
                count++;
                if (count >= maxitems) break;
            }
            if (insert) {
                sp.push(document.URL);
            }
            Cookies.set(cookiename, sp.toString(), { expires: 30, path: '/' });
        } else {
            var stack = new Array();
            stack.push(document.URL);
            Cookies.set(cookiename, stack.toString(), { expires: 30, path: '/' });
        }
    }

    function clearHistory_jquery(targetId) {
        var cookiename = 'history' + $("#currentClientCode").val() + '_' + $("#currentUserName").val();
        Cookies.remove(cookiename, { path: '/' });
        $('#' + targetId).html("");
        //alert("Visited page links were cleared");
    }

    function searchParameterByName(paramname, searchstring) {
        paramname = paramname.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + paramname + "=([^&#]*)"),
            results = regex.exec(searchstring);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function init_recentPageViews() {
        checkHistory_jquery("recentPageViews", 10);
        $(".recentprod img").attr('onerror', 'this.src="/Style Library/Julia/img/' +
        '<%=(string.IsNullOrEmpty(System.Threading.Thread.CurrentThread.CurrentUICulture.Name)) ? "es-ES" : System.Threading.Thread.CurrentThread.CurrentUICulture.Name%>' +
        '/NODISPONIBLE' + '_1' + '.jpg"');
    }
    _spBodyOnLoadFunctionNames.push("init_recentPageViews");

</script>
<span class="productInfoRelatedTitleText">
    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductosRecientes")%></a></li></span><br />
<br />
<div id="recentPageViews" style="height: 180px; width: 100%; background: #f7f7f7;">
</div>
