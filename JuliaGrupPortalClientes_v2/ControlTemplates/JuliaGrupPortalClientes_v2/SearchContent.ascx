﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchContent.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.SearchContent" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<script type="text/javascript">
    function changePrice(it, field, target, minmax) {
        var result = +$("#" + field).val() + it;
        if ((field == "minPrice" && result <= +$("#minPrice").attr("max") && result >= +$("#minPrice").attr("min")) ||
        (field == "maxPrice" && result >= +$("#maxPrice").attr("min") && result <= +$("#maxPrice").attr("max"))) {
            $("#" + field).val(result);
            $("#" + target).attr(minmax, result);
        }
    };
    function checkPrice(field) {
        if (field == "minPrice" && +$("#minPrice").val() > +$("#minPrice").attr("max")) {
            $("#minPrice").val($("#minPrice").attr("max"));
        }
        if (field == "maxPrice" && +$("#maxPrice").val() < +$("#maxPrice").attr("min")) {
            $("#maxPrice").val($("#maxPrice").attr("min"));
        }
    };
    function addToWishlist(EANcode, code, catalog) {
        var heartelem = $("[id=searchHeart-" + code + "]");
        if (heartelem.hasClass("fa-heart-o")) {
            JuliaClients.Service.searchEANCode(EANcode, $('#currentPriceSelId').val(), $('#currentUserName').val(), $('#currentClientCode').val(), catalog);
            heartelem.removeClass("fa-heart-o");
            heartelem.addClass("fa-heart");
        } else if (heartelem.hasClass("fa-heart")) {
            JuliaClients.Service.deleteWishlistItem(code, $('#currentUserName').val(), $('#currentClientCode').val());
            heartelem.removeClass("fa-heart");
            heartelem.addClass("fa-heart-o");
        };
    };
    function moreCategories(field, button) {
        $("." + field).removeClass(field);
        $("#" + button).css("display", "none");
    };
</script>
<div id="searchProducts" style="display: none; visibility: hidden;">
    <div class="searchProductsFilterData">
        <div class="searchProductsFilterCategory">
            <h5>
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price").ToUpperInvariant() %></h5>
            <div style="margin-bottom: 0.7rem; margin-top: 0.8rem;">
                <span class="priceFilterLabelStyle">Min:</span>
                <div class="btn-minus" id="minusMinPrice" onclick="changePrice(-1,'minPrice', 'maxPrice', 'min')">
                    <i class="fa fa-minus"></i>
                </div>
                <input type="number" class="SearchResultsPriceInput" id="minPrice" onblur="checkPrice('minPrice')" />
                <div class="btn-plus" id="plusMinPrice" onclick="changePrice(1,'minPrice', 'maxPrice', 'min')">
                    <i class="fa fa-plus"></i>
                </div>
            </div>
            <div style="margin-bottom: 0.7rem;">
                <span class="priceFilterLabelStyle">Max:</span>
                <div class="btn-minus" id="minusMaxPrice" onclick="changePrice(-1,'maxPrice', 'minPrice', 'max')">
                    <i class="fa fa-minus"></i>
                </div>
                <input type="number" class="SearchResultsPriceInput" id="maxPrice" onblur="checkPrice('maxPrice')" />
                <div class="btn-plus" id="plusMaxPrice" onclick="changePrice(1,'maxPrice', 'minPrice', 'max')">
                    <i class="fa fa-plus"></i>
                </div>
            </div>
            <div class="filterTransparentButton" onclick="javascript:JuliaClients.Service.filterPrice();return false;">
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SeeResults") %>
            </div>
        </div>
        <div class="searchProductsFilterCategory">
            <h5>
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Categories").ToUpperInvariant() %></h5>
            <div id="categoriesFilter">
            </div>
            <div id="seeMoreLinkCategory" onclick="moreCategories('filterSectionHideContentCategory', 'seeMoreLinkCategory')" style="height: 3rem;display: none;">
                <a class="seeMoreLink"><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Search_MoreResults").ToUpperInvariant() %></a></div>
        </div>
        <div class="searchProductsFilterCategory">
            <h5>
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Color").ToUpperInvariant() %></h5>
            <a href="#">
                <div id="colorFilter">
                </div>
            </a>
            <div id="seeMoreLinkColor" onclick="moreCategories('filterSectionHideContentColor', 'seeMoreLinkColor')" style="height: 3rem;display: none;">
                <a class="seeMoreLink"><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Search_MoreResults").ToUpperInvariant() %></a></div>
        </div>
    </div>
    <div>
        <div class="searchProductsResultsInfo">
            <asp:Label ID="lblResults" runat="server" /><span id="resultsNumber" style="padding-left: 5px;"></span>
        </div>
        <div id="searchProductsDiv">
        </div>
    </div>
</div>
