﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web;
using JuliaGrupUtils.Business;
using System.Collections.Generic;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class SearchContent : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblResults.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Results") + ": ";
        }
    }
}
