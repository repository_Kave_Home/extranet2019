﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SummaryNew.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.SummaryNew" %>


<div class="art-box art-post post-4883 post type-post status-publish format-standard hentry category-diario category-informes category-interdin primero normal">
    <div class="art-box-body art-post-body newspos<%= PagePosition.ToString() %>">
        <div class="art-post-inner art-article">
            <div class="home-texto">
                <div id="SummaryImgDiv" class="SummaryImgDiv" runat="server"/>
                <h2 class="art-postheader">
                    <a id="linktitle" href="#" rel="Bookmark" runat="server"></a>
                </h2>
                <div class="art-postcontent squareNew"><!-- article-content -->
                    <div class="art-postheadericons art-metadata-icons">
                        <span class="art-postcategoryicon" id="categories" runat="server">
                            <span class="categories"><asp:Literal ID="PublicadoEnLbl" runat="server"></asp:Literal></span> 
                        </span>
                    </div>
                    <div class="SummaryDivBottomRow">     
                        
                        <div id="SummaryTextDiv">                                                                
                            <div><p class="featured" id="newsummarytext" runat="server"></p><!-- /article-content --></div>
                            <div class="newsItempreview">           
                                <%-- 
                                <ul class="socialFeedback">
                                    <li id="sociallikes" class="sociallikes" runat="server" style="cursor:hand;"></li>
                                    <div id="listOfLikeFacesContainer" class="SummListOfLikeFacesContainer" runat="server" style="display:none"></div>
                                    <li id="socialComments" class="socialComments" runat="server" style="cursor:hand;"></li>
                                     <div id="listOfCommentFacesContainer" class="SummListOfCommentFacesContainer" runat="server" style="display:none"></div>
                                </ul>
                                
                                <p class="time">
                                    <span class="dots">...</span> </br><asp:Literal id="lblnewdate" runat="server" />
                                </p>
                                --%>
                                  <div class="moreinfo"><a id="MoreInfoLynk" href="#" runat="server"><img src="/Style%20Library/Julia/img/more.jpg" alt="" /><span><asp:Literal id="lblmoreinfo" runat="server" /></span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cleared"></div>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
</div>
