﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Utilities;
using System.Web;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class SummaryNew : UserControl
    {

        #region Properties
        public string Url { get; set; }
        public string Title { get; set; }
        public string ImgUrl { get; set; }
        public string SummaryText { get; set; }
        public int LikesNum { get; set; }
        public int CommentsNum { get; set; }
        public string[] Categories { get; set; }
        public int Id { get; set; }
        public int PagePosition { get; set; }
        //public List<Like> LikesList { get; set; }
        //public List<Comment> CommentsList { get; set; }
        public DateTime NewDate { get; set; }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            this.PublicadoEnLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("PublishedIn"); 
            EnsureChildControls();

            linktitle.InnerText = Title;
            linktitle.HRef = Url;
            //this.lblnewdate.Text = NewDate.ToShortDateString();
            this.lblmoreinfo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NewsMoreInfoLink");
            this.MoreInfoLynk.HRef = Url;
            newsummarytext.InnerText = SPHttpUtility.ConvertSimpleHtmlToText(SummaryText, -1);
            //int numLikes = this.LikesNum;
            //int numComments = this.CommentsNum;
            //sociallikes.InnerText = numLikes + "";
            //socialComments.InnerText = numComments + "";

            //if (numLikes > 0)
            //    sociallikes.Attributes.Add("onclick", "javascript:ShowDivLikesSumm(this," + this.Id + ")");

            //if (numComments > 0)
            //    socialComments.Attributes.Add("onclick", "javascript:ShowDivCommentsSumm(this," + this.Id + ")");

            //ListOfFaces likesFaces = (ListOfFaces)LoadControl(_ListOffacesascxPath);
            //likesFaces.ListOfLikes = this.LikesList;
            //this.listOfLikeFacesContainer.Controls.Add(likesFaces);

            //ListOfFaces commentFaces = (ListOfFaces)LoadControl(_ListOffacesascxPath);
            //commentFaces.ListOfComments = this.CommentsList;
            //this.listOfCommentFacesContainer.Controls.Add(commentFaces);

            if (ImgUrl == null || ImgUrl == "")
            {
                this.SummaryImgDiv.Visible = false;
            }
            else
            {
                this.SummaryImgDiv.InnerHtml = this.ImgUrl;
            }

            int categoriescounter = 0;
            foreach (string cat in Categories)
            {
                var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                nameValues.Remove("page");  //REZ - 27/11/2012 -- Al canviar de categoria no te sentit mantenir la pagina perque cal reindexar
                nameValues.Remove("category");
                nameValues.Set("category", cat);

                //categories.Controls.Add(new HyperLink()
                //{
                //    Text = (categoriescounter > 0)?", "+cat:cat,
                //    NavigateUrl = Page.Request.Url + ((HttpContext.Current.Request.QueryString.Count == 0)?"?":"&") + "category=" + cat
                //});
                categories.Controls.Add(new LiteralControl()
                {
                    Text = ((categoriescounter > 0) ? ", " : "") + "<a href=\"" + Page.Request.Url.AbsolutePath + "?" + nameValues.ToString() + "\">" + cat + "</a>"
                });
                categoriescounter++;
            }
        }
    }
}
