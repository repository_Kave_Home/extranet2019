﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using System.Collections.Generic;
using JuliaGrupUtils.DataAccessObjects;
using System.Linq;
using System.Diagnostics;

namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class TopMenu : UserControl
    {
        User currentUser;
        Client currentClient;
        int tipoCompra;
        List<TopMenuItem> topMenuItems;
        List<TopMenuItem> subcategoriesMenuItems;
        List<TopMenuItem> subsubcategoriesMenuItems;
        List<TopMenuItem> estanciasMenuItems;
        List<TopMenuItem> estilosMenuItems;

        public List<TopMenuItem> TopMenuCategories
        {
            get
            {
                return topMenuItems.Where(p => p.Tipo == "0").ToList();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.currentUser = (User)Session["User"];
                this.currentClient = (Client)Session["Client"];
                this.tipoCompra = 0; //LaForma                

                TopMenuDAO topMenuDAO = new TopMenuDAO(currentUser.UserName, currentClient.Code, tipoCompra, currentUser.Language);
                topMenuItems = topMenuDAO.MenuItems;

                JuliaTopMenu.DataSource = TopMenuCategories;
                JuliaTopMenu.DataBind();

                subcategoriesMenuItems = new List<TopMenuItem>();
                subsubcategoriesMenuItems = new List<TopMenuItem>();
                estanciasMenuItems = new List<TopMenuItem>();
                estilosMenuItems = new List<TopMenuItem>();
            }
        }

        protected void lnkbtnclose_click(object sender, EventArgs e)
        {
            try
            {
                Sesion SesionJulia = new Sesion();
                this.currentUser = (User)Session["User"];
                Client cl = (Client)Session["Client"];
                SesionJulia.CleanArticleCache(currentUser.UserName, cl.Code);
            }
            catch (Exception ex)
            {
                //Session expired
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            }
            Session.Clear();
            Page.Response.Redirect("/_layouts/SignOut.aspx");
        }


        protected void JuliaTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                string Code = DataBinder.Eval(e.Item.DataItem, "Codigo").ToString().Trim();
                //Obtenemos subcategorias
                if (subcategoriesMenuItems != null) subcategoriesMenuItems.Clear();
                GetSubMenuItems(Code, "1", ref subcategoriesMenuItems);  //Tipo 1 = subcategoria
                Repeater SubLevelMenuRepeater = (Repeater)e.Item.FindControl("SubcategoriesMenuRepeater");
                SubLevelMenuRepeater.DataSource = subcategoriesMenuItems;
                SubLevelMenuRepeater.DataBind();
                SubLevelMenuRepeater.Visible = (subcategoriesMenuItems.Count() > 0) ? true : false;                

                //Obtenemos estilos
                if (estilosMenuItems != null) estilosMenuItems.Clear();
                GetSubMenuItems(Code, "2", ref estilosMenuItems);  //Tipo 2 = Estilos
                Repeater SubLevel3MenuRepeater = (Repeater)e.Item.FindControl("EstilosMenuRepeater");
                SubLevel3MenuRepeater.DataSource = estilosMenuItems;
                SubLevel3MenuRepeater.DataBind();
                SubLevel3MenuRepeater.Visible = (estilosMenuItems.Count() > 0) ? true : false;

                //Obtenemos estancias
                if (estanciasMenuItems != null) estanciasMenuItems.Clear();
                GetSubMenuItems(Code, "3", ref estanciasMenuItems);  //Tipo 3 = Estancias
                Repeater SubLevel2MenuRepeater = (Repeater)e.Item.FindControl("EstanciasMenuRepeater");
                SubLevel2MenuRepeater.DataSource = estanciasMenuItems;
                SubLevel2MenuRepeater.DataBind();
                SubLevel2MenuRepeater.Visible = (estanciasMenuItems.Count() > 0) ? true : false;



                Repeater CategoryFavoriteProduct = (Repeater)e.Item.FindControl("CategoryFavoriteProduct");
                ArticleDataAccessObject articles = (ArticleDataAccessObject)Session["ArticleDAO"];
                List<Article> arts = articles.GetAllProducts();
                arts = (from p in arts where !p.CatalogType.Equals("Inspirate") select p).ToList();
                arts = (from p in arts where !p.CatalogType.Equals("Promociones") || p.CatalogNo.StartsWith("PRELIQ") select p).ToList();
                //arts = (from p in arts orderby p.Recomendado descending select p).ToList();

                Article recom = arts.Where(p => p.Recomendado == Code).FirstOrDefault();
                if (recom == null)
                {
                    arts = arts.Where(p => p.CategoriesList.Contains(Code)).Take(1).ToList();
                }
                else
                {
                    arts = arts.Where(p => p.Recomendado == Code).Take(1).ToList();
                }
                uint currentLcId = (uint)System.Threading.Thread.CurrentThread.CurrentUICulture.LCID;
                foreach (Article a in arts)
                {
                    /* 2019-06-04 - Aida Lucha - Shop in shop */
                    
                    a.CalculatePriceWithDiscount(Session["PriceSelector"].ToString(), currentClient.CoefDivisaWEB, currentClient.LiteralDivisaWEB, currentUser.Coeficient, currentLcId, 0, default(decimal), currentClient.ShopInShop);
                    


                }

                CategoryFavoriteProduct.DataSource = arts;
                CategoryFavoriteProduct.DataBind();

            }
        }

        protected void JuliaSubTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {                
                string Code = DataBinder.Eval(e.Item.DataItem, "Codigo").ToString().Trim();
             
                if (subsubcategoriesMenuItems != null) subsubcategoriesMenuItems.Clear();
                foreach (TopMenuItem tItem in subcategoriesMenuItems)
                {
                    GetSubMenuItems(Code.Trim(), "4", ref subsubcategoriesMenuItems);  //Tipo 4 = sub subcategoria

                }
                Repeater SubsubLevelMenuRepeater = (Repeater)e.Item.FindControl("SubsubcategoriesMenuRepeater");
                SubsubLevelMenuRepeater.DataSource = subsubcategoriesMenuItems;
                SubsubLevelMenuRepeater.DataBind();
                SubsubLevelMenuRepeater.Visible = (subsubcategoriesMenuItems.Count() > 0) ? true : false;                
            }
        }
        protected void GetSubMenuItems(string key, string tipo, ref List<TopMenuItem> LevelMenuItems)
        {
            LevelMenuItems = topMenuItems.Where(p => p.Tipo == tipo && p.ParentCode.ToUpper() == key.ToUpper()).ToList();
        }

    }
}
