﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserHeader.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2.UserHeader" %>
<style type="text/css">
    .ClientLogo
    {
        position: relative;
        top: -105px;
        left: 800px;
        margin-top: 25px;
    }
    .UserLogo, .UserHeader
    {

     overflow:hidden;   
     float:left;
     }

</style>
<%-- script type="text/javascript">
$(document).ready(function () {
    $(".ClientLogo").each(function () {
        $(this).error(function () {
            $(".ClientLogo").css("display", "none");
        });
        $(this).attr("src", $(this).attr("src"));
    });
});
</script --%>

<SharePoint:SPLinkButton runat="server" NavigateUrl="~sitecollection/" ID="onetidProjectPropertyTitleGraphic" CssClass="UserLogo">
    <asp:Image ID="HeaderImage" runat="server" CssClass="UserHeader" />
    <%-- asp:Image ID="ClientLogo" runat="server" Height="50px" Width="157px" CssClass="ClientLogo" Visible="false" / --%>
</SharePoint:SPLinkButton>

<script language="javascript" type="text/javascript">
    var searchStickyClick = function () {
        $("#headerSearch").toggleClass("searchBoxStickyOn");
        $("#searchProducts").toggleClass("searchContentStickyOn");
    };

    $(document).ready(function () {

        $('#searchIconSticky').click(function () {
            $("#headerSearch").toggleClass("searchBoxStickyOn");
            $("#searchProducts").toggleClass("searchContentStickyOn");
        });


        //Check to see if the window is top if not then display button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 60) {
                $('.menu').removeClass("menu").addClass("menu menuStickyOn");
                $('#myaccountmenuTitle').removeClass("myAccountStickyOn").addClass("myAccountStickyOn");
                $('.searchStickyOff').removeClass("searchStickyOff").addClass("searchStickyOff iconsStickyOn");
                $('.userStickyOff').removeClass("userStickyOff").addClass("userStickyOff iconsStickyOn");
                $('.cartStickyOff').removeClass("cartStickyOff").addClass("cartStickyOff iconsStickyOn");
            } else {
                $('.menu').removeClass("menuStickyOn");
                $('.searchStickyOff').removeClass("iconsStickyOn");
                $('.userStickyOff').removeClass("iconsStickyOn");
                $('.cartStickyOff').removeClass("iconsStickyOn");
                $('#myaccountmenuTitle').removeClass("myAccountStickyOn");

                //$('.iconsStickyOn').unbind("click", searchStickyClick);
                
                $("#headerSearch").removeClass("searchBoxStickyOn");
                $("#searchProducts").removeClass("searchContentStickyOn");
            }
        });
    });

    </script>
