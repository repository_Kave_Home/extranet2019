﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils;
using Microsoft.SharePoint;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Linq;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using System.Diagnostics;
using System.IO;
using System.Web;


namespace JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2
{
    public partial class UserHeader : UserControl
    {
        private User currentUser;
        private Client currentClient;
        private ArticleDataAccessObject articleDAO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["User"] != null)
                    {
                        this.currentUser = (User)Session["User"];
                    }
                    if (Session["Client"] != null)
                    {
                        this.currentClient = (Client)Session["Client"];
                    }

                    GetClientBusinessLine();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Utils.Logger.WiteLog("ArticlesCatalogUserControl->" + ex.Message, ex.StackTrace);
            }
        }
        private void getClientLogo()
        {
              
               string URLImage = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ClientLogo&ClientCode=" + currentClient.Code;
               
               //if (string.IsNullOrEmpty(this.ClientLogo.ImageUrl))
               //{ 
               //    this.ClientLogo.ImageUrl = URLImage;
               //}
               //this.ClientLogo.Attributes["onerror"] = "$(this).css('display', 'none');";

               //22032016 - Devolvemos la Url ya que la asignaremos al mismo control substituyendo el logo de Julià
               this.HeaderImage.ImageUrl = URLImage;
        }

        private void GetClientBusinessLine()
        {
            try
            {
                string cacheKey = "ClientBusinessLine" + currentUser.UserName + currentClient.Code;
                string strHeaderImageUrl;
               
                    switch (currentClient.HeaderImage){
                        case 0: //Banner LaForma + Logo Client
                            strHeaderImageUrl = "/Style Library/Julia/img/Top_laforma.png";
                            getClientLogo();
                            break;
                        case 1: //Banner eCommerce + Logo Client
                            strHeaderImageUrl = "/Style Library/Julia/img/Top_ecommerce.png";
                            getClientLogo();
                            break;
                        case 2: //Banner Client + Error Banner La forma
                            strHeaderImageUrl = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetListUrl + "/" + currentClient.Code + "/ClientBanner.png";
                            this.HeaderImage.Attributes["onerror"] = "this.src='/Style Library/Julia/img/Top_laforma.png';";
                            break;
                        case 3: //Banner Client + Error Banner eCommerce
                            strHeaderImageUrl = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetListUrl + "/" + currentClient.Code + "/ClientBanner.png";
                            this.HeaderImage.Attributes["onerror"] = "this.src='/Style Library/Julia/img/Top_ecommerce.png';";
                            break;
                        case 4: //Banner InStyle + Logo Client
                            strHeaderImageUrl = "/Style Library/Julia/img/Top_case4.png";
                            getClientLogo();
                            break;
                        case 5: //Banner Client + Error Banner InStyle
                            strHeaderImageUrl = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetListUrl + "/" + currentClient.Code + "/ClientBanner.png";
                            this.HeaderImage.Attributes["onerror"] = "this.src='/Style Library/Julia/img/Top_case4.png';";
                            break;
                        default:    
                            strHeaderImageUrl = "/Style Library/Julia/img/Top_laforma.png";
                            break;
                    }

                //this.HeaderImage.ImageUrl = strHeaderImageUrl;
                //22032016 - Si no se ha asignado logo de cliente, asignamos el que toque. Si se ha asignado el de cliente lo ponemos por si no se encuentra.
                    if (String.IsNullOrEmpty(this.HeaderImage.ImageUrl)) this.HeaderImage.ImageUrl = strHeaderImageUrl;
                    else this.HeaderImage.Attributes["onerror"] = "this.src='" + strHeaderImageUrl + "';";

                //REZ 19032014 - Lo obtenemos solo para los casos 0 y 1 de HeaderImage
                //getClientLogo();
            }
            catch (Exception ex)
            {
                this.HeaderImage.ImageUrl = "/Style Library/Julia/img/Top_laforma.png"; 
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
