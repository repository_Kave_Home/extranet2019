﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
//using Microsoft.Office.DocumentManagement.DocumentSets;
using System.IO;
//using JuliaGrupUtils.SecureStoreUtils;
using JuliaGrupUtils.Utils;
using System.Linq;
using System.Xml.Linq;
using System.Threading;
using System.Web;
using System.Diagnostics;
using JuliaGrupUtils.DataAccessObjects;

namespace JuliaGrupPortalClientes.Layouts.JuliaGrupPortalClientes
{
    public partial class JuliaGrupPortalClientesDocuments : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string operation = Request.QueryString["operation"];
            if (!String.IsNullOrEmpty(operation))
            {
                switch (operation.ToLower())
                { 
                    case "image":
                        this.GetImages();
                        break;
                    case "imageid":
                        GetIntranetImage(Request.QueryString["id"], Request.QueryString["type"]);
                        break;
                    case "document":
                        GetIntranetDocument(Request.QueryString["id"]);
                        break;
                    case "bill":
                        GetIntranetClientBill(Request.QueryString["ClientId"], Request.QueryString["FileName"]);
                        break;
                    case "deliverynote":
                        GetIntranetClientDeliveryNote(Request.QueryString["ClientId"], Request.QueryString["FileName"]);
                        break;
                    case "clientinfo":
                        GetClientInfo(Request.QueryString["ClientCode"], Request.QueryString["Username"]);
                        break;
                    case "informevista":
                        GetInformeVista(Request.QueryString["UserCode"], Request.QueryString["ClientCode"], Request.QueryString["codCatalog"], Request.QueryString["familia"], Request.QueryString["subfamilia"], Request.QueryString["programa"], Request.QueryString["filtre"], Request.QueryString["Unidad"], Request.QueryString["Dto"], Request.QueryString["prov"], Request.QueryString["Gdo"], Request.QueryString["divisa"], Request.QueryString["TipoUnion"]);
                        break;
                    case "ofertapdf":
                        GetOfertaPDF(Request.QueryString["ClientCode"], Request.QueryString["OfertaNo"], Request.QueryString["Selector"], Request.QueryString["TipoCompra"]);
                        break;
                    case "imageweb":
                        GetImaWeb(Request.QueryString["Code"], Request.QueryString["Cataleg"],Request.QueryString["Versio"]);
                        break;
                    case "clientlogo":
                        GetLogo(Request.QueryString["ClientCode"]);
                        break;

                    default:
                        HttpContext.Current.Response.StatusCode = 404;
                        HttpContext.Current.Response.End();
                        break;
                }
            }
        }
        private void GetLogo(string ClientCode)
        {

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                 {
                     using (SPSite site = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                     using (SPWeb web = site.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                     {


                         SPFile file = web.GetFile(JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetListUrl + "/" + ClientCode + "/ClientLogo.png");
                         if (file.Exists)
                         {
                             Image image = null;
                             SPFile imageStream = web.GetFile(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetListUrl + "/" + ClientCode + "/ClientLogo.png");
                             Response.ContentType = "image/png";
                             image = Image.FromStream(imageStream.OpenBinaryStream());
                             image.Save(Response.OutputStream, ImageFormat.Png);
                         }
                         else
                         {
                             throw new Exception("Not found");
                         }

                     }

                 });
            }
            catch (Exception ex) {
                HttpContext.Current.Response.StatusCode = 404;
                HttpContext.Current.Response.End();
            }      
                  

        }
        private void GetImaWeb(string code, string Cataleg, string Versio)
        {
            string extension = string.Empty;
            SPFile imageStream = this.GetImaWebByCodArticleAndVersion(code,Cataleg,Versio ,ref extension);
            Image image = null;
            Response.Clear();
            if (imageStream != null)
            {
                switch (extension.ToUpper())
                {
                    case ".JPG":
                        Response.ContentType = "image/jpeg";
                        image = Image.FromStream(imageStream.OpenBinaryStream());
                        image.Save(Response.OutputStream, ImageFormat.Jpeg);
                        break;
                    case ".JPEG":
                        Response.ContentType = "image/jpeg";
                        image = Image.FromStream(imageStream.OpenBinaryStream());
                        image.Save(Response.OutputStream, ImageFormat.Jpeg);
                        break;
                    case ".PNG":
                        Response.ContentType = "image/png";
                        image = Image.FromStream(imageStream.OpenBinaryStream());
                        image.Save(Response.OutputStream, ImageFormat.Png);
                        break;
                    case ".GIF":
                        Response.ContentType = "image/gif";
                        image = Image.FromStream(imageStream.OpenBinaryStream());
                        image.Save(Response.OutputStream, ImageFormat.Gif);
                        break;
                }
            }
        }

     
        private void GetOfertaPDF(string ClientCode, string OfertaNo, string vista, string TipoCompra)
        {
            OrderDataAccesObject orderDAO = new OrderDataAccesObject();

            string doc = string.Empty;
            doc = orderDAO.GetOfertaPdf(ClientCode, OfertaNo, vista, Convert.ToInt32(TipoCompra));


            if (doc.Contains("<Base64>"))
            {
                byte[] todecode_byte = Convert.FromBase64String(doc.Replace("<Base64>", "").Replace("</Base64>", ""));
                try
                {

                    Response.Clear();
                    MemoryStream stream = new MemoryStream(todecode_byte);

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-length", stream.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=InformeVista.pdf");
                    Response.BinaryWrite(stream.ToArray());
                    Response.Flush();
                    stream.Close();
                    Response.End();
                }
                catch { }

            }
            else
            {
                string HtmlResponseContet = "<html><head><script>alert('" + doc + "'); window.close();</script></head><body></body></html>";
                Response.Clear();
                Response.Write(HtmlResponseContet);
                Response.Flush();

                Response.End();
            }

        }

        private void GetInformeVista(string usercode, string clientcode, string codCatalog, string familia, string subfamilia, string programa, string filtre, string unidades, string filtroDto, string proveedor, string Gdo, string divisa, string tipoUnion)
        {
            string doc = string.Empty;
            ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(usercode, clientcode,0);
            filtroDto = (String.IsNullOrEmpty(filtroDto)) ? "0" : filtroDto;
            tipoUnion = (String.IsNullOrEmpty(tipoUnion)) ? String.Empty : tipoUnion;
            if (!String.IsNullOrEmpty(Gdo)) {
                doc = articleDAO.GetInformeProductosGdo(usercode, clientcode, proveedor, familia, subfamilia, programa, filtre, divisa, Convert.ToInt32(filtroDto), tipoUnion);
            }
            else
            {
                doc = articleDAO.GetInformeProductos(usercode, clientcode, codCatalog, familia, subfamilia, programa, filtre, Convert.ToInt32(unidades), Convert.ToInt32(filtroDto));
            }
            if (doc.Contains("<Base64>"))
            {
                byte[] todecode_byte = Convert.FromBase64String(doc.Replace("<Base64>", "").Replace("</Base64>", ""));
                try
                {

                    Response.Clear();
                    MemoryStream stream = new MemoryStream(todecode_byte);

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-length", stream.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=InformeVistaGdo.pdf");
                    Response.BinaryWrite(stream.ToArray());
                    Response.Flush();
                    stream.Close();
                    Response.End();
                }
                catch { }

            }
            else
            {
                string HtmlResponseContet = "<html><head><script>alert('" + doc + "'); window.close();</script></head><body></body></html>";
                Response.Clear();
                Response.Write(HtmlResponseContet);
                Response.Flush();

                Response.End();
            }

         
           
        }

        private void GetClientInfo(string CodClient, string Username)
        {

            ClientDataAccessObject clientDAO = new ClientDataAccessObject();
            string doc = clientDAO.GetInformeCliente(Username, CodClient);
            if (doc.Contains("<Base64>"))
            {
                byte[] todecode_byte = Convert.FromBase64String(doc.Replace("<Base64>", "").Replace("</Base64>", ""));
                try
                {

                    Response.Clear();
                    MemoryStream stream = new MemoryStream(todecode_byte);

                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-length", stream.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=InformeVista.pdf");
                    Response.BinaryWrite(stream.ToArray());
                    Response.Flush();
                    stream.Close();
                    Response.End();
                }
                catch { }

            }
            else
            {
                string HtmlResponseContet = "<html><head><script>alert('" + doc + "'); window.close();</script></head><body></body></html>";
                Response.Clear();
                Response.Write(HtmlResponseContet);
                Response.Flush();

                Response.End();
            }

        }

       
        private MemoryStream GetFileStream(string fileID, ref string filename, string type)
        {
            MemoryStream stream = new MemoryStream();
            string name = string.Empty;

            if (!string.IsNullOrEmpty(fileID))
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            SPList list = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];

                            if (!String.IsNullOrEmpty(type))
                            {
                                fileID = fileID.Replace("·1", "·" + type);
                            }
                            SPQuery query = new SPQuery();
                            query.Query =
                            //"<Where><Eq><FieldRef Name=\"_dlc_DocId\" /><Value Type=\"Text\">" + fileID + "</Value></Eq></Where>";
                            "<Where><Eq><FieldRef Name=\"FileLeafRef\" /><Value Type=\"Text\">" + fileID + "</Value></Eq></Where>";
                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //query.ViewFields = "<FieldRef Name=\"_dlc_DocId\" />";
                            query.ViewFields = "<FieldRef Name=\"FileLeafRef\" />";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPListItemCollection results = list.GetItems(query);

                            foreach (SPListItem item in results)
                            {
                                if (item.FileSystemObjectType == SPFileSystemObjectType.File)
                                {
                                    name = item.Name;
                                    using (Stream fileStream = item.File.OpenBinaryStream())
                                    {
                                        byte[] buffer = new byte[4096];
                                        int sourceBytes;
                                        do
                                        {
                                            sourceBytes = fileStream.Read(buffer, 0, buffer.Length);
                                            stream.Write(buffer, 0, sourceBytes);
                                        } while (sourceBytes > 0);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                });
            }
            filename = name;

            return stream;
        }

        private MemoryStream GetFileStream(string fileID, ref string filename)
        {
            MemoryStream stream = new MemoryStream();
            string name = string.Empty;

            if (!string.IsNullOrEmpty(fileID))
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            SPList list = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];

                            SPQuery query = new SPQuery();
                            query.Query =
                                "<Where><Eq><FieldRef Name=\"_dlc_DocId\" /><Value Type=\"Text\">" + fileID + "</Value></Eq></Where>";
                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            query.ViewFields = "<FieldRef Name=\"_dlc_DocId\" />";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPListItemCollection results = list.GetItems(query);

                            foreach (SPListItem item in results)
                            {
                                if (item.FileSystemObjectType == SPFileSystemObjectType.File)
                                {
                                    name = item.Name;
                                    using (Stream fileStream = item.File.OpenBinaryStream())
                                    {
                                        byte[] buffer = new byte[4096];
                                        int sourceBytes;
                                        do
                                        {
                                            sourceBytes = fileStream.Read(buffer, 0, buffer.Length);
                                            stream.Write(buffer, 0, sourceBytes);
                                        } while (sourceBytes > 0);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                });
            }
            filename = name;

            return stream;
        }


        private MemoryStream GetFileStream(string ClientCode,  string FileName)
        {
            MemoryStream stream = new MemoryStream();
            string name = string.Empty;


                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {

                            SPList list = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetList];
                            SPFolder folder = web.GetFolder(list.RootFolder.ServerRelativeUrl + "/" + ClientCode);
                            SPFile oFile = web.GetFile(folder.Url + "/" +  FileName);

                            using (Stream fileStream = oFile.OpenBinaryStream())
                            {
                                byte[] buffer = new byte[4096];
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fileStream.Read(buffer, 0, buffer.Length);
                                    stream.Write(buffer, 0, sourceBytes);
                                } while (sourceBytes > 0);

                            }
                        }
                    }
                });
            

            return stream;
        }

        public void GetIntranetImage(string imgID, string type)
        {
            string filename = string.Empty;
            MemoryStream stream = GetFileStream(imgID, ref filename, type);
            
            try
            {
                Response.Clear();

                if (string.IsNullOrEmpty(filename))
                {
                    string ImageType = string.IsNullOrEmpty(Request.QueryString["type"]) ? "1" : Request.QueryString["type"];
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    Response.Redirect("/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_" + ImageType + ".jpg");
                }
                else
                {
                    string extension = Path.GetExtension(filename);
                    switch (extension.ToUpper())
                    {
                        case ".JPG":
                            Response.ContentType = "image/jpeg";
                            break;
                        case ".JPEG":
                            Response.ContentType = "image/jpeg";
                            break;
                        case ".PNG":
                            Response.ContentType = "image/png";
                            break;
                        case ".GIF":
                            Response.ContentType = "image/gif";
                            break;
                    }
                    Response.AddHeader("content-length", stream.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");
                    Response.BinaryWrite(stream.ToArray());
                    Response.Flush();
                    stream.Close();
                    Response.End();
                }
            }
            catch { }
        }

        public void GetIntranetDocument(string docID)
        {
            string filename = string.Empty;
            MemoryStream stream = GetFileStream(docID, ref filename);

            try
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-length", stream.Length.ToString());
                Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");
                Response.BinaryWrite(stream.ToArray());
                Response.Flush();
                stream.Close();
                Response.End();
            }
            catch { }
        }

        public void GetIntranetClientBill(string ClientID, string filename)
        {

            MemoryStream stream = GetFileStream(ClientID, filename);

            try
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-length", stream.Length.ToString());
                Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");
                Response.BinaryWrite(stream.ToArray());
                Response.Flush();
                stream.Close();
                Response.End();
            }
            catch { }
        }

        public void GetIntranetClientDeliveryNote(string ClientID, string filename)
        {
            MemoryStream stream = GetFileStream(ClientID, filename);

            try
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-length", stream.Length.ToString());
                Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");
                Response.BinaryWrite(stream.ToArray());
                Response.Flush();
                stream.Close();
                Response.End();
            }
            catch { }
        }

        public void GetImages()
        {
            Response.Clear();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                string extension = string.Empty;
                //SPFile imageStream = this.GetIntranetImageByCodArticle(Request.QueryString["id"], ref extension);

                string id = string.IsNullOrEmpty(Request.QueryString["id"]) ? "":Request.QueryString["id"];
                string type = string.IsNullOrEmpty(Request.QueryString["type"]) ? "·1" : "·" + Request.QueryString["type"];
                string p = string.IsNullOrEmpty(Request.QueryString["p"]) ? "1" : Request.QueryString["p"];
                
                //REZ:: prova 28/08/2012 segueix anant lent
                //using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                //{
                //    using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                //    {
                //        SPFile imagefile = web.GetFile(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                //                        JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" +
                //                        id + "/" + id + type + ".jpg");
                //        if (imagefile.Exists)
                //        {
                //            Response.Redirect(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                //                        JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" +
                //                        id + "/" + id + type + ".jpg");
                //        }
                //        else
                //        {
                //            //Response.Redirect("/Style%20Library/Julia/img/catalogoProducto1.png");
                //            string ImageType = string.IsNullOrEmpty(Request.QueryString["type"]) ? "1" : Request.QueryString["type"];
                //            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                //            Response.Redirect("/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_" + ImageType + ".jpg");
                //        }
                //    }
                //}

                SPFile imageStream = this.GetIntranetImageByCodArticle(id, type, ref extension, p);
                Image image = null;

                if (imageStream != null)
                {
                    switch (extension.ToUpper())
                    {
                        case ".JPG":
                            Response.ContentType = "image/jpeg";
                            image = Image.FromStream(imageStream.OpenBinaryStream());
                            image.Save(Response.OutputStream, ImageFormat.Jpeg);
                            break;
                        case ".JPEG":
                            Response.ContentType = "image/jpeg";
                            image = Image.FromStream(imageStream.OpenBinaryStream());
                            image.Save(Response.OutputStream, ImageFormat.Jpeg);
                            break;
                        case ".PNG":
                            Response.ContentType = "image/png";
                            image = Image.FromStream(imageStream.OpenBinaryStream());
                            image.Save(Response.OutputStream, ImageFormat.Png);
                            break;
                        case ".GIF":
                            Response.ContentType = "image/gif";
                            image = Image.FromStream(imageStream.OpenBinaryStream());
                            image.Save(Response.OutputStream, ImageFormat.Gif);
                            break;
                    }
                }
                else
                {
                    //Response.Redirect("/Style%20Library/Julia/img/catalogoProducto1.png");
                    string ImageType = string.IsNullOrEmpty(Request.QueryString["type"]) ? "1" : Request.QueryString["type"];
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    Response.Redirect("/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_" + ImageType + ".jpg");
                }
            }
        }

        //REZ:: 28082012 -- No se hace servir
        //public SPFile GetIntranetImageByCodArticle(string codArticle, ref string extension)
        //{
        //    SPFile result = null;
        //    try
        //    {
        //        string resultextension = string.Empty;

        //        SPSecurity.RunWithElevatedPrivileges(delegate(){
        //            using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
        //            {
        //                using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
        //                {
        //                    SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + codArticle);
        //                    SPContentType docType = folder.DocumentLibrary.ContentTypes["Product image"];
        //                    //TODO - CUTRE A ARREGLAR
        //                    string file_name = string.Empty;
        //                    //file_name += Request.QueryString["id"];
        //                    file_name += string.IsNullOrEmpty(Request.QueryString["type"]) ? string.Empty : "·" + Request.QueryString["type"];
        //                    foreach (SPFile file in folder.Files)
        //                    {
        //                        if (file.Item["ContentTypeId"].ToString() == docType.Id.ToString() && file.Name.Contains(file_name))
        //                        {
        //                            resultextension = Path.GetExtension(file.Name);
        //                            result = file;
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        });
        //        extension = resultextension;
        //    } catch
        //    {
        //    }
        //    return result;


        //    //        {
        //    //            using (SPWeb spwWeb = spsSitio.OpenWeb(SecureStoreUtils.DocumentManagementUrl))
        //    //            {
        //    //                SPList articlesList = spwWeb.Lists[SecureStoreUtils.ArticlesDocumentLibraryName];
        //    //                SPQuery query = new SPQuery();
        //    //                SPQuery queryDocument = new SPQuery();

        //    //                //agafem la carpeta de l'article que toca, es la que te per titol el codi de l'article                            
        //    //                query.Query = string.Format("<Where><And><Eq><FieldRef Name=\"Article_x0020_code1\" /><Value Type=\"Text\">{0}</Value></Eq><Eq><FieldRef Name='ContentType'/><Value Type='Computed'>Articles Document Set</Value></Eq></And></Where>", codArticle);
        //    //                query.ViewFields = "<FieldRef Name=\"Title\" />";
        //    //                query.ViewAttributes = "Scope='Recursive'";

        //    //                SPListItemCollection documentSetSeleccionat = articlesList.GetItems(query);
        //    //                DocumentSet articleSeleccionat = DocumentSet.GetDocumentSet(documentSetSeleccionat[0].Folder);

        //    //                queryDocument.Folder = articleSeleccionat.Folder;
        //    //                queryDocument.Query = @"<Where><Eq><FieldRef Name=""ContentType"" /><Value Type=""Computed"">Assembly instructions (image)</Value></Eq></Where>";
        //    //                SPListItemCollection listArticleSeleccionat = articlesList.GetItems(queryDocument);

        //    //                SPFile imatgeArticle = listArticleSeleccionat[0].File;

        //    //                resultextension = Path.GetExtension(imatgeArticle.Name);
        //    //                result = imatgeArticle;

        //    //            }
        //    //        }
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        Console.Write(e);
        //    //    }


        //    //});
        //    //extension = resultextension;
        //    //return result;
        //}

        //Consultamos la lista de imagenes del producto y devolvemos la de la posicion indicada
        //ES importante que la funcion sea la misma que monta la lista de imagenes de producto que se encuentra en ArticleDataAccessObject
        //REZ:: 28082012:: Lo pasamos a ArticleDataAccessObject

        public SPFile GetImaWebByCodArticleAndVersion(string codArticle,string cataleg ,string version, ref string extension)
        {
            
            SPFile output = null;
            try
            {
                string resultextension = string.Empty;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {

                            //SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + codArticle);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + codArticle);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    
                                                    <Eq>
                                                        <FieldRef Name=""ContentType"" />
                                                            <Value Type=""Computed"">Assembly instructions (image)</Value>
                                                    </Eq>
                                                     
                                                   
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList list = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = list.GetItems(query);

                            foreach (SPListItem image in imagescollection)
                            {
                              
                                    resultextension = Path.GetExtension(image.File.Name);
                                    output = image.File;
                            }


                            //foreach (SPFile file in folder.Files)
                            //{
                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
                            //        && IsTypedImage(file.Item.Name, type))
                            //    {
                            //        cpos++;
                            //        if (cpos == int.Parse(position))
                            //        {
                            //            resultextension = Path.GetExtension(file.Name);
                            //            output = file;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                    }
                });
                extension = resultextension;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            }

            return output;
        }

        public SPFile GetIntranetImageByCodArticle(string codArticle, string type, ref string extension, string position)
        {
            int cpos = 0;
            SPFile output = null;
            try
            {
                string resultextension = string.Empty;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {

                            //SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + codArticle);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + codArticle);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <And>
                                                        <Eq>
                                                            <FieldRef Name=""ContentType"" />
                                                                <Value Type=""Computed"">Product image</Value>
                                                        </Eq>
                                                        <Contains>
                                                            <FieldRef Name=""FileLeafRef"" />
                                                            <Value Type=""File"">" + type + @"</Value>
                                                        </Contains>
                                                    </And>
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList list = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = list.GetItems(query);

                            foreach (SPListItem image in imagescollection)
                            {
                                cpos++;
                                if (cpos == int.Parse(position))
                                {
                                    resultextension = Path.GetExtension(image.File.Name);
                                    output = image.File;
                                    break;
                                }
                            }


                            //foreach (SPFile file in folder.Files)
                            //{
                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
                            //        && IsTypedImage(file.Item.Name, type))
                            //    {
                            //        cpos++;
                            //        if (cpos == int.Parse(position))
                            //        {
                            //            resultextension = Path.GetExtension(file.Name);
                            //            output = file;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                    }
                });
                extension = resultextension;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
            }

            return output;
        }

        private bool IsTypedImage(string name, string type)
        {
            if (!string.IsNullOrEmpty(name) && name.Contains(type))
                return true;
            return false;
        }

        private bool IsImgType(SPFolder folder, string doctype)
        {
            SPContentType docType = folder.DocumentLibrary.ContentTypes["Product image"];
            if (doctype == docType.Id.ToString())
                return true;
            return false;
        }

        private bool CheckProperty(SPListItem item, string name, string value)
        {
            try
            {
                if (item != null)
                {
                    SPField field = item.Fields.GetFieldByInternalName(name);
                    if (field != null && item[field.Id] != null)
                    {
                        bool val;
                        bool.TryParse(item[field.Id].ToString(), out val);

                        if (val)
                            return true;
                    }
                }
            }
            catch { }

            return false;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
