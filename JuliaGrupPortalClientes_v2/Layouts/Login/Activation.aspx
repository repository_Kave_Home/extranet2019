﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Activation.aspx.cs" Inherits="JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2.Activation"
    %>

<!DOCTYPE html>
<html>
<head>
    <title>Julià Grup Extranet - Activate Account</title>
    <SharePoint:SPShortcutIcon ID="SPShortcutIcon1" runat="server" IconUrl="/Style Library/v5/i/favicon.ico" />
    <link href="./RegisterStyles/RegisterForm.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,height=device-height,minimum-scale=0.4" />
    <meta http-equiv="Expires" content="0" />
    <SharePoint:RobotsMetaTag ID="RobotsMetaTag1" runat="server" />
</head>
<body style="text-align: center;">
    <form id="formu" runat="server">
    <div id="FormBox">
        <div class="lineImage">
            <div class="ImageLeft">
                <asp:Image ID="LaForma" runat="server" ImageUrl="./RegisterImage/LaForma.png" CssClass="LaFormaImage" />
            </div>
            <%-- div class="ImageRight">
                <asp:Image ID="Dorsuir" runat="server" ImageUrl="./RegisterImage/Dorsuit.png" CssClass="DorsuitImage" />
            </div --%>
        </div>
        <div class="lineTitle">
            <asp:Label ID="RegisterTitle" runat="server" class="title"></asp:Label>
        </div>
        <div class="line">
            <asp:Label ID="ErrorLabel" runat="server" Visible="false"></asp:Label>
        </div>
        <asp:Panel runat="server" Visible="false">
            <div class="line">
                <div class="label">
                    <asp:Label ID="UserLbl" runat="server" class="label"></asp:Label>
                </div>
                <div class="txt">
                    <asp:TextBox ID="UserTxt" runat="server" class="input"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="UserTxt" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" />
                </div>
            </div>
            <div class="line">
                <div class="label">
                    <asp:Label ID="PasswordLbl" runat="server" class="label"></asp:Label></div>
                <div class="txt">
                    <asp:TextBox ID="PasswordTxt" runat="server" TextMode="Password" class="input"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                        ControlToValidate="PasswordTxt" ErrorMessage="The textbox can not be empty!"
                        EnableClientScript="true" />
                </div>
            </div>
        </asp:Panel>
        <div class="line">
            <asp:Label ID="ErrorLbl" CssClass="errMsg" runat="server" Visible="false"></asp:Label>
        </div>
        <asp:Panel runat="server" Visible="false">
            <div class="lineButton">
                <asp:LinkButton ID="SendButton" OnClick="SendRegister" runat="server"></asp:LinkButton>
            </div>
        </asp:Panel>
        <!--Validators-->
    </div>
    </form>
</body>
</html>