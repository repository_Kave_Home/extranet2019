﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using System.Threading;

namespace JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2
{
    public partial class Activation : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.LoadControlText();   //19/11/2012 - REZ:: Solo pasamos el Guid y validamos al cargar la página
                SendRegister(null, null);
            }
        }

        private void LoadControlText()
        {
            this.RegisterTitle.Text = LanguageManager.GetLocalizedString("Activation");
            this.UserLbl.Text = LanguageManager.GetLocalizedString("Username");
            this.PasswordLbl.Text = LanguageManager.GetLocalizedString("Password");            
            this.SendButton.Text = LanguageManager.GetLocalizedString("Send");
            this.RequiredFieldValidator2.ErrorMessage = this.RequiredFieldValidator3.ErrorMessage = LanguageManager.GetLocalizedString("MandatoryTextMessage");
        }

        public void SendRegister(object sender, EventArgs e)
        {
            //19/11/2012 - REZ:: Solo pasamos el Guid y validamos al cargar la página
            //string username = this.UserTxt.Text;            
            //string pass = this.PasswordTxt.Text;
            Guid token = new Guid(Request.QueryString["token"]);
            int language = Thread.CurrentThread.CurrentUICulture.LCID;

            UserDataAccessObject userDAO = new UserDataAccessObject();
            //string result = userDAO.ActivateRegisterUser(username, pass, token, language);
            string result = userDAO.ActivateRegisterUser(token, language);
            if (result == "")
            {
                string direction = SPContext.Current.Site.Url + "/_login/default.aspx";
                Response.Redirect(direction);
            }
            else
            {
                this.ErrorLbl.Text = result;    // LanguageManager.GetLocalizedString("ErrorLogin");
                this.ErrorLbl.Visible = true;
            }

        }
    }
}
