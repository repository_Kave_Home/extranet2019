﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Changepass.aspx.cs" Inherits="JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2.Changepass" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<%@ Register TagPrefix="wssuc" TagName="MUISelector" src="~/_controltemplates/MUISelector.ascx" %>

<!DOCTYPE html>
<html>
<head>
    <title>Julià Grup Extranet - Change Password</title>
    <SharePoint:SPShortcutIcon ID="SPShortcutIcon1" runat="server" IconUrl="/Style Library/v5/i/favicon.ico" />
    <link href="./RegisterStyles/RegisterForm.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,height=device-height,minimum-scale=0.4" />
    <meta http-equiv="Expires" content="0" />
    <SharePoint:RobotsMetaTag ID="RobotsMetaTag1" runat="server" />
</head>
<body style="text-align:center;">
    <form id="formu" runat="server">
    <div id="FormBox">
        <div class="lineImage">
            <div class="ImageLeft">
                <asp:Image ID="LaForma" runat="server" ImageUrl="./RegisterImage/LaForma.png" CssClass="LaFormaImage"  />
            </div>
            <%--div class="ImageRight">
                <asp:Image ID="Dorsuir" runat="server" ImageUrl="./RegisterImage/Dorsuit.png" CssClass="DorsuitImage"  />
            </div--%>
            <div class="ImageRight">
            <a href="javascript:ChangeLanguage('Catalan');" onclick="OnSelectionChange(1027);return false;"><img style="height: 12px;" src="./RegisterImage/cat.png" border="0" alt="Catalan" /></a> 
            <a href="javascript:ChangeLanguage('Spanish');" onclick="OnSelectionChange(3082);return false;"><img style="height: 12px;" src="./RegisterImage/spa.png" border="0" alt="Spanish" /></a> 
            <a href="javascript:ChangeLanguage('French');" onclick="OnSelectionChange(1036);return false;"><img style="height: 12px;" src="./RegisterImage/fra.png" border="0" alt="French" /></a> 
            <a href="javascript:ChangeLanguage('Italian');" onclick="OnSelectionChange(1040);return false;"><img style="height: 12px;" src="./RegisterImage/ita.png" border="0" alt="Italian" /></a> 
            <a href="javascript:ChangeLanguage('English');" onclick="OnSelectionChange(1033);return false;"><img style="height: 12px;" src="./RegisterImage/eng.png" border="0" alt="English" /></a> 
            <a href="javascript:ChangeLanguage('Deutsch');" onclick="OnSelectionChange(1031);return false;"><img style="height: 12px;" src="./RegisterImage/ger.png" border="0" alt="Deutsch" /></a>
            <wssuc:MUISelector ID="MUISelector1" runat="server"/>
            </div>
        </div>
        <div class="lineTitle">
            <asp:Label ID="RegisterTitle" runat="server" class="title"></asp:Label>
        </div> 
       <div class="lineTitle">
            <div>
                <asp:Label ID="LabelOutdatedPassword" runat="server"></asp:Label></div>
        </div>  
        <div class="line">
            <div>
                <asp:Label ID="LabelSpecification" runat="server"></asp:Label></div>
        </div>
        
        <div class="line">
            <div class="label">
                <asp:Label ID="OldPasswordLbl" runat="server" class="label"></asp:Label></div>
            <div class="txt">
                <asp:TextBox ID="oldPasswordTxt" runat="server" TextMode="Password" class="input" MaxLength="20"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Runat="server" Display="Dynamic" ControlToValidate="oldPasswordTxt" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" CssClass="errMsg"/>
                </div>
        </div>

        <div class="line">
            <div class="label">
                <asp:Label ID="PasswordLbl" runat="server" class="label"></asp:Label></div>
            <div class="txt">
                <asp:TextBox ID="PasswordTxt" runat="server" TextMode="Password" class="input" MaxLength="20"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Runat="server" Display="Dynamic" ControlToValidate="PasswordTxt" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" CssClass="errMsg"/>
                </div>
        </div>
        <div class="line">
            <div class="label">
                <asp:Label ID="Password1Lbl" runat="server" class="label"></asp:Label></div>
            <div class="txt">
                <asp:TextBox ID="Password1Txt" runat="server" TextMode="Password" class="input" MaxLength="20"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Runat="server" Display="Dynamic" ControlToValidate="Password1Txt" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" CssClass="errMsg"/>
                <asp:CompareValidator ID="CompareValidator" runat="server" Display="Dynamic" 
                    ControlToCompare="PasswordTxt" ControlToValidate="Password1Txt"  ErrorMessage="Passwords does not match" 
                    EnableClientScript="true" CssClass="errMsg"></asp:CompareValidator>
                </div>
        </div>
        <div class="line">
            <asp:Label ID="ErrorLbl" runat="server" Visible="false"></asp:Label>
        </div>
        <div class="line">
            <asp:Label ID="ErrorLabel" CssClass="errMsg" runat="server" Visible="false"></asp:Label>
        </div>     
        <div class="lineButton">
            <asp:LinkButton ID="SendButton" OnClick="SendRegister" runat="server"></asp:LinkButton>
        </div>
        <!--Validators-->
    </div>
    </form>
</body>
</html>