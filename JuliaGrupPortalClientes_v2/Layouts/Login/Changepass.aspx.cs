﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using System.Web.UI;
using System.Threading;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2
{
    public partial class Changepass : UnsecuredLayoutsPageBase
    {
        User CurrentUser;
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadControlText();
            }
        }

        private void LoadControlText()
        {
            this.RegisterTitle.Text = LanguageManager.GetLocalizedString("ChangePass");


            this.OldPasswordLbl.Text = LanguageManager.GetLocalizedString("OldPass");
            this.PasswordLbl.Text = LanguageManager.GetLocalizedString("Password");
            this.Password1Lbl.Text = LanguageManager.GetLocalizedString("RepeatPassword");
            this.CompareValidator.ErrorMessage = LanguageManager.GetLocalizedString("PasswordsNotMatchMessage");
            this.LabelOutdatedPassword.Text = LanguageManager.GetLocalizedString("LabelOutdatedPassword"); 
            this.LabelSpecification.Text = LanguageManager.GetLocalizedString("LabelSpecification"); 

            this.SendButton.Text = LanguageManager.GetLocalizedString("Send");
            //Required Fields Messages
            this.RequiredFieldValidator2.ErrorMessage = this.RequiredFieldValidator3.ErrorMessage  = this.RequiredFieldValidator4.ErrorMessage = LanguageManager.GetLocalizedString("MandatoryTextMessage");

            string outdatedpassword = Request.QueryString["outdated"];
            if (!String.IsNullOrEmpty(outdatedpassword))
            {
                if (outdatedpassword == "true")
                {
                    LabelOutdatedPassword.Visible = true;
                }
                else
                {
                    LabelOutdatedPassword.Visible = false;
                }
            }
        }

        public void SendRegister(object sender, EventArgs e)
        {
            string output = String.Empty;
            try
            {
                string username = Request.QueryString["u"];
                //string mail = this.MailText.Text;
                string oldPass = this.oldPasswordTxt.Text;
                string pass = this.PasswordTxt.Text;
                string repeatPass = this.Password1Txt.Text;
                int language = Thread.CurrentThread.CurrentUICulture.LCID;

                UserDataAccessObject userDAO = new UserDataAccessObject();
                if (!(string.IsNullOrEmpty(oldPass))||(!string.IsNullOrEmpty(pass)) || (!string.IsNullOrEmpty(repeatPass))||(pass == repeatPass))
                {
                    userDAO.ChangePass(username, oldPass, pass);
                    if (output == String.Empty)
                    {
                        try
                        {
                            Session.Clear();    //Peta porque no podemos usar session. La borramos antes de venir aqui
                        } catch {}
                        this.ShowMessage(LanguageManager.GetLocalizedString("ChangePassSuccess"));
                        //Borramos las variables de sesión para que no vuelva a redirigirlo aqui si ha caducado la contraseña
                         
                        //Response.Redirect("/");
                    }
                }
                else
                {
                    NavException ex = new NavException("La contrasenya ha de coincidir");
                    throw ex;
                }
                
                //
                //userDAO.PreRegisterUser(username, pass, mail, clientCode, nif);
                //output = userDAO.PreRegisterUser(username, pass, mail, clientCode, nif, clientCode, language);
                //if (output == String.Empty)
                //{
                //    this.ShowMessage(LanguageManager.GetLocalizedString("RegistrationSuccess"));
                //    //Response.Redirect("http://www.juliagrup.com/");
                //}
                //else
                //{
                //    ErrorLabel.Visible = true;
                //    ErrorLabel.Text = output;
                //}
            }
            catch (NavException ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = ex.Message;
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = LanguageManager.GetLocalizedString("ErrorLogin");
            }
        }

        private void ShowMessage(string message)
        {
            // Cleans the message to allow single quotation marks 
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');" + 
                "window.location = '/';" +
                "</script>";

            // Gets the executing web page 
            Page page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page 
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Changepass), "alert", script);
            }
        }    
    }
}
