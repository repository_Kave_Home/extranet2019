﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>

<%@ Register TagPrefix="wssuc" TagName="MUISelector" src="~/_controltemplates/MUISelector.ascx" %>

<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginForm.aspx.cs" Inherits="JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2.LoginForm"  %>
<!DOCTYPE html>
<html>
<head>
    <title>Julià Grup Extranet - Login</title>
    <SharePoint:SPShortcutIcon ID="SPShortcutIcon1" runat="server" IconUrl="/Style Library/v5/i/favicon.ico" />
    <link href="./RegisterStyles/RegisterForm.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,height=device-height,minimum-scale=0.4" />
    <meta http-equiv="Expires" content="0" />
    <SharePoint:RobotsMetaTag ID="RobotsMetaTag1" runat="server" />
    <script type="text/javascript" src="/Style%20Library/Julia/js/jquery-1.7.2.min.js"></script> 
    <script type="text/javascript" src="/Style%20Library/Julia/js/jquery.blockUI.js"></script> 
    <script type ="text/javascript">
        function SetdisplayLang(value, newurl) {
            //Disable form
            $('.input').prop("disabled", true);
            $('.LoginButton').prop("disabled", true);
            var today = new Date();
            var oneYear = new Date(today.getTime() + 365 * 24 * 60 * 60 * 1000);
            var url = window.location.href;
            document.cookie = "lcid=" + value + ";path=/;expires=" + oneYear.toGMTString();
            window.location.href = newurl;
        }

        function SubmitLogin() {
            if (Page_ClientValidate()){
                $.blockUI({ message: $('#loadingPnl') })
            }
        }
    </script>
</head>
<body style="text-align:center;">
    <form id="formu" runat="server">
        <div id="FormBox">
            <div class="lineImage">
                <div class="ImageLeft">
                    <a href="http://www.juliagrup.com">
                        <asp:Image ID="Image1" runat="server" ImageUrl="./RegisterImage/LaForma.png" CssClass="LaFormaImage"  />
                    </a>
                </div>
                <div class="ImageRight">
                <a href="javascript:ChangeLanguage('Catalan');" onclick="OnSelectionChange(1027);return false;"><img style="height: 12px;" src="./RegisterImage/cat.png" border="0" alt="Catalan" /></a> 
                <a href="javascript:ChangeLanguage('Spanish');" onclick="OnSelectionChange(3082);return false;"><img style="height: 12px;" src="./RegisterImage/spa.png" border="0" alt="Spanish" /></a> 
                <a href="javascript:ChangeLanguage('French');" onclick="OnSelectionChange(1036);return false;"><img style="height: 12px;" src="./RegisterImage/fra.png" border="0" alt="French" /></a> 
                <a href="javascript:ChangeLanguage('Italian');" onclick="OnSelectionChange(1040);return false;"><img style="height: 12px;" src="./RegisterImage/ita.png" border="0" alt="Italian" /></a> 
                <a href="javascript:ChangeLanguage('English');" onclick="OnSelectionChange(1033);return false;"><img style="height: 12px;" src="./RegisterImage/eng.png" border="0" alt="English" /></a> 
                <a href="javascript:ChangeLanguage('Deutsch');" onclick="OnSelectionChange(1031);return false;"><img style="height: 12px;" src="./RegisterImage/ger.png" border="0" alt="Deutsch" /></a>
                <wssuc:MUISelector ID="MUISelector1" runat="server"/>
                </div>
            </div>
            <div class="lineTitle">
                <asp:Label ID="RegisterTitle" runat="server" class="title">Login page</asp:Label>
            </div>
            <div class="line">
                <div class="label">
                    <asp:Label ID="lblUserName" runat="server">UserName</asp:Label></div>
                <div class="txt">
                    <asp:TextBox ID="UserName" runat="server" class="input" MaxLength="20"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator0" Runat="server" Display="Dynamic" ControlToValidate="UserName" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" />
                </div>
            </div>
            <div class="line">
                <div class="label">
                    <asp:Label ID="lblPassword" runat="server" class="label" >Password</asp:Label></div>
                <div class="txt">
                    <asp:TextBox ID="Password" runat="server" class="input" textmode="Password" MaxLength="20"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Runat="server" Display="Dynamic" ControlToValidate="Password" ErrorMessage="The textbox can not be empty!" EnableClientScript="true" />
                </div>
            </div>      

            <div class="line">
                <asp:Label ID="lblError" CssClass="errMsg"  runat="server" Visible="false"></asp:Label>
            </div>

              <div class="line">
                <div class="label">
                    <asp:HyperLink ID ="Link" runat ="server" NavigateUrl="./register.aspx">Create a user</asp:HyperLink>
                </div> 
            </div>
            <div class="lineButton">
             <asp:button id="Button1" runat="server" cssclass="LoginButton" OnClientClick="javascript:SubmitLogin();" onclick="Login_Click" text="Submit" UseSubmitBehavior="true" />
        
            </div>
            <div class="lineImage">
                <div class="txt">
                     <asp:HyperLink ID ="RememberPass" runat ="server" NavigateUrl="./Rememberpass.aspx">Remember password</asp:HyperLink>    
                </div>
            </div>
        </div>
    </form>
    <div id="loadingPnl" style="display:none;"><img src="/Style Library/Julia/img/loading.gif"  width="66px" height="66px" /></div>
</body>

</html>
