﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.IdentityModel;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;
using System.Diagnostics;

namespace JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2
{
    public partial class LoginForm : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }
        private void LoadControlText()
        {
            this.RegisterTitle.Text = LanguageManager.GetLocalizedString("LoginForm_FormTitle");

            this.lblUserName.Text = LanguageManager.GetLocalizedString("LoginForm_Usernamelbl");
            this.lblPassword.Text = LanguageManager.GetLocalizedString("LoginForm_Passwordlbl");
            this.RequiredFieldValidator0.ErrorMessage = LanguageManager.GetLocalizedString("LoginForm_RequiredFieldMessage");
            this.RequiredFieldValidator1.ErrorMessage = LanguageManager.GetLocalizedString("LoginForm_RequiredFieldMessage");

            this.Link.Text = LanguageManager.GetLocalizedString("LoginForm_CreateUserLinkText");
            this.RememberPass.Text = LanguageManager.GetLocalizedString("RememberPass");
            this.Button1.Text = LanguageManager.GetLocalizedString("LoginForm_SendButtonText");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadControlText();
            }
        }
        protected void Login_Click(object sender, EventArgs e)
        {
            string strLoginErrorMsg = String.Empty;
            string strLoginGenericMsg  = "Wrong Userid or Password";
            string strUsername = UserName.Text.Trim();
            string strPassword = Password.Text;
            
            //Reseteamos el mensaje de error
            lblError.Visible = false;
            lblError.Text = "";
            
            //Session.Clear();  //No podem fer-ho aquí pq el web.config es el de layouts
            if (!(strUsername.Length > 0 && strPassword.Length > 0))
                lblError.Text = "User Name or Password can not be empty!";
            else
            {
                bool status = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, strUsername, strPassword);
                if (!status)// if auth failed
                {
                    try
                    {
                        UserDataAccessObject userDAO = new UserDataAccessObject();
                        strLoginErrorMsg = userDAO.ValidateUser(strUsername, strPassword);
                        if (strLoginErrorMsg != string.Empty)
                        {
                            strLoginGenericMsg = strLoginErrorMsg;
                        }
                    }
                    catch (Exception ex) { }
                    lblError.Text = strLoginGenericMsg;
                    lblError.Visible = true;
                    //UnblockUI();
                }
                else //if success
                {
                    formu.Disabled = true;
                    Sesion SesionJulia = new Sesion();
                    string currentUserLang = SesionJulia.GetUserLanguageForUsername(strUsername);
                    int currentLang = LoadLanguage(currentUserLang);
                    SetDisplayLanguage(currentLang, SPContext.Current.Web.Url);
                    //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception("Changing User Language- With WriteDebug"), new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);

                    //Response.Redirect(SPContext.Current.Web.Url); //Use site url
                }

            }
        }

        private int LoadLanguage(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "CAT":
                    ret = "1027";
                    break;
                case "ESP":
                    ret = "3082";
                    break;
                case "FRA":
                    ret = "1036";
                    break;
                case "ITA":
                    ret = "1040";
                    break;
                case "ENU":
                    ret = "1033";
                    break;
                case "DEU":
                    ret = "1031";
                    break;
                default:
                    ret = "3082";
                    break;
            }
            return Convert.ToInt32(ret);
        }

        private void SetDisplayLanguage(int lcid, string redirectUrl)
        {
            string script = "<script language='javascript'>SetdisplayLang('" + lcid + "','" + redirectUrl + "')</script>";
            Page.ClientScript.RegisterStartupScript(GetType(), "Register", script);
        }

        //private void UnblockUI()
        //{
        //    string script = "<script language='javascript'>$.unblockUI();</script>";
        //    Page.ClientScript.RegisterStartupScript(GetType(), "UnBlockUI", script);
        //}
    }
}
