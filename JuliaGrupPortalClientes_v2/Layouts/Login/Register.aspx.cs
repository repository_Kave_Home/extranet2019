﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using System.Web.UI;
using System.Threading;

namespace JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2
{
    public partial class Register : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadControlText();
            }
        }

        private void LoadControlText()
        {
            this.RegisterTitle.Text = LanguageManager.GetLocalizedString("Register");
            
            this.ClientCodeLbl.Text = LanguageManager.GetLocalizedString("ClientCode");
            
            this.NifLbl.Text = LanguageManager.GetLocalizedString("NIF");

            this.MailLbl.Text = LanguageManager.GetLocalizedString("MailAddress");
            this.RepeatMailLbl.Text = LanguageManager.GetLocalizedString("RptMailAddress");
            this.RegularExpressionValidator1.ErrorMessage = this.RegularExpressionValidator2.ErrorMessage = LanguageManager.GetLocalizedString("InvalidEmail");
            this.CompareValidator1.ErrorMessage = LanguageManager.GetLocalizedString("EmailsNotMatchMessage");
            
            this.UserLbl.Text = LanguageManager.GetLocalizedString("Username");
            
            this.PasswordLbl.Text = LanguageManager.GetLocalizedString("Password");
            this.Password1Lbl.Text = LanguageManager.GetLocalizedString("RepeatPassword");
            this.CompareValidator.ErrorMessage = LanguageManager.GetLocalizedString("PasswordsNotMatchMessage");

            this.SendButton.Text = LanguageManager.GetLocalizedString("Send");
            //Required Fields Messages
            this.RequiredFieldValidator0.ErrorMessage = this.RequiredFieldValidator1.ErrorMessage = this.RequiredFieldValidator2.ErrorMessage = 
                this.RequiredFieldValidator3.ErrorMessage = this.RequiredFieldValidator4.ErrorMessage = this.RequiredFieldValidator5.ErrorMessage =
                    this.RequiredFieldValidator6.ErrorMessage = LanguageManager.GetLocalizedString("MandatoryTextMessage");

            this.RegularExpressionValidator3.ErrorMessage = LanguageManager.GetLocalizedString("InvalidUsername");

        }

        public void SendRegister(object sender, EventArgs e)
        {
            string output = String.Empty;
            try
            {
                string clientCode = this.ClientCodeTxt.Text;
                string nif = this.NifTxt.Text;
                string username = this.UserTxt.Text;
                string mail = this.MailText.Text;
                string pass = this.PasswordTxt.Text;
                int language = Thread.CurrentThread.CurrentUICulture.LCID;

                UserDataAccessObject userDAO = new UserDataAccessObject();
                //
                //userDAO.PreRegisterUser(username, pass, mail, clientCode, nif);
                output = userDAO.PreRegisterUser(username, pass, mail, clientCode, nif, clientCode, language);
                if (output == String.Empty)
                {
                    this.ShowMessage(LanguageManager.GetLocalizedString("RegistrationSuccess"));
                    //Response.Redirect("http://www.juliagrup.com/");
                }
                else
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = output;
                }
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = LanguageManager.GetLocalizedString("ErrorLogin");
            }
        }

        private void ShowMessage(string message)
        {
            // Cleans the message to allow single quotation marks 
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');" + 
                "window.location = 'http://www.juliagrup.com/';" +
                "</script>";

            // Gets the executing web page 
            Page page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page 
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Register), "alert", script);
            }
        }    
    }
}
