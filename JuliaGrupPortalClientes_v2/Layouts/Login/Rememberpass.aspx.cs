﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using System.Web.UI;
using System.Threading;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupPortalClientes_v2.Layouts.JuliaGrupPortalClientes_v2
{
    public partial class Rememberpass : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadControlText();
            }
        }

        private void LoadControlText()
        {
            this.RegisterTitle.Text = LanguageManager.GetLocalizedString("RememberPass");
            this.LblUsername.Text = LanguageManager.GetLocalizedString("LoginForm_Usernamelbl");
            this.MailLbl.Text = LanguageManager.GetLocalizedString("Administration_Email");
            this.SendButton.Text = LanguageManager.GetLocalizedString("Send");
            //Required Fields Messages
             this.RequiredFieldValidator5.ErrorMessage =LanguageManager.GetLocalizedString("MandatoryTextMessage");

        }

        public void SendRegister(object sender, EventArgs e)
        {
            string output = String.Empty;
            try
            {
            
                string mail = this.MailText.Text;
                string Username = this.UsernameTxt.Text;
                int language = Thread.CurrentThread.CurrentUICulture.LCID;
                
                UserDataAccessObject userDAO = new UserDataAccessObject();
                //User currentUser =  userDAO.GetUserWithUsername(Username);

                string RandomPass = CreatePassword(6);

                //output = userDAO.RememberPass(currentUser.Code, mail, RandomPass);
                output = userDAO.RememberPass(Username, mail, RandomPass);

                if (output == String.Empty)
                {
                    this.ShowMessage(LanguageManager.GetLocalizedString("RememberPassSuccess"));
                
                }
                else
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = output;
                }
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = LanguageManager.GetLocalizedString("ErrorRememberPass");
            }
        }
        public string CreatePassword(int length)
        {
            string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            string res = "";
            Random rnd = new Random();
            while (0 < length--)
                res += valid[rnd.Next(valid.Length)];
            return res;
        }

        private void ShowMessage(string message)
        {
            // Cleans the message to allow single quotation marks 
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');" +
                "window.location.href = '/';" +
                "</script>";

            // Gets the executing web page 
            Page page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page 
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Rememberpass), "alert", script);
            }
        }    
    }
}
