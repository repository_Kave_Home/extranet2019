﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TokenValidator.aspx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Layouts.Login.TokenValidator" %>

<!DOCTYPE html>
<html>
<head>
    <title>Julià Grup Extranet - Token Validator</title>
    <SharePoint:SPShortcutIcon ID="SPShortcutIcon1" runat="server" IconUrl="/Style Library/v5/i/favicon.ico" />
    <link href="./RegisterStyles/RegisterForm.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,height=device-height,minimum-scale=0.4" />
    <meta http-equiv="Expires" content="0" />
    <SharePoint:RobotsMetaTag ID="RobotsMetaTag1" runat="server" />
    <script type="text/javascript" src="/Style%20Library/Julia/js/jquery-1.7.2.min.js"></script> 
    <script type="text/javascript" src="/Style%20Library/Julia/js/jquery.blockUI.js"></script> 
    <script type ="text/javascript">
        function Login(newurl) {
            $.blockUI({ message: $('#loadingPnl') })
            window.location.href = newurl;
        }

    </script>
</head>
<body style="text-align:center;">
<div id="loadingPnl" style="display:none;"><img src="/Style Library/Julia/img/loading.gif"  width="66px" height="66px" /></div>
</body>
</html>