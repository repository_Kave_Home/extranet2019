﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;
using System.Web;
using Microsoft.SharePoint.IdentityModel;

namespace JuliaGrupPortalClientes_v2.Layouts.Login
{
    public partial class TokenValidator : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SendRequest(null, null);
            }
        }
        public void SendRequest(object sender, EventArgs e)
        {
            String token = Request.QueryString["token"];
            String strUserName = GetInfoFromToken(token);
            if (!string.IsNullOrEmpty(strUserName))
            {
                AutoAuthenticateUser(strUserName);
            }
            else {
                Response.Redirect("/"); //Use site url    
            }
        }

        public static String GetInfoFromToken(String token)
        {
            String username = String.Empty;
            String decryptes = Utilities.Decrypt(token);
            var list = decryptes.Split('#');
            if (list.Length > 1)
            {
                try
                {
                    DateTime t = Convert.ToDateTime(list[2]);
                    if ((DateTime.Now - t).TotalMinutes <= 1)
                        username = list[1] + "_" + list[0];
                }
                catch (FormatException ex)
                {
                    username = "";
                }
            }
            return username;
        }

        public void AutoAuthenticateUser(string username)
        {
            bool status = SPClaimsUtility.AuthenticateFormsUser(HttpContext.Current.Request.Url, username, "prova");
            if (!status)// if auth failed
            {
                Response.Redirect("/"); //Use site url            
            }
            else {
                Response.Redirect("/gdo"); //Use site url
                //string script = "<script language='javascript'>Login('/gdo')</script>";
                //Page.ClientScript.RegisterClientScriptBlock(GetType(), "SignIn", script);

            }
                  
        }
    }
}
