﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="julia" TagName="NoticiasUserControl" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Noticias/NoticiasUserControl.ascx" %>





<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:UIVersionedContent ID="UIVersionedContent2" UIVersion="4" runat="server">
		<ContentTemplate>
			<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
			<PublishingWebControls:EditModePanel ID="EditModePanel1" runat="server"><!-- Styles for edit mode only--><SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
					After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/></PublishingWebControls:EditModePanel>
		</ContentTemplate>
	</SharePointWebControls:UIVersionedContent>
	<SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
    <style>
.layout_left
{
	float: left;
}
.layout_right
{
	float: right;
}
.layout_2_c
{
	width: 100%;
	padding-top: 10px;
	padding-right: 0px;
	padding-bottom: 35px;
	padding-left: 0px;
}
.layout_2_c .layout_left
{
	width: 190px;
    padding-left: 10px;
}
.layout_2_c .layout_right
{
	width: 750px;
    padding-right: 10px;
}
h2
{
	line-height: 35px;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 20px;
	padding-left: 0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 2.8em;
	font-style: normal;
	font-variant: normal;
	font-weight: bold;
	font-size-adjust: none;
	font-stretch: normal;
}
h3
{
	line-height: 25px;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 10px;
	padding-left: 0px;
	font-family: "Arial Narrow", Helvetica, sans-serif;
	font-size: 1.8em;
	font-style: normal;
	font-variant: normal;
	font-weight: bold;
	font-size-adjust: none;
	font-stretch: normal;
}
h3.size_m
{
	line-height: 20px;
	font-size: 1.4em;
}
a
{
	color: #808080;
	text-decoration: none;
}
p
{
	color: #808080;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 15px;
	padding-left: 0px;
}
strong, em, bold
{
	color: #000000;
}
#new_time
{
	height: 16px;
	font-size: 0.9em;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 15px;
	margin-left: 0px;
	background-image: url("images/highlight.gif");
	background-attachment: scroll;
	background-repeat: repeat;
	background-position-x: 0px;
	background-position-y: 0px;
	background-size: auto;
	background-origin: padding-box;
	background-clip: border-box;
	background-color: transparent;
}
#new_date
{
	height: 16px;
	line-height: 16px;
	padding-top: 0px;
	padding-right: 8px;
	padding-bottom: 0px;
	padding-left: 8px;
	border-left-color: #da1638;
	border-left-width: 4px;
	border-left-style: solid;
	float: left;
}
#new_rol
{
	height: 16px;
	line-height: 16px;
	padding-top: 0px;
	padding-right: 8px;
	padding-bottom: 0px;
	padding-left: 8px;
	float: right;
}
#new_page h2
{
	font-family: "Arial Narrow", Helvetica, sans-serif;
}
#new_content
{
	color: #808080;
}
#new_content p
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 15px;
	padding-left: 0px;
}
#new_content em, #new_content strong, #new_content b
{
	color: #000000;
}
#new_content img
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 12px;
	padding-left: 0px;
}
#new_content p img
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 0px;
}
#new_content p a
{
	color: #000;
	text-decoration: underline;
}
#new_content ul
{
	padding-top: 0px;
	padding-right: 35px;
	padding-bottom: 0px;
	padding-left: 35px;
}
#new_content ul li
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 12px;
	padding-left: 0px;
	list-style-type: square;
	list-style-position: outside;
	list-style-image: none;
}
#new_others
{
	width: 215px;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 15px;
	float: right;
}
.new_other_content
{
	width: 100%;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 20px;
	margin-left: 0px;
	background-image: none;
	background-attachment: scroll;
	background-repeat: repeat;
	background-position-x: 0%;
	background-position-y: 0%;
	background-size: auto;
	background-origin: padding-box;
	background-clip: border-box;
	background-color: rgb(244, 245, 246);
}
.new_other_content .section
{
	color: #ffffff;
	text-transform: uppercase;
	padding-top: 3px;
	padding-right: 10px;
	padding-bottom: 3px;
	padding-left: 10px;
	font-weight: bold;
	border-left-color: #989898;
	border-left-width: 15px;
	border-left-style: solid;
	background-image: none;
	background-attachment: scroll;
	background-repeat: repeat;
	background-position-x: 0%;
	background-position-y: 0%;
	background-size: auto;
	background-origin: padding-box;
	background-clip: border-box;
	background-color: rgb(59, 64, 71);
}
.new_other_content .section_grey
{
	border-left-color: #989898;
	border-left-width: 15px;
	border-left-style: solid;
}
#new_content .new_other_content ul, .new_other_content ul
{
	padding-top: 7px;
	padding-right: 5px;
	padding-bottom: 7px;
	padding-left: 5px;
	font-size: 0.9em;
}
#new_content .new_other_content ul li, .new_other_content ul li
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 0px;
	list-style-type: none;
	list-style-position: outside;
	list-style-image: none;
}
.new_other_content ul li a
{
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
	display: block;
}
.new_other_content ul li .title
{
	color: #000000;
	line-height: 12px;
	padding-top: 0px;
	padding-right: 5px;
	padding-bottom: 0px;
	padding-left: 5px;
	font-weight: bold;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 4px;
	margin-left: 0px;
	display: block;
}
.new_other_content ul li .title_red
{
	border-left-color: #da1638;
	border-left-width: 5px;
	border-left-style: solid;
}
#news_images
{
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 20px;
	padding-left: 0px;
}
#news_images img
{
	margin-top: 0px;
	margin-right: 2px;
	margin-bottom: 5px;
	margin-left: 0px;
	border-top-color: #f0f1f2;
	border-right-color: #f0f1f2;
	border-bottom-color: #f0f1f2;
	border-left-color: #f0f1f2;
	border-top-width: 5px;
	border-right-width: 5px;
	border-bottom-width: 5px;
	border-left-width: 5px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	float: left;
}
#all_news_list
{
	line-height: 13px;
	font-size: 0.9em;
	border-top-color: #e3e3e3;
	border-right-color: #e3e3e3;
	border-bottom-color: #e3e3e3;
	border-left-color: #e3e3e3;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}
#all_news_list .all_news_pager
{
	height: 25px;
	line-height: 25px;
	padding-top: 0px;
	padding-right: 5px;
	padding-bottom: 0px;
	padding-left: 5px;
	border-bottom-color: #e3e3e3;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	background-image: none;
	background-attachment: scroll;
	background-repeat: repeat;
	background-position-x: 0%;
	background-position-y: 0%;
	background-size: auto;
	background-origin: padding-box;
	background-clip: border-box;
	background-color: rgb(237, 238, 240);
}
#all_news_list .all_news_pager .actual
{
	float: left;
}
#all_news_list .all_news_pager a
{
	color: #000000;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 5px;
	font-size: 0.9em;
	float: right;
}
#all_news_list .all_news_pager a.off
{
	color: #a5a5a5;
}
#all_news_list ul li
{
	border-bottom-color: #e3e3e3;
	border-bottom-width: 1px;
	border-bottom-style: solid;
}
#all_news_list ul li.last
{
	border-bottom-color: currentColor;
	border-bottom-width: 0px;
	border-bottom-style: none;
}
#all_news_list ul li a
{
	padding-top: 4px;
	padding-right: 8px;
	padding-bottom: 4px;
	padding-left: 8px;
	display: block;
}
#all_news_list ul li a:hover, #all_news_list ul li a.selected
{
	background-image: none;
	background-attachment: scroll;
	background-repeat: repeat;
	background-position-x: 0%;
	background-position-y: 0%;
	background-size: auto;
	background-origin: padding-box;
	background-clip: border-box;
	background-color: rgb(244, 245, 246);
}
#all_news_list ul li a .time
{
	display: block;
}
#all_news_list ul li a .title
{
	color: #000000;
	font-weight: bold;
	display: block;
}

/* LightBox */
#lightbox{
	position: absolute;
	top: 40px;
	left: 0;
	width: 100%;
	z-index: 100;
	text-align: center;
	line-height: 0;
	}

#lightbox a img{ border: none; }

#outerImageContainer{
	position: relative;
	background-color: #fff;
	width: 250px;
	height: 250px;
	margin: 0 auto;
	}

#imageContainer{
	padding: 10px;
	}

#loading{
	position: absolute;
	top: 40%;
	left: 0%;
	height: 25%;
	width: 100%;
	text-align: center;
	line-height: 0;
	}
#hoverNav{
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	z-index: 10;
	}
#imageContainer>#hoverNav{ left: 0;}
#hoverNav a{ outline: none;}

#prevLink, #nextLink{
	width: 49%;
	height: 100%;
	background: transparent url(/_wpresources/QuestechSystems.SharePoint.PictureThumbnails/1.0.0.0__ce574b585167cfb8/Images/blank.gif) no-repeat; /* Trick IE into showing hover */
	display: block;
	}
#prevLink { left: 0; float: left;}
#nextLink { right: 0; float: right;}
#prevLink:hover, #prevLink:visited:hover { background: url(/_wpresources/QuestechSystems.SharePoint.PictureThumbnails/1.0.0.0__ce574b585167cfb8/Images/prevlabel.gif) left 15% no-repeat; }
#nextLink:hover, #nextLink:visited:hover { background: url(/_wpresources/QuestechSystems.SharePoint.PictureThumbnails/1.0.0.0__ce574b585167cfb8/Images/nextlabel.gif) right 15% no-repeat; }


#imageDataContainer{
	font: 10px Verdana, Helvetica, sans-serif;
	background-color: #fff;
	margin: 0 auto;
	line-height: 1.4em;
	}

#imageData{
	padding:0 10px;
	}
#imageData #imageDetails{ width: 70%; float: left; text-align: left; }	
#imageData #caption{ font-weight: bold;	}
#imageData #numberDisplay{ display: block; clear: left; padding-bottom: 1.0em;	}			
#imageData #bottomNavClose{ width: 66px; float: right;  padding-bottom: 0.7em;	}	
		
#overlay{
	position: absolute;
	top: 0;
	left: 0;
	z-index: 90;
	width: 100%;
	height: 500px;
	background-color: #000;
	filter:alpha(opacity=60);
	-moz-opacity: 0.6;
	opacity: 0.6;
	}
	

.clearfix:after {
	content: "."; 
	display: block; 
	height: 0; 
	clear: both; 
	visibility: hidden;
	}

* html>body .clearfix {
	display: inline-block; 
	width: 100%;
	}

* html .clearfix {
	/* Hides from IE-mac \*/
	height: 1%;
	/* End hide from IE-mac */
	}	
	
    /* PT */
    .PT ul
{
    /*margin:3px;*/
	padding:0;
	list-style:none;
}

.PT ul li
{
	padding:0;
	float:left;
	margin:0 .4em .4em 0;
	background:#fff;
	width:auto;
	/*border:1px solid #1a150f;*/
	background-position:center center;
	background-repeat:no-repeat;
	overflow:hidden;
}

.PT li a
{
	width:130px;
	height:130px;
	border:solid 5px #f0f1f2;
	display:block;
	text-indent:-5000px;
	font:0/0 Arial;
}
</style>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:UIVersionedContent ID="UIVersionedContent4" UIVersion="4" runat="server">
		<ContentTemplate>
			<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
		</ContentTemplate>
	</SharePointWebControls:UIVersionedContent>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server"> 
	<SharePointWebControls:UIVersionedContent ID="UIVersionedContent5" UIVersion="4" runat="server"> 
        <ContentTemplate> 
            <SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigation" 
                RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" 
                NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" 
                RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=353 
                NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/images/fgimg.png" 
                HideInteriorRootNodes="true" SkipLinkText="" /> 
        </ContentTemplate> 
    </SharePointWebControls:UIVersionedContent> 
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<SharePointWebControls:UIVersionedContent ID="UIVersionedContent7" UIVersion="4" runat="server">
		<ContentTemplate>
            <div class="article">
                <div class="leftmargin" style="width:41px;"></div>
                <div class="layout_2_c">
 
                 
                 	
    <div class="layout_left" id="div_listado_actualidad">
        <!-- List of news -->
        <julia:NoticiasUserControl runat="server" newsPage="/News" pageSize="8"/>
        <!-- List of news -->
    </div>
    <div class="layout_right">
        <!-- News content -->
        <div id="new_page">
            <!--div id="new_time">
                <span id="new_date">Marzo 2013</span>
                <span id="new_rol">Publicaciones Actiu</span>    			
            </div-->
            <h2><SharePointWebControls:TextField ID="TextField1" runat="server" FieldName="Title" /></h2>
            <div id="new_content">
                <div id="new_others">
                    <!-- Images links -->
                    <!-- Other content -->
                    <div class="new_other_content">
                        <WebPartPages:WebPartZone runat="server" Title="Enlaces relacionados" ID="Links">
                            <ZoneTemplate> 
                                
                            </ZoneTemplate> 
                        </WebPartPages:WebPartZone>
                    </div>
                </div>
                <!-- New body -->
                <div id="_invisibleIfEmpty" name="_invisibleIfEmpty">
                    <PublishingWebControls:RichImageField ID="RichImageField1" FieldName="PublishingPageImage" runat="server"/>
                </div>
                <div id="_invisibleIfEmpty" name="_invisibleIfEmpty">
                    <PublishingWebControls:RichHtmlField ID="RichHtmlField1" FieldName="PublishingImageCaption"  AllowTextMarkup="false" AllowTables="false" AllowLists="false" AllowHeadings="false" AllowStyles="false" AllowFontColorsMenu="false" AllowParagraphFormatting="false" AllowFonts="false" PreviewValueSize="Small" AllowInsert="false" runat="server"/>
                </div>
                <div id="_invisibleIfEmpty" name="_invisibleIfEmpty">
                    <PublishingWebControls:RichHtmlField ID="RichHtmlField2" FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="400px" runat="server"/>              			
                </div>
            </div>
            <div class="clearboth"></div>
           <!-- Images -->
           <div id="news_images">	
                <WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Footer%>" ID="Footer">
                    <ZoneTemplate> 
                        
                    </ZoneTemplate>
                </WebPartPages:WebPartZone> 
           </div>     		
        </div>        	
    </div>
    <div class="clearboth"></div>
    <script language="javascript">if(typeof(MSOLayout_MakeInvisibleIfEmpty) == "function") {MSOLayout_MakeInvisibleIfEmpty();}</script>            
</div>

				<PublishingWebControls:EditModePanel ID="EditModePanel4" runat="server" CssClass="edit-mode-panel roll-up">
					<PublishingWebControls:RichImageField ID="RichImageField2" FieldName="PublishingRollupImage" AllowHyperLinks="false" runat="server" />
					<asp:Label text="<%$Resources:cms,Article_rollup_image_text%>" runat="server" />
					<SharePointWebControls:TextField ID="TextField2" FieldName="JuliaNewsRollupText" runat="server"  />
                    <SharePointWebControls:DateTimeField FieldName="ArticleStartDate" runat="server"/>
					<SharePointWebControls:TextField FieldName="ArticleByLine" runat="server"/>
				</PublishingWebControls:EditModePanel>
			</div>


		</ContentTemplate>
	</SharePointWebControls:UIVersionedContent>
</asp:Content>

