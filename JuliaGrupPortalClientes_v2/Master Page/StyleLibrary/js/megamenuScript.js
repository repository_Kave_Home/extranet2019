﻿/*global $ */
/*$(document).ready(*/

function init_megamenu() {

    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI

    //$('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\">Navigation</a>");

    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
    //Done this way so it can be used with wordpress without any trouble

    /*$(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });*/

    $(".menu > ul > li").hover(
        function (e) {
            if ($(window).width() > 943) {
                var a = $(this).children("ul").children("li.megamenuOnly");
                var b = $(this).children("ul.userOptionsMenu");
                if (a.length > 0 || b.length > 0)
                    $(this).children("ul").stop(true, false).fadeIn(150);
                e.preventDefault();
            }
        },
        function (e) {
            if ($(window).width() > 943) {
                var old = $(this).children("ul").children("#subsubcategory").children("ul");
                old.hide();
                var idOld = old.attr("id");
                var li = $(this).children("ul").children("li:first").children("ul").children("#" + idOld + "");
                li.append(old);
                var oldback = $(this).children("ul").children("#subsubcategory");
                oldback.append("<a style='border-bottom:none;'></a>");
                $(this).children("ul").stop(true, false).fadeOut(150);
                e.preventDefault();
            }
        });

    $('.menu > ul > li > ul > li > ul > li').hover(
        function (e) {
            if ($(window).width() > 943) {
                var a = $(this).parent().parent().children("a");
                if (a.length == 0) {
                    var fOut = $(this).parent().parent().parent().children("#subsubcategory").children("ul");
                    fOut.hide();
                    var id = fOut.attr('id');
                    var old = $('#' + id);
                    old.append(fOut);
                    var fIn = $(this).children("ul");
                    $(this).children("ul").stop(true, false).fadeIn(150);
                    e.preventDefault();
                    $(this).parent().parent().parent().children("#subsubcategory").html(fIn);
                }
            }
        },
        function (e) {
            if ($(window).width() > 943) {
                //var fOut = $('#subsubcategory ul:first');
                //fOut.stop(true, false).fadeOut(150);
                //fOut.hide();
                //var id = fOut.attr('id');
                //$(this).append(fOut);
                //$(this).children("ul").stop(true, false).fadeOut(20);
                e.preventDefault();
            }
        });

    //If width is more than 943px dropdowns are displayed on hover

    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 943) {
            $(this).children("ul").fadeToggle(150);
        }
    });
    //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

}
/*);*/