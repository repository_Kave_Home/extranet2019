﻿
//Namespace
window.JuliaClients = window.JuliaClients || {};

//Library Pattern
//Performs CRUD operations on the list
window.JuliaClients.Service = {
    getDataAsync: function (_type, _url, _contentType, _dataType, OK, NOK) {
        if (_type == null) {
            _type = "GET";
        }
        if (_contentType == null) {
            _contentType = "application/json; charset=utf-8";
        }
        if (_dataType == null) {
            _dataType = "json";
        }

        $.ajax({
            type: _type,
            url: _url,
            contentType: _contentType,
            dataType: _dataType,
            success: function (output) {
                OK(output);
            },
            error: function (output) {
                NOK(output);
            }
        });
    },
    testMySiteService: function () {
        var OK = function (text) { if (window.console) console.log('Se ha encontrado el servicio, respuesta: ' + text['TestResult']) };
        var NOK = function (text) { if (window.console) console.log('No se ha encontrado el servicio.') };
        getDataAsync("GET", '/_vti_bin/JuliaClientsSvc/JuliaClients.svc/test', "application/json; charset=utf-8", 'json', OK, NOK);
    },
    filterPrice: function () {
        window.location.href = "/Pages/RedirectTo.aspx?r=products&pr=" + $("#minPrice").val() + "_" + $("#maxPrice").val() + "&t=" + $("#search").val();
    },
    searchProducts: function (search, username, clientCode, tipoCompra) {
        var _search = search || "";
        var _username = username;
        var _clientCode = clientCode;
        var _tipoCompra = tipoCompra;
        

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/searchProducts",
            //input parameters (JSON)
            ///* 2019-07-04 - Aida Lucha - Shop in shop*/
            data: JSON.stringify({ search: _search, username: _username, clientCode: _clientCode, iPriceSel: $('#currentPriceSelId').val(), coefDivisaWEB: $('#coefDivisaWEB').val(), literalDivisaWEB: $('#literalDivisaWEB').val(), coeficient: $('#coeficient').val(), lcid: _spPageContextInfo.currentLanguage, tipoCompra: _tipoCompra, dollarCoef: $('#dollarCoef').val()}),
            success: function (data) {
                /* load habndlebars template */
                if (tipoCompra == 0) {
                    var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ProductSearch.htm' });
                } else {
                    var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ProductSearchGdo.htm' });
                }
                var articles = data;
                hbTemplate.success(
    	            function (origin) {
    	                $('#searchProductsDiv').text("");
    	                var template = Handlebars.compile(origin);
    	                $('#resultsNumber').text(articles.length.toString());
    	                JuliaClients.Service.loadCategoryList(username, clientCode, articles, tipoCompra);
    	                var min = 0;
    	                var max = 0;
    	                $.each(articles, function (index, article) {
    	                    if (index == 0) {
    	                        min = article.PriceInt;
    	                        max = article.PriceInt;
    	                    } else {
    	                        if (article.PriceInt < min) min = article.PriceInt;
    	                        if (article.PriceInt > max) max = article.PriceInt;
    	                    }
    	                    article.verQty = ($("#currentUserVerCantidades").val() === "True" || $("#currentUserVerCantidades").val() === "true");
    	                    $('#searchProductsDiv').append(template(article));
    	                    JuliaClients.Service.setIndividualRes(article);
    	                });
    	                $('#minPrice').val(min.toString()).attr({ "min": min, "max": max });
    	                $('#maxPrice').val(max.toString()).attr({ "min": min, "max": max });
    	                JuliaClients.Service.setRes();
    	            }
                );
            },
            error: function (output) {
                $('#tblStaff').append('Error: ' + output + '<br/>');
            }
        });
    },
    loadCategoryList: function (username, clientcode, articles, tipoCompra) {
        var _username = username;
        var _clientcode = clientcode;
        var _tipoCompra = tipoCompra;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/loadCategoryList",
            //input parameters (JSON)
            data: JSON.stringify({ username: _username, clientcode: _clientcode, language: $("#currentUserLanguage").val(), tipocompra: _tipoCompra }),
            async: false,
            success: function (data) {
                $('#categoriesFilter').text("");
                for (var i = 0; i < data.length; i++) {
                    var id = data[i]._codigo.replace(/\s+/g, '');
                    var string = "";
                    if (tipoCompra == 0) {
                        string = "<a href='/Pages/RedirectTo.aspx?r=products&c=" + id + "&t=" + $("#search").val() + "'><div class='categoriesFilterDiv' id='" + id + "' style='display:none;' ><div class='categoriesFilterName'>" + data[i]._descripcion + "</div><span class='categoriesFilterValue'></span></div></a>";
                    } else {
                        string = "<a href='/gdo/Pages/RedirectTo.aspx?r=products&c=" + id + "&t=" + $("#search").val() + "'><div class='categoriesFilterDiv' id='" + id + "' style='display:none;' ><div class='categoriesFilterName'>" + data[i]._descripcion + "</div><span class='categoriesFilterValue'></span></div></a>";
                    }
                    $('#categoriesFilter').append(string);
                }
                JuliaClients.Service.setSeeMore(false, null);
                JuliaClients.Service.searchFilterSections(articles, "categorizacion", "#categoriesFilter");
                if (tipoCompra == 0) JuliaClients.Service.searchFilterSections(articles, "color", "#colorFilter");
                JuliaClients.Service.setSeeMore(true, tipoCompra);
            },
            error: function (output) {
                $('#tblStaff').append('Error: ' + output + '<br/>');
            }
        });
    },
    setSeeMore: function (set, tipoCompra) {
        if (set) {
            if ($("#categoriesFilter").height() > 145) {
                $("#categoriesFilter").addClass("filterSectionHideContentCategory");
                $("#seeMoreLinkCategory").css("display", "block");
            }
            if (tipoCompra == 0) {
                if ($("#colorFilter").height() > 145) {
                    $("#colorFilter").addClass("filterSectionHideContentColor");
                    $("#seeMoreLinkColor").css("display", "block");
                }
            }
        } else {
            $(".filterSectionHideContentCategory").removeClass("filterSectionHideContentCategory");
            $("#seeMoreLinkCategory").css("display", "none");
            $(".filterSectionHideContentColor").removeClass("filterSectionHideContentColor");
            $("#seeMoreLinkColor").css("display", "none");
        }
    },
    searchFilterSections: function (articles, section, field) {
        var groups = {};
        var subsection = null;
        for (var i = 0; i < articles.length; i++) {
            var item = articles[i];
            if (section == "categorizacion") subsection = item.Categorizacion;
            if (section == "color") subsection = item.Color;
            if (!groups[subsection]) {
                groups[subsection] = [];
            }
            groups[subsection].push({ Code: item.Code });
        }
        var result = [];
        for (var x in groups) {
            if (Object.prototype.hasOwnProperty.call(groups, x)) {
                var obj = {}
                obj[x] = groups[x];
                result.push(obj);
            }
        }
        if (section == "categorizacion") {
            for (var y = 0; y < result.length; y++) {
                for (var z in result[y]) {
                    var aux = z.split(";");
                    var string = "";
                    for (var x = 0; x < aux.length; x++) {
                        if ($("#" + aux[x]).length) {
                            if ($("#" + aux[x]).find(".categoriesFilterValue").text() == "") {
                                $("#" + aux[x]).find(".categoriesFilterValue").text(result[y][z].length);
                                $("#" + aux[x]).attr("style", "display: block;");
                            } else {
                                var span = $("#" + aux[x]).find(".categoriesFilterValue").text();
                                $("#" + aux[x]).find(".categoriesFilterValue").text(result[y][z].length + +span);
                            }
                        }
                    }
                }
            }
        } else {
            $(field).text("");
            for (var y = 0; y < result.length; y++) {
                for (var z in result[y]) {
                    if (z == "") { z2 = Res["unespecified"]; } else { z2 = z };
                    var string = "<a href='/Pages/RedirectTo.aspx?r=products&cl=" + encodeURI(z2) + "&t=" + $("#search").val() + "'><div class='categoriesFilterDiv'><div class='categoriesFilterName'>" + z2 + "</div><span class='categoriesFilterValue'>" + result[y][z].length + "</span></div></a>";
                    $(field).append(string);
                }
            }
        }
    },
    loadComparation: function (codes) {
        var _codes = codes;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/loadComparation",
            data: JSON.stringify({ codes: _codes, username: $("#currentUserName").val(), clientCode: $("#currentClientCode").val(), iPriceSel: $('#currentPriceSelId').val(), coefDivisaWEB: $('#coefDivisaWEB').val(), literalDivisaWEB: $('#literalDivisaWEB').val(), coeficient: $('#coeficient').val(), lcid: _spPageContextInfo.currentLanguage}),
            success: function (data) {
                var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ComparationElement.htm' });
                hbTemplate.success(
                    function (origin) {
                        var template = Handlebars.compile(origin);
                        $.each(data, function (index, article) {
                            if (article.Product.Color == "") { article.Product.Color = Res["unespecified"]; }
                            article.lineNo = index;
                            article = JuliaClients.Service.loadStockInfo(article);
                            $('#comparatorModal').append(template(article));
                            JuliaClients.Service.loadMeasures(article.Product);
                            if (article.Product.PriceWithDiscount != "") {
                                $("#Comp_Price-" + index).addClass("comparatorProductOldPrice");
                            }
                            JuliaClients.Service.setIndividualRes(article);
                        });
                        JuliaClients.Service.setRes();
                    }
                );
            },
            error: function (output) {
                alert('Error: ' + output.statusText);
            }
        });
    },
    loadMeasures: function (Product) {
        var table = $('<table></table>');
        $('#measures_' + Product.EANcode).append(table);
        var row = $('<tr></tr>');
        row.append("<th></th><th><img height='25px' src='/style library/julia/img/Height.png' /></th><th><img height='25px' src='/style library/julia/img/Width.png' /></th><th><img height='25px' src='/style library/julia/img/Depth.png' /></th>");
        $('#measures_' + Product.EANcode + ' table').append(row);
        var measures = Product.Measures;
        measures = measures.split("#");
        for (var i = 0; i < measures.length; i++) {
            var row = $('<tr></tr>');
            var row1 = $('<td></td>').text(Res["bulto"] + " " + (i + 1) + ":");
            row.append(row1);
            var values = measures[i].split(";");
            for (var j = 0; j < values.length; j++) {
                var row2 = $('<td></td>').text(values[j] + " " + Res["cm"]);
                row.append(row2);
            }
            $('#measures_' + Product.EANcode + ' table').append(row);
        }
    },
    loadWishlist: function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/loadWishlist",
            data: JSON.stringify({ username: $("#currentUserName").val(), clientCode: $("#currentClientCode").val(), iPriceSel: $('#currentPriceSelId').val(), coefDivisaWEB: $('#coefDivisaWEB').val(), literalDivisaWEB: $('#literalDivisaWEB').val(), coeficient: $('#coeficient').val(), lcid: _spPageContextInfo.currentLanguage}),
            success: function (data) {
                var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/WishlistElement.htm' });
                hbTemplate.success(
                    function (origin) {
                        var template = Handlebars.compile(origin);
                        $('#wishlist').html('');
                        $.each(data, function (index, article) {
                            article.lineNo = index;
                            article = JuliaClients.Service.loadStockInfo(article);
                            $('#wishlist').prepend(template(article));
                            if (article.Product.PriceWithDiscount != "") {
                                $("#Label_Price-" + index).css("text-decoration", "line-through");
                                $("#Label_Price-" + index).css("font-family", "Brown Light");
                            }
                            JuliaClients.Service.setIndividualRes(article);
                        });
                        JuliaClients.Service.setRes();
                    }
                );
            },
            error: function (output) {
                alert('Error: ' + output.statusText);
            }
        });
    },
    setRes: function () {
        $("[name=Programa]").text(Res["program"]);
        $("[name=CatalogNo]").text(Res["catalogLbl"]);
        $("[name=DescripcionLarga]").text(Res["productDescription"]);
        $("[name=Color]").text(Res["color"]);
        $("[name=ItemMateriales]").text(Res["productMaterials"]);
        $("[name=ItemMontaje]").text(Res["productBuilt"]);
        $("[name=Apilable]").text(Res["captionApilable"]);
        $("[name=Volume]").text(Res["productVolume"]);
        $("[name=Weight]").text(Res["productWeight"]);
        $("[name=EANCode]").text(Res["captionEAN"]);
        $("[name=CodigoArancelario]").text(Res["captionCodArancel"]);
        $("[name=PaisOrigen]").text(Res["captionPaisOrigen"]);
        $("[name=NumItems]").text(Res["numberOfPorducts"]);
        $("[name=MedidasEmbalaje]").text(Res["lblMedidasBulto"]);
        $("[name=Medidas]").text(Res["lblMedidasProducto"]);
        $("[name=PriceComp]").text(Res["price"]);
        $("[name=Price]").text(Res["price"] + ": ");
        $("[name=Quantity]").text(Res["quantity"] + ": ");
        $("[name=cm]").text(Res["cm"]);
        $(".gstock").prop('title', Res["productStockTooltip_Verde"]);
        $(".astock").prop('title', Res["productStockTooltip_Naranja"]);
        $(".rstock").prop('title', Res["productStockTooltip_Rojo"]);
        $(".bstock").prop('title', Res["productStockTooltip_Negro"]);
    },
    setIndividualRes: function (article) {
        if (article.verQty) {
            if (article.Code != null) {
                $("#gastock-" + article.Code).prop('title', String.format(Res["productStockTooltip_Gris"], article.DescripcionGris));
                if (article.NiveldeServicio != 0 && article.NiveldeServicio == 1) {
                    $("#NoNullImgNivelServicio-" + article.Code).prop('title', String.format(Res["nivelDeServicio1"], article.NiveldeservicioDescripcion));
                    $("#NullImgNivelServicio-" + article.Code).prop('title', String.format(Res["nivelDeServicio1"], article.NiveldeservicioDescripcion));
                } else if (article.NiveldeServicio != 0) {
                    $("#NoNullImgNivelServicio-" + article.Code).prop('title', String.format(Res["nivelDeServicio2"], article.NiveldeservicioDescripcion));
                    $("#NullImgNivelServicio-" + article.Code).prop('title', String.format(Res["nivelDeServicio2"], article.NiveldeservicioDescripcion));
                }
            } else {
                $("#gastock-" + article.Product.Code).prop('title', String.format(Res["productStockTooltip_Gris"], article.Product.DescripcionGris));
                if (article.Product.NiveldeServicio != 0 && article.Product.NiveldeServicio == 1) {
                    $("#NoNullImgNivelServicio-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio1"], article.Product.NiveldeservicioDescripcion));
                    $("#NullImgNivelServicio-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio1"], article.Product.NiveldeservicioDescripcion));
                } else if (article.Product.NiveldeServicio != 0) {
                    $("#NoNullImgNivelServicio-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio2"], article.Product.NiveldeservicioDescripcion));
                    $("#NullImgNivelServicio-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio2"], article.Product.NiveldeservicioDescripcion));
                }
            }
        } else {
            if (article.Code != null) {
                $("#ImgStock-" + article.Code).prop('src', JuliaClients.Service.getStockImg(article.Stock));
                $("#ImgStock-" + article.Code).prop('title', JuliaClients.Service.setStockTooltip(article.Stock, article.DescripcionGris));
                if (article.NiveldeServicio != 0 && article.NiveldeServicio == 1) {
                    $("#ImgNivelDeServicioClient-" + article.Code).prop('title', String.format(Res["nivelDeServicio1"], article.NiveldeservicioDescripcion));
                } else if (article.NiveldeServicio != 0) {
                    $("#ImgNivelDeServicioClient-" + article.Code).prop('title', String.format(Res["nivelDeServicio2"], article.NiveldeservicioDescripcion));
                }
            } else {
                $("#ImgStock-" + article.Product.Code).prop('src', JuliaClients.Service.getStockImg(article.Product.Stock));
                $("#ImgStock-" + article.Product.Code).prop('title', JuliaClients.Service.setStockTooltip(article.Product.Stock, article.Product.DescripcionGris));
                if (article.Product.NiveldeServicio != 0 && article.Product.NiveldeServicio == 1) {
                    $("#ImgNivelDeServicioClient-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio1"], article.Product.NiveldeservicioDescripcion));
                } else if (article.Product.NiveldeServicio != 0) {
                    $("#ImgNivelDeServicioClient-" + article.Product.Code).prop('title', String.format(Res["nivelDeServicio2"], article.Product.NiveldeservicioDescripcion));
                }
            }
        }
    },
    getStockImg: function (p) {
        var imgURL = "";
        var stock = p;
        switch (stock) {
            case 0:
                imgURL = "/Style%20Library/Julia/img/Red_Stock.png";
                break;
            case 1:
                imgURL = "/Style%20Library/Julia/img/Orange_Stock.png";
                break;
            case 2:
                imgURL = "/Style%20Library/Julia/img/Green_Stock.png";
                break;
            case 3:
                imgURL = "/Style%20Library/Julia/img/Black_Stock.png";
                break;
            case 4:
                imgURL = "/Style%20Library/Julia/img/Gray_Stock.png";
                break;
        }
        return imgURL;
    },
    setStockTooltip: function (p, GrisDesc) {
        var output = "";
        var stock = p;
        switch (stock) {
            case 0:
                output = Res["productStockTooltip_Rojo"];
                break;
            case 1:
                output = Res["productStockTooltip_Naranja"];
                break;
            case 2:
                output = Res["productStockTooltip_Verde"];
                break;
            case 3:
                output = Res["productStockTooltip_Negro"];
                break;
            case 4:
                output = String.format(Res["productStockTooltip_Gris"], GrisDesc);
                break;
        }
        return output;
    },
    loadStockInfo: function (article) {
        article.verQty = ($("#currentUserVerCantidades").val() === "True" || $("#currentUserVerCantidades").val() === "true");
        article.stock = (article.Product.Stock != 3);
        article.cantidadVerde = (article.Product.CantidadVerde !== 0);
        article.cantidadAzul = (article.Product.CantidadAzul !== 0);
        article.cantidadRojo = (article.Product.CantidadRojo !== 0);
        article.cantidadGris = (article.Product.CantidadGris !== 0);
        article.niveldeServicio = (article.Product.NiveldeServicio !== 0);
        article.isNew = (article.Product.ProductNew == "No") ? false : true;
        if (!(article.Product.Discount == "0,00")) {
            article.discount = (article.Product.TieneDtoExclusivo) ? false : true;
        } else { article.discount = false; }
        return article;
    },
    searchEANCode: function (eANCode, iPriceSel, username, clientCode, catalog) {
        var _eANCode = eANCode || "";
        var _iPriceSel = iPriceSel || "";
        var _username = username;
        var _clientCode = clientCode;
        var _catalog = catalog || "";

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/searchEANCode",
            //input parameters (JSON)
            data: JSON.stringify({ eANCode: _eANCode, iPriceSel: _iPriceSel, username: _username, clientCode: _clientCode, coefDivisaWEB: $('#coefDivisaWEB').val(), literalDivisaWEB: $('#literalDivisaWEB').val(), coeficient: $('#coeficient').val(), lcid: _spPageContextInfo.currentLanguage, catalog: _catalog}),
            success: function (data) {
                $('#wishlist').text("");
                JuliaClients.Service.loadWishlist();
                setWishlist();
            },
            error: function (output) {
                $('#EANCodeResult').text('Error: ' + output.statusText);
                $('#EANCodeResult').addClass("EANCodeResultIcon");
            }
        });
        $("#EANCode").val("");
    },
    addToCart: function (username, clientCode, pOfertaNo) {
        if ($("#wishlist").children().length > 0) {
            var _username = username;
            var _clientCode = clientCode;
            var _pOfertaNo = pOfertaNo;
            $.blockUI({ message: $('#loadingPnl') });
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //url to web service
                url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/addToCart",
                //input parameters (JSON)
                data: JSON.stringify({ username: _username, clientCode: _clientCode, pOfertaNo: _pOfertaNo }),
                success: function () {
                    JuliaClients.Service.loadWishlist();
                    RefreshTable();
                },
                error: function (output) {
                    alert('Error: ' + output.statusText);
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }
    },
    addToCartFromWishlist: function (codes) {
        var _codes = codes;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/addToCartFromWishlist",
            data: JSON.stringify({ codes: _codes, username: $("#currentUserName").val(), clientcode: $("#currentClientCode").val(), orderID: $("#currentOrderNo").val() }),
            success: function () {
                JuliaClients.Service.loadWishlist();
                RefreshTable();
                $("#selectAll").prop("checked", false);
            },
            error: function (output) {
                alert('Error: ' + output.statusText);
            },
            complete: function () {
                $.unblockUI();
            }
        });
    },
    deleteWishlistItem: function (code, username, clientCode) {
        if ($("#wishlist").children().length > 0) {
            var _code = code;
            var _username = username;
            var _clientCode = clientCode;

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //url to web service
                url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/deleteWishlistItem",
                //input parameters (JSON)
                data: JSON.stringify({ code: _code, username: _username, clientCode: _clientCode }),
                success: function (data) {
                    $("#wishlist #" + data).remove();
                    $('#EANCode').focus();
                    checkComparatorItems();
                    checkAddToCartCheckboxes();
                },
                error: function (output) {
                    alert('Error: ' + output.statusText);
                }
            });
        }
    },
    deleteWishlistAll: function (username, clientCode) {
        if ($("#wishlist").children().length > 0) {
            var _username = username;
            var _clientCode = clientCode;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //url to web service
                url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/deleteWishlistAll",
                //input parameters (JSON)
                data: JSON.stringify({ username: _username, clientCode: _clientCode }),
                success: function () {
                    $('#wishlist').text("");
                },
                error: function (output) {
                    alert('Error: ' + output.statusText);
                }
            });
        }
    },
    updateProductQuantity: function (quantity, code, username, clientCode) {
        var _code = code;
        var _quantity = parseInt(quantity);
        var _username = username;
        var _clientCode = clientCode;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/updateProductQuantity",
            //input parameters (JSON)
            data: JSON.stringify({ code: _code, quantity: _quantity, username: _username, clientCode: _clientCode }),
            success: function (data) { },
            error: function (output) {
                alert('Error: ' + output.statusText);
            }
        });
    },
    getArticles: function (ipos, iCount, user) {
        var _inipos = ipos || 0;
        var _itemCount = iCount || 64;
        var _username = user || "lleida";

        if (true) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //url to web service
                url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getArticles",

                //input parameters (JSON)
                data: JSON.stringify({ inipos: _inipos, itemCount: _itemCount, username: _username }),
                success: function (data) {
                    /* load habndlebars template */
                    var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ArticleCatalog.htm' });
                    var articles = data;
                    hbTemplate.success(
    	                    function (origin) {
    	                        var template = Handlebars.compile(origin);
    	                        //iterate through data
    	                        var k = 0;
    	                        $.each(articles, function (index, article) {
    	                            //view data
    	                            article["language"] = "es-ES";
    	                            $('#tblStaff').append(template(article));
    	                            $('#searchResultSpan').append(template(article));
    	                            k++;
    	                        });
    	                    }
                        );
                },
                error: function (output) {
                    $('#tblStaff').append('Error: ' + output + '<br/>');
                }
            });
        } else {
            $('#tblStaff').append("End Data");
        }
    },
    getArticlesOld: function (ipos, iCount, user) {
        var _inipos = ipos || 0;
        var _itemCount = iCount || 64;
        var _username = user || "lleida";

        if ($('#currentPos').data("end") === false) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //url to web service
                url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getArticles",

                //input parameters (JSON)
                data: JSON.stringify({ inipos: _inipos, itemCount: _itemCount, username: _username }),
                success: function (data) {
                    /* load habndlebars template */
                    var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ArticleCatalog.htm' });
                    var articles = data;
                    hbTemplate.success(
    	                    function (origin) {
    	                        var template = Handlebars.compile(origin);
    	                        //iterate through data
    	                        var k = 0;
    	                        $.each(articles, function (index, article) {
    	                            //view data
    	                            article["language"] = "es-ES";
    	                            $('#tblStaff').append(template(article));
    	                            k++;
    	                        });
    	                        var endpos = _inipos + k;
    	                        $('#currentPos').data("currentpos", endpos);
    	                        if (k < _itemCount) {
    	                            $('#currentPos').data("end", true);
    	                        }
    	                    }
                        );
                },
                error: function (output) {
                    $('#tblStaff').append('Error: ' + output + '<br/>');
                }
            });
        } else {
            $('#tblStaff').append("End Data");
        }
    },
    buyArticle: function (_ProdCode, _quantity, _InsertCat, _BLine, _LineDesc) {

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/buyArticle",

            //input parameters (JSON)
            data: JSON.stringify({ OderNo: $('#currentOrderNo').val(), ProdCode: _ProdCode, quantity: _quantity, InsertCat: _InsertCat, BLine: _BLine, LineDesc: _LineDesc, UserName: $('#currentUserName').val() }),
            success: function (data) {
                /* load habndlebars template */
                DialogCallback1(SP.UI.DialogResult.OK, data);   //Refresh Cart. Esta funcion solo está en ArticlesCatalogUserControl...
                //Al llamarla se pierde la posición del carrito...
            },
            error: function (output) {
                alert('Error: ' + output.Message);
            }
        });

    },

    getOrderLines: function (_iniPos, _maxRows) {

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getOrderLines",

            //input parameters (JSON)
            data: JSON.stringify({ OrderNo: $('#currentOrderNo').val(), Username: $('#currentUserName').val(), ClientCode: $('#currentClientCode').val(), iniPos: _iniPos, maxRows: _maxRows }),
            success: function (data) {
                /* load habndlebars template */
                var orderlines = data;
                var i = 0;
                /*$.each(orderlines, function (index, article) {
                alert('getOrderLines OK - linea ' + i.toString() + ' = ' + orderlines[i].LineNo);
                i++;
                });*/

                updateGrid(orderlines);

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    //url to web service
                    url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getOrderLinesCount",

                    //input parameters (JSON)
                    data: JSON.stringify({ OrderNo: $('#currentOrderNo').val(), Username: $('#currentUserName').val(), ClientCode: $('#currentClientCode').val() }),
                    success: function (data) {
                        updateVirtualItemCount(data);
                    },
                    error: function (output) {
                        alert('Error: ' + output.Message);
                    }
                });
                //Al llamarla se pierde la posición del carrito...
            },
            error: function (output) {
                alert('Error: ' + output.Message);
            }
        });
    },
    getOrderLinesCount: function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getOrderLinesCount",
            //input parameters (JSON)
            data: JSON.stringify({ OrderNo: $('#currentOrderNo').val(), Username: $('#currentUserName').val(), ClientCode: $('#currentClientCode').val() }),
            success: function (data) {
                var orderlines = data;
                $('#CarritoLinesNo').text(data);
            },
            error: function (output) {
                //alert('Error: ' + output.Message);
                $('#CarritoLinesNo').text('-');
            }
        });
    },
    getProductDocuments: function (cod, vers) {
        var _cod = cod || 0;
        var _vers = vers || 1;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getProductDocumentsByCode",

            //input parameters (JSON)
            data: JSON.stringify({ productCode: _cod, Version: _vers }),
            success: function (data) {
                /* load habndlebars template */
                var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ProductDocument.htm' });
                var productdocs = data;
                hbTemplate.success(
    	                    function (origin) {
    	                        var template = Handlebars.compile(origin);
    	                        //iterate through data
    	                        var itech = 0;
    	                        var icert = 0;
    	                        $.each(productdocs, function (index, productdoc) {
    	                            //view data
    	                            if (productdoc["ContentType"] != "Certificates") {
    	                                $('#ptechdocs').append(template(productdoc));
    	                                itech++;
    	                            } else {
    	                                $('#pcertdocs').append(template(productdoc));
    	                                icert++;
    	                            }
    	                            if (itech > 0) {
    	                                $('.DocumentationPanelDocumentacionTenica').show();
    	                            }
    	                            if (icert > 0) {
    	                                $('.CertificadosPanel').show();
    	                            }
    	                        });
    	                    }
                        );
            },
            error: function (output) {
                //$('#ptechdocs').append('Error: ' + output + '<br/>');
            }
        });

    },
    getProductImagesByCode: function (cod, vers, type) {
        var _cod = cod || 0;
        var _vers = vers || 1;
        var _type = type || "·1";

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //url to web service
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getProductImagesByCode",

            //input parameters (JSON)
            data: JSON.stringify({ code: _cod, version: _vers, type: _type }),
            success: function (data) {
                /* load habndlebars template */
                var hbTemplate = $.ajax({ url: '/Style Library/Julia/templates/ProductImage.htm' });
                var productimages = data;
                hbTemplate.success(
    	                    function (origin) {
    	                        var template = Handlebars.compile(origin);
    	                        //iterate through data
    	                        var i = 0;

    	                        try {
    	                            $.each(productimages, function (index, productimage) {
    	                                //view data
    	                                $('#thumbs').append(template(productimage));
    	                                if (i == 0) {
    	                                    $('#Zoomer').attr("href", productimage["BigUrl"]);
    	                                    $('#Zoomer2').attr("src", productimage["MidUrl"]);
    	                                }
    	                                i++;
    	                            });
    	                            if (i > 0) {
    	                                MagicZoomPlus.refresh();    //set zoom image
    	                                initCarousel_productInfo(); //build slider
    	                            }
    	                        }
    	                        catch (err) {
    	                            //Handle errors here
    	                        }
    	                    }
                        );
            },
            error: function (output) {
                //$('#ptechdocs').append('Error: ' + output + '<br/>');
            }
        });
    },
    getJuliaURL: function (userName, clientCode) {
        var _userName = userName;
        var _clientCode = clientCode;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getJuliaURL",
            data: JSON.stringify({ userName: _userName, clientCode: _clientCode }),
            success: function (data) {
                window.location.href = data;
            },
            error: function (output) {
                alert('Error: ' + output.statusText);
            }
        });
    },
    PrepareTreeView: function (treeView, search, color, catalog, program, provider, grupProv, estance, style, minPrice, maxPrice, filterMin, filterMax, dto, productEsEx) {
        try {
            /* sender es el objecte TreeView en client */
            /* l'event es llança tant a la càrrega inicial com despres de fer click a una opció */
            var nodes = treeView.get_nodes();
            nodes._array.forEach(function (currentValue, index, arr) {
                var node = nodes.getNode(index);
                JuliaClients.Service.getCategoryTreeViewQty(node, search, color, catalog, program, provider, grupProv, estance, style, minPrice, maxPrice, filterMin, filterMax, dto, productEsEx);
                var subnodes = node.get_nodes();
                subnodes._array.forEach(function (currentValue2, index2, arr2) {
                    var subnode = subnodes.getNode(index2);
                    JuliaClients.Service.getCategoryTreeViewQty(subnode, search, color, catalog, program, provider, grupProv, estance, style, minPrice, maxPrice, filterMin, filterMax, dto, productEsEx);
                    var subsubnodes = subnode.get_nodes();
                    subsubnodes._array.forEach(function (currentValue3, index3, arr3) {
                        var subsubnode = subsubnodes.getNode(index3);
                        JuliaClients.Service.getCategoryTreeViewQty(subsubnode, search, color, catalog, program, provider, grupProv, estance, style, minPrice, maxPrice, filterMin, filterMax, dto, productEsEx);
                    });
                });
            });
        } catch (err) { }
    },
    getCategoryTreeViewQty: function (node, search, color, catalog, program, provider, grupProv, estance, style, minPrice, maxPrice, filterMin, filterMax, dto, productEsEx) {
        var _value = node.get_value();
        var _search = search;
        var _color = color || "";
        var _program = program;
        var _catalog = catalog || "";
        var _provider = provider || "";
        var _grupProv = grupProv || "";
        var _estance = estance || "";
        var _style = style || "";
        var _minPrice = minPrice || 0;
        var _maxPrice = maxPrice || 2000;
        var _filterMin = filterMin || "";
        var _filterMax = filterMax || "";
        var _dto = dto || "0";
        var _productEsEx = productEsEx || "";
        var _tipoCompra = (color != null) ? 0 : 1;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/_vti_bin/JuliaClientsSvc/JuliaClients.svc/getCategoryTreeViewQty",
            data: JSON.stringify({ username: $("#currentUserName").val(), clientcode: $("#currentClientCode").val(), value: _value, search: _search, color: _color, program: _program, catalog: _catalog, provider: _provider, grupProv: _grupProv, estance: _estance, style: _style, minPrice: _minPrice, maxPrice: _maxPrice, filterMin: _filterMin, filterMax: _filterMax, dto: _dto, tipoCompra: _tipoCompra, productEsEx: _productEsEx }),
            success: function (data) {
                if (node.get_textElement() == null) {
                    var aux = 0;
                } else {
                    node.get_textElement().innerText += " (" + data + ")";
                }
            },
            error: function (output) { }
        });
    }
}

window.JuliaClients.Templates = {
    loadTemplate: function (templatePath, listItem, el) {
        var hbTemplate = $.ajax({ url: templatePath });
        hbTemplate.success(
    	    function (origin) {
    	        var template = Handlebars.compile(origin);
    	        $(el).html(template(listItem));
    	    }
        );
    },
    getDataKey: function (that, key) {
        return that.data(key);
    }
}
