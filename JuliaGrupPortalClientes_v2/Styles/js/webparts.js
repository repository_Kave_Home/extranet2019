﻿function RefreshCarrito() {
    $('.refreshCarrito').click();
}

function OpenProductInfoDialog(code, version) {

    var options = {
        url: "/Pages/ProductInfo.aspx?code=" + code + "&version=" + version + "&catalog=" + catalog,
        width: 900,
        height: 600,      
        // autosize: 1,
        title: '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductInfo") %>',
        dialogReturnValueCallback: DialogCallback
    };

    SP.UI.ModalDialog.showModalDialog(options);
}

function DialogCallback(dialogResult, returnValue) {
    if (dialogResult == SP.UI.DialogResult.OK) {
        RefreshTable();
    }
}

function PageIndexChanging(sender, eventArgs) {
    var oldPageIndex = eventArgs.get_oldPageIndex();
    var newPageIndex = eventArgs.get_newPageIndex();
    eventArgs.set_newPageIndex(newPageIndex);
    ProductEffects();
}

function setQttProduct(codeWS, code) {
    $('#<%= this.qttNumber.ClientID%>').val($('#' + codeWS).children('.NewitemQty').children('span').children('input[type="text"]').val());
    $('#<%= this.codeProduct.ClientID%>').val(code);
    return true;
}

//function transfereffect() {



//}

   
							
function ProductEffects() {
    $('.Container').mouseenter(function (evento) {
        $(this).children('.Contenido').css('position', 'relative');
        $(this).children('.bloq_info_front').css('position', 'relative');
        $(this).children('.Contenido, .Contenido div').css('display', 'inline-block');
        $(this).children('.bloq_info_front').css('z-index', '99');
        $(this).children('.Contenido').css('z-index', '99');
        $(this).children('#imgProducto1').css('border-top', '1px solid rgb(224,224,224)');
        $(this).children('#imgProducto1').css('border-left', '1px solid rgb(224,224,224)');
        $(this).children('#imgProducto1').css('z-index', '99');
        $(this).children('.Contenido,.bloq_info_front').css('border-left', '1px solid rgb(224,224,224)');
        //$(this).children('#imgProducto1, .Contenido').css('border-left', '1px solid rgb(224,224,224)');
        //$(this).children('#imgProducto1').css('border-top', '1px solid rgb(224,224,224)');

        $(this).children('div').css('box-shadow', 'gray 0.7em 0.7em 0.7em ');
        $(this).children('.Contenido').css('box-shadow', 'gray 0.7em 0.7em 0.7em ');
        //$(this).children('.Container, .Contenido').css('border-right', '1px solid rgb(224,224,224)');

    });
    $('.Container').mouseleave(function (evento) {
        $(this).children('.Contenido').css('position', 'static');
        $(this).children('.bloq_info_front').css('position', 'static');
        $(this).children('.Contenido, .Contenido div').css('display', 'none');
        $(this).children('.Contenido, #imgProducto1, .Container').css('box-shadow', 'none');
        $(this).children('.Contenido, #imgProducto1,.bloq_info_front').css('border', 'none');
        $(this).children('.bloq_info_front').css('z-index', '0');
        $(this).children('#imgProducto1').css('z-index', '0');

        $(this).children('.Contenido').css('z-index', '0');
        // $(this).children('#imgProducto1').css('border', '0px');
        //$(this).children('#imgProducto1').css('box-shadow', 'none');
        // $(this).removeClass('FrontItem').addClass('BackItem');
        $(this).children('.Contenido').css('box-shadow', 'none');
         $(this).children('div').css('box-shadow', 'none');


    });
}

function ShowCarrito() {
    $('.left-box-cart').css('border', 'none');
    $('.left-box-cart').css('padding', 'none');
    $(".carrito").show(1000);
    $('.prova').css('display', 'none');
    RefreshTable();
}

function HideCarrito() {

    $(".carrito").hide(1000);
    $('.prova').css('display', 'block');
    $('.left-box-cart').css('border', '1px solid #555');
    $('.left-box-cart').css('padding', '6px');

}
																																																																			  			$(this).remove();
	
