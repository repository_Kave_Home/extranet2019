﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountAdministrationUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Web_Parts.AccountAdministration.AccountAdministrationUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<SharePoint:CssRegistration ID="CssRegistrationAccountAdministrator" Name="/Style Library/Julia/AccountAdministrator.css"
    runat="server" />
<style type="text/css">
    .1stSection_LeftBox
    {
        float: left;
        width: 500px;
    }
    .LblUsername, .TxtBoxUsername
    {
        font: 12px "segoe ui" ,arial,sans-serif;
        color: Black;
        vertical-align: middle;
    }
    .SaveBtn
    {
        margin-left: 185px;
    }
    .SaveBtnMail
    {
        margin-left: 185px;
    }
</style>
<script type="text/javascript">
    //        $(document).ready(function () {
    //            var firstSrc = $(".rgCommandTable img").attr("src");
    //            var lastSRC = firstSrc.replace("add_to_car.gif", "anadir usuario.png");
    //            $(".rgCommandTable img").attr("src", lastSRC);
    //        })
    //       
    //    
</script>
<asp:Panel ID="Panel_1st_Section" CssClass="Panel_1st_Section" runat="server" Visible="false">
    <div class="1st_Secton">
        <asp:Label runat="server" CssClass="Header" ID="Header_1stSection" />
        <div class="1stSection_LeftBox" style="width: 400px; float: left;">
            <div class="Username_1stSection box_userinfo">
                <asp:Label ID="UsernameLbl" runat="server" Width="180px" CssClass="LblUsername"></asp:Label>
                <asp:Label runat="server" ID="UsernameValue" CssClass="TxtBoxUsername" />
            </div>
            <div class="Username_1stSection box_userinfo">
                <telerik:RadTextBox runat="server" ID="EmailTxtBox" Width="340px" LabelWidth="180px"
                    MaxLength="80" CssClass="Input TxtBoxEmail" onkeydown="return (event.keyCode!=13);">
                </telerik:RadTextBox>
            </div>
        </div>
        <div class="ChangePass box_userinfo">
            <telerik:RadTextBox runat="server" TextMode="Password" ID="OldPass" LabelWidth="180px"
                MaxLength="20" LabelCssClass="InputLbl" Width="340px" CssClass="Input TxtBoxOldPass"
                onkeydown="return (event.keyCode!=13);">
            </telerik:RadTextBox>
            <telerik:RadTextBox runat="server" TextMode="Password" ID="NewPass" LabelWidth="180px"
                MaxLength="20" LabelCssClass="InputLbl" Width="340px" CssClass="Input TxtBoxPass"
                Style="margin-top: 10px" onkeydown="return (event.keyCode!=13);">
            </telerik:RadTextBox>
            <telerik:RadTextBox runat="server" TextMode="Password" ID="ReNewPass" LabelWidth="180px"
                MaxLength="20" LabelCssClass="InputLbl" Width="340px" CssClass="Input TxtBoxRePass"
                Style="margin-top: 10px" onkeydown="return (event.keyCode!=13);">
            </telerik:RadTextBox>
        </div>
    </div>
</asp:Panel>
<div class="ButtonsDiv" style="width: 100%; float: left; margin-bottom: 20px;">
    <asp:ImageButton runat="server" ID="Btn_1stSection" CssClass="SaveBtnMail" OnClick="Btn_1stSection_Click"
        ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/guardar email.png%>"
        onmouseover="this.src=this.src.replace('email','email_on');" onmouseout="this.src=this.src.replace('email_on','email');" />
    <asp:ImageButton runat="server" ID="ChangePassBtn" CssClass="SaveBtnPass" OnClick="ChangePassBtn_Click"
        ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/guardar contrasena.png%>"
        onmouseover="this.src=this.src.replace('contrasena','contrasena_on');" onmouseout="this.src=this.src.replace('contrasena_on','contrasena');" />
</div>
<asp:Panel runat="server" ID="Panel_2ndSection" CssClass="Panel_2ndSection" Visible="false">
    <div class="2nd_Secton">
        <asp:Label runat="server" CssClass="Header" ID="Header_2ndSection" />
        <div class="Coeficient_2nSection box_userinfo">
            <telerik:RadNumericTextBox ID="txtCoeficient" ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                runat="server" LabelWidth="180px" Width="340px" CssClass="Input TxtBoxCoeficient"
                MaxLength="7" MinValue="0" MaxValue="100" onkeydown="return (event.keyCode!=13);">
                <NumberFormat DecimalDigits="4" />
            </telerik:RadNumericTextBox>
            <%--  <telerik:RadTextBox runat="server" ID="txtCoeficient" LabelWidth="140px" Width="300px"
                CssClass="Input TxtBoxCoeficient" MaxLength="4">
            </telerik:RadTextBox>--%>
        </div>
        <div class="Check_2nSection box_userinfo">
            <asp:CheckBox ID="CheckApply" CssClass="CheckAllButons check" runat="server" Checked="false" />
        </div>
        <!-- Divisa -->
        <div class="Check_2nSection box_userinfo">
            <asp:Label ID="LblCheckDivisa" runat="server" Width="180px" CssClass="LblUsername"></asp:Label>
            <asp:CheckBox ID="CheckDivisa" CssClass="CheckAllButons check" runat="server" Checked="false" />
        </div>
        <div class="Coeficient_2nSection box_userinfo" style="display: none;">
            <telerik:RadNumericTextBox ID="txtCoefDivisa" ShowSpinButtons="false" IncrementSettings-InterceptArrowKeys="true"
                runat="server" LabelWidth="180px" Width="340px" CssClass="Input" MaxLength="7"
                MinValue="0" MaxValue="100" Value="1" onkeydown="return (event.keyCode!=13);">
                <NumberFormat DecimalDigits="4" />
            </telerik:RadNumericTextBox>
        </div>
        <div class="Username_1stSection box_userinfo">
            <telerik:RadTextBox runat="server" ID="LiteralDivisa" Width="240px" LabelWidth="180px"
                MaxLength="5" CssClass="Input" onkeydown="return (event.keyCode!=13);">
            </telerik:RadTextBox>
        </div>
    </div>
    <div style="width: 100%; float: left; display: block;">
        <asp:ImageButton runat="server" ID="Btn_2ndSection" CssClass="SaveBtn" OnClick="Btn_2ndSection_Click"
            ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/guardar coeficiente.png%>"
            onmouseover="this.src=this.src.replace('coeficiente','coeficiente_on');" onmouseout="this.src=this.src.replace('coeficiente_on','coeficiente');" />
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="Panel_3rdSection" CssClass="Panel_3rdSection" Visible="false">
    <div class="3rd_Section">
        <asp:Label runat="server" CssClass="Header RadgridHeader" ID="Header_3rdSection" />
        <telerik:RadGrid ID="RadGrid1" Width="100%" AllowSorting="False" OnNeedDataSource="RadGrid1_NeedDataSource"
            HeaderStyle-CssClass="PedidoAdvancedHeaderClass" CssClass="Grid" AutoGenerateColumns="false"
            PageSize="9999" AllowPaging="True" EnableAjaxSkinRendering="true" AllowMultiRowSelection="True"
            runat="server" GridLines="None" ShowFooter="True" EnableLinqExpressions="false"
            AllowAutomaticUpdates="true" Style="width: auto;" OnItemDeleted="RadGrid1_ItemDeleted"
            OnItemInserted="RadGrid1_ItemInserted" OnItemUpdated="RadGrid1_ItemUpdated" OnItemCommand="RadGrid1_ItemCommand"
            OnItemDataBound="RadGrid1_ItemDataBound">
            <ClientSettings EnableRowHoverStyle="true">
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Top"
                UseAllDataFields="true" InsertItemPageIndexAction="ShowItemOnCurrentPage">
                <Columns>
                    <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                    <telerik:GridBoundColumn DataField="bloqueado" UniqueName="bloqueado" Display="false" />
                    <telerik:GridBoundColumn DataField="usuario" UniqueName="usuario" />
                    <telerik:GridBoundColumn DataField="customerNoAndName" UniqueName="customerNoAndName" />
                    <telerik:GridBoundColumn Display="false" DataField="customerNo" UniqueName="customerNo" />
                    <telerik:GridBoundColumn DataField="tipo" UniqueName="tipo" Visible="false" />
                    <telerik:GridBoundColumn DataField="tipoAcceso" UniqueName="tipoAcceso" Visible="false" />
                    <telerik:GridBoundColumn DataField="mail" UniqueName="mail" />
                    <telerik:GridBoundColumn DataField="coeficiente" UniqueName="coeficiente" />
                    <telerik:GridBoundColumn DataField="fechaCreacion" UniqueName="fechaCreacion" DataType="System.DateTime"
                        DataFormatString="{0:dd/MM/yyyy}" />
                    <telerik:GridBoundColumn DataField="fechaUltimoRegistro" UniqueName="fechaUltimoRegistro"
                        DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy}" />
                    <telerik:GridBoundColumn DataField="fehcaCaducidadPsw" UniqueName="fehcaCaducidadPsw"
                        DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy}" />
                    <telerik:GridCheckBoxColumn DataField="accesoGdo" UniqueName="accesoGdo" DataType="System.Boolean"
                        Visible="false" />
                    <telerik:GridCheckBoxColumn DataField="verStock" UniqueName="verStock" DataType="System.Boolean"
                        Visible="false" />
                    <telerik:GridButtonColumn UniqueName="EditColumn" CommandName="Edit" ButtonType="ImageButton" />
                    <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ButtonType="ImageButton"
                        ConfirmDialogType="RadWindow">
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings EditFormType="Template" CaptionDataField="Edit Form">
                    <FormTemplate>
                        <div class="addUser" style="color: Black;">
                            <br>
                            <asp:Label ID="TitleUser" runat="server">
                            </asp:Label>
                        </div>
                        <div class="EditUserFIelds">
                            <!-- mateixa vista bloquejats -->
                            <table id="EditFormTable" runat="server">
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormBloqueadoLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormBloqueado" CssClass="EditFormField" Visible="false"
                                                Enabled="false" runat="server" Text='<%# Bind("bloqueado") %>' Width="300px"
                                                onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormusuarioLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormusuario" CssClass="EditFormField" runat="server"
                                                Width="300px" MaxLength="20" Text='<%# Bind("usuario") %>' onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormPassLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormPass" CssClass="EditFormField" Enabled="true" TextMode="Password"
                                                Width="300px" runat="server" MaxLength="20" onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormRePassLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormRePass" CssClass="EditFormField" TextMode="Password"
                                                Enabled="true" Width="300px" runat="server" MaxLength="20" onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormcustomerNoLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormcustomerNo" CssClass="EditFormField" runat="server"
                                                Width="300px" Visible="false" Enabled="true" Text='<%# Bind("customerNo") %>'
                                                onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormtipoLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormtipo" CssClass="EditFormField" Visible="false" runat="server"
                                                Width="300px" Enabled="false" Text='<%# Bind("tipo") %>' onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormtipoAccesoLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormtipoAcceso" CssClass="EditFormField" Visible="false"
                                                Width="300px" Enabled="false" runat="server" Text='<%# Bind("tipoAcceso") %>'
                                                onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormmailLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormmail" CssClass="EditFormField" runat="server" Enabled="true"
                                                Width="300px" MaxLength="80" Text='<%# Bind("mail") %>' onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="EditFormCoeficienteLbl" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadTextBox ID="EditFormCoeficiente" CssClass="EditFormField" Visible="false"
                                                Enabled="true" Width="300px" MaxLength="4" runat="server" Text='<%# Bind("coeficiente") %>'
                                                onkeydown="return (event.keyCode!=13);">
                                            </telerik:RadTextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="LblEditFormAccesoGdo" runat="server" Width="115px" CssClass="LblUsername"
                                                Visible="false"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadButton ID="EditFormAccesoGdo" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                AutoPostBack="false" Checked='<%# Bind("accesoGdo") %>' Visible="false">
                                                <ToggleStates>
                                                    <telerik:RadButtonToggleState Value="true"></telerik:RadButtonToggleState>
                                                    <telerik:RadButtonToggleState Value="false"></telerik:RadButtonToggleState>
                                                </ToggleStates>
                                            </telerik:RadButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="EditFormField">
                                            <asp:Label ID="LbLEditFormVerStock" runat="server" Width="115px" CssClass="LblUsername"
                                                Visible="false"></asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="EditFormField">
                                            <telerik:RadButton ID="EditFormVerStock" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                AutoPostBack="false" Checked='<%# Bind("verStock") %>' Visible="false">
                                                <ToggleStates>
                                                    <telerik:RadButtonToggleState Value="true"></telerik:RadButtonToggleState>
                                                    <telerik:RadButtonToggleState Value="false"></telerik:RadButtonToggleState>
                                                </ToggleStates>
                                            </telerik:RadButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="EditFormField">
                                <div class="addItemButton">
                                    <asp:LinkButton ID="LinkButton1" Width="11px" Height="11px" runat="server" CommandName='<%# ((bool)DataBinder.Eval(Container, "OwnerTableView.IsItemInserted")) ? "SaveLine" : "UpdateLine" %>'
                                        CssClass="savebut"></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" Width="11px" Height="11px" runat="server" CausesValidation="False"
                                        CommandName="Cancel" CssClass="raditemButton cancelEditAdd"></asp:LinkButton>
                                    <asp:Label runat="server" ID="ErrorTxt" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </FormTemplate>
                </EditFormSettings>
                <EditItemStyle ForeColor="Gray"></EditItemStyle>
                <CommandItemSettings AddNewRecordImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/anadir usuario.png%>" />
            </MasterTableView>
            <PagerStyle Mode="NextPrev" />
        </telerik:RadGrid>
    </div>
</asp:Panel>
<script type="text/javascript">

    $(".rgCommandTable").find("img")
    .hover(function () {
        $(this).attr("src", $(this).attr("src").replace("usuario", "usuario_on"));
    }, function () {
        $(this).attr("src", $(this).attr("src").replace("usuario_on", "usuario"));
    })

</script>
