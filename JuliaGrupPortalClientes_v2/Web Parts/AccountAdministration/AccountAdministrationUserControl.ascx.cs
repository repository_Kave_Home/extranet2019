﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.Utils;
using System.Text.RegularExpressions;
using JuliaGrupUtils.ErrorHandler;
using System.ComponentModel;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Data;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Globalization;
using System.Web.UI.HtmlControls;
namespace JuliaGrupPortalClientes_v2.Web_Parts.AccountAdministration
{
    public partial class AccountAdministrationUserControl : UserControl
    {
        User CurrentUser;
        Client CurrentClient;
        bool invalid = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                CurrentUser = (User)Session["User"];
            }
            if (!IsPostBack)
            {

                if ((CurrentUser.AccessType == "Admin") && (CurrentUser is Client))
                {
                    LoadClientList();
                    this.Panel_1st_Section.Visible = true;
                    this.Panel_2ndSection.Visible = true;
                    this.Panel_3rdSection.Visible = true;
                    this.CheckApply.Visible = false;
                }
                else if (CurrentUser is Agent)
                {
                    this.RadGrid1.DataSource = null;
                    this.RadGrid1.Enabled = false;
                    this.RadGrid1.Visible = false;
                    this.Panel_1st_Section.Visible = true;
                    this.Panel_2ndSection.Visible = true;
                    this.CheckApply.Visible = false;
                }
                else if (CurrentUser is ClientGroup)
                {
                    this.RadGrid1.DataSource = null;
                    this.RadGrid1.Enabled = false;
                    this.RadGrid1.Visible = false;
                    this.Panel_1st_Section.Visible = true;
                    this.Panel_2ndSection.Visible = true;
                    this.CheckApply.Visible = true;
                }
                else
                {
                    this.Panel_1st_Section.Visible = true;

                }
                LiteralControl ltr = new LiteralControl();
                ltr.Text = "<style type=\"text/css\" rel=\"stylesheet\">" +
                        @"div.RadUpload .ruAdd
                    {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/anadir usuario.png')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                    }
                  
                    </style>
                    ";
                this.Page.Header.Controls.Add(ltr);

                SetInitialValues();

                LoadValues();

            }
            else
            {

            }

        }


        private void LoadValues()
        {
            try
            {

                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                //FIRST SECTIO N VALUES
                this.UsernameValue.Text = CurrentUser.UserName;
                this.EmailTxtBox.Text = CurrentUser.Email;
                //2ndSECTION VALUES
                this.txtCoeficient.Text = CurrentUser.Coeficient.ToString("N4");
                //Checked
                this.CheckDivisa.Checked = CurrentClient.VerDivisaWEB;
                this.txtCoefDivisa.Text = CurrentClient.CoefDivisaWEB.ToString("N4");
                this.LiteralDivisa.Text = CurrentClient.LiteralDivisaWEB;

                //3rd Section VALUES

            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void LoadClientList()
        {
            try
            {
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                UsuariosWebDataAccesObject UsuariosWebDAO = new UsuariosWebDataAccesObject(CurrentUser.UserName, CurrentClient.Code);
                List<UsuariosWEB> usuariosweb = UsuariosWebDAO.GetUsuariosList();
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert', alertCallBackFn); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        private void SetInitialValues()
        {
            try
            {
                this.Header_1stSection.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header1st_Section");
                this.Header_2ndSection.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header2nd_Section");
                this.Header_3rdSection.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header3rd_Section");
                this.UsernameLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Administration_Name");

                this.EmailTxtBox.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Administration_Email");
                this.txtCoeficient.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TxtCoeficient");

                this.CheckApply.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ApplyAllClients");
                //28072014 - Divisa
                this.txtCoefDivisa.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("txtCoefDivisa");
                this.CheckDivisa.Text = ""; // JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CheckDivisa");
                this.LblCheckDivisa.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CheckDivisa");
                this.LiteralDivisa.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LiteralDivisa");


                this.OldPass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OldPass");
                this.NewPass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NewPass");
                this.ReNewPass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ReNewPass");

                //text fot Buttons
                //this.Btn_1stSection.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SaveData");
                //this.Btn_2ndSection.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SaveData");
                //this.ChangePassBtn.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SaveData");
                //this.RadGrid1.MasterTableView.CommandItemSettings.AddNewRecordImageUrl = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NewRecordImgAccount");

                //Headers


                this.RadGrid1.Columns.FindByUniqueNameSafe("bloqueado").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Bloqueado");
                this.RadGrid1.Columns.FindByUniqueNameSafe("usuario").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Usuario");
                this.RadGrid1.Columns.FindByUniqueNameSafe("customerNoAndName").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_CodClient");
                this.RadGrid1.Columns.FindByUniqueNameSafe("tipo").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Tipo");
                this.RadGrid1.Columns.FindByUniqueNameSafe("tipoAcceso").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Tipo_Acceso");
                this.RadGrid1.Columns.FindByUniqueNameSafe("mail").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Mail");
                this.RadGrid1.Columns.FindByUniqueNameSafe("coeficiente").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_Coeficiente");
                //REZ 29042013 -- quitar literal €
                //this.RadGrid1.Columns.FindByUniqueNameSafe("fechaCreacion").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_FechaCreacion") + " (" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
                this.RadGrid1.Columns.FindByUniqueNameSafe("fechaCreacion").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_FechaCreacion");
                this.RadGrid1.Columns.FindByUniqueNameSafe("fechaUltimoRegistro").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_FechaUltimoRegistro");
                this.RadGrid1.Columns.FindByUniqueNameSafe("fehcaCaducidadPsw").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_FechaCaducidadPsw");
                ((GridButtonColumn)RadGrid1.MasterTableView.GetColumnSafe("DeleteColumn")).ConfirmText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteUser");
                //REZ 10072014 - Añadimos columnas de AccesoGDO y VerStock
                this.RadGrid1.Columns.FindByUniqueNameSafe("accesoGdo").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_AccesoGDO");
                this.RadGrid1.Columns.FindByUniqueNameSafe("verStock").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Header_VerStocks");
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                if (this.CurrentClient.AccesoGDO)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("accesoGdo").Visible = true;
                }
                if (this.CurrentClient.VerCantidades)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("verStock").Visible = true;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public bool CanConvert<T>(string data)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
            return converter.IsValid(data);
        }
        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;



            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn, @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                      RegexOptions.IgnoreCase);
            }
            catch (NavException ex)
            {
                return false;
            }
        }
        //RADGRID COMANDS!
        private void LoadSelectedUsers()
        {

            if (Session["User"] != null)
            {
                CurrentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                CurrentClient = (Client)Session["Client"];
            }
            UsuariosWebDataAccesObject UsuariosWebDAO = new UsuariosWebDataAccesObject(CurrentUser.UserName, CurrentClient.Code);

            List<UsuariosWEB> usuariosweb = UsuariosWebDAO.GetUsuariosList();
            try
            {
                DataTable data = new DataTable();

                data.Columns.Add("bloqueado");
                data.Columns.Add("usuario");
                data.Columns.Add("customerNoAndName");
                data.Columns.Add("customerNo");
                data.Columns.Add("tipo");
                data.Columns.Add("tipoAcceso");
                data.Columns.Add("mail");
                data.Columns.Add("coeficiente");
                data.Columns.Add("fechaCreacion", typeof(DateTime));
                data.Columns.Add("fechaUltimoRegistro", typeof(DateTime));
                data.Columns.Add("fehcaCaducidadPsw", typeof(DateTime));
                data.Columns.Add("accesoGdo", typeof(bool));
                data.Columns.Add("verStock", typeof(bool));

                data.Columns.Add("CurrentUserUsername");


                foreach (UsuariosWEB usuario in usuariosweb)
                {
                    DataRow row = data.NewRow();

                    row["bloqueado"] = usuario.bloqueado;
                    row["usuario"] = usuario.usuario;
                    row["tipo"] = usuario.tipo;
                    row["tipoAcceso"] = usuario.tipoAcceso;
                    row["customerNoAndName"] = usuario.customerNo + " - " + usuario.customerName;
                    row["customerNo"] = usuario.customerNo;
                    row["mail"] = usuario.mail;
                    row["coeficiente"] = usuario.coeficiente;
                    row["fechaCreacion"] = usuario.fechaCreacion;
                    row["fechaUltimoRegistro"] = usuario.fechaUltimoRegistro;
                    row["fehcaCaducidadPsw"] = usuario.fehcaCaducidadPsw;

                    row["accesoGdo"] = usuario.accesoGdo;
                    row["verStock"] = usuario.verStock;

                    row["CurrentUserUsername"] = CurrentUser.UserName;

                    data.Rows.Add(row);
                }



                this.RadGrid1.DataSource = data;
                //this.RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadSelectedUsers();

            //this.RadGrid1.DataBind();

        }
        public void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                e.KeepInInsertMode = false;
            }
        }
        public void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            string bloqueado = string.Empty;
            string CodClient = string.Empty;
            string Pass = string.Empty;
            string RePass = string.Empty;
            Decimal Coeficiente = Decimal.Zero;
            string Mail = string.Empty;
            string Tipo = string.Empty;
            string TipoAcceso = string.Empty;
            string user = string.Empty;
            bool bAccesoGdo = false;
            bool bVerStock = false;

            if (Session["User"] != null)
            {
                CurrentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                CurrentClient = (Client)Session["Client"];
            }
            UserDataAccessObject userDAO = new UserDataAccessObject();
            try
            {
                if (e.CommandName == RadGrid.EditCommandName)
                {
                    RadGrid1.MasterTableView.IsItemInserted = false;

                }
                if (e.CommandName == RadGrid.InitInsertCommandName)
                {
                    //RadGrid1.MasterTableView.ClearEditItems();

                    e.Canceled = true;
                    //Prepare an IDictionary with the predefined values   
                    System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                    //set initial checked state for the checkbox on init insert   
                    newValues["accesoGdo"] = false;
                    newValues["verStock"] = false;

                    //Insert the item and rebind   
                    e.Item.OwnerTableView.InsertItem(newValues);



                }

                switch (e.CommandName)
                {
                    case ("SaveLine"):



                        Pass = (e.Item.FindControl("EditFormPass") as RadTextBox).Text;
                        RePass = (e.Item.FindControl("EditFormRePass") as RadTextBox).Text;
                        string CoeficienteString = ((e.Item.FindControl("EditFormCoeficiente") as RadTextBox).Text);
                        CoeficienteString = "0";
                        Mail = (e.Item.FindControl("EditFormmail") as RadTextBox).Text;

                        user = (e.Item.FindControl("EditFormusuario") as RadTextBox).Text;

                        bAccesoGdo = (e.Item.FindControl("EditFormAccesoGdo") as RadButton).Checked;
                        bVerStock = (e.Item.FindControl("EditFormVerStock") as RadButton).Checked;

                        if (Pass == RePass && IsValidEmail(Mail) && CanConvert<Decimal>(CoeficienteString))
                        {
                            var regexItem = new Regex("^[a-zA-Z0-9_.-]*$");

                            if (regexItem.IsMatch(user))
                            {
                                userDAO.AddUsuario(CurrentUser.UserName, user, Pass, CurrentUser.Code, Mail, Convert.ToDecimal(CoeficienteString), bAccesoGdo, bVerStock);
                                string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserAdded") + "', 320, 100," + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserAdded") + "); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                            }
                            else
                            {
                                throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidUsername"));
                            }

                        }
                        else
                        {
                            throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidData"));
                        }



                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;

                    case ("Cancel"):
                        break;
                    case ("Delete"):
                        String User = this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem["usuario"].Text;
                        userDAO.DeleteUsuario(CurrentUser.UserName, User);

                        break;
                    case ("UpdateLine"):


                        RadTextBox codclienttxt = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormcustomerNo");
                        RadTextBox passtx = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormPass");
                        RadTextBox passtxRe = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormRePass");
                        RadTextBox coeficientetxt = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormCoeficiente");
                        RadTextBox Mailtxt = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormmail");
                        RadTextBox usertxt = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormusuario");

                        RadButton AccesoGdotxt = (RadButton)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormAccesoGdo");
                        RadButton VerStocktxt = (RadButton)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("EditFormVerStock");





                        if (passtx.Text == passtxRe.Text && IsValidEmail(Mailtxt.Text))
                        {

                            userDAO.UpdateUsuario(CurrentUser.UserName, usertxt.Text, passtx.Text, codclienttxt.Text, Mailtxt.Text, Convert.ToDecimal(coeficientetxt.Text), AccesoGdotxt.Checked, VerStocktxt.Checked);

                            string radalertscript = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserUpdated") + "', 320, 100," + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserAdded") + "); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);


                        }
                        else
                        {
                            throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidData"));
                        }







                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                    case (RadGrid.InitInsertCommandName):


                        break;
                    case (RadGrid.EditCommandName):


                        //------------------------------------------------
                        e.Item.Edit = false;
                        RadGrid1.Rebind();


                        break;



                    default:
                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;

                }
            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        public void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        {

        }
        public void RadGrid1_ItemUpdated(object source, GridUpdatedEventArgs e)
        {

        }
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if ((e.Item is GridEditFormInsertItem) && e.Item.IsInEditMode)
            {
                GridEditFormItem Form = (e.Item as GridEditFormItem);
                RadTextBox Bloqueado = (Form.FindControl("EditFormBloqueado") as RadTextBox);
                //Bloqueado.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditBloqueado");
                if (Bloqueado.Visible)
                {
                    Label Bloqueadolbl = (Form.FindControl("EditFormBloqueadoLbl") as Label);
                    Bloqueadolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditBloqueado");
                }
                else
                {
                    Bloqueado.Parent.Parent.Visible = false;
                }

                RadTextBox CodClient = (Form.FindControl("EditFormcustomerNo") as RadTextBox);
                //CodClient.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditcode");
                if (CodClient.Visible)
                {
                    Label CodClientlbl = (Form.FindControl("EditFormcustomerNoLbl") as Label);
                    CodClientlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditcode");
                }
                else
                {
                    CodClient.Parent.Parent.Visible = false;
                }

                RadTextBox Pass = (Form.FindControl("EditFormPass") as RadTextBox);
                //Pass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditPass");
                if (Pass.Visible)
                {
                    Label Passlbl = (Form.FindControl("EditFormPassLbl") as Label);
                    Passlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditPass");
                }
                else
                {
                    Pass.Parent.Parent.Visible = false;
                }

                RadTextBox RePass = (Form.FindControl("EditFormRePass") as RadTextBox);
                //RePass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditRePass");
                if (RePass.Visible)
                {
                    Label RePasslbl = (Form.FindControl("EditFormRePassLbl") as Label);
                    RePasslbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditRePass");
                }
                else
                {
                    RePass.Parent.Parent.Visible = false;
                }

                RadTextBox Coeficiente = (Form.FindControl("EditFormCoeficiente") as RadTextBox);
                if (Coeficiente.Visible)
                {
                    Label Coeficientelbl = (Form.FindControl("EditFormCoeficienteLbl") as Label);
                    Coeficientelbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditCoeficiente");
                }
                else
                {  
                    Coeficiente.Parent.Parent.Visible = false;
                }

                RadTextBox Mail = (Form.FindControl("EditFormmail") as RadTextBox);
                //Mail.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditMail");
                if (Mail.Visible)
                {
                    Label Maillbl = (Form.FindControl("EditFormmailLbl") as Label);
                    Maillbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditMail");
                }
                else
                {
                    Mail.Parent.Parent.Visible = false;
                }

                RadTextBox Tipo = (Form.FindControl("EditFormtipo") as RadTextBox);
                //Tipo.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipo");
                if (Tipo.Visible)
                {
                    Label Tipolbl = (Form.FindControl("EditFormtipoLbl") as Label);
                    Tipolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipo");
                }
                else
                {
                    Tipo.Parent.Parent.Visible = false;
                }

                RadTextBox TipoAcceso = (Form.FindControl("EditFormtipoAcceso") as RadTextBox);
                //TipoAcceso.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipoAcceso");
                if (TipoAcceso.Visible)
                {
                    Label TipoAccesolbl = (Form.FindControl("EditFormtipoAccesoLbl") as Label);
                    TipoAccesolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipoAcceso");
                }
                else
                {
                    TipoAcceso.Parent.Parent.Visible = false;
                }

                /*RadTextBox Bloqueado = (Form.FindControl("EditFormBloqueado") as RadTextBox);
                Bloqueado.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditBloqueado");

                RadTextBox CodClient = (Form.FindControl("EditFormcustomerNo") as RadTextBox);
                CodClient.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditcode");

                RadTextBox Pass = (Form.FindControl("EditFormPass") as RadTextBox);
                Pass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditPass");

                RadTextBox RePass = (Form.FindControl("EditFormRePass") as RadTextBox);

                RePass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditRePass");


                RadTextBox Coeficiente = (Form.FindControl("EditFormCoeficiente") as RadTextBox);
                Coeficiente.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditCoeficiente");

                RadTextBox Mail = (Form.FindControl("EditFormmail") as RadTextBox);
                Mail.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditMail");

                RadTextBox Tipo = (Form.FindControl("EditFormtipo") as RadTextBox);
                Tipo.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipo");

                RadTextBox TipoAcceso = (Form.FindControl("EditFormtipoAcceso") as RadTextBox);
                TipoAcceso.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipoAcceso");*/

                RadTextBox user = (Form.FindControl("EditFormusuario") as RadTextBox);
                //user.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditUser");
                if (user.Visible)
                {
                    Label userlbl = (Form.FindControl("EditFormusuarioLbl") as Label);
                    userlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditUser");
                }
                else
                {
                    user.Parent.Parent.Visible = false;
                }

                if (this.CurrentClient.AccesoGDO)
                {
                    RadButton AccesoGDO = (Form.FindControl("EditFormAccesoGdo") as RadButton);
                    AccesoGDO.Visible = true;
                    Label lbAccesoGDO = (Form.FindControl("LblEditFormAccesoGdo") as Label);
                    lbAccesoGDO.Visible = true;
                    lbAccesoGDO.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditAccesoGdo");
                    //LblEditFormAccesoGdo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditAccesoGdo");
                }
                if (this.CurrentClient.VerCantidades)
                {
                    RadButton VerStock = (Form.FindControl("EditFormVerStock") as RadButton);
                    VerStock.Visible = true;
                    Label lbVerStock = (Form.FindControl("LblEditFormVerStock") as Label);
                    lbVerStock.Visible = true;
                    lbVerStock.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditFormVerStock");
                    //LbLEditFormVerStock.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditVerStock");
                }

            }
            if (!(e.Item is GridEditFormInsertItem) && e.Item.IsInEditMode)
            {

                GridEditFormItem Form = (e.Item as GridEditFormItem);

                RadTextBox Bloqueado = (Form.FindControl("EditFormBloqueado") as RadTextBox);
                //Bloqueado.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditBloqueado");
                if (Bloqueado.Visible)
                {
                    Label Bloqueadolbl = (Form.FindControl("EditFormBloqueadoLbl") as Label);
                    Bloqueadolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditBloqueado");
                }
                else
                {
                    Bloqueado.Parent.Parent.Visible = false;
                }

                RadTextBox CodClient = (Form.FindControl("EditFormcustomerNo") as RadTextBox);
                //CodClient.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditcode");
                if (CodClient.Visible)
                {
                    Label CodClientlbl = (Form.FindControl("EditFormcustomerNoLbl") as Label);
                    CodClientlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditcode");
                }
                else
                {
                    CodClient.Parent.Parent.Visible = false;
                }

                RadTextBox Pass = (Form.FindControl("EditFormPass") as RadTextBox);
                //Pass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditPass");
                if (Pass.Visible)
                {
                    Label Passlbl = (Form.FindControl("EditFormPassLbl") as Label);
                    Passlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditPass");
                }
                else
                {
                    Pass.Parent.Parent.Visible = false;
                }

                RadTextBox RePass = (Form.FindControl("EditFormRePass") as RadTextBox);
                //RePass.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditRePass");
                if (RePass.Visible)
                {
                    Label RePasslbl = (Form.FindControl("EditFormRePassLbl") as Label);
                    RePasslbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditRePass");
                }
                else
                {
                    RePass.Parent.Parent.Visible = false;
                }

                RadTextBox Coeficiente = (Form.FindControl("EditFormCoeficiente") as RadTextBox);
                Coeficiente.Enabled = false;

                RadTextBox Mail = (Form.FindControl("EditFormmail") as RadTextBox);
                //Mail.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditMail");
                if (Mail.Visible)
                {
                    Label Maillbl = (Form.FindControl("EditFormmailLbl") as Label);
                    Maillbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditMail");
                }
                else
                {
                    Mail.Parent.Parent.Visible = false;
                }

                RadTextBox Tipo = (Form.FindControl("EditFormtipo") as RadTextBox);
                //Tipo.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipo");
                if (Tipo.Visible)
                {
                    Label Tipolbl = (Form.FindControl("EditFormtipoLbl") as Label);
                    Tipolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipo");
                }
                else
                {
                    Tipo.Parent.Parent.Visible = false;
                }

                RadTextBox TipoAcceso = (Form.FindControl("EditFormtipoAcceso") as RadTextBox);
                //TipoAcceso.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipoAcceso");
                if (TipoAcceso.Visible)
                {
                    Label TipoAccesolbl = (Form.FindControl("EditFormtipoAccesoLbl") as Label);
                    TipoAccesolbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditTipoAcceso");
                }
                else
                {
                    TipoAcceso.Parent.Parent.Visible = false;
                }

                RadTextBox user = (Form.FindControl("EditFormusuario") as RadTextBox);
                //user.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditUser");
                if (user.Visible)
                {
                    Label userlbl = (Form.FindControl("EditFormusuarioLbl") as Label);
                    userlbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditUser");
                }
                else
                {
                    user.Parent.Parent.Visible = false;
                }

                user.Enabled = false;

                if (this.CurrentClient.AccesoGDO)
                {
                    RadButton AccesoGDO = (Form.FindControl("EditFormAccesoGdo") as RadButton);
                    AccesoGDO.Visible = true;
                    Label lbAccesoGDO = (Form.FindControl("LblEditFormAccesoGdo") as Label);
                    lbAccesoGDO.Visible = true;
                    lbAccesoGDO.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditAccesoGdo");
                    //LblEditFormAccesoGdo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditAccesoGdo");
                }
                if (this.CurrentClient.VerCantidades)
                {
                    RadButton VerStock = (Form.FindControl("EditFormVerStock") as RadButton);
                    VerStock.Visible = true;
                    Label lbVerStock = (Form.FindControl("LblEditFormVerStock") as Label);
                    lbVerStock.Visible = true;
                    lbVerStock.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditFormVerStock");
                    //LbLEditFormVerStock.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UserEditVerStock");
                }

            }
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item["fechaCreacion"].Text == "01/01/0001")
                {
                    item["fechaCreacion"].Text = "";
                }
                if (item["fechaUltimoRegistro"].Text == "01/01/0001")
                {
                    item["fechaUltimoRegistro"].Text = "";
                }
                if (item["fehcaCaducidadPsw"].Text == "01/01/0001")
                {
                    item["fehcaCaducidadPsw"].Text = "";
                }

            }


        }

        protected void Btn_1stSection_Click(object sender, EventArgs e)
        {
            try
            {

                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                UsuariosWebDataAccesObject usuariosWEBDAO = new UsuariosWebDataAccesObject(CurrentUser.UserName, CurrentClient.Code);

                if (IsValidEmail(this.EmailTxtBox.Text))
                {

                    usuariosWEBDAO.UpdateMail(CurrentUser.UserName, CurrentClient.UserName, this.EmailTxtBox.Text);

                    CurrentUser.Email = EmailTxtBox.Text;
                    Session.Add("User", CurrentUser);
                    Session["User"] = this.CurrentUser;
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DatosModificados") + "', 320, 100,''); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                }
                else
                {
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EmailFormatIncorrect") + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);



                }
                this.RadGrid1.Rebind();


            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);

            }
            catch (Exception ex)
            {


            }

        }

        protected void ChangePassBtn_Click(object sender, EventArgs e)
        {
            try
            {

                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (!(string.IsNullOrEmpty(this.OldPass.Text)) && (!(string.IsNullOrEmpty(this.NewPass.Text))) && (!(string.IsNullOrEmpty(this.ReNewPass.Text))))
                {
                    if (this.NewPass.Text == this.ReNewPass.Text)
                    {
                        UserDataAccessObject UserDAO = new UserDataAccessObject();
                        UserDAO.ChangePass(CurrentUser.UserName, this.OldPass.Text, this.NewPass.Text);
                        string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DatosModificados") + "', 320, 100,''); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                        this.RadGrid1.Rebind();
                    }
                    else
                    {
                        string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidData") + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                    }
                }
                else
                {
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidData") + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                }

            }
            catch (NavException ex)
            {

                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void Btn_2ndSection_Click(object sender, EventArgs e)
        {
            try
            {
                Decimal coef = Decimal.Zero;
                Decimal coefDivisa = Decimal.Zero;
                bool Todos = false;
                if (!(string.IsNullOrEmpty(txtCoeficient.Text)))
                {
                    coef = Convert.ToDecimal(txtCoeficient.Text);
                }
                if (!(string.IsNullOrEmpty(txtCoefDivisa.Text)))
                {
                    coefDivisa = Convert.ToDecimal(txtCoefDivisa.Text);
                }

                Todos = this.CheckApply.Checked;
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                UsuariosWebDataAccesObject usuariosWEBDAO = new UsuariosWebDataAccesObject(CurrentUser.UserName, CurrentClient.Code);
                //(CheckDivisa.Checked) &&
                if (
                        ((CheckDivisa.Checked && !(string.IsNullOrEmpty(LiteralDivisa.Text)))
                        &&
                        (!(string.IsNullOrEmpty(txtCoefDivisa.Text)) && (CanConvert<Decimal>(txtCoefDivisa.Text))))
                        || (!CheckDivisa.Checked)
                    )
                {
                    //REZ 29072014 - Actualizamos los campos de divisa alternativa
                    usuariosWEBDAO.UpdateDivisa(CurrentUser.UserName, CurrentClient.UserName, CurrentClient.Code, CheckDivisa.Checked, coefDivisa, LiteralDivisa.Text, Todos);
                    CurrentClient.VerDivisaWEB = CheckDivisa.Checked;
                    CurrentClient.LiteralDivisaWEB = LiteralDivisa.Text;
                    CurrentClient.CoefDivisaWEB = coefDivisa;
                    Session.Add("Client", CurrentClient);
                    Session["Client"] = CurrentClient;
                }
                else
                {
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaFormatIncorrect") + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                }

                if (!(string.IsNullOrEmpty(txtCoeficient.Text)) && (CanConvert<Decimal>(txtCoeficient.Text)))
                {
                    usuariosWEBDAO.UpdateCoef(CurrentUser.UserName, CurrentClient.UserName, CurrentClient.Code, coef, Todos);
                    CurrentUser.Coeficient = Convert.ToDecimal(txtCoeficient.Text);
                    Session.Add("User", CurrentUser);
                    Session["User"] = this.CurrentUser;
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DatosModificados") + "', 320, 100,'Modificación de Datos'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                }
                else
                {
                    string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CoeficientFormatIncorrect") + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                }
                this.RadGrid1.Rebind();
            }
            catch (NavException ex)
            {

                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
};