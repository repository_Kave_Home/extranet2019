﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarritoUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.Carrito.CarritoUserControl" %>
<%@ Register TagPrefix="julia" TagName="mainPedidoCart" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/MainPedidoCartUserControl.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<!-- link href="/Style%20Library/Julia/Pedido.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationPedido" name="/Style Library/Julia/Pedido.css" runat="server"/>

<%-- 
<asp:Literal runat="server" ID="StyleBackImg" />
<telerik:RadAjaxManagerProxy runat="server" ID="RadAjaxManagerProxy1">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnDeleteCart">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="mainPedidoCartPanel" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>

      <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ImageButton1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="mainPedidoCartPanel" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>


</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" Skin="Vista">
</telerik:RadAjaxLoadingPanel>

<div id="containerExpanded" class="containerNewOrderMax">
<div class="hide_Carrito" onclick="HideCarrito()">
<img src='/Style%20Library/Julia/img/back.png' width="32px" alt="" height="32px" />
</div>
    <div class="groupOrderExp">
        <table>
            <tr class="orderLine">
                <td class="columnOrderLine">
                    <asp:Label ID="lblLineTitleExp" CssClass="txtOrderLine" runat="server" />
                </td>
                <td class="columnOrderLine">
                    <asp:TextBox ID="orderTitle" CssClass="inputOrderLine" runat="server" Width="148px"/>
                </td>
            </tr>
            <tr class="orderLine">
                <td class="columnOrderLine">
                    <asp:Label ID="lblLinePayMethodExp" CssClass="txtOrderLine" runat="server" />
                </td>
                <td class="columnOrderLine">
                    <telerik:RadComboBox ID="PayMethodList" Enabled="false" runat="server" Width="150px" DataTextField="value"
                        DataValueField="id"/>
                </td>
            </tr>
            <tr class="orderLine">
                <td class="columnOrderLine">
                    <asp:Label ID="lblLineDestAddressExp" CssClass="txtOrderLine" runat="server" />
                </td>
                <td class="columnOrderLine">
                    <telerik:RadComboBox ID="DestinationAddressList" runat="server" Width="150px" DataTextField="value"
                        DataValueField="id" />
                </td>
            </tr>
            <tr class="orderLine">
                <td class="columnOrderLine">
                    <asp:Label ID="lblLineDestinationExp" CssClass="txtOrderLine" runat="server" />
                </td>
                <td class="columnOrderLine">
                   <asp:RadioButtonList runat="server" ID="ShipmentMethod" CssClass="radioOrderLine" RepeatDirection="Horizontal" Enabled="true">                    
                   </asp:RadioButtonList>                   
                </td>
            </tr>
        </table>
    </div>
    <div class="groupSelectedProducts">
        <!--Selected products title-->
        <div class="groupSelectedProductsTitle">
            <asp:Label ID="lblSelectedProductsTitle" CssClass="txtSelectedProductsTitle" runat="server" />
            <div class="imgSelectedProductsTitle">

                <telerik:RadAjaxPanel ID="PanelDeleteCard" runat="server">
                    <asp:ImageButton ID ="btnDeleteCart" ImageUrl="/Style%20Library/Julia/img/Borrar_carrito.jpg" runat="server" width="32px" Height="32px" DescriptionUrl="Borrar detalles de pedido" OnClick="DeleteCard" />
                </telerik:RadAjaxPanel>
            
            </div>
        </div>
     
        <!--Selected products box-->
        <div class="classBlueBoxPedidos">
            <div class="groupOrderLines">
                <asp:Panel ID="mainPedidoCartPanel" runat="server">
                    <julia:mainPedidoCart id="mainPedidoCart" runat="server">
                    </julia:mainPedidoCart>
                </asp:Panel>
                <div class="itemOrderLineGlobalPrice" style="display:none">
                    <asp:Label ID="lblTotal" CssClass="txtOrderLineGlobalPrice" runat="server" />
                </div>
            </div>
            <!--Selected products box footer-->
            <div class="imagenesOrdenPedido_footer">
                <asp:LinkButton ID="ImageButton2" runat="server" OnClick="PlaceOrder">
                    <div class="imgOrderButton"></div>


                    <div class="textOrderButton">
                        <asp:Literal ID="SubmitOrderLiteral" runat="server" Text="" />
                    </div>
                </asp:LinkButton></div></div></div><!--Julia Error:<asp:Label ID="ErrorLabel" runat="server" />--></div>
--%>


<script type="text/javascript">
    $(document).ready(function () {
        RefreshTable();
    });
    function RefreshTable() {
        JuliaClients.Service.getOrderLinesCount();
    }
</script>

<div class="LblCarrito">
    <span id="CarritoLinesNo" class="alignMiddle"></span>
</div>