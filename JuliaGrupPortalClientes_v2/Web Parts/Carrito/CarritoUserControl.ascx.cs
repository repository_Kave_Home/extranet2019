﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using System.Threading;
using System.Diagnostics;
using Microsoft.SharePoint;
using JuliaGrupUtils.ErrorHandler;




namespace JuliaGrupPortalClientes_v2.Web_Parts.Carrito
{
    public partial class CarritoUserControl : UserControl
    {
        public string IssuesPageURL { get; set; }
        private ArticleDataAccessObject articleDAO;
        private VentaDataAccessObject ventaDAO;
        public Order order;
        private User currentUser;
        public int expanded = 0;

        protected override void OnPreRender(EventArgs e)
        {
            
            //RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
 
            //if (ajaxManager == null)
            //{
            //    RadAjaxManager manager = new RadAjaxManager();
            //    manager.ID = "RadAjaxManager1";
            //    Page.Items.Add(typeof(RadAjaxManager), manager);
            //    Page.Form.Controls.Add(manager);

            //}

            //base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //protected override void CreateChildControls()
        //{

        //    try
        //    {
        //        if ((!this.IsPostBack))
        //        {
        //            Initialize();
        //        }
        //        //else
        //        //{
        //        //    if (!Page.IsCallback)
        //        //    {
        //        //        //Initialize();
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLabel.Text = ex.Message + ex.StackTrace;
        //        Logger.WiteLog(ex.Message, ex.StackTrace);
        //    }
        //    base.CreateChildControls();
        //}

//        private void Initialize()
//        {
//            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
//            //this.PanelImgOrder.BackImageUrl = "/Style Library/Julia/img/" + strCultureName + "/bckground_pedido.png";

//            StyleBackImg.Text = @"<style>
//                                    .containerNewOrderMax {
//                                        background-image: url('/Style Library/Julia/img/" + strCultureName + @"/carrito.png');
//                                    }
//                                    </style>";
//            //Client clAux = (Client)Session["Client"];

//            ////client no seleccinoat
//            //if (clAux == null)
//            //{
//            //    this.Visible = false;
//            //}
//            //else
//            //{
//            //    this.Visible = true;
//            //}

//            try
//            {
//                //if (Session["User"] != null)
//                //{
//                //    this.currentUser = (User)Session["User"];
//                //}


//                //if (Session["ArticleDAO"] != null)
//                //{
//                //    this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
//                //}
              
//                //No usado
//                //if (Session["VentasDAO"] != null)
//                //{
//                //    this.ventaDAO = (VentaDataAccessObject)Session["VentasDAO"];

//                //}
                

//                //set the order to de user control                    
//                //((MainPedidoCartUserControl)this.mainPedidoCart).SetOrderData(this.order);

//                //if (Session["Order"] != null)
//                //{
//                //    this.order = (Order)Session["Order"];

//                //}
//                //No usado
//                //if (!String.IsNullOrEmpty(this.order.ReferenciaPedido))
//                //    this.orderTitle.Text = this.order.ReferenciaPedido;
//                //this.LoadPayMethods();
//                //this.LoadAddresses();
//                //this.LoadShipmentMethods();
//                this.SetLabels();


//            }
//            catch (Exception e)
//            {
//                ErrorLabel.Text = e.Message + e.StackTrace;
//                Logger.WiteLog(e.Message, e.StackTrace);
//            }
//        }

        //private void LoadPayMethods()
        //{
        //    try
        //    {
        //        if (Session["Client"] != null)
        //        {
        //            int i = 0;
        //            foreach (string[] paymethods in ((Client)Session["Client"]).PayMethods)
        //            {
        //                RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(paymethods[1], paymethods[0]);
        //                RadComboBoxItem aux2 = new Telerik.Web.UI.RadComboBoxItem(paymethods[1], paymethods[0]);

        //                // si es el que toca o es buit
        //                if (this.order.PaymentMethod == paymethods[0] || (String.IsNullOrEmpty(this.order.PaymentMethod) && i == 0))
        //                {
        //                    aux.Selected = true;
        //                    aux2.Selected = true;
        //                }

        //                this.PayMethodList.Items.Add(aux);

        //                i++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        //private void LoadAddresses()
        //{
        //    try
        //    {
        //        if (Session["Client"] != null)
        //        {
        //            int i = 0;
        //            foreach (string[] addresses in ((Client)Session["Client"]).Addresses)
        //            {
        //                RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);
        //                RadComboBoxItem aux2 = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);

        //                if (this.order.DeliveryAddress == addresses[0] || (String.IsNullOrEmpty(this.order.DeliveryAddress) && i == 0))
        //                {
        //                    aux.Selected = true;
        //                    aux2.Selected = true;
        //                }

        //                this.DestinationAddressList.Items.Add(aux);

        //                i++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        //private void LoadShipmentMethods()
        //{
        //    try
        //    {
        //        //load two values to show
        //        ListItem total = new ListItem(LanguageManager.GetLocalizedString("total"), "total");
        //        //per defecte
        //        total.Selected = true;
        //        ListItem parcial = new ListItem(LanguageManager.GetLocalizedString("partial"), "partial");

        //        if (this.order.DeliveryType == "total")
        //            total.Selected = true;
        //        else
        //            parcial.Selected = true;

        //        if (!((Client)Session["Client"]).PartialDelivery)
        //        {
        //            //si no entrega parcial marquem el total i deshabilitem pq no ho puguin tocar
        //            total.Selected = true;
        //            //RTP: Poso el Enabled a True pq ho puguin tocar
        //            //total.Enabled = false;
        //            //parcial.Enabled = false;

        //        }
        //        total.Enabled = true;
        //        parcial.Enabled = true;

        //        this.ShipmentMethod.Items.Add(total);
        //        this.ShipmentMethod.Items.Add(parcial);
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}
        //public void DeleteCard(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.order = (Order)Session["Order"];
        //        if (order != null)
        //        {
        //            this.currentUser = (User)Session["User"];
        //            this.order.DeleteAllProducts(currentUser.UserName);
        //            Session["Order"] = this.order;
        //            this.Refresh();
        //        }
        //    }
        //    catch (NavException ex)
        //    {
        //        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
            
        //}

      
        //public void PlaceOrder(object sender, EventArgs e)
        //{
        //    try
        //    {
               
        //        Response.Redirect("/Pages/AdvancedOrder.aspx");
              
                
        //    }
        //    catch (ThreadAbortException)
        //    {
        //        // Do nothing. ASP.NET is redirecting.
        //        // Always comment this so other developers know why the exception 
        //        // is being swallowed.
        //        //Capturamos la excepción que por diseño ASP.Net lanza para Response.End o para Response.Redirect
        //        //Otra alternativa es mover el redirect fuera del catch
        //        //http://stackoverflow.com/questions/4368640/response-redirect-and-thread-was-being-aborted-error
        //        //http://briancaos.wordpress.com/2010/11/30/response-redirect-throws-an-thread-was-being-aborted/
        //    }
        //    catch (NavException ex)
        //    {
        //        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        //private void SetLabels()
        //{
        //    try
        //    {
        //        this.lblLineTitleExp.Text = LanguageManager.GetLocalizedString("OrderReference");
        //        //this.lblLineTitleExp.Text = "Título de Pedido";
        //        this.lblLinePayMethodExp.Text = LanguageManager.GetLocalizedString("paymentMethod");
        //        this.lblLineDestAddressExp.Text = LanguageManager.GetLocalizedString("deliveryAddress");
        //        this.lblLineDestinationExp.Text = LanguageManager.GetLocalizedString("deliveryType");
        //        this.lblSelectedProductsTitle.Text = LanguageManager.GetLocalizedString("Cart");
        //        this.lblTotal.Text = LanguageManager.GetLocalizedString("total");
        //        this.SubmitOrderLiteral.Text = LanguageManager.GetLocalizedString("SumbitOrder");
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}

        private string getText(string text)
        {
            return JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(text);
        }

        public void Refresh()
        {

            //((MainPedidoCartUserControl)this.mainPedidoCart).Refresh();
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Refresh", "RefreshTable();");
        }
    }
}
