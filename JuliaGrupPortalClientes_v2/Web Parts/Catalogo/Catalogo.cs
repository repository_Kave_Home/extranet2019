﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace JuliaGrupPortalClientes_v2.Catalogo
{
    [ToolboxItemAttribute(false)]
    public class Catalogo : WebPart
    {
        #region Properties

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Nombre de la lista de Catálogo")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string catalogListName { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Subtitulo de la webpart")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string webPartSubTitle { get; set; }

        #endregion

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Catalogo/CatalogoUserControl.ascx";


        

        /// <summary>
        /// Method that loads the Web Part
        /// </summary>
        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(_ascxPath);
            Controls.Add(control);
        }

    }
}
