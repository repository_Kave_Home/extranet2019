﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogoUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Catalogo.CatalogoUserControl" %>
<%@ Register TagPrefix="julia" TagName="articlesCatalogUserControl" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/ArticlesCatalogUserControl.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<!--<link href="/Styles/Site.css" rel="stylesheet" type="text/css" />-->
<!-- link href="/Style%20Library/Julia/NewPedido2.css" rel="stylesheet" type="text/css" / -->
<script type="text/javascript">
    _spBodyOnLoadFunctionNames.push("SelectProductsMenuItem");
    function SelectProductsMenuItem() {
        $(".menu.horizontal.menu-horizontal ul li").removeClass("selected");
        $(".menu.horizontal.menu-horizontal ul li a").removeClass("selected");
        $(".menu.horizontal.menu-horizontal ul li:nth-child(2) a").addClass('selected');
    }   
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerArticlesCatalogUser" runat="server">
    <AjaxSettings> 
        <telerik:AjaxSetting AjaxControlID="catalogPanel">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="catalogPanel" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"
    EnableEmbeddedSkins="false" BackColor="White" Transparency="50">
        <img src="/Style Library/Julia/img/loading.gif" style="margin:0 auto;position:fixed;top:48%" />
</telerik:RadAjaxLoadingPanel>
<asp:Panel ID="Panel" runat="server" OnPreRender="catalogPanel_PreRender" Width="100%"
    CssClass="displaytable">
    <julia:articlesCatalogUserControl ID="articlesCatalogUserControl" runat="server">
    </julia:articlesCatalogUserControl>
</asp:Panel>
