﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using Telerik.Web.UI;
using System.Diagnostics;

namespace JuliaGrupPortalClientes_v2.Catalogo
{
    public partial class CatalogoUserControl : UserControl
    {
        private ArticleDataAccessObject articleDAO;
        public Order order;

        protected override void CreateChildControls()
        {
            try
            {
                if (!this.Page.IsPostBack)
                {
                    this.SetCatalogProducts(Request.QueryString["catalog"]);
                }
            }
            catch (Exception ex)
            {
                Logger.WiteLog(ex.Message, ex.StackTrace);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //EnsureChildControls();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        private void SetCatalogProducts(string catalogNo)
        {
            try
            {
                if (articleDAO == null && Session["ArticleDAO"] != null)
                {
                    this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];

                    List<Article> articles;
                    string catalogCode;

                    if (string.IsNullOrEmpty(catalogNo))
                    {
                        articles = this.articleDAO.GetCatalogProducts();
                        catalogCode = "AllCatalogs";
                    }
                    else
                    {
                        articles = this.articleDAO.GetCatalogProducts(catalogNo);
                        catalogCode = catalogNo;
                    }
                    Session["Articles"] = articles;

                    string username = ((User)Session["User"]).UserName;
                    Cache.Remove("Articles_" + username);
                    Cache.Add("Articles_" + username, articles, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    //Session["CatalogCode"] = catalogCode;    //No utilizado
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void catalogPanel_PreRender(object sender, EventArgs e)
        {
            try
            {
                RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
                manager.AjaxSettings.AddAjaxSetting(Panel, Panel, RadAjaxLoadingPanel1);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
