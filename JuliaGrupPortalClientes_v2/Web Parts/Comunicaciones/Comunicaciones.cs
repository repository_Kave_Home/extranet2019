﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;

namespace JuliaGrupPortalClientes_v2.Comunicaciones
{
    [ToolboxItemAttribute(false)]
    public class Comunicaciones : WebPart
    {
        #region Properties

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Pagina de comunicaciones")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string communicationsPage { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Dirección de Twitter")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string twitterURL { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Dirección de Facebook")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string facebookURL { get; set; }

        #endregion

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Comunicaciones/ComunicacionesUserControl.ascx";

         #region Constructor

        public Comunicaciones()
        {
            // Set default List Name
            this.communicationsPage = "/Pages/Communications.aspx";
            this.twitterURL = "http://www.twitter.com";
            this.facebookURL = "http://www.facebook.com";

            // Set default chrome state
            this.ChromeType = PartChromeType.None;
            this.ChromeState = PartChromeState.Normal;
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            try
            {
                ComunicacionesUserControl control = (Page.LoadControl(_ascxPath) as ComunicacionesUserControl);

                // Set Web Part parameters
                control.commPage = this.communicationsPage;
                control.urlTwitter = this.twitterURL;
                control.urlFacebook = this.facebookURL;
                control.wpTitle = LanguageManager.GetLocalizedString("CommunicationsWebPartTitle");

                this.Controls.Add(control);
            }
            catch (Exception ex)
            {
                this.Controls.Add(new LiteralControl("Error building the web part: " + ex.Message));
            }
        }

        #endregion
    }
}
