﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComunicacionesUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Comunicaciones.ComunicacionesUserControl" %>

<!--<link href="/Styles/Site.css" rel="stylesheet" type="text/css" />-->
<!-- link href="/Style%20Library/Julia/Comunicaciones.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationComunicaciones" name="/Style Library/Julia/Comunicaciones.css" runat="server"/>

<div class="containerCommunications">

    <div class="headerCommunications">
        <div class="txtCommunicationsTitle"><asp:Label id="lblTitleWebPart" runat="server"/></div>       
        <div class="imgTwitt_Fb">
            <asp:HyperLink id="linkToTwitter" runat="server">
                <div class="imgTwitter"></div>
            </asp:HyperLink>
            <asp:HyperLink id="linkToFacebook" runat="server">
                <div class="imgFb"></div>
            </asp:HyperLink>   
        </div>    
    </div>  
            
    <div class="listCommunications">

        <div id="c1" runat="server">
            <div class="itemCommunication">
                <asp:HyperLink id="linkToComm1" runat="server">
                    <div class="txtCommunicationCaption"><b><asp:Label id="lblCommTitle1" runat="server"/></b> - <asp:Label id="lblCommDesc1" runat="server"/></div>
                </asp:HyperLink>
                <div class="txtCommunicationDate"><asp:Label id="lblCommDate1" runat="server"/></div>
            </div>
        </div>

        <div id="c2" runat="server">
            <div class="itemCommunication">
                <asp:HyperLink id="linkToComm2" runat="server">
                    <div class="txtCommunicationCaption"><b><asp:Label id="lblCommTitle2" runat="server"/></b> - <asp:Label id="lblCommDesc2" runat="server"/></div>
                </asp:HyperLink>
                <div id="txtCommunicationDate"><asp:Label id="lblCommDate2" runat="server"/></div>
            </div>
        </div>
    </div>

    <asp:HyperLink id="linkToList" runat="server">
        <div class="linkCommunications">
            <asp:Label id="lblCommunicationsPage" runat="server" />
            <asp:Label ID="lblCommunicationsCaption" CssClass="linkCommunicationsCaption" runat="server" />
        </div>
    </asp:HyperLink>
</div>