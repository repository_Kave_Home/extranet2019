﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;

namespace JuliaGrupPortalClientes_v2.Comunicaciones
{
    public partial class ComunicacionesUserControl : UserControl
    {
        #region Properties

        public string wpTitle { get; set; }
        public string commList { get; set; }
        public string commTitle { get; set; }
        public string commDescription { get; set; }
        public string commAssignedTo { get; set; }
        public string commDate { get; set; }
        public string commURL { get; set; }
        public string commPage { get; set; }
        public string urlTwitter { get; set; }
        public string urlFacebook { get; set; }

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblTitleWebPart.Text = this.wpTitle;

                CommunicationDataAccessObject communicationsDAO = new CommunicationDataAccessObject();

                List<Communication> communications = communicationsDAO.GetCommunications();

                this.linkToList.NavigateUrl = commPage;
                this.linkToTwitter.NavigateUrl = urlTwitter;
                this.linkToFacebook.NavigateUrl = urlFacebook;
                this.lblCommunicationsPage.Text = "View all ";
                this.lblCommunicationsCaption.Text = "communications";

                #region ADD DATA


                this.c1.Visible = false;
                this.c2.Visible = false;

                if (communications.Count < 1) return;

                this.c1.Visible = true;
                
                // Set title
                if (!string.IsNullOrEmpty(communications[0].Title))
                    this.lblCommTitle1.Text = communications[0].Title;
                
                // Set description
                if (!string.IsNullOrEmpty(communications[0].Subtitle))
                    this.lblCommDesc1.Text = communications[0].Subtitle;

                // Set date
                if (!string.IsNullOrEmpty(communications[0].Date))
                    this.lblCommDate1.Text = communications[0].Date;

                // Set link
                if (!string.IsNullOrEmpty(communications[0].Url))
                    this.linkToComm1.NavigateUrl = communications[0].Url;

                if (communications.Count < 2) return;

                this.c2.Visible = true;

                // Set title
                if (!string.IsNullOrEmpty(communications[1].Title))
                    this.lblCommTitle2.Text = communications[1].Title;

                // Set description
                if (!string.IsNullOrEmpty(communications[1].Subtitle))
                    this.lblCommDesc2.Text = communications[1].Subtitle;

                // Set date
                if (!string.IsNullOrEmpty(communications[1].Date))
                    this.lblCommDate2.Text = communications[1].Date;

                // Set link
                if (!string.IsNullOrEmpty(communications[1].Url))
                    this.linkToComm2.NavigateUrl = communications[1].Url;

                #endregion

            }
            catch (Exception ex)
            {
                this.Controls.Add(new LiteralControl("Error in User Control" + ex.Message));
            }
        }

        #endregion
    }
}
