﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactoUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.Contacto.ContactoUserControl" %>

<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>

<style type="text/css">
.Agent_contact_container
{
width:100%;
margin:0px auto;
}
.Tittle_Contact
{
    color:Black;
    font-size:12px;
   
    font-weight:bold;
    margin-left:5px;
    margin-bottom:10px;
    }
.TimeSheet
{
    padding-bottom:10px;
    margin-left:5px;
}
.table-2 
{
    margin:0px auto;
    text-align:left;
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
    width: 60%;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
}

.subtittle
{
    font-weight:bold;
    background-color:#C2C2C2;
    
    }
.table-2 th 
{
    text-align:center;
    }
  
.table-2 td, .table-2 th {
	
	color: #333;
}
.table-2 thead,{
	/*font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;*/
	font-family: "Segoe UI";
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #c8c8c8;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#e3e3e3), color-stop(.6,#B3B3B3));
	background-image: -moz-linear-gradient(top, #D6D6D6, #B0B0B0, #B3B3B3 90%);
	border-bottom: solid 1px #999;
}
.table-2 th {
	/*font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;*/
	font-family: "Segoe UI";
	font-size: 12px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: left;
	text-shadow: white 1px 1px 1px;
}
.table-2 td {
	line-height: 20px;
	/*font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;*/
	font-family: "Segoe UI";
	font-size: 11px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
.table-2 tr:hover {
	background-color: #fff;
}
</style>
  <div class="Tittle_Contact">
    <asp:Label ID="titleCatalog" runat="server"></asp:Label>
  </div>
<div class="TimeSheet">
   <%--<asp:Label ID="Lbl_Contact_Comercial_Zgz" runat="server"></asp:Label>--%>
   <asp:Label ID="LabeLbl_Contact_ZaragozaNorte" runat="server"></asp:Label>
  </div>
   
<asp:Panel ID="ContactClient" CssClass="contact_client" runat="server">
  <table class="table-2">
    <thead class="Header_table">
  	  <th><asp:Label ID="Lbl_Contact_Area" runat="server"></asp:Label></th>
	  <th><asp:Label ID="Lbl_Contact_Personal" runat="server"></asp:Label></th>
	  <th>E-MAIL</th>
	  <th>EXT</th>
	</thead>
	<tr class="subtittle">
	  <td><asp:Label ID="Lbl_Contact_Comercial_Area" runat="server"></asp:Label></td>
	  <td>+34 972 853 628</td>
	  <td></td>
	  <td></td>
	</tr>
	<tr>
      <td><asp:Label ID="Lbl_Contact_Comercial_Tgn" runat="server"></asp:Label></td>
	  <td>Aroa Porcel</td>
	  <td><a href="mailto:comercial1@juliagrup.com">comercial1@juliagrup.com</a></td>
	  <td>204</td>
	</tr>
	<tr>
	  <td><asp:Label ID="Lbl_Contact_Comercial_LLeida" runat="server"></asp:Label></td>
	  <td>Nati González</td>
	  <td><a href="mailto:comercial2@juliagrup.com">comercial2@juliagrup.com</a></td>
	  <td>205</td>
	</tr>
	<tr>
      <td><asp:Label ID="Label115" runat="server" Text="a"/></td>
      <td>Ester Pons</td>
      <td><a href="mailto:comercial3@juliagrup.com">comercial3@juliagrup.com</a></td>
      <td>203</td>
    </tr>
	<--!<tr>
      <td><asp:Label ID="Lbl_Contact_Comercial_Area_Bcn" runat="server" /></td>
      <td>Ester Pons</td>
      <td><a href="mailto:comercial3@juliagrup.com">comercial3@juliagrup.com</a></td>
      <td>203</td>
    </tr> -->
	<tr>
	  <td><asp:Label ID="Lbl_Contact_Comercial_Andalucia" runat="server"></asp:Label></td>
	  <td>Eva Sotoca</td>
	  <td><a href="mailto:comercial7@juliagrup.com">comercial7@juliagrup.com</a></td>
	  <td>213</td>
	</tr>
	<tr>
	  <td><asp:Label ID="Lbl_Contact_Comercial_Levante" runat="server"></asp:Label>
	  </td>
	  <td>Carmen Joals</td>
	  <td><a href="mailto:export2@juliagrup.com" >export2@juliagrup.com </a></td>
	  <td>265</td>
	</tr>
	<tr>
	  <td><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LabelLbl_Contact_Italia")%></td>
	  <td>Marco Romaniello</td>
	  <td><a href="mailto:export3@juliagrup.com">export3@juliagrup.com</a></td>
	  <td>263</td>
	</tr>
	<tr>
	  <%--<td><asp:Label ID="Lbl_Contact_Comercial_Zgz" runat="server"></asp:Label></td>--%>
	  <td>Diana Boancas</td>
	  <td><a href="mailto:export1@juliagrup.com">export1@juliagrup.com</a></td>
	  <td>206</td>
	</tr>
	<tr>
	  <td><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LabelLbl_Contact_Italia")%></td>
	  <td>Elena Maslova</td>
	  <td><a href="mailto:export4@juliagrup.com">export4@juliagrup.com</a></td>
	  <td>211</td>
	</tr>
	<tr>
	  <td><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LabelLbl_Contact_Italia")%></td>
	  <td>Sara D'Agostini</td>
	  <td><a href="mailto:export5@juliagrup.com">export5@juliagrup.com</a></td>
	  <td>214</td>
	</tr>
	<!--
	<tr class="subtittle">
   	  <td><asp:Label ID="Lbl_Contact_Postventa_May" runat="server"></asp:Label></td>
	  <td>972 853 628</td>
	  <td><asp:Label ID="Lbl_Contact_Movil_Envio" runat="server"></asp:Label></td>
	  <td></td>
	</tr>
	<tr>
	  <td><asp:Label ID="Lbl_Contact_Postventa_Min_3" runat="server"></asp:Label></td>
	  <td>Silvia Deulofeu</td>
	  <td><a href="mailto:postvenda1@juliagrup.com">postvenda1@juliagrup.com</a></td>
	  <td>209</td>
	</tr>
	<tr>
	  <td><asp:Label ID="Lbl_Contact_Postventa_Min_4" runat="server"></asp:Label></td>
	  <td>Ester Pons</td>
	  <td><a href="mailto:postvenda2@juliagrup.com">postvenda2@juliagrup.com</a></td>
	  <td>222</td>
	</tr> -->
  </table>  
</asp:Panel>
<asp:Panel CssClass="Agent_contact_container" runat="server" ID="Agent_Contact">
<table class="table-2">
	  
		<!-- Results table headers -->
			
	  <thead class="Header_table">
		    <th><asp:Label ID="Lbl_Contact_Area2" runat="server"></asp:Label></th>
		    <th><asp:Label ID="Lbl_Contact_Personal2" runat="server"></asp:Label></th>
		    <th>E-MAIL</th>
		    <th>EXT</th>
  	  </thead>			
	  <tbody>
		<tr class="subtittle">
		  <td><asp:Label ID="Lbl_Contact_Contabilidad" runat="server"></asp:Label></td>
		  <td>+34 972 853 951</td>
		  <td>E-MAIL</td>
		  <td></td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_DirContabilidad" runat="server"></asp:Label> </td>
		  <td>Núria Bolívar</td>
		  <td><a href="mailto:nbolivar@juliagrup.com">nbolivar@juliagrup.com</a></td>
		  <td>(5) 243</td>
		</tr>
		<tr>
		<td></td>
		  <td>Bárbara Sala</td>
		  <td><a href="mailto:compta1@juliagrup.com">compta1@juliagrup.com</a></td>
		  <td>(5) 260</td>
		</tr>
		<tr>
		  <td></td>
		  <td>Jacob Roger</td>
		  <td><a href="mailto:compta3@juliagrup.com">compta3@juliagrup.com</a></td>
		  <td>(5) 245</td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_Gestion_Clientes" runat="server"></asp:Label></td>
		  <td>Mònica Cívico</td>
		  <td><a href="mailto:compta4@juliagrup.com">compta4@juliagrup.com</a></td>
		  <td>(5) 231</td>
		</tr>
		<tr>
		  <td></td>
		  <td>Delfina Vidal</td>
		  <td><a href="mailto:compta5@juliagrup.com">compta5@juliagrup.com</a></td>
		  <td>(5) 271</td>
		</tr>
		<tr>
		  <td></td>
		  <td>Roger Guillamon</td>
		  <td><a href="mailto:compta6@juliagrup.com">compta6@juliagrup.com</a></td>
		  <td>(5) 201</td>
		</tr>
		<tr>
		  <td></td>
		  <td>Carlos Saenz</td>
		  <td><a href="mailto:compta7@juliagrup.com">compta7@juliagrup.com</a></td>
		  <td>(5) 290</td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_Control_Riesgos" runat="server"></asp:Label></td>
		  <td>Neus Farré</td>
		  <td><a href="mailto:compta9@juliagrup.com">compta9@juliagrup.com</a></td>
		  <td>(5) 296</td>
		</tr>
        <tr>
		  <td><asp:Label ID="Label1" runat="server"></asp:Label></td>
		  <td>Aura Casaponsa</td>
		  <td><a href="mailto:compta10@juliagrup.com">compta10@juliagrup.com</a></td>
		  <td>(5) 210</td>
		</tr>
		
		
          <!--<tr>
		  <td>Example1</td>
		  <td><a href="mailto:compta2@juliagrup.com">compta2@juliagrup.com</a><br><a href="mailto:compta5@juliagrup.com">compta5@juliagrup.com</a></td>
		</tr>	-->
		
		<!--<tr>
		  <td><asp:Label ID="Lbl_Contact_Contabilidad_general" runat="server"></asp:Label></td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>-->
		<tr class="subtittle">
		  <td><asp:Label ID="Lbl_Contact_OficinaComercial" runat="server"></asp:Label> </td>
		  <td>+34 972 853 628</td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_ComercialyLogistica" runat="server"></asp:Label></td>
		  <td>Delphine Burgunder</td>
		  <td><a href="mailto:dburgunder@juliagrup.com">dburgunder@juliagrup.com</a></td>
		  <td>(5) 225</td>
		</tr>		
		<tr>
		  <td><asp:Label ID="LabeLbl_Contact_OfComercial_Tgn_BLLobregat" runat="server"></asp:Label></td>
		   <td>Atención al cliente – España y Portugal</td>
          <td><a href="mailto:clientes@juliagrup.com">clientes@juliagrup.com</a></td>
          <td>(5) 204, 213, 205, 233</td>
		</tr>
		<tr>
		  <td><asp:Label ID="LabeLbl_Contact_OfComercial_LleidaValles" runat="server"></asp:Label></td>
		  <td>Customer service – Export sales</td>
          <td><a href="mailto:customers@juliagrup.com">customers@juliagrup.com</a></td>
          <td>(5) 211, 214, 265</td>
		</tr>
		<!--<tr>
		  <td><asp:Label ID="LabeLbl_Contact_OfComercial" runat="server"></asp:Label></td>
		  <td>Ester Pons</td>
		  <td><a href="mailto:comercial3@juliagrup.com">comercial3@juliagrup.com</a></td>
		  <td>(5) 203</td>
		</tr> -->
		<tr>
		  <td><asp:Label ID="LabeLbl_Contact_AndaluciaCentro" runat="server"></asp:Label></td>
		  <td>Kundenbetreuung – Germany and Austria</td>
          <td><a href="mailto:kunde@juliagrup.com">kunde@juliagrup.com</a></td>
          <td>(5) 265, 233, 211, 206</td>
		</tr>
		<%--<tr>
		  <td><asp:Label ID="LabeLbl_Contact_ZaragozaNorte" runat="server"></asp:Label></td>
		  <td></td>
          <td>Customer service</td>
          <td><a href="mailto:licensing@juliagrup.com">licensing@juliagrup.com</a></td>
          <td>(5) 272, 258</td>
		</tr> --%>
		<tr>
		  <td><asp:Label ID="LabeLbl_Contact_Levante" runat="server"></asp:Label></td>
		 <td>Service clientèle - France</td>
         <td><a href="mailto:client@juliagrup.com">client@juliagrup.com</a></td>
         <td>(5) 214, 206, 203</td>
		</tr>
		<tr>
		  <td><asp:Label ID="LabelLbl_Contact_Italia" runat="server"></asp:Label></td>
		  <td>Servizio clienti – Italy</td>
          <td><a href="mailto:Clienti@juliagrup.com">clienti@juliagrup.com</a></td>
          <td>(5) 263, 283, 206, 214</td>
		</tr>
	
		<!--<tr>
		  <td><asp:Label ID="Lbl_Contact_DeudasyComisiones" runat="server"></asp:Label></td>
		  <td>Elena Maslova</td>
		  <td><a href="mailto:export4@juliagrup.com">export4@juliagrup.com</a></td>
		  <td>(5) 211</td>
		</tr>-->
		<!--<tr>
	   	  <td><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LabelLbl_Contact_Italia")%></td>
	   	  <td>Sara D'Agostini</td>
	   	  <td><a href="mailto:export5@juliagrup.com">export5@juliagrup.com</a></td>
	   	  <td>(5) 214</td>
	  	</tr>		-->
		<!--<tr>
	   	  <td></td>
	   	  <td>Chunhai Li</td>
	   	  <td><a href="mailto:exportasia@juliagrup.com">exportasia@juliagrup.com</a></td>
	   	  <td>(5) 233</td>
	  	</tr>-->
		<tr class="subtittle">
		  <td><asp:Label ID="Lbl_Contact_PostVenta" runat="server"></asp:Label></td>
		  <td>+34 972 853 628</td>
		  <td><asp:Label ID="Lbl_Contact_MobilEnvio" runat="server"></asp:Label></td>
		  <td></td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_DirPostVenta" runat="server"></asp:Label></td>
		  <td>Ovidi Colomer</td>
		  <td><a href="mailto:ocolomer@juliagrup.com">ocolomer@juliagrup.com</a></td>
		  <td>(5) 268</td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_PostVenta_Min1" runat="server"></asp:Label></td>
		  <td>Silvia Deulofeu</td>
		  <td><a href="mailto:postvenda1@juliagrup.com">postvenda1@juliagrup.com</a></td>
		  <td>(5) 209</td>
		</tr>			
		<tr>
		  <td><asp:Label ID="Lbl_Contact_PostVenta_Min2" runat="server" ></asp:Label></td>
		  <td>Ester Pons</td>
		  <td><a href="mailto:postvenda2@juliagrup.com">postvenda2@juliagrup.com</a></td>
		  <td>(5) 222</td>
		</tr>	
		<tr>
		  <td><asp:Label ID="Lbl_Contact_PostVenta_Min3" runat="server" ></asp:Label></td>
		  <td>Olga Tudela</td>
		  <td><a href="mailto:postvenda3@juliagrup.com">postvenda3@juliagrup.com</a></td>
		  <td>(5) 262</td>
		</tr>
		<tr>
		  <td><asp:Label ID="Lbl_Contact_PostVenta_Min4" runat="server" ></asp:Label></td>
		  <td>Silvia Soler</td>
		  <td><a href="mailto:postvenda4@juliagrup.com">postvenda4@juliagrup.com</a></td>
		  <td>(5) 280</td>
		</tr>
		
		<tr class="subtittle">
		  <td><asp:Label ID="Lbl_Contact_Marketing" runat="server"></asp:Label></td>
		  <td>+34 972 853 628</td>
		  <td></td>
		  <td></td>
		</tr>	
		<tr>
		  <td><asp:Label ID="Lbl_Contact_Marketing_resp" runat="server"></asp:Label></td>
		  <td>Victor Font</td>
		  <td><a href="mailto:vfont@juliagrup.com">vfont@juliagrup.com</a></td>
		  <td>(5) 235</td>
		</tr>
		<tr>
		  <td></td>
		  <td>Nerea Molina</td>
		  <td><a href="mailto:nmolina@juliagrup.com">nmolina@juliagrup.com</a></td>
		  <td>(5) 254</td>
		</tr>
	  </tbody>
</table>
</asp:Panel>
