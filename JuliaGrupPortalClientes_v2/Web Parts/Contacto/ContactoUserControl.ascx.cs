﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Data;
using System.Linq;
using Microsoft.SharePoint;
using System.Diagnostics;
namespace JuliaGrupPortalClientes_v2.Web_Parts.Contacto
{
    public partial class ContactoUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
           
            checkUserType();

            
        }

        private void checkUserType()
        {
            User currentUser;
            currentUser = (User)Session["User"];

            if (currentUser is Agent)       
            {
                Inicialize("Agent");
                this.Agent_Contact.Visible = true;
                this.ContactClient.Visible = false;
            }
            else if (currentUser is ClientGroup)
            {
                Inicialize("Agent");
                this.Agent_Contact.Visible = true;
                this.ContactClient.Visible = false;
            }
            else
            {
                Inicialize("Client");
                this.Agent_Contact.Visible = false;
                this.ContactClient.Visible = true;
            }
        }

        private void Inicialize(string p)
        {
            this.titleCatalog.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("titleCatalog");
            if (p == "Client")
            {
                   this.Lbl_Contact_Area.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Area");
                   this.Lbl_Contact_Personal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Personal");
                   this.Lbl_Contact_Comercial_Area.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OfComercialTit");
                   this.Lbl_Contact_Comercial_Area_Bcn.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OfComercialBarna");
                   this.Lbl_Contact_Comercial_Tgn.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Tgn");
                   this.Lbl_Contact_Comercial_LLeida.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_LLeida");
                   this.Lbl_Contact_Comercial_Andalucia.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Andalucia");
                   //this.Lbl_Contact_Comercial_Zgz.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Zgz");
                   this.Lbl_Contact_Comercial_Levante.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Levante");
                   this.Lbl_Contact_Postventa_May.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa");
                   this.Lbl_Contact_Postventa_Min_3.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa_Min");
                   this.Lbl_Contact_Postventa_Min_4.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa_Min");
            }else if(p == "Agent")
            {
                    this.Lbl_Contact_Area2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Area");
                    this.Lbl_Contact_Personal2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Personal");
                    this.Lbl_Contact_Contabilidad.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Contabilidad");
                    this.Lbl_Contact_DirContabilidad.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_DirContabilidad");
                    this.Lbl_Contact_DeudasyComisiones.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_DeudasyComisiones");
                    this.Lbl_Contact_Control_Riesgos.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Control_Riesgos");
                    this.Lbl_Contact_Gestion_Clientes.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Gestion_Clientes");
                    this.Lbl_Contact_Contabilidad_general.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Contabilidad_general");
                    this.Lbl_Contact_OficinaComercial.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OfComercialTit");
                    this.Lbl_Contact_ComercialyLogistica.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_ComercialyLogistica");
                    this.LabeLbl_Contact_OfComercial.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OfComercialBarna");
                    this.LabeLbl_Contact_OfComercial_Tgn_BLLobregat.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Tgn");
                    this.LabeLbl_Contact_OfComercial_LleidaValles.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_LLeida");
                    this.LabeLbl_Contact_AndaluciaCentro.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Andalucia");
                    this.LabeLbl_Contact_ZaragozaNorte.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Zgz");
                    this.LabeLbl_Contact_Levante.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Comercial_Levante");
                    this.LabelLbl_Contact_Italia.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LabelLbl_Contact_Italia");
                    this.Lbl_Contact_PostVenta.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa");
                    this.Lbl_Contact_MobilEnvio.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_MobilEnvio");
                    this.Lbl_Contact_DirPostVenta.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_DirPostVenta");
                    this.Lbl_Contact_PostVenta_Min1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa_Min");
                    this.Lbl_Contact_PostVenta_Min2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Postventa_Min");
                    this.Lbl_Contact_Marketing.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Marketing");
                    this.Lbl_Contact_Marketing_resp.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Lbl_Contact_Marketing_resp");


            }
        }

     
    }
}
