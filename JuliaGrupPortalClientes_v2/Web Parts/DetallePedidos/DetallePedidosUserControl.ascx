﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetallePedidosUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.DetallePedidos.DetallePedidosUserControl" %>

<!--link href="/Style%20Library/Julia/DetallePedidos.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationDetallePedidos" name="/Style Library/Julia/DetallePedidos.css" runat="server"/>

<script type="text/javascript" src="/Style%20Library/Julia/slides.min.jquery.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".capaEsconderMostrarFacturas").click(function () {
            if ($(".classLeftPedidos").is(":visible")) {
                $(".classLeftPedidos").hide("slide", { direction: "left" }, 1000);
                $('.capaEsconderMostrarFacturas').css("background-image", "url(/Style%20Library/Julia/img/SlideRight11.png)");
                $('.classRightPedidos').css('visibility', 'visible');
            }

            else {
                $(".classLeftPedidos").show("slide", { direction: "left" }, 1000);
                $('.capaEsconderMostrarFacturas').css("background-image", "url(/Style%20Library/Julia/img/SlideLeft11.png)");
                $('.classRightPedidos').css('visibility', 'hidden');

            }
        });


        $(function () {
            // Set starting slide to 1
            var startSlide = 1;
            // Get slide number if it exists
            if (window.location.hash) {
                startSlide = window.location.hash.replace('#', '');
            }
            // Initialize Slides
            $('#slides').slides({
                preload: true,
                preloadImage: '/Style%20Library/Julia/img/loading.gif',
                generatePagination: true,
                play: 5000,
                pause: 2500,
                hoverPause: true,
                // Get the starting slide
                start: startSlide,
                animationComplete: function (current) {
                    // Set the slide number as a hash
                    window.location.hash = '#' + current;
                }
            });
        });

    });
                

    
</script>
    
    <div class="webpartDetallePedidos">
        <span id="txtTitleDetallePedidos">Detalle Pedidos</span>

        <div class="capaEsconderMostrarFacturas"></div>
            
        <div class="classLeftPedidos">
            <div class="classTitlesFacturas">
                <span id="txtTitle_NumPedido">Pedido</span>
                <span id="txtTitle_ReferenciaCliente">Referencia Cliente</span>
                <span id="txtTitle_FechaPedido">Fecha Pedido</span>
                <span id="txtTitle_Importe">Importe</span>
                <span id="txtTitle_Estado">Estado</span>
                <span id="txtTitle_DescargarFactura">Descargar Factura</span>
            </div>

            <div class="classLineaPedido1">
                    <span class="txtNumPedido">0544</span>
                    <span class="txtReferenciaCliente">Kibuc BCN</span>
                    <span class="txtFechaDetalleFactura">14.09.2011</span>
                    <span class="txtImporte">324 €</span>
                    <span class="txtEstadoEntregado">Entregado</span>
                    <div class="imgDescargarFacturaPDF"></div>
            </div>

            <div class="classLineaPedido2">
                    <span class="txtNumPedido">0545</span>
                    <span class="txtReferenciaCliente">Kibuc BCN</span>
                    <span class="txtFechaDetalleFactura">25.09.2011</span>
                    <span class="txtImporte">775 €</span>
                    <span class="txtEstadoEntregado">Entregado</span>
                    <div class="imgDescargarFacturaPDF"></div>
            </div>

            <div class="classLineaPedido3">
                    <span class="txtNumPedido">0546</span>
                    <span class="txtReferenciaCliente">Sants Mobles</span>
                    <span class="txtFechaDetalleFactura">14.10.2011</span>
                    <span class="txtImporte">1399 €</span>
                    <span class="txtEstadoPendienteDetalleFacturas">Pendiente</span>
                    <div class="imgDescargarFacturaPDF"></div>
            </div>

            <div class="classLineaPedido4">
                    <span class="txtNumPedido">0875</span>
                    <span class="txtReferenciaCliente">LaFabrica</span>
                    <span class="txtFechaDetalleFactura">15.10.2011</span>
                    <span class="txtImporte">2690 €</span>
                    <span class="txtEstadoEntregado">Entregado</span>
                    <div class="imgDescargarFacturaPDF"></div>
            </div>

            <div class="classLineaPedido5">
                    <span class="txtNumPedido">0877</span>
                    <span class="txtReferenciaCliente">LaFabrica</span>
                    <span class="txtFechaDetalleFactura">20.10.2011</span>
                    <span class="txtImporte">4100 €</span>
                    <span class="txtEstadoPendienteDetalleFacturas">Pendiente</span>
                    <div class="imgDescargarFacturaPDF"></div>
            </div>
        </div>

        <div class="classRightPedidos">
            <!--<div class="prov" style="foat:left;clear:left;border:1px solid green;width:1px">a</div>-->
            <div class="classTitlesProductos">
                <span id="txtTitleFactura">Factura</span>
                <span id="txtTitleRefArt">Ref. Art.</span>
                <span id="txtTitleDescripcion">Descripción</span>
                <span id="txtTitlePV">P.V. Unidad</span>
                <span id="txtTitleUds">Uds.</span>
                <span id="txtTitleImporte">Importe</span>
            </div>

            <div class="classLineaArticulo1">
                    <span class="txtNumFactura">1323</span>
                    <span class="txtReferenciaArticulo">AE-1285-R</span>
                    <span class="txtDescripcion">Silla Modelo AE beige</span>
                    <span class="txtPVUnit">50 €</span>
                    <span class="txtUds">4</span>
                    <span class="txtImporteProducto">200 €</span>
            </div>

            <div class="classLineaArticulo2">
                    <span class="txtNumFactura">1324</span>
                    <span class="txtReferenciaArticulo">PLN-112</span>
                    <span class="txtDescripcion">Silla Plegable Negra</span>
                    <span class="txtPVUnit">35 €</span>
                    <span class="txtUds">2</span>
                    <span class="txtImporteProducto">70 €</span>
            </div>

                <div class="classLineaArticulo3">
                    <span class="txtNumFactura">1325</span>
                    <span class="txtReferenciaArticulo">ME cc 210x80</span>
                    <span class="txtDescripcion">Mesa escritorio caoba</span>
                    <span class="txtPVUnit">85 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">85 €</span>
            </div>

                <div class="classLineaArticulo4">
                    <span class="txtNumFactura">1326</span>
                    <span class="txtReferenciaArticulo">AC-01212</span>
                    <span class="txtDescripcion">Armario Comedor</span>
                    <span class="txtPVUnit">185 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">185 €</span>
            </div>

            <div class="classLineaArticulo5">
                    <span class="txtNumFactura">1350</span>
                    <span class="txtReferenciaArticulo">LzS0011</span>
                    <span class="txtDescripcion">Luz Salón</span>
                    <span class="txtPVUnit">68 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">68 €</span>
            </div>

            <div class="classLineaArticulo6">
                    <span class="txtNumFactura">1365</span>
                    <span class="txtReferenciaArticulo">AC-01212</span>
                    <span class="txtDescripcion">Puerta 200x90</span>
                    <span class="txtPVUnit">205 €</span>
                    <span class="txtUds">2</span>
                    <span class="txtImporteProducto">410 €</span>
            </div>

            <div class="classLineaArticulo7">
                    <span class="txtNumFactura">1385</span>
                    <span class="txtReferenciaArticulo">JEM 0123</span>
                    <span class="txtDescripcion">Juego Estanterías Mirrov</span>
                    <span class="txtPVUnit">122 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">122 €</span>
            </div>

            <div class="classLineaArticulo8">
                    <span class="txtNumFactura">1386</span>
                    <span class="txtReferenciaArticulo">SE.234</span>
                    <span class="txtDescripcion">Sofá Extendible</span>
                    <span class="txtPVUnit">432 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">432 €</span>
            </div>

            <div class="classLineaArticulo9">
                    <span class="txtNumFactura">1387</span>
                    <span class="txtReferenciaArticulo">AC-01212</span>
                    <span class="txtDescripcion">Litera pnt 02</span>
                    <span class="txtPVUnit">185 €</span>
                    <span class="txtUds">1</span>
                    <span class="txtImporteProducto">185 €</span>
            </div>

            <div class="imgProductoFactura">
                <span id="titleProdImg">Sofá extendible</span>
                <span id="titleRefImg">SE.234</span>

            </div>
        </div>  
    </div>  
            
        <!--
        <div class="classBottomIndicadoresPedido">
            <div class="classDatosPedido">
                <span id="titleDatosPedido">Datos Pedido</span>
                <div id="imgDatosPedido"></div>

                <div class="classDatosPedidoLineas">
                    <div class="classLineaDatosPedido1">
                        <span id="txtTitleDatosPedido_NumPedido">Nº Pedido:</span>
                        <span id="txtDatosPedido_NumPedido">1255</span>
                    </div>

                    <div class="classLineaDatosPedido2">
                        <span id="txtTitleDatosPedido_EstadoPedido">Estado Pedido:</span>
                        <span id="txtDatosPedido_EstadoPedido">Entregado</span>
                    </div>

                    <div class="classLineaDatosPedido3">
                        <span id="txtTitleDatosPedido_RefCliente">Ref. Cliente:</span>
                        <span id="txtDatosPedido_RefCliente">Kibuc BCN</span>
                    </div>

                    <div class="classLineaDatosPedido4">
                        <span id="txtTitleDatosPedido_FechaPedido">F. Pedido</span>
                        <span id="txtDatosPedido_FechaPedido">04.10.11</span>
                    </div>
                </div>
            </div>

            <div class="classDatosEnvio">
                <span id="titleDatosEnvio">Datos de Envío</span>
                <div id="imgDatosEnvio"></div>
                <div class="classDatosEnvioLineas">
                    <div class="Webpart_DatosEnvioLinea1">
                        <span id="txtTitleDE_Destino">Destino:</span>
                        <span id="txtDE_Destino">Julià Grup Furnitures Solutions SL</span>
                    </div>

                    <div class="Webpart_DatosEnvioLinea2">
                        <span id="txtTitleDE_DireccionEntrega">Dirección Entrega:</span>
                        <span id="txtDE_DireccionEntrega">Plg. Bosc Can Cua P3-A</span>
                    </div>

                    <div class="Webpart_DatosEnvioLinea3">
                        <span id="txtTitleDE_Poblacion">Población:</span>
                        <span id="txtDE_Poblacion">Sils (Girona)</span>
                    </div>

                    <div class="Webpart_DatosEnvioLinea4">
                        <span id="txtTitleDE_CodigoPostal">Código Postal:</span>
                        <span id="txtDE_CodigoPostal">17410</span>
                    </div>

                    <div class="Webpart_DatosEnvioLinea5">
                        <span id="txtTitleDE_Pais">País: </span>
                        <span id="txtDE_Pais">España</span>
                    </div>
                </div>
            </div>

            <div class="classDatosAgencia">
                <span id="titleAgencia">Transporte</span>
                <div id="imgAgencia"></div>
                    
                <div class="classAgenciasLineas">
                    <div class="classLineaAgencia1">
                        <span id="txtTitleAgencia_Nombre">Nombre:</span>
                        <span id="txtAgencia_Nombre">Mud. Musquera SA</span>
                    </div>

                    <div class="classLineaAgencia2">
                        <span id="txtTitleAgencia_Telefono">Teléfono:</span>
                        <span id="txtAgencia_Telefono">902 315 430</span>
                    </div>

                    <div class="classLineaAgencia3">
                        <span id="txtTitleAgencia_NumSeguimiento">Número Seguimiento:</span>
                        <span id="txtAgencia_NumSeguimiento">003452121</span>
                    </div>
                </div>
            </div>

            <div class="classDatosEconomicos">
                <span id="titleEconomicos">Datos Económicos</span>
                <div id="imgEconomicos"></div>

                <div class="classDatosEconomicosLineas">
                    <div class="classLineaEconomicos1">
                        <span id="txtTitleEconomicos_FormaPago">Forma Pago:</span>
                        <span id="txtEconomicos_FormaPago">Contado</span>
                    </div>

                    <div class="classLineaEconomicos2">
                        <span id="txtTitleEconomicos_PrecioBase">Base Imp:</span>
                        <span id="txtEconomicos_PrecioBase">237.97€</span>
                    </div>

                    <div class="classLineaEconomicos3">
                        <span id="txtTitleEconomicos_IVA">Impuestos:</span>
                        <span id="txtEconomicos_IVA">IVA 18%</span>
                    </div>

                    <div class="classLineaEconomicos4">
                        <span id="txtTitleEconomicos_Precio">Total:</span>
                        <span id="txtEconomicos_Precio">280.80 €</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
        
    <!-- Slider -->
    <div id="container">
	<div id="example">
		<div id="slides">
			<div class="slides_container">
				<div class="slide">
					<div class="classDatosPedido">
                        <span class="titleDatosPedido">Datos Pedido</span>
                        <div class="imgDatosPedido"></div>

                        <div class="classDatosPedidoLineas">
                            <div class="classLineaDatosPedido1">
                                <span class="txtTitleDatosPedido_NumPedido">Nº Pedido:</span>
                                <span class="txtDatosPedido_NumPedido">1255</span>
                            </div>

                            <div class="classLineaDatosPedido2">
                                <span class="txtTitleDatosPedido_EstadoPedido">Estado Pedido:</span>
                                <span class="txtDatosPedido_EstadoPedido">Entregado</span>
                            </div>

                            <div class="classLineaDatosPedido3">
                                <span class="txtTitleDatosPedido_RefCliente">Ref. Cliente:</span>
                                <span class="txtDatosPedido_RefCliente">Kibuc BCN</span>
                            </div>

                            <div class="classLineaDatosPedido4">
                                <span class="txtTitleDatosPedido_FechaPedido">F. Pedido</span>
                                <span class="txtDatosPedido_FechaPedido">04.10.11</span>
                            </div>
                            </div>
                    </div>

                    <div class="classDatosEnvio" style="margin-left:40px">
                        <span class="titleDatosEnvio">Datos de Envío</span>
                        <div class="imgDatosEnvio"></div>
                        <div class="classDatosEnvioLineas">
                            <div class="Webpart_DatosEnvioLinea1">
                                <span class="txtTitleDE_Destino">Destino:</span>
                                <span class="txtDE_Destino">Julià Grup Furnitures Solutions SL</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea2">
                                <span class="txtTitleDE_DireccionEntrega">Dirección Entrega:</span>
                                <span class="txtDE_DireccionEntrega">Plg. Bosc Can Cua P3-A</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea3">
                                <span class="txtTitleDE_Poblacion">Población:</span>
                                <span class="txtDE_Poblacion">Sils (Girona)</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea4">
                                <span class="txtTitleDE_CodigoPostal">Código Postal:</span>
                                <span class="txtDE_CodigoPostal">17410</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea5">
                                <span class="txtTitleDE_Pais">País: </span>
                                <span class="txtDE_Pais">España</span>
                            </div>
                        </div>
                    </div>                
				</div>

				<div class="slide">
					<div class="classDatosEnvio">
                        <span class="titleDatosEnvio">Datos de Envío</span>
                        <div class="imgDatosEnvio"></div>
                        <div class="classDatosEnvioLineas">
                            <div class="Webpart_DatosEnvioLinea1">
                                <span class="txtTitleDE_Destino">Destino:</span>
                                <span class="txtDE_Destino">Julià Grup Furnitures Solutions SL</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea2">
                                <span class="txtTitleDE_DireccionEntrega">Dirección Entrega:</span>
                                <span class="txtDE_DireccionEntrega">Plg. Bosc Can Cua P3-A</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea3">
                                <span class="txtTitleDE_Poblacion">Población:</span>
                                <span class="txtDE_Poblacion">Sils (Girona)</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea4">
                                <span class="txtTitleDE_CodigoPostal">Código Postal:</span>
                                <span class="txtDE_CodigoPostal">17410</span>
                            </div>

                            <div class="Webpart_DatosEnvioLinea5">
                                <span class="txtTitleDE_Pais">País: </span>
                                <span class="txtDE_Pais">España</span>
                            </div>
                        </div>
                    </div>

                    <div class="classDatosAgencia" style="margin-left:40px">
                        <span class="titleAgencia">Transporte</span>
                        <div class="imgAgencia"></div>
                    
                        <div class="classAgenciasLineas">
                            <div class="classLineaAgencia1">
                                <span class="txtTitleAgencia_Nombre">Nombre:</span>
                                <span class="txtAgencia_Nombre">Mud. Musquera SA</span>
                            </div>

                            <div class="classLineaAgencia2">
                                <span class="txtTitleAgencia_Telefono">Teléfono:</span>
                                <span class="txtAgencia_Telefono">902 315 430</span>
                            </div>

                            <div class="classLineaAgencia3">
                                <span class="txtTitleAgencia_NumSeguimiento">Número Seguimiento:</span>
                                <span class="txtAgencia_NumSeguimiento">003452121</span>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="slide">
                    <div class="classDatosAgencia">
                        <span class="titleAgencia">Transporte</span>
                        <div class="imgAgencia"></div>
						<div class="classAgenciasLineas">
                            <div class="classLineaAgencia1">
                                <span class="txtTitleAgencia_Nombre">Nombre:</span>
                                <span class="txtAgencia_Nombre">Mud. Musquera SA</span>
                            </div>

                            <div class="classLineaAgencia2">
                                <span class="txtTitleAgencia_Telefono">Teléfono:</span>
                                <span class="txtAgencia_Telefono">902 315 430</span>
                            </div>

                            <div class="classLineaAgencia3">
                                <span class="txtTitleAgencia_NumSeguimiento">Número Seguimiento:</span>
                                <span class="txtAgencia_NumSeguimiento">003452121</span>
                            </div>
                        </div>
                    </div>

                        <div class="classDatosEconomicos" style="margin-left:40px">
                        <span class="titleEconomicos">Datos Económicos</span>
                        <div class="imgEconomicos"></div>

                        <div class="classDatosEconomicosLineas">
                            <div class="classLineaEconomicos1">
                                <span class="txtTitleEconomicos_FormaPago">Forma Pago:</span>
                                <span class="txtEconomicos_FormaPago">Contado</span>
                            </div>

                            <div class="classLineaEconomicos2">
                                <span class="txtTitleEconomicos_PrecioBase">Base Imp:</span>
                                <span class="txtEconomicos_PrecioBase">237.97€</span>
                            </div>

                            <div class="classLineaEconomicos3">
                                <span class="txtTitleEconomicos_IVA">Impuestos:</span>
                                <span class="txtEconomicos_IVA">IVA 18%</span>
                            </div>

                            <div class="classLineaEconomicos4">
                                <span class="txtTitleEconomicos_Precio">Total:</span>
                                <span class="txtEconomicos_Precio">280.80 €</span>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			<a href="#" class="prev"><img src="/Style%20Library/Julia/img/arrow-prev.png" width="24" height="43" alt="Arrow Prev"  style="border:none" /></a>
			<a href="#" class="next"><img src="/Style%20Library/Julia/img/arrow-next.png" width="24" height="43" alt="Arrow Next" style="border:none" /></a>
		</div>
	</div>
</div>
