﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacturasListUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.FacturasList.FacturasListUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>


<!-- link href="/Style%20Library/Julia/PedidoAdvanced.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/PedidoAdvanced.css" runat="server"/>

<%-- <script type="text/javascript">
     $(document).ready(function () {
         $(".BillImgUrl").click(function () {
             if ($(this).attr('src').Value() == "#") {
                 alert("No se encuentra el documento");
             } else { 
                alert("El documento existe! ")
             }
         });
     }) 
     
 
 </script>--%>
 <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
      <script type="text/javascript" >

          function OpenFile(url) {
              var ctype;
              var success = false;
              $.ajax({
                  type: "HEAD",
                  url: url,
                  success: function (data) {
                      success = true;
                  },
                  error: function (request, status) {
                      success = false;
                  },
                  complete: function (xhr, stat) {
                      var typ = xhr.contentType;
                      var res = xhr.responseText;
                      ctype = xhr.getResponseHeader('Content-Type');
                      //alert("Success: " + success + " -- Type: " + typ + " -- res: " + res + " -- ctype: " + ctype);
                      if ((success == true) && (ctype == "application/octet-stream")) {
                          window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
                      } else {
                          alert('<%= JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FileNotFound")%>');
                      }
                  }

              });
          }
</script>
</telerik:RadCodeBlock>

<telerik:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel" Skin="Vista" runat="server" />

<div class="tableOrderList">

        <telerik:RadGrid ID="RadGrid1" 
        Width="100%" 
        OnNeedDataSource="RadGrid1_NeedDataSource" 
        HeaderStyle-CssClass="PedidoAdvancedHeaderClass" 
        CssClass="Grid" 
        PageSize="9" 
        runat="server" 
        OnSortCommand="RadGrid1_SortCommand" 
        AllowFilteringByColumn="true" 
        AllowPaging="true" 
        AllowSorting="true" 
        AutoGenerateColumns="false" 
        GridLines="None" 
        OnItemDataBound="RadGrid1_ItemDataBound"
        OnPreRender="RadGrid1_PreRender"
        >
      
    
         
         

          <GroupingSettings CaseSensitive="false" /> 
        
      
           <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Bottom"  
             AutoGenerateColumns="false"
            UseAllDataFields="true"
            InsertItemPageIndexAction="ShowItemOnCurrentPage" 
            CanRetrieveAllData="false"
            AllowNaturalSort ="true">
      
            
          <Columns>
            
                <telerik:GridTemplateColumn AllowFiltering="false"  HeaderText="Enlace Pdf"  UniqueName="PDFFact">
                    <ItemTemplate>
                        <a class="BillImgUrl" onclick="OpenFile('<%# Eval("Enlace Pdf") %>'); return false;" href="<%# Eval("Enlace Pdf") %>">
                            <img  src="/Style%20Library/Julia/img/pdf_ico.jpg" width="25px"  alt=""  /> 
                        </a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn >
             
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="Codigo Factura" HeaderText="Codigo Factura" UniqueName="CodigoFactura" />
                <telerik:GridBoundColumn    AllowFiltering="false" DataField="Fecha" HeaderText="Fecha" DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy}"  UniqueName="Fecha" ReadOnly="true"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true"    DataField="Codigo Cliente" HeaderText="Codigo Cliente" UniqueName="CodigoCliente" />
                <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                <telerik:GridBoundColumn  CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true"     DataField="Nombre Cliente" HeaderText="Nombre Cliente" UniqueName="NombreCliente" />
                <telerik:GridBoundColumn  CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true"     DataField="Referencia Cliente" HeaderText="Referencia Cliente" UniqueName="RefClient" />
                <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Importe" ItemStyle-CssClass="FacturasCol" HeaderText="Importe" UniqueName="Importe"  DataType="System.Decimal" ReadOnly="true" DataFormatString="{0:N2}"/>
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Divisa"  UniqueName="Divisa" Display="true" />
            

            </Columns>
        </MasterTableView>
         <PagerStyle Mode="NextPrev" />
       
  
    </telerik:RadGrid>

</div>
  
    