﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Telerik.Web.UI;
using System.Data;
using Microsoft.SharePoint;
using System.IO;
using System.Diagnostics;
namespace JuliaGrupPortalClientes_v2.Web_Parts.FacturasList
{
    public partial class FacturasListUserControl : UserControl
    {

        

        private User currentUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            EnsureChildControls();
            Sesion SesionJulia = new Sesion();
            Client cl = (Client)Session["Client"];
            BillDataAccessObject BillsDAO;
            BillsDAO = SesionJulia.GetBills(cl.Code);


            this.currentUser = (User)Session["User"];

            if (!IsPostBack)
            {
                //if (this.currentUser is Agent)
                //{
                //    BtnTodos.Visible = true;
                //}
                //else
                //{
                //    BtnTodos.Visible = false;
                //}

                this.Initialize();

            }
            else
            {
                //this.LoadSelectedArticles();

            }
        }

        private void Initialize()
        {

            this.RadGrid1.Columns.FindByUniqueNameSafe("PDFFact").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsEnlace");
            this.RadGrid1.Columns.FindByUniqueNameSafe("CodigoFactura").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsCodFactura");
            this.RadGrid1.Columns.FindByUniqueNameSafe("Fecha").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsFecha");
            this.RadGrid1.Columns.FindByUniqueNameSafe("CodigoCliente").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsCodigoCliente");
            this.RadGrid1.Columns.FindByUniqueNameSafe("NombreCliente").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsNombreCliente");
            this.RadGrid1.Columns.FindByUniqueNameSafe("RefClient").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsRefCliente");
            this.RadGrid1.Columns.FindByUniqueNameSafe("Importe").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("BillsImporte"); // +" (" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
        
        }

        /// <summary>
        /// Rebind of RadGrid1
        /// </summary>

       


        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DataTable data = null;
            data = loadBillsOrder();
            this.RadGrid1.DataSource = data;
        }

        /// <summary>
        /// Load Bills of a Client
        /// </summary>
        /// <returns> Data with  Bills </returns>
        private DataTable loadBillsOrder()
        {
            
            DataTable data = null;
            try 
            {
                Sesion SesionJulia = new Sesion();
               
                if (Session["ClientOrders"] == null)
                {
                    BillDataAccessObject BillsDAO;
                    Client cl = (Client)Session["Client"];
                    BillsDAO = SesionJulia.GetBills(cl.Code);
                    // Carrega les factures del client
                    data = GenerateTable();

                    LoadFacturas(currentUser.UserName, BillsDAO.GetAllBills(), ref data);

                    Session.Add("ClientBills", data);
                }
                else
                {
                    data = (DataTable)Session["ClientBills"];
                }
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            } 
            return data;
        }
        /// <summary>
        /// Load ALL bills of all clients
        /// </summary>
        /// <returns>Data with all BILLS</returns>
       

        private DataTable GenerateTable()
        { 
            DataTable data = new DataTable();
            try
            {
               
                data.Columns.Add("Enlace Pdf");
                data.Columns.Add("Codigo Factura");
                data.Columns.Add("Fecha", typeof(DateTime));
                data.Columns.Add("Codigo Cliente");
                data.Columns.Add("Nombre Cliente");
                data.Columns.Add("Referencia Cliente");
                data.Columns.Add("Importe", typeof(Decimal));
                data.Columns.Add("Divisa");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            return data;
        }
        private void LoadFacturas(String Client, List<Bill> bill, ref DataTable data)
        {
            try
            {
                
                foreach (Bill factura in bill)
                {
                    //PROVISIONALMENT POSEM PER DEFECTE EL MATEIX COI DE FACTURA I CLIENT FINS QUE DISPOSEM DUNA FACTURA

                    DataRow row = data.NewRow();
                    //row["Enlace Pdf"] = GetFactureURL(factura.Code, factura.CostumerCode);
                    row["Enlace Pdf"] = GetFactureURL(factura.Code, factura.CostumerCode);
                    row["Codigo Factura"] = factura.Code;
                    row["Fecha"] = factura.Date;
                    row["Codigo Cliente"] = factura.CostumerCode;
                    row["Nombre Cliente"] = factura.CostumerName;
                    row["Referencia Cliente"] = factura.Referencia;
                    row["Importe"] = factura.Import;
                        //+ " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");

                    row["Divisa"] = (factura.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");

                    data.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }




        }
        protected void  RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button addButton = e.Item.FindControl("AddNewRecordButton") as Button;
                if (addButton != null) 
                    addButton.Visible = false;
                LinkButton lnkButton = (LinkButton)e.Item.FindControl("InitInsertButton");
                if (lnkButton != null) 
                    lnkButton.Visible = false;
            }
        } 



        public string GetFactureURL(string codFactura, string codClient)
        {
            string output = string.Empty;
            //string ret = string.Empty;
            try
            {

                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=bill&ClientId=" + codClient + "&FileName=" + codFactura + ".pdf";
                   //http://shp2010/GestionDocumental/Documentacin%20Clientes/MALLORCA/FV11-001554.pdf       
                //if (File.Exists(output))
                //{
                //    ret = output;
                //}
                //else
                //{
                //    ret = "#";
                  
                //}
                      
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


            return output;
        }
       
        //protected void Rad_PreRender(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);

        //        manager.AjaxSettings.AddAjaxSetting(RadGrid1,PanelAjax, RadAjaxLoadingPanel1);

        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }

        //}
        protected void RadGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                //Apply custom sorting
                GridSortExpression sortExpr;

                sortExpr = new GridSortExpression();

                switch (e.OldSortOrder)
                {
                    case GridSortOrder.None:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Descending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Ascending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = RadGrid1.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Descending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Ascending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                }

                e.Canceled = true;
                RadGrid1.Rebind();


                //Default sort order Descending


                if (!e.Item.OwnerTableView.SortExpressions.ContainsExpression(e.SortExpression))
                {
                    sortExpr = new GridSortExpression();
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);

            manager.AjaxSettings.AddAjaxSetting(RadGrid1, RadGrid1, LoadingPanel);

        }




    }
}
