﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GestioDeDocumentsUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.GestioDeDocuments.GestioDeDocumentsUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<SharePoint:CssRegistration ID="CssRegistrationComunicaciones" name="/Style Library/Julia/Comunicaciones.css" runat="server"/>
<script type="text/javascript" >

//    function OpenFile(url) {
//        var ctype;
//        var success = false;
//        $.ajax({
//            type: "HEAD",
//            url: url,
//            success: function (data) {
//                success = true;
//            },
//            error: function (request, status) {
//                success = false;
//                //alert('<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FileNotFound")%>');
//            },
//            complete: function (xhr, stat) {
//                var typ = xhr.contentType;
//                var res = xhr.responseText;
//                ctype = xhr.getResponseHeader('Content-Type');
//                //alert("Success: " + success + " -- Type: " + typ + " -- res: " + res + " -- ctype: " + ctype);
//                if ((success == true) && (ctype == "application/octet-stream")) {
//                    window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
//                } else {
//                    alert('<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FileNotFound")%>');
//                }
//            }

//        });
    //    }
    $(document).ready(function () {

        $(".Main_Box_PersonalDocuments .DocumentItem").hide();
        $(".HeaderFolder").click(function () {
            $(".DocumentItem").hide();
            /*var headerValue = $(this).children("span").text().replace(" ", "");*/
            var headerValue = $(this).children("span").text().replace(/ /gi, "");
            $("." + headerValue).toggle();
        })
    })
</script>

<div class="FixaClient">
    <asp:Label ID="CodeClientLbl" runat="server">Ficha de cliente</asp:Label>
    <%--<asp:TextBox runat="server" ID="TxtCode" MaxLength="15"></asp:TextBox>--%>
    
<telerik:RadComboBox 
    id="RadComboBox" 
    runat="server" 
    EnableTextSelection="true"
    MarkFirstMatch="true"
    Width ="300px"
    CssClass="ComboClients"
    Filter="Contains"
    DataTextField ="Name" DataValueField ="Code" 
 
    OnClientKeyPressing="(function(sender, e){ if (!sender.get_dropDownVisible()) sender.showDropDown(); })"
    AutoPostBack ="false" 
    > 
</telerik:RadComboBox>
    <asp:ImageButton ID="SendCode" runat="server" ImageUrl="/Style%20Library/Julia/img/descargarPDF.png"
    CssClass="EyeIcon" onclick="SendCode_Click"/>
</div>
<div class="Box_Documentation">
    <div class="PersonalDocuemntation">
        <div class="TittlePersonalDocumentation">
            <asp:Image runat="server" ImageUrl="/Style%20Library/Julia/img/doc_personal.png" Width="15px"
                CssClass="ImageHeaderWebpart_PersonalDocumentation" />
            <asp:Label ID="TittleLblDocPer" runat="server" Text="Documentación Personal" CssClass="LblHeaderWebpart_PersonalDocumentation"></asp:Label>
        </div>
        <div class="Main_Box_PersonalDocuments">
         
            <telerik:RadListView ID="DocumentList" runat="server" ItemPlaceholderID="PlaceHolder1"
                >
                <LayoutTemplate>
                    <div class="contenedorProductos">
                        <asp:Panel ID="productPanel" runat="server">
                            <div>
                                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="CatalogPagination">
                     <%--   <div class="CatalogNumberPagination">
                            <telerik:RadDataPager ID="RadDataPager1" CssClass="RadDataPager"  BorderStyle="None" BorderWidth="0px" BackColor="White" PageSize="8" runat="server">
                                <Fields>
                                    <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                                    <telerik:RadDataPagerButtonField FieldType="Numeric" />
                                    <telerik:RadDataPagerButtonField FieldType="NextLast" />
                                </Fields>
                            </telerik:RadDataPager>
                        </div>--%>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="ContentDocuments">
                        <div class="HeaderFolder">
                            <asp:Image ID="HeaderImagePersonal" Width="24px" runat="server" Visible='<%# Convert.ToBoolean(Eval("Visible"))%>' ImageAlign="Left"
                                ImageUrl="/Style%20Library/Julia/img/folderIcon.png" />
                            <asp:Label ID="HeaderLblPer"  Text='<%#Eval("Header")%>' Visible='<%# Convert.ToBoolean(Eval("Visible"))%>' CssClass="HeaderFolderLbl" runat="server"></asp:Label>
                        </div>
                        
                        <div class="DocumentItem <%#Eval("Header_WithoutSpace")%>" >
                                <a href='<%#Eval("URL")%>'>
                                    <asp:Image ID="DocIcon" Width="15px" runat="server" ImageAlign="Left" CssClass="DocIcon"
                                        ImageUrl="/Style%20Library/Julia/img/docIcon.png" />
                                    <asp:Label ID="DocName" Text='<%#Eval("Name")%>' runat="server"></asp:Label>
                                </a>
                        </div>
                    </div>
                </ItemTemplate>
            </telerik:RadListView>
            <%--<asp:Label ID="defaultTextPersonal" runat="server" Style="display: none"></asp:Label>--%>
        </div>
    </div>
    <div class="GeneralDocumentation">
        <div class="TittleGeneralDocumentation">
            <asp:Image ID="GeneralDocICon" runat="server" ImageUrl="/Style%20Library/Julia/img/doc_general.png"
                Width="20px" CssClass="ImageHeaderWebpart_PersonalDocumentation" />
            <asp:Label ID="TittleLblDocGen" runat="server" Text="Documentación General" CssClass="LblHeaderWebpart_PersonalDocumentation"></asp:Label>
        </div>
        <div class="Main_Box_GeneralDocumentation">
            <div class="HeaderFolder" style="display: none">
                <asp:Image ID="HeaderImageGeneral" Width="30px" runat="server" ImageAlign="Left"
                    ImageUrl="/Style%20Library/Julia/img/folderIcon.png" />
                <asp:Label ID="HeaderLblGer" Text="Header" CssClass="HeaderFolderLbl" runat="server"></asp:Label>
            </div>
            <telerik:RadListView ID="GeneralDocumentList" runat="server" AllowPaging="true"  ItemPlaceholderID="PlaceHolder1">
                 <LayoutTemplate>
                    <div class="contenedorProductos">
                        <asp:Panel ID="productPanel" runat="server">
                            <div>
                          
                                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                           
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="CatalogPagination">
                   <%--     <div class="CatalogNumberPagination">
                            <telerik:RadDataPager ID="RadDataPager2" BorderStyle="None" BorderWidth="0px" BackColor="White" CssClass="RadDataPager" PageSize="8" runat="server">
                                <Fields>
                                    <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                                    <telerik:RadDataPagerButtonField FieldType="Numeric" />
                                    <telerik:RadDataPagerButtonField FieldType="NextLast" />
                                   
                                </Fields>
                            </telerik:RadDataPager>
                        </div>--%>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="ContentDocuments">
                        <div class="DocumentGeneral">
                            <a href='<%#Eval("URL")%>'>
                                <asp:Image ID="DocIconGer" Width="15px" runat="server" ImageAlign="Left" CssClass="DocIcon"
                                    ImageUrl="/Style%20Library/Julia/img/docIcon.png" />
                                <asp:Label ID="DocNameGer" Text='<%#Eval("Name")%>' runat="server"></asp:Label>
                            </a>
                        </div>
                    </div>
                </ItemTemplate>
            </telerik:RadListView>
              <%--<asp:Label ID="defaultTextGeneric" runat="server" style="display:none"></asp:Label>--%>
        </div>
    </div>
</div>
