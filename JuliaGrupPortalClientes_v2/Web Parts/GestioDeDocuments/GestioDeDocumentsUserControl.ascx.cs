﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Telerik.Web.UI;
using System.Data;
using Microsoft.SharePoint;
using System.IO;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using Microsoft.SharePoint.Utilities;

namespace JuliaGrupPortalClientes_v2.Web_Parts.GestioDeDocuments
{

    public partial class GestioDeDocumentsUserControl : UserControl
    {
        User CurrentUser;
        Client CurrentClient;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                loadClients_Combo();
                GetDocuments();
                loadtexts();
            } 
        }

        protected void loadtexts()
        {
            TittleLblDocPer.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Documentacion_TittleLblDocPer_Lbl");
            TittleLblDocGen.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Documentacion_TittleLblDocGen_Lbl");
            CodeClientLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Documentacion_FichaCliente_Lbl"); 
        }

        protected void loadClients_Combo()
        {
            if (Session["User"]!= null)
            {
                CurrentUser = (User)Session["User"];
            }

            if (RadComboBox.DataSource == null)
            {
                if (this.CurrentUser is Agent)
                {
                    Agent agent = (Agent)this.CurrentUser;
                    //set the first client as selected client
                    //Session["Client"] = agent.ListClients.First();


                    this.RadComboBox.DataSource = agent.ListClientsName;
                    this.RadComboBox.SelectedValue = null;
                    this.RadComboBox.DataBind();
                 
                }
                else if (this.CurrentUser is ClientGroup)//CLIENTGROUP
                {
                    ClientGroup clientgroup = (ClientGroup)this.CurrentUser;
                    //set the first client as selected client
                    //Session["Client"] = agent.ListClients.First();


                    this.RadComboBox.DataSource = clientgroup.ListClientsName;
                    this.RadComboBox.SelectedValue = null;
                    this.RadComboBox.DataBind();
                  }
            }
        }

       

        //protected void SendCode_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string doc = string.Empty;
        //        if (!(string.IsNullOrEmpty(this.RadComboBox.SelectedValue.ToString())))
        //        {
        //            ClientDataAccessObject clientDAO = new ClientDataAccessObject();
        //            if (Session["Client"] != null)
        //            {
        //                this.CurrentUser = (Client)Session["Client"];
        //            }
        //            //doc = clientDAO.GetInformeCliente(CurrentUser.Code, this.RadComboBox.SelectedValue);
        //            //REZ 19032013 - Crec que està malamet la crida...
        //            doc = clientDAO.GetInformeCliente(CurrentUser.UserName, this.RadComboBox.SelectedValue);

        //            byte[] todecode_byte = Convert.FromBase64String(doc.Replace("<Base64>", "").Replace("</Base64>", ""));
        //            if (todecode_byte != null)
        //            {
        //                muestraArchivo(todecode_byte);
        //            }
        //        }
        //        else
        //        {
        //            throw new NavException("Introduce un Valor");
        //        }
               
              
        //    }
        //    catch (NavException ex)
        //    {
        //        string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

        //    }
        //    catch (Exception ex)
        //    {

        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
           
            

        //}
        private void muestraArchivo(byte[] documento)
        {
            string extension = ".pdf";
            string contentType = "";
            switch (extension)
            {
                case ".xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".doc":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/msword";
                    break;
                case ".ppt":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
            }
            //MUESTRA VÍA WEB
            Response.Clear();
            MemoryStream ms = new MemoryStream(documento);
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=clientinfo.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.Redirect(Server.MapPath("/"));
            Response.End();

        }
        private void GetDocuments()
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
             {
                 using (SPSite site = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                 {

                     using (SPWeb web = site.OpenWeb())
                     {


                         if (Session["Client"] != null)
                         {
                             this.CurrentClient = (Client)Session["Client"];
                         }

                         if (Session["User"] != null)
                         {
                             this.CurrentUser = (User)Session["User"];
                         }
                         SPWeb cweb = SPContext.Current.Web;
                         string siteurl = cweb.Url.ToString();
                         SPListItemCollection itemCol;
                         // Get data from a list.


                         SPList GeneralDocList;
                         SPFolder FolderGeneral;
                         SPList PersonalDocsList;
                         SPFolder folderPersonal;
                         
                  
                         //General DOCS LIST
                         if (web.Lists["Documentación Comercial"] != null)
                         {
                             GeneralDocList = web.Lists["Documentación Comercial"];
                             FolderGeneral = web.GetFolder(GeneralDocList.RootFolder.ServerRelativeUrl);
                             //DOCUMENTACIO GENRAL
                             if (FolderGeneral.Exists && FolderGeneral.ItemCount > 0)
                             {

                                 SPQuery query = new SPQuery();
                                 query.Folder = FolderGeneral;
                                 query.Query = "<Where><Eq><FieldRef Name='Idioma' /><Value Type='Text'>" + CurrentUser.Language + "</Value></Eq></Where><OrderBy><FieldRef Name='LinkFilename' Ascending='True' /></OrderBy>";
                                 itemCol = GeneralDocList.GetItems(query);

                                 this.GeneralDocumentList.DataSource = LoadDataGeneralDoc(itemCol, FolderGeneral.Url);
                             }
                             else
                             {
                                 //this.defaultTextGeneric.Style.Remove("display");
                                 //this.defaultTextGeneric.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoDocs");
                             }
                         }
                         else
                         {


                             //this.defaultTextGeneric.Style.Remove("display");
                             //this.defaultTextGeneric.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoDocs");
                         }
                       
                         
                         //PERSONAL DOCS LIST
                         if (web.Lists[JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetList] != null)
                         { 
                            PersonalDocsList = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetList];
                            folderPersonal = web.GetFolder(PersonalDocsList.RootFolder.ServerRelativeUrl + "/" + CurrentUser.Code);
                            //DOCUMENTACIO PERSONAL
                            if (folderPersonal.Exists && folderPersonal.ItemCount > 0)
                            {
                                //<OrderBy><FieldRef Name=""ContentType"" Ascending=""False"" /></OrderBy>
                                SPQuery query = new SPQuery();
                                query.Query = @"
                                  
                                        <Where>
                                            <And>
                                                <Neq>
                                                    <FieldRef Name=""ContentType"" />
                                                     <Value Type=""Computed"">Client Bill</Value>
                                                </Neq>
                                                <Neq>
                                                     <FieldRef Name=""ContentType"" />
                                                     <Value Type=""Computed"">Client DeliveryNote</Value>
                                                </Neq>
                                            </And>
                                          </Where>
                                        
                                        <OrderBy><FieldRef Name=""LinkFilename"" Ascending=""True"" /></OrderBy>
                                        <GroupBy>
                                            <FieldRef Name=""ContentType""/>
                                          </GroupBy>";
                                query.Folder = folderPersonal;
                                itemCol = PersonalDocsList.GetItems(query);
                                
                                this.DocumentList.DataSource = LoadDataPersonalDoc(itemCol, folderPersonal.Url);

                            }
                            else
                            {

                                //this.defaultTextPersonal.Style.Remove("display");
                                //this.defaultTextPersonal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoDocs");

                            }
                         }
                         else
                         {

                             //this.defaultTextPersonal.Style.Remove("display");
                             //this.defaultTextPersonal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoDocs");

                         }
                        
                         
                       
                       
                       
                       

                     }

                 }
             });

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private int GetLCID(string lng)
        {
            string ret = string.Empty;
            switch (lng)
            {
                case "CAT":
                    ret = "1027";
                    break;
                case "ESP":
                    ret = "3082";
                    break;
                case "FRA":
                    ret = "1036";
                    break;
                case "ITA":
                    ret = "1040";
                    break;
                case "ENU":
                    ret = "1033";
                    break;
                case "DEU":
                    ret = "1031";
                    break;
                default:
                    ret = "3082";
                    break;
            }
            return Convert.ToInt32(ret);
        }

        public DataTable LoadDataGeneralDoc(SPListItemCollection items, string listUrl)
        {
            DataTable table = new DataTable();

            if (items != null)
            {
                table.Columns.Add("ID");
                table.Columns.Add("Name");
                table.Columns.Add("URL");

                foreach (SPItem item in items)
                {
                    DataRow row = table.NewRow();
                    //row["ID"] = item.ID;
                    row["Name"] = item["Name"];
                    //row["URL"] = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + listUrl + "/" + CurrentUser.Code + "/" + item["Name"];
                    //REZ 04052013 -- Esto esta fatal. Deberia ser la Url de _layouts para busqueda de documentación...
                    row["URL"] = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + listUrl + "/" + item["Name"];


                    table.Rows.Add(row);
                }
            }
            return table;

            
        }

        public DataTable LoadDataPersonalDoc(SPListItemCollection items, string listurl)
        {
            DataTable table = new DataTable();
            //string newContent = string.Empty;
            //string oldContent = string.Empty;
            List<string> contents = new List<string>();
            
            if (items != null)
            {
                table.Columns.Add("ID");
                table.Columns.Add("Name");
                table.Columns.Add("URL");
                table.Columns.Add("Header");
                table.Columns.Add("Header_WithoutSpace");
                table.Columns.Add("Visible");
               


                foreach (SPItem item in items)
                {
                   
                        DataRow row = table.NewRow();

                        if (contents.Count != 0)
                        {
                            if (!(contents.Contains(item["Content Type"].ToString())))
                            {
                                contents.Add(item["Content Type"].ToString());
                                row["Visible"] = "true";
                            }
                            else
                            {
                                row["Visible"] = "false";
                            }

                        }
                        else
                        {
                            row["Visible"] = "true";
                            contents.Add(item["Content Type"].ToString());
                        }
                        row["Header"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("cType_" + item["Content Type"]);
                        row["Header_WithoutSpace"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("cType_" + item["Content Type"]).Replace(" ","");
                        row["Name"] = item["Name"];
                        //REZ 04052013 -- Esto esta fatal. Deberia ser la Url de _layouts para busqueda de documentación...
                        row["URL"] = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + listurl + "/" + item["Name"];
                        table.Rows.Add(row);
                
                    
                   
                    
                   
                }
            }
            return table;

        }

        protected void SendCode_Click(object sender, ImageClickEventArgs e)
        {
         string output = string.Empty;

         try
         {
             string doc = string.Empty;
             if (!(string.IsNullOrEmpty(this.RadComboBox.SelectedValue.ToString())))
             {
                 ClientDataAccessObject clientDAO = new ClientDataAccessObject();
                 if (Session["Client"] != null)
                 {
                     this.CurrentUser = (Client)Session["Client"];
                 }
                 doc = clientDAO.GetInformeCliente(CurrentUser.UserName, this.RadComboBox.SelectedValue);


                //string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FitxaClientFound") + "', 320, 100,"+ JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FitxaDeClientSend") +"); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                 string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("FitxaClientFound") + "', 320, 100,''); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                 Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                 



             }
             else
             {
                 throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("InvalidData"));
             }
         }
         catch (NavException ex)
         {
             string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Navision Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
             Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
         }

       }
    }
}
