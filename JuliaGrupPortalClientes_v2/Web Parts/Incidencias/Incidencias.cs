﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;
using JuliaGrupUtils.Utils;

namespace JuliaGrupPortalClientes_v2.Incidencias
{
    [ToolboxItemAttribute(false)]
    public class Incidencias : WebPart
    {

        #region Constructor

        public Incidencias()
        {
            // Set chrome type
            this.ChromeType = PartChromeType.None;
            this.ChromeState = PartChromeState.Normal;
        }

        #endregion

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Incidencias/IncidenciasUserControl.ascx";

        #region Methods

        protected override void CreateChildControls()
        {
            IncidenciasUserControl control = (Page.LoadControl(_ascxPath) as IncidenciasUserControl);

            // Set internal parameters
            control.WPTitle = LanguageManager.GetLocalizedString("IssuesWebPartTitle");
            control.WPSubtitle = LanguageManager.GetLocalizedString("IssuesWebPartSubtitle");

            Controls.Add(control);
        }

        #endregion
    }
}
