﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncidenciasUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Incidencias.IncidenciasUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>

<!-- link href="/Style%20Library/Julia/Incidencias.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationIncidencias" Name="/Style Library/Julia/Incidencias.css"
    runat="server" />
  
 <script type="text/javascript">
     function alertCallBackFn(arg) {        
         window.location.href = window.location.pathname;
     }
     function setImgURL(url) {
         $(".imgIssue").attr("src", url);
     }
     function HidePanelImage() {
         $("#DivImage").hide();
     }
     function ShowPanelImage() {
         $("#DivImage").show();
     }
     function alertRetorno(arg) {
         window.location = "/Pages/Issues.aspx";
     }
     function SetRequiredImage() {
         $("#DivImage span.txtIssueMain").append("<span class='RequiredField'>*</span>");
     }
     function ResetImgLbl() {
         $("#DivImage span.txtIssueMain .RequiredField").remove();
     }
     function loadingPnl() {         
             $.blockUI({ message: $('#loadingPnl') })         
     }     
 </script>
 <script type="text/javascript">

     function checkExtension(radUpload, eventArgs) {
         var input = eventArgs.get_fileInputField();
         if (!radUpload.isExtensionValid(input.value)) {
             var inputs = radUpload.getFileInputs();
             for (i = 0; inputs.length > i; i++) {
                 if (inputs[i] == input) {

                     radUpload.clearFileInputAt(i);
                     break;
                 }
             }
         }
     }

     window.validationFailed = function (radAsyncUpload, args) {
         var $row = $(args.get_row());
         var erorMessage = getErrorMessage(radAsyncUpload, args);
         var span = createError(erorMessage);
         $row.addClass("ruError");
         $row.append(span);
     }

     function getErrorMessage(sender, args) {
         var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
         if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
             if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                 return (Res["issues_FileTypeNotSupported"]);
             }
             else {
                 return (Res["issues_FileSizeNotSupported"]);
             }
         }
         else {
             return (Res["issues_FileTypeNotSupported"]);
         }
     }

     function createError(erorMessage) {
         var input = '<span class="ruErrorMessage">' + erorMessage + ' </span>';
         return input;
     }

</script>

<script type="text/javascript">
    function OnloadFunction() {

        var cont = 0;
        $(".photoIssueMark").click(function (e) {

            if (cont < 4) {
                var img = $(".imgIssue");
                var offset = img.offset();
                var x = e.pageX - offset.left;
                var y = e.pageY - offset.top;
                var totalX = x + offset.left - 25;
                var totaly = y + offset.top - 20;
                //                 alert(totalX + "  --  " + totaly);
                //                 alert("e.pageX - this.offsetLeft--->" + x + "     e.pageX --->" + e.pageX);
                //                 alert("e.pageY - this.offsetLtop-->" + y + "     e.pageY-->  " + e.pageY);
                var img = $('<img>');

                img.css("position", "absolute");
                img.css('top', totaly + "px");
                img.css('left', totalX + "px");
                //img.css('top', "100px");
                //img.css('left', "100px");

                img.css('z-index', '99');
                img.attr('src', '/Style%20Library/Julia/img/Mark.png');

                img.attr('name', 'error' + cont);
                img.addClass('error_mark');
                img.appendTo('body');
                if (cont == 0) {
                    $('.CoordX0').val(x);
                    $('.CoordY0').val(y);
                }

                if (cont == 1) {
                    $('.CoordX1').val(x);
                    $('.CoordY1').val(y);
                }

                if (cont == 2) {
                    $('.CoordX2').val(x);
                    $('.CoordY2').val(y);
                }
                if (cont == 3) {
                    $('.CoordX3').val(x);
                    $('.CoordY3').val(y);
                }
                $('.cont').val(cont);

                cont++;
            }
        });

        //funcion que controla los colores del menu segun clicados o no 
        $(".error_mark").live("click", function () {
            $(this).remove();
            cont--;
        });

        $(".DeleteMarks").click(function () {
            $(".error_mark").each(function () {
                $(this).remove();
            });
            cont = 0;
            $('.CoordX0').val("");
            $('.CoordY0').val("");
            $('.CoordX1').val("");
            $('.CoordY1').val("");
            $('.CoordX2').val("");
            $('.CoordY2').val("");
            $('.CoordX3').val("");
            $('.CoordY3').val("");
            $('.cont').val("");
        });

        //alert('OnloadFunction ends');
    }
    $(document).ready(function () {
        OnloadFunction();
    })

    function DeleteAddValue() {
        $("li.ruActions .ruAdd").removeAttr("value");
    }
    //muestra caracteres restantes en campo observación
    function checkLength(txt) {
        var txtLength;
        if (txt != null) {
            var remaining = txt.maxLength - txt.value.length;
            $('#TextRemaining').text(remaining);
        }
    }
    function loadTextBox(sender) {
        var remaining = sender._maxLength - sender._displayText.length;
        $('#TextRemaining').text(remaining);
    }
</script>

<style type="text/css">
div.RadUpload .ruAdd /* change add button image  upload*/
   {
    height: 35px !important;
    width: 135px !important;
   }
    div.RadUpload .ruBrowse
    {
        width:36px!important;height:18px;border:none;background-color:transparent!important;
    }
 
</style>
<script type="text/javascript">
    $("div.RadUpload .ruAdd").attr("value", "");
</script>
<div class="classIncidencias">
    <div class="headerIncidencias">
        <asp:Label ID="lblTitle" CssClass="titleIncidencias" runat="server" />
        <asp:Label ID="lblSubtitle" CssClass="subtitleIncidencia" runat="server" />
    </div>
    <div class="bodyIncidencias">
        <asp:Panel runat="server" ID="Panel1" CssClass="PanelLoading">
            <div class="lineIssue">
                <div class="txtIssue">
                    <div class="txtLabel">
                        <asp:Label ID="lblNCliente" CssClass="txtIssueMain" runat="server" /></div>
                </div>
                <div class="inputIssue">
                    <asp:TextBox ID="CoordX0" CssClass="CoordX0 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordX1" CssClass="CoordX1 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordX2" CssClass="CoordX2 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordX3" CssClass="CoordX3 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordY0" CssClass="CoordY0 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordY1" CssClass="CoordY1 HideLblCoord"  runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordY2" CssClass="CoordY2 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="CoordY3" CssClass="CoordY3 HideLblCoord" runat="server"></asp:TextBox>
                    <asp:TextBox ID="cont" CssClass="cont HideLblCoord" runat="server"> </asp:TextBox>
                    <telerik:RadComboBox ID="DropDownListClient" AutoPostBack="True" runat="server" Width="300px" MaxLength="60"
                        EnableTextSelection="true" MarkFirstMatch="true" CssClass="ComboClients" Filter="Contains"
                        DataTextField="Name" DataValueField="Code" OnSelectedIndexChanged="DropDownListClient_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </div>
            </div>
            <div class="lineIssue">
                <div class="IssueType">
                    <div class="txtIssue ">
                        <div class="txtLabel">
                            <asp:Label ID="LblIssuesType" CssClass="txtIssueMain" runat="server" /></div>
                    </div>
                    <div class="inputIssue">
                        <telerik:RadComboBox ID="DropDownIssueType" 
                            AutoPostBack="true" 
                            CssClass="DropDownIssueType"
                            runat="server" Width="200px" 
                            OnSelectedIndexChanged="DropDownIssueType_SelectedIndexChanged"
                            Filter="Contains"
                            AllowCustomText="true"
                            onclientselectedindexchanged="loadingPnl" />                         
                    </div>
                </div>
            </div>
            <div class="lineIssue">
                <div class="IssueSubType">
                    <div class="txtIssue ">
                        <div class="txtLabel">
                            <asp:Label ID="LblIssueSubType" CssClass="txtIssueMain" runat="server" /></div>
                    </div>
                    <div class="inputIssue">
                        <telerik:RadComboBox ID="DropDownIssueSubType" 
                            AutoPostBack="true" 
                            CssClass="DropDownIssueType"
                            runat="server" Width="200px"  
                            OnSelectedIndexChanged="DropDownIssueSubType_SelectedIndexChanged" 
                            Enabled="false"
                            Filter="Contains"
                            AllowCustomText="true"                            
                            onclientselectedindexchanged="loadingPnl"/>
                    </div>
                </div>
            </div>
            <%-- Codi de Producte  --%>
                <asp:Panel ID="PanelReference" runat="server">
                    <div class="lineIssue issueReference">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblReference" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadComboBox ID="DropDownReference" AutoPostBack="true" runat="server" Width="300px"
                                 EnableTextSelection="true" MarkFirstMatch="true" Filter="Contains"
                                OnSelectedIndexChanged="DropDownReference_SelectedIndexChanged"
                                Enabled="false"
                                onclientselectedindexchanged="loadingPnl"/>
                        </div>
                    </div>
                </asp:Panel>
                <%-- End Codi de Producte --%>
                <asp:Panel ID="PanelDoc" runat="server">
                    <div class="lineIssue issueDocument">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblDoc" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadComboBox ID="DropDownDoc" AutoPostBack="true" runat="server" Width="200px"
                                OnSelectedIndexChanged="DropDownDoc_SelectedIndexChanged" 
                                onclientselectedindexchanged="loadingPnl"
                                Enabled="false"
                                Filter="Contains"
                                AllowCustomText="true"/>
                            <asp:RadioButton ID="AlbaranButton" GroupName="AlbaranFacturaBut" runat="server"
                                AutoPostBack="True"  OnCheckedChanged="AlbaranFacturaChange" onchange="loadingPnl"/>
                            <asp:RadioButton ID="FacturaButton" GroupName="AlbaranFacturaBut" runat="server"
                                Checked="true" AutoPostBack="True" OnCheckedChanged="AlbaranFacturaChange" onchange="loadingPnl"/>
                            <asp:RadioButton ID="PedidoButton" GroupName="AlbaranFacturaBut" runat="server"
                                AutoPostBack="True" OnCheckedChanged="AlbaranFacturaChange" onchange="loadingPnl"/>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelCodiBarres" runat="server">
                    <div class="lineIssue issueCodiBarres">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblCodiBarres" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadComboBox ID="DropDownCodiBarres" AutoPostBack="true" runat="server" Width="200px"
                                OnSelectedIndexChanged="DropDownCodiBarres_SelectedIndexChanged" Enabled="false"/>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelComent" runat="server">
                    <div class="lineIssue issueComent">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblComent" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadTextBox ID="TxtAreaComentary" 
                                runat="server" TextMode="MultiLine" 
                                Width="445px" MaxLength="250"
                                Rows="4"
                                onkeyup="return checkLength(this);">
                                <ClientEvents OnLoad="loadTextBox" />                                
                            </telerik:RadTextBox>
                            <div class="remainingText">
                                <asp:Label ID="LblTextRemaining" CssClass="txtIssueMain" runat="server" />
                                <span class="txtIssueMain" id="TextRemaining"></span>
                            </div>   
                        </div>                                            
                    </div>
                </asp:Panel>            
        </asp:Panel>
        
         <div id="DivImage">
                <div  class="lineIssue issuePhotoIssue">
                    <div class="txtIssue">
                        <div class="txtLabel">
                            <asp:Label ID="LblImage" CssClass="txtIssueMain" runat="server" />
                        </div>
                    </div>
                    <div class="inputIssue photoIssueMark">
                        <asp:Image ID="ProductImageAssembly" 
                            CssClass="imgIssue" Width="640px" Height="420px" runat="server" />
                    </div>
                    <asp:Panel runat="server" ID="DeleteMarks" BackImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/borrar flechas.png%>" 

Width="135px" Height="30px" class="DeleteMarks"
                        
                        ><%--<span class="deleteMarks" style="cursor: pointer;">Limpiar</span>--%></asp:Panel>
                </div>
         </div>
            <asp:Panel ID="Panel2" runat="server" CssClass="PanelLoading">
          
                <asp:Panel ID="PanelQty" runat="server">
                    <div class="lineIssue issueQty">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblQty" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="Qty_input">
                            <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                IncrementSettings-InterceptMouseWheel="true" MaxLength="3" runat="server" ID="IssueQty"
                                Width="60px" MinValue="1" MaxValue="999" NumberFormat-DecimalDigits="0" Value='1' Enabled="false"/>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelUploadPhoyo" runat="server">
                    <div class="lineIssue issuePhoto">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblPhoto" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">

                            <telerik:RadAsyncUpload RenderMode="Lightweight" ID="inputPhoto0" runat="server"  
                                allowedfileextensions=".jpg,.jpeg,.gif,.png,.tiff,.avi,.mov,.wmv,.mp3,.mp4,.wma"  
                                ControlObjectsVisibility="AddButton,RemoveButtons"  Localization-Add="" 
                                Localization-Remove="" Localization-Select=""
                                MaxFileInputsCount="4" OverwriteExistingFiles="false" 
                                
                                MaxFileSize="5242880" Skin="Metro" 
                                OnFileUploaded="RadAsyncUpload1_FileUploaded"
                                OnClientValidationFailed="validationFailed" UploadedFilesRendering="BelowFileInput"
                                UseApplicationPoolImpersonation="true"
                                 PostbackTriggers="btEnviar">
                            </telerik:RadAsyncUpload>
                            <div class="qsf-results">
 
                <asp:Panel ID="ValidFiles" Visible="false" runat="server" CssClass="qsf-success">
                    <h3>You successfully uploaded:</h3>
                    <ul class="qsf-list" runat="server" id="ValidFilesList"></ul>
                </asp:Panel>
 
                <asp:Panel ID="InvalidFiles" Visible="false" runat="server" CssClass="qsf-error">
                    <h3>The Upload failed for:</h3>
                    <ul class="qsf-list ruError" runat="server" id="InValidFilesList">
                        <li>
                            <p class="ruErrorMessage">The size of your overall upload exceeded the maximum of 5 MB</p>
                        </li>
                    </ul>
 
                </asp:Panel>
                <telerik:RadButton RenderMode="Lightweight" Skin="Silk" ID="RefreshButton" runat="server" OnClick="RefreshButton_Click" Visible="false" Text="Back" ></telerik:RadButton>
            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelClientReference" runat="server">
                    <div class="lineIssue IssueClientReference">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblClientReference" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadTextBox ID="TextBoxClientReference" runat="server" Width="200px" MaxLength="30">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelShippment" runat="server">
                    <div class="lineIssue IssueShipmentDirection">
                        <div class="txtIssue">
                            <div class="txtLabel">
                                <asp:Label ID="LblShipment" CssClass="txtIssueMain" runat="server" /></div>
                        </div>
                        <div class="inputIssue">
                            <telerik:RadComboBox ID="DropDownShipment" AutoPostBack="false" runat="server" Width="445px" 
                                EnableTextSelection="true" MarkFirstMatch="true" Filter="Contains" />
                        </div>
                    </div>
                </asp:Panel>
           
        </asp:Panel>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <asp:ImageButton ID="btEnviar" runat="server" CssClass="botonEnviarIncidencia" 
            ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/enviar incidencia.png%>" 
            OnClick="btEnviar_Click1"
            onmouseover="this.src=this.src.replace('incidencia.','incidencia_on.');"  
            onmouseout="this.src=this.src.replace('incidencia_on.','incidencia.');"
            Visible="false"
            />
       
    </div>
   
</div>
<div id="loadingPnl" style="display:none;"><img src="/Style Library/Julia/img/loading.gif"  width="66px" height="66px" /></div>