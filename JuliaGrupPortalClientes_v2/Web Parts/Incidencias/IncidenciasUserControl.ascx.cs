﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using Microsoft.SharePoint;
using System.Text;
using System.Web;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;
using Microsoft.Office.DocumentManagement.DocumentSets; 
using Microsoft.SharePoint.Utilities;
using JuliaGrupUtils.ErrorHandler;
using System.Reflection; 
using System.Linq;
using Telerik.Charting;
using System.Globalization;
using System.Web.UI.HtmlControls;

namespace JuliaGrupPortalClientes_v2.Incidencias
{
    public partial class IncidenciasUserControl : UserControl
    {
        #region Fields

        int formID = -1;    //0 - Te camp producte; 1 - No te camp producte
        private const int COORD_POINTS_MARGIN = 5;
        Sesion SesionJulia = new Sesion();
        private string ventaCode = string.Empty;
        private string URL = string.Empty;

        List<Pareto> paretoList;
        private Order CurrentOrder;
        List<IssueProduct> issueProducts;
        List<IssueProduct> BarCodes;
        List<Document> DocumentList;
        DocumentDataAccessObject DocumentDAO;
        private IList<string> CodeUsernameArray = new List<string>();
        User currentUser;
        Client CurrentClient;
        Client ComboClientAux;
        private static Client ComboCLient;
        private static string productImageIssueURL = string.Empty;
        #endregion

        #region Properties

        public string WPTitle { get; set; }
        public string WPSubtitle { get; set; }
        public enum DocumentTypes { Factura, Albaran, Pedido }

        #endregion

        #region PropietosofaddIssue

        private string username = string.Empty;
        private string codClient = string.Empty;
        private string type = string.Empty;
        private string subType = string.Empty;
        private int ObjectID = 0;
        private string documentNo = string.Empty;
        private string itemNo = string.Empty;
        private string barcode = string.Empty;
        private decimal quantity = 0;
        private string RefClient = string.Empty;
        private string Observacioens = string.Empty;
        private string codiDireccionEnvio = string.Empty;

        #endregion

        #region Methods
        //Variables para enviar de alta

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Client cl = (Client)Session["Client"];
                if (this.CurrentOrder == null)
                    this.CurrentOrder = (Order)Session["Order"];

                LiteralControl ltr = new LiteralControl();
                ltr.Text = "<style type=\"text/css\" rel=\"stylesheet\">" +
                        @"div.RadUpload .ruAdd
                    {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/anadir foto.png')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                        cursor:pointer;
                    }
                    div.RadUpload .ruAdd:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/anadir foto_ON.png')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                        cursor:pointer;
                    }
                        div.RadUpload .ruBrowse
                    {
                        background-image:url('/Style%20Library/Julia/img/boto carpeta seleccionar.png')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                        cursor:pointer;
                    }
                    div.RadUpload .ruBrowse:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/boto carpeta seleccionar_ON.png')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                        cursor:pointer;
                    }
                         div.RadUpload .ruRemove,  div.RadUpload .ruRemove:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/cancelbuttonradgrid.gif')!important;
                        background-repeat:no-repeat!important;
                        background-position-x:0px!important;
                        background-position-y:0px!important;
                        cursor:pointer;
                    }
                    
                    .DeleteMarks:hover {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/borrar flechas_on.png')!important;
                    }
                    </style>
                    ";
                this.Page.Header.Controls.Add(ltr);
                if (!IsPostBack)
                {
                    SetInitialValues();
                    defaultView();
                    loadclients();
                }
                else
                {
                    if (formID != -1)
                    {
                        HideFields(formID);
                    }

                    if (productImageIssueURL != string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "showPanelImage", "ShowPanelImage();", true);
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "setImgURL", "setImgURL('" + productImageIssueURL + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetInitialValues()
        {
            SetValues();// Set static values
        }

        private void FillParetoTypes(List<string> TypesDescription, List<string> Types)
        {
            try
            {
                this.DropDownIssueType.Items.Clear();

                var cont = 0;
                foreach (String s in Types)
                {
                    if (TypesDescription[cont] == null)
                    {
                        this.DropDownIssueType.Items.Add(new RadComboBoxItem(s, s));
                    }
                    else
                    {
                        this.DropDownIssueType.Items.Add(new RadComboBoxItem(TypesDescription[cont].ToString(), s));
                    }
                    cont++;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetValues()
        {
            this.lblTitle.Text = this.WPTitle;
            this.lblSubtitle.Text = this.WPSubtitle;
            this.lblNCliente.Text = LanguageManager.GetLocalizedString("IssuesClient");
            this.LblIssuesType.Text = LanguageManager.GetLocalizedString("IssuesType");
            this.LblIssueSubType.Text = LanguageManager.GetLocalizedString("IssuesSubType");
            this.LblDoc.Text = LanguageManager.GetLocalizedString("IssuesDoc");
            this.LblReference.Text = LanguageManager.GetLocalizedString("IssuesReference");
            this.LblCodiBarres.Text = LanguageManager.GetLocalizedString("IssuesCodiBarres");
            this.LblComent.Text = LanguageManager.GetLocalizedString("IssuesComent");
            this.LblTextRemaining.Text = LanguageManager.GetLocalizedString("IssuesTextRemaining");
            this.LblQty.Text = LanguageManager.GetLocalizedString("IssuesQty");
            this.LblPhoto.Text = LanguageManager.GetLocalizedString("IssuesPhoto");
            this.LblClientReference.Text = LanguageManager.GetLocalizedString("IssuesClientReference");
            this.LblShipment.Text = LanguageManager.GetLocalizedString("IssuesShippment");
            this.LblImage.Text = LanguageManager.GetLocalizedString("IssueImage");

            this.DropDownListClient.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageClient");
            this.DropDownIssueType.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageType");
            this.DropDownIssueSubType.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageSubtype");
            this.DropDownDoc.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageDoc");
            this.DropDownReference.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageReference");
            this.DropDownCodiBarres.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageCodiBarres");
            this.TextBoxClientReference.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageClientReference");
            this.DropDownShipment.EmptyMessage = LanguageManager.GetLocalizedString("IssuesEmptyMessageShipment");

            this.AlbaranButton.Text = LanguageManager.GetLocalizedString("AlbaranCombo");
            this.FacturaButton.Text = LanguageManager.GetLocalizedString("FacturaCombo");
            this.PedidoButton.Text = LanguageManager.GetLocalizedString("PedidoCombo");
            this.inputPhoto0.AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".gif", ".png", ".tiff", ".avi", ".mov", ".wmv", ".mp3", ".mp4", ".wma", ".pdf" };
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "style for addbutton", "<style type='text/cs'>div.RadUpload .ruAdd{background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + "/borrar flechas.png')}</style>", false);
            LiteralControl ltr = new LiteralControl();
            ltr.Text = "<style type=\"text/css\" rel=\"stylesheet\">" +
                     @"div.RadUpload .ruAdd
                    {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/anadir foto.png')!important;
                        background-repeat:no-repeat!important;
                        background-position:0px!important;
                      
                        cursor:pointer;
                    }
                    div.RadUpload .ruAdd:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/" + CultureInfo.CurrentUICulture.Name + @"/anadir foto_on.png')!important;
                        background-repeat:no-repeat!important;
                        background-position:0px!important;
                      
                        cursor:pointer;
                    }
                        div.RadUpload .ruBrowse
                    {
                        background-image:url('/Style%20Library/Julia/img/boto carpeta seleccionar.png')!important;
                        background-repeat:no-repeat!important;
                        background-position:0px!important;
                        
                        cursor:pointer;
                    }
                    div.RadUpload .ruBrowse:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/boto carpeta seleccionar_on.png')!important;
                        background-repeat:no-repeat!important;
                        background-position:0px!important;
                        
                        cursor:pointer;
                    }
                   
                         div.RadUpload .ruRemove, div.RadUpload .ruRemove:hover
                    {
                        background-image:url('/Style%20Library/Julia/img/cancelbuttonradgrid.gif')!important;
                        background-repeat:no-repeat!important;
                        background-position:0px!important;
                        
                        cursor:pointer;
                    }
                    </style>
                    ";
            this.Page.Header.Controls.Add(ltr);
        }

        private void defaultView()
        {
            this.PanelDoc.Style.Add("display", "none");
            this.PanelReference.Style.Add("display", "none");
            this.PanelCodiBarres.Style.Add("display", "none");
            this.PanelDoc.Style.Add("display", "none");
            this.PanelComent.Style.Add("display", "none");
            this.PanelQty.Style.Add("display", "none");
            this.PanelUploadPhoyo.Style.Add("display", "none");
            this.PanelClientReference.Style.Add("display", "none");
            this.PanelShippment.Style.Add("display", "none");
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallImgeFunction", "HidePanelImage();", true);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "ResetImgLbl", "ResetImgLbl();", true);
            this.FacturaButton.Checked = true;
            this.AlbaranButton.Checked = false;
        }
        private void loadclients()
        {
            User currentuser = null;
            if (Session["User"] != null)
            {
                currentuser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                CurrentClient = (Client)Session["Client"];
            }
            if (currentuser is Agent)
            {
                Agent a = (Agent)currentuser;

                List<ClientHeader> ClientList = a.ListClientsName;

                foreach (ClientHeader i in a.ListClientsName)
                {
                    this.DropDownListClient.Items.Add(new RadComboBoxItem(i.Name, i.Code));
                }
                this.DropDownListClient.DataSource = a.ListClientsName;
                this.DropDownListClient.DataBind();
                this.DropDownListClient.SelectedValue = CurrentClient.Code;
                ComboCLient = SesionJulia.GetClient(this.DropDownListClient.SelectedValue, currentuser.UserName);
            }
            else if (currentuser is Client)
            {
                this.DropDownListClient.Items.Add(new Telerik.Web.UI.RadComboBoxItem(CurrentClient.Name, currentuser.Code));
                this.DropDownListClient.SelectedValue = currentuser.Code;
                this.DropDownListClient.Enabled = false;
                ComboCLient = SesionJulia.GetClient(this.DropDownListClient.SelectedValue, currentuser.UserName);
            }
            else//ClientGROUP
            {
                ClientGroup a = (ClientGroup)currentuser;

                List<ClientHeader> ClientList = a.ListClientsName;

                foreach (ClientHeader i in a.ListClientsName)
                {
                    this.DropDownListClient.Items.Add(new RadComboBoxItem(i.Name, i.Code));
                }
                this.DropDownListClient.DataSource = a.ListClientsName;
                this.DropDownListClient.DataBind();
                this.DropDownListClient.SelectedValue = CurrentClient.Code;
                ComboCLient = SesionJulia.GetClient(this.DropDownListClient.SelectedValue, currentuser.UserName);
            }
            if (Session["ComboCLientAux"] == null)
                Session.Add("ComboCLientAux", ComboCLient);
                     
            ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(ComboCLient.UserName, ComboCLient.Code);

            FillParetoTypes(paretoDAO.GetparetosTypeDescription(), paretoDAO.GetparetosType());

            loadDirections();
        }
        private void loadDirections()
        {
            this.DropDownShipment.Items.Clear();
            this.DropDownShipment.ClearSelection();
            Client client;
            if (Session["Order"] != null)
            {
                CurrentOrder = (Order)Session["Order"];

            }
            if (Session["User"] != null)
            {
                currentUser = (User)Session["User"];
            }
            try
            {
                ClientDataAccessObject clientDAO = new ClientDataAccessObject();
                if (this.DropDownListClient.SelectedValue != string.Empty)
                    client = clientDAO.GetClientNameByClientCode(this.DropDownListClient.SelectedValue, currentUser.UserName);
                else
                    client = (Client)Session["Client"];

                if (Session["Client"] != null)
                {
                    int i = 0;
                    foreach (string[] addresses in client.Addresses)
                    {
                        RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);
                        this.DropDownShipment.Items.Add(aux);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void DropDownListClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            resetFields(1);
            defaultView();
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (formID == 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallImgeFunction", "MarkImageShow();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallImgeFunction", "MarkImageHide();", true);
            }
            
            ComboCLient = SesionJulia.GetClient(this.DropDownListClient.SelectedValue, currentUser.UserName);
            Session["ComboCLientAux"] = ComboCLient;
            loadDirections();
            //setShipmentAdress
        }

        protected void DropDownIssueType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                resetFields(2);
                defaultView();
                this.type = this.DropDownIssueType.SelectedValue;

                //SetValues();
                if (formID == 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallImgeFunction", "MarkImageShow();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "CallImgeFunction", "MarkImageHide();", true);
                }
                FillParetoSubTypes(this.DropDownIssueType.SelectedValue);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void FillParetoSubTypes(string Type)
        {
            try
            {
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                else
                {
                    this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                    if (this.currentUser == null)
                        throw new Exception("Error NULL en el CurrentUSer");
                    Session.Add("User", currentUser);
                    this.currentUser = (User)Session["User"];
                }
                if (Session["ComboCLientAux"] != null)
                {
                    ComboClientAux = (Client)Session["ComboCLientAux"];
                }
                this.DropDownIssueSubType.Items.Clear();

                //ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
                ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(currentUser.UserName, ComboClientAux.Code);
                List<string> TypeDescription = paretoDAO.GetParetoSubtypeDescription(Type);
                var cont = 0;
                foreach (String s in paretoDAO.GetParetoSubtype(Type))
                {
                    this.DropDownIssueSubType.Items.Add(new RadComboBoxItem(TypeDescription[cont].ToString(), s));
                    cont++;
                }
                if (this.DropDownIssueSubType.Items.Count > 0)
                    this.DropDownIssueSubType.Enabled = true;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected void DropDownIssueSubType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            resetFields(3);
            defaultView();

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }
            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }
            SetValues();

            //ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(currentUser.UserName, ComboClientAux.Code);
            paretoList = paretoDAO.ParetosList;
            formID = paretoDAO.GetFormId(this.DropDownIssueType.SelectedValue, this.DropDownIssueSubType.SelectedValue);
            //SetValues();//Duplicado...
            setRequiredFields(paretoDAO.GetRequiredFields(this.DropDownIssueType.SelectedValue, this.DropDownIssueSubType.SelectedValue));

            HideFields(formID);

            //En función del FormId cargamos documentos o códigos de producto
            if (formID == 0)
            {
                //Carreguem la llista de productes
                loadProducts();
            }
            else
            {
                loadDocuments(DocumentTypes.Factura);   //Debemos pasar el valor seleccionado del radiobutton
            }
            loadDirections();
        }

        protected void AlbaranFacturaChange(object sender, EventArgs e)
        {
            this.DropDownDoc.ClearSelection();
            this.DropDownDoc.Items.Clear();
            this.DropDownCodiBarres.Enabled = false;

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }
            //ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(currentUser.UserName, ComboClientAux.Code);
            formID = paretoDAO.GetFormId(this.DropDownIssueType.SelectedValue, this.DropDownIssueSubType.SelectedValue);
            this.itemNo = this.DropDownReference.SelectedValue;
            DocumentTypes doc = DocumentTypes.Albaran;

            if (this.PedidoButton.Checked)
                doc = DocumentTypes.Pedido;
            else if (this.FacturaButton.Checked)
                doc = DocumentTypes.Factura;

            if (formID == 0)
                loadDocuments(doc, itemNo);
            else
                loadDocuments(doc);
        }

        private void loadDocuments(DocumentTypes choice, string itemNo)
        {
            this.DropDownDoc.ClearSelection();
            this.DropDownDoc.Items.Clear();

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }

            //DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);
            DocumentList = new List<Document>();
            if (choice == DocumentTypes.Albaran)
                DocumentList = DocumentDAO.GetAlvaranByClientandProduct(currentUser.UserName, ComboClientAux.Code, itemNo);
            if (choice == DocumentTypes.Factura)
                DocumentList = DocumentDAO.GetFacturasByClientandProduct(currentUser.UserName, ComboClientAux.Code, itemNo, this.DropDownIssueType.SelectedValue);
            if (choice == DocumentTypes.Pedido)
                DocumentList = DocumentDAO.GetPedidosByClientItem(currentUser.UserName, ComboClientAux.Code, itemNo);
            foreach (Document d in DocumentList)
            {
                this.DropDownDoc.Items.Add(new RadComboBoxItem(d.DocNo, d.DocNo));
            }
            //Aqui deberíamos poner todos los campos que se ponían cuando se seleccionaba producto
            resetFields(7); //Cambiar para que no borre el campo Documento...
        }

        private void loadDocuments(DocumentTypes choice)
        {
            this.DropDownDoc.ClearSelection();
            this.DropDownDoc.Items.Clear();

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);
                
                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }
            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }

            //DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);

            DocumentList = new List<Document>();
            if (choice == DocumentTypes.Albaran)
                //DocumentList = DocumentDAO.GetAlvaranByClient(ComboCLient.UserName, ComboCLient.Code);
                DocumentList = DocumentDAO.GetAlvaranByClient(currentUser.UserName, ComboClientAux.Code);
            if (choice == DocumentTypes.Factura)
                //DocumentList = DocumentDAO.GetFacturasByClient(ComboCLient.UserName, ComboCLient.Code);
                DocumentList = DocumentDAO.GetFacturasByClient(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);
            if (choice == DocumentTypes.Pedido)
                //DocumentList = DocumentDAO.GetPedidosByClient(ComboCLient.UserName, ComboCLient.Code);
                DocumentList = DocumentDAO.GetPedidosByClient(currentUser.UserName, ComboClientAux.Code);
            foreach (Document d in DocumentList)
            {
                this.DropDownDoc.Items.Add(new RadComboBoxItem(d.DocNo, d.DocNo));
            }
            resetFields(4);
        }

        protected void DropDownDoc_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value))
                return;

            resetFields(8);
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else throw new SPException("Error: User session var is Null");

            //loadProducts();
            //Cargamos codigos de barras
            loadBarCodes();
            //Cargamos valores de cantidades y la imagen del documento
            loadProductFromDocument();
        }

        //Load all products for selected client
        protected void loadProducts()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }

            List<IssueProduct> issueProducts = new List<IssueProduct>();
            //JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : IssueProductDataAccessObject(clientUsername = {0}, codCLient = {1})", ComboCLient.UserName, ComboCLient.Code), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
            JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : IssueProductDataAccessObject(clientUsername = {0}, codCLient = {1})", currentUser.UserName, ComboClientAux.Code), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
            //IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(currentUser.UserName, ComboClientAux.Code);
            issueProducts = IssueProductDAO.GetProductsFromDoc(String.Empty);

            List<IssueProduct> issueProductsDistinc = DistinctProducts(issueProducts);

            foreach (IssueProduct product in issueProductsDistinc)
            {
                this.DropDownReference.Items.Add(new RadComboBoxItem(product.ItemNo + " - " + product.ItemDescription, product.ItemNo));
            }
        }

        protected void loadProductsFromDocument()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }

            //DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);
            List<IssueProduct> issueProducts = new List<IssueProduct>();
            int ObjectID = DocumentDAO.GetObjectIDFromDoc(this.DropDownDoc.SelectedValue);

            //IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(ComboCLient.UserName, ObjectID, this.DropDownDoc.SelectedValue);
            IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(currentUser.UserName, ObjectID, this.DropDownDoc.SelectedValue);
            issueProducts = IssueProductDAO.GetProductsFromDoc(this.DropDownDoc.SelectedValue);

            List<IssueProduct> issueProductsDistinc = DistinctProducts(issueProducts);

            foreach (IssueProduct product in issueProductsDistinc)
            {
                this.DropDownReference.Items.Add(new RadComboBoxItem(product.ItemNo + " - " + product.ItemDescription, product.ItemNo));
            }
        }

        private List<IssueProduct> DistinctProducts(List<IssueProduct> issueProducts)
        {
            List<IssueProduct> issueProductsWork = new List<IssueProduct>();

            foreach (IssueProduct product in issueProducts)
            {
                bool repeat = false;
                for (var j = 0; j < issueProductsWork.Count; j++)
                {
                    if (issueProductsWork[j].DocLine == product.DocLine && issueProductsWork[j].ItemNo == product.ItemNo)
                    {
                        repeat = true;
                    }
                }
                if (!(repeat))
                {
                    issueProductsWork.Add(product);
                }
            }
            return issueProductsWork;
        }
        //Aqui cargaremos la lista de documentos en base a: Cliente, producto, tipo documento
        //Al seleccionar un producto
        protected void DropDownReference_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            resetFields(5);
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            //Cargamos la lista de documetos que incluyen este producto
            this.FacturaButton.Checked = true;
            loadDocuments(DocumentTypes.Factura, e.Value);
            this.FacturaButton.Enabled = true;
            this.AlbaranButton.Enabled = true;
            this.PedidoButton.Enabled = true;
            this.DropDownDoc.Enabled = true;
        }

        protected void loadProductFromDocument()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }

            //DocumentDataAccessObject DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            DocumentDataAccessObject DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);

            int ObjectID = DocumentDAO.GetObjectIDFromDoc(this.DropDownDoc.SelectedValue);
            //IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(ComboCLient.UserName, ObjectID, this.DropDownDoc.SelectedValue);
            IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(currentUser.UserName, ObjectID, this.DropDownDoc.SelectedValue);

            if (!String.IsNullOrEmpty(this.DropDownReference.SelectedValue))
            {
                IssueProduct producto = IssueProductDAO.GetProductFromItemNo(this.DropDownReference.SelectedValue);
                this.IssueQty.MaxValue = producto != null ? (Double)producto.CantidadTotal : 999;
                this.IssueQty.Enabled = true;
            }
            else
            {
                this.IssueQty.Enabled = false;
            }

            //ArticleDataAccessObject productsDAO = new ArticleDataAccessObject(ComboCLient.UserName, ComboCLient.Code, 0);
            ArticleDataAccessObject productsDAO = new ArticleDataAccessObject(currentUser.UserName, ComboClientAux.Code, 0);
            string ProductVersion = string.Empty;
            string ProductCode = this.DropDownReference.SelectedValue;
            //CAL FILTRAR PER VERSIOOO
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);

            string productImageIssue = productsDAO.GetProductImagesIssuesByCode(ProductCode, "", "");

            if (!(string.IsNullOrEmpty(productImageIssue)))
            {
                //productImageIssueURL = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + productImageIssue;
                productImageIssueURL = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ImageWeb&Code=" + ProductCode + "&Versio=" + ProductVersion;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "showPanelImage", "ShowPanelImage();", true);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "setImgURL", "setImgURL('" + productImageIssueURL + "');", true);

                //this.ProductImageAssembly.ImageUrl = productImageIssue;
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
            }
        }

        protected void loadBarCodes()
        {
            BarCodes = new List<IssueProduct>();
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
                this.currentUser = (User)Session["User"];
            }

            if (Session["ComboCLientAux"] != null)
            {
                ComboClientAux = (Client)Session["ComboCLientAux"];
            }
            //DocumentDataAccessObject DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
            DocumentDataAccessObject DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);

            int ObjectID = DocumentDAO.GetObjectIDFromDoc(this.DropDownDoc.SelectedValue);
            //IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(ComboCLient.UserName, ObjectID, this.DropDownDoc.SelectedValue);
            IssueProductDataAccessObject IssueProductDAO = new IssueProductDataAccessObject(currentUser.UserName, ObjectID, this.DropDownDoc.SelectedValue);
            issueProducts = IssueProductDAO.GetBarCodesFromProd(this.DropDownReference.SelectedValue);

            foreach (IssueProduct product in issueProducts)
            {
                this.DropDownCodiBarres.Items.Add(new RadComboBoxItem(product.Barcode, product.Barcode));
                this.DropDownShipment.SelectedValue = product.direccionEnvio;
            }
            if (this.DropDownCodiBarres.Items.Count > 0)
            {
                this.DropDownCodiBarres.Enabled = true;
            }
            else
            {
                this.DropDownCodiBarres.Enabled = false;
            }
        }

        protected void DropDownCodiBarres_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            resetFields(6);
        }

        private void HideFields(int FormID)
        {
            try
            {
                if (FormID == 0)//es Incidencia de producte o transport
                {
                    this.PanelReference.Style.Add("display", "block");
                    this.PanelCodiBarres.Style.Add("display", "block");
                    this.PanelDoc.Style.Add("display", "block");
                    this.PanelComent.Style.Add("display", "block");
                    this.PanelQty.Style.Add("display", "block");
                    this.PanelUploadPhoyo.Style.Add("display", "block");
                    this.PanelClientReference.Style.Add("display", "block");
                    this.PanelShippment.Style.Add("display", "block");

                    if (this.DropDownReference.SelectedIndex != -1)
                    {
                        this.DropDownDoc.Enabled = true;
                        this.AlbaranButton.Enabled = true;
                        this.FacturaButton.Enabled = true;
                        this.PedidoButton.Enabled = true;
                    }
                    else
                    {
                        this.DropDownDoc.Enabled = false;
                        this.AlbaranButton.Enabled = false;
                        this.FacturaButton.Enabled = false;
                        this.PedidoButton.Enabled = false;
                    }
                    //SetImaWebURL();
                    //this.PanelImage.Style.Add("display", "block");
                }
                else if (FormID == 1)//es Incidencia de gestió
                {
                    this.PanelReference.Style.Add("display", "none");
                    this.PanelCodiBarres.Style.Add("display", "none");
                    this.PanelDoc.Style.Add("display", "block");
                    this.PanelComent.Style.Add("display", "block");
                    this.PanelQty.Style.Add("display", "none");
                    this.PanelUploadPhoyo.Style.Add("display", "block");
                    this.PanelClientReference.Style.Add("display", "block");
                    this.PanelShippment.Style.Add("display", "none");

                    this.DropDownDoc.Enabled = true;
                    this.AlbaranButton.Enabled = true;
                    this.FacturaButton.Enabled = true;
                    this.PedidoButton.Enabled = true;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    //this.PanelImage.Style.Add("display", "none");
                }
                else
                {
                    this.PanelDoc.Style.Add("display", "none");
                    this.PanelReference.Style.Add("display", "none");
                    this.PanelCodiBarres.Style.Add("display", "none");
                    this.PanelDoc.Style.Add("display", "block");
                    this.PanelComent.Style.Add("display", "block");
                    this.PanelQty.Style.Add("display", "none");
                    this.PanelUploadPhoyo.Style.Add("display", "block");
                    this.PanelClientReference.Style.Add("display", "block");
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    //this.PanelImage.Style.Add("display", "none");
                }
                if (this.DropDownIssueType.SelectedIndex != -1)
                {
                    this.DropDownIssueSubType.Enabled = true;
                }
                if (this.DropDownIssueSubType.SelectedIndex != -1)
                {
                    this.DropDownReference.Enabled = true;
                    btEnviar.Visible = true;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        //private void SetImaWebURL()
        //{
        //    ArticleDataAccessObject productsDAO = new ArticleDataAccessObject(ComboCLient.UserName, ComboCLient.Code);

        //    string ProductCode = this.DropDownReference.SelectedValue;
        //    if (DropDownReference.SelectedValue != "")
        //    {
        //        //CAL FILTRAR PER VERSIOOO
        //        string productImageIssue = productsDAO.GetProductImagesIssuesByCode(ProductCode, "", "");
        //        string productImageIssueURL = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + productImageIssue;
        //        if (!(string.IsNullOrEmpty(productImageIssue)))
        //        {
        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "showPanelImage", "ShowPanelImage();", true);
        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "setImgURL", "setImgURL('" + productImageIssueURL + "');", true);

        //            //this.ProductImageAssembly.ImageUrl = productImageIssue;

        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
        //        }
        //    }

        //}

        public bool GetRequiredFiels()
        {
            bool Valid = true;
            try
            {
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                else
                {
                    this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                    if (this.currentUser == null)
                        throw new Exception("Error NULL en el CurrentUSer");
                    Session.Add("User", currentUser);
                    this.currentUser = (User)Session["User"];
                }

                if (Session["ComboCLientAux"] != null)
                {
                    ComboClientAux = (Client)Session["ComboCLientAux"];
                }

                //ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
                ParetoDataAccessObject paretoDAO = new ParetoDataAccessObject(currentUser.UserName, ComboClientAux.Code);
                Pareto Pareto = paretoDAO.GetRequiredFields(this.DropDownIssueType.SelectedValue, this.DropDownIssueSubType.SelectedValue);

                if (Pareto.Adjunto)
                {
                    if (inputPhoto0.UploadedFiles.Count == 0)
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Descripcion)
                {
                    if (this.TxtAreaComentary.Text == "")
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Direccion)
                {
                    if (this.DropDownShipment.Text == DropDownShipment.EmptyMessage)
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Doc)
                {
                    if (this.DropDownDoc.Text == DropDownDoc.EmptyMessage)
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Matricula)
                {
                    if (this.DropDownCodiBarres.Text == DropDownCodiBarres.EmptyMessage)
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Product)
                {
                    if (this.DropDownReference.Text == DropDownReference.EmptyMessage)
                    {
                        Valid = false;
                    }
                }

                if (Pareto.RefCliente)
                {
                    if (this.TextBoxClientReference.Text == "")
                    {
                        Valid = false;
                    }
                }
                if (Pareto.ImagenDefecto)
                {
                    if (!string.IsNullOrEmpty(this.cont.Text))
                    {
                        if (Convert.ToInt32(this.cont.Text) <= 0)
                        {
                            Valid = false;
                        }
                    }
                    else
                    {
                        Valid = false;
                    }
                }

                if (Pareto.Unidades)
                {

                }

                return Valid;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void setRequiredFields(Pareto Pareto)
        {
            try
            {
                if (Pareto != null)
                {
                    string RequiredClass = " (<span class='RequiredField'>*</span>)";
                    //producto defectos de acabado
                    if (Pareto.Adjunto)
                    {
                        this.LblPhoto.Text += RequiredClass;
                        //this.inputPhoto0.Controls.Add(Validator(inputPhoto0));
                    }

                    if (Pareto.Descripcion)
                    {
                        this.LblComent.Text += RequiredClass;
                        //this.TxtAreaComentary.Controls.Add(Validator(TxtAreaComentary));
                    }

                    if (Pareto.Direccion)
                    {
                        this.LblShipment.Text += RequiredClass;
                        // this.DropDownShipment.Controls.Add(Validator(DropDownShipment));
                    }

                    if (Pareto.Doc)
                    {
                        this.LblDoc.Text += RequiredClass;
                        // this.DropDownDoc.Controls.Add(Validator(DropDownDoc));
                    }

                    if (Pareto.Matricula)
                    {
                        this.LblCodiBarres.Text += RequiredClass;
                        // this.DropDownCodiBarres.Controls.Add(Validator(DropDownCodiBarres));
                    }

                    if (Pareto.Product)
                    {
                        this.LblReference.Text += RequiredClass;
                        //this.DropDownReference.Controls.Add(Validator(DropDownReference));
                    }

                    if (Pareto.RefCliente)
                    {
                        this.LblClientReference.Text += RequiredClass;
                        //this.DropDownReference.Controls.Add(Validator(DropDownReference));
                    }
                    if (Pareto.ImagenDefecto)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "SetRequiredImage", "SetRequiredImage();", true);
                    }
                    if (Pareto.Unidades)
                    {
                        this.LblQty.Text += RequiredClass;
                        this.IssueQty.MinValue = 1;
                        this.IssueQty.Value = 1;
                        // this.IssueQty.Controls.Add(Validator(IssueQty));  
                    }
                    else
                    {
                        this.IssueQty.MinValue = 0;
                        this.IssueQty.Value = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        protected Control Validator(Control ctrl)
        {
            RequiredFieldValidator Validator = new RequiredFieldValidator();
            Validator.Display = ValidatorDisplay.Dynamic;
            Validator.ErrorMessage = "The textbox can not be empty!";
            Validator.EnableClientScript = true;
            Validator.CssClass = "errMsg";
            Validator.ControlToValidate = ctrl.ID;

            return Validator;
        }
        private void resetFields(int FieldNumber)
        {
            switch (FieldNumber)
            {
                case 1:
                    this.DropDownIssueType.ClearSelection();
                    this.DropDownIssueSubType.ClearSelection();
                    this.DropDownDoc.ClearSelection();
                    this.DropDownDoc.Items.Clear();
                    this.DropDownReference.ClearSelection();
                    this.DropDownReference.Items.Clear();
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.IssueQty.Enabled = false;
                    this.TextBoxClientReference.Text = string.Empty;
                    this.DropDownShipment.ClearSelection();
                    this.DropDownShipment.Items.Clear();
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    productImageIssueURL = string.Empty;
                    break;
                case 2:
                    this.DropDownIssueSubType.ClearSelection();
                    this.DropDownDoc.ClearSelection();
                    this.DropDownDoc.Items.Clear();
                    this.DropDownReference.ClearSelection();
                    this.DropDownReference.Items.Clear();
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    productImageIssueURL = string.Empty;
                    break;
                case 3:
                    this.DropDownDoc.ClearSelection();
                    this.DropDownDoc.Items.Clear();
                    this.DropDownReference.ClearSelection();
                    this.DropDownReference.Items.Clear();
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;
                    productImageIssueURL = string.Empty;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    break;
                case 4: //Cambiamos Codigo Documento (Antiguo)
                    productImageIssueURL = string.Empty;
                    this.DropDownReference.ClearSelection();
                    this.DropDownReference.Items.Clear();
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    break;
                case 5: //Cambiamos Código Articulo (Antiguo)
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;
                    this.FacturaButton.Checked = true;
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    break;
                case 6: //Cambiamos Código de barras
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;

                    break;
                case 7: //Cambiamos Código Articulo
                    productImageIssueURL = string.Empty;
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    break;
                case 8: //Cambiamos Codigo Documento
                    productImageIssueURL = string.Empty;
                    this.DropDownCodiBarres.ClearSelection();
                    this.DropDownCodiBarres.Items.Clear();
                    this.IssueQty.Value = 1;
                    this.TextBoxClientReference.Text = string.Empty;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                    break;
            }
        }

        protected void btEnviar_Click1(object sender, EventArgs e)
        {
            try
            {
                bool Validate = true;
                SPUtility.ValidateFormDigest();

                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                else
                {
                    this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                    if (this.currentUser == null)
                        throw new Exception("Error NULL en el CurrentUSer");
                    Session.Add("User", currentUser);
                    this.currentUser = (User)Session["User"];
                }

                if (Session["ComboCLientAux"] != null)
                {
                    ComboClientAux = (Client)Session["ComboCLientAux"];
                }
                //DocumentDAO = new DocumentDataAccessObject(ComboCLient.UserName, ComboCLient.Code);
                DocumentDAO = new DocumentDataAccessObject(currentUser.UserName, ComboClientAux.Code, this.DropDownIssueType.SelectedValue);

                Validate = GetRequiredFiels();
                if (Validate == true)
                {
                    this.username = this.currentUser.UserName;
                    this.codClient = this.DropDownListClient.SelectedValue;
                    this.type = this.DropDownIssueType.SelectedValue;
                    this.subType = this.DropDownIssueSubType.SelectedValue;
                    if (this.DropDownDoc.SelectedValue != "")
                    {
                        this.ObjectID = Convert.ToInt32(DocumentDAO.GetObjectIDFromDoc(this.DropDownDoc.SelectedValue));
                        this.documentNo = this.DropDownDoc.SelectedValue;
                    }
                    else
                    {
                        NavException ex = new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueNoDoc"));
                        throw ex;
                    }
                    this.itemNo = this.DropDownReference.SelectedValue;
                    this.barcode = this.DropDownCodiBarres.SelectedValue;
                    //CHeck if is obligatory

                    this.quantity = Convert.ToDecimal(this.IssueQty.Value);

                    this.Observacioens = this.TxtAreaComentary.Text;
                    this.RefClient = this.TextBoxClientReference.Text;
                    this.codiDireccionEnvio = this.DropDownShipment.SelectedValue;
                }
                else
                {
                    NavException ex = new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueRequiredFields"));
                    throw ex;
                }

                

                IssueDataAccessObject issueDAO = new IssueDataAccessObject();
                String CodIssue = issueDAO.AddIssue(this.username, this.codClient, this.type, this.subType, this.ObjectID, this.documentNo, this.itemNo, this.barcode, this.quantity, this.RefClient, this.Observacioens, this.codiDireccionEnvio);
                


                // get the image to mar

                //ArticleDataAccessObject productsDAO = new ArticleDataAccessObject(ComboCLient.UserName, ComboCLient.Code, 0);
                ArticleDataAccessObject productsDAO = new ArticleDataAccessObject(currentUser.UserName, ComboClientAux.Code, 0);
                string ProductCode = this.DropDownReference.SelectedValue;
                //CAL FILTRAR PER VERSIOOO
                string productImageIssue = productsDAO.GetProductImagesIssuesByCode(ProductCode, "", "");
                string productImageIssueURL = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + productImageIssue;

                //string urlParent = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + "/Style%20Library/julia/images/logo.jpg";
                string urlMark = "/Style%20Library/Julia/img/Mark.png";
                String sharePointSite = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + "/Procesos/Incidencias/IssuesDocs/Forms/AllItems.aspx";
                String documentLibraryName = "IssuesDocs";

                if (!(string.IsNullOrEmpty(this.cont.Text)))
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                   {
                       using (SPSite site = new SPSite(productImageIssueURL))
                       {
                           using (SPWeb web = site.OpenWeb())
                           {
                               int cont = Convert.ToInt32(this.cont.Text);
                               List<int> coordsX = new List<int>();
                               List<int> coordsY = new List<int>();
                               int coordX0 = string.IsNullOrEmpty(this.CoordX0.Text) ? -1 : GetIntWithoutDecimals(this.CoordX0.Text);
                               //Convert.ToInt32(this.CoordX0.Text.IndexOf('.'));
                               int coordX1 = string.IsNullOrEmpty(this.CoordX1.Text) ? -1 : GetIntWithoutDecimals(this.CoordX1.Text);
                               int coordX2 = string.IsNullOrEmpty(this.CoordX2.Text) ? -1 : GetIntWithoutDecimals(this.CoordX2.Text);
                               int coordX3 = string.IsNullOrEmpty(this.CoordX3.Text) ? -1 : GetIntWithoutDecimals(this.CoordX3.Text);
                               int coordY0 = string.IsNullOrEmpty(this.CoordY0.Text) ? -1 : GetIntWithoutDecimals(this.CoordY0.Text);
                               int coordY1 = string.IsNullOrEmpty(this.CoordY1.Text) ? -1 : GetIntWithoutDecimals(this.CoordY1.Text);
                               int coordY2 = string.IsNullOrEmpty(this.CoordY2.Text) ? -1 : GetIntWithoutDecimals(this.CoordY2.Text);
                               int coordY3 = string.IsNullOrEmpty(this.CoordY3.Text) ? -1 : GetIntWithoutDecimals(this.CoordY3.Text);

                               if (coordX0 != -1)
                               {
                                   coordsX.Add(coordX0);
                               }
                               if (coordX1 != -1)
                               {
                                   coordsX.Add(coordX1);
                               }
                               if (coordX2 != -1)
                               {
                                   coordsX.Add(coordX2);
                               }
                               if (coordX3 != -1)
                               {
                                   coordsX.Add(coordX3);
                               }
                               if (coordY0 != -1)
                               {
                                   coordsY.Add(coordY0);
                               }
                               if (coordY1 != -1)
                               {
                                   coordsY.Add(coordY1);
                               }
                               if (coordY2 != -1)
                               {
                                   coordsY.Add(coordY2);
                               }
                               if (coordY3 != -1)
                               {
                                   coordsY.Add(coordY3);
                               }

                               SPFile file2;
                               Stream stfile2;
                               System.Drawing.Image marks;

                               SPFile fileOrigin = web.GetFile(productImageIssueURL);
                               SPFile file = fileOrigin;
                               Stream stfile = file.OpenBinaryStream();
                               System.Drawing.Image imgParent = System.Drawing.Image.FromStream(stfile);
                               SPSecurity.RunWithElevatedPrivileges(delegate ()
                               {
                                   using (SPSite siteMarkImage = new SPSite(JuliaGrupUtils.Utils.ConstantManager.ExtranetURL))
                                   {
                                       using (SPWeb web2 = siteMarkImage.RootWeb)
                                       {
                                           file2 = web2.GetFile(urlMark);
                                           stfile2 = file2.OpenBinaryStream();
                                           marks = System.Drawing.Image.FromStream(stfile2);
                                           Graphics g = Graphics.FromImage(imgParent);
                                           for (int i = 0; i <= cont; i++)
                                           {
                                               g.DrawImage(marks, new Point(Convert.ToInt32(coordsX[i]), Convert.ToInt32(coordsY[i])));
                                           }
                                       }
                                   }
                               });

                               //Save Image to SHarepoint 
                               using (SPSite oSite = new SPSite(sharePointSite))
                               {
                                   using (SPWeb oWeb = oSite.OpenWeb())
                                   {
                                       //get the library
                                       SPDocumentLibrary milista = (SPDocumentLibrary)oWeb.Lists[documentLibraryName];
                                       oWeb.AllowUnsafeUpdates = true;
                                       //Save the image
                                       var stream = new System.IO.MemoryStream();
                                       string filename = CodIssue + "_" + "Marcada.jpg";
                                       MemoryStream ms = new MemoryStream();
                                       imgParent.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                       byte[] ImageByte = ms.ToArray();

                                       //Create document set                         
                                       SPContentType docsetCT = milista.ContentTypes["Document Set"];
                                       Hashtable properties = new Hashtable();
                                       properties.Add("DocumentSetDescription", "New Document Set");
                                       SPFolder parentFolder = milista.RootFolder;
                                       DocumentSet docSet = null;


                                       //--> 2019-06-04 - Aida Lucha - Simplificar procés guardar imatges Incidències
                                       SPFolder folder = oWeb.GetFolder(CodIssue);
                                       if (folder.Exists == true)
                                       {
                                           docSet = DocumentSet.GetDocumentSet(folder);
                                       }
                                       else
                                       {
                                           docSet = DocumentSet.Create(parentFolder, CodIssue, SPContentTypeId.Empty, null, true);
                                       }
                                       
                                       //<--ALB - 20/03/2019
                                       
                                       SPFile spfile = milista.RootFolder.Files.Add("/Procesos/Incidencias/IssuesDocs/" + CodIssue + "/" + filename, ImageByte, true);
                                       milista.Update();

                                       //Move the document created to dcoumentset                               
                                       web.Update();

                                       oWeb.AllowUnsafeUpdates = false;
                                   }
                               }
                           }
                       }
                   });
                }

                if ((this.inputPhoto0.UploadedFiles.Count != 0) || (string.IsNullOrEmpty(CodIssue)))
                    uploadPhoto(CodIssue);




                ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueOK1") + ": " + CodIssue + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueOK2") + "', 700, 180,'Incidencia Enviada', alertCallBackFn); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);

                resetFields(1);
                productImageIssueURL = string.Empty;
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "EndIssue", "IssueOK('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueOK") + "');", true);
            }

            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Client RadAlert'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "HidePanelImage", "HidePanelImage();", true);
            }
        }

        const int MaxTotalBytes = 5242880; // 5 MB
        long totalBytes;

        public void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            var liItem = new HtmlGenericControl("li");
            liItem.InnerText = e.File.FileName;

            if (totalBytes < MaxTotalBytes)
            {
                // Total bytes limit has not been reached, accept the file
                e.IsValid = true;
                totalBytes += e.File.ContentLength;
            }
            else
            {
                // Limit reached, discard the file
                e.IsValid = false;
            }

            if (e.IsValid)
            {
                ValidFiles.Visible = true;
                ValidFilesList.Controls.AddAt(0, liItem);
            }
            else
            {
                InvalidFiles.Visible = true;
                InValidFilesList.Controls.AddAt(0, liItem);
            }
        }

        protected void RefreshButton_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect(Request.RawUrl);
        }

        private int GetIntWithoutDecimals(string Coord)
        {
            Int32 output;
            if (Coord.Contains("."))
            {
                var Position = Coord.IndexOf(".");
                Coord = Coord.Substring(0, Position);
                output = Convert.ToInt32(Coord);
            }
            else
            {
                output = Convert.ToInt32(Coord);
            }

            return output;
        }

        private void uploadPhoto(string codeIssue)
        {
            String sharePointSite = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + "/Procesos/Incidencias/IssuesDocs/Forms/AllItems.aspx";
            String documentLibraryName = "IssuesDocs";

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(sharePointSite))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPDocumentLibrary milista = (SPDocumentLibrary)oWeb.Lists[documentLibraryName];
                        oWeb.AllowUnsafeUpdates = true;
                        foreach (UploadedFile UploadFile in inputPhoto0.UploadedFiles)
                        {
                            UploadedFile UploadFiles = UploadFile;
                            Stream fs = UploadFiles.InputStream;
                            SPFolder parentFolder = milista.RootFolder;
                            DocumentSet docSet = null;
                            
                            bool exist = false;
                            SPFolderCollection foldercol = parentFolder.SubFolders;
                            foreach (SPFolder fold in foldercol)
                            {
                                if (fold.Name == codeIssue)
                                {
                                    exist = true;
                                    docSet = DocumentSet.GetDocumentSet(fold);
                                }
                            }
                            if (!exist)
                            {
                                docSet = DocumentSet.Create(parentFolder, codeIssue, SPContentTypeId.Empty, null, true);
                            }

                            milista.RootFolder.Files.Add("/Procesos/Incidencias/IssuesDocs/" + codeIssue + "/" + UploadFile.GetName(), fs, true);
                            milista.Update();
                            oWeb.Update();
                        }
                        oWeb.AllowUnsafeUpdates = false;
                    }
                }
            });
        }

        private void uploadPhoto2(string codeIssue)
        {
            String sharePointSite = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + "/Procesos/Incidencias/IssuesDocs/Forms/AllItems.aspx";
            String documentLibraryName = "IssuesDocs";

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(sharePointSite))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPDocumentLibrary milista = (SPDocumentLibrary)oWeb.Lists[documentLibraryName];
                        oWeb.AllowUnsafeUpdates = true;

                        DocumentSet docSet = null;
                        SPFolder parentFolder = milista.RootFolder;
                        SPFolder folder = oWeb.GetFolder(codeIssue);
                       
                        if (folder.Exists == true)
                        {
                            docSet = DocumentSet.GetDocumentSet(folder);
                        }
                        else
                        {
                            docSet = DocumentSet.Create(parentFolder, codeIssue, SPContentTypeId.Empty, null, true);
                        }

                                                

                        foreach (UploadedFile UploadFile in inputPhoto0.UploadedFiles)
                        {
                            UploadedFile UploadFiles = UploadFile;
                            Stream fs = UploadFiles.InputStream;
                          
                            milista.RootFolder.Files.Add("/Procesos/Incidencias/IssuesDocs/" + codeIssue + "/" + UploadFile.GetName(), fs, true);
                            milista.Update();
                            oWeb.Update();
                        }
                        oWeb.AllowUnsafeUpdates = false;
                    }
                }
            });
        }

        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                string doc = string.Empty;
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                else
                {
                    this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                    if (this.currentUser == null)
                        throw new Exception("Error NULL en el CurrentUSer");
                    Session.Add("User", currentUser);
                    this.currentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    this.CurrentClient = (Client)Session["Client"];
                }

                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ofertapdf&ClientCode=" + currentUser.UserName + "&ofertano=" + CurrentOrder.NavOferta.No + "&selector=" + Session["PriceSelector"].ToString() + "&TipoCompra=0";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return output;
        }
    }
}

#endregion




