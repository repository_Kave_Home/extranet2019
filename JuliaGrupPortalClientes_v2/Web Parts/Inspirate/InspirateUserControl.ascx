﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InspirateUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.Inspirate.InspirateUserControl" %>
<%-- script type="text/javascript">
    $(document).ready(function () {
        // configuracion personalizada
        $("#thumbs").carouFredSel({
            circular: true,
            items: 1,
            direction: "left",
            auto: {
                play: true,
                delay: 3000
            },
            next: {
                button: "#foo2_next",
                items: 1,
                fx: "crossfade",
                easing: "linear",
                duration: 200,
                pauseOnHover: true
            },
            prev: {
                button: "#foo2_prev",
                items: 1,
                fx: "crossfade",
                easing: "linear",
                duration: 200,
                pauseOnHover: true
            }
        });
    });
</script>
<style type="text/css">
a.prev_Inpirate, a.next_Inpirate {
    width: 29px;
    height: 47px;
    display: block;
	position:absolute;
    border: none;
    top:45%;
}
a.next_Inpirate{
	right:0px;
    background: url('/Style%20Library/Julia/img/next.png') no-repeat transparent;
}
a.prev_Inpirate{
	
    background: url('/Style%20Library/Julia/img/previous.png') no-repeat transparent;
}
.CarouselMainBox
{
 height:200px;   
    }

</style>
 <link href="/Style%20Library/Julia/Slide.css" rel="stylesheet" type="text/css" / --%>
<%-- div class="sliderTitle" >
<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TitleInspirate")%>
</div --%>
<%-- div class="CarouselMainBox">
  <img src="/Style Library/Julia/img/Inspirate.png" style="border:0;width: 96%;" />
     <div class="image_carousel">
        <div id="thumbs">
            <asp:Literal ID="listItems" runat="server"></asp:Literal>
        </div>
        <div class="clearfix">
        </div>
        <a class="prev_Inpirate" id="foo2_prev" href="#"></a> 
        <a class="next_Inpirate" id="foo2_next" href="#"></a>
        <div class="pagination" id="foo2_pag">
        </div>
    </div>
</div --%>
<div style="float:right;">
    <div class="headerText" style="padding-bottom: 9px;">
    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TitleLiquidaciones")%>
    </div>
    <div style="margin-right: 25px;">
        <asp:HyperLink runat="server" Visible="false" NavigateUrl="/Pages/RedirectTo.aspx?r=products&catalog=LDORSUIT" ID="LDORSUIT">
            <asp:Image CssClass="headerText" ID="ImageLDORSUIT" runat="server" ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/Specialoffers_dorsuit.png%>" />
        </asp:HyperLink>
        <asp:HyperLink runat="server" Visible="false" NavigateUrl="/Pages/RedirectTo.aspx?r=products&catalog=LLF" ID="LLF">
            <asp:Image CssClass="headerText" ID="ImageLLF" runat="server" ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/Specialoffers_laforma.png%>" />
        </asp:HyperLink>
    </div>
</div>