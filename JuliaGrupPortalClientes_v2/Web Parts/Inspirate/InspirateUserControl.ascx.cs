﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;

namespace JuliaGrupPortalClientes_v2.Web_Parts.Inspirate
{
    public partial class InspirateUserControl : UserControl
    {
        private User currentUser;
        private ArticleDataAccessObject articleDAO;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }

                    //if (this.currentUser is Client)
                    //{
                    //    Session["Client"] = this.currentUser;
                    //}
                //loadImages();

                if (Session["ArticleDAO"] != null)
                {
                    this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                }

                if (!IsPostBack)
                {
                    GetClientBusinessLine();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Utils.Logger.WiteLog("ArticlesCatalogUserControl->" + ex.Message, ex.StackTrace);
            }
        }

        private void loadImages()
        {
            try
            {
              //  SPSecurity.RunWithElevatedPrivileges(delegate()
              //{
              //    using (SPSite site = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/Documentacin%20Inspirate/Forms/AllItems.aspx"))
              //    {

              //        using (SPWeb web = site.OpenWeb())
              //        {


              //            if (Session["Client"] != null)
              //            {
              //                this.currentUser = (Client)Session["Client"];
              //            }
              //            SPWeb cweb = SPContext.Current.Web;
              //            string siteurl = cweb.Url.ToString();

              //            // Get data from a list.
              //            string listUrl = JuliaGrupUtils.Utils.ConstantManager.IntranetURL + JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/Documentacin%20Inspirate/Forms/AllItems.aspx";
              //            SPList list = web.GetList(listUrl);


              //            string text = string.Empty;

              //            foreach (SPItem item in list.Items)
              //            {
              //                text += "<a href='/Pages/Inspirate%20Pages/"  + item["Name"].ToString().Replace(".jpg","") + ".aspx'><img src=\"" + web.Url + "/Documentacin Inspirate/" + item["Name"] + "\" width='250px'/> </a>";
              //            }
              //            this.listItems.Text = text;
              //        }

              //    }
              //});

            }
            catch (Exception ex)
            {

            }


            
        }
        private void GetClientBusinessLine()
        {
            //List<Article> collection = this.articleDAO.GetAllProducts();
            //var BLLaforma = (from p in collection
            //                 where p.BusinessLine == "LF"
            //                 select p.BusinessLine).Count();
            try
            {
                List<Article> collectionLLF = this.articleDAO.GetCatalogProducts("LLF");   //this.articleDAO.GetAllProducts();
                var BLLaforma = (from p in collectionLLF select p.CatalogNo).Count();

                List<Article> collectionLDORSUIT = this.articleDAO.GetCatalogProducts("LDORSUIT");   //this.articleDAO.GetAllProducts();
                var BLDorsuit = (from p in collectionLDORSUIT select p.CatalogNo).Count();

                //List<Article> articles = Session["Articles"] as List<Article>;
                //if (articles != null)
                //{
                //    ////load catalogs
                //    //var catalogs = (from p in articles
                //    //                orderby p.Catalog
                //    //                select p.Catalog).Distinct();


                //var BLDorsuit = (from p in collection
                //                 where p.BusinessLine == "DORSUIT"
                //                 select p.BusinessLine).Count();
                if ((BLLaforma > 0) && (BLDorsuit > 0))
                {
                    //Cabecera 2 logos
                    this.LDORSUIT.Visible = true;
                    this.LLF.Visible = true;
                }
                else if (BLLaforma > 0)
                {
                    this.LDORSUIT.Visible = false;
                    this.LLF.Visible = true;
                }
                else if (BLDorsuit > 0)
                {
                    this.LDORSUIT.Visible = true;
                    this.LLF.Visible = false;
                }
            } 
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        
    }
}
