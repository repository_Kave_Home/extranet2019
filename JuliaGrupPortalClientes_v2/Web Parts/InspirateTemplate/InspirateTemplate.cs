﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace JuliaGrupPortalClientes_v2.Web_Parts.InspirateTemplate.ascx
{
    [ToolboxItemAttribute(false)]
    public class InspirateTemplate : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2.Web_Parts/InspirateTemplate/InspirateTemplateUserControl.ascx";

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Biblioteca Imágenes Inspirate")]
        [System.ComponentModel.Category("Custom")]
        public string InspirateList { get; set; }

        protected override void CreateChildControls()
        {
            InspirateTemplateUserControl control = Page.LoadControl(_ascxPath) as InspirateTemplateUserControl;

            control.InspirateList = this.InspirateList;

            Controls.Add(control);
        }
    }
}
