﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InspirateTemplateUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.InspirateTemplate.ascx.InspirateTemplateUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>

<script src="/Style%20Library/Julia/js/jquery.carouFredSel-5.6.1-packed.js" type="text/javascript"></script>

<style type="text/css">
    #<%= this.SliderID %> {
        overflow: hidden;
    }
</style>

 <script type="text/javascript">

     function initCarousel<%= this.SliderID %>() {
        jQuery("#<%= this.SliderID %>").carouFredSel({
            circular: true,
            auto: true,
            align: "center",
            scroll: {
                items: 1,
                duration        : 2000,                         
                pauseOnHover    : true
            },
            prev: {
                button: "#<%= this.SliderID %>_prev",
                key: "left"
            },
            next: {
                button: "#<%= this.SliderID %>_next",
                key: "right"
            },
            pagination: "#<%= this.SliderID %>_pager"
        });
         $('.sliderCarousel').css("visibility", "visible");
         
    }
    jQuery(document).ready(function () {
        /*SP.SOD.executeOrDelayUntilScriptLoaded(initCarousel<%= this.SliderID %>, "sp.js");*/
        initCarousel<%= this.SliderID %>();
    });

     function OpenProductInfoDialog1(code, version, catalog) {

         var options = {
             url: "/Pages/ProductInfo.aspx?code=" + code + "&version=" + version + "&catalog=" + catalog,
             width: 900,
             height: 600,

             title: '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductInfo") %>',
             dialogReturnValueCallback: DialogCallback1
         };

         SP.UI.ModalDialog.showModalDialog(options);
     }


     function DialogCallback1(dialogResult, returnValue) {
         if (dialogResult == SP.UI.DialogResult.OK) {
             RefreshTable();
             var notification = $find("<%= RadNotification1.ClientID %>");
             var message = '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct")%>';
             notification.set_text(message);
             notification.show();

         }
     }

     function BuyAllCallBack(dialogResult, returnValue) {
         if (dialogResult == true) {
             RefreshTable();
             var notification = $find("<%= RadNotification1.ClientID %>");
             var message = '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct")%>';
             notification.set_text(message);
             notification.show();
         }
     }

     function BuyAll_ClientClick() {
         $.blockUI({ message: $('#loadingPnl') });
     }

    </script>
    <asp:Panel ID="Pnlmain" runat="server">
    <%-- <div class="sliderCarousel" style="visibility:hidden;width:940px;margin:auto;"> --%>
    <div class="sliderCarousel" style="visibility:hidden;width:1340px;margin:auto;padding:0;">
<div class="anythingBase" id="<%= this.SliderID %>">
    <asp:Repeater ID="InspiratesRepeater" runat="server">
        <ItemTemplate>
        <%-- kavehome 1364 x 546 --%>
            <%--<div class='panel' id='<%# Eval("CodigoInspirate") %>' style="width:940px;height:448px;">--%>
            <div class='panel' id='<%# Eval("CodigoInspirate") %>' style="width:1340px;height:546px;">
                <asp:HiddenField ID="hiddenInspirateCode"   runat="server"   Value='<%# Eval("CodigoInspirate") %>' />  
                <asp:HiddenField ID="HiddenInspirateProducts"   runat="server"   Value='<%# Eval("ProductsInspirate") %>' />  
                <%-- Template for Banner Link & Banner Filtro --%>
                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("UrlBanner") %>' Visible='<%# Eval("isNotInspirate") %>'>
                    <%--<asp:Image ID="InspirateImageRptrBannerLink" CssClass="inspirateimage" runat="server" 
                    ImageUrl='<%# Eval("ImageUrlInspirate")%>' Width="940px" />--%>
                    <asp:Image ID="InspirateImageRptrBannerLink" CssClass="inspirateimage" runat="server" 
                    ImageUrl='<%# Eval("ImageUrlInspirate")%>' Width="1340px" />
                </asp:HyperLink>

                <%-- Template for Inspirate --%>
                <%--<asp:Image ID="InspirateImageRptr" CssClass="inspirateimage" runat="server" 
                    ImageUrl='<%# Eval("ImageUrlInspirate")%>' Width="940px" Visible='<%# Eval("isInspirate") %>' />--%>
                    <asp:Image ID="InspirateImageRptr" CssClass="inspirateimage" runat="server" 
                    ImageUrl='<%# Eval("ImageUrlInspirate")%>' Width="1340px" Visible='<%# Eval("isInspirate") %>' />
                
                <asp:Repeater ID="articlesInspirate" runat="server">
                    <ItemTemplate>
                            <a style='top: <%# Eval("Top") %>px; left: <%# Eval("Left") %>px' class='piece'
                                href='<%# Eval("NavigateUrl") %>'
                                <span class='title'><%# Eval("ArticleDescription")%></span> 
                                <span class='price'><%# Eval("ArticlePrice")%></span> 
                            </a>     
                    </ItemTemplate>
                </asp:Repeater>
                <asp:ImageButton runat="server" id="BuyAllBut"  Visible='<%# Eval("isInspirate") %>' 
                    ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/inspirate_add_to_car.gif%>" 
                    CssClass="InspirateimgProductAdd" BorderStyle="None" BackColor="Transparent" 
                    onclick="BuyAllBut_Click"  OnClientClick="javascript:BuyAll_ClientClick()"
                    /> 
            </div>
        </ItemTemplate>
    </asp:Repeater>    
</div>
    <div class="clearfix"></div>
    <div class="inspager" id="<%= this.SliderID %>_pager"></div>
    <a class="prev prev_Inspirate" id="<%= this.SliderID %>_prev" href="#"></a>
    <a class="next next_Inspirate" id="<%= this.SliderID %>_next" href="#"></a>
</div>
</asp:Panel>
<asp:Panel ID="PnlErr" runat="server" Visible="false">
    <asp:Literal ID="LitError" runat="server" />
</asp:Panel>
    <telerik:RadNotification ID="RadNotification1" runat="server"  EnableRoundedCorners="true" AutoCloseDelay="3000" OffsetY="-100" 
                                    EnableShadow="true"  Position="MiddleLeft"  ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"  
                                  Skin="Silk" Text="Producto Añadido" ></telerik:RadNotification>
