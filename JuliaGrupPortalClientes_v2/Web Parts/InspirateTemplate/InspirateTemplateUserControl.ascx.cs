﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;

using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.Data;
using JuliaGrupUtils.ErrorHandler;
using Telerik.Web.UI;
using JuliaGrupPortalClientes_v2.Web_Parts.Carrito;
using System.Globalization;
using System.Web;




namespace JuliaGrupPortalClientes_v2.Web_Parts.InspirateTemplate.ascx
{
    public partial class InspirateTemplateUserControl : UserControl
    {
        private string sliderID = "slider";
        private User currentUser;
        //private ArticleDataAccessObject articleDAO;
        private string inspiratelist;
        string codCatalog;
        private Client CurrentClient;
        private ArticleDataAccessObject articleDAO;
        DataTable inspirates;
        DataTable articles;

        public string InspirateList
        {
            get { return inspiratelist; }
            set { inspiratelist = value; }
        }
        public string SliderID
        {
            get { return sliderID; }
            set { sliderID = value; }
        }
        public string currentUserName
        {
            get { return currentUser.UserName; }
        }
        public string CurrentClientCode
        {
            get { return CurrentClient.Code; }
        }

        private void loaduserandclient()
        {
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                this.CurrentClient = (Client)Session["Client"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                loaduserandclient();
                if (!Page.IsPostBack)
                {
                    this.sliderID = this.Parent.ID.Replace("$", "");
                    inspirates = new DataTable();
                    inspirates.Columns.Add("CodigoInspirate");
                    inspirates.Columns.Add("ImageUrlInspirate");
                    inspirates.Columns.Add("ProductsInspirate");

                    //inspirates.Columns.Add("ContentType");
                    inspirates.Columns.Add("UrlBanner");
                    //inspirates.Columns.Add("FiltroCatalogo");
                    //inspirates.Columns.Add("FiltroFamilia");
                    //inspirates.Columns.Add("FiltroSubFamilia");
                    //inspirates.Columns.Add("FiltroPrograma");
                    //inspirates.Columns.Add("FiltroTextoLibre");

                    inspirates.Columns.Add("isInspirate", typeof(bool));
                    inspirates.Columns.Add("isNotInspirate", typeof(bool));



                    codCatalog = Request.QueryString["catalog"];
                    //loadarticles(codCatalog);

                    if (Session["ArticleDAO"] != null)
                    {
                        this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                    }
                    else
                    {
                        string currentUser;

                        if (Session["User"] != null)
                            currentUser = (Session["User"] as User).UserName;
                        else
                            return;

                        Client cl = (Client)Session["Client"];
                        if (cl == null)
                            return;
                        this.articleDAO = new ArticleDataAccessObject(currentUser, cl.Code, 0);
                        Session.Add("ArticleDAO", this.articleDAO);
                    }

                    loadInspirates(codCatalog);
                }
            }
            catch (Exception ex)
            {
                PnlErr.Visible = true;
                Pnlmain.Visible = false;
                LitError.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Inspirate_Main_Error_Msg");
            }
        }

        private void loadInspirates(string code)
        {
            try
            {
                if (HttpContext.Current.Cache.Get("Inspirates" + currentUser.UserName + CurrentClient.Code) != null) {
                    inspirates = (DataTable)HttpContext.Current.Cache.Get("Inspirates" + currentUser.UserName + CurrentClient.Code);
                }
                else
                {
                    List<string> Inspirates = this.articleDAO.GetCatalogsCode("Inspirate");

                    string catalogcode = this.Request.QueryString["catalog"];

                    int size = Inspirates.Count;
                    //if (size == 0)
                    //{
                    //    //SetInspirateImageDefault();
                    //    throw new Exception("Inspirate no encontrado");
                    //}
                    //else
                    //{
                    if (size > 0)
                    {
                        //Si nos han pasado item para posicionarnos lo colocamos en la primera posicion a tratar (ultima)
                        if (!string.IsNullOrEmpty(catalogcode))
                        {
                            var index = Inspirates.FindIndex(x => x == catalogcode);
                            if (index > -1)
                            {   //Si encontramos el inspirate pasado como parámetro reordenamos la lista para mostrarlo en primera posicion
                                var item = Inspirates[index];
                                Inspirates[index] = Inspirates[size - 1];
                                Inspirates[size - 1] = item;
                            }
                            else
                            {
                                throw new Exception("Inspirate no encontrado");
                            }
                        }
                    }
                        //REZ 20032014 - Lo substituyo por una unica llamada. No se si soportara ordenación... Pero en la home no es necesario
                        //while (size > 0)
                        //{
                        //    size--;

                        //    String Inscode = Inspirates[size];
                        //    try
                        //    {
                        //        GetInspirateData(Inscode);
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        //REZ 19032014 - No muestro mensaje de error ya que está en la home...
                        //        ////Si no encuentra la información de alguno de los inspirates mostramos los que si encuentra pero ademas el mensaje de error
                        //        //PnlErr.Visible = true;
                        //        //LitError.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Inspirate_Main_Error_Msg");
                        //    }
                        //}

                        try
                        {
                            GetInspirateDataAll(Inspirates);
                            HttpContext.Current.Cache.Add("Inspirates" + currentUser.UserName + CurrentClient.Code, inspirates, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                        }
                        catch (Exception ex)
                        {
                            //REZ 19032014 - No muestro mensaje de error ya que está en la home...
                            ////Si no encuentra la información de alguno de los inspirates mostramos los que si encuentra pero ademas el mensaje de error
                            //PnlErr.Visible = true;
                            //LitError.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Inspirate_Main_Error_Msg");
                        }


                    //}
                }

                    this.InspiratesRepeater.DataSource = inspirates;
                    this.InspiratesRepeater.DataBind();

                    foreach (RepeaterItem repeaterItem in InspiratesRepeater.Items)
                    {
                        LoadRepeaterArticles(repeaterItem);
                    }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        
        //void InspiratesRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        void LoadRepeaterArticles(RepeaterItem item)
        {
            //RepeaterItem item = e.Item;
            if ((item.ItemType == ListItemType.Item) ||
                (item.ItemType == ListItemType.AlternatingItem))
            {
                Repeater articlesInspirate = (Repeater)item.FindControl("articlesInspirate");
                //DataRowView drv = (DataRowView)item.DataItem;
                //articlesInspirate.DataSource = drv.CreateChildView("Inspirate_Items");
                 
                //loadarticles(((HiddenField)item.FindControl( "hiddenInspirateCode" )).Value);
                try
                {
                    AddInspirateArticles(((HiddenField)item.FindControl("HiddenInspirateProducts")).Value);
                    articlesInspirate.DataSource = articles;
                    articlesInspirate.DataBind();
                }
                catch (Exception ex) {
                    //Si no puede cargar los artículos no mostramos el elemento
                    item.Visible = false;
                }

            }
        }

        //REZ30122014 - Filtrar por idioma y añadir FiltroDto
        private void GetInspirateDataAll(List<string> inscodes)
        {
            try
            {
                SPSite siteColl = SPContext.Current.Site;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                    {
                        using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                        {
                            SPList list = ElevatedSite.Lists[inspiratelist];

                            if (list != null)
                            {
                                string codevalues = String.Empty;
                                if (inscodes.Count > 0)
                                {
                                    foreach (string code in inscodes)
                                    {
                                        codevalues += string.Format("<Value Type=\"Text\">{0}</Value>", code);
                                    }
                                }
                                else
                                {
                                    codevalues = string.Format("<Value Type=\"Text\">{0}</Value>", "NOINSPIRATE");
                                }

                                SPQuery query = new SPQuery();
                                //query.ViewFields = "<FieldRef Name=\"CodigoInspirate\"/><FieldRef Name=\"Productos\"/><FieldRef Name=\"FileLeafRef\"/>";
                                //query.ViewAttributes = "Scope=\"Recursive\"";
                                //query.Query = string.Format(
                                //     "<Where><In><FieldRef Name = \"CodigoInspirate\"/><Values>{0}</Values></In></Where>"
                                //     , codevalues
                                //     );

                                //REZ 21072014 - Añadimos que muestre tanto Inspirates por código de catalogo como otros tipos de elementos (Banners Custom)
                                query.ViewFields = "<FieldRef Name=\"CodigoInspirate\"/><FieldRef Name=\"Productos\"/><FieldRef Name=\"FileLeafRef\"/>";
                                //Campos para otros tipos de contenido
                                query.ViewFields += "<FieldRef Name=\"ContentType\"/>";
                                //Campos para Inspirate tipo Banner Link
                                query.ViewFields += "<FieldRef Name=\"UrlBanner\"/>";
                                //Campos para Inspirate tipo Banner Filtros
                                query.ViewFields += "<FieldRef Name=\"FiltroCatalogo\"/><FieldRef Name=\"FiltroFamilia\"/><FieldRef Name=\"FiltroSubFamilia\"/><FieldRef Name=\"FiltroPrograma\"/><FieldRef Name=\"FiltroTextoLibre\"/>";

                                //Campos para idioma
                                query.ViewFields += "<FieldRef Name=\"Language\"/>";
                                //Campos para FiltroDescuento
                                query.ViewFields += "<FieldRef Name=\"FiltroDescuento\"/>";
                                

                                string BannerCTypes = "<Value Type=\"Text\">Banner Link</Value><Value Type=\"Text\">Banner Filtros</Value>";


                                query.Query = string.Format(
                                     "<Where>"+
                                        "<And>"+
                                            "<Or>" + 
                                                "<In><FieldRef Name = \"CodigoInspirate\"/><Values>{0}</Values></In>" +
                                                "<In><FieldRef Name = \"ContentType\"/><Values>{1}</Values></In>" + 
                                            "</Or>"+
                                            "<Or>"+
                                                "<IsNull><FieldRef Name=\"Language\"/></IsNull>" +
                                                "<Eq><FieldRef Name=\"Language\"/><Value Type=\"Text\">{2}</Value></Eq>"+
                                            "</Or>"+
                                        "</And>"+
                                     "</Where>"+
                                     "<OrderBy><FieldRef Name=\"Orden\" Ascending=\"True\" /></OrderBy>"


                                     , codevalues
                                     , BannerCTypes

                                     , getCurrentLanguage()

                                     );


                                query.ViewAttributes = "Scope=\"FilesOnly\"";
                                query.RowLimit = 500;
                                query.ViewFieldsOnly = true; // Fetch only the data that we need.


                                SPListItemCollection items = list.GetItems(query);

                                if (items == null || items.Count == 0)
                                    throw new Exception("Inspirate no encontrado");

                                foreach (SPListItem item in items)
                                {
                                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                                        continue;


                                    DataRow dtr = inspirates.NewRow();
                                    dtr["CodigoInspirate"] = (item["CodigoInspirate"] != null)?item["CodigoInspirate"].ToString():"";
                                    dtr["ImageUrlInspirate"] = "/" + item.Url;
                                    dtr["ProductsInspirate"] = (item["Productos"] != null) ? item["Productos"].ToString() : "";


                                    if (item["ContentType"].ToString() == "Picture") //"Banner Inspirate":
                                    {
                                        dtr["isInspirate"] = true;
                                        dtr["isNotInspirate"] = false;
                                    }
                                    else {
                                        dtr["isInspirate"] = false;
                                        dtr["isNotInspirate"] = true;
                                    }
                                    

                                    switch (item["ContentType"].ToString()){
                                        case "Picture": //"Banner Inspirate":
                                            break;
                                        case "Banner Filtros":
                                            string strFiltroUrl = String.Format("/Pages/RedirectTo.aspx?r=products&catalog={0}&f={1}&sf={2}&p={3}&t={4}&d={5}",
                                                (item["FiltroCatalogo"] != null) ? item["FiltroCatalogo"].ToString() : "",
                                                (item["FiltroFamilia"] != null) ? item["FiltroFamilia"].ToString() : "",
                                                (item["FiltroSubFamilia"] != null) ? item["FiltroSubFamilia"].ToString() : "",
                                                (item["FiltroPrograma"] != null) ? item["FiltroPrograma"].ToString() : "",
                                                (item["FiltroTextoLibre"] != null) ? item["FiltroTextoLibre"].ToString() : "",
                                                (item["FiltroDescuento"] != null) ? item["FiltroDescuento"].ToString() : "");
                                            dtr["UrlBanner"] = strFiltroUrl;
                                            break;
                                        case "Banner Link":
                                            if (item["UrlBanner"] != null)
                                            {
                                                SPFieldUrlValue value = new SPFieldUrlValue(item["UrlBanner"].ToString());
                                                dtr["UrlBanner"] = value.Url;
                                            }
                                            else {
                                                dtr["UrlBanner"] = "";
                                            }
                                            break;
                                    }


                                    inspirates.Rows.Add(dtr);
                                }
                                //this.InspiratesRepeater.DataSource = inspirates;
                                //this.InspiratesRepeater.DataBind();
                            }
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void GetInspirateData(string code)
        {
            try
            {
                SPSite siteColl = SPContext.Current.Site;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                    {
                        using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                        {
                            SPList list = ElevatedSite.Lists[inspiratelist];

                            if (list != null)
                            {
                                SPQuery query = new SPQuery();
                                query.ViewFields = "<FieldRef Name=\"CodigoInspirate\"/><FieldRef Name=\"Productos\"/><FieldRef Name=\"FileLeafRef\"/>";
                                //query.ViewAttributes = "Scope=\"Recursive\"";
                                query.Query = string.Format(
                                     "<Where><Eq><FieldRef Name=\"CodigoInspirate\"/><Value Type=\"Text\">{0}</Value></Eq></Where>"
                                     , code
                                     );
                                query.ViewAttributes = "Scope=\"FilesOnly\"";
                                query.RowLimit = 500;
                                query.ViewFieldsOnly = true; // Fetch only the data that we need.


                                SPListItemCollection items = list.GetItems(query);
                                
                                if (items == null || items.Count == 0)
                                    throw new Exception("Inspirate no encontrado");

                                //DataTable inspirates = new DataTable();
                                //inspirates.Columns.Add("CodigoInspirate");
                                //inspirates.Columns.Add("ImageUrlInspirate");

                                foreach (SPListItem item in items)
                                {
                                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                                        continue;

                                    
                                    DataRow dtr = inspirates.NewRow();
                                    dtr["CodigoInspirate"] = item["CodigoInspirate"].ToString();
                                    dtr["ImageUrlInspirate"] = "/" + item.Url;
                                    dtr["ProductsInspirate"] = item["Productos"].ToString();
                                    inspirates.Rows.Add(dtr);
                                }
                                //this.InspiratesRepeater.DataSource = inspirates;
                                //this.InspiratesRepeater.DataBind();
                            }
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        //No se usa. Era ineficiente...
        private void loadarticles(string code)
        {
            try
            {
                SPSite siteColl = SPContext.Current.Site;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                    {
                        using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                        {
                            SPList list = ElevatedSite.Lists[inspiratelist];

                            if (list != null)
                            {
                                SPQuery query = new SPQuery();
                                query.ViewFields = "<FieldRef Name=\"CodigoInspirate\"/><FieldRef Name=\"Productos\"/><FieldRef Name=\"FileLeafRef\"/>";
                                query.ViewAttributes = "Scope=\"Recursive\"";
                                query.Query = string.Format(
                                    "<Where><Eq><FieldRef Name=\"CodigoInspirate\"/><Value Type=\"Text\">{0}</Value></Eq></Where>"
                                    , code
                                    );
                                query.ViewAttributes = "Scope=\"FilesOnly\"";
                                query.RowLimit = 500;
                                query.ViewFieldsOnly = true; // Fetch only the data that we need.
                                SPListItemCollection items = list.GetItems(query);
                                //if (items == null || items.Count == 0)
                                //{
                                //    query = new SPQuery();
                                //    query.ViewFields = "<FieldRef Name=\"LinkFilename\"/><FieldRef Name=\"Code\"/><FieldRef Name=\"Caducity\"/><FieldRef Name=\"ImageType\"/>";
                                //    query.ViewAttributes = "Scope=\"Recursive\"";
                                //    query.Query = string.Format(
                                //        "<Where><Eq><FieldRef Name=\"Code\"/><Value Type=\"Text\">{0}</Value></Eq></Where>"
                                //        , code);

                                //    items = list.GetItems(query);
                                //}
                                //slideInfo.code = code;
                                //slideInfo.link = this.elemLink + "?catalog=" + code;
                                if (items == null || items.Count == 0)
                                    throw new Exception("Inspirate no encontrado");

                                foreach (SPListItem item in items)
                                {
                                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                                        continue;

                                    //string cad = item.Properties["Caducity"].ToString();
                                    //if (!string.IsNullOrEmpty(cad))
                                    //{
                                    //    DateTime caducity = DateTime.Parse(cad);
                                    //    if (DateTime.Now.CompareTo(caducity) <= 0)
                                    //    {
                                    //        if (item.Properties["ImageType"].ToString() == "On")
                                    //        {
                                    //            slideInfo.img_url = "/" + item.Url;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        if (item.Properties["ImageType"].ToString() == "Off")
                                    //        {
                                    //            slideInfo.img_url = "/" + item.Url;
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //this.InspirateImage.ImageUrl = "/" + item.Url; ;    // item["FileLeafRef"].ToString();
                                    AddInspirateArticles(item["Productos"].ToString());
                                    break;
                                    //}
                                }
                            }
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
      
        void AddInspirateArticles(string products)
        {
            //DataTable articles = new DataTable();
            articles = new DataTable();
            articles.Columns.Add("ArticleCode");
            articles.Columns.Add("ArticleDescription");
            articles.Columns.Add("ArticlePrice");
            articles.Columns.Add("Catalog");
            articles.Columns.Add("Top");
            articles.Columns.Add("Left");
            articles.Columns.Add("NavigateUrl");
            
            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];
            }
            ArticleDataAccessObject articleDAO = Session["articleDAO"] as ArticleDataAccessObject;


            string[] inspirateproducts = products.Split(new string[]{Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string inspirateproduct in inspirateproducts)
            {
                string[] inspirateproductprops = inspirateproduct.Split(';');
                Article inspirateArticle = articleDAO.GetProductByCode(inspirateproductprops[0]);
                DataRow dtr = articles.NewRow();
                dtr["ArticleCode"] = inspirateArticle.Code;
                //REZ 29042013 -- Cambiem descripcio per programa
                //dtr["ArticleDescription"] = inspirateArticle.Code + " - " + inspirateArticle.Description;
                dtr["ArticleDescription"] = inspirateArticle.Code + " - " + inspirateArticle.Programa;
                int iPriceSel = (int)Session["PriceSelector"];
                CultureInfo culture = new CultureInfo("es-Es", false);
                
                if (iPriceSel == 0)
                {
                    dtr["ArticlePrice"] = inspirateArticle.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                }
                else if (iPriceSel == 1)
                {
                    dtr["ArticlePrice"] = inspirateArticle.Price.ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                }
                else if (iPriceSel == 2)
                {
                    dtr["ArticlePrice"] = ((Convert.ToDouble(inspirateArticle.PriceInPoints, culture)) * (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                } else if (iPriceSel == 3) {
                    dtr["ArticlePrice"] = ((Convert.ToDouble(inspirateArticle.Price.ToString("N2", culture), culture)) *
                                                (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))).ToString("N2", culture) 
                                                + " " + CurrentClient.LiteralDivisaWEB;
                }else if (iPriceSel == 4) {
                    dtr["ArticlePrice"] = (((Convert.ToDouble(inspirateArticle.PriceInPoints, culture)) *
                                                (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))) *
                                                (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2", culture)
                                                + " " + CurrentClient.LiteralDivisaWEB + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                 }

                dtr["Catalog"] = this.codCatalog;
                dtr["Left"] = inspirateproductprops[1];
                dtr["Top"] = inspirateproductprops[2];

                dtr["NavigateUrl"] = "/Pages/ProductInfo.aspx?code=" + inspirateArticle.Code + "&version=" + inspirateArticle.ArticleVersion + "&catalog=" + inspirateArticle.InsertcatalogNo;

                articles.Rows.Add(dtr);

            }

            //this.articlesInspirate.DataSource = articles;
            //this.articlesInspirate.DataBind();
        }

        protected void BuyAllBut_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }

                if (Session["Client"] != null)
                {
                    this.CurrentClient = (Client)Session["Client"];
                }
                string codArt = string.Empty;
                string catalogArt = Request.QueryString["catalog"];
                RepeaterItem item = (RepeaterItem)((Control)sender).Parent;
                catalogArt = ((HiddenField)(item.FindControl("hiddenInspirateCode"))).Value;
                string versionArt = string.Empty;

                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                Order order = orderDAO.GetCurrentNavOrder(CurrentClient,0);

                //ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(CurrentUser.UserName, CurrentClient.Code);
                //Article art = articleDAO.GetProductByCodeAndCatalog(codArt, versionArt, catalogArt);

                string InserInspirateResult = string.Empty;
                //InserInspirateResult = orderDAO.InsertInspirate(order, currentUser.UserName, catalogArt, order.NavOferta.No);
                Order currentOrder = Session["Order"] as Order;
                currentOrder.AddInspirate(currentUser.UserName, catalogArt, order.NavOferta.No);
                if (!String.IsNullOrEmpty(InserInspirateResult))
                {
                    throw new NavException(InserInspirateResult);
                }
                else
                {
                    //CarritoUserControl carrito = (CarritoUserControl)this.FindControlRecursive(this.Page.Master, "Carrito");
                    //carrito.Refresh();
                    RefreshCarrito();
                    //REZ04042016 - Quitamos los popUps, debe mostrarse el aumento de número en el sticky
                    //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));
                }
            }
            catch (NavException ex)
            {
                //string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                //Page.scripClientScript.RegisterClientScriptBlock(this.GetType(), "radalert", radalertscript);
                RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
                ajaxManager.ResponseScripts.Add(@"radalert('" + ex.Message + "', 330, 100);");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);


            }
        }

        private void RefreshCarrito()
        {
            RadAjaxManager ajaxManager = RadAjaxManager.GetCurrent(Page);
            ajaxManager.ResponseScripts.Add(@"RefreshTable();");
        }

        public Control FindControlRecursive(Control Root, string Id)
        {
            try
            {
                if (Root.ID == Id)
                    return Root;
                foreach (Control Ctl in Root.Controls)
                {
                    Control FoundCtl = FindControlRecursive(Ctl, Id);
                    if (FoundCtl != null)
                        return FoundCtl;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return null;
        }

        //REZ30122014 - Función para obtener el idioma del usuario. Debería ir a Utils porque está replicada en varios sitios
        private string getCurrentLanguage()
        {
            string lang = string.Empty;

            try
            {
                string cur_lan = System.Threading.Thread.CurrentThread.CurrentUICulture.LCID.ToString();

                switch (cur_lan)
                {
                    case "3082":
                        lang = "Spanish";
                        break;
                    case "1040":
                        lang = "Italian";
                        break;
                    case "1036":
                        lang = "French";
                        break;
                    case "1027":
                        lang = "Catalan";
                        break;
                    case "1031":
                        lang = "German";
                        break;
                    default:
                        lang = "English";
                        break;
                }

            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return lang;
        }


    }
}
