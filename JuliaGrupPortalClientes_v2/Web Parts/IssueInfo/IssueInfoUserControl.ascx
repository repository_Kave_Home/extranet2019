﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IssueInfoUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.IssueInfo.IssueInfoUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<!-- link href="/Style%20Library/Julia/Incidencias.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationIncidencias" Name="/Style Library/Julia/Incidencias.css" runat="server" />

<div class="classIncidencias">
    <div class="headerIncidencias">
        <asp:Label ID="lblTitle" CssClass="titleIncidencias" runat="server" />
        <asp:Label ID="lblSubtitle" CssClass="subtitleIncidencia" runat="server" />
    </div>
    <div class="bodyIncidencias">
        <div id="clientField" class="lineIssue">
            <div class="txtIssue">
                <div class="txtLabel">
                    <asp:Label ID="lblNCliente" CssClass="txtIssueMain" runat="server" />
                </div>
                 <div class="inputIssue">
                    <telerik:RadTextBox ID="TxtClient" runat="server" Width="200px" Enabled="false">
                    </telerik:RadTextBox>
                </div>
            </div>       
        </div>
        <div id="issueCodeField" class="lineIssue">
            <div class="txtIssue ">
                <div class="txtLabel">
                    <asp:Label ID="LblIssueCode" CssClass="txtIssueMain" runat="server" />
                </div>
                <div class="inputIssue">
                    <telerik:RadTextBox ID="TxtIssueCode" runat="server" Width="200px" Enabled="false">
                    </telerik:RadTextBox>
                </div>
            </div>            
        </div>
        <div id="issueTypeField" class="lineIssue">
            <div class="txtIssue ">
                <div class="txtLabel">
                    <asp:Label ID="LblIssueType" CssClass="txtIssueMain" runat="server" />
                </div>
                <div class="inputIssue">
                    <telerik:RadTextBox ID="TxtIssueType" runat="server" Width="200px" Enabled="false">
                    </telerik:RadTextBox>
                </div>
            </div>            
        </div>
        <div id="entranceDateField" class="lineIssue">
            <div class="txtIssue ">
                <div class="txtLabel">
                    <asp:Label ID="LblEntranceDate" CssClass="txtIssueMain" runat="server" />
                </div>
                <div class="inputIssue">
                    <telerik:RadDatePicker RenderMode="Lightweight" ID="DpEntranceDate" CssClass="txtIssueMain" 
                        Width="200px" Enabled="false" runat="server">
                    </telerik:RadDatePicker>
                </div>
            </div>            
        </div>
    </div>
</div>



