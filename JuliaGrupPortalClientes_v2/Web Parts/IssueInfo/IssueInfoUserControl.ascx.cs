﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
//using JuliaGrupPortalClientes_v2.Resources;
using Microsoft.SharePoint;
using System.Text;
using System.Web;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using System.Diagnostics;
using System.Data;
using System.Linq;

namespace JuliaGrupPortalClientes_v2.Web_Parts.IssueInfo
{
    public partial class IssueInfoUserControl : UserControl
    {
        #region Fields
        private IssueDataAccessObject IssueDao;
        private User currentUser;
        private List<Issue> issue;
        private string issueId = string.Empty;
        #endregion

        #region Properties

        public string WPTitle { get; set; }
        public string WPSubtitle { get; set; }

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sesion SesionJulia = new Sesion();
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }

                if (!IsPostBack)
                {
                    Initialize();
                    //recibe id issue desde listado de issues
                    if (!string.IsNullOrEmpty(Page.Request.QueryString["issueId"]))
                    {
                        issueId = Page.Request.QueryString["issueId"].ToString();
                        LoadIssue(issueId);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void Initialize()
        {
            this.lblTitle.Text = this.WPTitle;
            this.lblSubtitle.Text = this.WPSubtitle;
            this.LblIssueCode.Text = LanguageManager.GetLocalizedString("IssuesListIssueCode");
            this.lblNCliente.Text = LanguageManager.GetLocalizedString("IssuesClient");
            this.LblIssueType.Text = LanguageManager.GetLocalizedString("IssuesType");
            this.LblEntranceDate.Text = LanguageManager.GetLocalizedString("IssuesEntranceDate");
            this.DpEntranceDate.DatePopupButton.Visible = false;
            //this.LblDoc.Text = LanguageManager.GetLocalizedString("IssuesDoc");
            //this.LblProduct.Text = LanguageManager.GetLocalizedString("IssuesProduct");
            //this.LblSerialNumber.Text = LanguageManager.GetLocalizedString("IssuesSerialNumber");
        }

        private void LoadIssue(string Id)
        {
            try
            {
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                IssueDataAccessObject IssuesDao = new IssueDataAccessObject();
                List<Issue> issueList = new List<Issue>();
                issueList = IssuesDao.GetIssuesList(currentUser.UserName, "");

                Issue item = (Issue) issueList.Where(a => a.IssueNo == Id).FirstOrDefault();
                //Cabecera código Incidencia
                //this.TxtDoc.Text = item.IssueNo;
                //Tipo incidencia +causa
                this.TxtIssueType.Text = item.TipoResolucion;
                //cliente
                this.TxtClient.Text = item.customerName;
                //referencia
                //this.TxtProduct.Text = item.Reference;
                //fecha Apertura
                this.DpEntranceDate.SelectedDate = item.EntranceDate;
                //estado
                setState(item.state);
                //dirección envío
                this.TxtIssueCode.Text = item.IssueNo;
                //Detalle
                //item.Descripcion;
                //Nº producto

                //nombre producto
                //cantidad
                //item.cantidad;
                //Nº Albarán, Nº Factura, Nº Pedido
                //Descripción incidencia
                //fotos adjuntas
                //Histórico comentarios + Cambios de estado

                //Resolución 
                //tipo decisión
                //fecha decisión
                //fecha cierre
                //Nº Pedido Ventas(con vínculo a la nueva pedido, si los hay)
                //Nº Alba Ventas(con vínculo al nuevo albarán, si los hay)
                //Nº Factura Ventas(con vínculo a la nueva factura, si los hay)
                //Nº Abono Ventas(con vínculo al abono, si los hay)
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }            
        }

        private string setState(string state)
        {
            string ret = string.Empty;
            Dictionary<string, string> statesString = new Dictionary<string, string>();
            statesString.Add("0", string.Empty);
            statesString.Add("1", LanguageManager.GetLocalizedString("IssueListPending"));
            statesString.Add("2", LanguageManager.GetLocalizedString("IssueListEnCurso"));
            statesString.Add("3", LanguageManager.GetLocalizedString("IssueListFinalizado"));
            statesString.Add("4", LanguageManager.GetLocalizedString("IssueListCerrado"));

            ret = LanguageManager.GetLocalizedString(statesString[state]);

            return string.IsNullOrEmpty(ret) ? string.Empty : ret;
        }

       
        #endregion
    }
}
