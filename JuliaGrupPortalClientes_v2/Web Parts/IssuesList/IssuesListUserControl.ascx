﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IssuesListUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.IssuesList.IssuesListUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<!-- link href="/Style%20Library/Julia/PedidoAdvanced.css" rel="stylesheet" type="text/css" / -->
<%--<SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/PedidoAdvanced.css" runat="server"/>--%>
<SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/Incidencias.css" runat="server"/>
<style type="text/css">
.SupBtnsLeft
{
    float:left;}
 .supBtnsRight
 {
     float:right;
     }   
 .supBtns
 {
     width:100%;}
 .PriceColumn
 {
     text-align:right;
     }
</style>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
<AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnTodos">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoPendientes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoEnCurso">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoFinalizado">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>        
          <telerik:AjaxSetting AjaxControlID="BtnEstadoCerrado">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>        
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxLoadingPanel ID="LoadingPanel" Skin ="Default" runat="server"></telerik:RadAjaxLoadingPanel>



<div class="tableOrderList">
    <div class="supBtns SupBtnsLeft">
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnTodos" runat="server" ToggleType="CheckBox" 
            ButtonType="StandardButton" oncheckedchanged="BtnTodos_CheckedChanged">
                <ToggleStates>
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
         <div class="supBtnsRight">
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoPendientes" runat="server" ToggleType="CheckBox" 
                ButtonType="StandardButton"  Checked="true"
                oncheckedchanged="BtnEstadoPendientes_CheckedChanged">
                <ToggleStates>
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoEnCurso" Checked="true" runat="server" ToggleType="CheckBox" 
            ButtonType="StandardButton" oncheckedchanged="BtnEstadoEnCurso_CheckedChanged">
                <ToggleStates>
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
         <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoFinalizado" Checked="true" runat="server" ToggleType="CheckBox" 
            ButtonType="StandardButton" 
                 oncheckedchanged="BtnEstadoFinalizado_CheckedChanged">
                <ToggleStates>
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
         <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoCerrado" Checked="true" runat="server" ToggleType="CheckBox" 
            ButtonType="StandardButton" 
                 oncheckedchanged="BtnEstadoCerrado_CheckedChanged">
                <ToggleStates>
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
        </div>
       </div>
    <telerik:RadGrid ID="RadGrid1" Width="100%" AllowSorting="true" OnNeedDataSource="RadGrid1_NeedDataSource" 
    HeaderStyle-CssClass="PedidoAdvancedHeaderClass" CssClass="Grid" AllowFilteringByColumn="true"
        AutoGenerateColumns="false"
        PageSize="9" AllowPaging="True"
        EnableAjaxSkinRendering="true" AllowMultiRowSelection="False" 
        runat="server" GridLines="None" OnSortCommand="RadGrid1_SortCommand" 
        onitemdatabound="RadGrid1_ItemDataBound">
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Bottom"
            UseAllDataFields="true" InsertItemPageIndexAction="ShowItemOnCurrentPage" AllowMultiColumnSorting="true" AllowNaturalSort ="true">
            <CommandItemSettings ShowAddNewRecordButton="false" />
                 
            <Columns>
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="Code" HeaderText="Code" UniqueName="Code" />
                <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="CodiIncidencia" HeaderText="CodiIncidencia" UniqueName="CodiIncidencia" />
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="Reference" HeaderText="Reference" UniqueName="Reference" ReadOnly="true">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EntryData" ShowFilterIcon="false" Visible="false" Display="false"  AllowFiltering="false"  HeaderText="EntryData" UniqueName="EntryData"  DataFormatString="{0:dd/MM/yyyy}"
                    ReadOnly="true" />
                <telerik:GridBoundColumn DataField="EntryStartGest" ShowFilterIcon="false"  AllowFiltering="false" HeaderText="EntryStartGest"  DataFormatString="{0:dd/MM/yyyy}"
                    UniqueName="EntryStartGest" />
                 <telerik:GridBoundColumn DataField="EndDate"  HeaderText="EndDate" ShowFilterIcon="false"  AllowFiltering="false"  DataFormatString="{0:dd/MM/yyyy}"
                    UniqueName="EndDate" />
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="State" HeaderText="State"
                    UniqueName="State" />
                      
                      <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="ResolutionType" HeaderText="ResolutionType"
                    UniqueName="ResolutionType" />
                      <telerik:GridBoundColumn   DataField="FechaCerrado" HeaderText="FechaCerrado"    ShowFilterIcon="false"  AllowFiltering="false"   DataFormatString="{0:dd/MM/yyyy}"
                    UniqueName="FechaCerrado" />
              
                
               
            </Columns>
        </MasterTableView>
        <PagerStyle Mode="NextPrev" />
        <ClientSettings>
            <ClientEvents OnRowClick="RowClick">
            </ClientEvents>
        </ClientSettings>
    </telerik:RadGrid>


</div>
