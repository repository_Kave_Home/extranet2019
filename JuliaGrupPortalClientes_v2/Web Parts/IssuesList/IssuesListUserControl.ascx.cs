﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using JuliaGrupUtils.Utils;
using System.Data;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Microsoft.SharePoint;


namespace JuliaGrupPortalClientes_v2.Web_Parts.IssuesList
{
    public partial class IssuesListUserControl : UserControl
    {             
        private List<Issue> issue;
        private User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sesion SesionJulia = new Sesion();
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }

                if (!IsPostBack)
                {                    
                    if (this.currentUser is Agent)
                    {
                        BtnTodos.Visible = true;
                    }
                    else if (this.currentUser is Client)
                    {
                        BtnTodos.Visible = false;
                    }
                    else//CLIENT GROUP
                    {
                        BtnTodos.Visible = true;
                    }
                    this.Initialize();                    
                }
                else
                {
                    //this.LoadSelectedArticles();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void Initialize()
        {
            this.BtnTodos.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListTodos");
            this.BtnEstadoPendientes.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListPending");
            this.BtnEstadoEnCurso.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListEnCurso");
            this.BtnEstadoFinalizado.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListFinalizado");
            this.BtnEstadoCerrado.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListCerrado");

            this.RadGrid1.Columns.FindByUniqueNameSafe("Code").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssuesListIssueClient");
            this.RadGrid1.Columns.FindByUniqueNameSafe("CodiIncidencia").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssuesListIssueCode");
            this.RadGrid1.Columns.FindByUniqueNameSafe("Reference").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListReference");
            this.RadGrid1.Columns.FindByUniqueNameSafe("EntryData").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListIssueEntryDate");
            this.RadGrid1.Columns.FindByUniqueNameSafe("EntryStartGest").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListIssueStartDate");
            this.RadGrid1.Columns.FindByUniqueNameSafe("State").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListState");
            this.RadGrid1.Columns.FindByUniqueNameSafe("EndDate").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListIssueEndDate");
            this.RadGrid1.Columns.FindByUniqueNameSafe("ResolutionType").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssuesListTipoResolucion");
            this.RadGrid1.Columns.FindByUniqueNameSafe("FechaCerrado").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("IssueListCerradoHeader");                        
        }       

        private List<Issue> FiltrarChecks(List<Issue> Entrada)
        {
            var result = from rows in Entrada
                         select rows;

            List<string> names = new List<string>();

            if (BtnEstadoPendientes.Checked)
            {
               
                names.Add("1");
            }
            if (BtnEstadoEnCurso.Checked)
            {
                
                names.Add("2");
            }
            if (BtnEstadoFinalizado.Checked)
            {
               
                names.Add("3");
                
            }
            if (BtnEstadoCerrado.Checked)
            {
                names.Add("4");
            }

            var matching = result.Where(x => names.Contains(x.state));
            return matching.ToList<Issue>();
        }

        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                DataTable data = null;
               
                if (BtnTodos.Checked)
                {
                    data = LoadAllIssues();
                }
                else
                {
                    data = loadClientIssues();
                }

                this.RadGrid1.DataSource = data;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }         
        }

        private DataTable loadClientIssues()
        {
            DataTable data = null;
            try
            {
                Sesion SesionJulia = new Sesion();
                List<Issue> Issue;
                IssueDataAccessObject IssueDao = new IssueDataAccessObject();
                if (Session["Client"] == null)
                    throw new Exception("Session de client es null");

                Client cli = (Client)Session["Client"];

                Issue = IssueDao.GetIssuesList(cli.UserName, cli.Code);
                
                data = GenerateTable();
                List<Issue> res = FiltrarChecks(Issue);
                LoadIssues(res, ref data);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }
        private DataTable LoadAllIssues()
        {
            DataTable data = null;
            try
            {
               
                 if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                 IssueDataAccessObject IssuesDao = new IssueDataAccessObject();
                List<Issue> issueList = new List<Issue>();
                issueList = IssuesDao.GetIssuesList(currentUser.UserName, "");
                                        
                data = GenerateTable();
                List<Issue> res = FiltrarChecks(issueList);
                LoadIssues(res, ref data);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;                                              
        }

        private void LoadIssues(List<Issue> fIssue, ref DataTable data)
        {
            try
            {
                if (fIssue != null)
                {
                    foreach (Issue issue in fIssue)
                    {
                        DataRow row = data.NewRow();
                        row["Code"] = issue.customerNumber + " - " + issue.customerName;
                        row["CodiIncidencia"] = issue.IssueNo;
                        row["Reference"] = issue.Reference;
                        row["EntryData"] = issue.EntranceDate;
                        row["EntryStartGest"] = issue.StartDate;
                        row["State"] = setState(issue.state);
                            
                        row["EndDate"] = issue.EndDate;
                        row["ResolutionType"] = issue.TipoResolucion;
                        row["FechaCerrado"] = issue.CerradoDate;
                        data.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        private string setState(string state)
        {
            string ret = string.Empty;
            Dictionary<string, string> statesString = new Dictionary<string, string>();
            statesString.Add("0", string.Empty);
            statesString.Add("1", LanguageManager.GetLocalizedString("IssueListPending"));
            statesString.Add("2", LanguageManager.GetLocalizedString("IssueListEnCurso"));
            statesString.Add("3", LanguageManager.GetLocalizedString("IssueListFinalizado"));
            statesString.Add("4", LanguageManager.GetLocalizedString("IssueListCerrado"));
       
            ret = statesString[state];
                    
            return string.IsNullOrEmpty(ret)? string.Empty : ret;
        }

        private DataTable GenerateTable()
        {           
            DataTable data = new DataTable();
            try
            {
                data.Columns.Add("Code");
                data.Columns.Add("CodiIncidencia");
                data.Columns.Add("Reference");
                data.Columns.Add("EntryData",typeof(DateTime));
                data.Columns.Add("EntryStartGest", typeof(DateTime));
                data.Columns.Add("State");
                data.Columns.Add("EndDate", typeof(DateTime));
                data.Columns.Add("ResolutionType");
                data.Columns.Add("FechaCerrado", typeof(DateTime));                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            return data;
        }
        protected void BtnTodos_CheckedChanged(object sender, EventArgs e)
        {
            this.RadGrid1.Rebind();
        }
        private string getText(string text)
        {
            return JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(text);
        }
        
        protected void RadGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                //Apply custom sorting
                GridSortExpression sortExpr;
                sortExpr = new GridSortExpression();

                switch (e.OldSortOrder)
                {
                    case GridSortOrder.None:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Descending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Ascending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = RadGrid1.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Descending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Ascending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                }
                e.Canceled = true;
                RadGrid1.Rebind();

                //Default sort order Descending

                if (!e.Item.OwnerTableView.SortExpressions.ContainsExpression(e.SortExpression))
                {
                    sortExpr = new GridSortExpression();
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void BtnEstadoPendientes_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void BtnEstadoEnCurso_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void BtnEstadoFinalizado_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void BtnEstadoCerrado_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item["EndDate"].Text == "01/01/0001")
                {
                    item["EndDate"].Text = "";
                }
                if (item["EntryData"].Text == "01/01/0001")
                {
                 item["EntryData"].Text = "";
                }
                if (item["EntryStartGest"].Text == "01/01/0001")
                {
                    item["EntryStartGest"].Text = "";
                }
                if (item["FechaCerrado"].Text == "01/01/0001")
                {
                    item["FechaCerrado"].Text = "";
                }                
            }
        }       
    }
}
