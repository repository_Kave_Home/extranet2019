﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;

namespace JuliaGrupPortalClientes_v2.Web_Parts.ItemsGrid
{
    [ToolboxItemAttribute(false)]
    public class ItemsGrid : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2.Web_Parts/ItemsGrid/ItemsGridUserControl.ascx";
        public enum Dato { ProductosCatalogo, Promociones }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Título de la grid")]
        [System.ComponentModel.Category("Custom")]
        public string Titulo { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Horizontal/Vertical")]
        [System.ComponentModel.Category("Custom")]
        public string Orientation { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Tipo de datos")]
        [System.ComponentModel.Category("Custom")]
        public Dato Datos { get; set; }

        protected override void CreateChildControls()
        {
            ItemsGridUserControl itemsGridcontrol = Page.LoadControl(_ascxPath) as ItemsGridUserControl;
            if (this.Titulo == null)
                itemsGridcontrol.Title = string.Empty;
            else if(!this.Titulo.Contains("{0}"))
                itemsGridcontrol.Title = this.Titulo;
            else
                itemsGridcontrol.Title = string.Format(this.Titulo, LanguageManager.GetLocalizedString("CatalogosTitle"));
            if (Orientation == "Vertical")
                itemsGridcontrol.isVertical = true;
            else
                itemsGridcontrol.isVertical = false;
            itemsGridcontrol.Datos = this.Datos;
            Controls.Add(itemsGridcontrol);
        }
    }
}
