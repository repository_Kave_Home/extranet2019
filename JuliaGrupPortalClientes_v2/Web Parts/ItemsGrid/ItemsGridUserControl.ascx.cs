﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Business;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint;
using System.Diagnostics;

namespace JuliaGrupPortalClientes_v2.Web_Parts.ItemsGrid
{
    public partial class ItemsGridUserControl : UserControl
    {
        private ArticleDataAccessObject articleDAO;
        private ItemsGrid.Dato datos;

        public string Title = string.Empty;
        public bool isVertical = false;
        public ItemsGrid.Dato Datos
        {
            get { return datos; }
            set { datos = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (Session["ArticleDAO"] != null)
                    this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];

                //this.EnsureChildControls();   //no puede ir aqui porque entonces carga todos los controles antes de calcular el usuario
                base.OnInit(e);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected override void CreateChildControls()
        {
            try
            {
                if (this.articleDAO != null)
                {
                    List<string> catalogs = null;
                    switch (datos)
                    {
                        case ItemsGrid.Dato.Promociones:
                            catalogs = this.articleDAO.GetCatalogsCode("Promociones");
                            break;
                        case ItemsGrid.Dato.ProductosCatalogo:
                            catalogs = this.articleDAO.GetCatalogsCode("General");
                            break;
                    }

                    int limit = 0;
                    TableRow r = new TableRow();
                    int mod = 3;

                    if (isVertical)
                        mod = 2;
                    string code = this.Request.QueryString["catalog"];
                    foreach (string catalog in catalogs)
                    {

                        if (limit % mod == 0)
                        {
                            r = new TableRow();
                            this.ItemsTable.Controls.Add(r);
                        }
                        TableCell c = new TableCell();
                        HtmlGenericControl div = new HtmlGenericControl("div");

                        if (code == catalog)
                            div.Attributes.Add("class", "itemGridCellSelected");
                        else
                            div.Attributes.Add("class", "itemGridCell");
                        div.Controls.Add(getItem(catalog));

                        c.Controls.Add(div);
                        r.Cells.Add(c);
                        limit++;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private HtmlGenericControl getItem(string code)
        {        
            HtmlGenericControl a = new HtmlGenericControl("a");
            try
            {
                SlideInfo slideInfo = getCatalogInfo("Catalogs", code);
              
                a.Attributes.Add("href", slideInfo.link);
                a.Attributes.Add("name", "gridItem");
                HtmlGenericControl img = new HtmlGenericControl("img");
                img.Attributes.Add("src", slideInfo.img_url);
                img.Attributes.Add("border", "0px");

                a.Controls.Add(img);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return a;
        }

        private SlideInfo getCatalogInfo(string listName, string code)
        {
            SlideInfo slideInfo = new SlideInfo();
            try
            {
                SPSite siteColl = SPContext.Current.Site;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                    {
                        using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                        {
                            SPList list = ElevatedSite.Lists[listName];

                            if (list != null)
                            {
                                SPQuery query = new SPQuery();
                                query.ViewFields = "<FieldRef Name=\"LinkFilename\"/><FieldRef Name=\"Code\"/><FieldRef Name=\"Caducity\"/><FieldRef Name=\"ImageType\"/><FieldRef Name=\"Language\"/>";
                                query.ViewAttributes = "Scope=\"Recursive\"";
                                query.Query = string.Format(
                                    "<Where><And><Eq><FieldRef Name=\"Code\"/><Value Type=\"Text\">{0}</Value></Eq><Eq><FieldRef Name=\"Language\"/><Value Type=\"Text\">{1}</Value></Eq></And></Where>"
                                    , code
                                    , getCurrentLanguage());

                                SPListItemCollection items = list.GetItems(query);
                                slideInfo.code = code;
                                slideInfo.link = "?catalog=" + code;

                                foreach (SPListItem item in items)
                                {
                                    string cad = item.Properties["Caducity"].ToString();
                                    if (!string.IsNullOrEmpty(cad))
                                    {
                                        DateTime caducity = DateTime.Parse(cad);
                                        if (DateTime.Now.CompareTo(caducity) <= 0)
                                        {
                                            if (item.Properties["ImageType"].ToString() == "On")
                                            {
                                                slideInfo.img_url = "/" + item.Url;
                                            }
                                        }
                                        else
                                        {
                                            if (item.Properties["ImageType"].ToString() == "Off")
                                            {
                                                slideInfo.img_url = "/" + item.Url;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        slideInfo.img_url = "/" + item.Url;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return slideInfo;
        }

        private string getCurrentLanguage()
        {     
            string lang = string.Empty;

            try
            {
                string cur_lan = System.Threading.Thread.CurrentThread.CurrentUICulture.LCID.ToString();
          
                switch (cur_lan)
                {
                    case "1034":
                        lang = "Spanish";
                        break;
                    case "1040":
                        lang = "Italian";
                        break;
                    case "1036":
                        lang = "French";
                        break;
                    case "1027":
                        lang = "Catalan";
                        break;
                    case "1031":
                        lang = "German";
                        break;
                    default:
                        lang = "English";
                        break;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
           
            return lang;
        }
    }

    class SlideInfo
    {

        public string img_url;
        public string link;
        public string width;
        public string height;
        public string code;
        public string description;
        public string price;
    }
}
