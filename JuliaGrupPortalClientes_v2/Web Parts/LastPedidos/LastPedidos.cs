﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace JuliaGrupPortalClientes_v2.Web_Parts.LastPedidos
{
    [ToolboxItemAttribute(false)]
    public class LastPedidos : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2.Web_Parts/LastPedidos/LastPedidosUserControl.ascx";

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Número de elementos a listar")]
        [System.ComponentModel.Category("Custom")]
        public int NumeroPedidos { get; set; }

        protected override void CreateChildControls()
        {
            LastPedidosUserControl lastPedidosControl = Page.LoadControl(_ascxPath) as LastPedidosUserControl;
            lastPedidosControl.NumPedidos = this.NumeroPedidos;
            Controls.Add(lastPedidosControl);
        }
    }
}
