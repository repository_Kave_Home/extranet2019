﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LastPedidosUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.LastPedidos.LastPedidosUserControl" %>
<!-- link href="/Style%20Library/Julia/LastPedidos.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationLastPedidos" name="/Style Library/Julia/LastPedidos.css" runat="server"/>
<script language="javascript" type="text/javascript">
    function RowClick(code, type, client) {
        OpenOrderInfoDialog(code, type, client);
    }
    function OpenOrderInfoDialog(code, type, client) {
        var options = {
            url: "/Pages/OrderInfo.aspx?code=" + code + "&type=" + type +"&client=" + client ,
            width: 900,
            height: 600,      
            dialogReturnValueCallback: DialogCallback
        };

        SP.UI.ModalDialog.showModalDialog(options);
    }

    function DialogCallback(dialogResult, returnValue) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            RefreshTable();
        }
    }
</script>

<div class="containerCurrentOrders">
    <div class="headerCurrentOrders">
        <div class="txtCurrentOrdersTitle"><%= this.Title %></div>
        <div class="salto"></div>
        <div class="txtCurrentOrdersSubtitle"><%= this.Subtitle %></div>
    </div>
    <div class="pedidoSeparator" style="margin-right:7%;margin-left:7%;"></div>
    <div class="pedidoArea">
    <asp:PlaceHolder ID="LastOrdersHolder" runat="server">
    <asp:Repeater runat="server" ID="RptLastOrdersHolder">
        <ItemTemplate>
            <div class="<%# Eval("PedidoClass") %>" onclick="<%# Eval("PedidoClick") %>">
                <table class="infoPedidoCurso">
                    <tr>
                        <td>
                            <div class="titlePedidoCurso"><%# Eval("ventaNumber") %></div>
                        </td>
                        <td>
                            <div class="<%# Eval("ventaEstadoClass")%>"><%# Eval("ventaEstadoText")%></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="descPedidoCurso"><%# Eval("ventaImporte")%></div>
                        </td>
                        <td>
                            <div class="datePedidoCurso"><%# Eval("ventaFecha")%></div>
                        </td>
                    </tr>
                </table>
                <div class="pedidoSeparator"></div>
           </div>
        </ItemTemplate>
    </asp:Repeater>
    </asp:PlaceHolder>
    </div>
    <div class="salto"></div>
    <div class="linkViewDetails" onclick="location.href='/Pages/OrdersList.aspx'">
        <asp:Label ID="lblLinkViewDetails" CssClass="txtViewDetails" runat="server" />
        <div class="imgViewDetails">
        </div>
    </div>
</div>