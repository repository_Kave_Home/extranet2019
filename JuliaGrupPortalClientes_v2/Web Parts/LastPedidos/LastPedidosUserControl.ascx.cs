﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.DataAccessObjects;
using System.Web.UI.HtmlControls;
using JuliaGrupUtils.Business;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.Data;

namespace JuliaGrupPortalClientes_v2.Web_Parts.LastPedidos
{
    public partial class LastPedidosUserControl : UserControl
    {
        private int numPedidos = 0;
        private string title = "title";
        private string subtitle = "subtitle";
        private string details = "details";
        private DataTable dtVentas;
        private User CurrentUser;

        List<Venta> ventas;
        private VentaDataAccessObject ventasDAO;

        #region public properties

            public string Subtitle
            {
                get { return subtitle; }
                set { subtitle = value; }
            }
            public string Title
            {
                get { return title; }
                set { title = value; }
            }
            public string Details
            {
                get { return details; }
                set { details = value; }
            }
            public int NumPedidos
            {
                get { return numPedidos; }
                set { numPedidos = value; }
            }
        #endregion public properties


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            this.subtitle = getText("FollowDetails");
            this.details = getText("seeDetails");
            lblLinkViewDetails.Text = this.details;
            int numventas = 0;
            try
            {
                if (Session["VentasDAO"] == null)
                {
                    //Carrego Ventas

                    Sesion SesionJulia = new Sesion();
                    ventasDAO = SesionJulia.GetVentas(((Client)Session["Client"]).Code);
                    if (ventasDAO == null)
                        throw new Exception("Error NULL en el GetVentas del cliente seleccionado");
                    Session.Add("VentasDAO", ventasDAO);
                }
                else
                {
                    ventasDAO = (VentaDataAccessObject)Session["VentasDAO"];
                }
                numventas = ventasDAO.TotalVentas();

                ventas = ventasDAO.GetLastVentas(this.numPedidos);
                this.title = string.Format(getText("lastOrdersTitle"), numventas);
                LoadData(ventas);
                RptLastOrdersHolder.DataSource = dtVentas;
                RptLastOrdersHolder.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Utils.Logger.WiteLog("LastPedidosUserControl->" + ex.Message, ex.StackTrace);
            }
        }

        private void LoadData(List<Venta> ventas)
        {
            try
            {
                dtVentas = new DataTable();
                #region Add Columns
                    dtVentas.Columns.Add("PedidoClass");
                    dtVentas.Columns.Add("PedidoClick");
                    dtVentas.Columns.Add("ventaNumber");
                    dtVentas.Columns.Add("ventaEstadoClass");
                    dtVentas.Columns.Add("ventaEstadoText");
                    dtVentas.Columns.Add("ventaImporte");
                    dtVentas.Columns.Add("ventaFecha");
                #endregion Add Columns
                foreach (Venta venta in ventas)
                {
                    //LastOrdersHolder.Controls.Add(GetOrderLine(venta));

                    DataRow dtr = dtVentas.NewRow();
                    dtr["PedidoClass"] = "pedidoCurso";
                    dtr["PedidoClick"] = String.Format("javascript:RowClick('{0}','{1}','{2}')", venta.Number, venta.VentaType(), venta.ClientCode);
                    dtr["ventaNumber"] = string.IsNullOrEmpty(venta.Number) ? "" : venta.Number;

                    #region Switch Estado
                    switch (venta.Estado)
                    {
                        //Valores campo Status
                        case "Open":
                            dtr["ventaEstadoClass"] = "openPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusOpen");
                            break;
                        case "Released":
                            dtr["ventaEstadoClass"] = "releasedPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusReleased");
                            break;
                        case "Pending_Approval":
                            dtr["ventaEstadoClass"] = "pendingPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusPendingAp");
                            break;
                        case "Pending_Prepayment":
                            dtr["ventaEstadoClass"] = "pendingPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusPendingPre");
                            break;

                        //Valores campo Status_Web
                        case "Pendiente":
                            dtr["ventaEstadoClass"] = "pendingPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusPending");
                            break;
                        case "En_curso":
                            dtr["ventaEstadoClass"] = "openPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusInProgress");
                            break;
                        case "Exp_Parcial":
                            dtr["ventaEstadoClass"] = "releasedPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusPartialReleased");
                            break;
                        case "Expedido":
                            dtr["ventaEstadoClass"] = "releasedPedidoCurso";
                            dtr["ventaEstadoText"] = getText("StatusReleased");
                            break;
                    }
                    #endregion Switch Estado
                    #region Get User Price
                    
                    if (Session["User"] != null)
                    {
                        CurrentUser = (User)Session["User"];
                    }
                    int priceselector = (int)Session["PriceSelector"];
                    if (priceselector == 2)
                    {
                        string strdivisa = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        dtr["ventaImporte"] = (venta.Puntos * CurrentUser.Coeficient).ToString("N2") + " " +
                            strdivisa;
                    }
                    else if (priceselector == 1)
                    {
                        string strdivisa = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        dtr["ventaImporte"] = venta.Importe.ToString("N2") + " " +
                            strdivisa;
                    }
                    else
                    {
                        dtr["ventaImporte"] = venta.Puntos.ToString() + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                    }
                    #endregion Get User Price
                    dtr["ventaFecha"] = (venta.Fecha == null) ? "" : getFormattedDate(venta.Fecha);
                    dtVentas.Rows.Add(dtr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        
        private HtmlGenericControl GetOrderLine(Venta venta)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            try
            {
                div.Attributes.Add("class", "pedidoCurso");
                div.Attributes.Add("OnClick", String.Format("javascript:RowClick('{0}','{1}','{2}')", venta.Number, venta.VentaType(), venta.ClientCode));

                HtmlGenericControl table = new HtmlGenericControl("table");
                table.Attributes.Add("class", "infoPedidoCurso");

                HtmlGenericControl tr = new HtmlGenericControl("tr");
                HtmlGenericControl td = new HtmlGenericControl("td");
                HtmlGenericControl div_title = new HtmlGenericControl("div");
                div_title.Attributes.Add("class", "titlePedidoCurso");
                div_title.InnerHtml = string.IsNullOrEmpty(venta.Number) ? "" : venta.Number;
                td.Controls.Add(div_title);
                tr.Controls.Add(td);

                HtmlGenericControl td_2 = new HtmlGenericControl("td");
                HtmlGenericControl div_estado = new HtmlGenericControl("div");
                switch (venta.Estado)
                {
                    //Valores campo Status
                    case "Open":
                        div_estado.Attributes.Add("class", "openPedidoCurso");
                        div_estado.InnerHtml = getText("StatusOpen");
                        break;
                    case "Released":
                        div_estado.Attributes.Add("class", "releasedPedidoCurso");
                        div_estado.InnerHtml = getText("StatusReleased");
                        break;
                    case "Pending_Approval":
                        div_estado.Attributes.Add("class", "pendingPedidoCurso");
                        div_estado.InnerHtml = getText("StatusPendingAp");
                        break;
                    case "Pending_Prepayment":
                        div_estado.Attributes.Add("class", "pendingPedidoCurso");
                        div_estado.InnerHtml = getText("StatusPendingPre");
                        break;

                    //Valores campo Status_Web
                    case "Pendiente":
                        div_estado.Attributes.Add("class", "pendingPedidoCurso");
                        div_estado.InnerHtml = getText("StatusPending");
                        break;
                    case "En_curso":
                        div_estado.Attributes.Add("class", "openPedidoCurso");
                        div_estado.InnerHtml = getText("StatusInProgress");
                        break;
                    case "Exp_Parcial":
                        div_estado.Attributes.Add("class", "releasedPedidoCurso");
                        div_estado.InnerHtml = getText("StatusPartialReleased");
                        break;
                    case "Expedido":
                        div_estado.Attributes.Add("class", "releasedPedidoCurso");
                        div_estado.InnerHtml = getText("StatusReleased");
                        break;


                }
                td_2.Controls.Add(div_estado);

                tr.Controls.Add(td_2);
                table.Controls.Add(tr);

                HtmlGenericControl tr_2 = new HtmlGenericControl("tr");
                HtmlGenericControl td_3 = new HtmlGenericControl("td");
                HtmlGenericControl div_desc = new HtmlGenericControl("div");
                div_desc.Attributes.Add("class", "descPedidoCurso");
                User CurrentUser = new User();
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                int priceselector = (int)Session["PriceSelector"];
                if (priceselector == 2)
                {
                    string strCurrency = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                    div_desc.InnerHtml = (venta.Puntos * CurrentUser.Coeficient).ToString("N2") + " " + strCurrency;
                }
                else if (priceselector == 1)
                {
                    string strCurrency = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                    div_desc.InnerHtml = venta.Importe.ToString("N2") + " " + strCurrency;
                }
                else
                {
                    div_desc.InnerHtml = venta.Puntos.ToString() + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                }
                
                td_3.Controls.Add(div_desc);
                tr_2.Controls.Add(td_3);

                HtmlGenericControl td_4 = new HtmlGenericControl("td");
                HtmlGenericControl div_date = new HtmlGenericControl("div");
                div_date.Attributes.Add("class", "datePedidoCurso");
                div_date.InnerHtml = (venta.Fecha == null) ? "" : getFormattedDate(venta.Fecha);

                td_4.Controls.Add(div_date);

                tr_2.Controls.Add(td_4);
                table.Controls.Add(tr_2);
                div.Controls.Add(table);

                HtmlGenericControl div_sep = new HtmlGenericControl("div");
                div_sep.Attributes.Add("class", "pedidoSeparator");
                div.Controls.Add(div_sep);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return div;
        }

        private string getFormattedDate(DateTime date)
        {  
            try
            {
                return date.Day.ToString() + "." + date.Month + "." + date.Year;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private string getText(string text)
        {
            return JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(text);
        }
    }
}
