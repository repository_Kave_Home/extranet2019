﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using JuliaGrupUtils.Utils;

namespace JuliaGrupPortalClientes_v2.Noticias
{
    [ToolboxItemAttribute(false)]
    public class Noticias : WebPart
    {

        #region Properties

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Pagina de Noticias")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string newsPage { get; set; }

        #endregion

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Noticias/NoticiasUserControl.ascx";

        #region Constructor

        public Noticias()
        {
            // Set default List Name
            this.newsPage = "/Pages/Not%c3%adcias.aspx";

            // Set default chrome state
            this.ChromeType = PartChromeType.None;
            this.ChromeState = PartChromeState.Normal;
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            // ADD USER CONTROL
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                try
                {
                    NoticiasUserControl control = (Page.LoadControl(_ascxPath) as NoticiasUserControl);

                    // Set control parameters
                    control.newsPage = this.newsPage;
                    control.wpTitle = LanguageManager.GetLocalizedString("NewsWebPartTitle");

                    this.Controls.Add(control);
                }
                catch (Exception ex)
                {
                    this.Controls.Add(new LiteralControl("Error building the web part: " + ex.Message));
                }
                
            });
        }

        #endregion
    }
}
