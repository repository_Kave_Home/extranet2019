﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoticiasUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Noticias.NoticiasUserControl" %>

<SharePoint:ScriptLink ID="ScriptLink1" name="SP.js" runat="server" OnDemand="true" localizable="false" />
<SharePoint:CssRegistration ID="CssRegistrationNoticias" name="/Style Library/Julia/Noticias.css" runat="server"/>

<div id="Global_container" runat="server">
  <h3 class="size_m">
    <asp:HyperLink ID="linkToList" runat="server">
        <asp:Label id="lblTituloWebPart" runat="server"/>
    </asp:HyperLink>
  </h3>
    <div id="all_news_list">
       <div class="all_news_pager">
        	<span class="actual"><asp:label ID="lbl1" runat="server" /></span>
            <asp:LinkButton id="btnNext" runat="server" text="Next" OnClick="cmdNext_Click"></asp:LinkButton>
            <asp:LinkButton id="btnPrevious" runat="server" text="Previous" OnClick="cmdPrev_Click"></asp:LinkButton>
       </div>
        <ul> 
            <asp:Repeater ID="listadonoticias" runat="server">
                <ItemTemplate>
                    <li class="">
         	                <a class="" href="<%# getUrlFromArticleDR(Eval("FileRef").ToString() , Eval("EncodedAbsUrl").ToString())%>">
         		                <!-- span class="time">Marzo 2013</span -->
         		                <span class="title"><%# Eval("Title")%></span>
         		                <span class="text"><%# Eval("JuliaNewsRollupText")%></span>
         	                </a>	
                    </li>
                </ItemTemplate>
                </asp:Repeater>                   		    
        </ul>
        <div class="all_news_pager">
        	<span class="actual"><asp:label ID="lbl2" runat="server" /></span>
            <asp:LinkButton id="btnNext2" runat="server" text="Next" OnClick="cmdNext_Click"></asp:LinkButton>
            <asp:LinkButton  id="btnPrevious2" runat="server" text="Previous" OnClick="cmdPrev_Click"></asp:LinkButton>
       </div>
    </div>

</div>