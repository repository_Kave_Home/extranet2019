﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Web;
using System.Collections.Generic;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;

using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;

using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using System.Globalization;
using JuliaGrupPortalClientes_v2.Web_Parts.UltimasNoticias;


namespace JuliaGrupPortalClientes_v2.Noticias
{
    public partial class NoticiasUserControl : UserControl
    {
        #region Properties

        public string newsPage { get; set; }
        public string wpTitle { get; set; }
        public int pageSize { get; set; }

        #endregion

        //private const int pageSize = 9;
        private const int minPage = 4;
        private int lastPage = 0;
        private const int numFeaturedNews = 2;


        #region Methods

        /// <summary>
        /// This method loads the ASCX control of the webpart
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                EnsureChildControls();

                //this.lblTituloWebPart.Text = wpTitle;
                this.lblTituloWebPart.Text = LanguageManager.GetLocalizedString("TittleNews"); ;
                this.linkToList.NavigateUrl = newsPage;
                //this.lblNewsPage.Text = LanguageManager.GetLocalizedString("NewsWebPartLinkText");
                //this.lblNewsCaption.Text = LanguageManager.GetLocalizedString("NewsWebPartLinkCaption");
                if (pageSize == null)
                    pageSize = 1;

            }
            catch (Exception ex)
            {
                this.Controls.Add(new LiteralControl("News Control Error: " + ex.Message + "\n"));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            int pageNum = CurrentPage;
            string CategoryFilter = string.Empty;
            //try
            //{
            //    pageNum = Convert.ToInt32(HttpContext.Current.Request.QueryString["page"]);
            //    CategoryFilter = HttpContext.Current.Request.QueryString["category"];
            //}
            //catch (Exception ex)
            //{
            //    pageNum = 0;
            //}
            //if (pageNum <= 0)
            //    pageNum = 1;
            //int numNews = pageSize * (pageNum + minPage + 1);
            //En nuestro caso queremos todas las noticias
            int numNews = 2000;

            DataTable allNews;
            allNews = JuliaNews.GetLastNewsAsDataTable((uint)numNews);

            //DataTable news = WPUtils.PageResults(pageSize, pageNum, out lastPage, allNews);

            if (allNews.Rows.Count == 0)
                Global_container.Visible = false;
            else
            {
                //Using PagedData Source:: http://www.codeproject.com/Articles/15629/Paging-using-PagedDataSource-class
                PagedDataSource Pds1 = new PagedDataSource();
                Pds1.DataSource = allNews.DefaultView;
                Pds1.AllowPaging = true;
                Pds1.PageSize = pageSize;
                Pds1.CurrentPageIndex = pageNum;
                lbl1.Text = "Page: " + (pageNum + 1).ToString() + " of " + Pds1.PageCount.ToString();
                lbl2.Text = "Page: " + (pageNum + 1).ToString() + " of " + Pds1.PageCount.ToString();
                btnPrevious.Enabled = !Pds1.IsFirstPage;
                btnNext.Enabled = !Pds1.IsLastPage;
                btnPrevious2.Enabled = !Pds1.IsFirstPage;
                btnNext2.Enabled = !Pds1.IsLastPage;
                listadonoticias.DataSource = Pds1;
                listadonoticias.DataBind();
            }
            base.OnPreRender(e);
        }

        public int CurrentPage
        {
            get
            {
                // look for current page in ViewState
                object o = this.ViewState["_CurrentPage"];
                if (o == null)
                    return 0; // default page index of 0
                else
                    return (int)o;
            }

            set
            {
                this.ViewState["_CurrentPage"] = value;
            }
        }

        public void cmdPrev_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the previous page
            CurrentPage -= 1;

            // Reload control
            //ItemsGet();
        }

        public void cmdNext_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the next page
            CurrentPage += 1;

            // Reload control
            //ItemsGet();
        } 

        public static string getUrlFromArticleDR(string FileRef, string EncodedAbsUrl)
        {
            string href = FileRef.Split('#')[1];
            string absUrl = (new Uri(EncodedAbsUrl)).GetLeftPart(UriPartial.Authority) + "/";
            return absUrl + href;
        }

        #endregion
    }
}
