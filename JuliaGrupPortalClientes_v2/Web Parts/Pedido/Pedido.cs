﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace JuliaGrupPortalClientes_v2.Pedido
{
    [ToolboxItemAttribute(false)]
    public class Pedido : WebPart
    {
        #region Properties

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Nombre de la lista de Pedido")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string communicationsListName { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDescription("Pagina de incidencias")]
        [System.ComponentModel.Category("Parámetros internos")]
        public string issuesPageURL { get; set; }

        #endregion

        #region Constructor

        public Pedido()
        {
            // Set default values
            this.issuesPageURL = "/Pages/Issues.aspx";

            // Set default chrome state
            this.ChromeType = PartChromeType.None;
        }

        #endregion

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/Pedido/PedidoUserControl.ascx";

        protected override void CreateChildControls()
        {
            if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["Debug"]))
            {
                this.Controls.Add(new LiteralControl("Communications List Name: " + communicationsListName));
            }

            try
            {
                PedidoUserControl control = (Page.LoadControl(_ascxPath) as PedidoUserControl);

                //Insert WebPart parameters
                control.IssuesPageURL = this.issuesPageURL;
                Controls.Add(control);
            }
            catch (Exception ex)
            {
                this.Controls.Add(new LiteralControl("Exception in web part: " + ex.Message));
            }
        }
    }
}
