﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidoUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Pedido.PedidoUserControl" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<!-- link href="/Style%20Library/Julia/Pedido.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationPedido" name="/Style Library/Julia/Pedido.css" runat="server"/>

<asp:Literal runat="server" ID="StyleBackImg" />

<!-- link href="/wpresources/Pedido.css" rel="stylesheet" type="text/css" / -->

<%--Webpart Nuevo Pedido (plegada)--%>
<div id="containerMinimized" class="containerNewOrderMin" >   
        <div class="groupOrder">
            <table>
                <tr class="orderLine">
                    <td class="columnOrderLine">
                        <asp:Label ID="lblLineTitle" CssClass="txtOrderLine" runat="server" />
                    </td>
                    <td class="columnOrderLine">
                        <asp:TextBox ID="orderTitle2" CssClass="inputOrderLine" Width="170px" runat="server" MaxLength="30" />
                    </td>
                </tr>
                <tr class="orderLine">
                    <td class="columnOrderLine"  id="lblPayMethod">
                        <asp:Label ID="lblLinePayMethod" CssClass="txtOrderLine" runat="server" />
                    </td>
                    <td class="columnOrderLine">
                        <telerik:RadComboBox ID="PayMethodList2" runat="server" Width="170px" DataTextField="value"
                            DataValueField="id" Visible="false" />
                        <asp:TextBox ID="TxtbPayMethod"  Enabled ="false" CssClass="inputOrderLine" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr class="orderLine">
                    <td class="columnOrderLine">
                        <asp:Label ID="lblLineDestAddress" CssClass="txtOrderLine" runat="server" />
                    </td>
                    <td class="columnOrderLine" id="columnOrder">
                        <telerik:RadComboBox ID="DestinationAddressList2" CssClass="DestinationAdress" runat="server" Width="176px"  DataTextField="value"
                            DataValueField="id" />
                    </td>
                </tr>
           
                <tr class="orderLine" >
                    <td class="columnOrderLine">
                        <asp:Label ID="lblLineDestinationExp" CssClass="txtOrderLine" runat="server" />
                    </td>
                    <td class="columnOrderLine" >
                       <asp:RadioButtonList runat="server" ID="ShipmentMethod" CssClass="radioOrderLine" 
                                RepeatDirection="Horizontal" Enabled ="true" AutoPostBack="true" 
                                OnSelectedIndexChanged="ShipmentMethod_selectedindexchanged">                    
                       </asp:RadioButtonList>   
       
                    </td>
                </tr>
            </table>
        </div>
    
	    <div id="btn_pedidos">

            <div id="realizar_pedido">        
                <asp:ImageButton ID="OrderBtn"  runat="server" ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/comanda.gif%>" 
                OnClick="PlaceOrder" />
             </div>
             <div id="pedido_express">
		        <asp:ImageButton ID="ExpressOrderBtn"  runat="server" 
                ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/pedido_express.gif%>" 
                OnClick="PlaceOrder" />
            </div>
	    </div>
  
        <%--<asp:Button ID="btExpand" CssClass="buttonExpand" BorderStyle="None" BackColor="Transparent" runat="server" />--%>
    </div>
<%--Link a pagina de incidencias--%>
<asp:HyperLink ID="linkToIssues" runat="server" >
           <a href="Issues.aspx" target="_self" ><asp:Image ID="ImageButton1"  runat="server" ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/incidencia.png%>"
               CssClass="imgIncidencia" /> </a>
</asp:HyperLink>
