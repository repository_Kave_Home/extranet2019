﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Drawing;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Microsoft.SharePoint;
using System.Data;
using Telerik.Web.UI;
using System.Threading;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupPortalClientes_v2.Pedido
{
    public partial class PedidoUserControl : UserControl
    {        
        public string IssuesPageURL { get; set; }        
        private ArticleDataAccessObject articleDAO;
        //private VentaDataAccessObject ventaDAO;
        public Order order;
        private User currentUser;

        public int expanded=0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    Initialize();                    
                }
                else
                {
                    //Sesion SesionJulia = new Sesion();
                    if (Session["User"] != null)
                    {
                        currentUser = (User)Session["User"];
                    }
                }
                //this.SetCatalogProducts();    //Not needed
            }
            catch (Exception ex)
            {
                //ErrorLabel.Text = ex.Message + ex.StackTrace;
                Logger.WiteLog(ex.Message, ex.StackTrace);
            }
        }

        public void PlaceOrder(object sender, ImageClickEventArgs e)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                this.order = (Order)Session["Order"];
                this.order.ReferenciaPedido = orderTitle2.Text;

                if (!String.IsNullOrEmpty(this.PayMethodList2.SelectedValue))
                {
                    this.order.PaymentMethod = this.PayMethodList2.SelectedValue;
                }
                if (!String.IsNullOrEmpty(this.DestinationAddressList2.SelectedValue))
                {
                    this.order.DeliveryAddress = this.DestinationAddressList2.SelectedValue;
                }
                this.order.DeliveryType = this.ShipmentMethod.SelectedValue;
                //if (this.order.OrderObservations == null)
                //{
                //    this.order.OrderObservations = "";
                //}


                //Session.Add("Order", order);
                //Session["Order"] = this.order;

                //this.order = (Order)Session["Order"];
                if (order != null)
                {
                    this.order.Update(currentUser.UserName);
                }
              

                if (((ImageButton)sender).ID == "OrderBtn")
                {
                    //Response.Redirect(LanguageManager.GetLocalizedString("WPPedido_ProductsUrl"), true);

                    //Response.Redirect("/Pages/Products.aspx");
                    Response.Redirect("/Pages/RedirectTo.aspx?r=products");
                }
                else
                {
                    //Response.Redirect(LanguageManager.GetLocalizedString("WPPedido_AdvancedOrderUrl"), true);
                    Response.Redirect("/Pages/advancedOrder.aspx");
                }
            }
            catch (ThreadAbortException)
            {
                // Do nothing. ASP.NET is redirecting.
                // Always comment this so other developers know why the exception 
                // is being swallowed.
                //Capturamos la excepción que por diseño ASP.Net lanza para Response.End o para Response.Redirect
                //Otra alternativa es mover el redirect fuera del catch
                //http://stackoverflow.com/questions/4368640/response-redirect-and-thread-was-being-aborted-error
                //http://briancaos.wordpress.com/2010/11/30/response-redirect-throws-an-thread-was-being-aborted/
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        
        private void Initialize()
        {

            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
            //this.PanelImgOrder.BackImageUrl = "/Style Library/Julia/img/" + strCultureName + "/bckground_pedido.png";

            StyleBackImg.Text = @"<style>
                                    .containerNewOrderMin {
                                        background-image: url('/Style Library/Julia/img/" + strCultureName + @"/bckground_pedido.png');
                                    }
                                    </style>";
            try
            {

                if (Session["Client"] != null)
                {
                    Client clAux = (Client)Session["Client"];
                }
                else throw new SPException("Error: Client session var is Null");


                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];
                }
                else throw new SPException("Error: User session var is Null");

                if (Session["ArticleDAO"] != null)
                {
                    this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                }
                else
                {
                    throw new SPException("Error: ArticleDAO session var is Null");
                }

                if (Session["Order"] != null)
                {
                    this.order = (Order)Session["Order"];

                }
                else
                {
                    throw new SPException("Error: Order session var is Null");
                }

                //if (Session["VentasDAO"] != null)
                //{
                //    this.ventaDAO = (VentaDataAccessObject)Session["VentasDAO"];
                //}
                //else
                //{
                //    throw new SPException("Error: VentasDAO session var is Null");
                //}

                //set the order to de user control                    
                //((MainPedidoCartUserControl)this.mainPedidoCart).SetOrderData(this.order);
                
                    this.orderTitle2.Text = this.order.ReferenciaPedido;

                
                this.LoadPayMethods();
                this.LoadAddresses();
                this.LoadShipmentMethods();
                                          
                this.SetEventHandlers();
                this.SetUrls();
                this.SetLabels();         
        
               
            }
            catch (Exception e)
            {
                //ErrorLabel.Text = e.Message + e.StackTrace;
                Logger.WiteLog(e.Message, e.StackTrace);
            }
        }       

        private void LoadPayMethods()
        {
            try
            {
                if (Session["Client"] != null)
                {
                    int i = 0;
                    foreach (string[] paymethods in ((Client)Session["Client"]).PayMethods)
                    {
                        RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(paymethods[1], paymethods[0]);
                        RadComboBoxItem aux2 = new Telerik.Web.UI.RadComboBoxItem(paymethods[1], paymethods[0]);

                        // si es el que toca o es buit
                        if (this.order.PaymentMethod == paymethods[0] || (String.IsNullOrEmpty(this.order.PaymentMethod) && i == 0))
                        {
                            aux.Selected = true;
                            aux2.Selected = true;
                        }

                        //this.PayMethodList.Items.Add(aux);
                        this.PayMethodList2.Items.Add(aux2);
                        i++;
                        //REZ:: Ponemos un textbox con la forma de pago
                        TxtbPayMethod.Text = (paymethods[1] != null) ? paymethods[1].ToString() : String.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadAddresses()
        {
            try
            {
                if (Session["Client"] != null)
                {
                    int i = 0;
                    foreach (string[] addresses in ((Client)Session["Client"]).Addresses)
                    {
                        RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);
                        RadComboBoxItem aux2 = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);

                        if (this.order.DeliveryAddress == addresses[0] || (String.IsNullOrEmpty(this.order.DeliveryAddress) && i == 0))
                        {
                            aux.Selected = true;
                            aux2.Selected = true;
                        }

                        //this.DestinationAddressList.Items.Add(aux);
                        this.DestinationAddressList2.Items.Add(aux2);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadShipmentMethods()
        {
            try
            {
                //load two values to show
                ListItem total = new ListItem(LanguageManager.GetLocalizedString("total"), "Complete");
                //per defecte
                total.Selected = true;
                ListItem parcial = new ListItem(LanguageManager.GetLocalizedString("partial"), "partial");


                if (this.order.DeliveryType.ToLower() == "Complete")
                    total.Selected = true;
                else if ((this.order.DeliveryType.ToLower() == "partial"))
                    parcial.Selected = true;
                else
                    total.Selected = true;

                if (this.order.DeliveryType == null)
                {
                    //si no entrega parcial marquem el total i deshabilitem pq no ho puguin tocar
                    total.Selected = true;
                    //RTP: faig que sempre ho puguin tocar
                    //total.Enabled = false;
                    //parcial.Enabled = false;
                }
                total.Enabled = true;
                parcial.Enabled = true;

                this.ShipmentMethod.Items.Add(total);
                this.ShipmentMethod.Items.Add(parcial);
                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

//        private void setExpandable()
//        {
//            string text = @"<script type=""text/javascript"">
//                                $(function () {
//                                    $("".linkNewOrder"").click(function Desplegar() {
//                                        $("".containerNewOrderMax"").css('display', 'block');
//                                        //$('.containerNewOrderMax').css({ opacity: 0.0, visibility: ""visible"" }).animate({ opacity: 1.0 }, 1000);
//                                    });
//                                    $("".linkCloseOrder"").click(function Plegar() {
//                                        //$('.containerNewOrderMax').css({ opacity: 1.0, visibility: ""visible"" }).animate({ opacity: 0.0 }, 1000);
//                                        $("".containerNewOrderMax"").css('display', 'none');
//                                    });
//                                });
//                            </script>";
//            this.setExpandedLiteral.Text = text;
//        }

//        private void setExpanded()
//        {
//            string text = @"<script type=""text/javascript"">                
//                $(function () {$("".containerNewOrderMax"").css('display', 'block');});
//                                    $("".linkNewOrder"").click(function Desplegar() {
//                                        $("".containerNewOrderMax"").css('display', 'block');
//                                        //$('.containerNewOrderMax').css({ opacity: 0.0, visibility: ""visible"" }).animate({ opacity: 1.0 }, 1000);
//                                    });
//                                    $("".linkCloseOrder"").click(function Plegar() {
//                                        //$('.containerNewOrderMax').css({ opacity: 1.0, visibility: ""visible"" }).animate({ opacity: 0.0 }, 1000);
//                                        $("".containerNewOrderMax"").css('display', 'none');
//                                    });
//                                });
//                        </script>";
//            this.setExpandedLiteral.Text = text;
//        }       

      
        #region Initializations

        private void SetUrls()
        {
            //this.linkToIssues.NavigateUrl = IssuesPageURL;
            //this.linkToIssuesExp.NavigateUrl = IssuesPageURL;
        }

        private void SetLabels()
        {
            // New Order Labels
            //this.lblLineTitle.Text = this.lblLineTitleExp.Text = LanguageManager.GetLocalizedString("orderTitle");
            //this.lblLinePayMethod.Text = this.lblLinePayMethodExp.Text = LanguageManager.GetLocalizedString("paymentMethod");
            //this.lblLineDestAddress.Text = this.lblLineDestAddressExp.Text = LanguageManager.GetLocalizedString("deliveryAddress");
            this.lblLineTitle.Text = LanguageManager.GetLocalizedString("orderTitle");
            this.lblLinePayMethod.Text = LanguageManager.GetLocalizedString("paymentMethod");
            this.lblLineDestAddress.Text = LanguageManager.GetLocalizedString("deliveryAddress");
            this.lblLineDestinationExp.Text = LanguageManager.GetLocalizedString("deliveryType");           
            //this.lblExpandOrder.Text = LanguageManager.GetLocalizedString("placeNewOrder");
            //this.lblSelectedProductsTitle.Text = LanguageManager.GetLocalizedString("selectedProducts");            
            //this.lblCloseOrder.Text = LanguageManager.GetLocalizedString("back");
        }

        //private void SetCatalogProducts()
        //{
        //    try
        //    {
        //        if (articleDAO == null)
        //        {
        //            if (Session["ArticleDAO"] != null)
        //            {
        //                this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
        //            }
        //            else
        //            {
        //                throw new SPException("Error: ArticleDAO session var is Null");
        //            }
        //        }

        //        Session.Add("AllArticlesList", articleDAO.GetCatalogProducts());
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
        //        throw new SPException(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}


        private void SetEventHandlers()
        {
            //this.ibPlaceNewOrder.Click += new ImageClickEventHandler(ibPlaceNewOrder_Click);            
        }

        #endregion
                

        #region Event Handlers
        
        void addProductToCart(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void btExpand_Click(object sender, EventArgs e)
        {
            Session["expandedPedido"] = expanded;
        }



        protected void ShipmentMethod_selectedindexchanged(object sender, EventArgs e)
        {
            try
            {
                if (ShipmentMethod.SelectedValue == "partial")
                {
                    string CostsMsg = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeliveryTypePartialCostsInfo");
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + CostsMsg + "', 330, 100,'Info'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        #endregion


        #region Private methods

        #endregion
    }
}
