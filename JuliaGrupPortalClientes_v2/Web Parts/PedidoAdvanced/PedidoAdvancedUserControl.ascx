﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidoAdvancedUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Web_Parts.PedidoAdvanced.PedidoAdvancedUserControl" %>

<SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/PedidoAdvanced.css" runat="server"/>
 <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                   
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
<script type="text/javascript">
    function OpenFile(url) {
        $.blockUI({ message: 'En Proceso' });
        window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
        $.unblockUI({ message: 'En Proceso' });
    }
    
    function GoBack() {
        //window.history.back();
        window.location.href = "/Pages/RedirectTo.aspx?r=back";
    }
    function sendOrderFocus() {
        $(function () {
            //document.getElementById('EnviarPedido').focus();  //No funciona en Chrome por un Bug...
            $(window).scrollTo($('#EnviarPedido'), 500);
        });
    }
     
</script>
<style type="text/css">
       .ms-WPBody td
    {
        font-size:inherit;
        }
        .rgMasterTable td
        {
            white-space:nowrap!important;
            }
            
        .left-box-cart
        {
            display:none!important;
        }
        .headerSearch { right:0;}
        #userselector { margin-right: 0; }
        #ctl00_ClientSelector_Panel1 { right: 0; }
        .PrintView
        {
            float:right;
            }
</style>
<script type="text/javascript">
    function callConfirm(msg) {
        radconfirm('<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("PortesMinimos1")%> ' + msg + '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("PortesMinimos2")%> ', confirmCallBackFn);
    }
    function callConfirmDelOrder(msg) {
        radconfirm(msg, confirmCallBackFnOrder);
    }
    function confirmCallBackFn(arg) {
        var ajaxManager = $find("<%=RadAjaxManager1.ClientID%>");
        if (arg) {
            ajaxManager.ajaxRequest('OK');
        }
        else {
            ajaxManager.ajaxRequest('Cancelar');
        }
    }
    function confirmCallBackFnOrder(arg) {
        var ajaxManager = $find("<%=RadAjaxManager1.ClientID%>");
        if (arg) {
            ajaxManager.ajaxRequest('OKOrder');
        }
        else {
            ajaxManager.ajaxRequest('Cancelar');
        }
    }
    function alertRetorno(arg) {
        window.location = "/Pages/home.aspx";
    }
    function Show(message, titleMsg) {
        var htmlP = document.createElement('p');
        var htmlMsg = document.createTextNode(message);
        htmlP.appendChild(htmlMsg);
        var options = {
            html: htmlP,
            title: titleMsg,
           width: 900,
            height:600,      
            dialogReturnValueCallback: ShowStatus
        };
        SP.UI.ModalDialog.showModalDialog(options);
    }

    function ShowError(message, titleMsg) {
        var htmlP = document.createElement('p');
        var htmlMsg = document.createTextNode(message);
        htmlP.appendChild(htmlMsg);
        var options = {
            html: htmlP,
            title: titleMsg,
             width: 900,
            height:600                 
        };
        SP.UI.ModalDialog.showModalDialog(options);
    }

    function ShowStatus(dialogResult, retValue) {
        window.location = "/Pages/home.aspx";
    }
    function RedirectOrderDel() {
        window.location = "/Pages/advancedOrder.aspx";
    }

    function DisableCombo(field) {
        if (field == "Cliente") {
            var combo = $find("<%= recogidaValue.ClientID %>");
            combo.disable();
            var combo2 = $find("<%= entregaValue2.ClientID %>");
            combo2.enable();
        } else {
            var combo = $find("<%= entregaValue2.ClientID %>");
            combo.disable();
            var combo2 = $find("<%= recogidaValue.ClientID %>");
            combo2.enable();
        }
        $.blockUI({ message: $('#loadingPnl') });
    }

</script>

<asp:Literal ID="ShowModalToUsers" runat="server"></asp:Literal>

 <div id="container">
    <div id="titol" class="title">
        <asp:Label ID="Pedido_ConfirmacionpedidoLbl" runat="server"></asp:Label>: 
        <asp:Label ID="PedidoNo" runat="server"></asp:Label>
         <asp:ImageButton ID="PrintCurentView"  OnClick="PrintPDF_Click"   ImageUrl="/Style%20Library/Julia/img/imprimir.png" 
         CssClass="PrintView" 
         onmouseover="this.src='/Style%20Library/Julia/img/imprimir_on.png'"  
         onmouseout="this.src='/Style%20Library/Julia/img/imprimir.png'"
         runat="server" />
    </div>
    <div id="ContainerTop">

        <div id="ContainerLeft">
            <div id="numeroPedido">
                <div id="numeroPedidolbl">
                    <asp:Label ID="Pedido_ClientLabel" runat="server"></asp:Label>
                    </div>
                <div id="clientevalue">
                    <asp:Label ID="clienteValue" runat="server" Width="335px"></asp:Label></div>
            </div>
            <div id="referencia">
                <div id="referenciaLbl">
                    <asp:Label ID="Pedido_referenciaLbl" runat="server"></asp:Label>
                    </div>
                <div id="referenciavalue">
                    <asp:TextBox ID="referenciaValue" runat="server" Width="335px"  MaxLength="30" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox></div>
            </div>
            <div id="formadepago">
                <div id="formadepagoLbl">
                   <asp:Label ID="Pedido_formapagoLbl" runat="server"></asp:Label>
                   </div>
                <div id="formadepagovalue">
                    <telerik:RadComboBox ID="formadepagoValue" runat="server" Enabled="false" Width="340px"  DataTextField="value"
                        DataValueField="id" />
                </div>
            </div>
            <div id="TerminisPagement">
                <div id="TerminisPagementLbl">
                    <asp:Label ID="Terminios_formapagoLbl" runat="server"></asp:Label>
                </div>
                <div id="Terminiosdepagovalue">
                    <telerik:RadComboBox ID="TerminiosdepagoValue" runat="server" Enabled="false" Width="340px" 
                        DataTextField="value" DataValueField="id" />
                </div>
            </div>
            <div id="direccionentrega" runat="server">
                <div id="entregalbl">
                    <asp:Label ID="Pedido_direccionentregaLbl" runat="server"></asp:Label>
                    </div>
                <div id="entregavalue">
                    <telerik:RadComboBox ID="entregaValue" runat="server" Width="340px" MaxHeight="200px"  DataTextField="value"
                        DataValueField="id"  />
                </div>
            </div>
            <div id="NewDireccionEntregaBlock" runat="server">
                <asp:Label ID="Pedido_entregaLbl" runat="server"></asp:Label>
                <div>
                    <asp:RadioButton ID="Cliente" GroupName="EntregaSystem" runat="server" onclick="DisableCombo('Cliente')"
                        OnCheckedChanged="Entrega_SelectedIndexChanged" AutoPostBack="true"/>
                    <div id="Div3" style="display:inline;" >
                        <telerik:RadComboBox ID="entregaValue2" runat="server" Width="340px" CssClass="EntregaCombo" MaxHeight="200px" DataTextField="value"
                            DataValueField="id"
                            />
                    </div>
                </div>
                <div style="margin-top:1rem">
                    <asp:RadioButton ID="Almacen" GroupName="EntregaSystem" runat="server" onclick="DisableCombo('Almacen')" 
                        OnCheckedChanged="Entrega_SelectedIndexChanged" AutoPostBack="true"/>
                    <div id="Div4" style="display:inline;" >
                        <telerik:RadComboBox ID="recogidaValue" runat="server" Width="340px" CssClass="EntregaCombo" MaxHeight="200px" DataTextField="value"
                            DataValueField="id" 
                             />
                    </div>
                </div>
            </div>
            <div id="tipopedido" runat="server" visible="false" class="tipopedido" style="width:102%;float:left">
                <div id="tipopedidolbl">
                    <asp:Label ID="Pedido_tipopedidoLbl" runat="server"></asp:Label>
                    </div>
                <div id="tipopedidovalue">
                    <telerik:RadComboBox ID="tipopedidoValue" runat="server" Width="88px" MaxHeight="200px"  DataTextField="value"
                        DataValueField="id"  OnSelectedIndexChanged="tipopedidoValue_selectedindexchanged" AutoPostBack="true" />
                    <asp:TextBox ID="CodCampanyaValue" Enabled="false" runat="server" Width="245px"  MaxLength="30" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
                </div>
            </div>           
        </div>
        <div id="ContainerRight">
		<div id="tipoentrega">
                <div id="tipoentregaLbl">
                     <asp:Label ID="Pedido_tipoentregaLbl" runat="server"></asp:Label></div>
                <div id="tipoentregavalue">
                    <asp:RadioButtonList runat="server" ID="tipoEntregaValue" CssClass="radioOrderLine"
                        RepeatDirection="Horizontal" Enabled ="true" AutoPostBack="true" OnSelectedIndexChanged="tipoEntregaValue_selectedindexchanged">
                    </asp:RadioButtonList>
                </div>
            </div>
              <div id="fechaRequerida">
                <div id="fechaRequeridalbl">
                     <asp:Label ID="Pedido_fechaRequeridalbl" runat="server"></asp:Label></div>
                <div id="fechaRequeridavalue">
                      <telerik:RadDatePicker runat="server"  ID="FechaRequeridaValue" Culture="Spanish (Spain)" Width="130px" onkeydown = "return (event.keyCode!=13);" >
                        <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                        </Calendar>
                    </telerik:RadDatePicker>
                </div>
            </div>
            <div id="observaciones">
                <div id="observacionesLbl">
                    <asp:Label ID="Pedido_observacionesLbl" runat="server"></asp:Label></div>
                <div id="observacionesvalue">
                    <telerik:RadTextBox ID="observacionesValue" runat="server" MaxLength="160" TextMode="MultiLine" CssClass="observacionesValue" Width="200px">
                    </telerik:RadTextBox></div>
            </div>
        </div>
    </div>
    <div id="Header_Advanced">
            <div id="titol2">
                <asp:Label ID="Pedido_detallespedidoLbl" runat="server"></asp:Label>
                  
            </div>
       <div class="Guardar_sin_enviar">
                    <asp:ImageButton runat="server" ID="Button_save_without_send"  OnClick="PlaceOrder"   
                        OnClientClick="$.blockUI({ message: $('#loadingPnl') });"
                    ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/guardar_NoSend.gif%>" 
                    onmouseover="this.src=this.src.replace('guardar_NoSend','guardar_NoSend_on');"  
                    onmouseout="this.src=this.src.replace('guardar_NoSend_on','guardar_NoSend');"  
                    
                    />
                </div>
        </div>
    <div id="ContainerBottom">
        

<telerik:RadAjaxLoadingPanel ID="LoadingPanel" runat="server" Skin="Default" />


<asp:Panel ID="PedidoPanel" runat ="server"> 
        <div class="tableOrderList">
                <telerik:RadGrid ID="RadGrid1"  Width="100%" AllowSorting="False" OnNeedDataSource="RadGrid1_NeedDataSource" 
                HeaderStyle-CssClass="PedidoAdvancedHeaderClass" CssClass="Grid"
                    AutoGenerateColumns="false" PageSize="9999" AllowPaging="True"
                    OnItemCommand="RadGrid1_ItemCommand" OnItemDeleted="RadGrid1_ItemDeleted" 
                    OnItemInserted="RadGrid1_ItemInserted" OnItemUpdated="RadGrid1_ItemUpdated" 
                    EnableAjaxSkinRendering="true" AllowMultiRowSelection="True" 
                    runat="server" GridLines="None" ondatabound="RadGrid1_DataBound" 
                    OnItemDataBound="RadGrid1_ItemDataBound"
                    ShowFooter="True" EnableLinqExpressions="false"
                    AllowAutomaticUpdates="true"
                    Style="width: auto;"
                    Skin="Metro"                    
                     >
                    <ClientSettings  EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
                    <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Bottom"
                        UseAllDataFields="true" InsertItemDisplay="Bottom" InsertItemPageIndexAction="ShowItemOnCurrentPage">
                      
                        <Columns>
                            <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                            <telerik:GridBoundColumn Visible="false" DataField="LineNo" HeaderText="LineNo"
                                UniqueName="LineNo"  />
                            <telerik:GridHTMLEditorColumn UniqueName="SmallImage" DataField="SmallImage" />
                            <telerik:GridBoundColumn DataField="ArticleCode"  UniqueName="ArticleCode" />
                            <telerik:GridBoundColumn DataField="Description" UniqueName="Description"
                                ReadOnly="true" />
                                <telerik:GridBoundColumn Aggregate="Sum"    FooterText=" "  DataField="Volumen" UniqueName="Volumen" DataFormatString="{0:N3}"
                                ReadOnly="true"   />
                            <telerik:GridBoundColumn DataField="LineReference" HeaderText="Line Reference"
                                UniqueName="LineReference" />
                              <telerik:GridTemplateColumn UniqueName="Stock" HeaderText="Stock" DataType="System.String" >
                              <ItemTemplate>
                                    <asp:Image ImageUrl='<%#Eval("StockImage")%>' Width="15px"  runat="server" CssClass="StockImg" ID="ImgStock" />
                                    <telerik:RadToolTip runat="server" ID="RadToolTip1"  TargetControlID="ImgStock" Width="100px" IsClientID="false"
                                        ShowEvent="OnMouseOver" CssClass="Tooltip" Visible='<%# Convert.ToBoolean(Eval("ShowToolTip").ToString())%>'  HideEvent="Default" Position="TopRight" RelativeTo="Mouse" Animation="None"  
                                        BackColor="ButtonHighlight" >
                                        <asp:Label ID="lblStockTooltip" Width="160px" Text='<%#Eval("StockText")%>' runat="server" />
                                    </telerik:RadToolTip>
                                    </ItemTemplate>
                            </telerik:GridTemplateColumn>
                      
                             
                        


                                 
                            <telerik:GridBoundColumn DataField="Num" HeaderText="Num." DataType="System.Int32" 
                                UniqueName="Num" ></telerik:GridBoundColumn>
                                    <%--DataFormatString="{0:c}"--%>
                            <telerik:GridBoundColumn DataField="Discount"  UniqueName="Discount"
                                ReadOnly="true" />
                            <telerik:GridBoundColumn DataField="Price" HeaderText="€" UniqueName="Price" ReadOnly="true">
                                    <ItemStyle CssClass="ColumnPrice" />
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn UniqueName="EditColumn"  CommandName="Edit" ButtonType="ImageButton" />
                            <telerik:GridButtonColumn UniqueName="DeleteColumn"  CommandName="Delete" ButtonType="ImageButton"
                                ConfirmDialogType="RadWindow" >
                            </telerik:GridButtonColumn>
                        </Columns>
                    
                        <EditFormSettings EditFormType="Template">
                            <FormTemplate>
                                <div class="addItemText" style="color:Black;">
                                    <br>
                                    <asp:Label ID="Title" runat="server" 
                                        >
                                    </asp:Label>                               
                                </div>
                                <div class="EditProdField">
                                    <div class="addItemField codeComboNew">
                                        <telerik:RadTextBox Display="false"  ReadOnly="true" Width="80px" Height="24px" ID="codiArticleForm" CssClass="CodiArticleFormu" runat="server" Text='<%# Bind("ArticleCode") %>'></telerik:RadTextBox>                            
                                        <telerik:RadTextBox Display="false"  ReadOnly="true" Width="80px" Height="24px" ID="CatalogArticleForm" CssClass="CatalogArticleForm" runat="server" Text='<%# Bind("Catalog") %>'></telerik:RadTextBox>  
                                        <telerik:RadTextBox Display="false"  ReadOnly="true" Width="80px" Height="24px" ID="VersionArticleForm" CssClass="VersionArticleForm" runat="server" Text='<%# Bind("Version") %>'></telerik:RadTextBox>  
                                        <telerik:RadComboBox 
                                            LoadingMessage="Loading..."
                                            ID="RadComboBox1" 
                                            runat="server" 
                                            Height="200px" 
                                            Width="455px"                                    
                                            DataTextField="DescriptionCombo" 
                                            DataValueField="ValueCombo"                                        
                                            OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged"
                                                       
                                            AutoPostBack="true"
                                            EnableLoadOnDemand="true"
                                            OnItemsRequested="RadComboBox1_ItemsRequested"
                                            enableitemcaching="True" 
                                            AllowCustomText="false"     
                                            MarkFirstMatch="false"
                                            CssClass="ComboEdit"
                                            Visible="true">
                                        </telerik:RadComboBox>
                                   </div>
                                    
                                  
                                    <div class="addItemField">
                                    <telerik:RadTextBox  ID="lineReferenceForm" Visible="true" CssClass="LineReferenceForm" MaxLength="30" Width="135px" Height="22px" runat="server"    Text='<%# Bind("LineReference") %>'>
                                      
                                    </telerik:RadTextBox>
                                        
                                    </div>
                                       <div class="addItemField" style="width:30px;height:24px;">   
                                             <asp:Image ImageUrl='<%#Eval("StockImage")%>' Width="15px" Visible="false" runat="server" CssClass="StockImg" ID="ImgStock" />
                                                <telerik:RadToolTip runat="server" ID="RadToolTip1"  TargetControlID="ImgStock" Width="100px" IsClientID="false"
                                                    ShowEvent="OnMouseOver" CssClass="Tooltip" HideEvent="Default" Position="TopRight" RelativeTo="Mouse" Animation="None"  
                                                    BackColor="ButtonHighlight" >
                                                    <asp:Label ID="lblStockTooltip" Width="160px" Text='<%#Eval("StockText")%>' runat="server" />
                                                </telerik:RadToolTip>
                                    </div>
                                    <div class="addItemField numQtyItemEdit">
                                    <telerik:RadNumericTextBox Height="22px" Visible="true" MaxLength="3" CssClass="UnitPerBoxEdit" LabelWidth="70px" ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true"  runat="server"
                                            ID="numForm" Width="70px"  MinValue="1" NumberFormat-DecimalDigits="0" DbValue='<%# Eval("Num") %>' >
                                              <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                              </telerik:RadNumericTextBox>                                
                                    </div>
                                    <div class="addItemButton">                                
                                        <asp:LinkButton ID="LinkButton1" Width="11px" Height="11px"  runat="server" CommandName='<%# ((bool)DataBinder.Eval(Container, "OwnerTableView.IsItemInserted")) ? "SaveLine" : "UpdateLine" %>' CssClass="savebut"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" Width="11px" Height="11px"  runat="server" CausesValidation="False" CommandName="Cancel" CssClass="raditemButton cancelEditAdd"></asp:LinkButton>
                                        <asp:Label runat="server" ID="ErrorTxt" Visible="false"></asp:Label>
                                    </div>
                                    <telerik:RadTextBox Display="false"  ReadOnly="true"  ID="LineNoForm" runat="server" Text='<%# Bind("LineNo") %>'></telerik:RadTextBox>                            
                                </div>
                            
                            </FormTemplate>
                        </EditFormSettings>
                        <EditItemStyle ForeColor="Gray"></EditItemStyle>
                    
                    </MasterTableView>
                    <PagerStyle Mode="NextPrev" />
                
                </telerik:RadGrid>
            <asp:ImageButton runat="server" CssClass="BorrarCarrito" Width="26px" ID="Button_Delete_Order" OnClick="DeleteOrder" onmouseover="this.src='/Style Library/Julia/img/Borrar_carritoHover.png'"  onmouseout="this.src='/Style Library/Julia/img/Borrar_carrito.png'" ImageUrl="/Style Library/Julia/img/Borrar_carrito.png" />
            <div style="width: auto;">
                 <table class="rgCommandRow" style="float:right; width:50%;margin-right:-25px;">
                            <tbody>
                                 <tr class="rgCommandRow" style="border-top:none;border">
                                    <td>
                                    
                                        <table class="table_source" style="width:100%;" cellpadding="0px" cellspacing="0px">
                                              <asp:Panel ID="PanelDescuentopp" runat="server" >
                                            <tr>
                                                <td class="caption_sourceTable">
                                                    <div class="lblfinal_field1"><asp:Label ID="LblPPD" runat="server"></asp:Label></div>
                                                </td>
                                                <td>
                                                    <div class="lblfinal_field2" ><asp:Label ID="LblPPDValue" class="TotalDiscountValue" runat="server" Text=""></asp:Label></div>
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td class="caption_sourceTable">
                                                    <div class="lblfinal_field3"><asp:Label ID="LblTotal" runat="server" ></asp:Label></div>
                                                </td>
                                                  
                                                <td>
                                                    <div class="lblfinal_field4"><asp:Label ID="LblTotalValue" CssClass="TotalValue" runat="server" Text=""></asp:Label></div>
                                                </td>
                                            </tr>
                               
                                
                                        </table>
                
                                    </td>
                                </tr>
                            </tbody>
                  </table>
            </div>
           </div>

 </asp:Panel>
   </div>           
 </div>
 <div class="PanelBtnsFooter" id="PanelBtnsFooter">
    <div class="BtnSeguirComprando">
       <img alt="" onclick="GoBack()" 
            onmouseover="this.src=this.src.replace('seguir-comprando','seguir-comprando_on');"  
            onmouseout="this.src=this.src.replace('seguir-comprando_on','seguir-comprando');"  
            style="cursor:pointer;"
            src="
            <asp:Literal ID="Literal1" runat="server" Text="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/seguir-comprando.png%>" />
       " />
    </div>
    <div class="EnviarPedido"  id="EnviarPedido">
        <asp:ImageButton ID="SendPedido"  OnClick="SendPedido_Click" runat="server" 
        ImageUrl="<%$SPUrl:~sitecollection/Style Library/Julia/img/~language/confirmar.gif%>" 
        onmouseover="this.src=this.src.replace('confirmar','confirmar_on');"  
        onmouseout="this.src=this.src.replace('confirmar_on','confirmar');"  
        
        />
    </div>
    
 </div>    


<script type="text/javascript">
    //sendOrderFocus();

    $(".rgCommandTable").find("img")
    .hover(function () {
        $(this).attr("src", $(this).attr("src").replace("add_to_car","add_to_car_on"));
    }, function () {
        $(this).attr("src", $(this).attr("src").replace("add_to_car_on", "add_to_car"));
    })

</script>

    <telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="450px" Width="900px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None" Title="Unsaved changes pending">
    <ContentTemplate>
        <div style="padding: 20px">
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            <br></br>

            <asp:Label ID="lblConfirm" runat="server" Text=""></asp:Label>
            <br></br>
            <asp:Label ID="lblDesc1" runat="server" Text=""></asp:Label>
            <br></br>
            <asp:Label ID="lblDesc2" runat="server" Text=""></asp:Label>
            <br></br>
            <asp:Label ID="lblDesc3" runat="server" Text=""></asp:Label>
          <br />
      
        
            <div style="padding: 20px" align="right">
            <asp:Button ID="btnOk" runat="server" Text="Ok" Width="50px" OnClick="btnOk_Click" />
            </div>

        </div>
    </ContentTemplate>
    </telerik:RadWindow>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" />