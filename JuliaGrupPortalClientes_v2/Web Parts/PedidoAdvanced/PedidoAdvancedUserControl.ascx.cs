﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using Telerik.Web.UI;
using JuliaGrupUtils.Utils;
using System.Data;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SharePoint;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using System.IO;
using System.Globalization;
using System.Threading;

namespace JuliaGrupPortalClientes_v2.Web_Parts.PedidoAdvanced
{
    public partial class PedidoAdvancedUserControl : UserControl
    {
        private Order CurrentOrder;
        private User currentUser;
        private Client CurrentClient;
        string imgURL = "";
        public Article currentArticle;

        private string ventaCode = string.Empty;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            EnsureChildControls();
            if (this.CurrentOrder == null){
                //Cargamos siempre la order de NAV
                Sesion SesionJulia = new Sesion();
                Client cl = (Client)Session["Client"];
                Order p = SesionJulia.GetOrder(cl);
                Session.Add("Order", p);

                this.CurrentOrder = (Order)Session["Order"];
            }

            if (!IsPostBack)
            {
                this.CurrentClient = this.CurrentOrder.OrderClient;
                this.Initialize();
            }
            else
            {
                if (!Page.IsCallback)
                {
                    this.CurrentClient = this.CurrentOrder.OrderClient;
                }
             
                
            }

            //Si estamos en vista PVP no mostramos el descuento
            int iPriceSel = (int)Session["PriceSelector"];
            if ((iPriceSel == 2) || (iPriceSel == 4))
            {
                this.RadGrid1.Columns.FindByUniqueName("Discount").Display = false;
            }

            
        }
        //protected void DeleteOrder(object sender, EventArgs e)
        //{
           

        //}
        private void Initialize()
        {
            try
            {
                Sesion SesionJulia = new Sesion();
                //CurrentOrder = (Order)Session["Order"];   //REZ09042016
                //load title
                this.referenciaValue.Text = this.CurrentOrder.ReferenciaPedido;
                //load forma de pago
                this.LoadPayMethodsAndTherms(CurrentOrder);
                //load direccion de entrega
                this.LoadAddresses(CurrentOrder);
                //load tipo de entrega
                this.LoadShipmentMethods();
                //load observaciones
                this.observacionesValue.Text = this.CurrentOrder.OrderObservations;
                //load client name
                this.clienteValue.Text = this.CurrentClient.Code + " - " + this.CurrentOrder.CustomerName;
                this.PedidoNo.Text = this.CurrentOrder.OrderNumber;
                this.tipoEntregaValue.SelectedValue = this.CurrentOrder.DeliveryType;

                this.FechaRequeridaValue.Calendar.DateTimeFormat.CalendarWeekRule = System.Globalization.CalendarWeekRule.FirstFourDayWeek;
                this.FechaRequeridaValue.MinDate = System.DateTime.Now;
                if (CurrentOrder.FechaRequerida >= System.DateTime.Now)
                {
                    this.FechaRequeridaValue.SelectedDate = CurrentOrder.FechaRequerida;
                }

                //REZ30062014 - Nuevo campo TipoCampaña
                this.LoadTiposPedido(CurrentOrder.Campaña);
                this.CodCampanyaValue.Text = CurrentOrder.Campaña;
                
                
                //load all selected articles
                //this.LoadSelectedArticles();
                //RadComboBox combo = (RadComboBox)RadGrid1.FindControl("RadComboBox1");
                //ArticleDataAccessObject articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                
                
                //combo.DataSource = articleDAO.GetAllProducts();
                //combo.DataBind();

                this.Pedido_ClientLabel.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ClientLabel");
                this.Pedido_referenciaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_referenciaLbl");
                this.Pedido_formapagoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_formapagoLbl");
                this.Terminios_formapagoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Terminios_formapagoLbl");
                this.Pedido_direccionentregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_direccionentregaLbl");
                this.Pedido_tipoentregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_tipoentregaLbl");
                this.Pedido_observacionesLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_observacionesLbl");
                this.Pedido_detallespedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_detallespedidoLbl");
                this.Pedido_ConfirmacionpedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ConfirmacionpedidoLbl");
                this.Pedido_fechaRequeridalbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_FechaRequerida");
                this.Pedido_tipopedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_TipoPedido");

                this.Pedido_entregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_entregaLbl");
                //this.Pedido_recogidaalmacenLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_recogidaalmacenLbl");
                //this.Pedido_direccionentrega2Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_direccionentregaLbl");

                Cliente.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_direccionentregaLbl");
                Almacen.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_recogidaalmacenLbl");


                if (this.CurrentClient.RecogidaAlmacen)
                {
                    this.direccionentrega.Visible = false;
                    this.NewDireccionEntregaBlock.Visible = true;
                    SetTipoRecogidaCombos();
                }
                else
                {
                    this.direccionentrega.Visible = true;
                    this.NewDireccionEntregaBlock.Visible = false;
                }

                if (this.CurrentClient.PuedeCrearCampañas) {
                    tipopedido.Visible = true;
                }
             
                this.RadGrid1.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Num").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Discount").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Discount");
                //this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price");
                this.RadGrid1.Columns.FindByUniqueNameSafe("LineReference").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblLineDescription");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Volumen").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Volumen");

                int iPriceSel = (int)Session["PriceSelector"];
                if (iPriceSel == 0)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") + ")";
                }
                else if (iPriceSel == 1)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
                }
                else if (iPriceSel == 2)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") + ")";
                }
                else if (iPriceSel == 3) {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = CurrentClient.LiteralDivisaWEB;
                }
                else if (iPriceSel == 4) {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                }

                ((GridButtonColumn)RadGrid1.MasterTableView.GetColumnSafe("DeleteColumn")).ConfirmText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteProduct");                
            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void SetTipoRecogidaCombos() {
            if (!String.IsNullOrEmpty(this.CurrentOrder.PuntoRecogida))
            {
                if (this.CurrentOrder.PuntoRecogida == Order.RecogidaAlmacen.almacen.ToString())
                {
                    this.Almacen.Checked = true;
                    this.entregaValue2.Enabled = false;
                    this.recogidaValue.Enabled = true;
                }
                else
                {
                    this.Cliente.Checked = true;
                    this.entregaValue2.Enabled = true;
                    this.recogidaValue.Enabled = false;
                }
            }
        }

        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadSelectedArticles();
            //this.RadGrid1.DataBind();
           
        }

        private void LoadSelectedArticles()
        {
            if (Session["User"] != null)
            {
                currentUser = (User)Session["User"];
            }
            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
            try
            {
                DataTable data = new DataTable();
                data.Columns.Add("SmallImage");
                data.Columns.Add("ArticleCode");
                data.Columns.Add("Description");
                data.Columns.Add("LineReference");
                data.Columns.Add("Volumen", typeof(Decimal));
                data.Columns.Add("StockImage");
                data.Columns.Add("ShowData");
                data.Columns.Add("StockText");
                data.Columns.Add("Num", typeof(Int32));
                data.Columns.Add("Discount");
                data.Columns.Add("Price");
                data.Columns.Add("ShowToolTip");
                data.Columns.Add("Type");
                data.Columns.Add("LineNo");
                data.Columns.Add("Catalog");
                data.Columns.Add("Version");

                if (this.CurrentOrder != null)
                {
                    foreach (OrderLine ol in this.CurrentOrder.ListOrderProducts)
                    {
                        DataRow row = data.NewRow();
                       
                        row["ArticleCode"] = ol.Product.Code;
                        row["SmallImage"] = "<a href='" + "/Pages/productInfo.aspx?code=" + ol.Product.Code + "&catalog=" + ol.Product.InsertcatalogNo + "&version=" + ol.Product.ArticleVersion + "'><img src=" + ol.Product.SmallImgUrl + " alt='' width='50px' height='50px' onerror='this.src=\"/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg\";' /></a>";//falta dada
                        row["Description"] = ol.Product.Description;
                        row["LineReference"] = ol.LineDescription;
                        row["Volumen"] = ol.Volumen;
                        row["Type"] = ol.Type;
                        if (ol.Type == "Item")
                        {
                            row["StockImage"] = setStock(ol.Product.Stock);
                            row["StockText"] = setStockText(ol.Product.Stock, ol.Product.DescripcionGris);
                            row["ShowToolTip"] = "true";

                        }
                        else {
                            row["StockImage"] = "/Style%20Library/Julia/img/null_pixel.png";
                            row["StockText"] = "";
                            row["ShowToolTip"] = "false";
                        }
                        row["Num"] = ol.Quantity;

                        row["Discount"] = ol.Discount.ToString("N2") + " %";
                        row["LineNo"] = ol.LineNo;

                        int iPriceSel = (int)Session["PriceSelector"];
                        CultureInfo culture = new CultureInfo("es-Es", false);
                        if (iPriceSel == 0)
                        {
                            row["Price"] = ol.Points;
                        }
                        else if (iPriceSel == 1)
                        {
                            row["Price"] = ol.Price.ToString("N2", culture);
                        }
                        else if (iPriceSel == 2)
                        {
                            row["Price"] = ((Convert.ToDouble(ol.Points, culture)) * (Convert.ToDouble(currentUser.Coeficient, culture))).ToString("N2");
                        }
                        else if (iPriceSel == 3)
                        {
                            row["Price"] = (
                                            (Convert.ToDouble(ol.Price, culture)) * 
                                            (Convert.ToDouble(currentUser.Coeficient, culture)) *
                                            (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                            ).ToString("N2");
                        }
                        else if (iPriceSel == 4)
                        {
                            row["Price"] = (
                                            (Convert.ToDouble(ol.Points, culture)) *
                                            (Convert.ToDouble(currentUser.Coeficient, culture)) *
                                            (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                            ).ToString("N2");
                        }

                        row["Catalog"] = ol.Product.InsertcatalogNo;
                        row["Version"] = ol.Product.ArticleVersion;
                        data.Rows.Add(row);
                    }

                }

                this.RadGrid1.DataSource = data;
                //this.RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }




        private object setStockText(decimal p, string GrisDesc)
        {
            string output = string.Empty;

            int Stock = 0;
            Stock = Convert.ToInt32(p);
            switch (Stock)
            {
                case 0:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                    break;
                case 1:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                    break;
                case 2:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                    break;
                case 3:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                    break;
                case 4:
                    output = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), GrisDesc);
                    break;
            }
            return (output);
        }

        //private String loadStock(decimal p)
        //{
        //    String ret = null ;
        //    int Stock = 0;
        //    Stock = Convert.ToInt32(p);
        //    switch (Stock)
        //    {
        //        case 0:
        //            ret = "Red_Stock";
        //            break;
        //        case 1:
        //            ret = "Orange_Stock";
        //            break;
        //        case 2:
        //            ret = "Green_Stock";
        //            break;
        //    }
        //    return ret;
        //}
        private String setStock(decimal p)
        {

            try
            {
                int Stock = 0;
                Stock = Convert.ToInt32(p);
                switch (Stock)
                {
                    case 0:
                        imgURL = "/Style%20Library/Julia/img/Red_Stock.png";
                        break;
                    case 1:
                        imgURL = "/Style%20Library/Julia/img/Orange_Stock.png";
                        break;
                    case 2:
                        imgURL = "/Style%20Library/Julia/img/Green_Stock.png";
                        break;
                    case 3:
                        imgURL = "/Style%20Library/Julia/img/Black_Stock.png";
                        break;
                    case 4:
                        imgURL = "/Style%20Library/Julia/img/Gray_Stock.png";
                        break;

                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return (imgURL);
        }

        private void LoadPayMethodsAndTherms(Order order)
        {
            try
            {
                if (Session["User"] != null)
                {
                    currentUser = (User)Session["User"];
                }
             
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                string PaymentMathodTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "PaymentMethod");
                string ThermsPaymentTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "ThermsPayment");
                formadepagoValue.Items.Add(new RadComboBoxItem(PaymentMathodTxt,order.CodFormaPago));
                TerminiosdepagoValue.Items.Add(new RadComboBoxItem(ThermsPaymentTxt,order.CodTerminosPago));

            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadAddresses(Order order)
        {
            try
            {
                int i = 0;
                //Añadimos las direcciones del cliente a los combos de entrega
                foreach (string[] addresses in this.CurrentClient.Addresses)
                {
                    RadComboBoxItem aux = new Telerik.Web.UI.RadComboBoxItem(addresses[1], addresses[0]);

                    if (this.CurrentOrder.DeliveryAddress == addresses[0] || (String.IsNullOrEmpty(this.CurrentOrder.DeliveryAddress) && i == 0))
                    {
                        aux.Selected = true;
                    }
                    if (this.CurrentClient.RecogidaAlmacen)
                    {//Permite recogida almacen
                        this.entregaValue2.Items.Add(aux);
                    }
                    else
                    {//No permite recogida almacen
                        this.entregaValue.Items.Add(aux);
                    }

                    i++;
                }
                //Cargamos el combo de almacenes
                this.recogidaValue.Items.Add(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("WarehouseAddress"));
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadShipmentMethods()
        {
            try
            {
                //load two values to show
                ListItem total = new ListItem(LanguageManager.GetLocalizedString("total"), "Complete");
                //per defecte
                total.Selected = true;
                ListItem parcial = new ListItem(LanguageManager.GetLocalizedString("partial"), "partial");

                if (this.CurrentOrder.DeliveryType.ToLower () == "complete")
                    total.Selected = true;
                else if ((this.CurrentOrder.DeliveryType.ToLower() == "partial"))
                    parcial.Selected = true;
                else
                    total.Selected = true;

                if (this.CurrentOrder.DeliveryType == null)
                {
                    //si no entrega parcial marquem el total i deshabilitem pq no ho puguin tocar
                    total.Selected = true;
                    //RTP: faig que sempre ho puguin tocar
                    //total.Enabled = false;
                    //parcial.Enabled = false;
                }

                total.Enabled = true;
                parcial.Enabled = true;
                this.tipoEntregaValue.Items.Add(total);
                this.tipoEntregaValue.Items.Add(parcial);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        
        private void LoadTiposPedido(string strCampaña)
        {
            try
            {
                //load two values to show
                RadComboBoxItem normal = new RadComboBoxItem(LanguageManager.GetLocalizedString("TipoPedidoNormal"), "Normal");
                RadComboBoxItem campaign = new RadComboBoxItem(LanguageManager.GetLocalizedString("TipoPedidoCampanya"), "Campaign");
                if (String.IsNullOrEmpty(strCampaña))
                {
                    normal.Selected = true;
                }
                else {
                    campaign.Selected = true;
                    CodCampanyaValue.Enabled = true;
                }
                
                //REZ 30062014 - Ver que campos indican esto en NAV
                //if (this.CurrentOrder.DeliveryType.ToLower() == "normal")
                //    normal.Selected = true;
                //else if ((this.CurrentOrder.DeliveryType.ToLower() == "campaign")) {
                //    campaign.Selected = true;
                //    CodCampanyaValue.Enabled = true;  //Habilitamos el combo textbox de código de campaña
                //} else
                //    campaign.Selected = true;

                //if (this.CurrentOrder.DeliveryType == null)
                //{
                //    normal.Selected = true;
                //}

                normal.Enabled = true;
                campaign.Enabled = true;
                this.tipopedidoValue.Items.Add(normal);
                this.tipopedidoValue.Items.Add(campaign);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void CustomValidator1_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            if (args.Value.StartsWith("X"))
            {
                args.IsValid = false;
            }
        }

        public void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.EditCommandName)
                {
                    RadGrid1.MasterTableView.IsItemInserted = false;
                }
                if (e.CommandName == RadGrid.InitInsertCommandName)
                {
                    RadGrid1.MasterTableView.ClearEditItems();
                } 
                ArticleDataAccessObject articleDAO = null;
                Article articleSelected = null;
                string code = string.Empty;
                string catalog = string.Empty;
                string version = string.Empty;
                switch (e.CommandName)
                {
                    case ("LoadArticle"):

                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        code = ((RadTextBox)e.Item.FindControl("codiArticleForm")).Text;
                        version = ((RadTextBox)e.Item.FindControl("VersionArticleForm")).Text;
                        catalog = ((RadTextBox)e.Item.FindControl("CatalogArticleForm")).Text;
                        //articleSelected = articleDAO.GetProductByCode(code);
                        articleSelected = articleDAO.GetProductByCodeAndCatalog(code, version, catalog);
                        if (articleSelected != null)
                        {
                            ((Label)e.Item.FindControl("productDescriptionForm")).Text = articleSelected.Description;
                        }
                        break;
                    case ("SaveLine"):

                        if ((Label)e.Item.FindControl("ErrorTxt") != null)
                        {
                            ((Label)e.Item.FindControl("ErrorTxt")).Visible = false;
                        }

                    
                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        RadComboBox combo = ((RadComboBox)e.Item.FindControl("RadComboBox1"));
                        string[] value = combo.SelectedValue.Split(';');

                        if (value.Length > 2)
                        {
                            code = value[0];
                            version = value[1];
                            catalog = value[2];
                        }
                        articleSelected = articleDAO.GetProductByCodeAndCatalog(code,version,catalog);
                        if (articleSelected != null)
                        {
                            string lineDescription = ((RadTextBox)e.Item.FindControl("lineReferenceForm")).Text;
                    
                            //int numItems = (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value;
                            
                            string Qtyvalue = ((RadNumericTextBox)e.Item.FindControl("numForm")).Value.ToString();
                            if (string.IsNullOrEmpty(Qtyvalue))
                            {
                                throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NoArticle"));
                            }
                            else
                            {
                                int numItems = Convert.ToInt32(Qtyvalue);
                                this.CurrentOrder.AddProduct(articleSelected, numItems, lineDescription, CurrentClient.UserName);
                            }
                            //int numItems = string.IsNullOrEmpty((((RadNumericTextBox)e.Item.FindControl("numForm")).Value).ToString()) ? (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value : 1;
                          
                        }
                        else
                        {
                           
                            throw new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ArticleInvalid"));
                        }

                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;

                    case ("Cancel"):
                        break;
                    case ("Delete"):

                        this.CurrentOrder.DeleteProduct(e.Item.DataSetIndex, CurrentClient.UserName);
                        RadGrid1.Rebind();
                        break;
                    case ("UpdateLine"):
                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        RadTextBox combocode = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("codiArticleForm");
                        RadTextBox combocatalog = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("CatalogArticleForm");
                        RadTextBox comboversion = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("VersionArticleForm");
                        RadTextBox IDTextbox = (RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("LineNoForm");
                      //Falta captura l'element del combobox i funcionara
                         
                        //string[] valueUdate = comboUpdate.SelectedValue.Split(';');

                        code = combocode.Text;
                        version = comboversion.Text;
                        catalog = combocatalog.Text;
                        articleSelected = articleDAO.GetProductByCodeAndCatalog(code,version,catalog);
                        if (articleSelected != null)
                        {
                            string lineDescription = ((RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("lineReferenceForm")).Text;
                            //int numItems = (int)((RadNumericTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("numForm")).Value;

                            //int numItems = string.IsNullOrEmpty((((RadNumericTextBox)e.Item.FindControl("numForm")).Value).ToString()) ? (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value : 1;
                            string Qtyvalue = ((RadNumericTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("numForm")).Value.ToString();
                            if (string.IsNullOrEmpty(Qtyvalue))
                            {
                                string radalertscript = "<script language='javascript'>function f(){radalert('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("QtyErrorMsg") + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                                break;
                            }
                            else
                            {
                                int ID = Convert.ToInt32(((RadTextBox)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("LineNoForm")).Text);
                                int numItems = Convert.ToInt32(Qtyvalue);
                                this.CurrentOrder.SetProduct(articleSelected, numItems, lineDescription, CurrentClient.UserName, ID);
                            }
                            //this.CurrentOrder.DeleteProduct(e.Item.ItemIndex);
                            // this.CurrentOrder.AddProduct(articleSelected, numItems, lineDescription);
                        }
                        else
                        {
                            if ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt") != null)
                            {
                                ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt")).Text = "Article not found";
                                ((Label)this.RadGrid1.Items[e.Item.DataSetIndex].EditFormItem.FindControl("ErrorTxt")).Visible = true;
                            }
                        }

                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                    case (RadGrid.EditCommandName):

                        // UserControl MyUserControl = e.Item.FindControl(GridEditFormItem.EditFormUserControlID) as UserControl;
                        //RadGrid grid = (RadGrid)item.FindControl("RadGrid2");

                        //  ((TextBox)MyUserControl.FindControl("lineReferenceForm")).Text = "";
                        // ((TextBox)this.RadGrid1.Items[e.Item.ItemIndex].EditFormItem.FindControl("lineReferenceForm")).Text;
                        //((RadNumericTextBox)e.Item.FindControl("numForm")).Value =((RadNumericTextBox)this.RadGrid1.Items[e.Item.ItemIndex].EditFormItem.FindControl("numForm")).Value ;
                        //((RadComboBox)e.Item.FindControl("RadComboBox1")).Text = ((RadComboBox)this.RadGrid1.Items[e.Item.ItemIndex].EditFormItem.FindControl("RadComboBox1")).Text;


                        //--------------
                        //articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        //code = ((TextBox)e.Item.FindControl("codiArticleForm")).Text;
                        //articleSelected = articleDAO.GetProductByCode(code);
                        //if (articleSelected != null)
                        //{
                        //    string lineDescription = ((TextBox)e.Item.FindControl("lineReferenceForm")).Text;
                        //    int numItems = (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value;
                        //    this.CurrentOrder.AddProduct(articleSelected, numItems, lineDescription);
                        //}
                        //-----------------

                        break;


                    case (RadGrid.InitInsertCommandName):
                        //articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        //code = ((TextBox)e.Item.FindControl("codiArticleForm")).Text;
                        //articleSelected = articleDAO.GetProductByCode(code);
                        //if (articleSelected != null)
                        //{
                        //    string lineDescription = ((TextBox)e.Item.FindControl("lineReferenceForm")).Text;
                        //    int numItems = (int)((RadNumericTextBox)e.Item.FindControl("numForm")).Value;

                        //    this.CurrentOrder.AddProduct(articleSelected, numItems, lineDescription);
                        //}
                        //else
                        //{
                        //    if ((Label)e.Item.FindControl("ErrorTxt") != null)
                        //    {
                        //        ((Label)e.Item.FindControl("ErrorTxt")).Text = "Article not found";
                        //        ((Label)e.Item.FindControl("ErrorTxt")).Visible = true;
                        //    }
                        //}

                        //e.Item.Edit = false;
                        //RadGrid1.Rebind();
                        //e.Canceled = true;
                        //inserta la linea
                        e.Item.OwnerTableView.InsertItem();
                        GridEditableItem insertedItem1 = e.Item.OwnerTableView.GetInsertItem();

                        Label Title1 = (Label)insertedItem1.FindControl("Title");
                        Title1.Visible = true;
                        RadComboBox combo1 = (RadComboBox)insertedItem1.FindControl("RadComboBox1");
                        combo1.Visible = true;
                        RadTextBox txtRef1 = (RadTextBox)insertedItem1.FindControl("lineReferenceForm");
                        txtRef1.Visible = true;
                        RadTextBox codiArticle1 = (RadTextBox)insertedItem1.FindControl("codiArticleForm");
                        codiArticle1.Enabled = true;
                        Image ImgStock1 = (Image)insertedItem1.FindControl("ImgStock");
                        ImgStock1.Visible = false;
                        RadToolTip tool = (RadToolTip)insertedItem1.FindControl("RadToolTip1");
                        tool.Visible = false;



                        combo1.Filter = (RadComboBoxFilter)Convert.ToInt32("1");
                        //attach SelectedIndexChanged event for the drodown control
                        combo1.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(RadComboBox1_SelectedIndexChanged);

                        //------------------------------------------------
                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                    default:
                        e.Item.Edit = false;
                        RadGrid1.Rebind();
                        break;
                }
                
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ClientID, "sendOrderFocus();", true);
                string FocusScript = "<script language='javascript'>function f(){sendOrderFocus(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "focus", FocusScript); 

            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript); 

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
           
  
        }
        public void PrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);


           
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
          
        }

        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                string doc = string.Empty;
                if (Session["User"] != null)
                {
                    this.currentUser = (User)Session["User"];

                }
                if (Session["Client"] != null)
                {
                    this.CurrentClient = (Client)Session["Client"];

                }



                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=ofertapdf&ClientCode=" + currentUser.UserName + "&ofertano=" + CurrentOrder.NavOferta.No + "&selector=" + Session["PriceSelector"].ToString() + "&TipoCompra=0";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


            return output;
        }
        private void muestraArchivo(byte[] documento)
        {
            string extension = ".pdf";
            string contentType = "";
            switch (extension)
            {
                case ".xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".doc":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/msword";
                    break;
                case ".ppt":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
            }
            //MUESTRA VÍA WEB
            Response.Clear();
            MemoryStream ms = new MemoryStream(documento);
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=order.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();

        }


        public void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                e.KeepInInsertMode = false;
            }
            string FocusScript = "<script language='javascript'>function f(){sendOrderFocus(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "focus", FocusScript); 
        }

        public void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        {
            try
            {

                int i = e.Item.DataSetIndex;
                this.CurrentOrder.DeleteProduct(i,CurrentClient.UserName);
                Session["Order"] = this.CurrentOrder;

                //this.LoadSelectedArticles();
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            string FocusScript = "<script language='javascript'>function f(){sendOrderFocus(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "focus", FocusScript); 
        }

        public void RadGrid1_ItemUpdated(object source, GridUpdatedEventArgs e)
        {
            string FocusScript = "<script language='javascript'>function f(){sendOrderFocus(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "focus", FocusScript); 
        }

        public void SendPedido_Click(object sender, EventArgs e)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
              
                    this.CurrentOrder.ReferenciaPedido = this.referenciaValue.Text;
              
                if (!String.IsNullOrEmpty(this.Pedido_formapagoLbl.Text))
                {
                    this.CurrentOrder.PaymentMethod = this.Pedido_formapagoLbl.Text;
                }
                if (this.CurrentClient.RecogidaAlmacen)
                {
                    if (!String.IsNullOrEmpty(this.entregaValue2.SelectedValue))
                    {
                        this.CurrentOrder.DeliveryAddress = this.entregaValue2.SelectedValue;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(this.entregaValue.SelectedValue))
                    {
                        this.CurrentOrder.DeliveryAddress = this.entregaValue.SelectedValue;
                    }
                }
                this.CurrentOrder.DeliveryType = this.tipoEntregaValue.SelectedValue;

                this.CurrentOrder.PuntoRecogida = (this.Almacen.Checked) ? JuliaGrupUtils.Business.Order.RecogidaAlmacen.almacen.ToString() : JuliaGrupUtils.Business.Order.RecogidaAlmacen.cliente.ToString();
            
                    this.CurrentOrder.OrderObservations = this.observacionesValue.Text;

                    if (FechaRequeridaValue.IsEmpty)
                    {
                        CurrentOrder.FechaRequerida = new DateTime();
                    }
                    else
                    {
                        CurrentOrder.FechaRequerida = (DateTime)FechaRequeridaValue.SelectedDate;
                    }
                  
               
                 //get Portes
                    Sesion SesionJulia = new Sesion();
                    currentUser = (User)Session["User"];
                    Client cl = (Client)Session["Client"];
                    Order Order;
                    Order = SesionJulia.GetOrder(cl);


                    Decimal portes = Decimal.Zero;
                    
                    //REZ 25/03/2013 : Cambiamos currentUser.Code por currentUser.Username...
                    portes = orderDAO.getPortes(currentUser.UserName, Order.OrderNumber, cl.Code);
                    if (portes != 0)
                    {
                        //Guardamos pedido y pedimos confirmación
                        PlaceOrder(sender, e);

                        string radalertscript = "<script language='javascript'>function f(){callConfirm('" + portes  + "'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);


                        //RadWindowManager1.RadConfirm("Server radconfirm: Are you sure?", "confirmCallBackFn", 330, 180, null, "Server RadConfirm");

                    }
                    else
                    {
                        

                        bool result = orderDAO.SendOrderToNav(this.CurrentOrder, CurrentClient.UserName,0);
                        if (result)
                        {
                            //RTP
                           

                            ////netegem les orders en memoria
                            Session.Remove("Order");
                            Session.Remove("OrderDAO");
                            //netegem la llista de pedidos oberts
                            Session.Remove("VentasDAO");

                            //Obtenemos una Order Nueva
                            SesionJulia = new Sesion();
                            currentUser = (User)Session["User"];
                            cl = (Client)Session["Client"];
                            Order = SesionJulia.GetOrder(cl);
                            Session.Add("Order", Order);


                            radwindowPopup.Title = CurrentOrder.OrderClient.Name;
                            radwindowPopup.VisibleOnPageLoad = true;

                            lblMessage.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation");
                            lblConfirm.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck");
                            lblDesc1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView");
                            lblDesc2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1") + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2");


                        }
                        else
                        {
                            radwindowPopup.VisibleOnPageLoad = true;
                            lblMessage.Text = "Order Error";
                          
                        }
                    }
                   
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void SeguidComprando_Click(object sender, EventArgs e)
        {
           


        }
         


        public void DeleteOrder(object sender, EventArgs e)
        {

            string radalertscript = "<script language='javascript'>function f(){callConfirmDelOrder('" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("callConfirmDelOrder") + "'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "radalertDelOrder", radalertscript);

        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            Order Order;
            Sesion SesionJulia = new Sesion();
            currentUser = (User)Session["User"];
            Client cl = (Client)Session["Client"];
     
            Order = SesionJulia.GetOrder(cl);
            OrderDataAccesObject orderDAO = new OrderDataAccesObject();
            if (e.Argument.ToString() == "OK")
            {
                bool result = orderDAO.SendOrderToNav(this.CurrentOrder, CurrentClient.UserName,0);
                if (result)
                {
                    //RTP
                    //RadWindowManager1.RadAlert(LanguageManager.GetLocalizedString("SendOrderMsgOk"), 330, 100, LanguageManager.GetLocalizedString("SentOrderTitle"), );

                    ////netegem les orders en memoria
                    Session.Remove("Order");
                    Session.Remove("OrderDAO");
                    //netegem la llista de pedidos oberts
                    Session.Remove("VentasDAO");

                    //Obtenemos una Order Nueva
                    SesionJulia = new Sesion();
                    currentUser = (User)Session["User"];
                    cl = (Client)Session["Client"];
                    Order = SesionJulia.GetOrder(cl);
                    Session.Add("Order", Order);


                    //radwindowPopup.Title = CurrentOrder.OrderClient.Name;
                    //radwindowPopup.VisibleOnPageLoad = true;

                    lblMessage.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation");
                    lblConfirm.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck");
                    lblDesc1.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView");
                    lblDesc2.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1") + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2");
                    string messageToShow = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderConfirmation") + " <br><br> " +
                        JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheck") + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckView") + " <br><br> " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN1")
                         + " <br><br> " + CurrentOrder.OrderNumber + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderCheckN2");




                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "radalert('" + messageToShow + "', 700, 250,'" + CurrentOrder.OrderClient.Name + "',alertRetorno);", true);
                    
                }
                else
                {
                    //radwindowPopup.VisibleOnPageLoad = true;
                    //lblMessage.Text = "Order Error";
                    string radalertscript = "<script language='javascript'>function f(){radalert('Order Error', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                }
            }
            else if(e.Argument.ToString() == "OKOrder")
            {
                try
                {
                    CurrentOrder = (Order)Session["Order"];
                    if (CurrentOrder != null)
                    {
                        this.currentUser = (User)Session["User"];
                        this.CurrentOrder.DeleteAllProducts(currentUser.UserName);

                        Session["Order"] = this.CurrentOrder;
                        LoadSelectedArticles();
                        RadGrid1.DataBind();

                    }
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", "RedirectOrderDel()", true);
                }
                catch (NavException ex)
                {
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                    throw new SPException(new StackFrame(1).GetMethod().Name, ex);
                }
            }
            else
            {
               
            }
        }
       

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/home.aspx");
        }
        protected void PlaceOrder(object sender, EventArgs e)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                this.CurrentOrder.ReferenciaPedido = this.referenciaValue.Text;

                if (!String.IsNullOrEmpty(this.formadepagoValue.SelectedValue))
                {
                    this.CurrentOrder.PaymentMethod = this.formadepagoValue.SelectedValue;
                }
                if (this.CurrentClient.RecogidaAlmacen)
                {
                    if (!String.IsNullOrEmpty(this.entregaValue2.SelectedValue))
                    {
                        this.CurrentOrder.DeliveryAddress = this.entregaValue2.SelectedValue;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(this.entregaValue.SelectedValue))
                    {
                        this.CurrentOrder.DeliveryAddress = this.entregaValue.SelectedValue;
                    }
                }
                this.CurrentOrder.DeliveryType = this.tipoEntregaValue.SelectedValue;

                this.CurrentOrder.PuntoRecogida = (this.Almacen.Checked) ? JuliaGrupUtils.Business.Order.RecogidaAlmacen.almacen.ToString() : JuliaGrupUtils.Business.Order.RecogidaAlmacen.cliente.ToString();

                this.CurrentOrder.OrderObservations = this.observacionesValue.Text;

                if (FechaRequeridaValue.IsEmpty)
                {
                    CurrentOrder.FechaRequerida = new DateTime();
                }
                else
                {
                    CurrentOrder.FechaRequerida = (DateTime)FechaRequeridaValue.SelectedDate;
                }

                //REZ 25072014 - Guardamos el campo campaña
                CurrentOrder.Campaña = CodCampanyaValue.Text;


                Sesion SesionJulia = new Sesion();
                Client cl = (Client)Session["Client"];
                //Order Order;
                //Order = SesionJulia.GetOrder(cl);
                //Session.Add("Order", Order);
                Session["Order"] = this.CurrentOrder;

                this.CurrentOrder = (Order)Session["Order"];
                if (CurrentOrder != null)
                {
                    this.CurrentOrder.Update(CurrentClient.UserName);
                    SetTipoRecogidaCombos();
                    //Actualizamos las lineas
                    LoadSelectedArticles();
                    RadGrid1.DataBind();
                }
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }


        public List<Article> GetAllArticles()
        {
            try
            {
                ArticleDataAccessObject articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                return articleDAO.GetAllProducts();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        //protected void RadComboBox1_TextChanged(object sender, EventArgs e)
        //{
        //    GridEditableItem editedItem = (sender as RadComboBox).NamingContainer as GridEditableItem;
        //    Image ImgStock = editedItem.FindControl("ImgStock") as Image;
        //    ImgStock.Visible = false;
        //}


        protected void tipoEntregaValue_selectedindexchanged(object sender, EventArgs e)
        {
            try
            {
                if (tipoEntregaValue.SelectedValue == "partial")
                {
                    string CostsMsg = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeliveryTypePartialCostsInfo");
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + CostsMsg + "', 330, 100,'Info'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void tipopedidoValue_selectedindexchanged(object sender, EventArgs e)
        {
            try
            {
                if (tipopedidoValue.SelectedValue == "Normal")
                {
                    CodCampanyaValue.Enabled = false;
                    CodCampanyaValue.Text = null;
                }
                else if (tipopedidoValue.SelectedValue == "Campaign")
                {
                    CodCampanyaValue.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }


        protected void RadComboBox1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs  e)
        {
            try
            {
                string code = string.Empty;
                string catalog = string.Empty;
                string version = string.Empty;
                ArticleDataAccessObject articleDAO = null;
                if (Session["ArticleDAO"] != null)
                {
                    articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                }
                else
                {
                    throw new SPException("Error: ArticleDAO session var is Null");
                }
                string[] value = e.Value.Split(';');

                if (value.Length > 2)
                {
                    code = value[0];
                    version = value[1];
                    catalog = value[2];
                }
                //    //first reference the edited grid item through the NamingContainer attribute
                GridEditableItem editedItem = (sender as RadComboBox).NamingContainer as GridEditableItem;
                ////Load Code
                //RadTextBox box = editedItem.FindControl("codiArticleForm") as RadTextBox;
                //box.Text = e.Value;
                //code = box.Text;
                ////Load Catalog
                //RadTextBox Catalogbox = editedItem.FindControl("CatalogArticleForm") as RadTextBox;
                //catalog = Catalogbox.Text;
                ////Load Version
                //RadTextBox VersionBox = (RadTextBox)editedItem.FindControl("VersionArticleForm") as RadTextBox;
                //version = VersionBox.Text;
                //if (!(articleDAO.GetProductByCode(e.Value) == null))
                if (!(articleDAO.GetProductByCodeAndCatalog(code, version , catalog) == null))
                {
                    this.currentArticle = articleDAO.GetProductByCodeAndCatalog(code, version, catalog);
                    //load stock
                    Image ImgStock = editedItem.FindControl("ImgStock") as Image;
                    ImgStock.Visible = true;
                    Label lbl = editedItem.FindControl("lblStockTooltip") as Label;
                    RadToolTip tool = (RadToolTip)editedItem.FindControl("RadToolTip1");
                    tool.Visible = true;
                    ImgStock.ImageUrl = this.SetStock(this.currentArticle);
                    lbl.Text = this.setStockText((decimal)this.currentArticle.Stock, this.currentArticle.DescripcionGris).ToString();



                    QtyTxtbox_SetValues(this.currentArticle, editedItem);
                }
                else {
                    Image ImgStock = editedItem.FindControl("ImgStock") as Image;
                    ImgStock.Visible = false;
                    Label lbl = editedItem.FindControl("lblStockTooltip") as Label;
                    RadToolTip tool = (RadToolTip)editedItem.FindControl("RadToolTip1");
                    tool.Visible = false;
                   
                    lbl.Text = "";
                }
                string FocusScript = "<script language='javascript'>function f(){sendOrderFocus(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "focus", FocusScript); 
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


         
        }

        private void QtyTxtbox_SetValues(Article article, GridEditableItem editedItem)
        {
            try
            {
                RadNumericTextBox QtyTxtbox = (RadNumericTextBox)editedItem.FindControl("numForm");
                QtyTxtbox.MinValue = 1;
                QtyTxtbox.IncrementSettings.Step = Convert.ToInt32(article.UnidadMedida);
                QtyTxtbox.Value = Convert.ToInt32(article.UnidadMedida);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            try
            {

                if (e.Item is GridEditableItem && e.Item.IsDataBound &&  !(e.Item is GridEditFormInsertItem))
                {



                    if (((DataRowView)e.Item.DataItem)["Type"].ToString() != "Item")
                    {
                        GridDataItem item = e.Item as GridDataItem;
                        if (item != null)
                        {
                            ImageButton ImgBtnEdit = item["EditColumn"].Controls[0] as ImageButton;
                            ImgBtnEdit.Visible = false;

                            ImageButton ImgBtnDelete = item["DeleteColumn"].Controls[0] as ImageButton;
                            ImgBtnDelete.Visible = false;

                            GridTableCell image = item["SmallImage"] as GridTableCell;
                            image.Text = "";
                      
                        } 
                    }
                   

                    
                }
                if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                {

                    GridEditableItem item = (GridEditableItem)e.Item;
                    LinkButton Linkbut = (LinkButton)item.FindControl("LinkButton1");
                    //Linkbut.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SaveLine"); 
                    
                } 
                   
                if (e.Item is GridCommandItem)
                {
                    GridCommandItem cmditm = (GridCommandItem)e.Item;
                   
                    //to hide Refresh button
                    Control FoundCtl = cmditm.FindControl("RefreshButton"); //hide the text
                    if (FoundCtl != null)   FoundCtl.Visible = false;

                    FoundCtl = cmditm.FindControl("RebindGridButton"); //hide the image
                    if (FoundCtl != null) FoundCtl.Visible = false;

                }
                if (!(e.Item is GridEditFormInsertItem) && e.Item.IsInEditMode)
                {

                    GridEditFormItem Form = (e.Item as GridEditFormItem);
                  

                    Label Title = (Label)Form.FindControl("Title");
                    Title.Visible = false;

                    RadComboBox combo = (RadComboBox)Form.FindControl("RadComboBox1");
                    combo.Visible = true;
                    combo.Enabled = false;
                    RadTextBox txtRef1 = (RadTextBox)Form.FindControl("lineReferenceForm");
                    txtRef1.Visible = true;
                   

                    Image ImgStock1 = (Image)Form.FindControl("ImgStock");
                    ImgStock1.Visible = true;

                    RadTextBox codiArticle = (RadTextBox)Form.FindControl("codiArticleForm");
                    codiArticle.Enabled = false;

                    RadTextBox ArticleVersion = (RadTextBox)Form.FindControl("VersionArticleForm");
                    RadTextBox ArticleCatalog = (RadTextBox)Form.FindControl("CatalogArticleForm");
                    ArticleDataAccessObject articleDAO = null;
                    if (Session["ArticleDAO"] != null)
                    {
                        articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                    }
                    else
                    {
                        throw new SPException("Error: ArticleDAO session var is Null");
                    }
                    //if (!(currentArticle == null))
                    //{
                        //this.currentArticle = articleDAO.GetProductByCode(codiArticle.Text);
                        this.currentArticle = articleDAO.GetProductByCodeAndCatalog(codiArticle.Text, ArticleVersion.Text, ArticleCatalog.Text);
                        ImgStock1.ImageUrl = SetStock(currentArticle);
                        combo.Text = this.currentArticle.Description;
                        RadNumericTextBox QtyTxtbox = (RadNumericTextBox)Form.FindControl("numForm");
                        QtyTxtbox.MinValue = 1;
                        QtyTxtbox.MaxLength = 3;
                        QtyTxtbox.IncrementSettings.Step = Convert.ToInt32(this.currentArticle.UnidadMedida);
                        
                       
                        //QtyTxtbox.Value = Convert.ToInt32(this.currentArticle.UnidadMedida);
                    //}
                    //else {
                    //    this.RadGrid1.EditIndexes.Clear();
                    //    LoadSelectedArticles();

                    //}
                   
                   
                }
                if (e.Item is GridEditFormInsertItem)
                { 
                 GridEditFormItem Form = (e.Item as GridEditFormItem);
                  

                    Label Title = (Label)Form.FindControl("Title");
                    Title.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TittleIsertMode");
                }
                
                //if (e.Item is GridDataItem && e.Item.IsInEditMode)
                //{
                //    GridDataItem dataItem = e.Item as GridDataItem;
                //    dataItem["EditCommandColumn"].Visible = false;

                //} 
                if (e.Item is GridDataItem)
                {
                    foreach (GridColumn col in RadGrid1.MasterTableView.Columns)
                    {
                        if (col.UniqueName == "DeleteColumn")
                        {
                            (col as GridButtonColumn).ConfirmText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ConfirmDeleteMsg");
                        }
                    } 
                }
                       
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

      
   
        private string SetStock(Article a)
        { 
            string URL = "";
            try
            {
               
                int Stock = 0;
                Stock = Convert.ToInt32(a.Stock);
                switch (Stock)
                {
                    case 0:
                        URL = "/Style%20Library/Julia/img/Red_Stock.png";

                        break;
                    case 1:
                        URL = "/Style%20Library/Julia/img/Orange_Stock.png";
                        break;
                    case 2:
                        URL = "/Style%20Library/Julia/img/Green_Stock.png";
                        break;
                    case 3:
                        imgURL = "/Style%20Library/Julia/img/Black_Stock.png";
                        break;
                    case 4:
                        imgURL = "/Style%20Library/Julia/img/Gray_Stock.png";
                        break;
                    default:
                        URL = "";
                        break;
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
                return URL;
        }

       
        
        protected void RadComboBox1_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            try
            {
                RadComboBox combo = (RadComboBox)sender;
                List<string> CodRepeat = new List<string>();

                string codeRepeated = string.Empty;
                // Clear the default Item that has been re-created from ViewState at this point.
                combo.Items.Clear();
                ArticleDataAccessObject articleDAO = null;
                articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                List<Article> list = articleDAO.GetAllProducts();




                var query1 = (from n in list
                               .Where(n => n.DescriptionCombo.ToUpperInvariant().Contains(e.Text.ToUpperInvariant()))
                              select n);
                List<Article> table = query1.ToList<Article>();


                table = (from p in table where !p.CatalogType.Equals("Inspirate") select p).ToList();
                table = (from p in table where !p.CatalogType.Equals("Promociones") || p.CatalogNo.StartsWith("PRELIQ") select p).ToList();

                List<String> duplicates = table.Select(a=> a.Code).GroupBy(x => x)
                             .Where(g => g.Count() > 1)
                             .Select(g => g.Key)
                             .ToList();






                if (!(duplicates.Count == 0))
                {
                
                    List<Article> tableLast = new List<Article>(table);
         
                    
                    int cont = 0;
                    foreach (string Codigorepetido in duplicates)
                    {
                        foreach (Article Articulo in tableLast)
                        {
                            if ((Articulo.Code.ToString() == Codigorepetido && Articulo.CatalogPorDefecto != "true"))
                            {

                                table.RemoveAt(cont);
                                cont--;
                            }
                            cont++;
                        }
                        cont = 0;
                        tableLast = new List<Article>(table);
                    

                    }

                }

                combo.DataSource = table;
                combo.DataBind();
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
 
        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            try
            {
                //---------------Actualitzo la Order------------------
                Sesion SesionJulia = new Sesion();
                //Carrego Order
                Order p;
                p = SesionJulia.GetOrder(CurrentOrder.OrderClient);
                Session.Add("Order", p);
                this.CurrentOrder = (Order)Session["Order"];
                //----------------------------------------------------
                int iPriceSel = (int)Session["PriceSelector"];
                CultureInfo culture = new CultureInfo("es-Es", false);
                if (iPriceSel == 0 || iPriceSel == 2 || iPriceSel == 4) //Si solo veo Puntos o PVP no muestro descuentos
                {
                    this.PanelDescuentopp.Visible = false;

                    this.LblTotal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Total_Import");
                    
                    if (iPriceSel == 0)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalPoints != Decimal.Zero ? p.NavOferta.TotalPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") : "-";
                        if (currentUser is Client)
                        {
                            (RadGrid1.MasterTableView.GetColumn("Discount") as GridBoundColumn).Display = false;//hide GridBoundColumn

                        }
                    }
                    else if (iPriceSel == 1)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") : "-";
                    }
                    else if (iPriceSel == 2)
                    {
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? ((Convert.ToDouble(p.NavOferta.TotalPoints, culture)) * (Convert.ToDouble(currentUser.Coeficient, culture))) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") : "-";
                    }
                    else if (iPriceSel == 3)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? 
                                    (p.NavOferta.TotalImportesConIVA * CurrentClient.CoefDivisaWEB)
                                    .ToString("N2", culture) + " " + CurrentClient.LiteralDivisaWEB
                                    : "-";
                    }
                    else if (iPriceSel == 4)
                    {
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient * CurrentClient.CoefDivisaWEB) != Decimal.Zero ?
                            ((Convert.ToDouble(p.NavOferta.TotalPoints, culture)) * 
                            (Convert.ToDouble(currentUser.Coeficient, culture)) *
                            (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                            ).ToString("N2", culture)
                            + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo")
                            : "-";
                    }


                }
                else
                {
                    this.PanelDescuentopp.Visible = true;
                    this.LblTotal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Total_Import");


                    if (iPriceSel == 0)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalPoints != Decimal.Zero ? p.NavOferta.TotalPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") : "-";
                       
                    }
                    else if (iPriceSel == 1)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", new CultureInfo("es-Es")) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") : "-";
                    }
                    else if (iPriceSel == 2)
                    {
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? ((Convert.ToDouble(p.NavOferta.TotalPoints, new CultureInfo("es-Es", false))) * (Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es", false)))) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") : "-";
                    }
                    else if (iPriceSel == 3)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? 
                            (p.NavOferta.TotalImportesConIVA * CurrentClient.CoefDivisaWEB
                            ).ToString("N2", culture) 
                            + " " + CurrentClient.LiteralDivisaWEB : "-";
                    }
                    else if (iPriceSel == 4)
                    {
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? 
                            ((Convert.ToDouble(p.NavOferta.TotalPoints, culture)) *
                            (Convert.ToDouble(currentUser.Coeficient, culture)) *
                            (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                            ).ToString("N2", culture)
                            + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo")
                            : "-";
                    }


                    //REZ 29072014 - Creo que aqui faltara comprobar algo de la divisa
                    this.LblPPD.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TotalDiscount") + "  <strong>(" + p.NavOferta.TotalDtoPPPercent.ToString() + "%)</strong>";
                    LblPPDValue.Text = p.NavOferta.TotalDtoPP != Decimal.Zero ? p.NavOferta.TotalDtoPP.ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") : "-";

                }
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }



        
        protected void Entrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Guardamos el pedido
            PlaceOrder(sender, e);
        }


        //protected void PedidoPanel_PreRender(object sender, EventArgs e)
        //{
        //    RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
        //    manager.AjaxSettings.AddAjaxSetting(PedidoPanel, PedidoPanel, LoadingPanel);
        //}




    }
}
