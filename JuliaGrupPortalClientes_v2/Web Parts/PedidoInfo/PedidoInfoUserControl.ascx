﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidoInfoUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.PedidoInfo.PedidoInfoUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<SharePoint:CssRegistration ID="CssRegistrationPedidoInfo" name="/Style Library/Julia/PedidoInfo.css" runat="server"/>
<script type="text/javascript" >
    function OpenFile(url) {
        $.blockUI({ message: 'En Proceso' });
        window.open(url, '_blank', 'fullscreen=no,height=100,location=no,menubar=no,width=100');
        $.unblockUI({ message: 'En Proceso' });
    }
</script>
<style type="text/css">
.ColumnPrice
{
    text-align:right;}
</style>

<asp:PlaceHolder ID="PlaceHolder1" runat="server"> 
<asp:Panel ID="OrderExist" runat="server" Visible="true">
<div id="container">
    <div id="titol" class="title">
        <asp:Label ID="Pedido_ConsultapedidoLbl" runat="server" ></asp:Label>: 
        <asp:Label ID="PedidoNo" runat="server" CssClass="labelTittleandCode"></asp:Label>
        <asp:ImageButton ID="PrintPDF" ImageUrl="/Style%20Library/Julia/img/imprimir.png" runat="server"   onclick="PrintPDF_Click" CssClass="PrintIcon" />
    </div>
    <div id="ContainerTop">
        <div id="ContainerLeft">
           <div id="numeroPedido">
                <div id="numeroPedidolbl">
                <asp:Label ID="Pedido_ClientLabel" runat="server"></asp:Label>
                    </div>
                <div id="clientevalue">
                    <asp:Label ID="clienteValue" Width="335px" runat="server"></asp:Label></div>
            </div>
            <div id="referencia">
                <div id="referenciaLbl">
                <asp:Label ID="Pedido_referenciaLbl" runat="server"></asp:Label>
                    </div>
                <div id="referenciavalue">
                    <asp:TextBox ID="referenciaValue" runat="server" Width="335px"  Enabled="false"></asp:TextBox></div>
            </div>
            <div id="formadepago">
                <div id="formadepagoLbl">
                <asp:Label ID="Pedido_formapagoLbl" runat="server"></asp:Label>
                    </div>
                <div id="formadepagovalue">
                    <telerik:RadComboBox ID="formadepagoValue" runat="server" Width="340px"  DataTextField="value"
                        DataValueField="id" Enabled="false"/>
                </div>
            </div>
               <div id="TerminisPagement">
                <div id="TerminisPagementLbl">
                    <asp:Label ID="Terminios_formapagoLbl" runat="server"></asp:Label>
                </div>
                <div id="Terminiosdepagovalue" style="float:right;">
                    <telerik:RadComboBox ID="TerminiosdepagoValue" runat="server" Enabled="false" Width="340px" 
                        DataTextField="value" DataValueField="id" />
                </div>
            </div>
            <div id="direccionentrega">
                <div id="entregalbl">
                <asp:Label ID="Pedido_direccionentregaLbl" runat="server"></asp:Label>
                    </div>
                <div id="entregavalue">
                    <telerik:RadTextBox  ID="entregaValue" Enabled="false" runat="server" Width="340px" />
                </div>
            </div>
</div>
        <div id="ContainerRight">
		<div id="tipoentrega">
                <div id="tipoentregaLbl">
                     <asp:Label ID="Pedido_tipoentregaLbl" runat="server"></asp:Label></div>
                <div id="tipoentregavalue">
                    <asp:RadioButtonList runat="server" Enabled="false" ID="tipoEntregaValue" CssClass="radioOrderLine"
                        RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </div>
            </div>
            <div id="fechaRequerida">
                <div id="fechaRequeridalbl">
                     <asp:Label ID="Pedido_fechaRequeridalbl" runat="server"></asp:Label></div>
                <div id="fechaRequeridavalue">
                   
                    <telerik:RadTextBox ID="FechaReqValue" runat="server" enabled="false" Width="140px"></telerik:RadTextBox>
                   
                </div>
            </div>

            <div id="observaciones">
                <div id="observacionesLbl">
                    <asp:Label ID="Pedido_observacionesLbl" runat="server"></asp:Label></div>
                <div id="observacionesvalue">
                    <telerik:RadTextBox ID="observacionesValue" runat="server" MaxLength="160" TextMode="MultiLine" CssClass="observacionesValue">
                    </telerik:RadTextBox></div>
            </div>
        </div>
    </div>


    <div id="ContainerBottom">
        <div id="titol2">
        <asp:Label ID="Pedido_detallespedidoLbl" runat="server"></asp:Label>
        </div>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
            <telerik:RadGrid ID="RadGrid1" Width="100%" AllowSorting="False" OnNeedDataSource="RadGrid1_NeedDataSource" 
            HeaderStyle-CssClass="PedidoAdvancedHeaderClass" CssClass="Grid"
                AutoGenerateColumns="false" OnItemCommand="RadGrid1_ItemCommand" EnableLinqExpressions="false"
                PageSize="9" AllowPaging="True" ShowFooter="true"
                EnableAjaxSkinRendering="true" AllowMultiRowSelection="True" runat="server" GridLines="None" 
                OnItemDataBound="RadGrid1_ItemDataBound">
                <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Bottom"
                    UseAllDataFields="true" InsertItemPageIndexAction="ShowItemOnCurrentPage" OnPageIndexChanged="RadGrid1_PageIndexChanged">
                    <CommandItemSettings ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                        <telerik:GridBoundColumn DataField="ArticleCode" HeaderText="Code" UniqueName="ArticleCode" />
                        <telerik:GridBoundColumn DataField="Description" HeaderText="Description" UniqueName="Description"
                            ReadOnly="true" />
                        <telerik:GridBoundColumn DataField="LineReference" HeaderText="Line description"
                            UniqueName="LineReference" />
                        <telerik:GridBoundColumn DataField="Num" HeaderText="Num." 
                            UniqueName="Num">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CantidadPendiente" headerText="CantidadPendiente"  UniqueName="CantidadPendiente" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SemanaExpedicion" headerText="SemanaExpedicion"  UniqueName="SemanaExpedicion" ></telerik:GridBoundColumn>
                       <telerik:GridBoundColumn DataField="Albaranes" headerText="Lista Albaranes"  UniqueName="Albaranes" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Discount" HeaderText="Discount %" UniqueName="Discount"
                            ReadOnly="true" />
                        <telerik:GridBoundColumn DataField="Price" HeaderText="€" UniqueName="Price"  ReadOnly="true"
                            DataType="System.Double">
                                   <ItemStyle CssClass="ColumnPrice" />
                        </telerik:GridBoundColumn>
<%--                        <telerik:GridButtonColumn UniqueName="EditColumn" CommandName="Edit" ButtonType="ImageButton" />
                        <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ButtonType="ImageButton"
                            ConfirmDialogType="RadWindow" ConfirmText="Delete this product?">
                        </telerik:GridButtonColumn>--%>
                    </Columns>
                    
                </MasterTableView>
                <PagerStyle Mode="NextPrev" />
            </telerik:RadGrid>
            <div class="RadGrid RadGrid_Default Grid">
                 <table class="rgCommandRow" style="float:right; width:50%;">
                            <tbody>
                                 <tr class="rgCommandRow" style="border-top:none;border">
                                    <td>
                                        <table class="table_source" style="width:100%;" cellpadding="0px" cellspacing="0px">
                                            <asp:Panel ID="PanelDescuentopp" runat="server" >
                                                <tr>
                                                    <td class="caption_sourceTable">
                                                        <div class="lblfinal_field1"><asp:Label ID="LblPPD" runat="server"></asp:Label></div>
                                                    </td>
                                                    <td>
                                                        <div class="lblfinal_field2" ><asp:Label ID="LblPPDValue" class="TotalDiscountValue" runat="server" Text=""></asp:Label></div>
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td class="caption_sourceTable">
                                                    <div class="lblfinal_field3"><asp:Label ID="LblTotal" runat="server" ></asp:Label></div>
                                                </td>
                                                  
                                                <td>
                                                    <div class="lblfinal_field4"><asp:Label ID="LblTotalValue" CssClass="TotalValue" runat="server" Text=""></asp:Label></div>
                                                </td>
                                            </tr>
                               
                                
                                        </table>
                
                                    </td>
                                </tr>
                            </tbody>
                  </table>
            </div>
        </telerik:RadAjaxPanel>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Skin="Vista" runat="server" />
    </div>
</div>
</asp:Panel>
<asp:Panel runat="server" ID="OrderNotExist" Visible="false">
    <div class="ErrorContainerOrder">
        <asp:Label ID="MsgErrorOrderNoexist" CssClass="MsgErrorOrderNoexist" runat="server"></asp:Label>
        <input type="button" value="<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CloseOrderButtonValue")%>" id="CloseOrderError"  onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel, 'Cancel clicked'); return false;"/>
    </div>
</asp:Panel>
</asp:PlaceHolder> 
