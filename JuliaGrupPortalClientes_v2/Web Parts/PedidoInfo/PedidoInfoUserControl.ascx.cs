﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.Utils;
using Telerik.Web.UI;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.SharePoint;
using System.IO;
using System.Xml;
using System.IO.Compression;
using JuliaGrupUtils.ErrorHandler;
using System.Web;
using System.Globalization;

namespace JuliaGrupPortalClientes_v2.Web_Parts.PedidoInfo
{
    public partial class PedidoInfoUserControl : UserControl
    {
        private string ventaType = string.Empty;
        private string ventaCode = string.Empty;
        private string codClient = string.Empty;

        User currentUser;
        private Client CurrentClient;
        private Order CurrentOrder;
        private string ReferenciaPedido = string.Empty;
        private string Observations = string.Empty;
        private List<OrderLine> ListProducts;
        private string PaymentMethod = string.Empty;
        private string DeliveryAddress = string.Empty;
        private string DeliveryType = string.Empty;
        private string NumPedido = string.Empty;

        private void ShowVentaNotFoundMessage() {
            this.MsgErrorOrderNoexist.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("MsgErrorOrderNoexist");
            //No se ha encontrado el pedido. Mostrar un mensaje
            this.OrderExist.Visible = false;
            this.OrderNotExist.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            try
            {  
                ventaType = Request.QueryString["type"];
                ventaCode = Request.QueryString["code"];
                codClient = Request.QueryString["client"];

                //validamos que los parametros llegan correctamente
                if (string.IsNullOrEmpty(ventaType)
                    || string.IsNullOrEmpty(ventaCode)
                    || string.IsNullOrEmpty(codClient)
                    )
                {
                    ShowVentaNotFoundMessage();
                    return;
                }


                if (codClient != null)
                {
                   
                    Sesion SesionJulia = new Sesion();
                    if (Session["User"] != null)
                    {
                        currentUser = (User)Session["User"];
                    }

                    //Agent agent = (Agent)this.currentUser;    //19/11/2012 - REZ:: No se utiliza la variable agent... Además puede que currentUser sea de tipo Client y peta
                    Client cli = SesionJulia.GetClient(codClient, this.currentUser.UserName);
                    if (cli == null)
                        throw new Exception("Error NULL en el GetClient del cliente seleccionado");


                    OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                    Order CurrentOrder = orderDAO.GetOrderById(ventaCode, cli);
                    //31/10/2012 ==> Hay que controlar que puede devolver null porque se haya evolucionado la oferta en NAV y se mantenga la pantalla abierta. En ese caso mostramos un mensaje
                    //              Pendiente definir el mensaje

                    if (CurrentOrder != null)
                    {
                        setInfo(CurrentOrder);
                        if (!IsPostBack)
                        {
                            this.CurrentClient = CurrentOrder.OrderClient;
                            this.Initialize(CurrentOrder);
                        }
                    }
                    else
                    {
                        ShowVentaNotFoundMessage();
                    }
                }
                int iPriceSel = (int)Session["PriceSelector"];
                if ((iPriceSel == 2) || ((iPriceSel == 0)) || (iPriceSel == 4)) //En vista PVP no mostramos descuentos
                {
                    this.RadGrid1.Columns.FindByUniqueName("Discount").Display = false;
                }
            
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void setInfo(Order p)
        {
            try
            {
                this.ReferenciaPedido = p.ReferenciaPedido;
                this.Observations = p.OrderObservations;
                this.ListProducts = p.ListOrderProducts;
                this.PaymentMethod = p.PaymentMethod;
                this.DeliveryAddress = p.DeliveryAddress; 
                this.DeliveryType = p.DeliveryType;
                this.NumPedido = p.OrderNumber;
                
                if (p.FechaRequerida == new DateTime())
                {
                    this.FechaReqValue.Text = string.Empty;
                }
                else
                {
                    this.FechaReqValue.Text =String.Format("{0:dd/MM/yy}", p.FechaRequerida);
                }

                int iPriceSel = (int)Session["PriceSelector"];
                if (iPriceSel == 0 || iPriceSel == 2 || iPriceSel == 4) //En vista Puntos o PVP no mostramos descuentos
                {
                    this.PanelDescuentopp.Visible = false;

                    this.LblTotal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Total_Import");
                    if (iPriceSel == 0)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalPoints != Decimal.Zero ? p.NavOferta.TotalPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") : "-";
                    }
                    else if (iPriceSel == 1)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                    else if (iPriceSel == 2)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? (Convert.ToDouble(p.NavOferta.TotalPoints, new CultureInfo("es-Es")) * Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es"))).ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                    //REZ 29072014 - Aqui hay que cambiarlo o se muestra como se ha pagado? Que hacemos con los Dollares? 
                    else if (iPriceSel == 3)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                    else if (iPriceSel == 4)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? (Convert.ToDouble(p.NavOferta.TotalPoints, new CultureInfo("es-Es")) * Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es"))).ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                   
                    
                }
                else
                {
                    this.PanelDescuentopp.Visible = true;
                    this.LblTotal.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Total_Import");


                    if (iPriceSel == 0)
                    {
                        LblTotalValue.Text = p.NavOferta.TotalPoints != Decimal.Zero ? p.NavOferta.TotalPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") : "-"; 
                    }
                    else if (iPriceSel == 1)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-"; 
                    }
                    else if (iPriceSel == 2)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? (Convert.ToDouble(p.NavOferta.TotalPoints, new CultureInfo("es-Es")) * Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es"))).ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                    //REZ 29072014 - Aqui hay que cambiarlo o se muestra como se ha pagado? Que hacemos con los Dollares? 
                    else if (iPriceSel == 3)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        LblTotalValue.Text = p.NavOferta.TotalImportesConIVA != Decimal.Zero ? p.NavOferta.TotalImportesConIVA.ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                    else if (iPriceSel == 4)
                    {
                        string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        LblTotalValue.Text = (p.NavOferta.TotalPoints * currentUser.Coeficient) != Decimal.Zero ? (Convert.ToDouble(p.NavOferta.TotalPoints, new CultureInfo("es-Es")) * Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es"))).ToString("N2", new CultureInfo("es-Es", false)) + " " + strCurrency : "-";
                    }
                   
                    
                    
                    
                    this.LblPPD.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TotalDiscount") + "  <strong>(" + p.NavOferta.TotalDtoPPPercent.ToString() + "%)</strong>";
                    string strCurrency2 = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                    LblPPDValue.Text = p.NavOferta.TotalDtoPP != Decimal.Zero ? p.NavOferta.TotalDtoPP + " " + strCurrency2 : "-";
                    
                }
              
             
               
              
                this.observacionesValue.Enabled = false;

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

   

        private void Initialize(Order p)
        {
            try
            {
                if (Session["Order"] != null)
                {
                    CurrentOrder = (Order)Session["Order"];
                }
                //load title
                this.referenciaValue.Text = this.ReferenciaPedido;
                //load forma de pago
                this.LoadPayMethodsAndTherms(CurrentOrder);
                //load direccion de entrega
                this.LoadAddresses(p);
                //load tipo de entrega
                this.LoadShipmentMethods();
                //load observaciones
                this.observacionesValue.Text = this.Observations;
                //load client name
                this.clienteValue.Text = p.OrderClient.Code + " - " + p.CustomerName;
                this.PedidoNo.Text = this.NumPedido;
                
                //load all selected articles
                //this.LoadSelectedArticles();            

                this.Pedido_ClientLabel.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ClientLabel");
                this.Pedido_referenciaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_referenciaLbl");
                this.Pedido_formapagoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_formapagoLbl");
                this.Terminios_formapagoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Terminios_formapagoLbl");
                this.Pedido_direccionentregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_direccionentregaLbl");
                this.Pedido_tipoentregaLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_tipoentregaLbl");
                this.Pedido_observacionesLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_observacionesLbl");
                this.Pedido_detallespedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_detallespedidoLbl");
                this.Pedido_ConsultapedidoLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_ConsultapedidoLbl");
                this.Pedido_fechaRequeridalbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Pedido_FechaRequerida");
               
                this.RadGrid1.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListCode");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Num").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Discount").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Discount");
                //this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport");
                this.RadGrid1.Columns.FindByUniqueNameSafe("LineReference").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListReference");
                this.RadGrid1.Columns.FindByUniqueNameSafe("CantidadPendiente").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cantidad_Pendiente");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Albaranes").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Albaranes");
                this.RadGrid1.Columns.FindByUniqueNameSafe("SemanaExpedicion").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SemanaExpedicion");

                
                int iPriceSel = (int)Session["PriceSelector"];
                if (iPriceSel == 0)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") + ")";
                }
                else if (iPriceSel == 1)
                {
                    string strCurrency = (p.Divisa == "USD")?JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars"):JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");

                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" +
                        strCurrency
                        + ")";
                }
                else if (iPriceSel == 2)
                {
                    string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");

                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" +
                        strCurrency
                        + ")";
                }
                //REZ 29072014 - Aqui hay que cambiarlo o se muestra como se ha pagado? Que hacemos con los Dollares? 
                else if (iPriceSel == 3)
                {
                    string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");

                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" +
                        strCurrency
                        + ")";
                }
                else if (iPriceSel == 4)
                {
                    string strCurrency = (p.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");

                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + "(" +
                        strCurrency
                        + ")";
                }
          
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


           


        }

        private void LoadPayMethodsAndTherms(Order order)
        {
            try
            {
                if (Session["User"] != null)
                {
                    currentUser = (User)Session["User"];
                }

                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                string PaymentMathodTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "PaymentMethod");
                string ThermsPaymentTxt = orderDAO.GetDescripcionPagos(currentUser.UserName, order.CodFormaPago, order.CodTerminosPago, "ThermsPayment");
                formadepagoValue.Items.Add(new RadComboBoxItem(PaymentMathodTxt, order.CodFormaPago));
                TerminiosdepagoValue.Items.Add(new RadComboBoxItem(ThermsPaymentTxt, order.CodTerminosPago));

            }
            catch (NavException ex)
            {
                string radalertscripts = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 320, 100,'Error'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscripts);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
             this.LoadSelectedArticles();          
        }

        private void LoadSelectedArticles()
        {
            if (CurrentOrder == null)
            {
                Sesion SesionJulia = new Sesion();
                if (Session["User"] != null)
                {
                    currentUser = (User)Session["User"];
                }

                //Agent agent = (Agent)this.currentUser;    //19/11/2012 - REZ:: No se utiliza la variable agent... Además puede que currentUser sea de tipo Client y peta
                Client cli = SesionJulia.GetClient(codClient, this.currentUser.UserName);
                if (cli == null)
                    throw new Exception("Error NULL en el GetClient del cliente seleccionado");


                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                CurrentOrder = orderDAO.GetOrderById(ventaCode, cli);
                //31/10/2012 ==> Hay que controlar que puede devolver null porque se haya evolucionado la oferta en NAV y se mantenga la pantalla abierta. En ese caso mostramos un mensaje
                    //              Pendiente definir el mensaje

                if (CurrentOrder != null)
                {
                    this.CurrentClient = CurrentOrder.OrderClient;
                }
                
            }

            if (CurrentOrder != null)
            {
                try
                {
                    DataTable data = new DataTable();
                    data.Columns.Add("ArticleCode");
                    data.Columns.Add("Description");
                    data.Columns.Add("LineReference");
                    data.Columns.Add("Num", typeof(Int32));
                    data.Columns.Add("Discount", typeof(Double));
                    data.Columns.Add("Price");
                    data.Columns.Add("CantidadPendiente");
                    data.Columns.Add("Albaranes");
                    data.Columns.Add("SemanaExpedicion");


                    if (this.ListProducts != null)
                    {
                        foreach (OrderLine ol in this.ListProducts)
                        {
                            DataRow row = data.NewRow();

                            row["ArticleCode"] = ol.Product.Code;
                            row["Description"] = ol.Product.Description;
                            row["LineReference"] = ol.LineDescription;
                            row["Num"] = ol.Quantity;
                            row["Discount"] = ol.Discount;
                            int iPriceSel = (int)Session["PriceSelector"];
                            if (iPriceSel == 0)
                            {
                                row["Price"] = ol.Points;
                            }
                            else if (iPriceSel == 1)
                            {
                                row["Price"] =  ol.Price.ToString("N2") ;
                            }
                            else if (iPriceSel == 2)
                            {
                                row["Price"] =((Convert.ToDouble(ol.Points, new CultureInfo("es-Es"))) * (Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es")))).ToString("N2");
                            }
                            //REZ 29072014 - Hay que cambiarlo??
                            else if (iPriceSel == 3)
                            {
                                row["Price"] = ol.Price.ToString("N2");
                            }
                            else if (iPriceSel == 4)
                            {
                                row["Price"] = ((Convert.ToDouble(ol.Points, new CultureInfo("es-Es"))) * (Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es")))).ToString("N2");
                            }


                            //row["Price"] = (ol.Price != null) ? ol.Price.ToString("N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") : "-";

                            row["CantidadPendiente"] = ol.CantidadPendiente.ToString();
                            if (!(String.IsNullOrEmpty(ol.Albaranes)))
                            {
                                if ((iPriceSel != 2) || (iPriceSel != 4))
                                {
                                    row["Albaranes"] = getAlbaran(ol.Albaranes, CurrentClient.Code);
                                }
                                else
                                {
                                    row["Albaranes"] = ol.Albaranes;
                                }
                            }
                            else
                            {
                                row["Albaranes"] = "";
                            }
                            row["SemanaExpedicion"] = ol.SemanasExpedicion;
                            data.Rows.Add(row);
                        }
                    }

                    this.RadGrid1.DataSource = data;
                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                    throw new SPException(new StackFrame(1).GetMethod().Name, ex);
                }
            }
        }
        public string getAlbaran(string codAlbaran, string codClient)
        {
            string ret = string.Empty;
            string Albaran = codAlbaran;
            char delimiterChars = ':';
            



            string[] Albaranes = Albaran.Split(delimiterChars);


            foreach (string s in Albaranes)
            {
                if(ret == "")
                {
                    ret += "<label style='cursor:pointer; text-decoration:underline;' class='LinkAlbaran' onclick='OpenFile(\"" + GetAlbaranURL(codAlbaran, codClient)+ "\");'>" + codAlbaran + "</label>";
                }
                else
                {
                    ret += "; <label style='cursor:pointer; text-decoration:underline;' class='LinkAlbaran' onclick='OpenFile(\"" + GetAlbaranURL(codAlbaran, codClient) + "\");'>" + codAlbaran + "</label>";
                }
              
            }
            
           
            // Keep the console window open in debug mode.
          


            return ret;
        }
        public string GetAlbaranURL(string codAlbaran, string codClient)
        {
            string output = string.Empty;

            try
            {
                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=deliverynote&ClientId=" + codClient + "&FileName=" + codAlbaran + ".pdf";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


            return output;
        }
        public string GetDocURL()
        {
            string output = string.Empty;

            try
            {
                if ((Client)Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                if ((Order)Session["Order"] != null)
                {
                    CurrentOrder = (Order)Session["Order"];
                }
                output = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=OfertaPDF&ClientCode=" + currentUser.UserName + "&OfertaNo=" + Request.QueryString["code"].ToString() + "&Selector=" + Session["PriceSelector"].ToString() + "&TipoCompra=0";
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }


            return output;
        }
        

        private void LoadAddresses(Order p)
        {
            try
            {
                if (!String.IsNullOrEmpty(p.PuntoRecogida) && p.PuntoRecogida.ToLower() == "almacen")
                {
                    this.entregaValue.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("WarehouseAddress");
                }
                else
                {
                    int i = 0;
                    foreach (string[] addresses in this.CurrentClient.Addresses)
                    {


                        if (this.DeliveryAddress == addresses[0] || (String.IsNullOrEmpty(this.DeliveryAddress) && i == 0))
                        {
                            this.entregaValue.Text = addresses[1];
                        }


                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void LoadShipmentMethods()
        {
            try
            {
                //load two values to show
                ListItem total = new ListItem(LanguageManager.GetLocalizedString("total"), "total");
                //per defecte
                total.Selected = true;
                ListItem parcial = new ListItem(LanguageManager.GetLocalizedString("partial"), "partial");

                if (this.DeliveryType.ToLower() == "complete")
                    total.Selected = true;
                else if (this.DeliveryType.ToLower() == "partial")
                    parcial.Selected = true;
                else
                    total.Selected = true;

                if (!this.CurrentClient.PartialDelivery)
                {
                    //si no entrega parcial marquem el total i deshabilitem pq no ho puguin tocar
                    total.Selected = true;
                    total.Enabled = false;
                    parcial.Enabled = false;
                }

                this.tipoEntregaValue.Items.Add(total);
                this.tipoEntregaValue.Items.Add(parcial);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            
        }
        protected void RadGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            LoadSelectedArticles();
        }

        protected void PrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "OpenFile", "OpenFile(\"" + GetDocURL() + "\");", true);
            
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

            }
            catch (Exception ex)
            {

                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
          
        }
        private void muestraArchivo(byte[] documento, string contenidoFixero)
        {
            string extension = ".pdf";
            string contentType = "";
            switch (extension)
            {
                case ".xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".xlsx":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".doc":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/msword";
                    break;
                case ".ppt":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
                case ".pptx":
                    contentType = "application/vnd.ms-powerpoint";
                    break;
            }



            string fileContent = contenidoFixero;

            //string fileUrl = GenerateDownloadLink(fileContent, "test" +  extension);
            Response.Clear();
            MemoryStream ms = new MemoryStream(documento);
            Response.ContentType = contentType;
            Response.AddHeader("content-disposition", "attachment;filename=Order.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartDownload", "window.location = '" + fileUrl + "';", true);
            //MUESTRA VÍA WEB
            //Response.Buffer = false;
            //Response.ContentType = contentType;
            //Response.AddHeader("content-length", documento.Length.ToString());
            //Response.BinaryWrite(documento);
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;

                if (item["SemanaExpedicion"].Text == "0")
                {
                    item["SemanaExpedicion"].Text = "";
                }
            }

        }
     
    }
}
