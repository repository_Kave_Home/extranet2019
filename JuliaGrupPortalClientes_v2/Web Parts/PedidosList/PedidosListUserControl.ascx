﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PedidosListUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.PedidosList.PedidosListUserControl" %>
<!-- link href="/Style%20Library/Julia/PedidoAdvanced.css" rel="stylesheet" type="text/css" / -->
<SharePoint:CssRegistration ID="CssRegistrationPedidoAdvanced" name="/Style Library/Julia/PedidoAdvanced.css" runat="server"/>
<script language="javascript" type="text/javascript">
    function RowClick(sender, eventArgs) {
        
        var CodclienteCodName = eventArgs.get_gridDataItem().get_element().cells[0].innerText;
        if (CodclienteCodName == undefined) {
            /* innertext no funciona con Firefox*/
            CodclienteCodName = eventArgs.get_gridDataItem().get_element().cells[0].textContent;
        }
        CodclienteCodName = CodclienteCodName.split(' - ');

        var cliente = CodclienteCodName[0];
       
        
        if (cliente == undefined) {
            /* innertext no funciona con Firefox*/
            var CpdClientNameExplorer = eventArgs.get_gridDataItem().get_element().cells[0].textContent.split(' - ');
            cliente = CpdClientNameExplorer[0];
        }
        var code = eventArgs.get_gridDataItem().get_element().cells[1].innerText;
        if (code == undefined) {
            /* innertext no funciona con Firefox*/
            code = eventArgs.get_gridDataItem().get_element().cells[1].textContent;
        }
        var type = eventArgs.get_gridDataItem().get_element().cells[8].innerText;
        if (type == undefined) {
            /* innertext no funciona con Firefox*/
            type = eventArgs.get_gridDataItem().get_element().cells[8].textContent;
        }
        
        OpenOrderInfoDialog(code, type, cliente);
    }
    function OpenOrderInfoDialog(code, type, cliente) {
        var options = {
            url: "/Pages/OrderInfo.aspx?code=" + code + "&type=" + type + "&client=" + cliente,
            width: 900,
            height: 600,   
            dialogReturnValueCallback: DialogCallback
        };

        SP.UI.ModalDialog.showModalDialog(options);
    }

    function DialogCallback(dialogResult, returnValue) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            RefreshTable();
        }
    }

</script>
<style type="text/css">
.SupBtnsLeft
{
    float:left;}
 .supBtnsRight
 {
     float:right;
     }   
 .supBtns
 {
     width:100%;}
 .PriceColumn
 {
     text-align:right;
     }
</style>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
<AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnTodos">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoPendiente">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoCurso">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="BtnEstadoServido">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>        
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<telerik:RadAjaxLoadingPanel ID="LoadingPanel" Skin ="Default" runat="server"></telerik:RadAjaxLoadingPanel>


  <div class="supBtns">
    <div class="csscheckbtn SupBtnsLeft">
        <telerik:RadButton ID="BtnTodos" runat="server" ToggleType="CheckBox" 
        ButtonType="StandardButton"  Checked ="false"  oncheckedchanged="BtnTodos_CheckedChanged"  CssClass ="btntds"  >
            <ToggleStates>
                <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckbox" />
            </ToggleStates>

        </telerik:RadButton>
        </div>
    <div class="supBtnsRight">
            
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoPendiente" runat="server" ToggleType="CheckBox" 
                ButtonType="StandardButton" 
                oncheckedchanged="BtnEstadoPendiente_CheckedChanged"   CssClass ="btn_actual_factura" Checked ="true">
                <ToggleStates>
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>    
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoCurso" runat="server" ToggleType="CheckBox" 
                ButtonType="StandardButton" 
                oncheckedchanged="BtnEstadoCurso_CheckedChanged"   
                CssClass ="btn_actual_factura"  Checked ="true">
                <ToggleStates>
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckbox" />
                </ToggleStates>
            </telerik:RadButton>
        </div>
        <div class="csscheckbtn">
            <telerik:RadButton ID="BtnEstadoServido" runat="server" ToggleType="CheckBox" 
            ButtonType="StandardButton" 
                oncheckedchanged="BtnEstadoServido_CheckedChanged"  
                CssClass ="btn_pedidos_servidos"  Checked ="true">
                  <ToggleStates>
                        <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                        <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleCheckbox" />
                    </ToggleStates>
            </telerik:RadButton>
          </div>
         </div>
       </div>
<div class="tableOrderList">
  
    <telerik:RadGrid ID="RadGrid1" 
    Width="100%" 
    AllowFilteringByColumn="true" 
    AllowPaging="true" 
    AllowSorting="true" 
    AutoGenerateColumns="false"  
    GridLines="None" 
    OnNeedDataSource="RadGrid1_NeedDataSource" 
    HeaderStyle-CssClass="PedidoAdvancedHeaderClass" 
    CssClass="Grid" 
    PageSize="9" 
    runat="server" 
    OnSortCommand="RadGrid1_SortCommand" 
    OnItemDataBound="RadGrid1_ItemDataBound"
    >
         

        <GroupingSettings CaseSensitive="false" /> 
         <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
      
        <MasterTableView Width="100%" Summary="RadGrid table" CommandItemDisplay="Bottom"  
            UseAllDataFields="true" InsertItemPageIndexAction="ShowItemOnCurrentPage" 
          
           CanRetrieveAllData="false"
             >
            <CommandItemSettings  ShowAddNewRecordButton="false" />
          
            <Columns>
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="Client" UniqueName="Client" />
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true"  DataField="ArticleCode" UniqueName="ArticleCode" />
                <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true" DataField="Reference"  UniqueName="Reference"
                    ReadOnly="true" />
                <telerik:GridBoundColumn AllowFiltering="false"  DataField="OrderDate" SortExpression="OrderDate"  DataType="System.DateTime" 
                    UniqueName="OrderDate" DataFormatString="{0:dd/MM/yyyy}"  />
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Volume" 
                    UniqueName="Volume" DataFormatString="{0:N3}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Status" UniqueName="Status"
                    ReadOnly="true" /> 
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Description"  UniqueName="Description" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime" ReadOnly="true">
                </telerik:GridBoundColumn>
            
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Price" DataType="System.Decimal"  UniqueName="Price" ReadOnly="true" DataFormatString="{0:N2}">
                    <ItemStyle CssClass="PriceColumn" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="PedidoType"  UniqueName="PedidoType" Display="false" />
                <telerik:GridRowIndicatorColumn  Display="false" UniqueName="Id" />
                <telerik:GridBoundColumn  AllowFiltering="false" DataField="Divisa"  UniqueName="Divisa" Display="true" />
            </Columns>
        </MasterTableView>
        <PagerStyle Mode="NextPrev" />
        <ClientSettings>
            <ClientEvents OnRowClick="RowClick">
            </ClientEvents>
        </ClientSettings>
    </telerik:RadGrid>
</div>


