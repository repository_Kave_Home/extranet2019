﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Telerik.Web.UI;
using System.Data;
using JuliaGrupUtils.Utils;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using Microsoft.SharePoint;

namespace JuliaGrupPortalClientes_v2.Web_Parts.PedidosList
{
    public partial class PedidosListUserControl : UserControl
    {
        private List<Venta> ventas;

        private User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            VentaDataAccessObject ventasDAO;
            if (Session["VentasDAO"] == null)
            {
                //Carrego Ventas

                Sesion SesionJulia = new Sesion();
                ventasDAO = SesionJulia.GetVentas(((Client)Session["Client"]).Code);
                if (ventasDAO == null)
                    throw new Exception("Error NULL en el GetVentas del cliente seleccionado");
                Session.Add("VentasDAO", ventasDAO);
            }
            else
            {
                 ventasDAO = (VentaDataAccessObject)Session["VentasDAO"];
            }
            this.ventas = ventasDAO.Ventas();
            this.currentUser = (User)Session["User"];
      
            if (!IsPostBack)
            {
                if (this.ventas != null)
                {
                    
                    if (this.currentUser is Agent)
                    {
                        BtnTodos.Visible = true;
                    }
                    else if (this.currentUser is ClientGroup)
                    {
                        BtnTodos.Visible = true;
                    }
                    else
                    {
                        BtnTodos.Visible = false;
                    }
                    this.Initialize();
                }
            }
            else
            {
                //this.LoadSelectedArticles();
            }
        }

        private void Initialize()
        {
            try
            {
                this.BtnTodos.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListAllOrders");
                this.BtnEstadoPendiente.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListPending");
                this.BtnEstadoCurso.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListInProgress");
                this.BtnEstadoServido.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListIssued");

                this.RadGrid1.Columns.FindByUniqueNameSafe("Client").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListClient");
                this.RadGrid1.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListCode");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Reference").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListReference");
                this.RadGrid1.Columns.FindByUniqueNameSafe("OrderDate").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderLisrOrderDate");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Volume").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListVolume");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Status").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListStatusWeb");
                this.RadGrid1.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListIssueDate");
                int iPriceSel = (int)Session["PriceSelector"];
                if (iPriceSel == 0)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport"); // +"(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") + ")";
                }
                else if (iPriceSel == 1)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport"); // +"(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
                }
                else if (iPriceSel == 2)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport"); // +"(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") + ")";
                }
                //REZ 29072014 - Hay que cambiarlo?
                else if (iPriceSel == 3)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport"); // +"(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + ")";
                }
                else if (iPriceSel == 4)
                {
                    this.RadGrid1.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OrderListImport"); // +"(" + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") + ")";
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);

            }
        }
      

        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                DataTable data = null;
                String Where = "";

                if (BtnTodos.Checked)
                {
                    data = LoadAllOrders();
                }
                else
                {
                    data = LoadClientOrders();
                }



                this.RadGrid1.DataSource = data;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private DataTable LoadAllOrders()
        {
            DataTable data = null;
            try
            {
                Sesion SesionJulia = new Sesion();
                VentaDataAccessObject VentasDAO;
                List<Venta> Ventas = new List<Venta>();
               
                if (Session["ALLOrders"] == null)
                {
                    // Carrega les factures de tots els clients del Agent
                    if (Session["User"] != null)
                    {
                        this.currentUser = (User)Session["User"];   
                    }
                    
                    if(this.currentUser is Agent)
                    {
                        Agent agente = (Agent)Session["User"];
                        VentasDAO = SesionJulia.GetVentasAgent(agente.CodVendedor);
                        Ventas= VentasDAO.Ventas();
                        Session.Add("ALLOrders", Ventas);
                    }
                    else if (this.currentUser is ClientGroup)
                    {
                        ClientGroup clientgroup = (ClientGroup)Session["User"];
                        VentasDAO = SesionJulia.GetVentasClientGroup(clientgroup.CodGrupo);
                        Ventas = VentasDAO.Ventas();
                        Session.Add("ALLOrders", Ventas);
                    }
                  
                }
                else
                {
                    Ventas = (List<Venta>)Session["ALLOrders"];
                }
                 data = GenerateTable();
                List<Venta> res = FiltrarChecks(Ventas);
                LoadVentas(res, ref data);  
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }
        private DataTable LoadClientOrders()
        {
            DataTable data = null;
            try
            {
                Sesion SesionJulia = new Sesion();
                List<Venta> Ventas;
                if (Session["Client"] == null)
                    throw new Exception("Session de client es null");

                Client cli = (Client)Session["Client"];

                if (Session[("ClientOrders" + cli.UserName)] == null)
                {
                    // Carrega les factures del client                
                    Ventas = this.ventas;
                    Session.Add("ClientOrders:" + cli.UserName, Ventas);
                }
                else
                {
                    Ventas = (List<Venta>)Session[("ClientOrders" + cli.UserName)];
                  
                }
                data = GenerateTable();
                List<Venta> res = FiltrarChecks(Ventas);
                LoadVentas(res, ref data);           
            
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }

        private List<Venta > FiltrarChecks(List<Venta > Entrada)
        {
            var result = from rows in Entrada
                         select rows;

            List<string> names = new List<string>();

            if (BtnEstadoPendiente.Checked)
            {
                //result = result.Where(rows => rows.Estado == "Pendiente");
                names.Add("Pendiente");
            }
            if (BtnEstadoCurso.Checked)
            {
                //result= result.Where(rows => rows.Estado == "En_curso");
                names.Add("En_curso");
            }
            if (BtnEstadoServido.Checked)
            {
                //result=result.Where(rows => rows.Estado == "Exp_Parcial");
                names.Add("Exp_Parcial");
                names.Add("Expedido");
            }

            var matching = result.Where(x => names.Contains(x.Estado));        
            return matching.ToList<Venta>();
        }
        private DataTable GenerateTable()
        {
            
            DataTable data = new DataTable();
            try
            {
                data.Columns.Add("Client");
                data.Columns.Add("ArticleCode");
                data.Columns.Add("Reference");
                data.Columns.Add("OrderDate", typeof(DateTime));
                data.Columns.Add("Volume", typeof(Double));
                data.Columns.Add("Status");
                data.Columns.Add("Description", typeof(DateTime));//fecha expedicion
                data.Columns.Add("Price", typeof(Decimal));
                data.Columns.Add("PedidoType");
                data.Columns.Add("Divisa");
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }
        private void LoadVentas(List <Venta > fVentas, ref DataTable data)
        {
            try
            {
                double aux;
                if (fVentas != null)
                {
                    foreach (Venta venta in fVentas)
                    {
                        //aux = Convert.ToDouble(venta.Volumen);
                        DataRow row = data.NewRow();
                        row["Client"] = venta.ClientCode + " - " +  venta.ClientName;
                        row["ArticleCode"] = venta.Number;
                        row["Reference"] = venta.Reference;
                        //row["OrderDate"] = venta.Fecha.Date.ToString();
                        row["OrderDate"] = venta.Fecha;
                        row["Volume"] = venta.Volumen;
                        row["Status"] = setStatus(venta.Estado);
                        //El camp descripcio correspont a FechaExpedicion

                        row["Description"] = venta.FechaExpedicion;
                        int iPriceSel = (int)Session["PriceSelector"];
                        if (iPriceSel == 0)
                        {
                            row["Price"] = venta.Puntos;
                        }
                        else if (iPriceSel == 1)
                        {
                            row["Price"] = venta.Importe;
                        }
                        else if (iPriceSel == 2)
                        {
                            row["Price"] = (venta.Puntos * currentUser.Coeficient);
                        }
                        //REZ 29072014 - Hay que cambiarlo de divisa??
                        else if (iPriceSel == 3)
                        {
                            row["Price"] = venta.Importe;
                        }
                        else if (iPriceSel == 4)
                        {
                            row["Price"] = (venta.Puntos * currentUser.Coeficient);
                        }
                       
                        row["PedidoType"] = venta.VentaType();

                        if (iPriceSel == 0)
                        {
                            row["Divisa"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                        }
                        else if (iPriceSel == 1)
                        {
                            row["Divisa"] = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                            
                        }
                        else if (iPriceSel == 2)
                        {
                            row["Divisa"] = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");

                        }
                        //REZ 29072014 - Hay que cambiarlo de divisa??
                        else if (iPriceSel == 3)
                        {
                            row["Divisa"] = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");

                        }
                        else if (iPriceSel == 4)
                        {
                            row["Divisa"] = (venta.Divisa == "USD") ? JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DollarsPVPSelector") : JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");

                        }





                        data.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
           
        }
        private string getText(string text)
        {
            return JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(text);
        }
        private string  setStatus(string p)
        {  
            string ret = string.Empty;
            try
            {


                switch (p)
                {
                    //Valores campo Status
                    case "Open":
                        ret = getText("StatusOpen");
                        break;
                    case "Released":
                        ret = getText("StatusReleased");
                        break;
                    case "Pending_Approval":
                        ret = getText("StatusPendingAp");
                        break;
                    case "Pending_Prepayment":
                        ret = getText("StatusPendingPre");
                        break;

                    //Valores campo Status_Web
                    case "Pendiente":
                        ret = getText("StatusPending");
                        break;
                    case "En_curso":
                        ret = getText("StatusInProgress");
                        break;
                    case "Exp_Parcial":
                        ret = getText("StatusPartialReleased");
                        break;
                    case "Expedido":
                        ret = getText("StatusReleased");
                        break;


                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return ret;
        }

        protected void BtnTodos_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void BtnEstadoPendiente_CheckedChanged(object sender, EventArgs e)
        {
            //if (BtnEstadoPendiente.Checked)
            //{
            //    string filterExpression;
            //    filterExpression = "([Status] = '" + "Pendiente" + "')";
            //    RadGrid1.MasterTableView.FilterExpression = filterExpression;
            //    //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
            //    //column.CurrentFilterFunction = GridKnownFunction.EqualTo;
            //    //column.CurrentFilterValue = "Pendiente";                          
            //}
            //else
            //{
            //    RadGrid1.MasterTableView.FilterExpression = string.Empty;
            //    RadGrid1.MasterTableView.Rebind();
            //    //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
            //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;                    
            //    //column.CurrentFilterValue = string.Empty;
            //}

            //RadGrid1.MasterTableView.Rebind();
           RadGrid1.Rebind();
        }
        protected void BtnEstadoCurso_CheckedChanged(object sender, EventArgs e)
        {

        //    if (BtnEstadoCurso.Checked)
        //    {
        //        string filterExpression;
        //        filterExpression = "([Status] = '" + "En_curso" + "')";
        //        RadGrid1.MasterTableView.FilterExpression = filterExpression;
        //        //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
        //        //column.CurrentFilterFunction = GridKnownFunction.EqualTo;
        //        //column.CurrentFilterValue = "En_curso";
        //    }
        //    else
        //    {
        //        RadGrid1.MasterTableView.FilterExpression = string.Empty;
        //        RadGrid1.MasterTableView.Rebind();
        //        //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
        //        //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
        //        //column.CurrentFilterValue = string.Empty;
        //    }
        //    RadGrid1.MasterTableView.Rebind();
       RadGrid1.Rebind();
        }
        protected void BtnEstadoServido_CheckedChanged(object sender, EventArgs e)
        {

            //if (BtnEstadoServido.Checked)
            //{
            //    string filterExpression;
            //    filterExpression = "([Status] = '" + "Exp_Parcial" + "')";
            //    RadGrid1.MasterTableView.FilterExpression = filterExpression;
            //    //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
            //    //column.CurrentFilterFunction = GridKnownFunction.EqualTo;
            //    //column.CurrentFilterValue = "Exp_Parcial";
            //}
            //else
            //{
            //    RadGrid1.MasterTableView.FilterExpression = string.Empty;
            //    RadGrid1.MasterTableView.Rebind();

            //    //GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("Status");
            //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            //    //column.CurrentFilterValue = string.Empty;

            //}
            //RadGrid1.MasterTableView.Rebind();
           RadGrid1.Rebind();
        }
        protected void RadGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                //Apply custom sorting
                GridSortExpression sortExpr;

                sortExpr = new GridSortExpression();

                switch (e.OldSortOrder)
                {
                    case GridSortOrder.None:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Descending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Ascending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = RadGrid1.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                    case GridSortOrder.Descending:
                        sortExpr.FieldName = e.SortExpression;
                        sortExpr.SortOrder = GridSortOrder.Ascending;

                        e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                        break;
                }

                e.Canceled = true;
                RadGrid1.Rebind();


                //Default sort order Descending


                if (!e.Item.OwnerTableView.SortExpressions.ContainsExpression(e.SortExpression))
                {
                    sortExpr = new GridSortExpression();
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        //protected void Panel1_PreRender(object sender, EventArgs e)
        //{
        //    RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);

        //    manager.AjaxSettings.AddAjaxSetting(BtnTodos, Panel1, LoadingPanel);
        //    manager.AjaxSettings.AddAjaxSetting(RadGrid1, Panel1, LoadingPanel);
        //}

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                if (item["Description"].Text == "01/01/0001")
                {
                    item["Description"].Text = "";
                }
            }

        }
      
       
    }
}
