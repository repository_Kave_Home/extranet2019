﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductInfoUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Web_Parts.ProductInfo.ProductInfoUserControl" %>
<%@ Register TagPrefix="julia" TagName="ImageGalleryUserControl" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/ImageGalleryUserControl.ascx" %>
<%@ Register TagPrefix="julia" TagName="RecentlyViewedUserControl" Src="~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/RecentlyViewed.ascx" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<SharePoint:CssRegistration ID="CssRegistrationProductInfo" Name="/Style Library/Julia/ProductInfo.css"
    runat="server" />
<style type="text/css">
    .ValuestableMeasuresTr td {
        padding: 0px 5px;
        white-space: nowrap;
    }

    .panelStock img, .panelStock ImgService {
        /*vertical-align: auto;*/
        display: inline !important;
    }

    .panelStock img, .panelStock span {
        /*vertical-align: baseline;*/
    }

    .RadGrid .rgActiveRow td, .RadGrid .rgHoveredRow td, .RadGrid .rgEditRow td {
        border: none !important;
        background: none !important;
        cursor: default;
    }

    .rgMasterTable, .RadGrid .rgMasterTable .rgSelectedCell, .RadGrid .rgSelectedRow {
        border: none !important;
        background: none !important;
    }

        .RadGrid .rgSelectedRow td {
            border-bottom-width: 0px !important;
        }
</style>
<script type="text/javascript">
<% if (this.currentArticle != null)
    { %>
    $(document).ready(function () {


        // Load Images
        JuliaClients.Service.getProductImagesByCode("<%= this.currentArticle.Code %>", "<%= this.currentArticle.ArticleVersion %>", "·1");
        //modify the href of download Image

        $("#DownloadImage").click(function () {
            $("#DownloadImage").attr("href", $("#Zoomer").attr("href"));
        });

        $(".tab_content").hide(); //Hide all content
        if ($("#tabs").children().length == 1) {

            $("#tabs li:first").attr("class", "active");
            $("#tabs li:first").show();
            $(".tab_container div:first").show();
        } else {
            //Mostramos por defecto el tercer tab
            $(".ThirdTab").addClass("active");
            $(".ThirdTab li").show();
            //$(".tab_container div:first + div + div").show();
            $(".tab_container div:first + div").show();

        }
        //modify the href of download Image
        $("ul.tabs li").click(function () {
            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active content
            $("html body").animate({
                scrollTop: $(document).height()
            }, "slow");
            //            return false;
        });

        //Load Documentation
        JuliaClients.Service.getProductDocuments("<%= this.currentArticle.Code %>", "<%= this.currentArticle.ArticleVersion %>");

    });
    function GoBack() {
        //window.history.back();
        window.location.href = "/Pages/RedirectTo.aspx?r=back&artId=<%= this.currentArticle.Code %>";
    }
<% } %>
</script>
<asp:Panel ID="ProductInfoPanel" runat="server">
    <div>
        <div>
            <img alt="" class="BackBtn" onclick="GoBack()" height="20px" src="/Style Library/Julia/img/Back_ProductInfo.png" />
        </div>
        <asp:Panel ID="TIttleDownload" CssClass="CaptionTitle" runat="server">
            <asp:Label runat="server" CssClass=" CaptionLbl TittleInside" ID="CaptionTitle2"></asp:Label>
        </asp:Panel>
    </div>
    <div class="productDetailZone">
        <div class="productLeftZone">
            <asp:Panel runat="server" ID="ProductImage" />
            <julia:ImageGalleryUserControl ID="ImageGalleryUserControl1" runat="server"></julia:ImageGalleryUserControl>
        </div>
        <div class="productRightZone">
            <asp:Panel ID="ProductTitle" runat="server" CssClass="productInfoBlock">
                <asp:Panel ID="ImgNewProd" runat="server">
                    <span>
                        <img src="/Style Library/Julia/img/new_ficha.png" /></span>
                </asp:Panel>
                <asp:Label runat="server" CssClass="productInfoTitle" ID="CaptionTitle"></asp:Label>
                <asp:Panel ID="Panel_Discount" CssClass="PanelImgDiscount" runat="server">
                    <asp:Label ID="Discount_lbl" runat="server" CssClass="Label_Discount"></asp:Label>
                </asp:Panel>
                <a class="DownloadImage" id="DownloadImage" href="#" target="_blank" download style="float: left; margin-left: 10px; display: none;">
                    <img alt="" src="/Style Library/Julia/img/descargar.png" onmouseover="this.src=this.src.replace('descargar.','descargar_on.');"
                        onmouseout="this.src=this.src.replace('descargar_on.','descargar.');" />
                </a>
            </asp:Panel>
            <br />
            <br />
            <div class="productInfoBlock">
                <asp:Label runat="server" ID="CaptionMeasures" CssClass="productInfoLabelText"></asp:Label>
                <table cellspacing="0" class="tableMeasures" style="width: 100%; max-width: 200px; margin: 0;">
                    <tr>
                        <td>
                            <img alt="" height="16px" src="/Style Library/Julia/img/Height.png" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="ProductHeight" CssClass="productInfoValueText"></asp:Label>
                        </td>
                        <td></td>
                        <td>
                            <img alt="" height="16px" src="/Style Library/Julia/img/Width.png" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="ProductWidth" CssClass="productInfoValueText"></asp:Label>
                        </td>
                        <td></td>
                        <td>
                            <img alt="" height="16px" src="/Style Library/Julia/img/Depth.png" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="ProductDepth" CssClass="productInfoValueText"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="0" style="width: 100%;">
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="CaptionCode" CssClass="productInfoLabelText"></asp:Label>
                            <asp:Label runat="server" ID="ValueCode" CssClass="productInfoValueText" Style="background-color: #eee; border-radius: 15px; padding: 3px 10px;"></asp:Label>
                        </td>
                        <td>
                            <span class="field_info_lbl lblstock" style="margin-left: 20px;">Stock</span>
                        </td>
                        <td>
                            <asp:Panel ID="PanelStockConCantidades" runat="server" CssClass="panelStock">
                                <asp:Panel runat="server" ID="StockNoNull">
                                    <asp:Panel ID="Panel1" runat="server" CssClass="panelStock">
                                        <asp:Label runat="server" ID="LblCantidadVerde" CssClass="LblStockBasicDec"></asp:Label>
                                        <asp:Image runat="server" ID="stockVerdeImg" CssClass="imgstock" ImageUrl="/Style%20Library/Julia/img/Green_Stock.png"
                                            Width="16px" />
                                    </asp:Panel>
                                    <asp:Panel ID="Panel2" runat="server" CssClass="panelStock">
                                        <asp:Label runat="server" ID="LblCantidadAzul" CssClass="LblStockBasicDec"></asp:Label>
                                        <asp:Image runat="server" CssClass="imgstock" ID="stockAzulImg" ImageUrl="/Style%20Library/Julia/img/Orange_Stock.png"
                                            Width="16px" />
                                    </asp:Panel>
                                    <asp:Panel ID="Panel3" runat="server" CssClass="panelStock">
                                        <asp:Label runat="server" ID="LblCantidadRoja" CssClass="LblStockBasicDec"></asp:Label>
                                        <asp:Image runat="server" ID="stockRojoImg" CssClass="imgstock" ImageUrl="/Style%20Library/Julia/img/Red_Stock.png"
                                            Width="16px" />
                                    </asp:Panel>
                                    <asp:Panel ID="Panel4" runat="server" CssClass="panelStock">
                                        <asp:Label runat="server" ID="LblCantidadGris" CssClass="LblStockBasicDec"></asp:Label>
                                        <asp:Image runat="server" ID="stockGrisImg" CssClass="imgstock" ImageUrl="/Style%20Library/Julia/img/Gray_Stock.png"
                                            Width="16px" />
                                    </asp:Panel>
                                    <asp:Image runat="server" ID="ImgNivellRepreStockNoNull" Width="15px" CssClass="ImgService" />
                                    <telerik:RadToolTip runat="server" ID="RadToolTipRepRojo" TargetControlID="stockRojoImg"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblRadToolTipRepRojo" runat="server" />
                                    </telerik:RadToolTip>
                                    <telerik:RadToolTip runat="server" ID="RadToolTipRepGreen" TargetControlID="stockVerdeImg"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblRadToolTipRepVerde" runat="server" />
                                    </telerik:RadToolTip>
                                    <telerik:RadToolTip runat="server" ID="RadToolTipRepAzul" TargetControlID="stockAzulImg"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblRadToolTipRepAzul" runat="server" />
                                    </telerik:RadToolTip>
                                    <telerik:RadToolTip runat="server" ID="RadToolTipRepGris" TargetControlID="stockGrisImg"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblRadToolTipRepGris" runat="server" />
                                    </telerik:RadToolTip>
                                    <telerik:RadToolTip runat="server" ID="ToolTipNivell" TargetControlID="ImgNivellRepreStockNoNull"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblToolTipImgNivellRepreNoNull" runat="server" />
                                    </telerik:RadToolTip>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="StockNull">
                                    <asp:Image runat="server" CssClass="panelStock imgstock" ID="StockNegroImg" ImageUrl="/Style%20Library/Julia/img/Black_Stock.png"
                                        Width="16px" />
                                    <asp:Image runat="server" ID="ImgNivellRepreStockNull" Width="15px" CssClass="ImgService" />
                                    <telerik:RadToolTip runat="server" ID="RadToolTip2" TargetControlID="StockNegroImg"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblRadToolTipRepNegro" runat="server" />
                                    </telerik:RadToolTip>
                                    <telerik:RadToolTip runat="server" ID="RadToolTip3" TargetControlID="ImgNivellRepreStockNull"
                                        IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                        RelativeTo="Mouse" Animation="None">
                                        <asp:Label ID="LblToolTipNivellRepreNull" runat="server" />
                                    </telerik:RadToolTip>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="PanelStockSinCantidades" CssClass="panelStock">
                                <asp:Image runat="server" ID="ImgStock" Width="15px" CssClass="panelStock imgstock" />
                                <asp:Image runat="server" ID="ImgNivellClient" Width="15px" CssClass="ImgService" />
                                <telerik:RadToolTip runat="server" ID="RadToolTip1" TargetControlID="ImgStock" IsClientID="false"
                                    ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight" RelativeTo="Mouse"
                                    Animation="None">
                                    <asp:Label ID="LblStock" runat="server" />
                                </telerik:RadToolTip>
                                <telerik:RadToolTip runat="server" ID="RadToolTip4" TargetControlID="ImgNivellClient"
                                    IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                    RelativeTo="Mouse" Animation="None">
                                    <asp:Label ID="LblNivellToolTipClient" runat="server" />
                                </telerik:RadToolTip>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="horizontalSeparator">
            </div>
            <div class="productInfoBlock">
                <asp:Label runat="server" CssClass="productInfoLabelText" ID="CaptionPrice"></asp:Label>
                <asp:Label runat="server" CssClass="productInfoPriceText" ID="ValuePrice"></asp:Label>
                <asp:Label runat="server" CssClass="productInfoPriceTextDisc" ID="ValuePriceDiscount"></asp:Label>
                <div class="infoDicsount">
                </div>
            </div>
            <div class="productInfoBlock">
                <div style="float: left; margin-right: 20px; padding-top: 5px;">
                    <telerik:RadNumericTextBox ShowSpinButtons="true" LabelCssClass="field_info_lbl LblCantidad"
                        IncrementSettings-InterceptArrowKeys="true" IncrementSettings-InterceptMouseWheel="true"
                        runat="server" MaxLength="5" ID="itemQty" MaxValue="99999" NumberFormat-DecimalDigits="0"
                        Value="1" Skin="Metro" />
                </div>
                <div>
                    <asp:ImageButton ID="AddToOrder" OnClick="AddTOrder" runat="server" ImageUrl="<%$SPUrl:/Style Library/Julia/img/~language/add_to_car.gif%>"
                        onmouseover="this.src=this.src.replace('add_to_car.','add_to_car_on.');" onmouseout="this.src=this.src.replace('add_to_car_on.','add_to_car.');" />
                    <i class="fa fa-heart-o addToWishlistIcon" id="searchHeart-<%= this.currentArticle.Code%>"
                        onclick="addToWishlist('<%= this.currentArticle.EANcode %>', '<%= this.currentArticle.Code %>', '<%= this.currentArticle.CatalogNo %>')"></i>
                </div>
            </div>
            <div class="productInfoBlock">
                <asp:Label runat="server" ID="CaptionUnidades"></asp:Label>
                <asp:Label ID="ValueUnidades" runat="server"></asp:Label>
            </div>
            <div class="productInfoBlock">
                <asp:Label runat="server" ID="CaptionReference"></asp:Label>
                <telerik:RadTextBox TextMode="MultiLine" MaxLength="30" ButtonCssClass="lineInfoTxt"
                    runat="server" ID="ValueDescriptionArea" CssClass="txtareareference" Skin="Metro">
                </telerik:RadTextBox>
            </div>
            <div class="productInfoBlock">
                <julia:RecentlyViewedUserControl runat="server"></julia:RecentlyViewedUserControl>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="PnldtoWebMsg" Visible="false">
        <div id="Left" style="float: left; margin-left: 38px; margin-top: 10px; margin-bottom: 10px; width: 450px; overflow: hidden; background-color: #E8E8E8;">
            <asp:Label runat="server" ID="CaptionDtoWebMsg"></asp:Label>
        </div>
    </asp:Panel>
    <div id="photo" class="catalogPhoto" style="display: none;">
        <asp:Panel runat="server" CssClass="BasicInformation" ID="BasicInformation">
            <asp:Panel runat="server" CssClass="BasicInfoLeft" ID="BasicInfoLeft">
            </asp:Panel>
            <asp:Panel runat="server" ID="BasicInfoRight" CssClass="BasicInfoRight">
            </asp:Panel>
        </asp:Panel>
    </div>
    <div class="container">
        <ul id="tabs" class="tabs">
            <li class="firsttab"><a href="#tab1" onclick="return false;">
                <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DescripciónProductoTab")%></a></li>
            <%-- <asp:Panel runat="server" ID="ColorMeasureTab">
                <li class="SecondTab"><a href="#tab2" onclick="return false;">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("OpcionesMedidasColoresTab")%></a></li></asp:Panel>--%>
            <asp:Panel runat="server" ID="ArticlesDisponibleTab">
                <li class="ThirdTab"><a href="#tab3" onclick="return false;">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ColeccióncompletaTab")%></a></li>
            </asp:Panel>
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="weight">
                    <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionCodProgram"></asp:Label>
                    <asp:HyperLink ID="LinkProgram" CssClass="LinkProveidor" runat="server">
                        <asp:Label runat="server" ID="ValueCodProgram"></asp:Label>
                    </asp:HyperLink>
                </div>
                <div class="Catalog">
                    <asp:Label runat="server" ID="CaptionCatalog" CssClass="CaptionLbl"></asp:Label>
                    <asp:Label runat="server" ID="ValueCatalog"></asp:Label>
                </div>
                <div class="Description">
                    <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionDescriptiion"></asp:Label>
                    <asp:Label runat="server" ID="ValueDescription"></asp:Label>
                </div>
                <div class="ColorMeasuresAndOthers">
                    <div class="ColorsAndMore" style="width: 31%; float: left; margin: 0 1%;">
                        <div class="color ">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionColor"></asp:Label>
                            <asp:Label runat="server" ID="ValueColor"></asp:Label>
                        </div>
                        <div class="materials darkenRow">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMaterials"></asp:Label>
                            <asp:Label runat="server" ID="ValueMaterials"></asp:Label>
                        </div>
                        <div class="Mantain">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMantain"></asp:Label>
                            <asp:Label runat="server" ID="ValueMantain"></asp:Label>
                        </div>
                        <div class="build darkenRow">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionBuild"></asp:Label>
                            <asp:Label runat="server" ID="ValueBuild"></asp:Label>
                        </div>
                        <div class="build">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionApilable"></asp:Label>
                            <asp:Label runat="server" ID="ValueApilable"></asp:Label>
                        </div>
                        <div class="cubic darkenRow">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionCubic"></asp:Label>
                            <asp:Label runat="server" ID="ValueCubic"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionWeight"></asp:Label>
                            <asp:Label runat="server" ID="ValueWeight"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 25%; float: left; margin: 0 1%;">
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionEAN"></asp:Label>
                            <asp:Label runat="server" ID="ValueEAN"></asp:Label>
                        </div>
                        <div class="weight darkenRow">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionCodArancel"></asp:Label>
                            <asp:Label runat="server" ID="ValueCodArancel"></asp:Label>
                        </div>
                        <div class="weight">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionPaisOrigen"></asp:Label>
                            <asp:Label runat="server" ID="ValuePaisOrigen"></asp:Label>
                        </div>
                        <div class="NumberofBultos darkenRow">
                            <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionNumbers"></asp:Label>
                            <asp:Label runat="server" ID="ValueNumbers"></asp:Label>
                        </div>
                        <div class="darkenRow">
                            <asp:Panel ID="PanelMeasures" runat="server">
                                <asp:Label runat="server" CssClass="CaptionLbl" ID="CaptionMeasuresBulto"></asp:Label>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="DocumentationPanel" style="width: 37%; float: left; margin: 0 1%;">
                        <div id="Documentation" class="DocumentationPanelDocumentacionTenica" style="display: none">
                            <div class="DocsHeader">
                                <asp:Label runat="server" ID="DocsHeaderLbl" CssClass="CaptionLbl"></asp:Label>
                            </div>
                            <div class="documents" id="ptechdocs">
                            </div>
                        </div>
                        <div id="Certificados" class="CertificadosPanel" style="display: none">
                            <div class="DocsHeader">
                                <asp:Label runat="server" ID="DocsHeaderLblCert" CssClass="CaptionLbl"></asp:Label>
                            </div>
                            <div class="documents" id="pcertdocs">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Description">
                    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductoHomologado")%>
                </div>
            </div>
            <%--<div id="tab2" class="tab_content">
                <asp:Panel runat="server" ID="Panel_Colors" CssClass="PanelColorsAgrup">
                    <asp:Label runat="server" ID="AvaliableColors_Lbl" CssClass="LblAgrupCaption LblColorAgrupCaption CaptionLbl LblProductCompatible"></asp:Label>
                    <telerik:RadListView ID="RadListViewColors" CssClass="RadListView" runat="server"
                        ItemPlaceholderID="PlaceHolder1">
                        <LayoutTemplate>
                            <asp:Panel ID="productPanel" runat="server" CssClass="ProductPanelColorAgrup">
                                <div>
                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                </div>
                            </asp:Panel>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Url") %>' ID="LynkColor" CssClass="LynkColor">
                                <img alt="<%# Eval("Description") %>" src="<%# Eval("ImageUrl") %>" class="ColorItem" />
                            </asp:HyperLink>
                            <telerik:RadToolTip runat="server" ID="ToolTipColor" TargetControlID='LynkColor'
                                IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                Width="100px" RelativeTo="Mouse" Animation="None">
                                <asp:Label ID="LblTollTIpCOlor" Text='<%#Eval("ToolTipText")%>' Width="200px" runat="server" />
                            </telerik:RadToolTip>
                        </ItemTemplate>
                    </telerik:RadListView>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="Panel_Measures" ID="Panel_Measures">
                    <div>
                        <asp:Label runat="server" ID="Measures_Lbl" CssClass="CaptionLbl LblProductCompatible"></asp:Label>
                        <telerik:RadComboBox runat="server" CssClass="Combomeasures" ID="ComboMeasures" OnSelectedIndexChanged="ComboMeasures_SelectedIndexChanged"
                            AutoPostBack="true">
                        </telerik:RadComboBox>
                    </div>
                </asp:Panel>
            </div>--%>
            <div id="tab3" class="tab_content">
                <asp:Label runat="server" ID="Disponibilidad_Lbl" CssClass="CaptionLbl LblProductCompatible"></asp:Label>
                <telerik:RadGrid ID="RagridDisponiblitat" runat="server" Width="97%" CssClass="Grid"
                    Skin="Metro" AutoGenerateColumns="false" EnableAjaxSkinRendering="true" AllowMultiRowSelection="false"
                    GridLines="None" AllowAutomaticUpdates="true" OnNeedDataSource="RagridDisponiblitat_NeedDataSource"
                    OnItemCommand="RagridDisponiblitat_ItemCommand">
                    <ClientSettings EnableRowHoverStyle="true">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <MasterTableView Summary="RadGrid table" UseAllDataFields="true">
                        <Columns>
                            <telerik:GridRowIndicatorColumn Display="false" UniqueName="Id" />
                            <telerik:GridBoundColumn DataField="ArticleUrl" UniqueName="ArticleUrl" Display="false" />
                            <telerik:GridHTMLEditorColumn UniqueName="SmallImage" DataField="SmallImage" />
                            <telerik:GridBoundColumn DataField="ArticleCode" UniqueName="ArticleCode" />
                            <telerik:GridBoundColumn DataField="Description" UniqueName="Description" ReadOnly="true" />
                            <telerik:GridBoundColumn DataField="Color" UniqueName="Color" ReadOnly="true" />
                            <telerik:GridBoundColumn DataField="Measures" UniqueName="Measures" ReadOnly="true" />
                            <telerik:GridBoundColumn DataField="Price" HeaderText="Price" UniqueName="Price"
                                ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="Stock" HeaderText="Stock" DataType="System.String">
                                <ItemTemplate>
                                    <div id="Div1" class="DivStockImages" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibleStockControlRepresentante").ToString())%>'>
                                        <label id="Label1" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibleStockNoNull").ToString())%>'
                                            class="LblCantdidades" style="display: inline;">
                                            <div id="Div6" class="StockBox" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibilidadCantidadVerde").ToString())%>'>
                                                <%#Eval("CantidadVerde")%>
                                                <asp:Image runat="server" ID="Image1" ImageUrl="/Style%20Library/Julia/img/Green_Stock.png"
                                                    Width="16px" />
                                            </div>
                                            <div id="Div7" class="StockBox" visible='<%# Convert.ToBoolean(Eval("VisibilidadCantidadAzul").ToString())%>'
                                                runat="server">
                                                <%#Eval("CantidadAzul")%>
                                                <asp:Image runat="server" ID="Image2" ImageUrl="/Style%20Library/Julia/img/Orange_Stock.png"
                                                    Width="16px" />
                                            </div>
                                            <div id="Div8" class="StockBox" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibilidadCantidadRoja").ToString())%>'>
                                                <%#Eval("CantidadRoja")%>
                                                <asp:Image runat="server" ID="Image3" ImageUrl="/Style%20Library/Julia/img/Red_Stock.png"
                                                    Width="16px" />
                                            </div>
                                            <div id="Div11" class="StockBox" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibilidadCantidadGris").ToString())%>'>
                                                <%#Eval("CantidadGris")%>
                                                <asp:Image runat="server" ID="Image5" ImageUrl="/Style%20Library/Julia/img/Gray_Stock.png"
                                                    Width="16px" />
                                            </div>
                                            <div class="ServiceImgBox">
                                                <asp:Image ID="ImgStockNoNullNivelServicio" runat="server" ImageUrl='<%#Eval("ImgNivelServicio")%>'
                                                    Width="15px" CssClass="ImgNivelServicio" Visible='<%# Convert.ToBoolean(Eval("NivelDeServicioVIsible").ToString())%>' />
                                            </div>
                                        </label>
                                        <div id="Div9" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibleStockNull").ToString())%>'>
                                            <div class="StockBox">
                                                <asp:Image runat="server" ID="Image4" ImageUrl="/Style%20Library/Julia/img/Black_Stock.png"
                                                    Width="16px" />
                                            </div>
                                            <div class="ServiceImgBox">
                                                <asp:Image Visible='<%# Convert.ToBoolean(Eval("NivelDeServicioVIsible").ToString())%>'
                                                    ID="ImgStockNullNivelServicio" CssClass="ImgNivelServicio" Width="15px" ImageUrl='<%#Eval("ImgNivelServicio")%>'
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipRep1" TargetControlID="Image3"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="Label2" Text='<%#Eval("StockTooltipRojo")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipRep2" TargetControlID="Image1"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="Label3" Text='<%#Eval("StockTooltipVerde")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipRep3" TargetControlID="Image2"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="Label4" Text='<%#Eval("StockTooltipAzul")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipRep4" TargetControlID="Image5"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="Label7" Text='<%#Eval("StockTooltipGris")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTip5" TargetControlID="Image4"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="Label5" Text='<%#Eval("StockTooltipNegro")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipNivelServicioNoNull" TargetControlID="ImgStockNoNullNivelServicio"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="LblToolTipStockNoNullServicio" Text='<%#Eval("ToolTipNivelServicio")%>'
                                                runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipNivelServicioNull" TargetControlID="ImgStockNullNivelServicio"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="LblToolTipStockNullServicio" Text='<%#Eval("ToolTipNivelServicio")%>'
                                                runat="server" />
                                        </telerik:RadToolTip>
                                    </div>
                                    <!-- STOCK FOR CLIENTES -->
                                    <div id="Div10" class="DivStockImages" runat="server" visible='<%# Convert.ToBoolean(Eval("VisibleStockControlClient").ToString())%>'>
                                        <div class="StockBox">
                                            <asp:Image runat="server" ID="ImgStockClient" ImageUrl='<%#Eval("Stock")%>' Width="16px" />
                                        </div>
                                        <div class="ServiceImgBox">
                                            <asp:Image class="ImgNivelServicio" ID="ImgNivelDeServicioClient" CssClass="ImgNivelServicio"
                                                Style="margin-left: 0px;" ImageUrl='<%#Eval("ImgNivelServicio")%>' Visible='<%# Convert.ToBoolean(Eval("NivelDeServicioVIsible").ToString())%>'
                                                runat="server" Width="15px" />
                                        </div>
                                        <telerik:RadToolTip runat="server" ID="RadToolTip6" TargetControlID="ImgStockClient" IsClientID="false"
                                            ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight" RelativeTo="Mouse"
                                            Animation="None">
                                            <asp:Label ID="Label6" Text='<%#Eval("StockTooltip")%>' runat="server" />
                                        </telerik:RadToolTip>
                                        <telerik:RadToolTip runat="server" ID="RadToolTipClientNivelServicio" TargetControlID="ImgNivelDeServicioClient"
                                            IsClientID="false" ShowEvent="OnMouseOver" HideEvent="Default" Position="TopRight"
                                            RelativeTo="Mouse" Animation="None">
                                            <asp:Label ID="LblToolClientlServicio" Text='<%#Eval("ToolTipNivelServicio")%>' runat="server" />
                                        </telerik:RadToolTip>
                                    </div>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="wishlsts" HeaderText="Wishlist" DataType="System.String">
                                <ItemTemplate>
                                    <i class="fa fa-heart-o addToWishlistIcon" id="searchHeart-<%# Eval("ArticleCode").ToString() %>"
                                        onclick="addToWishlist('<%# Eval("EANCode").ToString() %>', '<%# Eval("ArticleCode").ToString() %>', '<%# Eval("CatalogNo").ToString() %>')"></i>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditItemStyle ForeColor="Gray"></EditItemStyle>
                    </MasterTableView>
                    <PagerStyle Mode="NextPrev" />
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</asp:Panel>
<telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
    AutoCloseDelay="3000" OffsetY="-100" EnableShadow="true" Position="MiddleLeft"
    ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"
    Skin="Metro" Text="Producto Añadido">
</telerik:RadNotification>
<asp:Panel ID="debugPanel" runat="server">
    <asp:Literal ID="DegubInfoText" runat="server"></asp:Literal>
</asp:Panel>
