﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Collections.Generic;
using Microsoft.SharePoint;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint.Utilities;
using Telerik.Web.UI;
using System.Globalization;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using JuliaGrupUtils.Utils;
using System.Data;
using System.Linq;
using System.Threading;
namespace JuliaGrupPortalClientes_v2.Web_Parts.ProductInfo
{
    public partial class ProductInfoUserControl : UserControl
    {
        Order CurrentOrder;
        public Article currentArticle;
        List<Article> Products = new List<Article>();
        private Order currentOrder;
        private User CurrentUser;
        private Client CurrentClient;
        ArticleDataAccessObject articleDAO = null;
        string imgURL = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos parámetros de query string para ver si podemos cargar la página
                string productCode = Request.QueryString["code"];
                string catalog = Request.QueryString["catalog"];
                string Version = Request.QueryString["version"];

                if (string.IsNullOrEmpty(productCode) || string.IsNullOrEmpty(catalog))
                {
                    //No podemos mostrar la pagina
                    DegubInfoText.Text = "Product not found";
                    ProductInfoPanel.Visible = false;
                    debugPanel.Visible = true;
                    return; //End
                }

                if (Session["ArticleDAO"] != null)
                    {
                        this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];

                    }
                    else
                    {
                        throw new SPException("Error: ArticleDAO session var is Null");
                    }
                    Products = articleDAO.GetAllProducts();

                    this.currentArticle = articleDAO.GetProductByCodeAndCatalog(productCode, Version, catalog);
                    this.CurrentOrder = (Order)Session["Order"];
                    this.inicialice();


                    if (!String.IsNullOrEmpty(productCode))
                    {

                        //this.currentArticle = articleDAO.GetProductByCode(productCode);   //Ya tenemos el currentArticle de antes. Con esto la liamos... porque no filtra por catalogo...
                        //this.currentArticle = articleDAO.GetProductByCode(productCode, productVersion);

                        if (!Page.IsPostBack)
                        {
                            this.SetArticleDescriptionValues(this.currentArticle);
                        }
                        
                        //REZ 03032014 -- No cargo las imagenes en server. Las cargo en cliente
                        List<SPFile> images = new List<SPFile>();
                        //List<SPFile> images = articleDAO.GetProductImagesByCode(productCode, Version, "·1");
                        SetPreviewImages(images);

                    }
                
                
            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
           
        }
       
        private void SetPreviewImages(List<SPFile> images)
        {
            try
            {
                string url = "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=imageid&id={0}&type={1}&p={2}";
                ((ImageGalleryUserControl)this.ImageGalleryUserControl1).ImageUrls = new List<string>();

                bool first = true;
                int imgorder = 1;
                foreach (SPFile item in images)
                {
                    //if (item.Properties["_dlc_DocId"] != null)
                    //{
                    //    ((ImageGalleryUserControl)this.imageGallery).ImageUrls.Add(string.Format(url, item.Properties["_dlc_DocId"].ToString(), "1"));
                    //    if (first)
                    //        ((ImageGalleryUserControl)this.imageGallery).ProductCode = item.Properties["_dlc_DocId"].ToString();
                    //}
                    ////Lo intento por Nombre de documento
                    if (item.Name != null)
                    {
                        //((ImageGalleryUserControl)this.ImageGalleryUserControl1).ImageUrls.Add(string.Format(url, item.Name.ToString(), "1", "1"));
                        //REZ 19082013 - Cargamos directamente de intranet para evitar problemas de lentitud de carga
                        ((ImageGalleryUserControl)this.ImageGalleryUserControl1).ImageUrls.Add(JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                                JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                item.Url);
                        if (first)
                        {
                            //((ImageGalleryUserControl)this.ImageGalleryUserControl1).ProductCode = item.Name.ToString();
                            ((ImageGalleryUserControl)this.ImageGalleryUserControl1).ProductCode =JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                                JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                    item.Url;
                            first = false;
                        }
                    }

                    //((ImageGalleryUserControl)this.imageGallery).ImageUrls.Add(string.Format(url, this.productCode, "1", imgorder.ToString()));

                    //if (imgorder == 1)
                    //        ((ImageGalleryUserControl)this.imageGallery).ProductCode = this.productCode;
                    //imgorder++;
                }
                if (string.IsNullOrEmpty(((ImageGalleryUserControl)this.ImageGalleryUserControl1).ProductCode))
                {
                    //((ImageGalleryUserControl)this.ImageGalleryUserControl1).ProductCode = "nodisponible";
                    string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                    ((ImageGalleryUserControl)this.ImageGalleryUserControl1).ProductCode = "/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE" + "_1" + ".jpg";
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private HtmlGenericControl getHtmlDocumentItem(string link, string text, SPListItem item)
        { 
            HtmlGenericControl div = new HtmlGenericControl("div");
            try
            {
               
                div.Attributes.Add("class", "documentItem");
                HtmlGenericControl icon = new HtmlGenericControl("img");
                icon.Attributes.Add("class", "pdf_ico");
                icon.Attributes.Add("alt", "");

                //REZ 03032014 -- No se usa...
                //string docIcon = SPUtility.ConcatUrls("/_layouts/images/",
                //SPUtility.MapToIcon(item.Web, SPUtility.ConcatUrls(item.Web.Url, item.Url), "", IconSize.Size16));
                icon.Attributes.Add("src", "/Style Library/Julia/Img/pdf_ico.jpg");


                HtmlGenericControl a = new HtmlGenericControl("a");
                a.Attributes.Add("href", link);
                a.InnerHtml = text;

                div.Controls.Add(icon);
                div.Controls.Add(a);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return div;
        }
        private void inicialice()
        {
            if (Session["Order"] != null)
            {
                currentOrder = (Order)Session["Order"];
            }

            //this.CaptionTitle.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString(""); 
            this.CaptionCatalog.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CatalogLbl") + " : ";
            this.CaptionDescriptiion.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription") + " : ";
            this.CaptionColor.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Color") + " :";
            this.CaptionMaterials.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMaterials") + " : ";
            this.CaptionMantain.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductMantain") + " : ";
            this.CaptionBuild.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductBuilt") + " : ";
            this.CaptionCubic.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductVolume") + " : ";
            this.CaptionWeight.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductWeight") + " : ";
            this.CaptionNumbers.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NumberOfPorducts") + " : ";
            this.DocsHeaderLblCert.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DocumentacionTecnica") + " : ";
            this.CaptionCode.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode") + " : ";
            this.CaptionMeasures.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasProducto");
            this.CaptionPrice.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Price") + " : ";
            this.CaptionMeasuresBulto.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasBulto") + " : ";
            this.CaptionUnidades.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblUnidadesVenta") + " : ";
            this.itemQty.Label = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Number") + " : ";
            this.CaptionReference.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblLineDescription") + " : ";
            this.DocsHeaderLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DocumentacionTecnica") + " : ";
            this.DocsHeaderLblCert.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Certificaciones") + " : ";
            //this.AvaliableColors_Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Product_AvaliableColors");

            //REZ 25072014 - Nous atributs
            this.CaptionEAN.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionEAN") + " : ";
            this.CaptionCodArancel.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionCodArancel") + " : ";
            this.CaptionCodProgram.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionCodProgram") + " : ";
            this.CaptionApilable.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionApilable") + " : ";
            this.CaptionPaisOrigen.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionPaisOrigen") + " : ";

            //headers

            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("SmallImage").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SmallImageOrder");
            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("ArticleCode").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductCode");
            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Description").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductDescription");
            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Price").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("UnitariPriceOrder");
            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Color").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Color");
            this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Measures").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("LblMedidasProducto");
            //this.RagridDisponiblitat.Columns.FindByUniqueNameSafe("Stock").HeaderText = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Stock");

            //REZ21012015 - Mensaje DtoWeb
            if (Session["Client"] != null)
            {
                CurrentClient = (Client)Session["Client"];
            }
            if (CurrentClient.DtoWeb > 0)
            {
                this.PnldtoWebMsg.Visible = true;
                this.CaptionDtoWebMsg.Text = String.Format(
                                                JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DtoWebMsg"),
                                                CurrentClient.DtoWeb.ToString()
                                             );
            }

        }
        private void SetArticleDescriptionValues(Article a)
        {
            try
            {
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                string volume = a.Height + " " + a.Width + " " + a.Length;

                //Cal obtenir la dada de unidadVenta

                this.CaptionTitle.Text = a.Code + " - " + a.Description;
                this.ValueDescription.Text = a.DescripcionLarga;
                this.ValueColor.Text = a.Color;
                this.ValueMaterials.Text = a.ItemMateriales;
                this.ValueMantain.Text = a.ItemMantenimiento;
                this.ValueBuild.Text = a.ItemMontaje;
                this.ValueCubic.Text = a.Volume;
                this.ValueWeight.Text = a.Weight;
                this.ValueNumbers.Text = (a.Numitems.IndexOf(",") >=0)?a.Numitems.Substring(0,a.Numitems.IndexOf(",")):a.Numitems;
                this.ValueCode.Text = a.Code;

                this.ValueEAN.Text = a.EANcode;
                this.ValueCodArancel.Text = a.CodigoArancelario;
                this.ValuePaisOrigen.Text = a.PaisOrigen;
                this.ValueCodProgram.Text = a.Programa;
                this.LinkProgram.NavigateUrl = "/Pages/RedirectTo.aspx?r=products&p=" + a.Programa;
                this.ValueApilable.Text = a.Apilable;

                int iPriceSel = (int)Session["PriceSelector"];
                CultureInfo culture = new CultureInfo("es-Es", false);
                if (iPriceSel == 0)
                {
                    this.ValuePrice.Text = a.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points") + " ";
                }
                else if (iPriceSel == 1)
                {
                    this.ValuePrice.Text = a.Price.ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros") + " ";
                }
                else if (iPriceSel == 2)
                {
                    this.ValuePrice.Text = ((Convert.ToDouble(a.PriceInPoints, new CultureInfo("es-Es", false))) * (Convert.ToDouble(CurrentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector") + " ";
                }
                else if (iPriceSel == 3)
                {
                    this.ValuePrice.Text = (Convert.ToDouble(a.Price, culture) * 
                                            Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                            .ToString("N2", culture) + " " + CurrentClient.LiteralDivisaWEB + " ";
                }
                else if (iPriceSel == 4)
                {
                    this.ValuePrice.Text = (
                                            (Convert.ToDouble(a.PriceInPoints, culture)) *
                                            (Convert.ToDouble(CurrentUser.Coeficient, culture)) * 
                                            (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                            ).ToString("N2", new CultureInfo("es-Es", false))
                                            + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                }


                if (a.Discount.ToString() == "0,00")
                {
                     
                    this.Panel_Discount.BackImageUrl = "";
                    this.Discount_lbl.Text = "";
                }
                else
                {
                    //this.discount.Text = !String.IsNullOrEmpty(a.Discount) ? Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N0") : "-";
                    this.Panel_Discount.BackImageUrl = setImgDiscountURL(a);
                  
                    //
                    //this.Discount_lbl.Text = "-"+Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N0") + "%";
                    this.Discount_lbl.Text = "-" + Convert.ToDouble(a.Discount, new CultureInfo("es-ES")).ToString("N2",culture).Replace(",00","") + "%";
               
                    this.ValuePrice.Style.Add("text-decoration", "line-through");
                    this.ValuePrice.Style.Add("margin-right", "1rem");
                    this.ValuePriceDiscount.Text = a.DiscountPriceNet;
                    this.ValuePriceDiscount.Style.Add("color", "green");
                    this.ValuePriceDiscount.Style.Add("font-size", "20px");
                    
                    if (iPriceSel == 0)
                    {
                        this.ValuePriceDiscount.Text = a.DiscountNet + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                    }
                    else if (iPriceSel == 1)
                    {
                        this.ValuePriceDiscount.Text = a.DiscountPriceNet.ToString(culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                    }
                    else if (iPriceSel == 2)
                    {
                        if (CurrentUser.ShopInShop)
                        {
                            this.ValuePriceDiscount.Text = ((Convert.ToDouble(a.DiscountNetDecimal, culture)) * (Convert.ToDouble(CurrentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        }
                        else
                        {
                            this.ValuePriceDiscount.Text = ((Convert.ToDouble(a.DiscountNet, culture)) * (Convert.ToDouble(CurrentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                        }
                    }
                    else if (iPriceSel == 3)
                    {
                        this.ValuePriceDiscount.Text = ((Convert.ToDouble(a.DiscountPriceNet, culture)) * 
                                                        (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                                        ).ToString(culture) 
                                                        + " " + CurrentClient.LiteralDivisaWEB;
                    }
                    else if (iPriceSel == 4)
                    {
                        this.ValuePriceDiscount.Text = (
                                                        (Convert.ToDouble(a.DiscountNet, culture)) *
                                                        (Convert.ToDouble(CurrentUser.Coeficient, culture)) *
                                                        (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                                        ).ToString("N2", culture)
                                                        + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                    }

                }





                if (a.ProductNew == "No")
                {
                    this.ImgNewProd.Visible = false;
                }
                else
                {
                    this.ImgNewProd.Visible = true;
                }
                this.ValueUnidades.Text = a.UnidadMedida.ToString("N0"); ;
                this.ValueCatalog.Text = a.CatalogNo;
               
                SetTableMeasures(a.Measures);
                this.ProductWidth.Text = a.Width + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                this.ProductHeight.Text = a.Height + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                this.ProductDepth.Text = a.Length + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm"); 
                this.itemQty.MinValue = 1;
                this.itemQty.MaxLength = 5;
                this.itemQty.IncrementSettings.Step = (Double)a.UnidadMedida;
                this.itemQty.Value = (Double)a.UnidadMedida;

                if (a.TipoAgrup != 0 && a.DetalleAgrup != string.Empty)
                {
                    
                  
                    if (a.TipoAgrup == 1 && a.DetalleAgrup != string.Empty)
                    {
                        RagridDisponiblitat.Visible = true;
                        //ColorMeasureTab.Visible = true;
                        ArticlesDisponibleTab.Visible = true;
                        //Panel_Colors.Visible = true;
                        //Panel_Measures.Visible = false;
                        //this.RadListViewColors.DataSource = ColorItems(a);
                        (RagridDisponiblitat.MasterTableView.GetColumn("Measures") as GridBoundColumn).Display = false;
                        this.RagridDisponiblitat.DataSource = LoadArticlesDisponibles(a);
                    }
                    else if (a.TipoAgrup == 2 && a.DetalleAgrup != string.Empty)
                    {
                        RagridDisponiblitat.Visible = true;
                        //ColorMeasureTab.Visible = true;
                        ArticlesDisponibleTab.Visible = true;
                        //Measures_Lbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Product_Measures_Lbl");
                        //Panel_Colors.Visible = false;
                        //Panel_Measures.Visible = true;
                        (RagridDisponiblitat.MasterTableView.GetColumn("Color") as GridBoundColumn).Display = false;
                        //loadComboMeasuersValue(a);
                        this.RagridDisponiblitat.DataSource = LoadArticlesDisponibles(a);
                    }
                    else
                    {
                        //Si hacemos la query por programa, mostramos siempre la pestanya
                        //RagridDisponiblitat.Visible = false;
                        //ColorMeasureTab.Visible = false;
                        ArticlesDisponibleTab.Visible = false;
                        //Panel_Colors.Visible = false;
                        //Panel_Measures.Visible = false;
                    }
                   
                    
                }
                else
                {
                    //ColorMeasureTab.Visible = false;
                    ArticlesDisponibleTab.Visible = false;
                    //Panel_Colors.Visible = false;
                    //Panel_Measures.Visible = false;
                    RagridDisponiblitat.Visible = false;
                }
                //Mostramos siempre
                ArticlesDisponibleTab.Visible = true;
                RagridDisponiblitat.Visible = true;
                this.RagridDisponiblitat.DataSource = LoadArticlesDisponibles(a);

                ////NO DECIMALS
                //this.colorLbl.Text = !String.IsNullOrEmpty(a.Color) ? a.Color : "-";
                //this.widthLbl.Text = !String.IsNullOrEmpty(a.Width) ? a.Width : "-";
                //this.heightLbl.Text = !String.IsNullOrEmpty(a.Height) ? a.Height : "-";
                //this.lenghtLbl.Text = !String.IsNullOrEmpty(a.Length) ? a.Length : "-";
               
               
                ////1 DECIMAL
                //this.weightLbl.Text = !String.IsNullOrEmpty(a.Weight) ? Convert.ToDouble(a.Weight, new CultureInfo("es-ES")).ToString("N1") : "-";
                
                ////3 DECIMALS
                //this.volumeLbl.Text = !String.IsNullOrEmpty(a.Volume) ? Convert.ToDouble(a.Volume, new CultureInfo("es-ES")).ToString("N3") : "-";
                
           

                CultureInfo ci = CultureInfo.CurrentCulture;
                ci = CultureInfo.CurrentUICulture;
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (CurrentUser.VerCantidades == false)
                {
                    this.PanelStockConCantidades.Visible = false;
                    this.PanelStockSinCantidades.Visible = true;
                    loadStock(a.Stock, a.DescripcionGris);

                    if (a.NiveldeServicio == 0)
                    {
                        //this.ImgNivellClient.Style.Add("display", "none");
                        this.ImgNivellClient.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";

                    }
                    else if (a.NiveldeServicio == 1)
                    {
                        //this.ImgNivellClient.Style.Add("display", "block");
                        this.ImgNivellClient.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.LblNivellToolTipClient.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio1"), a.NiveldeservicioDescripcion);
                    }
                    else
                    {
                        //this.ImgNivellClient.Style.Add("display", "block");
                        this.ImgNivellClient.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.LblNivellToolTipClient.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio2"), a.NiveldeservicioDescripcion);
                    }

                }
                else
                {
                    this.PanelStockConCantidades.Visible = true;
                    this.PanelStockSinCantidades.Visible = false;
                    loadStockConCantidades(a.CantidadRojo, a.CantidadAzul, a.CantidadVerde, a.CantidadGris, a.DescripcionGris, a.Stock, a.NiveldeServicio, a.NiveldeservicioDescripcion);
                   
                }

               
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            
        }
        /*private void loadComboMeasuersValue(Article a)
        {
            this.ComboMeasures.EmptyMessage = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ComboMeasuresEmptyMessage");
            List<string> ListMeasures = Products.Where(p => p.PatronAgrup == a.PatronAgrup).Select(p => p.CodMedida).ToList();
            this.ComboMeasures.Items.Clear();
            foreach (string artMed in ListMeasures)
            {
                if (artMed != a.CodMedida)
                {
                    this.ComboMeasures.Items.Add(new Telerik.Web.UI.RadComboBoxItem(artMed, a.PatronAgrup + ";" + artMed));
                }
            }
        }*/
  
        private DataTable ColorItems(Article a)
        {


            DataTable table = new DataTable();
            table.Columns.Add("Code");
            table.Columns.Add("ImageUrl");
            table.Columns.Add("Description");
            table.Columns.Add("Url");
            table.Columns.Add("ToolTipText");
            string[] ArtCol = a.DetalleAgrup.Split(';');
            for (int i = 0; i < ArtCol.Length; i++)
            {
                Article newProduct = Products.Where(p => p.PatronAgrup == a.PatronAgrup && p.CodColor == ArtCol[i]).SingleOrDefault() as Article;
                DataRow row = table.NewRow();
                row["Code"] = a.Code;
                row["ImageUrl"] = "/Style Library/Color Images/" + ArtCol[i] + ".jpg";
                row["Description"] = ArtCol[i];
                row["Url"] = "/Pages/productInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion;
                row["ToolTipText"] = JuliaGrupUtils.Utils.LanguageManager.GetColorToolTip(ArtCol[i]);
                table.Rows.Add(row);
            }








            return table;
        }

        public void RagridDisponiblitat_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {

            //this.RadGrid1.DataBind();

        }
        private DataTable LoadArticlesDisponibles(Article a)
        {
            DataTable table = new DataTable();

            table.Columns.Add("Id");
            table.Columns.Add("SmallImage");
            table.Columns.Add("ArticleCode");
            table.Columns.Add("Description");
            table.Columns.Add("Color");
            table.Columns.Add("Measures");
            table.Columns.Add("Price");

            table.Columns.Add("Stock");
            table.Columns.Add("StockTooltip");
            table.Columns.Add("StockTooltipRojo");
            table.Columns.Add("StockTooltipVerde");
            table.Columns.Add("StockTooltipAzul");
            table.Columns.Add("VisibleStockControlRepresentante");
            table.Columns.Add("VisibleStockControlClient");
            table.Columns.Add("CantidadRoja");
            table.Columns.Add("VisibilidadCantidadRoja");
            table.Columns.Add("CantidadAzul");
            table.Columns.Add("VisibilidadCantidadAzul");
            table.Columns.Add("CantidadVerde");
            table.Columns.Add("VisibilidadCantidadVerde");

            table.Columns.Add("CantidadGris");
            table.Columns.Add("VisibilidadCantidadGris");
            table.Columns.Add("StockTooltipGris");

            table.Columns.Add("StockTooltipNegro");
            table.Columns.Add("VisibleStockNull");
            table.Columns.Add("VisibleStockNoNull");
            table.Columns.Add("ImgNivelServicio");
            table.Columns.Add("ToolTipNivelServicio");
            table.Columns.Add("NivelDeServicioVIsible");
            table.Columns.Add("ArticleUrl");
            table.Columns.Add("CatalogNo");
            table.Columns.Add("EANCode");

            //Query productos en agrupacion
            List<Article> ProductList = new List<Article>();
            List<Article> GroupedProducts = new List<Article>();
            if (!string.IsNullOrEmpty(a.PatronAgrup)) {
                GroupedProducts = Products.Where(c => c.PatronAgrup == a.PatronAgrup && c.CatalogNo == a.CatalogNo).ToList();
            }
            if (GroupedProducts.Count > 0)
            {
                ProductList = GroupedProducts;
            }
            else
            {
                //Si no está agrupado Query productos del programa del catálogo por defecto
                ProductList = Products.Where(c => c.Programa == a.Programa && c.CatalogPorDefecto == "true").ToList();
            }
            foreach (Article art in ProductList)

            {
                DataRow row = table.NewRow();

                row["CatalogNo"] = art.CatalogNo;
                row["EANCode"] = art.EANcode;

                row["SmallImage"] = "<a href='/Pages/productInfo.aspx?code=" + art.Code + "&catalog=" + art.InsertcatalogNo + "&version=" + art.ArticleVersion + "'><img src=" + art.SmallImgUrl + " alt='' width='50px' height='50px' /></a>";
                row["ArticleUrl"] = "/Pages/productInfo.aspx?code=" + art.Code + "&catalog=" + art.InsertcatalogNo + "&version=" + art.ArticleVersion;
                row["ArticleCode"] = art.Code;
                row["Description"] = art.Description;
                row["Color"] = art.Color;
                row["Measures"] = art.CodMedida;

                int iPriceSel = (int)Session["PriceSelector"];
                CultureInfo culture = new CultureInfo("es-Es", false);
                if (iPriceSel == 0)
                {
                    row["Price"] = art.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                }
                else if (iPriceSel == 1)
                {
                    row["Price"] = art.Price.ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                }
                else if (iPriceSel == 2)
                {
                    row["Price"] = ((Convert.ToDouble(art.PriceInPoints, culture)) * (Convert.ToDouble(CurrentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                }
                else if (iPriceSel == 3)
                {
                    row["Price"] = (
                                        (Convert.ToDouble(art.Price, culture)) * 
                                        (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                    ).ToString("N2", culture) + " " + CurrentClient.LiteralDivisaWEB;
                }
                else if (iPriceSel == 4)
                {
                    row["Price"] = (
                                        (Convert.ToDouble(art.PriceInPoints, culture)) *
                                        (Convert.ToDouble(CurrentUser.Coeficient, culture)) *
                                        (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                   ).ToString("N2", culture)
                                   + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo");
                }
             
                 
                if (CurrentUser.VerCantidades)
                {
                    row["VisibleStockControlRepresentante"] = "true";
                    row["VisibleStockControlClient"] = "false";
                    row["VisibilidadCantidadRoja"] = art.CantidadRojo == 0 ? "false" : "true";
                    row["CantidadRoja"] = art.CantidadRojo.ToString("N0", new CultureInfo("es-Es", false));
                    row["VisibilidadCantidadAzul"] = art.CantidadAzul == 0 ? "false" : "true";
                    row["CantidadAzul"] = art.CantidadAzul.ToString("N0", new CultureInfo("es-Es", false));
                    row["VisibilidadCantidadVerde"] = art.CantidadVerde == 0 ? "false" : "true";
                    row["CantidadVerde"] = art.CantidadVerde.ToString("N0", new CultureInfo("es-Es", false));

                    row["VisibilidadCantidadGris"] = art.CantidadGris == 0 ? "false" : "true";
                    row["CantidadGris"] = art.CantidadGris.ToString("N0", new CultureInfo("es-Es", false));

                    //REZ 08042015 - En colección completa siempre muestra el stock como anulado si el producto principal está anulado
                    //if (a.Stock == 3) 
                    if (art.Stock == 3)
                    {
                        row["VisibleStockNull"] = "true";
                        row["VisibleStockNoNull"] = "false";
                    }
                    else
                    {
                        row["VisibleStockNull"] = "false";
                        row["VisibleStockNoNull"] = "true";
                    }

                    row["StockTooltipRojo"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                    row["StockTooltipVerde"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                    row["StockTooltipAzul"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                    row["StockTooltipNegro"] = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                    row["StockTooltipGris"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), art.DescripcionGris);
                    


                    row["StockTooltip"] = "";


                    
                    
                }
                else
                {
                    row["VisibilidadCantidadRoja"] = "false";
                    row["VisibilidadCantidadAzul"] = "false";
                    row["VisibilidadCantidadVerde"] = "false";
                    row["VisibilidadCantidadGris"] = "false";

                    row["VisibleStockControlRepresentante"] = "false";
                    row["VisibleStockControlClient"] = "true";
                    row["Stock"] = setStock(art.Stock);
                    row["StockTooltip"] = setStockTooltip(art.Stock, art.DescripcionGris);
                    row["StockTooltipRojo"] = "";
                    row["StockTooltipVerde"] = "";
                    row["StockTooltipAzul"] = "";
                    row["StockTooltipNegro"] = "";
                    row["StockTooltipGris"] = "";
                    row["VisibleStockNull"] = "false";
                    row["VisibleStockNoNull"] = "false";
                    
                    
                }
                //if (a.NiveldeServicio == 0)
                if (art.NiveldeServicio == 0)
                {
                    row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                    row["ToolTipNivelServicio"] = "";
                    row["NivelDeServicioVIsible"] = "false";
                }
                //else if (a.NiveldeServicio == 1)
                else if (art.NiveldeServicio == 1)
                {
                    row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                    row["ToolTipNivelServicio"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio1"), art.NiveldeservicioDescripcion);
                    row["NivelDeServicioVIsible"] = "true";
                }
                else
                {
                    row["ImgNivelServicio"] = "/Style Library/Julia/img/NivelServicio.png";
                    row["ToolTipNivelServicio"] = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio2"), art.NiveldeservicioDescripcion);
                    row["NivelDeServicioVIsible"] = "true";
                }
                   
             
                table.Rows.Add(row);
            }
            return table;

        }

        private String setStock(decimal p)
        {
            string imgURL = string.Empty;

            int Stock = 0;
            Stock = Convert.ToInt32(p);
            switch (Stock)
            {
                case 0:
                    imgURL = "/Style%20Library/Julia/img/Red_Stock.png";
                    break;
                case 1:
                    imgURL = "/Style%20Library/Julia/img/Orange_Stock.png";
                    break;
                case 2:
                    imgURL = "/Style%20Library/Julia/img/Green_Stock.png";
                    break;
                case 3:
                    imgURL = "/Style%20Library/Julia/img/Black_Stock.png";
                    break;
                case 4:
                    imgURL = "/Style%20Library/Julia/img/Gray_Stock.png";
                    break;
            }
            return (imgURL);
        }
        private String setStockTooltip(decimal p, string GrisDesc)
        {
            string output = string.Empty;

            int Stock = 0;
            Stock = Convert.ToInt32(p);
            switch (Stock)
            {
                case 0:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                    break;
                case 1:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                    break;
                case 2:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                    break;
                case 3:
                    output = LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                    break;
                case 4:
                    output = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), GrisDesc);
                    break;
            }
            return (output);
        }

        

        private void SetTableMeasures(string measures)
        {
            try
            {
                if (!string.IsNullOrEmpty(measures))
                {
                    //“Alto1” + “;” + “Ancho1” + “;” + “Fondo1” + “;” + “Alto2” + “;” + “Ancho2” + “:” + “Fondo2”

                    Table table = new Table();
                    table.Style.Add("text-align", "center");
                    //Add headers
                    TableRow row = new TableRow();
                    TableHeaderCell headerCell = new TableHeaderCell();
                    headerCell.Text = "";
                    row.Cells.Add(headerCell);
                    headerCell = new TableHeaderCell();
                    headerCell.Text = "<img height='25px' src='/style library/julia/img/Height.png' />";
                    row.Cells.Add(headerCell);
                    headerCell = new TableHeaderCell();
                    headerCell.Text = "<img height='25px' src='/style library/julia/img/Width.png' />";
                    row.Cells.Add(headerCell);
                    headerCell = new TableHeaderCell();
                    headerCell.Text = "<img height='25px' src='/style library/julia/img/Depth.png' />";
                    row.Cells.Add(headerCell);
                    table.Rows.Add(row);
                    string[] Measures = measures.Split('#');

                    //Add the Column values
                    for (int i = 0; i < Measures.Length; i++)
                    {
                        string[] values = Measures[i].Split(';');
                        row = new TableRow();
                        TableCell cellBulto = new TableCell();
                        cellBulto.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("bulto") + " " + (i + 1) + ":";
                        row.Cells.Add(cellBulto);
                        for (int j = 0; j < values.Length; j++)
                        {
                            TableCell cell = new TableCell();
                            cell.Text = values[j]+ " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Cm");
                            row.Cells.Add(cell);
                        }
                        table.Rows.Add(row);
                    }
                    // Add the the Table in the Form
                    PanelMeasures.Controls.Add(table);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void loadStockConCantidades(Decimal Rojo, Decimal Azul, Decimal Verde, Decimal Gris, string GrisDesc, Decimal stock, Decimal NiveldeServicio, string NivelServicioDesc)
        {
            try
            {
                if (stock != 3)
                {
                    this.StockNoNull.Visible = true;
                    this.StockNull.Visible = false;

                    this.LblCantidadVerde.Visible = Verde == 0 ? false : true;
                    this.LblCantidadVerde.Text = Verde.ToString("N0", new CultureInfo("es-Es", false));
                    this.stockVerdeImg.Visible = Verde == 0 ? false : true;

                    this.LblCantidadRoja.Visible = Rojo == 0 ? false : true;
                    this.LblCantidadRoja.Text = Rojo.ToString("N0", new CultureInfo("es-Es", false));
                    this.stockRojoImg.Visible = Rojo == 0 ? false : true;

                    this.LblCantidadAzul.Visible = Azul == 0 ? false : true;
                    this.LblCantidadAzul.Text = Azul.ToString("N0", new CultureInfo("es-Es", false));
                    this.stockAzulImg.Visible = Azul == 0 ? false : true;

                    //REZ 20032014 - Stock Gris
                    this.LblCantidadGris.Visible = Gris == 0 ? false : true;
                    this.LblCantidadGris.Text = Gris.ToString("N0", new CultureInfo("es-Es", false));
                    this.stockGrisImg.Visible = Gris == 0 ? false : true;
               
                    
                    this.LblRadToolTipRepAzul.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                    this.LblRadToolTipRepRojo.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                    this.LblRadToolTipRepVerde.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                    this.LblRadToolTipRepGris.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), GrisDesc);

                    if (NiveldeServicio == 0)
                    {
                        this.ImgNivellRepreStockNoNull.Style.Add("display", "none");
                        this.ImgNivellRepreStockNoNull.Visible = false;
                        this.ImgNivellRepreStockNoNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";

                    }
                    else if (NiveldeServicio == 1)
                    {
                        this.ImgNivellRepreStockNoNull.Style.Add("display", "block");
                        this.ImgNivellRepreStockNoNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.ImgNivellRepreStockNoNull.Visible = true;
                        this.LblToolTipImgNivellRepreNoNull.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio1"),NivelServicioDesc);
                    }
                    else
                    {
                        this.ImgNivellRepreStockNoNull.Style.Add("display", "block");
                        this.ImgNivellRepreStockNoNull.Visible = true;
                        this.ImgNivellRepreStockNoNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.LblToolTipImgNivellRepreNoNull.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio2"), NivelServicioDesc);
                    }
                }
                else
                {
                    this.StockNoNull.Visible = false;
                    this.StockNull.Visible = true;
                    this.LblRadToolTipRepNegro.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                    if (NiveldeServicio == 0)
                    {
                        this.ImgNivellRepreStockNull.Visible = false;//.Style.Add("display", "none");
                        this.ImgNivellRepreStockNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";

                    }
                    else if (NiveldeServicio == 1)
                    {
                        this.ImgNivellRepreStockNull.Style.Add("display", "block");
                        this.ImgNivellRepreStockNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.LblToolTipNivellRepreNull.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio1"), NivelServicioDesc);
                    }
                    else
                    {
                        this.ImgNivellRepreStockNull.Style.Add("display", "block");
                        this.ImgNivellRepreStockNull.ImageUrl = "/Style%20Library/Julia/img/NivelServicio.png";
                        this.LblToolTipNivellRepreNull.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("NivelDeServicio2"), NivelServicioDesc);
                    }
                }



            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
           
        }

        private void loadStock(decimal p, string GrisDesc)
        {
            try
            {
                int Stock = 0;
                Stock = Convert.ToInt32(p);
                switch (Stock)
                {
                    case 0:
                        this.ImgStock.ImageUrl = "/Style%20Library/Julia/img/Red_Stock.png";
                        this.LblStock.Text = LanguageManager.GetLocalizedString("ProductStockTooltip_Rojo");
                        break;
                    case 1:
                        this.ImgStock.ImageUrl = "/Style%20Library/Julia/img/Orange_Stock.png";
                        this.LblStock.Text = LanguageManager.GetLocalizedString("ProductStockTooltip_Naranja");
                        break;
                    case 2:
                        this.ImgStock.ImageUrl = "/Style%20Library/Julia/img/Green_Stock.png";
                        this.LblStock.Text = LanguageManager.GetLocalizedString("ProductStockTooltip_Verde");
                        break;
                    case 3:
                        this.ImgStock.ImageUrl = "/Style%20Library/Julia/img/Black_Stock.png";
                        this.LblStock.Text = LanguageManager.GetLocalizedString("ProductStockTooltip_Negro");
                        break;
                    //REZ 20032014 - Stock Gris
                    case 4:
                        this.ImgStock.ImageUrl = "/Style%20Library/Julia/img/Gray_Stock.png";
                        this.LblStock.Text = String.Format(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductStockTooltip_Gris"), GrisDesc);
                        break;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        protected void RagridDisponiblitat_ItemCommand(object source, GridCommandEventArgs e)
        {

            if (e.CommandName == "RowClick")
            {
                GridDataItem item = (GridDataItem)e.Item;
                string code = item["ArticleCode"].Text;
                
                //REZ 26112013 - Peta perque pot ser que en trovi mes d'un a diferents catalegs o versio
                //Article art = (Article)Products.Where(p => p.Code == code).SingleOrDefault();
                //Response.Redirect("/Pages/productInfo.aspx?code=" + art.Code + "&catalog=" + art.InsertcatalogNo + "&version=" + art.ArticleVersion);

                string ArticleUrl = item["ArticleUrl"].Text;
                Response.Redirect(ArticleUrl);
                
            }
        }   
      
     

        public void AddTOrder(object sender, EventArgs e)
        {
            try
            {
                int qty = Int32.Parse(this.itemQty.Text);

                string linedescription = this.ValueDescriptionArea.Text;
                if (Session["Order"] != null)
                {
                    currentOrder = (Order)Session["Order"];
                }
                this.currentOrder.AddProduct(this.currentArticle, qty, linedescription, currentOrder.OrderClient.UserName);
                //refresquem la order que tenim a la sessio
                Session["Order"] = this.currentOrder;

                //RefreshCarrito(); //No hace falta porque hace postback la pagina

                //REZ04042016 - Quitamos los popUps, debe mostrarse el aumento de número en el sticky - Deseable compra en cliente (sin postback)
                //this.RadNotification1.Show(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct"));

            }
            catch (NavException ex)
            {
                string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void RefreshCarrito()
        {
            string RefreshTableScript = "<script language='javascript'>RefreshTable();</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "RefreshTableScript", RefreshTableScript);
        }

        /*protected void ComboMeasures_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                string[] dataNewArt = ComboMeasures.SelectedValue.Split(';');
                Article newProduct = Products.Where(p => p.PatronAgrup == dataNewArt[0] && p.CodMedida == dataNewArt[1]).SingleOrDefault() as Article;
                if (newProduct != null)
                {
                    string Url = "/Pages/ProductInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion; ;
                    Response.Redirect(Url, true);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }*/

        private string setImgDiscountURL(Article a)
        {
            //Image Imagen = (Image)this.RadListView1.Page.FindControl("ImgDiscount");

            string output = string.Empty;


            if (a.Discount.ToString() == "0,00")
            {
                output = "/Style%20Library/Julia/img/null_pixel.png";
                //Imagen.Visible = false;

            }
            else
            {
                if (a.TieneDtoExclusivo)
                {
                    output = "/Style%20Library/Julia/img/estrella_xata_exclusive.png";
                }
                else
                {
                    //output = "/Style%20Library/Julia/img/estrella_xata.png";
                    //REZ 13052015 - Producte en liquidació
                    //if (a.CatalogNo.StartsWith("PRELIQ"))
                    if (a.TipusIcona == 2)
                    {
                        output = "/Style%20Library/Julia/img/estrella_xata.png";
                    }
                    else
                    {
                        output = "/Style%20Library/Julia/img/estrella_xata_promocion.png";
                    }
                }
                //Imagen.Visible = true;
            }

            return output;
        }

    }
}
