﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using System.Linq;

namespace JuliaGrupPortalClientes_v2.Web_Parts.RedirectToLaForma
{
    public partial class RedirectToLaFormaUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = string.Empty;
            string RedirMode = Request.QueryString["r"];
            string artId = Request.QueryString["artId"];
            if ((!String.IsNullOrEmpty(RedirMode)) && (RedirMode.Trim().ToUpper() == "PRODUCTS"))
            {
                //Venimos del menú superior o de los componentes de catalogos (Promociones, catalogos, liquidaciones)
                ClearFilters();
                SetFilters();
                string Catalog = Request.QueryString["catalog"];
                string cat = (!String.IsNullOrEmpty(Catalog)) ? "?catalog=" + Catalog.ToUpper() : String.Empty;
                url = "/Pages/Products.aspx" + cat;
                Response.Redirect(url);
            }
            else if ((!String.IsNullOrEmpty(RedirMode)) && (RedirMode.Trim().ToUpper() == "BACK"))
            {
                //Volvemos de ProductInfo o de Pedido Advanced. No borramos filtros y añadimos el parametro del catálogo
                string cat = (Session["fCatalog"] != null) ? "?catalog=" + Session["fCatalog"].ToString() : String.Empty;
                url = "/Pages/Products.aspx" + cat;
                string urlConn = (url.Contains("?")) ? "&" : "?";
                url = (!String.IsNullOrEmpty(artId)) ? url + urlConn + "artId=" + artId : url;
                Response.Redirect(url);
            }
            else
            {
                //Redirección de color
                string Colorcode = Request.QueryString["Colorcode"]; 
                string defaultCatalog = "true";
                string patronagrup = Request.QueryString["PatronAgrup"];
                ArticleDataAccessObject articledao = (ArticleDataAccessObject)Session["ArticleDAO"];
                List<Article> Products = articledao.GetAllProducts();

                //ALB - 02042019 - Filtre per catàleg per defecte per evitar duplicats
                //Article newProduct = Products.Where(p => p.PatronAgrup == patronagrup && p.CodColor == Colorcode).SingleOrDefault() as Article;
                Article newProduct = Products.Where(p => p.PatronAgrup == patronagrup && p.CodColor == Colorcode && p.CatalogPorDefecto == defaultCatalog).SingleOrDefault() as Article;

                if (newProduct != null)
                {

                    url = "/Pages/ProductInfo.aspx?code=" + newProduct.Code + "&catalog=" + newProduct.InsertcatalogNo + "&version=" + newProduct.ArticleVersion;
                    Response.Redirect(url);
                }
                else
                {
                    this.PanelRedirectError.Visible = true;
                    this.ErrorLbl.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ErroLblArtNotFound");
                }
            }

        }
        private void ClearFilters()
        {
            Session.Remove("fSearchTxt");
            Session.Remove("fFamily");
            Session.Remove("fSubFamily");
            Session.Remove("fProgram");
            Session.Remove("fSearchTxt");
            Session.Remove("fPageNo");
            Session.Remove("fCatalog");
            Session.Remove("prodOrder");
            Session.Remove("DtoExclusivo");
            //REZ22032016 - Nueva categoria
            Session.Remove("fCategory");
            Session.Remove("fSubCategory");
            Session.Remove("fEstance");
            Session.Remove("fStyle");
            Session.Remove("fColor");

            Session.Remove("fPrice");
            Session.Remove("fMinPrice");
            Session.Remove("fMaxPrice");

        }

        private void SetFilters()
        {

            string f = Request.QueryString["f"];
            if (!String.IsNullOrEmpty(f))
                Session.Add("fFamily", f);

            string sf = Request.QueryString["sf"];
            if (!String.IsNullOrEmpty(sf))
                Session.Add("fSubFamily", sf);

            string p = Request.QueryString["p"];
            if (!String.IsNullOrEmpty(p))
                Session.Add("fProgram", p);

            string t = Request.QueryString["t"];
            if (!String.IsNullOrEmpty(t))
                Session.Add("fSearchTxt", t);

            //REZ 30122014 - Pasamos filtro de DtoExclusivo
            string d = Request.QueryString["d"];
            if (!String.IsNullOrEmpty(d))
                Session.Add("DtoExclusivo", d);
            //Añadir c (Categoria), sc (Subcategoria), e (Estancia), es (Estilo)
            string c = Request.QueryString["c"];
            string sc = Request.QueryString["sc"];
            if (!String.IsNullOrEmpty(c))
            {
                string category = (!String.IsNullOrEmpty(sc)) ? sc : c;
                Session.Add("fCategory", category);
            }
            string e = Request.QueryString["e"];
            if (!String.IsNullOrEmpty(e))
                Session.Add("fEstance", e);
            string es = Request.QueryString["es"];
            if (!String.IsNullOrEmpty(es))
                Session.Add("fStyle", es);

            string pr = Request.QueryString["pr"];
            if (!String.IsNullOrEmpty(pr))
            {
                Session.Add("fPrice", pr);
                Session.Add("fMinPrice", pr.Split('_')[0]);
                Session.Add("fMaxPrice", pr.Split('_')[1]);
            }
            string cl = Request.QueryString["cl"];
            if (!String.IsNullOrEmpty(cl))
                Session.Add("fColor", cl);
        }

    }
}
