﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace JuliaGrupPortalClientes_v2.Web_Parts.Slider
{
    [ToolboxItemAttribute(false)]
    public class Slider : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2.Web_Parts/Slider/SliderUserControl.ascx";

        public enum Tipo { Imagen, DescripcionCompleta }
        public enum Dato { ProductosCatalogo, Promociones, ProductosDestacados,Inspirate}

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Título del slider")]
        [System.ComponentModel.Category("Custom")]
        public string Titulo { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Visualización de Elementos")]
        [System.ComponentModel.Category("Custom")]
        public Tipo TipoElemento { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Tipo de datos")]
        [System.ComponentModel.Category("Custom")]
        public Dato Datos { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Lista de Promociones")]
        [System.ComponentModel.Category("Custom")]
        public string PromotionList { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Página que enlaza cada elemento")]
        [System.ComponentModel.Category("Custom")]
        public string PageLink { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Ancho de las imágenes")]
        [WebDescription("Para promoción: 258, para catalogo: 99, para producto: 145")]
        [System.ComponentModel.Category("Custom")]
        public string IMWidth { get; set; }

        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Alto de las imágenes")]
        [WebDescription("Para promoción: 130, para catalogo: 140, para producto: 120")]
        [System.ComponentModel.Category("Custom")]
        public string IMHeight { get; set; }



        [System.Web.UI.WebControls.WebParts.WebBrowsable(true)]
        [System.Web.UI.WebControls.WebParts.Personalizable(
        System.Web.UI.WebControls.WebParts.PersonalizationScope.Shared)]
        [WebDisplayName("Número máximo elementos")]
        [WebDescription("Vacio: sin Límite - Recomendado: 30")]
        [System.ComponentModel.Category("Custom")]
        public int MaxElems { get; set; }


        protected override void OnInit(EventArgs e)
        {
            //this.EnsureChildControls();   ////no puede ir aqui porque entonces carga todos los controles antes de calcular el usuario
            base.OnInit(e);
        }
        protected override void CreateChildControls()
        {
                SliderUserControl slidercontrol = Page.LoadControl(_ascxPath) as SliderUserControl;
                slidercontrol.SliderID = this.ID.Replace("$", "");
                slidercontrol.Titulo = this.Titulo;
                slidercontrol.PromotionList = this.PromotionList;
                slidercontrol.Datos = this.Datos;
                slidercontrol.Tipo = this.TipoElemento;
                slidercontrol.ElemLink = this.PageLink;
                if (this.Width != null && this.Height != null)
                {
                    slidercontrol.Width = this.IMWidth;
                    slidercontrol.Height = this.IMHeight;
                }
                slidercontrol.MaxElems = this.MaxElems;

                Controls.Add(slidercontrol);
        }
    }
}
