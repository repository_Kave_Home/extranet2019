﻿<%@ Assembly Name="JuliaGrupPortalClientes_v2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=99a5b16b7c5d690b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SliderUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.Slider.SliderUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<script src="/Style%20Library/Julia/js/jquery.carouFredSel-5.6.1-packed.js" type="text/javascript"></script>
<style type="text/css">
     .sliderCarousel
    {   
            visibility:hidden;
      
        }
    #<%= this.SliderID %> {
        /*width: 450px;
        height: 220px;*/
        overflow: hidden;
    }
   

</style>


 
<script type="text/javascript" language="javascript">

  

    function initCarousel<%= this.SliderID %>() {
        jQuery("#<%= this.SliderID %>").carouFredSel({
            circular: true,
            auto: false,
            align: "center",
            scroll: {
                items: 1
            },
            prev: {
                button: "#<%= this.SliderID %>_prev",
                key: "left"
            },
            next: {
                button: "#<%= this.SliderID %>_next",
                key: "right"
            },
            cookie: "<%= this.SliderID %>_pos"
        });
         $('.sliderCarousel').css("visibility", "visible");
    }
    jQuery(document).ready(function () {
        /*SP.SOD.executeOrDelayUntilScriptLoaded(initCarousel<%= this.SliderID %>, "sp.js");*/
        initCarousel<%= this.SliderID %>();
    });

    function OpenProductInfoDialog2(code, version, catalog) {
        var options = {
            url: "/Pages/ProductInfo.aspx?code=" + code + "&version=" + version + "&catalog=" + catalog,
            width:900,
            height:600,
            title: '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ProductInfo") %>',
            dialogReturnValueCallback: DialogCallback2
        };

        SP.UI.ModalDialog.showModalDialog(options);
    }

    function DialogCallback2(dialogResult, returnValue) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            RefreshTable();
              var notification = $find("<%= RadNotification2.ClientID %>");
            var message =  '<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddProduct")%>';           
            notification.set_text(message);
            notification.show();
        }
    }

 
</script>

<div class="headerText">
<%# DataBinder.Eval(this, "titulo") %>
</div>
<div class="sliderCarousel">
    <div id="<%= this.SliderID %>">
        <asp:PlaceHolder ID="SlideWrapper" runat="server">
        </asp:PlaceHolder>
    </div>
    <div class="clearfix"></div>
    <a class="prev" id="<%= this.SliderID %>_prev" href="#"></a>
    <a class="next" id="<%= this.SliderID %>_next" href="#"></a>
</div>
<telerik:RadNotification ID="RadNotification2" runat="server" Title="Carrito"  EnableRoundedCorners="true" AutoCloseDelay="3000" OffsetY="-100" 
                                    EnableShadow="true"  Position="MiddleLeft"  ShowCloseButton="false" ShowTitleMenu="false" ContentIcon="" Width="200px" Height="50px"  
                                  Skin="Silk" Text="Producto Añadido" ></telerik:RadNotification>