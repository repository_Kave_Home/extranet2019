﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using System.Collections.Generic;
using Microsoft.SharePoint;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using System.Threading;
using JuliaGrupUtils.ErrorHandler;
using System.Globalization;
using System.Linq;

namespace JuliaGrupPortalClientes_v2.Web_Parts.Slider
{
    public partial class SliderUserControl : UserControl
    {
        #region Private_Properties

        private string sliderID = "slider";
        private string titulo = "titulo";
        private string promotionList;
        private Slider.Dato datos;
        private Slider.Tipo tipo;
        private ArticleDataAccessObject articleDAO;
        private const string _ImageUCascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/ImageSlideUserControl.ascx";
        private const string _ImageDescUCascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/ImageDescSlideUserControl.ascx";
        private enum TipoCatalogo { LiquidacionLaForma = 2, LiquidacionDorSuit = 3, Promociones = 1, General = 4 }
        private string elemLink = string.Empty;

        private string width;
        private string height;
        private int maxElems;
        User CurrentUser;
        Client CurrentClient;
        #endregion

        #region Public_Properties

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }
        public string PromotionList
        {
            get { return promotionList; }
            set { promotionList = value; }
        }
        public Slider.Dato Datos
        {
            get { return datos; }
            set { datos = value; }
        }
        public Slider.Tipo Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        public string SliderID
        {
            get { return sliderID; }
            set { sliderID = value; }
        }
        public string ElemLink
        {
            get { return elemLink; }
            set { elemLink = value; }
        }
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        public string Height
        {
            get { return height; }
            set { height = value; }
        }

        public int MaxElems
        {
            get { return maxElems; }
            set { maxElems = value; }
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            if (this.width == null)
            {
                this.width = "100";
                this.height = "100";
            }
            base.OnInit(e);
        }

        protected override void CreateChildControls()
        {
            //Lo cargamos siempre porque sino no funciona PrintView de Products
            //if (!Page.IsPostBack)
            //{
                try
                {
                    switch (datos)
                    {
                        case Slider.Dato.Promociones:
                            titulo = LanguageManager.GetLocalizedString("Promociones");
                            break;
                        case Slider.Dato.ProductosCatalogo:
                            titulo = LanguageManager.GetLocalizedString("ProductosCatalogo");
                            break;
                        case Slider.Dato.ProductosDestacados:
                            titulo = LanguageManager.GetLocalizedString("Destacados");
                            break;
                        case Slider.Dato.Inspirate:
                            titulo = LanguageManager.GetLocalizedString("TitleInspirate");
                            break;
                    }
                    if (!(string.IsNullOrEmpty(promotionList) && datos != Slider.Dato.ProductosDestacados) || datos == Slider.Dato.Inspirate)
                    {
                        if (Session["ArticleDAO"] != null)
                        {
                            this.articleDAO = (ArticleDataAccessObject)Session["ArticleDAO"];
                        }
                        else
                        {
                            string currentUser;

                            if (Session["User"] != null)
                                currentUser = (Session["User"] as User).UserName;
                            else
                                return;

                            CurrentClient = (Client)Session["Client"];
                            if (CurrentClient == null)
                                return;
                            this.articleDAO = new ArticleDataAccessObject(currentUser, CurrentClient.Code, 0);
                            Session.Add("ArticleDAO", this.articleDAO);
                        }
                        switch (datos)
                        {
                            case Slider.Dato.Promociones:
                                SetCatalogo(this.articleDAO.GetCatalogsCode("Promociones"));
                                break;
                            case Slider.Dato.ProductosCatalogo:
                                //SetCatalogo(this.articleDAO.GetCatalogsCode("General"));
                                SetCatalogo(this.articleDAO.GetCatalogsCode("General", "Liquidaciones Laforma", "Liquidaciones Dorsuit"));
                                break;
                            case Slider.Dato.ProductosDestacados:
                                SetProductosDestacados();
                                break;
                            case Slider.Dato.Inspirate:
                                setInspirates(this.articleDAO.GetCatalogsCode("Inspirate"));
                                break;
                        }
                    }
                }
                catch (NavException ex)
                {
                    string radalertscript = "<script language='javascript'>function f(){radalert('" + ex.Message + "', 330, 100,'NavError'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                    throw new SPException(new StackFrame(1).GetMethod().Name, ex);
                }
            //}
        }

    
        protected void Page_Load(object sender, EventArgs e)
        {
            //EnsureChildControls();
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.DataBind();
            base.OnPreRender(e);
            
        }

        private string GetQueryStringValue(string name)
        {
            return this.Request.QueryString[name];
        }
        private void setInspirates(List<string> Inspirates)
        {
            try
            {
                 string InspirateCode = GetQueryStringValue("Inspirate");
               
                //RPT:Random deshabilitado
                 int size = Inspirates.Count;
                if (size == 0)
                {
                    SetInspirateImageDefault();
                }
                else 
                { 
                    while (size > 0)
                    {
                        size--;

                        String code = Inspirates[size];
                        SetInspirateImage(InspirateCode, code);
                        
                    } 
                }
                
            }
            catch (Exception ex)
            {
                
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetCatalogo(List<string> catalogos)
        {
            try
            {
                string catalogcode = GetQueryStringValue("catalog");
               
                //RPT:Random deshabilitado
                int size = catalogos.Count;
                if (size == 0)
                {
                    SetCatalogImageDefault();
                }
                else 
                {
                    int pos = 0;
                    while (pos < size)
                    {
                        //size--;
                        String code = catalogos[pos];
                        SetCatalogoImage(catalogcode, code);
                        pos++;
                    } 
                }
                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        private void SetInspirateImageDefault() {
            SlideInfo slideInfo = new SlideInfo();
            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;

            slideInfo.width = this.width + "px";
            slideInfo.height = this.height + "px";
            ImageSlideUserControl imgControl = Page.LoadControl(_ImageUCascxPath) as ImageSlideUserControl;
            imgControl.Image_url = "/Style Library/Julia/img/" + strCultureName + @"/NohayInspirate.png";
            imgControl.Height = "175";
            imgControl.Width = "300";
            imgControl.Link = "#";

            //posar lynk
            this.SlideWrapper.Controls.Add(imgControl);
        }
        private void SetCatalogImageDefault()
        {
          
            SlideInfo slideInfo = new SlideInfo();
            string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
            
            slideInfo.width = this.width + "px";
            slideInfo.height = this.height + "px";
            ImageSlideUserControl imgControl = Page.LoadControl(_ImageUCascxPath) as ImageSlideUserControl;
            imgControl.Image_url = "/Style Library/Julia/img/" + strCultureName + @"/Nohaypromos.png";
            imgControl.Height = "175";
            imgControl.Width = "300";
            imgControl.Link = "#";
            
           
            //posar lynk
            this.SlideWrapper.Controls.Add(imgControl);
               
        }
        private void SetInspirateImage(String catalogcode, String code)
        {
            bool selecteditem = false;
            SlideInfo slideInfo = getCatalogInfo(this.promotionList, this.datos, code);

            //285x143 - 99x140
            slideInfo.width = this.width + "px";
            slideInfo.height = this.height + "px";

            if (!string.IsNullOrEmpty(catalogcode) && code == catalogcode)
                selecteditem = true;
            switch (this.tipo)
            {
                case Slider.Tipo.Imagen:
                    addImageSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height, selecteditem, "");
                    break;
                case Slider.Tipo.DescripcionCompleta:
                    addImageDescSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height,
                        slideInfo.code, slideInfo.description, slideInfo.price, slideInfo.priceInPoints, selecteditem);
                    break;
            }
        }
        private void SetCatalogoImage(String catalogcode, String code)
        {
               bool selecteditem = false;
                    SlideInfo slideInfo = getCatalogInfo(this.promotionList, this.datos, code);
             
                    //285x143 - 99x140
                    slideInfo.width = this.width + "px";
                    slideInfo.height = this.height + "px";

                    if (!string.IsNullOrEmpty(catalogcode) && code == catalogcode)
                        selecteditem = true;
                    switch (this.tipo)
                    {
                        case Slider.Tipo.Imagen:
                            addImageSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height, selecteditem, slideInfo.document_url);
                            break;
                        case Slider.Tipo.DescripcionCompleta:
                            addImageDescSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height,
                                slideInfo.code, slideInfo.description, slideInfo.price,slideInfo.priceInPoints, selecteditem);
                            break;
                    }
        }
        private void SetProductosDestacados()
        {
            try
            {
               

                List<Article> articles = this.articleDAO.GetProductosDestacados();
                //REZ 03032016 - Hay que limitar el número de elementos. Otra opción es crear GetProductosDestacados(int max) y hacer el take ahi
                if ((maxElems != null) && (maxElems > 0)) {
                    articles = articles.Take(maxElems).ToList();
                }
                int size = articles.Count -1;
                int i= size;
                List<int> Control_repeat = new List<int>();
                while (i > 0)
                {

                    int index = (new Random()).Next(0, size);

                    while ((Control_repeat.Contains(index)))
                    {
                        index = (new Random()).Next(0, size);
                    }
                    Control_repeat.Add(index);
                    Article art = articles[index];
                    SetSliderImage(art);
                    i--;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void SetSliderImage(Article article)
        {
            SlideInfo slideInfo = new SlideInfo();

            slideInfo.code = article.Code;
            slideInfo.description = article.Description;
            slideInfo.img_url = article.SmallImgUrl;
            //slideInfo.link = "/Pages/ProductInfo.aspx?code=" + article.Code + "&version=" + article.ArticleVersion;
            slideInfo.link = "/Pages/ProductInfo.aspx?code=" + article.Code + "&version=" + article.ArticleVersion + "&catalog=" + article.InsertcatalogNo;
            slideInfo.price = article.Price.ToString("N2");

            
            slideInfo.priceInPoints = article.PriceInPoints.ToString();
            slideInfo.width = this.width + "px";
            slideInfo.height = this.height + "px";
            switch (this.tipo)
            {
                case Slider.Tipo.Imagen://145x120
                    addImageSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height, false, "");
                    break;
                case Slider.Tipo.DescripcionCompleta:
                    addImageDescSlide(slideInfo.img_url, slideInfo.link, slideInfo.width, slideInfo.height,
                        slideInfo.code, slideInfo.description, slideInfo.price ,slideInfo.priceInPoints,false);
                    break;
            }
        }

      
        private string resizeImage(SlideInfo elem, int max_height)
        {
            try
            {
                if (elem == null)
                    return string.Empty;
                if (!string.IsNullOrEmpty(elem.width))
                {
                    int width = Convert.ToInt32(elem.width.Replace("px", ""));
                    int height = Convert.ToInt32(elem.height.Replace("px", ""));

                    int newwidth = (int)((float)width / (float)height * (float)max_height);
                    return newwidth.ToString() + "px";
                }
                return string.Empty;
            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private SlideInfo getCatalogInfo(string listName, Slider.Dato dato, string code)
        {
            SlideInfo slideInfo = new SlideInfo();
            try
            {
                SPListItemCollectionAdapter catalogs = getAllCatalogInfo(listName);
                //1.- Filtrar por codigo e idioma. Si no hay idioma mostrar el genérico
                //Where , code, getCurrentLanguage());

                //List<SPListItem> items = (from l in list.Items.OfType<SPListItem>() where l["KPI Status"] != null && Convert.ToInt32(l["KPI Status"]) == 1 select l).ToList<SPListItem>();
                //http://solutionizing.net/tag/splistitemcollection/

                List<SPListItem> filteredCatalogs = catalogs.Where(c => c["Code"] != null && c["Code"].ToString() == code && c["Language"] != null && c["Language"].ToString() == getCurrentLanguage() && c["ContentType"].ToString() == "JuliaGroup Catalogs").ToList<SPListItem>();
                if (filteredCatalogs.Count() < 1)
                {
                    filteredCatalogs = catalogs.Where(c => c["Code"] != null && c["Code"].ToString() == code && c["ContentType"].ToString() == "JuliaGroup Catalogs").ToList<SPListItem>();
                }
                //2.- 
                slideInfo.code = code;
                slideInfo.link = this.elemLink + "?catalog=" + code + "&r=products";


                //3.- Para cada resultado

                foreach (SPListItem item in filteredCatalogs)
                {
                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                        continue;

                    string cad = (item["Caducity"] != null)?item["Caducity"].ToString():String.Empty;
                    if (!string.IsNullOrEmpty(cad))
                    {
                        DateTime caducity = DateTime.Parse(cad);
                        if (DateTime.Now.CompareTo(caducity) <= 0)
                        {
                            if ((item["ImageType"] != null) && (item["ImageType"].ToString() == "On"))
                            {
                                slideInfo.img_url = "/" + item.Url;
                            }
                        }
                        else
                        {
                            if ((item["ImageType"] != null) &&(item["ImageType"].ToString() == "Off"))
                            {
                                slideInfo.img_url = "/" + item.Url;
                            }
                        }
                    }
                    else
                    {
                        slideInfo.img_url = "/" + item.Url;
                        break;
                    }
                }
                //Obtenemos el enlace al catalogo
                List<SPListItem> filteredCatalogsDocument = catalogs.Where(c => c["Code"] != null && c["Code"].ToString() == code && c["Language"] != null && c["Language"].ToString() == getCurrentLanguage() && c["ContentType"].ToString() != "JuliaGroup Catalogs").ToList<SPListItem>();
                if (filteredCatalogsDocument.Count() < 1)
                {
                    filteredCatalogsDocument = catalogs.Where(c => c["Code"] != null && c["Code"].ToString() == code && c["ContentType"].ToString() != "JuliaGroup Catalogs").ToList<SPListItem>();
                }
                foreach (SPListItem item in filteredCatalogsDocument)
                {
                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                        continue;

                    slideInfo.document_url = "/" + item.Url;
                    break;
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            return slideInfo;
        }

        private SlideInfo oldgetCatalogInfo(string listName, Slider.Dato dato, string code)
        {
            SlideInfo slideInfo = new SlideInfo();
            try
            {
                SPSite siteColl = SPContext.Current.Site;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                    {
                        using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                        {
                            SPList list = ElevatedSite.Lists[listName];

                            if (list != null)
                            {
                                SPQuery query = new SPQuery();
                                query.ViewFields = "<FieldRef Name=\"LinkFilename\"/><FieldRef Name=\"Code\"/><FieldRef Name=\"Caducity\"/><FieldRef Name=\"ImageType\"/><FieldRef Name=\"Language\"/>";
                                query.ViewAttributes = "Scope=\"Recursive\"";
                                query.Query = string.Format(
                                    "<Where><And><Eq><FieldRef Name=\"Code\"/><Value Type=\"Text\">{0}</Value></Eq><Eq><FieldRef Name=\"Language\"/><Value Type=\"Text\">{1}</Value></Eq></And></Where>"
                                    , code
                                    , getCurrentLanguage());

                                SPListItemCollection items = list.GetItems(query);
                                if (items == null || items.Count == 0)
                                {
                                    query = new SPQuery();
                                    query.ViewFields = "<FieldRef Name=\"LinkFilename\"/><FieldRef Name=\"Code\"/><FieldRef Name=\"Caducity\"/><FieldRef Name=\"ImageType\"/>";
                                    query.ViewAttributes = "Scope=\"Recursive\"";
                                    query.Query = string.Format(
                                        "<Where><Eq><FieldRef Name=\"Code\"/><Value Type=\"Text\">{0}</Value></Eq></Where>"
                                        , code);

                                    items = list.GetItems(query);
                                }
                                slideInfo.code = code;
                                slideInfo.link = this.elemLink + "?catalog=" + code + "&r=products";

                                foreach (SPListItem item in items)
                                {
                                    if (item.FileSystemObjectType != SPFileSystemObjectType.File)
                                        continue;

                                    string cad = (item.Properties["Caducity"] != null)?item.Properties["Caducity"].ToString():String.Empty;
                                    if (!string.IsNullOrEmpty(cad))
                                    {
                                        DateTime caducity = DateTime.Parse(cad);
                                        if (DateTime.Now.CompareTo(caducity) <= 0)
                                        {
                                            if ((item.Properties["ImageType"] != null) && (item.Properties["ImageType"].ToString() == "On"))
                                            {
                                                slideInfo.img_url = "/" + item.Url;
                                            }
                                        }
                                        else
                                        {
                                            if ((item.Properties["ImageType"] != null) &&(item.Properties["ImageType"].ToString() == "Off"))
                                            {
                                                slideInfo.img_url = "/" + item.Url;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        slideInfo.img_url = "/" + item.Url;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }

            return slideInfo;
        }

        //Funcion que obtiene todos los elementos de la lista de Catalogos de la Home y la añade a cache para evitar multiples llamadas
        private SPListItemCollectionAdapter getAllCatalogInfo(string listName)
        {
            SPListItemCollectionAdapter itemsAdapter = null;

            try
            {
                var AllCatalogInfo = Cache.Get("AllCatalogInfo");
                if (AllCatalogInfo != null)
                {
                    itemsAdapter = (SPListItemCollectionAdapter)AllCatalogInfo;
                }
                else
                {
                    SPSite siteColl = SPContext.Current.Site;

                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite ElevatedsiteColl = new SPSite(siteColl.ID))
                        {
                            using (SPWeb ElevatedSite = ElevatedsiteColl.RootWeb)
                            {
                                SPList list = ElevatedSite.Lists[listName];

                                if (list != null)
                                {
                                    SPQuery query = new SPQuery();
                                    query.ViewFields = "<FieldRef Name=\"LinkFilename\"/><FieldRef Name=\"Code\"/><FieldRef Name=\"Caducity\"/><FieldRef Name=\"ImageType\"/><FieldRef Name=\"Language\"/><FieldRef Name=\"ContentType\"/>";
                                    query.ViewAttributes = "Scope=\"Recursive\"";
                                    query.Query = string.Empty; //Get all items

                                    SPListItemCollection items = list.GetItems(query);
                                    itemsAdapter = new SPListItemCollectionAdapter(items);
                                }
                            }
                        }
                    });
                }
                Cache.Remove("AllCatalogInfo");
                Cache.Add("AllCatalogInfo", itemsAdapter, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return itemsAdapter;
        }

        private string getCurrentLanguage()
        { 
            string lang = string.Empty;

            try
            {
                string cur_lan = System.Threading.Thread.CurrentThread.CurrentUICulture.LCID.ToString();
               
                switch (cur_lan)
                {
                    case "3082":
                        lang = "Spanish";
                        break;
                    case "1040":
                        lang = "Italian";
                        break;
                    case "1036":
                        lang = "French";
                        break;
                    case "1027":
                        lang = "Catalan";
                        break;
                    case "1031":
                        lang = "German";
                        break;
                    default:
                        lang = "English";
                        break;
                }
               
            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return lang;
        }

        private void addImageSlide(string img_url, string link, string width, string height, bool selecteditem, string documentUrl)
        {
            try
            {
                ImageSlideUserControl imgControl = Page.LoadControl(_ImageUCascxPath) as ImageSlideUserControl;
                imgControl.Image_url = img_url;
                imgControl.Height = height;
                imgControl.Width = width;
                imgControl.Link = link;
                imgControl.Document_url = documentUrl;
                if (selecteditem)
                    imgControl.CssClass = "imageSlideLink ISSelected";
                else
                    imgControl.CssClass = "imageSlideLink";
                this.SlideWrapper.Controls.Add(imgControl);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private void addImageDescSlide(string img_url, string link, string width, string height, string code, string description, string price, string priceInPoints, bool selecteditem)
        {
            try
            {
                
                if (Session["User"] != null)
                {
                    CurrentUser = (User)Session["User"];
                }
                if (Session["Client"] != null)
                {
                    CurrentClient = (Client)Session["Client"];
                }
                ImageDescSlideUserControl imgDescControl = Page.LoadControl(_ImageDescUCascxPath) as ImageDescSlideUserControl;
                imgDescControl.Image_url = img_url;
                imgDescControl.Height = height;
                imgDescControl.Width = width;
                imgDescControl.Link = link;
                imgDescControl.ProductModel = code;
                imgDescControl.ProductDescription = description;
                int iPriceSel = (int)Session["PriceSelector"];
                CultureInfo culture = new CultureInfo("es-Es", false);
                if (iPriceSel == 0)
                {
                    imgDescControl.ProductPrice = priceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points");
                }
                else if (iPriceSel == 1)
                {
                    imgDescControl.ProductPrice = price + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                }
                else if (iPriceSel == 2)
                {
                    //PRICE WITH PVP
                    imgDescControl.ProductPrice = ((Convert.ToDouble(priceInPoints, culture)) * (Convert.ToDouble(CurrentUser.Coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");
                }
                else if (iPriceSel == 3)
                {
                    imgDescControl.ProductPrice = (
                                                    (Convert.ToDouble(price, culture)) *
                                                    (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                                  ).ToString("N2", culture)
                                                  + " " + CurrentClient.LiteralDivisaWEB;
                }
                else if (iPriceSel == 4)
                {
                    //PRICE WITH PVP
                    imgDescControl.ProductPrice = (
                                                    (Convert.ToDouble(priceInPoints, culture)) * 
                                                    (Convert.ToDouble(CurrentUser.Coeficient, culture)) *
                                                    (Convert.ToDouble(CurrentClient.CoefDivisaWEB, culture))
                                                    ).ToString("N2", culture)
                                                    + " " + CurrentClient.LiteralDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo"); 
                }
             
             
                if (selecteditem)
                    imgDescControl.CssClass = "productElement ISSelected";
                else
                    imgDescControl.CssClass = "productElement";

                this.SlideWrapper.Controls.Add(imgDescControl);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }

    class SlideInfo
    {
        public string img_url;
        public string link;
        public string width;
        public string height;
        public string code;
        public string description;
        public string price;
        public string priceInPoints;
        public string document_url;
    }
}
