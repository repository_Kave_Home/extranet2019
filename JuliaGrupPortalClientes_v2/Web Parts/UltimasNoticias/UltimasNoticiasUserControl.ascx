﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UltimasNoticiasUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.UltimasNoticias.UltimasNoticiasUserControl" %>

<SharePoint:ScriptLink ID="ScriptLink1" name="SP.js" runat="server" OnDemand="true" localizable="false" />
<SharePoint:CssRegistration ID="CssRegistrationNoticias" name="/Style Library/Julia/Noticias.css" runat="server"/>

<div id="Global_container" runat="server">
   <asp:Label runat="server" id="TittleNews" CssClass="TittleNews"></asp:Label>
        <div id="featurednews" runat="server">

        </div>

        <div id="newsCont" runat="server">

        </div>

        <div id="pagination_article" runat="server">

        </div>

    </div>

