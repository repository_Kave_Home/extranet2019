﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;

using Microsoft.SharePoint;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupPortalClientes_v2.ControlTemplates.JuliaGrupPortalClientes_v2;
using System.Globalization;


namespace JuliaGrupPortalClientes_v2.Web_Parts.UltimasNoticias
{
    public partial class UltimasNoticiasUserControl : UserControl
    {
    
        private const int pageSize = 5;
        private const int minPage = 4;
        private int lastPage = 0;
        private const int numFeaturedNews = 2;

        private const string _FeaturedNewascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/FeaturedNew.ascx";
        private const string _SummaryNewascxPath = @"~/_CONTROLTEMPLATES/JuliaGrupPortalClientes_v2/SummaryNew.ascx";

        protected override void CreateChildControls()
        {
            int pageNum = 1;
            string CategoryFilter = string.Empty;
            try
            {
                pageNum = Convert.ToInt32(HttpContext.Current.Request.QueryString["page"]);
                CategoryFilter = HttpContext.Current.Request.QueryString["category"];
            }
            catch (Exception ex)
            {
                pageNum = 1;
            }
            if (pageNum <= 0)
                pageNum = 1;
            int numNews = pageSize * (pageNum + minPage + 1);

            DataTable allNews;
            allNews = JuliaNews.GetLastNewsAsDataTable((uint)numNews);
            //if (string.IsNullOrEmpty(CategoryFilter)){
            //    allNews = JuliaNews.GetLastNewsAsDataTable((uint)numNews);
            //}
            //else{
            //    allNews = JuliaNews.GetLastNewsByKeywordAsDataTable((uint)numNews, CategoryFilter);
            //}
            DataTable news =  new DataTable();;

            news = WPUtils.PageResults(pageSize,
                                            pageNum,
                                            out lastPage,
                                            allNews
                                        );
           
            
            //Article.AddUserInfoToLastNewsDataTable(news);

            if (news.Rows.Count == 0)
                Global_container.Visible = false;
            else
            {
                for (int i = 0; i < news.Rows.Count; i++)
                {
                    DataRow newRow = news.Rows[i];
                    //if ((pageNum == 1) && (i < numFeaturedNews)) 
                    if (i < numFeaturedNews) 
                    {
                        //creamos 2 noticias destacadas
                        FeaturedNew ftn = (FeaturedNew)LoadControl(_FeaturedNewascxPath);

                        string urlNew = WPUtils.getUrlFromArticleDR(newRow);
                        ftn.Title = (newRow["Title"] != null) ? newRow["Title"].ToString() : String.Empty;
                        ftn.Url = urlNew;
                        ftn.ImgUrl = (newRow["PublishingRollupImage"] != null) ? newRow["PublishingRollupImage"].ToString() : String.Empty;
                        ftn.SummaryText = (newRow["JuliaNewsRollupText"] != null) ? newRow["JuliaNewsRollupText"].ToString() : String.Empty;
                        ftn.NewDate = ((newRow["ArticleStartDate"] != null) && (!String.IsNullOrEmpty(newRow["ArticleStartDate"].ToString()))) ? DateTime.Parse(newRow["ArticleStartDate"].ToString()) : DateTime.MinValue;
                       

                        

                        //CommentsDataQuery CommentsDQ = new CommentsDataQuery();
                        //LikeDataQuery likeDQ = new LikeDataQuery();
                        //List<Comment> comments = CommentsDQ.GetCommentsList(SPContext.Current.Site.ID, SPContext.Current.Web.ID, urlNew.Split('/')[urlNew.Split('/').Length - 1]);
                        //List<Like> likes = likeDQ.GetLikes(SPContext.Current.Site.ID, SPContext.Current.Web.ID, urlNew.Split('/')[urlNew.Split('/').Length - 1]);
                        //ftn.CommentsList = comments;
                        //ftn.LikesList = likes;
                        //ftn.LikesNum = likes.Count;
                        //ftn.CommentsNum = comments.Count;
                        ftn.Id = i;

                        string aux = (string)newRow[13];

                        if (aux != null && aux.Length > 0)
                        {
                            ftn.Categories = new string[aux.Split(';').Length];
                            int j = 0;
                            foreach (string partial in aux.Split(';'))
                            {
                                ftn.Categories[j] = partial.Split('|')[0];
                                j++;
                            }
                        }
                        else
                        {
                            ftn.Categories = new string[0];
                        }

                        featurednews.Controls.Add(ftn);
                    }
                    else
                    {
                        SummaryNew smn = (SummaryNew)LoadControl(_SummaryNewascxPath);
                        string urlNew = WPUtils.getUrlFromArticleDR(newRow);
                        smn.Title = (newRow["Title"] != null) ? newRow["Title"].ToString() : String.Empty;
                        smn.Url = urlNew;
                        smn.ImgUrl = (newRow["PublishingRollupImage"] != null) ? newRow["PublishingRollupImage"].ToString() : String.Empty;
                        string summary = (newRow["JuliaNewsRollupText"] != null) ? newRow["JuliaNewsRollupText"].ToString() : String.Empty;
                        smn.NewDate = ((newRow["ArticleStartDate"] != null) && (!String.IsNullOrEmpty(newRow["ArticleStartDate"].ToString()))) ? DateTime.Parse(newRow["ArticleStartDate"].ToString()) : DateTime.MinValue;
                        smn.PagePosition = i % pageSize;                     
                        if(summary.Length > 140)
                            summary = summary.Substring(0, 140) + "...";
                        smn.SummaryText = summary;


                        //CommentsDataQuery CommentsDQ = new CommentsDataQuery();
                        //LikeDataQuery likeDQ = new LikeDataQuery();
                        //List<Comment> comments = CommentsDQ.GetCommentsList(SPContext.Current.Site.ID, SPContext.Current.Web.ID, urlNew.Split('/')[urlNew.Split('/').Length - 1]);
                        //List<Like> likes = likeDQ.GetLikes(SPContext.Current.Site.ID, SPContext.Current.Web.ID, urlNew.Split('/')[urlNew.Split('/').Length - 1]);

                        //smn.LikesNum = likes.Count;
                        //smn.CommentsNum = comments.Count;
                        //smn.CommentsList = comments;
                        //smn.LikesList = likes;
                        smn.Id = (pageNum == 1) ? (i - 2) : i;
                        string aux = (string)newRow[13];

                        if (aux != null && aux.Length > 0)
                        {
                            smn.Categories = new string[aux.Split(';').Length];
                            int j = 0;
                            foreach (string partial in aux.Split(';'))
                            {
                                smn.Categories[j] = partial.Split('|')[0];
                                j++;
                            }
                        }
                        else
                        {
                            smn.Categories = new string[0];
                        }
                        newsCont.Controls.Add(smn);
                    }

                    
                }
                pagination_article.Controls.Add(WPUtils.addPagination(pageNum, lastPage, minPage));
            }


            base.CreateChildControls();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            EnsureChildControls();
            this.TittleNews.Text = JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("TittleNews");
            //int pageNum = 1;
            //try
            //{
            //    pageNum = Convert.ToInt32(HttpContext.Current.Request.QueryString["page"]);
            //}
            //catch (Exception ex)
            //{
            //    pageNum = 1;
            //}
            //if (pageNum <= 0)
            //    pageNum = 1;
            //int numNews = pageSize * (pageNum + minPage + 1);

            //FeaturedNew ftn1 = new FeaturedNew()
            //{
            //    Title = "Hola",
            //    Url = "http://www.google.com",
            //    ImgUrl = "",
            //    SummaryText = "Texto Sumary",
            //    LikesNum = 3,
            //    CommentsNum = 4
            //};
            
            //featurednews.Controls.Add(ftn1);

            //DataTable allNews = TelefonicaNews.GetLastNewsAsDataTable((uint)numNews);
            //DataTable news = WPUtils.PageResults(pageSize, pageNum, out lastPage, allNews);
            //Article.AddUserInfoToLastNewsDataTable(news);

            //if (news.Rows.Count == 0)
            //    Global_container.Visible = false;
            //else
            //{
            //    for (int i = 0; i < news.Rows.Count; i++)
            //    {
            //        DataRow newRow = news.Rows[i];
            //        if (i == 0)
            //        {
            //            string urlNew = WPUtils.getUrlFromArticleDR(newRow);
            //            string stDate = (string)newRow["ArticleStartDate"];
            //            if (!String.IsNullOrEmpty(stDate))
            //            {
            //                DateTime date;
            //                DateTime.TryParse(stDate, out date);
            //                if (date != null)
            //                {
            //                    date_new_box.InnerText = (date).ToString("dd.MM.yyyy");
            //                }
            //            }
            //            usern_new_box.InnerText = newRow["ContactName"] + "";
            //            usern_new_box.HRef = newRow["ContactMySite"] + "";
            //            if (!String.IsNullOrEmpty(WPUtils.getFieldFromDR(newRow, "ContactThumbnail")))
            //                photo_user_new.Src = newRow["ContactThumbnail"].ToString();
            //            else photo_user_new.Visible = false;

            //            social_main_new.Controls.Add(WPUtils.getRatingsComp((string)newRow["AverageRating"]));
            //            social_main_new.Controls.Add(WPUtils.getIlikeitComp(urlNew));
            //            title_main_new.InnerText = newRow["Title"] + "";
            //            shortdesc_main_new.InnerText = newRow["Title"] + "";

            //            readmore_main_new.HRef = urlNew;
            //            link_main_new.HRef = urlNew;
            //            if (newRow["PublishingPageImage"] != null)
            //                link_main_new.Controls.Add(new LiteralControl(newRow["PublishingPageImage"] + ""));
            //        }
            //        else newsCont.Controls.Add(getNewsBoxPanel(newRow));
            //    }
            //    pagination_article.Controls.Add(WPUtils.addPagination(pageNum, lastPage, minPage));
            //}
        }

        //private Panel getNewsBoxPanel(DataRow newsRow)
        //{
        //    Panel newsBox = new Panel();
        //    newsBox.CssClass = "new_box_all";

        //    string urlNew = WPUtils.getUrlFromArticleDR(newsRow);
        //    HtmlGenericControl topBoxHtml = new HtmlGenericControl("div");
        //    topBoxHtml.Attributes.Add("class", "top_new_box");
        //    HtmlGenericControl dateHtml = new HtmlGenericControl("span");
        //    dateHtml.Attributes.Add("class", "date_new_box");
        //    string stDate = WPUtils.getFieldFromDR(newsRow, "ArticleStartDate");
        //    if (!String.IsNullOrEmpty(stDate))
        //    {
        //        DateTime date;
        //        DateTime.TryParse(stDate, out date);
        //        if (date != null)
        //        {
        //            dateHtml.InnerText = (date).ToString("dd.MM.yyyy");
        //            topBoxHtml.Controls.Add(dateHtml);
        //        }
        //    }
        //    string imgThumb = WPUtils.getFieldFromDR(newsRow, "ContactThumbnail");
        //    if (!String.IsNullOrEmpty(imgThumb))
        //    {
        //        HtmlGenericControl photo = new HtmlGenericControl("img");
        //        photo.Attributes.Add("src", imgThumb);
        //        topBoxHtml.Controls.Add(photo);
        //    }
        //    HtmlAnchor userNameHtml = new HtmlAnchor();
        //    userNameHtml.Attributes.Add("class", "user_new_box");
        //    userNameHtml.Attributes.Add("target", "_blank");
        //    userNameHtml.HRef = WPUtils.getFieldFromDR(newsRow, "ContactMySite");
        //    userNameHtml.InnerText = WPUtils.getFieldFromDR(newsRow, "ContactName");
        //    topBoxHtml.Controls.Add(userNameHtml);
        //    newsBox.Controls.Add(topBoxHtml);

        //    HtmlGenericControl bodyHtml = new HtmlGenericControl("div");
        //    bodyHtml.Attributes.Add("class", "body_new_box");
        //    HtmlGenericControl socialHtml = new HtmlGenericControl("div");
        //    socialHtml.Attributes.Add("class", "social_new_box");
        //    socialHtml.Controls.Add(WPUtils.getRatingsComp(WPUtils.getFieldFromDR(newsRow, "AverageRating")));
        //    socialHtml.Controls.Add(WPUtils.getIlikeitComp(urlNew));
        //    bodyHtml.Controls.Add(socialHtml);

        //    HtmlAnchor atitleHtml = new HtmlAnchor();
        //    atitleHtml.HRef = urlNew;
        //    HtmlGenericControl titleHtml = new HtmlGenericControl("h4");
        //    titleHtml.Attributes.Add("class", "title_new_box");
        //    titleHtml.InnerText = newsRow["Title"] + "";
        //    atitleHtml.Controls.Add(titleHtml);
        //    bodyHtml.Controls.Add(atitleHtml);
        //    if (WPUtils.getFieldFromDR(newsRow, "PublishingPageImage") != null)
        //    {
        //        HtmlAnchor aimgHtml = new HtmlAnchor();
        //        aimgHtml.HRef = urlNew;
        //        aimgHtml.InnerHtml = WPUtils.getFieldFromDR(newsRow, "PublishingPageImage") + "";
        //        bodyHtml.Controls.Add(aimgHtml);
        //    }

        //    HtmlAnchor readMoreHtml = new HtmlAnchor();
        //    readMoreHtml.Attributes.Add("class", "readmore_new_box");
        //    readMoreHtml.HRef = urlNew;
        //    readMoreHtml.InnerHtml = "<span>" + Utils.getRecursoStructure("Read_more") + "...</span>";
        //    bodyHtml.Controls.Add(readMoreHtml);
        //    newsBox.Controls.Add(bodyHtml);

        //    return newsBox;



        //    // <div class=lin-hor>&nbsp;</div>
        //}
    
    }

    public static class WPUtils
    {
        public static HtmlGenericControl getRatingsComp(string rating)
        {
            HtmlGenericControl ratingsHtml = new HtmlGenericControl();
            ratingsHtml.Attributes.Add("class", "ratings_star");
            int rat;
            bool halfStar = false;
            try
            {
                double ratD = Double.Parse(rating, CultureInfo.InvariantCulture);
                rat = Convert.ToInt32(ratD);
                ratD -= rat;
                if (ratD > 0.25)
                    if (ratD < 0.75)
                        halfStar = true;
                    else rat++;
            }
            catch (Exception ex)
            {
                rat = 0;
            }
            for (int i = 0; i < rat; i++)
                ratingsHtml.Controls.Add(new LiteralControl("<img src='/Style%20Library/Images/ImgWebParts/estrella.png' />"));
            if (halfStar)
                ratingsHtml.Controls.Add(new LiteralControl("<img src='/Style%20Library/Images/ImgWebParts/media-estrella.png' />"));

            return ratingsHtml;
        }

        public static HtmlGenericControl getIlikeitComp(string url)
        {
            HtmlGenericControl divHtml = new HtmlGenericControl("div");
            divHtml.Attributes.Add("class", "ilikeit_counter");

            //int counter = Utils.getLikesCounter(url);
            //divHtml.InnerText = counter.ToString();
            return divHtml;
        }

        public static string getUrlFromArticleDR(DataRow newRow)
        {
            string href = getFieldFromDR(newRow, ("FileRef")).Split('#')[1];
            string absUrl = (new Uri(getFieldFromDR(newRow, "EncodedAbsUrl"))).GetLeftPart(UriPartial.Authority) + "/";
            return absUrl + href;
        }




        public static DataTable PageResults(int firstPageSize, int pageSize, int pageNumber, out int totalPages, DataTable results)
        {
            int totalElem = results.Rows.Count;
            int restTotal = results.Rows.Count - firstPageSize;
            if (restTotal < 0)
                totalPages = 1;
            else
            {
                double dpages = (double)restTotal / pageSize;
                totalPages = Convert.ToInt32(Math.Ceiling(dpages)) + 1;
            }
            //all pages, return all
            if (pageNumber == -1 || pageNumber == 0)
                return results;

            int firstElem = Math.Max(firstPageSize + (pageSize * (pageNumber - 2)), 0);
            int lastElem = (pageNumber == 1) ? Math.Min(firstPageSize, totalElem) : Math.Min(firstElem + pageSize, totalElem);
            DataTable dt = results.Clone();
            for (int i = firstElem; i < lastElem; i++) //if pageNumber has not elements, lastElem < firstElem
            {
                dt.ImportRow(results.Rows[i]);
            }
            return dt;
        }

        public static DataTable PageResults(int pageSize, int pageNumber, out int totalPages, DataTable results)
        {
            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = results.DefaultView;
            pds.AllowPaging = true;
            pds.PageSize = pageSize;
            totalPages = pds.PageCount;

            //all pages, return all
            if (pageNumber == -1 || pageNumber == 0)
                return results;
            try
            {
                pds.CurrentPageIndex = pageNumber - 1;
            }
            catch (Exception ex)
            {
                pds.CurrentPageIndex = 0;
            }

            DataTable dt = results.Clone();
            foreach (DataRowView dr in pds)
            {
                dt.ImportRow(dr.Row);
            }
            return dt;
        }

        public static HtmlGenericControl addPagination(int currPage, int lastPage, int minPage, DateTime? daySel)
        {
            string addDateQS = "";
            if (daySel.HasValue)
                addDateQS = "&day=" + HttpUtility.UrlEncode(daySel.Value.ToString("yyyy-MM-dd"));
            return addPagination(currPage, lastPage, minPage, addDateQS);
        }

        public static HtmlGenericControl addPagination(int currPage, int lastPage, int minPage)
        {
            var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
            nameValues.Remove("page");
            string additionalQS = String.Empty;
            if (nameValues.Count > 0)
            {
                additionalQS = "&" + nameValues.ToString();
            }

            return addPagination(currPage, lastPage, minPage, additionalQS);
        }

        public static HtmlGenericControl addPagination(int currPage, int lastPage, int minPage, string additionalQS)
        {
            HtmlGenericControl pagination_article = new HtmlGenericControl("div");
            pagination_article.Attributes.Add("class", "pagination_article");
            if (lastPage > 1)
            {
                int fstPage = Math.Max(1, currPage - minPage);
                int maxPage = Math.Min(currPage + minPage, lastPage);

                if (fstPage > 1)
                    pagination_article.Controls.Add(getNumPage(1, false, additionalQS));
                if (fstPage > 2)
                    pagination_article.Controls.Add(new LiteralControl("<span> ... </span>"));
                for (int i = fstPage; i <= maxPage; i++)
                {
                    if (i == currPage)
                        pagination_article.Controls.Add(getNumPage(i, true, additionalQS));
                    else pagination_article.Controls.Add(getNumPage(i, false, additionalQS));
                }
                if (maxPage < lastPage)
                    pagination_article.Controls.Add(new LiteralControl("<span> ... </span>"));
            }
            return pagination_article;
        }

        public static HtmlAnchor getNumPage(int ind, bool current, string additionalQS)
        {
            string url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path) + "?page=" + ind + additionalQS;
            HtmlAnchor aInd = new HtmlAnchor();
            if (current)
                aInd.Attributes.Add("class", "page_index_sel");
            else aInd.Attributes.Add("class", "page_index");
            aInd.HRef = url;
            aInd.InnerText = ind.ToString();
            return aInd;
        }

        //public static HtmlGenericControl getLyncControl(UserInfo userInfo)
        //{
        //    HtmlGenericControl spanCont = new HtmlGenericControl("span");
        //    spanCont.Attributes.Add("class", "presence_lync");
        //    if (!String.IsNullOrEmpty(userInfo.SipAddress))
        //    {
        //        HtmlGenericControl imgPresence = new HtmlGenericControl("img");
        //        imgPresence.Attributes.Add("border", "0");
        //        imgPresence.Attributes.Add("height", "12");
        //        imgPresence.Attributes.Add("src", "/_layouts/images/imnhdr.gif");
        //        imgPresence.Attributes.Add("onload", "IMNRC('" + userInfo.SipAddress + "')");
        //        imgPresence.Attributes.Add("ShowOfflinePawn", "1");
        //        imgPresence.Attributes.Add("id", "TD_pawn" + userInfo.SipAddress + ",type=sip");
        //        imgPresence.Attributes.Add("alt", userInfo.FullName);
        //        spanCont.Controls.Add(imgPresence);
        //    }
        //    return spanCont;
        //}

        public static string getFieldFromDR(DataRow dr, string fieldName)
        {
            try
            {
                return (string)dr[fieldName];
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string getTimeAgo(DateTime dateEntry)
        {
            string dateSt = WPUtils.getResourceWebParts("HoursAbout");
            TimeSpan dateDiff = DateTime.UtcNow - dateEntry;
            if (dateDiff.Days > 0)
                dateSt = dateEntry.ToString("dd.MM.yyyy");
            else if (dateDiff.Hours > 0)
            {
                if (dateDiff.Hours > 1)
                    dateSt = String.Format(dateSt, dateDiff.Hours + WPUtils.getResourceWebParts("HoursTx"));
                else dateSt = String.Format(dateSt, dateDiff.Hours + WPUtils.getResourceWebParts("HourTx"));
            }
            else if (dateDiff.Minutes > 0)
            {
                if (dateDiff.Minutes > 1)
                    dateSt = String.Format(dateSt, dateDiff.Minutes + WPUtils.getResourceWebParts("MinutesTx"));
                else dateSt = String.Format(dateSt, dateDiff.Minutes + WPUtils.getResourceWebParts("MinuteTx"));
            }
            else if (dateDiff.Seconds > 0)
                dateSt = String.Format(dateSt, WPUtils.getResourceWebParts("SecondsTx"));
            else dateSt = dateEntry.ToString("dd.MM.yyyy");

            return dateSt;
        }

        public static string fromHtmlToText(string cadHtml)
        {
            string cadConv = null;
            if (cadHtml != null)
            {
                int a, b;
                string aux2, aux3;
                cadConv = cadHtml;
                cadConv = cadConv.Replace("&lt;", "<");
                cadConv = cadConv.Replace("&gt;", ">");
                do
                {
                    aux2 = "";
                    aux3 = "";
                    a = 0;
                    b = 0;

                    a = cadConv.IndexOf('<');
                    if (a != -1) //there is a '<'
                    {
                        if (a > 0) aux2 = cadConv.Substring(0, a); //substring up to '<'
                        if (cadConv.Length > (a + 1))
                        {
                            b = cadConv.IndexOf('>', (a + 1));
                            if ((b != -1) && (cadConv.Length > (b + 1)))
                                aux3 = cadConv.Substring((b + 1), (cadConv.Length - (b + 1)));
                        }
                        cadConv = aux2 + aux3;
                    }
                } while (a != -1);
            }
            return cadConv;
        }

        public static string getResourceWebParts(string key)
        {
            string tagVal = key;
            try
            {
                tagVal = HttpContext.GetGlobalResourceObject("JuliaGrupPortalClientesResources", key).ToString();
            }
            catch (Exception e) { }
            return tagVal;
        }
    }
}
