﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Publishing;


using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupPortalClientes_v2.Web_Parts.VariationRedirectLogic
{
    public partial class VariationRedirectLogicUserControl : UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //}

        private const string QualityValuePrefix = ";q=";
        private enum PropertiesOnLabelToUse
        {
            Language,
            Locale
        }
        private enum MatchingPreference
        {
            ImpreciseOrderFirst,
            PreciseMatch
        }
        private PropertiesOnLabelToUse PropertyOnLabelToUse
        {
            get { return this.propertyOnLabelToUse; }
            set { this.propertyOnLabelToUse = value; }
        }
        private PropertiesOnLabelToUse propertyOnLabelToUse = PropertiesOnLabelToUse.Locale;
        private MatchingPreference PreferenceOrder
        {
            get { return this.preferenceOrder; }
            set { this.preferenceOrder = value; }
        }
        private MatchingPreference preferenceOrder = MatchingPreference.ImpreciseOrderFirst;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            string targetUrl = this.GetRedirectTargetUrl();
            if (!string.IsNullOrEmpty(targetUrl))
            {
                SPUtility.Redirect(targetUrl, SPRedirectFlags.Default, Context);
                lblRedirectUrl.Text = targetUrl;
            }
        }

        User currentUser;

        private string GetRedirectTargetUrl()
        {

            Sesion SesionJulia = new Sesion();

            if (Session["User"] != null)
            {
                this.currentUser = (User)Session["User"];

            }
            else
            {
                this.currentUser = SesionJulia.GetUser(SPContext.Current.Web.CurrentUser.LoginName);

                if (this.currentUser == null)
                    throw new Exception("Error NULL en el CurrentUSer");
                Session.Add("User", currentUser);
            }

            string ret = string.Empty;
            switch (currentUser.Language)
            {
                case "CAT":
                    ret = "ca-es";
                    break;
                case "ESP":
                    ret = "es-es";
                    break;
                case "FRA":
                    ret = "fr-fr";
                    break;
                case "ITA":
                    ret = "it-it";
                    break;
                case "ENU":
                    ret = "en-us";
                    break;
                case "DEU":
                    ret = "de-de";
                    break;
                default:
                    ret = "es-es";
                    break;
            }
            return SPContext.Current.Web.Url + "/" + ret;


            //ReadOnlyCollection<VariationLabel> spawnedLabels = Variations.Current.UserAccessibleLabels;
            //if (spawnedLabels.Count > 0)
            //{
            //    string sourceLabelUrl = string.Empty;
            //    Dictionary<string, string> cultureCodeToUrlMapping = new Dictionary<string, string>();
            //    Dictionary<string, string> cultureCodeStrippedToUrlMapping = new Dictionary<string, string>();
            //    foreach (VariationLabel label in spawnedLabels)
            //    {
            //        if (label.IsSource)
            //        {
            //            sourceLabelUrl = label.TopWebUrl;
            //        }
            //        CultureInfo labelCultureInfo = this.GetLabelCultureInfo(label);
            //        string labelCultureInfoName = labelCultureInfo.Name.ToUpperInvariant();
            //        if (!cultureCodeToUrlMapping.ContainsKey(labelCultureInfoName) || label.IsSource)
            //        {
            //            cultureCodeToUrlMapping.Remove(labelCultureInfoName);
            //            cultureCodeToUrlMapping.Add(labelCultureInfoName, label.TopWebUrl);
            //            string strippedCode = labelCultureInfoName.Split('-')[0];
            //            if (!cultureCodeStrippedToUrlMapping.ContainsKey(strippedCode) || label.IsSource)
            //            {
            //                cultureCodeStrippedToUrlMapping.Remove(strippedCode);
            //                cultureCodeStrippedToUrlMapping.Add(strippedCode, label.TopWebUrl);
            //            }
            //        }
            //    }
            //    string matchedUrl;
            //    if (MatchingPreference.ImpreciseOrderFirst == this.preferenceOrder)
            //    {
            //        matchedUrl = this.GetRedirectTargetUrlImpreciseOrderFirst(
            //            cultureCodeToUrlMapping, cultureCodeStrippedToUrlMapping);
            //    }
            //    else
            //    {
            //        matchedUrl = this.GetRedirectTargetUrlPreciseMatch(
            //            cultureCodeToUrlMapping, cultureCodeStrippedToUrlMapping);
            //    }
            //    return (string.IsNullOrEmpty(matchedUrl) ? sourceLabelUrl : matchedUrl);
            //}
            //return null;
        }
        private CultureInfo GetLabelCultureInfo(VariationLabel label)
        {
            if (PropertiesOnLabelToUse.Locale == this.propertyOnLabelToUse)
            {
                return new CultureInfo(Convert.ToInt32(label.Locale, CultureInfo.InvariantCulture));
            }
            else
            {
                return new CultureInfo(label.Language);
            }
        }
        private string GetRedirectTargetUrlImpreciseOrderFirst(Dictionary<string, string> cultureCodeToUrlMapping, Dictionary<string, string> cultureCodeStrippedToUrlMapping)
        {
            string[] browserPrefLanguages = this.GetUserLanguages();
            if (null == browserPrefLanguages)
                return null;
            string browserPrefLang;
            string browserPrefLangStripped;
            for (int i = 0; i < browserPrefLanguages.Length; i++)
            {
                browserPrefLang = browserPrefLanguages[i].ToUpperInvariant();
                if (cultureCodeToUrlMapping.ContainsKey(browserPrefLang))
                {
                    return cultureCodeToUrlMapping[browserPrefLang];
                }
                browserPrefLangStripped = browserPrefLang.Split('-')[0];
                if ((browserPrefLang != browserPrefLangStripped) &&
                    (cultureCodeToUrlMapping.ContainsKey(browserPrefLangStripped)))
                {
                    return cultureCodeToUrlMapping[browserPrefLangStripped];
                }
                if (cultureCodeStrippedToUrlMapping.ContainsKey(browserPrefLangStripped))
                {
                    return cultureCodeStrippedToUrlMapping[browserPrefLangStripped];
                }
            }
            return null;
        }
        private string GetRedirectTargetUrlPreciseMatch(Dictionary<string, string> cultureCodeToUrlMapping, Dictionary<string, string> cultureCodeStrippedToUrlMapping)
        {
            string[] browserPrefLanguages = this.GetUserLanguages();
            if (null == browserPrefLanguages)
                return null;
            for (int i = 0; i < browserPrefLanguages.Length; i++)
            {
                string browserPrefLanguageName = browserPrefLanguages[i].ToUpperInvariant();
                if (cultureCodeToUrlMapping.ContainsKey(browserPrefLanguageName))
                {
                    return cultureCodeToUrlMapping[browserPrefLanguageName];
                }
            }
            for (int i = 0; i < browserPrefLanguages.Length; i++)
            {
                string browserPrefLanguageName = browserPrefLanguages[i].ToUpperInvariant();
                if (cultureCodeStrippedToUrlMapping.ContainsKey(browserPrefLanguageName))
                {
                    return cultureCodeStrippedToUrlMapping[browserPrefLanguageName];
                }
            }
            string browserPrefLangStripped;
            for (int i = 0; i < browserPrefLanguages.Length; i++)
            {
                browserPrefLangStripped = browserPrefLanguages[i].Split('-')[0].ToUpperInvariant();
                if (cultureCodeToUrlMapping.ContainsKey(browserPrefLangStripped))
                {
                    return cultureCodeToUrlMapping[browserPrefLangStripped];
                }
            }
            for (int i = 0; i < browserPrefLanguages.Length; i++)
            {
                browserPrefLangStripped = browserPrefLanguages[i].Split('-')[0].ToUpperInvariant();
                if (cultureCodeStrippedToUrlMapping.ContainsKey(browserPrefLangStripped))
                {
                    return cultureCodeStrippedToUrlMapping[browserPrefLangStripped];
                }
            }
            return null;
        }
        private string[] GetUserLanguages()
        {
            string[] browserPrefLanguages = Page.Request.UserLanguages;
            if (null != browserPrefLanguages)
            {
                int qualityIndexPos = -1;
                for (int i = 0; i < browserPrefLanguages.Length; i++)
                {
                    qualityIndexPos = browserPrefLanguages[i].IndexOf(QualityValuePrefix, StringComparison.Ordinal);
                    if (qualityIndexPos > 0)
                    {
                        browserPrefLanguages[i] = browserPrefLanguages[i].Substring(0, qualityIndexPos);
                    }
                }
            }
            return browserPrefLanguages;
        }
        private string ConcatUrls(string firstPart, string secondPart)
        {
            if (firstPart.EndsWith("/"))
            {
                if (secondPart.StartsWith("/"))
                {
                    firstPart = firstPart.TrimEnd('/');
                }
                return firstPart + secondPart;
            }
            else
            {
                if (secondPart.StartsWith("/"))
                    return firstPart + secondPart;
                else
                    return firstPart + "/" + secondPart;
            }
        }

    }
}
