﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WishlistUserControl.ascx.cs"
    Inherits="JuliaGrupPortalClientes_v2.Web_Parts.Wishlist.WishlistUserControl" %>
<%@ Assembly Name="JuliaGrupUtils, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f4f250518de63a98" %>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
    //$(document).ready(
    function setWishlist() {
        $('#EANCode').focus();
        checkComparatorItems();
        checkAddToCartCheckboxes();
        $(function () {
            $("#comparatorModal").dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                modal: true
            });
        });
        $("#comparatorModal").on("show", function () {
            $("body").addClass("modal-open");
        }).on("hidden", function () {
            $("body").removeClass("modal-open")
        });
        $("div[role=dialog] button:contains('Close')").css("background", "none");
        $("div[role=dialog] button:contains('Close')").css("border", "none");
        $("div[role=dialog] button:contains('Close')").css("color", "#777");

        $('#comparatorModal').on('dialogclose', function (event) {
            $('#EANCode').focus();
        });
        $("#EANCode").unbind().keypress(function (event) {
            if (event.which == 13) {
                $("#EANCodeResult").text("");
                $("#EANCodeResult").removeClass("EANCodeResultIcon");
                JuliaClients.Service.searchEANCode(this.value, $('#currentPriceSelId').val(), $('#currentUserName').val(), $('#currentClientCode').val());
            }
            checkAddToCartCheckboxes();
            $('#EANCode').focus();
        });
        $("#EANCode").focus(function () {
            $(this).parent().addClass("EANCodeInputBoxSelected");
        });
        $("#EANCode").blur(function () {
            $(this).parent().removeClass("EANCodeInputBoxSelected");
        });
        $("#comparator").click(function () {
            //if ($('.comparatorDiv input:checkbox:checked').length > 4 || $('.comparatorDiv input:checkbox:checked').length < 2) {
            if ($('.checkBoxChart input:checkbox:checked').length < 2) {
                $("#comparatorPopup").attr("title", Res["select2To4Elements"]);
                $("#comparatorPopup").addClass("comparePopup");
                setTimeout(function () {
                    $("#comparatorPopup").removeClass("comparePopup");
                    $("#comparatorPopup").attr("title", "");
                }, 5000);
            } else {
                var codes = [];
                $("#comparatorModal").text("");
                $elements = $('.checkBoxChart input:checkbox:checked').parent().parent().parent();
                $elements.each(function (index) {
                    codes.push($(this).attr('id'));
                    if (codes.length == 4)
                        return false;
                });
                JuliaClients.Service.loadComparation(codes);
                $("#comparatorModal").dialog("option", "width", codes.length * 325);
                $("#comparatorModal").dialog("option", "position", "top");
                $("#comparatorModal").dialog("open");
                $("#comparatorModal").parent().addClass("modalWindowContainer");
                $("#comparatorModal").addClass("modalWindowContent");
            }
        });
        $("#deleteAll").unbind().click(function () {
            JuliaClients.Service.deleteWishlistAll($('#currentUserName').val(), $('#currentClientCode').val());
            checkComparatorItems();
            checkAddToCartCheckboxes();
            check4ItemsToCompare();
            $('#EANCode').focus();
        });
        $("#addToCart").unbind().click(function () {
            //JuliaClients.Service.addToCart($('#currentUserName').val(), $('#currentClientCode').val(), $("#currentOrderNo").val());
            $('#EANCode').focus();
            if ($('.checkBoxChart input:checkbox:checked').length != 0) {
                $.blockUI({ message: $('#loadingPnl') });
                var codes = [];
                $elements = $('.checkBoxChart input:checkbox:checked').parent().parent().parent();
                $elements.each(function (index) {
                    codes.push($(this).attr('id'));
                });
                JuliaClients.Service.addToCartFromWishlist(codes);
                checkComparatorItems();
                checkAddToCartCheckboxes();
                check4ItemsToCompare();
            }
        });
        $("#EANCode").focus(function () {
            $(this).parent().addClass("EANCodeInputBoxSelected");
        });
        $(".checkBoxChart input:checkbox").change(function () {
            checkAddToCartCheckboxes();
            check4ItemsToCompare();
        });
        $("#selectAll").change(function () {
            var compValue = $("#comparatorValue");
            var val = compValue.text();
            if ($("#selectAll").prop("checked")) {
                $(".checkBoxChart input:checkbox").prop("checked", true);
                if ($(".checkBoxChart input:checkbox").length >= 4) {
                    compValue.text(4);
                } else {
                    compValue.text($(".checkBoxChart input:checkbox").length);
                }                
            } else {
                $(".checkBoxChart input:checkbox").prop("checked", false);
                compValue.text(0);
            }
            check4ItemsToCompare();
        });
    }
    //);
    function minus(element, step, code) {
        var value = $('[id="' + element + '"]').val();
        var result = +value - step;
        if (result >= step) {
            $('[id="' + element + '"]').val(result);
            JuliaClients.Service.updateProductQuantity(result.toString(), code, $('#currentUserName').val(), $('#currentClientCode').val());
        }
    };
    function plus(element, step, code) {
        var value = $('[id="' + element + '"]').val();
        var result = +value + step;
        $('[id="' + element + '"]').val(result);
        JuliaClients.Service.updateProductQuantity(result.toString(), code, $('#currentUserName').val(), $('#currentClientCode').val());
    };
    function qtyBlur(element, code) {
        var value = $('[id="' + element + '"]').val();
        if (+value > 0) {
            JuliaClients.Service.updateProductQuantity(value, code, $('#currentUserName').val(), $('#currentClientCode').val());
        }
    };
    function comparatorCheckbox(lineNo) {
        var checkbox = $("#wishlistCheckbox-" + lineNo);
        var compValue = $("#comparatorValue");
        var val = compValue.text();
        if (checkbox.is(':checked') && (+val < 4)) {
            compValue.text(+val + 1);
        } else {
            if ($(".checkBoxChart input:checkbox:checked").length < 4) {
                compValue.text(+val - 1);
            }
        };
    };
    function checkComparatorItems() {
        var compValue = $("#comparatorValue");
        compValue.text($('.comparatorDiv input:checkbox:checked').length);
    };
    function checkAddToCartCheckboxes() {
        if ($(".checkBoxChart").length != 0) {
            if ($(".checkBoxChart input:checkbox:checked").length == $(".checkBoxChart").length) {
                $("#selectAll").prop("checked", true);
            } else {
                $("#selectAll").prop("checked", false);
            } 
        }
    };
    function check4ItemsToCompare() {
        if ($('.checkBoxChart input:checkbox:checked').length > 4) {
            $("#comparatorPopup").attr("title", Res["Max4Elements"]);
            $("#comparatorPopup").addClass("comparePopup");
        } else {
            $("#comparatorPopup").removeClass("comparePopup");
            $("#comparatorPopup").attr("title", "");
        }
    };
    _spBodyOnLoadFunctionNames.push("setWishlist");


    $("#EANCode").focus(function () {
        $(this).parent().addClass("EANCodeInputBoxSelected");
    }); 
   
</script>
<input type="checkbox" name="selectAll" class="checkbox-custom" id="selectAll" style="display:none;" />
<label for="selectAll" class="checkbox-custom-label" style="margin-left: 2rem;">
</label>
<asp:Label runat="server" ID="lblselectAll"><%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("SelectAll") %></asp:Label>
<asp:Label runat="server" CssClass="paddingLeft" ID="CaptionEAN"></asp:Label>
<div id="EANCodeInputBox" class="EANCodeInputBoxSelected">
    <span class="fa fa-barcode fa-lg EANIcon"></span>
    <input id="EANCode" style="background: transparent;" type="text" placeholder="<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("CaptionEAN") %>"
        name="EANCode" />
</div>
<div id="EANCodeResult" class="EANCodeResultStyle">
</div>
<div id="deleteAll" class="deleteButton" style="float: right; margin-left: 1rem;">
    <i class="fa fa-trash-o fa-lg" style="padding-right: 0.4rem"></i>
    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DeleteAll") %>
</div>
<div id="comparator" style="float: right; margin-left: 1rem;">
    <span id="comparatorPopup" class="blueButton" tabindex="-1"><i class="fa fa-balance-scale fa-lg"
        style="padding-right: 0.4rem"></i>
        <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Compare")%>
        (<label id="comparatorValue">0</label>) </span>
</div>
<%--<div id="comparatorAlert" style="display: none;">--%>
<div id="addToCart" class="greenButton" style="float: right;">
    <i class="fa fa-cart-plus fa-lg" style="padding-right: 0.4rem"></i>
    <%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("AddToCart") %>
</div>
<div id="wishlist" style="padding-top: 1rem;">
</div>
<div id="comparatorModal" title="<%=JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Comparator") %>">
</div>