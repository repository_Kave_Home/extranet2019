﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JuliaGrupUtils.Utils;
using System.Web;
using JuliaGrupUtils.Business;

namespace JuliaGrupPortalClientes_v2.Web_Parts.Wishlist
{
    public partial class WishlistUserControl : UserControl
    {
        User currentUser;
        Client currentClient;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.inicialice();
        }

        private void inicialice()
        {          
            if (Session["User"] != null)
            {
                currentUser = (User)Session["User"];
            }
            if (Session["Client"] != null)
            {
                currentClient = (Client)Session["Client"];
            }

            if (currentUser != null || currentClient != null)
            {
                JuliaGrupUtils.Business.Wishlist wishlist = new JuliaGrupUtils.Business.Wishlist();
                if (HttpContext.Current.Cache.Get("Wishlist_" + currentUser.UserName + "_" + currentClient.Code) == null)
                {
                    wishlist.AssignUserAndCode(currentUser.UserName, currentClient.Code);
                    wishlist.GetWishlist();
                    HttpContext.Current.Cache.Add("Wishlist_" + currentUser.UserName + "_" + currentClient.Code, wishlist, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                }

                string loadWishlist = "<script language='javascript'>JuliaClients.Service.loadWishlist()</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "loadWishlist", loadWishlist);
            }
        }
    }
}
