﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI,  Version=2016.1.225.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="testFacturasListUserControl.ascx.cs" Inherits="JuliaGrupPortalClientes_v2.Web_Parts.testFacturasList.testFacturasListUserControl" %>


    <telerik:RadGrid ID="RadGrid1" 
    Width="100%" 
    AllowFilteringByColumn="true" 
    AllowPaging="true" 
    AllowSorting="true" 
    AutoGenerateColumns="false" 
    GridLines="None" 
    OnNeedDataSource="RadGrid1_NeedDataSource" 
    CssClass="Grid" 
    PageSize="9" 
    runat="server" 
    > 

      <GroupingSettings CaseSensitive="false" /> 
         <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView Width="100%" Summary="RadGrid table" 
        CommandItemDisplay="Bottom"  
        UseAllDataFields="true" 
        CanRetrieveAllData="false"
        >
        <CommandItemSettings  ShowAddNewRecordButton="false" />
         <Columns>
             <telerik:GridBoundColumn CurrentFilterFunction="Contains" ShowFilterIcon="false"  AllowFiltering="true" AutoPostBackOnFilter="true"  DataField="BillCode" UniqueName="BillCode" />
        </Columns>
         </MasterTableView>
        <PagerStyle Mode="NextPrev" />
        <ClientSettings>
            <ClientEvents OnRowClick="RowClick">
            </ClientEvents>
        </ClientSettings>
    </telerik:RadGrid>