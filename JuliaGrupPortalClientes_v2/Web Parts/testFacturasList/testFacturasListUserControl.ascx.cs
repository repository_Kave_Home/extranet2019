﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataAccessObjects;
using Telerik.Web.UI;
using System.Data;
using JuliaGrupUtils.Utils;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using Microsoft.SharePoint;

namespace JuliaGrupPortalClientes_v2.Web_Parts.testFacturasList
{
    public partial class testFacturasListUserControl : UserControl
    {
        private List<Venta> ventas;

        private User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                DataTable data = null;
              

               
                    data = LoadAllOrders();
              

                this.RadGrid1.DataSource = data;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        private DataTable LoadAllOrders()
        {
            DataTable data = null;
            try
            {
                Sesion SesionJulia = new Sesion();
                VentaDataAccessObject VentasDAO;
                List<Venta> Ventas;

              
                 this.currentUser = (User)Session["User"];
                   
                    VentasDAO = SesionJulia.GetVentasAgent(currentUser.Code);
                    Ventas = VentasDAO.Ventas();
                   
            
                data = GenerateTable();
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }
        private DataTable GenerateTable()
        {

            DataTable data = new DataTable();
            try
            {
               
                data.Columns.Add("BillCode");
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_LOG, JuliaGrupUtils.Log.Constantes.PRESENTATION_AREA_CAT_LAYER_LOG);
                throw new SPException(new StackFrame(1).GetMethod().Name, ex);
            }
            return data;
        }
    }
}
