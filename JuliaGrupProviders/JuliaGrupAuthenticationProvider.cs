﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Configuration;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupProviders
{
    public class JuliaGrupAuthenticationProvider : MembershipProvider
    {
        string connectionstring = ConfigurationManager.AppSettings["NavisionDBConnectionString"];

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            //using (SqlConnection conn = new SqlConnection(this.connectionstring))
            //{
                MembershipUserCollection userCollection = null ;
                totalRecords = 0;

                try
                {
                    userCollection = new System.Web.Security.MembershipUserCollection();

                    UserDataAccessObject userDAO = new UserDataAccessObject();
                    List<JuliaGrupUtils.Business.User> fusers = userDAO.FindUsersByEmail(emailToMatch);
                    foreach (JuliaGrupUtils.Business.User fuser in fusers)
                    {
                        MembershipUser user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                                                                                           fuser.UserName,
                                                                                           fuser.UserName,
                                                                                           fuser.Email,
                                                                                           string.Empty,
                                                                                           string.Empty,
                                                                                           true,
                                                                                           false,
                                                                                           DateTime.Now.ToLocalTime(),
                                                                                           DateTime.Now.ToLocalTime(),
                                                                                           DateTime.Now.ToLocalTime(),
                                                                                           DateTime.Now.ToLocalTime(),
                                                                                           DateTime.Now.ToLocalTime());
                        userCollection.Add(user);
                    }
                    totalRecords = userCollection.Count;

                    //userCollection = new System.Web.Security.MembershipUserCollection();
                    //string command = "select username, name, email from users where email = @email";
                    //SqlCommand cmd = new SqlCommand(command, conn);
                    //cmd.Parameters.AddWithValue("@email", emailToMatch);

                    //conn.Open();
                    //using (SqlDataReader reader = cmd.ExecuteReader())
                    //{

                    //    while (reader.Read())
                    //    {
                    //        MembershipUser user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                    //                                                                       reader["name"].ToString(),
                    //                                                                       reader["username"].ToString(),
                    //                                                                       reader["email"].ToString(),
                    //                                                                       string.Empty,
                    //                                                                       string.Empty,
                    //                                                                       true,
                    //                                                                       false,
                    //                                                                       DateTime.Now.ToLocalTime(),
                    //                                                                       DateTime.Now.ToLocalTime(),
                    //                                                                       DateTime.Now.ToLocalTime(),
                    //                                                                       DateTime.Now.ToLocalTime(),
                    //                                                                       DateTime.Now.ToLocalTime());
                    //        userCollection.Add(user);
                    //    }
                    //}
                    //totalRecords = userCollection.Count;
                    }
                    catch (Exception ex)
                    {
                        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                        throw new Exception(new StackFrame(1).GetMethod().Name, ex);
                    }
                        return userCollection;
            //}
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection userCollection = null ;
            totalRecords = 0;

            try
            {
                userCollection = new System.Web.Security.MembershipUserCollection();

                UserDataAccessObject userDAO = new UserDataAccessObject();
                List<JuliaGrupUtils.Business.User> fusers = userDAO.FindUsersByEmail(usernameToMatch);
                foreach (JuliaGrupUtils.Business.User fuser in fusers)
                {
                    MembershipUser user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                                                                                       fuser.UserName,
                                                                                       fuser.UserName,
                                                                                       fuser.Email,
                                                                                       string.Empty,
                                                                                       string.Empty,
                                                                                       true,
                                                                                       false,
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime());
                    userCollection.Add(user);
                }
                totalRecords = userCollection.Count;
                //using (SqlConnection conn = new SqlConnection(this.connectionstring))
                //{
                //    userCollection = new System.Web.Security.MembershipUserCollection();

                //    string command = "select username, name, email  from users where username = @username";
                //    SqlCommand cmd = new SqlCommand(command, conn);
                //    cmd.Parameters.AddWithValue("@username", usernameToMatch);

                //    conn.Open();
                //    using (SqlDataReader reader = cmd.ExecuteReader())
                //    {

                //        while (reader.Read())
                //        {
                //            MembershipUser user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                //                                                                           reader["name"].ToString(),
                //                                                                           reader["username"].ToString(),
                //                                                                           reader["email"].ToString(),
                //                                                                           string.Empty,
                //                                                                           string.Empty,
                //                                                                           true,
                //                                                                           false,
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime());
                //            userCollection.Add(user);
                //        }
                //        totalRecords = userCollection.Count;
                //    }
                //}

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return userCollection;
            
          
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser user =null;
            try
            {
                UserDataAccessObject userDAO = new UserDataAccessObject();
                List<JuliaGrupUtils.Business.User> fusers = userDAO.GetUser(username);
                foreach (JuliaGrupUtils.Business.User fuser in fusers)
                {
                    user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                                                                                       fuser.UserName,
                                                                                       fuser.UserName,
                                                                                       fuser.Email,
                                                                                       string.Empty,
                                                                                       string.Empty,
                                                                                       true,
                                                                                       false,
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime());
                    break;
                }

                //using (SqlConnection conn = new SqlConnection(this.connectionstring))
                //{
                //    string command = "select username, name, email from users where username = @username";
                //    SqlCommand cmd = new SqlCommand(command, conn);
                //    cmd.Parameters.AddWithValue("@username", username);

                //    conn.Open();
                //    using (SqlDataReader reader = cmd.ExecuteReader())
                //    {

                //        while (reader.Read())
                //        {
                //            user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                //                                                                           reader["name"].ToString(),
                //                                                                           reader["username"].ToString(),
                //                                                                           reader["email"].ToString(),
                //                                                                           string.Empty,
                //                                                                           string.Empty,
                //                                                                           true,
                //                                                                           false,
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime());
                //            break;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return user;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            MembershipUser user =null;
            try
            {
                UserDataAccessObject userDAO = new UserDataAccessObject();
                List<JuliaGrupUtils.Business.User> fusers = userDAO.GetUser((string)providerUserKey);   //Lo convertimos a string y miramos si es el username...
                foreach (JuliaGrupUtils.Business.User fuser in fusers)
                {
                    user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                                                                                       fuser.UserName,
                                                                                       fuser.UserName,
                                                                                       fuser.Email,
                                                                                       string.Empty,
                                                                                       string.Empty,
                                                                                       true,
                                                                                       false,
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime(),
                                                                                       DateTime.Now.ToLocalTime());
                    break;
                }
                //using (SqlConnection conn = new SqlConnection(this.connectionstring))
                //{
                //    string command = "select username, name, email from users where username = @username";
                //    SqlCommand cmd = new SqlCommand(command, conn);
                //    cmd.Parameters.AddWithValue("@username", providerUserKey.ToString());

                //    conn.Open();
                //    using (SqlDataReader reader = cmd.ExecuteReader())
                //    {

                //        while (reader.Read())
                //        {
                //            user = new System.Web.Security.MembershipUser("JuliaGrupAuthenticationProvider",
                //                                                                           reader["name"].ToString(),
                //                                                                           reader["username"].ToString(),
                //                                                                           reader["email"].ToString(),
                //                                                                           string.Empty,
                //                                                                           string.Empty,
                //                                                                           true,
                //                                                                           false,
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime(),
                //                                                                           DateTime.Now.ToLocalTime());
                //            break;
                //        }
                //    }
                //} 
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return user;
        }

        public override string GetUserNameByEmail(string email)
        {
            string result = null;

            try
            {
                UserDataAccessObject userDAO = new UserDataAccessObject();
                List<JuliaGrupUtils.Business.User> fusers = userDAO.FindUsersByEmail(email);
                if (fusers.Count() == 1)
                {
                    result = fusers[0].UserName;
                }
                
                //using (SqlConnection conn = new SqlConnection(this.connectionstring))
                //{
                //    string command = "select username, name, email from users where email = @email";
                //    SqlCommand cmd = new SqlCommand(command, conn);
                //    cmd.Parameters.AddWithValue("@email", email);

                //    conn.Open();
                //    using (SqlDataReader reader = cmd.ExecuteReader())
                //    {

                //        while (reader.Read())
                //        {
                //            result= reader["username"].ToString();
                //            break;
                //        }
                //        reader.Close();
                    
                //    }
                   
                //}
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public override bool ValidateUser(string username, string password)
        {
            string result = string.Empty;
            bool ret = false;
            try
            {
                UserDataAccessObject userDAO = new UserDataAccessObject();
                result = userDAO.ValidateUser(username, password);
                if (result == string.Empty)
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
               
                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            //return result == 0 // aixo es false , retorno false directament
            return ret;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        protected override byte[] DecryptPassword(byte[] encodedPassword)
        {
            return base.DecryptPassword(encodedPassword);
        }

        protected override byte[] EncryptPassword(byte[] password)
        {
            return base.EncryptPassword(password);
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        private string pApplicationName = "";
        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        private int pMaxInvalidPasswordAttempts = 0;

        public override int MaxInvalidPasswordAttempts
        {
            get { return pMaxInvalidPasswordAttempts; }
        }

        private int pMinRequiredNonAlphanumericCharacters = 0;

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return pMinRequiredNonAlphanumericCharacters; }
        }

        private int pMinRequiredPasswordLength = 0;

        public override int MinRequiredPasswordLength
        {
            get { return pMinRequiredPasswordLength; }
        }

        private int pPasswordAttemptWindow = 0;

        public override int PasswordAttemptWindow
        {
            get { return pPasswordAttemptWindow; }
        }

        private MembershipPasswordFormat pPasswordFormat = MembershipPasswordFormat.Clear;

        public override System.Web.Security.MembershipPasswordFormat PasswordFormat
        {
            get { return pPasswordFormat; }
        }

        private string pPasswordStrengthRegularExpression = "";

        public override string PasswordStrengthRegularExpression
        {
            get { return pPasswordStrengthRegularExpression; }
        }

        private bool pRequiresQuestionAndAnswer = false;

        public override bool RequiresQuestionAndAnswer
        {
            get { return pRequiresQuestionAndAnswer; }
        }

        private bool pRequiresUniqueEmail = false;

        public override bool RequiresUniqueEmail
        {
            get { return pRequiresUniqueEmail; }
        }

        private bool pEnablePasswordReset = false;

        public override bool EnablePasswordReset
        {
            get { return pEnablePasswordReset; }
        }

        private bool pEnablePasswordRetrieval = false;

        public override bool EnablePasswordRetrieval
        {
            get { return pEnablePasswordRetrieval; }
        }


    }
}
