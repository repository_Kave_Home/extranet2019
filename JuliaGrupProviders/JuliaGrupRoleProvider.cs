﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Configuration;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupProviders
{
    class JuliaGrupRoleProvider : RoleProvider
    {
        //string connectionstring = ConfigurationManager.AppSettings["NavisionDBConnectionString"];
        //string connectionstring = "Server=SHP2010;Database=NAVISION;User ID=navision;Password=Raona2010!;";

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        private string pApplicationName = "";

        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] aux;
            try{
                UserDataAccessObject userDAO = new UserDataAccessObject();
                aux = userDAO.GetRolesForUser(username);
                //string[] aux = { "Client", "Representante" };
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return aux;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            try{
                string lower_roleName = roleName.ToLower();

                string[] activeroles = { "client", "clientadmin", "clientbasic", "representant", "clientgrup" };

                //if (lower_roleName == "client" || lower_roleName == "representante")
                if (activeroles.Contains(roleName))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

    }
}
