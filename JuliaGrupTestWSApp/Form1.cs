﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JuliaGrupTestWSApp.JuliaUtilWS;
using System.Net;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebPartPages;
using System.Xml;
using System.Security.Cryptography;
using System.Web.UI.WebControls.WebParts;

namespace JuliaGrupTestWSApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            JuliaUtilWS.JuliaUtilWS juliaWS = new JuliaUtilWS.JuliaUtilWS();
            juliaWS.Url = textBox1.Text;
            byte[] test = {1,0};

            //string output = juliaWS.UploadArticleDocument(test, textBox3.Text, textBox4.Text, textBox5.Text, DocumentTypes.ProductImage, bool.Parse(textBox7.Text), bool.Parse(textBox8.Text));
            string output = juliaWS.UploadArticleDocument(test, textBox3.Text, textBox4.Text, textBox5.Text, DocumentTypes.AsemblyInstructionsDoc, bool.Parse(textBox7.Text), bool.Parse(textBox8.Text));

        }

        private void button2_Click(object sender, EventArgs e)
        {
            JuliaUtilWS.JuliaUtilWS juliaWS = new JuliaUtilWS.JuliaUtilWS();
            string output = juliaWS.CreateArticle(textBox9.Text, textBox10.Text, textBox11.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            JuliaUtilWS.JuliaUtilWS juliaWS = new JuliaUtilWS.JuliaUtilWS();
            byte[] test = { 1, 0 };

            string output = juliaWS.UploadClientDocument (test, textBox3.Text, textBox4.Text, "Client01", DocumentTypes.Bill , bool.Parse(textBox7.Text));
        }
        private void button4_Click(object sender, EventArgs e)
        {
            JuliaUtilWS.JuliaUtilWS juliaWS = new JuliaUtilWS.JuliaUtilWS();
            byte[] test = { 1, 0 };

            string output = juliaWS.UploadClientDocument(test, textBox3.Text, textBox4.Text, "Client01", DocumentTypes.DeliveryNote , bool.Parse(textBox7.Text));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            JuliaUtilWS.JuliaUtilWS juliaWS = new JuliaUtilWS.JuliaUtilWS();
            juliaWS.Url = textBox1.Text;
            juliaWS.Credentials = new NetworkCredential("administrador", "l@f0rm@08", "julia");
            byte[] test = { 1, 0 };

            string output = juliaWS.UploadClientDocument(test, textBox3.Text, textBox4.Text, "6692", DocumentTypes.Document, bool.Parse(textBox7.Text));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(textBox12.Text))
            {
                SPList list = site.GetCatalog(SPListTemplateType.MasterPageCatalog);
                SPListItemCollection items = list.Items;
                List<string> webParts = new List<string>();

                // find the right Page Layout
                using (MD5 md5Hash = MD5.Create())
                {

                    foreach (SPListItem item in items)
                    {
                        if (item.Name.Equals("JuliaGrupNewsPageLayout.aspx",
                          StringComparison.CurrentCultureIgnoreCase))
                        {
                            SPFile file = item.File;
                            // get the Web Part Manager for the Page Layout
                            using (SPLimitedWebPartManager wpm = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
                            {
                                try
                                {
                                    // iterate through all Web Parts and remove duplicates
                                    int i = 0;
                                    while (wpm.WebParts.Count > 0)
                                    {
                                        StringBuilder sb = new StringBuilder();
                                        XmlWriterSettings xws = new XmlWriterSettings();
                                        xws.OmitXmlDeclaration = true;
                                        XmlWriter xw = XmlWriter.Create(sb, xws);
                                        System.Web.UI.WebControls.WebParts.WebPart wp =
                                          wpm.WebParts[0];
                                        wpm.ExportWebPart(wp, xw);
                                        xw.Flush();
                                        string strmd5Hash = GetMd5Hash(md5Hash, sb.ToString());
                                        if (webParts.Contains(strmd5Hash))
                                            wpm.DeleteWebPart(wp);
                                        else
                                            webParts.Add(strmd5Hash);
                                        i++;
                                    }
                                }
                                finally
                                {
                                    wpm.Web.Dispose();
                                }
                            }

                        }
                    }
                }
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(textBox13.Text))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList list = web.Lists["Articles master"];
                    textBox14.Text += "Getting listitems to delete" + System.Environment.NewLine;
                    Application.DoEvents();

                    //StringBuilder deletebuilder = BatchCommand(list);
                    textBox14.Text += "Launch Batch Command" + System.Environment.NewLine;
                    Application.DoEvents();
                    BatchCommand(list, Convert.ToUInt32(numericUpDown1.Value));

                    //textBox14.Text += "DeleteBuilder " + System.Environment.NewLine + deletebuilder.ToString() + System.Environment.NewLine;
                    //Application.DoEvents();
                    //string output = web.ProcessBatchData(deletebuilder.ToString());
                    //textBox14.Text += output + System.Environment.NewLine;
                    Application.DoEvents();

                    //textBox14.Text += "Try in Root web " + System.Environment.NewLine;
                    //Application.DoEvents();
                    //output = site.RootWeb.ProcessBatchData(deletebuilder.ToString());
                    //textBox14.Text += output + System.Environment.NewLine;
                    //Application.DoEvents();


                    textBox14.Text += "End Detele" + System.Environment.NewLine;
                    Application.DoEvents();
                }
            }
        }


        private void BatchCommand(SPList spList, uint batchSize)
        {
            SPQuery query = new SPQuery();
            query.Query = @"<Where>
                                    <Eq>
                                        <FieldRef Name=""ContentType"" />
                                            <Value Type=""Computed"">Product image</Value>
                                    </Eq>
                            </Where>
                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";
            query.ViewAttributes = "Scope=\"RecursiveAll\"";
            query.QueryThrottleMode = SPQueryThrottleOption.Override;
            //query.RowLimit = batchSize;

            SPListItemCollection imagescollection;
            imagescollection = spList.GetItems(query);
            int k = 0;
            textBox14.Text += "All Images = " + imagescollection.Count.ToString() + " items" + System.Environment.NewLine;
            Application.DoEvents();

            while (k < imagescollection.Count)
            {
                // get ids of items to be deleted
                //imagescollection = spList.GetItems(query);
                //if (imagescollection.Count <= 0)
                //    break;
                // make batch command
                StringBuilder deletebuilder = new StringBuilder();
                deletebuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");
                string command = "<Method><SetList Scope=\"Request\">" + spList.ID +
                    "</SetList><SetVar Name=\"ID\">{0}</SetVar><SetVar Name=\"owsfileref\">{1}</SetVar><SetVar Name=\"Cmd\">Delete</SetVar></Method>";
                //int k = 0;
                //int itemCount = imagescollection.Count;            
                for (int j = 0; j < batchSize; j++)
                //foreach (SPListItem item in imagescollection)
                {
                    //imagescollection.Delete(k);
                    if (k >= imagescollection.Count) break;
                    SPListItem item = imagescollection[k];
                    deletebuilder.Append(string.Format(command, item.ID.ToString(), item.File.ServerRelativeUrl));
                    k++;
                }

                deletebuilder.Append("</Batch>");
                textBox14.Text += "It will delete " + k.ToString() + " items" + System.Environment.NewLine;
                Application.DoEvents();

                //// process batch command
                //bool unsafeUpdate = spList.ParentWeb.AllowUnsafeUpdates;
                //try
                //{
                //    spList.ParentWeb.AllowUnsafeUpdates = true;
                //    string output  = spList.ParentWeb.ProcessBatchData(deletebuilder.ToString());
                //    textBox14.Text += output + System.Environment.NewLine;
                //    Application.DoEvents();
                //}
                //finally
                //{
                //    spList.ParentWeb.AllowUnsafeUpdates = unsafeUpdate;
                //}


            }


            //return deletebuilder;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            BatchUpdate(Convert.ToUInt32(numericUpDown1.Value));
            return;

            using (SPSite site = new SPSite(textBox13.Text))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList list = web.Lists["Articles master"];
                    textBox14.Text += "Getting listitems to delete" + System.Environment.NewLine;
                    Application.DoEvents();

                    //StringBuilder deletebuilder = BatchCommand(list);
                    textBox14.Text += "Launch Batch Command" + System.Environment.NewLine;
                    Application.DoEvents();
                    //BatchCommand(list, Convert.ToUInt32(numericUpDown1.Value));

                    Application.DoEvents();


                    var items = from i in list.Items.OfType<SPListItem>()
                                where (
                                    i["ContentType"].ToString() == "Product image" &&
                                    i["ImageWidth"] != null && i["ImageWidth"].ToString() != "225"
                                    && i["ImageHeight"] != null
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"] != null)
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"].ToString() == "True")
                                    && i["ImageHeight"] != null) && (i["ImageWidth"].ToString() != i["ImageHeight"].ToString())
                                select i;

                    int imagesTodelete = items.Count();

                    textBox14.Text += "Se despublicaran = " + imagesTodelete.ToString() + " Product images" + System.Environment.NewLine;

                    foreach (SPListItem it in items) {
                        if (it["ContentType"].ToString() == "Product image")   //Aplicar solo a Product Image
                        {
                            var iWidth = it["ImageWidth"].ToString();
                            var iHeight = it["ImageHeight"].ToString();
                            //var art = it["Article"].ToString();
                            var publish = it["Publish_x0020_on_x0020_client_x0020_portal"].ToString();

                            //try
                            //{
                            //    if (it.File.CheckOutType == SPFile.SPCheckOutType.None)
                            //    {
                            //        it.File.CheckOut();
                            //    }
                            //    else
                            //    {
                            //        it.File.UndoCheckOut();
                            //        it.File.CheckOut();
                            //    }
                            //    it["Publish_x0020_on_x0020_client_x0020_portal"] = false;
                            //    it.Update();
                            //    it.File.CheckIn("To be deleted - horizontal image");
                            //}
                            //catch (Exception ex)
                            //{
                            //    textBox14.Text += "-- Error = " + ex.Message + System.Environment.NewLine;
                            //}
                            Application.DoEvents();
                        }
                    }

                    list.Update();


                    Application.DoEvents();

                    textBox14.Text += "End Detele" + System.Environment.NewLine;
                    Application.DoEvents();
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //Republish documents...
            using (SPSite site = new SPSite(textBox13.Text))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList list = web.Lists["Articles master"];
                    textBox14.Text += "Getting listitems to resolve..." + System.Environment.NewLine;
                    Application.DoEvents();

                    Application.DoEvents();


                    var items = from i in list.Items.OfType<SPListItem>()
                                where (
                                    i["ContentType"].ToString() != "Product image" 
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"] != null)
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"].ToString() == "False")
                                    && (DateTime)i["Modified"] > DateTime.Now.AddDays(-1)
                                    )
                                select i;

                    int imagesTodelete = items.Count();

                    textBox14.Text += "Se resolveran = " + imagesTodelete.ToString() + " despublicaciones erroneas" + System.Environment.NewLine;

                    foreach (SPListItem it in items)
                    {
                            var publish = it["Publish_x0020_on_x0020_client_x0020_portal"].ToString();

                            try
                            {
                                if (it.File.CheckOutType == SPFile.SPCheckOutType.None)
                                {
                                    it.File.CheckOut();
                                }
                                else
                                {
                                    it.File.UndoCheckOut();
                                    it.File.CheckOut();
                                }
                                it["Publish_x0020_on_x0020_client_x0020_portal"] = true;
                                it.Update();
                                it.File.CheckIn("Resolve - Not horizontal image");
                            }
                            catch (Exception ex)
                            {
                                textBox14.Text += "-- Error = " + ex.Message + System.Environment.NewLine;
                            }
                            Application.DoEvents();
                        }
                    list.Update();                


                    Application.DoEvents();

                    textBox14.Text += "End Detele" + System.Environment.NewLine;
                    Application.DoEvents();
                }
            }
        }

        //Unpublish horizontal files
        private void BatchUpdate(uint batchSize)
        {
            textBox14.Text += "Inicio desplublicacion imagenes horizontales - " + DateTime.Now.ToLongTimeString() + System.Environment.NewLine;
            string newValue = "False";
            string updateColumn = "Publish_x0020_on_x0020_client_x0020_portal";
            using (SPSite site = new SPSite(textBox13.Text))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList list = web.Lists["Articles master"];
                    textBox14.Text += "Getting listitems to resolve..." + System.Environment.NewLine;
                    Application.DoEvents();

                    var items = from i in list.Items.OfType<SPListItem>()
                                where (
                                    i["ContentType"].ToString() == "Product image" &&
                                    i["ImageWidth"] != null && i["ImageWidth"].ToString() != "225"
                                    && i["ImageHeight"] != null
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"] != null)
                                    && (i["Publish_x0020_on_x0020_client_x0020_portal"].ToString() == "True")
                                    && i["ImageHeight"] != null) && (i["ImageWidth"].ToString() != i["ImageHeight"].ToString())
                                select i;

                    //items
                    int k = 0;
                    int itemsCount = items.Count();
                    textBox14.Text += "All Images = " + itemsCount + " items" + System.Environment.NewLine;
                    Application.DoEvents();

                    while (k < itemsCount)
                    {
                        // get ids of items to be deleted
                        //imagescollection = spList.GetItems(query);
                        //if (imagescollection.Count <= 0)
                        //    break;
                        // make batch command
                        StringBuilder deletebuilder = new StringBuilder();
                        deletebuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><ows:Batch OnError=\"Continue\">");
                        string command = "<Method ID=\"{0}\">"+
                            "<SetList>" + list.ID + "</SetList>"+
                            "<SetVar Name=\"Cmd\">Save</SetVar>" +
                            "<SetVar Name=\"ID\">{0}</SetVar>"+
                            "<SetVar Name=\"owsfileref\">{1}</SetVar>" +
                            "<SetVar Name=\"urn:schemas-microsoft-com:office:office#{2}\">{3}</SetVar>" +
                            "</Method>";
                        
                        //int k = 0;
                        //int itemCount = imagescollection.Count;            
                        for (int j = 0; j < batchSize; j++)
                        //foreach (SPListItem item in imagescollection)
                        {
                            //imagescollection.Delete(k);
                            if (k >= itemsCount) break;
                            SPListItem item = items.ElementAt(k);
                            deletebuilder.Append(string.Format(command, item.ID.ToString(), item.File.ServerRelativeUrl, updateColumn,newValue));
                            k++;
                            Application.DoEvents();
                        }

                        deletebuilder.Append("</Batch>");
                        textBox14.Text += "It will update " + k.ToString() + " items" + System.Environment.NewLine;
                        Application.DoEvents();

                        // process batch command
                        bool unsafeUpdate = list.ParentWeb.AllowUnsafeUpdates;
                        try
                        {
                            list.ParentWeb.AllowUnsafeUpdates = true;
                            string output = list.ParentWeb.ProcessBatchData(deletebuilder.ToString());
                            textBox14.Text += output + System.Environment.NewLine;
                            Application.DoEvents();
                        }
                        finally
                        {
                            list.ParentWeb.AllowUnsafeUpdates = unsafeUpdate;
                        }


                    }
                }
            }



            textBox14.Text += "Fin desplublicacion imagenes horizontales - " + DateTime.Now.ToLongTimeString() + System.Environment.NewLine;

            //return deletebuilder;
        }

    }
}
