﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JuliaGrupTimerConfig.aspx.cs"
    Inherits="JuliaGrupTimers.Layouts.JuliaGrupTimers.JuliaGrupTimerConfig" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="MainContent" runat="server" ContentPlaceHolderID=PlaceHolderMain>
    <asp:DropDownList ID="dropdown" runat="server"></asp:DropDownList> 
    <asp:Panel ID="Articles" runat="server">
        <asp:Label ID="lblArticleOriginListName" runat="server" AssociatedControlID="ArticleOriginListName" Text="Article Origin List Name">
        </asp:Label><asp:TextBox ID="ArticleOriginListName" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblArticleDestinationListName" runat="server" AssociatedControlID="ArticleDestinationListName" Text="Article Destination List Name">
        </asp:Label><asp:TextBox ID="ArticleDestinationListName" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblArticleOriginKeyField" runat="server" AssociatedControlID="ArticleOriginKeyField" Text="Article Origin Key Field">
        </asp:Label><asp:TextBox ID="ArticleOriginKeyField" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblArticleSubsitePath" runat="server" AssociatedControlID="ArticleSubsitePath" Text="Article Subsite Path">
        </asp:Label><asp:TextBox ID="ArticleSubsitePath" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblArticleSubsiteList" runat="server" AssociatedControlID="ArticleSubsiteList" Text="Article Document library to create Document Sets">
        </asp:Label><asp:TextBox ID="ArticleSubsiteList" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblArticleDocumentSetContentType" runat="server" AssociatedControlID="ArticleDocumentSetContentType" Text="Article Document Sets Content Type Name">
        </asp:Label><asp:TextBox ID="ArticleDocumentSetContentType" runat="server"></asp:TextBox><br />        
        
        <asp:Button ID="saveButton" runat="server"/>
    </asp:Panel>
</asp:Content>
