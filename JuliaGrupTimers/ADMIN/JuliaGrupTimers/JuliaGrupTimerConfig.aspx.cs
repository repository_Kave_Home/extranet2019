﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;
using Microsoft.SharePoint.Administration;
using System.Linq;
using System.Web.UI.WebControls;

namespace JuliaGrupTimers.Layouts.JuliaGrupTimers
{
    public partial class JuliaGrupTimerConfig : LayoutsPageBase
    {
        private string JOB_NAME = "Julia Grup NAV Sync timer";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.saveButton.Click += new EventHandler(timerButton_Click);

            List<SPJobDefinition> jobCollection = this.GetTimerJobsByName(this.JOB_NAME);
            foreach (SPJobDefinition job in jobCollection)
            {
                dropdown.Items.Add(job.Name + " " + job.WebApplication);
            }

            this.dropdown.SelectedIndexChanged += new EventHandler(dropdown_SelectedIndexChanged);
            
        }

        void dropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = ((DropDownList)sender).SelectedIndex;
            List<SPJobDefinition> jobCollection = this.GetTimerJobsByName(this.JOB_NAME);

            SPJobDefinition job = jobCollection[idx];
            this.ArticleOriginListName.Text = (string)job.Properties["ArticleOriginListName"] != null ? (string)job.Properties["ArticleOriginListName"] : "bcs_Articles";
            this.ArticleDestinationListName.Text = (string)job.Properties["ArticleDestinationListName"] != null ? (string)job.Properties["ArticleDestinationListName"] : "Articles";
            this.ArticleOriginKeyField.Text = (string)job.Properties["ArticleOriginKeyField"] != null ? (string)job.Properties["ArticleOriginKeyField"] : "Articulo_Version";
            this.ArticleSubsitePath.Text = (string)job.Properties["ArticleSubsitePath"] != null ? (string)job.Properties["ArticleSubsitePath"] : "Articles";
            this.ArticleSubsiteList.Text = (string)job.Properties["ArticleSubsiteList"] != null ? (string)job.Properties["ArticleSubsiteList"] : "Articles master";
            this.ArticleDocumentSetContentType.Text = (string)job.Properties["ArticleDocumentSetContentType"] != null ? (string)job.Properties["ArticleDocumentSetContentType"] : "Articles Document Set";
        }

        void timerButton_Click(object sender, EventArgs e)
        {
            int idx = this.dropdown.SelectedIndex;
            List<SPJobDefinition> jobCollection = this.GetTimerJobsByName(this.JOB_NAME);

            SPJobDefinition job = jobCollection[idx];

            job.Properties["ArticleOriginListName"] = this.ArticleOriginListName.Text;
            job.Properties["ArticleDestinationListName"] = this.ArticleDestinationListName.Text;
            job.Properties["ArticleOriginKeyField"] = this.ArticleOriginKeyField.Text;
            job.Properties["ArticleSubsitePath"] = this.ArticleSubsitePath.Text;
            job.Properties["ArticleSubsiteList"] = this.ArticleSubsiteList.Text;
            job.Properties["ArticleDocumentSetContentType"] = this.ArticleDocumentSetContentType.Text;
        }

        private List<SPJobDefinition> GetTimerJobsByName(string displayName)
        {
            List<SPJobDefinition> AllJobs = new List<SPJobDefinition>();

            // For all servers in the farm (the servers could be different).
            foreach (SPServer server in SPFarm.Local.Servers)
            {
                // For each service instance on the server.
                foreach (SPServiceInstance svc in server.ServiceInstances)
                {
                    if (svc.Service.JobDefinitions.Count > 0)
                    {
                        // If it is a Web Service, then get the Web application from 
                        // the Web Service entity.
                        if (svc.Service is SPWebService)
                        {
                            SPWebService websvc = (SPWebService)svc.Service;
                            AllJobs.AddRange(from webapp in websvc.WebApplications
                                             from def in webapp.JobDefinitions
                                             where def.DisplayName.ToLower() == displayName.ToLower()
                                             select def);
                        }
                        else
                        {
                            //Otherwise Get it directly from the Service
                            AllJobs.AddRange(svc.Service.JobDefinitions.Where(def =>
                                             def.DisplayName.ToLower() == displayName.ToLower()));
                        }
                    }
                }
            }

            return AllJobs;
        }
    }

}
