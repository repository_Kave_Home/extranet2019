using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Administration;

namespace JuliaGrupTimers.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("269877f6-ad4e-415e-9a42-60dc53ead2a0")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        private const string ARTICLES_JOB_NAME = "Julia Grup NAV Articles Sync timer";
        private const string FAMILIES_JOB_NAME = "Julia Grup NAV Families Sync timer";
        private const string CATALOG_JOB_NAME = "Julia Grup NAV Catalog Sync timer";
        private const string PROGRAM_JOB_NAME = "Julia Grup NAV Program Sync timer";

        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWebApplication webApp = properties.Feature.Parent as SPWebApplication;
            foreach (SPJobDefinition job in webApp.JobDefinitions)
            {
                if (job.Name == ARTICLES_JOB_NAME)
                    job.Delete();
                if (job.Name == FAMILIES_JOB_NAME)
                    job.Delete();
                if (job.Name == CATALOG_JOB_NAME)
                    job.Delete();
                if (job.Name == PROGRAM_JOB_NAME)
                    job.Delete();
            }

            SPNavIntegrationJob_Articles timerService = new SPNavIntegrationJob_Articles(webApp, ARTICLES_JOB_NAME);
            timerService.Schedule = SPSchedule.FromString("every 15 minutes");
            timerService.Update();

            SPNavIntegrationJob_Families timerFamilies = new SPNavIntegrationJob_Families(webApp, FAMILIES_JOB_NAME);
            timerFamilies.Schedule = SPSchedule.FromString("every 30 minutes");
            timerFamilies.Update();

            SPNavIntegrationJob_Catalogs timerCatalogs = new SPNavIntegrationJob_Catalogs(webApp, CATALOG_JOB_NAME);
            timerCatalogs.Schedule = SPSchedule.FromString("every 30 minutes");
            timerCatalogs.Update();

            SPNavIntegrationJob_Program timerPrograms = new SPNavIntegrationJob_Program(webApp, PROGRAM_JOB_NAME);
            timerPrograms.Schedule = SPSchedule.FromString("every 30 minutes");
            timerPrograms.Update();
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SPWebApplication webApp = properties.Feature.Parent as SPWebApplication;
            foreach (SPJobDefinition job in webApp.JobDefinitions)
            {
                if (job.Name == ARTICLES_JOB_NAME)
                    job.Delete();
                if (job.Name == FAMILIES_JOB_NAME)
                    job.Delete();
                if (job.Name == CATALOG_JOB_NAME)
                    job.Delete();
                if (job.Name == PROGRAM_JOB_NAME)
                    job.Delete();
            }
        }

        
        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
