﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.Office.DocumentManagement.DocumentSets;
using System.Collections;
using Microsoft.SharePoint.Taxonomy;

namespace JuliaGrupTimers
{
    class SPNavIntegrationJob_Articles : SPJobDefinition
    {
        private string ArticleOriginList = "bcs_Articles";
        private string ArticleDestinationList = "Articles";
        private string ArticleOriginKeyField = "articulo";
        private string ArticleSubsite = "Articles";
        private string ArticleSubsiteServerRelativeUrl = "/GestionDocumental/";
        private string ArticleDocumentLibraryName = "Articles master";
        private string ArticleDocumentSetContentType = "Articles Document Set";
        private Dictionary<string, string> ArticleListFieldsMap;

        private Dictionary<string, TaxonomyFieldValue> FamiliesDictionary;
        private Dictionary<string, TaxonomyFieldValueCollection> CatalogDictionary;
        private Dictionary<string, TaxonomyFieldValue> ProgramDictionary;

        // Default constructor
        public SPNavIntegrationJob_Articles()
        {
            this.BuildArticleMapping();
        }


        /// <summary>
        /// Function wich builds the mapping between the bcs original document and the 
        /// destination list for Articles.
        /// </summary>
        private void BuildArticleMapping()
        {
            this.ArticleListFieldsMap = new Dictionary<string, string>();
            this.ArticleListFieldsMap.Add("articulo", "Article_x0020_code1");
            //this.ArticleListFieldsMap.Add("Version", "Article version");
            //this.ArticleListFieldsMap.Add("Codigo_Proveedor", "Provider code");
            this.ArticleListFieldsMap.Add("Descripcion", "Article Description");
            this.ArticleListFieldsMap.Add("familia", "Article_x0020_Family0");
            this.ArticleListFieldsMap.Add("subfamilia", "Article Subfamily");
            this.ArticleListFieldsMap.Add("Programa", "Article_x0020_Program0");
            this.ArticleListFieldsMap.Add("Catalogo", "Article_x0020_Catalog");
            this.ArticleListFieldsMap.Add("versionActiva", "Article Active version");
        }

        // Constructor with a controlled name. 
        // This will be handy on deactivate
        public SPNavIntegrationJob_Articles(SPWebApplication webApp, string JOB_NAME)
            : base(JOB_NAME, webApp, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JOB_NAME;
        }

        // This is what happens when the timer service executes.
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                using (SPSite site = this.WebApplication.Sites[0])
                {
                    FamiliesDictionary = new Dictionary<string, TaxonomyFieldValue>();
                    CatalogDictionary = new Dictionary<string, TaxonomyFieldValueCollection>();
                    ProgramDictionary = new Dictionary<string, TaxonomyFieldValue>();

                    this.SyncArticles(site.ID);
                    this.CreateMissingArticleDocumentsets(site.ID);
                }
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
        }

        /// <summary>
        /// Function which synchronizes the original bcs list from sQL Server with the
        /// list of Articles.
        /// </summary>
        /// <param name="guidSites"></param>
        private void SyncArticles(Guid guidSites)
        {
            try
            {

                using (SPSite site = new SPSite(guidSites))
                {
                    site.AllowUnsafeUpdates = true;
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPServiceContextScope scope = new Microsoft.SharePoint.SPServiceContextScope(SPServiceContext.GetContext(site)))
                        {
                            SPListItemCollection itemsOrigin = site.RootWeb.Lists[this.ArticleOriginList].Items;
                            SPList destinationList = site.RootWeb.Lists[this.ArticleDestinationList];
                            SPListItemCollection itemsDest = site.RootWeb.Lists[this.ArticleDestinationList].Items;
                            string camlQuery = "<Where><Eq><FieldRef Name=\"Article_x0020_code1\" /><Value Type=\"Text\">{0}</Value></Eq></Where>";
                            string viewFields = "<FieldRef Name=\"" + this.ArticleListFieldsMap[this.ArticleOriginKeyField]+ "\" />";

                            int idx = 1;
                            foreach (SPListItem item in itemsOrigin)
                            {
                                string itemDestField = this.ArticleListFieldsMap[this.ArticleOriginKeyField];
                                string keyCodeValue = (item[this.ArticleOriginKeyField] != null) ? item[this.ArticleOriginKeyField].ToString() : String.Empty;
                                
                                SPQuery query = new SPQuery();
                                query.Query = String.Format(camlQuery, keyCodeValue);
                                query.ViewFields = viewFields;
                                query.RowLimit = 1;
                                

                                SPListItemCollection destItem = destinationList.GetItems(query);
                                
                                if (destItem.Count > 0)
                                {
                                    //ja tenim l'element a updatar, simplement updatem en cas que sigui necessari
                                    //nomes tindrem un element perque el camp es clau!
                                    //hem d'agafar el item sencer, ens hem portat nomes un camppero en tenim l'id
                                    SPListItem destination = destinationList.GetItemById(destItem[0].ID);                                    
                                    this.CopyOrUptadeArticleItem(destination, item);
                                }
                                else
                                {

                                    //nou item a la llista
                                    //hem de crear l'item
                                    SPListItem newitem = itemsDest.Add();
                                    this.CopyOrUptadeArticleItem(newitem, item);
                                }
                                base.UpdateProgress(((idx * 100) / itemsOrigin.Count)/2);
                                idx++;
                            }
                        }
                    });
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
        }

        /// <summary>
        /// function which compares two list items and updates the destination one if 
        /// the vlaue is diferent
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="originalItem"></param>
        private void CopyOrUptadeArticleItem(SPListItem destination, SPListItem originalItem)
        {
            try
            {
                bool modifications = false;
                //emplenem els diferents valors buclant pel diccionari de propietats
                foreach (KeyValuePair<string, string> properties in this.ArticleListFieldsMap)
                {
                    try
                    {
                        if ((properties.Key == "familia" || properties.Key == "Programa" || properties.Key == "Catalogo") && originalItem[properties.Key] != null && originalItem[properties.Key] != String.Empty)
                        {
                            //son camps managed metadata, es tracten diferent                            
                            TaxonomySession session = new TaxonomySession(destination.Web.Site);
                            TermStoreCollection termstores = session.TermStores;
                            TermStore store = termstores.Where(a => a.Name == TimerUtils.TERMSTORENAME).First();
                            Group group = store.Groups.Where(g => g.Name == TimerUtils.TERMSTOREGROUPNAME).First();

                            if (properties.Key == "familia")
                            {
                                if (originalItem["subfamilia"] != null && originalItem["subfamilia"] != string.Empty)
                                {
                                    TaxonomyFieldValue value = null;

                                    if (!FamiliesDictionary.ContainsKey(originalItem[properties.Key] + "_" + originalItem["subfamilia"]))
                                    {
                                        TermSet famTermSet = group.TermSets.Where(t => t.Name == TimerUtils.FAMILYTERMSET).FirstOrDefault();
                                        //només una familia
                                        Term familia = famTermSet.Terms.Where(t => t.GetDefaultLabel(3082) == originalItem[properties.Key].ToString()).FirstOrDefault();
                                        Term[] terms = familia.Terms.Where(t => t.GetDefaultLabel(3082) == originalItem["subfamilia"].ToString()).ToArray();


                                        if (terms.Count() == 1)
                                        {
                                            value = new TaxonomyFieldValue(destination.Fields.GetFieldByInternalName(properties.Value));
                                            value.TermGuid = terms[0].Id.ToString();
                                            value.Label = terms[0].Name;

                                            FamiliesDictionary.Add(originalItem[properties.Key] + "_" + originalItem["subfamilia"], value);
                                        }
                                    }
                                    else
                                    {
                                        value = FamiliesDictionary[originalItem[properties.Key] + "_" + originalItem["subfamilia"]];
                                    }

                                    if (destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] != value)
                                    {
                                        modifications = true;
                                        destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] = value;
                                    }
                                }
                            }

                            if (properties.Key == "Programa")
                            {
                                TaxonomyFieldValue value = null;
                                string originalKey = originalItem[properties.Key] != null ? originalItem[properties.Key].ToString() : "";

                                if (!this.ProgramDictionary.ContainsKey(originalKey))
                                {
                                    TermSet programTS = group.TermSets.Where(t => t.Name == TimerUtils.PROGRAMTERMSET).First();
                                    Term[] terms = programTS.Terms.Where(t => t.GetDefaultLabel(3082) == originalItem[properties.Key].ToString()).ToArray();

                                    if (terms.Count() == 1)
                                    {
                                        value = new TaxonomyFieldValue(destination.Fields.GetFieldByInternalName(properties.Value));
                                        value.TermGuid = terms[0].Id.ToString();
                                        value.Label = terms[0].Name;

                                        this.ProgramDictionary.Add(originalKey, value);
                                    }
                                }
                                else
                                {
                                    value = this.ProgramDictionary[originalKey];
                                }
                                                               

                                if (destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] != value)
                                {
                                    modifications = true;
                                    destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] = value;
                                }  
                            }

                            if (properties.Key == "Catalogo")
                            {
                                string originalkey = originalItem[properties.Key] != null ? originalItem[properties.Key].ToString() : "";
                                TaxonomyFieldValueCollection colValue = new TaxonomyFieldValueCollection(destination.Fields.GetFieldByInternalName(properties.Value));
                                
                                if (!this.CatalogDictionary.ContainsKey(originalkey))
                                {
                                    TermSet programTS = group.TermSets.Where(t => t.Name == TimerUtils.CATALOGSTERMSET).First();
                                    Term[] terms = programTS.Terms.Where(t => t.GetDefaultLabel(3082) == originalItem[properties.Key].ToString()).ToArray();

                                    

                                    foreach (Term t in terms)
                                    {
                                        TaxonomyFieldValue value = new TaxonomyFieldValue(destination.Fields.GetFieldByInternalName(properties.Value));
                                        value.TermGuid = terms[0].Id.ToString();
                                        value.Label = terms[0].Name;
                                        colValue.Add(value);

                                        this.CatalogDictionary.Add(originalkey, colValue);
                                    }
                                }
                                else
                                {
                                    colValue = this.CatalogDictionary[originalkey];
                                }

                                if (destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] != colValue)
                                {
                                    modifications = true;
                                    destination[destination.Fields.GetFieldByInternalName(properties.Value).Id] = colValue;
                                }                              
                            }

                        }
                        else if(properties.Key != "subfamilia")
                        {
                            //campo normal
                            if (destination[properties.Value] == null || originalItem[properties.Key] == null)
                            {
                                destination[properties.Value] = originalItem[properties.Key];
                                modifications = true;
                            }
                            else if (destination[properties.Value].ToString() != originalItem[properties.Key].ToString())
                            {
                                modifications = true;
                                destination[properties.Value] = originalItem[properties.Key];
                            }
                        }
                        
                    }
                    catch (Exception e)
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
                    }
                }

                //si hem modificat algun
                if(modifications)
                    destination.Update();
                
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
        }

        private void CreateMissingArticleDocumentsets(Guid siteId)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(siteId))
                    {
                        var context = SPServiceContext.GetContext(site);

                        using (var scope = new SPServiceContextScope(context))
                        {
                            using (SPWeb rootWeb = site.RootWeb)
                            {
                                using (SPWeb web = site.OpenWeb(this.ArticleSubsiteServerRelativeUrl))
                                {
                                    web.AllowUnsafeUpdates = true;
                                    SPList list = web.Lists[ArticleDocumentLibraryName];
                                    SPListItemCollection articlesOriginalList = rootWeb.Lists[this.ArticleDestinationList].Items;

                                    SPListItemCollection articles = list.Items;
                                    string strQuery = "<Where><Eq><FieldRef Name=\"Title\" /><Value Type=\"Text\">{0}</Value></Eq></Where>";
                                    string fieldref = "<FieldRef Name=\"Title\" />";


                                    int idx = 1;
                                    foreach (SPListItem item in articlesOriginalList)
                                    {
                                        //check if exists a document set with this article code

                                        SPQuery query = new SPQuery();
                                        query.Query = String.Format(strQuery, item[this.ArticleListFieldsMap[ArticleOriginKeyField]].ToString());
                                        query.ViewFields = fieldref;
                                        query.RowLimit = 1;

                                        SPListItemCollection documentSet = list.GetItems(query);
                                        
                                        if (documentSet.Count == 0)
                                        {
                                            try
                                            {
                                                //create the new document set 
                                                Hashtable DocumentSetProperties = this.BuildArticleDocumentSetProperties(item);
                                                string title = item[this.ArticleListFieldsMap[ArticleOriginKeyField]] != null ? item[this.ArticleListFieldsMap[ArticleOriginKeyField]].ToString() : string.Empty;
                                                DocumentSet ds = DocumentSet.Create(list.RootFolder, title, list.ContentTypes[ArticleDocumentSetContentType].Id, DocumentSetProperties);                                                

                                                //set the article lookup value
                                                ds.Item[ds.Item.Fields["Article"].Id] = DocumentSetProperties["Article"];
                                                ds.Item.Update();
                                                
                                            }
                                            catch (Exception e)
                                            {
                                                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
                                            }
                                        }
                                        else
                                        {
                                            //document set already created
                                            //we just only have 1 item in the listitemcollecion
                                            //but we need all the fields
                                            SPListItem ds = list.GetItemById(documentSet[0].ID);

                                            //only can change the product description
                                            string origin = item["Article Description"] != null ? item["Article Description"].ToString() : String.Empty;
                                            string dest = ds["Description"] != null ? ds["Description"].ToString() : String.Empty;

                                            if (origin != dest)
                                            {
                                                ds["Description"] = origin;
                                                ds.Update();
                                            }
                                        }
                                        base.UpdateProgress((((idx * 100) / articlesOriginalList.Count) / 2) + 50);
                                        idx++;
                                    }
                                    web.AllowUnsafeUpdates = false;
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
        }

        private Hashtable BuildArticleDocumentSetProperties(SPListItem item)
        {
            Hashtable documentSetProperties = new Hashtable();
            try
            {
                documentSetProperties.Add("Title", item["Article code"]);
                documentSetProperties.Add("DocumentSetDescription", item["Article Description"]);
                string codeVersion = item["Article code"] != null ? item["Article code"].ToString() : string.Empty;
                SPFieldLookupValue lookup = new SPFieldLookupValue(item.ID, codeVersion);
                documentSetProperties.Add("Article", lookup);
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Articles", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
            return documentSetProperties;
        }
    }
}
