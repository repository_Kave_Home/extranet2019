﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.Data.SqlClient;
using Microsoft.SharePoint.Taxonomy;

namespace JuliaGrupTimers
{
    class SPNavIntegrationJob_Catalogs : SPJobDefinition
    {
        public SPNavIntegrationJob_Catalogs(SPWebApplication webApp, string JOB_NAME)
            : base(JOB_NAME, webApp, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JOB_NAME;
        }

        public SPNavIntegrationJob_Catalogs()
        {
            }
        public override void Execute(Guid targetInstanceId)
        {
            using (SPSite site = this.WebApplication.Sites[0])
            {
                List<string> catalogsList = this.GetCatalogsList(site.ID);
                this.UpdateCatalogs(catalogsList, site.ID);
            }
        }

        private void UpdateCatalogs(List<string> catalogsList, Guid siteId)
        {
            using (SPSite site = new SPSite(siteId))
            {
                TaxonomySession session = new TaxonomySession(site);
                TermStoreCollection termstores = session.TermStores;
                TermStore store = termstores.Where(a => a.Name == TimerUtils.TERMSTORENAME).First();
                Group group = store.Groups.Where(g => g.Name == TimerUtils.TERMSTOREGROUPNAME).First();
                TermSet catalogTermSet = group.TermSets.Where(t => t.Name == TimerUtils.CATALOGSTERMSET).First();

                int i = 1;
                foreach (string catalog in catalogsList)
                {
                    if (catalogTermSet.Terms.Where(t => t.GetDefaultLabel(3082) == catalog).Count() == 0 && catalog != string.Empty)
                    {
                        catalogTermSet.CreateTerm(catalog, 3082);
                        store.CommitAll();
                    }
                    base.UpdateProgress((int)(i * 100 / catalogsList.Count()));
                    i++;
                }
            }
        }

        private List<string> GetCatalogsList(Guid siteId)
        {
            List<string> results = new List<string>();

            string[] credentials = TimerUtils.GetSecureStoreCredentials(siteId, TimerUtils.SECURESTORETARGETID);
            string connectionstring = "Server=" + TimerUtils.SQLSERVERNAME + ";Database=" + TimerUtils.DATABASENAME + ";User ID=" + credentials[0] + ";Password=" + credentials[1] + ";Trusted_Connection=False;";
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                conn.Open();
                SqlCommand comm = new SqlCommand("select distinct coalesce(catalogo, '-') as catalogo from dbo.V_Articulos", conn);

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    results.Add(reader.GetString(0));
                }

                reader.Close();
                reader.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Catalogs", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }

            return results;
        }
    }
}
