﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using System.Linq;
using Microsoft.SharePoint.BusinessData.SharedService;
using Microsoft.BusinessData.MetadataModel;
using Microsoft.BusinessData.MetadataModel.Collections;
using Microsoft.BusinessData.Runtime;
using Microsoft.Office.Server.ApplicationRegistry.MetadataModel;
using System.Data;
using Microsoft.BusinessData.Infrastructure.SecureStore;
using Microsoft.Office.SecureStoreService.Server;
using System.Security;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace JuliaGrupTimers
{
    class SPNavIntegrationJob_Families : SPJobDefinition
    {
        // Constructor with a controlled name. 
        // This will be handy on deactivate
        public SPNavIntegrationJob_Families(SPWebApplication webApp, string JOB_NAME)
            : base(JOB_NAME, webApp, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JOB_NAME;
        }

        // Default constructor
        public SPNavIntegrationJob_Families()
        {
        }

        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                using (SPSite site = this.WebApplication.Sites[0])
                {
                    this.SyncFamilies(site.ID);
                }
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Families", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }
        }

        private void SyncFamilies(Guid siteId)
        {
            DataTable families = this.GetFamiliesList(siteId);
            base.UpdateProgress(10);
            this.UpdateSubfamilia(families,siteId);

        }

        private DataTable GetFamiliesList(Guid siteId)
        {
            DataTable result = new DataTable();
            result.Columns.Add("FAMILIA");
            result.Columns.Add("SUBFAMILIA");

            string[] credentials = TimerUtils.GetSecureStoreCredentials(siteId, TimerUtils.SECURESTORETARGETID);
            string connectionstring = "Server=" + TimerUtils.SQLSERVERNAME + ";Database=" + TimerUtils.DATABASENAME + ";User ID=" + credentials[0] + ";Password=" + credentials[1] + ";Trusted_Connection=False;";
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                conn.Open();
                SqlCommand comm = new SqlCommand("select distinct coalesce(familia,'-'),coalesce(subfamilia,'-') from [dbo].[V_Familia]", conn);

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    DataRow row = result.NewRow();
                    row["FAMILIA"] = reader.GetString(0);
                    row["SUBFAMILIA"] = reader.GetString(1);
                    result.Rows.Add(row);
                }

                reader.Close();
                reader.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Families", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }

            return result;
        }

        private void UpdateSubfamilia(DataTable families, Guid siteid)
        {
            using (SPSite site = new SPSite(siteid))
            {
                TaxonomySession session = new TaxonomySession(site);
                TermStoreCollection termstores = session.TermStores;
                TermStore store = termstores.Where(a => a.Name == TimerUtils.TERMSTORENAME).First();
                Group group = store.Groups.Where(g => g.Name == TimerUtils.TERMSTOREGROUPNAME).First();
                TermSet famTermSet = group.TermSets.Where(t => t.Name == TimerUtils.FAMILYTERMSET).First();
                
                string anteriorFamilia = string.Empty;

                var distinctFamilies = (from r in families.Rows.OfType<DataRow>()
                                        select r["FAMILIA"]).Distinct();
                int i=1;
                foreach (string familia in distinctFamilies)
                {
                    try
                    {
                        Term fam = null;
                        if (famTermSet.Terms.Where(t => t.GetDefaultLabel(3082) == familia && t.Parent == null).Count() == 0)
                        {
                            fam = famTermSet.CreateTerm(familia, 3082);
                        }
                        else
                        {
                            fam = famTermSet.Terms.Where(t => t.GetDefaultLabel(3082) == familia && t.Parent == null).First();
                        }

                        var distinctSubFamilies = (from r in families.Rows.OfType<DataRow>()
                                                   where r["FAMILIA"].ToString() == familia
                                                   select r["SUBFAMILIA"]).Distinct();

                        foreach (string subfamilia in distinctSubFamilies)
                        {
                            if (fam.Terms.Where(t => t.GetDefaultLabel(3082) == subfamilia).Count() == 0 && subfamilia != string.Empty)
                            {
                                fam.CreateTerm(subfamilia, 3082);
                            }
                            store.CommitAll();
                        }
                        store.CommitAll();
                        base.UpdateProgress((int)(i * 100 / distinctFamilies.Count()));
                        i++;
                    } catch (Exception e)
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Families", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
                    }
                }
                
            }
        }


      


    }
}
