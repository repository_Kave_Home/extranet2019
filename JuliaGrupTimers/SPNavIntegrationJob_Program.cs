﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.Data.SqlClient;
using Microsoft.SharePoint.Taxonomy;

namespace JuliaGrupTimers
{
    class SPNavIntegrationJob_Program: SPJobDefinition
    {
        public SPNavIntegrationJob_Program(SPWebApplication webApp, string JOB_NAME)
            : base(JOB_NAME, webApp, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JOB_NAME;
        }

        public SPNavIntegrationJob_Program()
        {
            }

        public override void Execute(Guid targetInstanceId)
        {
            using (SPSite site = this.WebApplication.Sites[0])
            {
                List<string> programList = this.GetProgramList(site.ID);
                this.UpdateProgram(programList, site.ID);
            }
        }

        private void UpdateProgram(List<string> programList, Guid siteId)
        {
            using (SPSite site = new SPSite(siteId))
            {
                TaxonomySession session = new TaxonomySession(site);
                TermStoreCollection termstores = session.TermStores;
                TermStore store = termstores.Where(a => a.Name == TimerUtils.TERMSTORENAME).First();
                Group group = store.Groups.Where(g => g.Name == TimerUtils.TERMSTOREGROUPNAME).First();
                TermSet programTermSet = group.TermSets.Where(t => t.Name == TimerUtils.PROGRAMTERMSET).First();

                int i = 1;
                foreach (string program in programList)
                {
                    if (programTermSet.Terms.Where(t => t.GetDefaultLabel(3082) == program).Count() == 0 && program != string.Empty)
                    {
                        programTermSet.CreateTerm(program, 3082);
                        store.CommitAll();
                    }
                    base.UpdateProgress((int)(i * 100 / programList.Count()));
                    i++;
                }
            }
        }

        private List<string> GetProgramList(Guid siteId)
        {
            List<string> results = new List<string>();

            string[] credentials = TimerUtils.GetSecureStoreCredentials(siteId, TimerUtils.SECURESTORETARGETID);
            string connectionstring = "Server=" + TimerUtils.SQLSERVERNAME + ";Database=" + TimerUtils.DATABASENAME + ";User ID=" + credentials[0] + ";Password=" + credentials[1] + ";Trusted_Connection=False;";
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                conn.Open();
                SqlCommand comm = new SqlCommand("select distinct coalesce(Programa, '-') as programa from dbo.V_Articulos", conn);

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    results.Add(reader.GetString(0));
                }

                reader.Close();
                reader.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Program", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
            }

            return results;
        }
    }
}
