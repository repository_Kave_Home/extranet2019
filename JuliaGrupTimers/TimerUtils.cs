﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.BusinessData.Infrastructure.SecureStore;
using Microsoft.Office.SecureStoreService.Server;
using System.Security;
using Microsoft.SharePoint.Administration;
using System.Runtime.InteropServices;

namespace JuliaGrupTimers
{
    public class TimerUtils
    {
        /*dev*/
        //public static string SQLSERVERNAME = "PEDAGOGIA";
        //public static string DATABASENAME = "NAVISION";
        //public static string TERMSTORENAME = "Managed Metadata Service";
        /*prod*/
        public static string SQLSERVERNAME = "srvsql2008";
        public static string DATABASENAME = "DESARROLLO";
        public static string TERMSTORENAME = "Servicio de metadatos administrados";
        /*ALL*/
        public static string SECURESTORETARGETID = "NAVISION";       
        
        public static string TERMSTOREGROUPNAME = "Julià Grup";
        public static string FAMILYTERMSET = "Family";
        public static string CATALOGSTERMSET = "Catalog";
        public static string PROGRAMTERMSET = "Program";

        public static string[] GetSecureStoreCredentials(Guid siteId, string credentialStoreUtilsId)
        {
            using (SPSite site = new SPSite(siteId))
            {
                // Get the default Secure Store Service provider.
                ISecureStoreProvider provider = SecureStoreProviderFactory.Create();
                if (provider == null)
                {
                    throw new InvalidOperationException("Unable to get an ISecureStoreProvider");
                }

                ISecureStoreServiceContext providerContext = provider as ISecureStoreServiceContext;
                providerContext.Context = SPServiceContext.GetContext(site);

                // Create the variables to hold the credentials.
                string userName = null;
                string password = null;
                string pin = null;
                
                try
                {
                    using (SecureStoreCredentialCollection creds = provider.GetCredentials(credentialStoreUtilsId))
                    {
                        if (creds != null)
                        {
                            foreach (SecureStoreCredential cred in creds)
                            {
                                if (cred == null)
                                {
                                    continue;
                                }

                                switch (cred.CredentialType)
                                {
                                    case SecureStoreCredentialType.UserName:
                                        if (userName == null)
                                        {
                                            userName = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                    case SecureStoreCredentialType.WindowsUserName:
                                        if (userName == null)
                                        {
                                            userName = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;

                                    case SecureStoreCredentialType.Password:
                                        if (password == null)
                                        {
                                            password = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                    case SecureStoreCredentialType.WindowsPassword:
                                        if (password == null)
                                        {
                                            password = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    if (userName == null || password == null)
                    {
                        throw new InvalidOperationException("Unable to get the credentials");
                    }

                    string[] result = { userName, password };
                    return result;
                }
                catch (SecureStoreException e)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Families", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
                }
                string[] aux = { };
                return aux;
            }
        }

        private static string GetStringFromSecureString(SecureString secStr)
        {
            if (secStr == null)
            {
                return null;
            }

            IntPtr pPlainText = IntPtr.Zero;
            try
            {
                pPlainText = Marshal.SecureStringToBSTR(secStr);
                return Marshal.PtrToStringBSTR(pPlainText);
            }
            finally
            {
                if (pPlainText != IntPtr.Zero)
                {
                    Marshal.FreeBSTR(pPlainText);
                }
            }
        }
    }
}
