﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Albaran
    {
        #region Private properties
        private string numAlbaran;
        private string numCliente;
        private string numIncidencia;
        private string dataRegistro;
        private string dataEnvio;
        private string datosTransporte;
        private string direccion;
        #endregion

        #region Public Properties
        public string NumAlbaran
        {
            get { return numAlbaran; }
            set { numAlbaran = value; }
        }

        public string NumCliente
        {
            get { return numCliente; }
            set { numCliente = value; }
        }

        public string NumIncidencia
        {
            get { return numIncidencia; }
            set { numIncidencia = value; }
        }

        public string DataRegistro
        {
            get { return dataRegistro; }
            set { dataRegistro = value; }
        }

        public string DataEnvio
        {
            get { return dataEnvio; }
            set { dataEnvio = value; }
        }

        public string DatosTransporte
        {
            get { return datosTransporte; }
            set { datosTransporte = value; }
        }


        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        #endregion

    }
}
