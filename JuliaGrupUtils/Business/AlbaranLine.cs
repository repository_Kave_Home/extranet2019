﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class AlbaranLine
    {
        #region Private properties
        private string numAlbaran;
        private int numLinia;
        private string numProducto;
        private double cantidad;
        private double descuento;
        #endregion

        #region Public Properties
        public string NumAlbaran
        {
            get { return numAlbaran; }
            set { numAlbaran = value; }
        }

        public int NumLinia
        {
            get { return numLinia; }
            set { numLinia = value; }
        }

        public string NumProducto
        {
            get { return numProducto; }
            set { numProducto = value; }
        }

        public double Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }

        public double Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }

        #endregion

    }
}
