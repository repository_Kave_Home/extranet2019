﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;
using JuliaGrupUtils.Utils;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    [DataContract]
    public class Article
    {
        #region Private properties

        private string code = String.Empty;
        private int articleVersion = 0;
        private string providerCode = String.Empty;
        private string description = String.Empty;
        private string descriptionCombo = String.Empty;
        private string valueCombo = string.Empty;
        private string catalog = String.Empty;
        private string catalogNo = String.Empty;
        private string insertcatalogNo = String.Empty;
        private string color = String.Empty;
        private string family = String.Empty;
        private string subFamily = String.Empty;
        private string programa = String.Empty;
        private string measures = string.Empty;
        private string volume = string.Empty;
        private string height = string.Empty;
        private string width = string.Empty;
        private string weight = string.Empty;
        private string productType = string.Empty;
        private string length = string.Empty;
        private string priceInPoints = string.Empty;
        private string descripcionLarga = string.Empty;
        private string businessLine = string.Empty;
        private string numItems = string.Empty;
        private bool active = false;
        private double stockIndex = 0;
        private float price = 0;
        private string priceString = string.Empty;
        private int priceInt = 0;
        private float priceInDollars = 0;
        private string priceWithDiscount = string.Empty;
        private string discount = string.Empty;
        private string catalogType = string.Empty;
        private string destacado = string.Empty;
        private decimal unidadMedida = 0;
        private int _stock = 0;
        private string discountNet = string.Empty;
        private string discountNetDecimal = string.Empty; //Puntos netos sense arrodonir. /* 2019-06-21 - Aida Lucha - Shop in shop*/
        private string discounPriceNet = string.Empty;
        private string catalogPorDefecto = string.Empty;
        private Decimal niveldeservicio = decimal.Zero;
        private Decimal cantidadAzul = Decimal.Zero;
        private Decimal cantidaVerde = Decimal.Zero;
        private Decimal cantidadRojo = Decimal.Zero;
        private string productNew = string.Empty;
        private string comingSoon = string.Empty;
        private Decimal cantidadGris = Decimal.Zero;
        private string descripcionGris = string.Empty;
        private string niveldeservicioDescripcion = string.Empty;


        private int gDOMinQty = 0;
        private string gDOTipoUnion = string.Empty;
        private string gDONoVendedor = string.Empty;

        private string codColor = string.Empty;
        private string codMedida = string.Empty;
        private string patronAgrup = string.Empty;
        private int tipoAgrup;
        private string principalAgrup = string.Empty;
        private string detalleAgrup = string.Empty;
        private string itemMateriales = string.Empty;
        private string itemMantenimiento = string.Empty;
        private string itemMontaje = string.Empty;

        private bool tieneDtoExclusivo = false;
        private string eancode = string.Empty;
        private string codigoArancelario = string.Empty;
        private string apilable = string.Empty;
        private string maxQtyContenedor = string.Empty;
        private string paisOrigen = string.Empty;

        private string detalleAgrupColor = string.Empty;
        //REZ19012015 - Ordenación de catalogos
        private int webOrden = 0;
        //REZ10062015 - Productes en liquidació
        private int tipusIcona = 0; // 0 -> Icona per defecte, 1 -> Icona verda de promoció, 2 -> Icona taronja de promoció de liquidació
        //REZ22032016 - Nova categoritzacio
        private string categorizacion = string.Empty;
        private List<string> categoriesList = new List<string>();
        private string recomendado = string.Empty;
        private string volumenBulto = string.Empty;
        private string pesoBulto = string.Empty;

        #endregion

        #region Public Properties

        [DataMember]
        public string NavigateUrl
        {
            get { return "/Pages/ProductInfo.aspx?code=" + this.code + "&version=" + this.articleVersion + "&catalog=" + this.insertcatalogNo; }
            set { }
        }
        [DataMember]
        public string ImgDiscountUrl
        {
            get
            {
                if (this.discount.ToString() == "0,00")
                {
                    return "/Style%20Library/Julia/img/null_pixel.png";
                }
                else
                {
                    string output = String.Empty;
                    //REZ 13052015 - Producte en liquidació
                    //if (this.CatalogNo.StartsWith("PRELIQ"))
                    if (this.TipusIcona == 2)
                    {
                        output = "/Style%20Library/Julia/img/dto_liquidaciones.png";
                    }
                    else
                    {
                        output = "/Style%20Library/Julia/img/dto_promocion.png";
                    }
                    return output;
                }

            }
            set { }
        }
        [DataMember]
        public string DiscountLabel
        {
            get
            {
                if (!(this.discount == "0,00"))
                {
                    return Convert.ToDouble(this.discount, new CultureInfo("es-ES")).ToString("N0") + "%";
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        [DataMember]
        public string ImageUrl
        {
            get
            {
                return this.SmallImgUrl;
            }
            set { }
        }
        [DataMember]
        public string ImgErrorUrl
        {
            get
            {
                string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                //return "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_1.jpg';";
                return "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg';";
            }
            set { }
        }
        [DataMember]
        public string PriceWithDiscount
        {
            get
            {
                //if (!(this.discount== "0,00"))
                //{
                //    if ((int)Session["PriceSelector"] == 0)
                //    {
                //        return "  " + this.discountNet + " " + LanguageManager.GetLocalizedString("Points");
                //    }
                //    else if ((int)Session["PriceSelector"] == 1)
                //    {
                //        if (!(this.discountNet == "0,00"))
                //        {
                //            return String.Format(a.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                //        }
                //    }
                //    else
                //    {
                //        if (!(this.discountNet == "0,00"))
                //        {
                //            //REZ 04052013 - Esta calculant malament
                //            //Output = String.Format(a.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                //            return ((Convert.ToDouble(this.discountNet, new CultureInfo("es-Es", false))) * (Convert.ToDouble(currentUser.Coeficient, new CultureInfo("es-Es", false)))).ToString("N2", new CultureInfo("es-Es", false)) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector");

                //        }
                //    }
                //}
                return priceWithDiscount;
            }
            set { priceWithDiscount = value; }
        }
        [DataMember]
        public string Height
        {
            get { return height; }
            set { height = value; }
        }
        [DataMember]
        public string BusinessLine
        {
            get { return businessLine; }
            set { businessLine = value; }
        }
        [DataMember]
        public string Numitems
        {
            get { return numItems; }
            set { numItems = value; }
        }
        [DataMember]
        public string CatalogNo
        {
            get { return catalogNo; }
            set { catalogNo = value; }
        }
        [DataMember]
        public string InsertcatalogNo
        {
            get { return insertcatalogNo; }
            set { insertcatalogNo = value; }
        }
        [DataMember]
        public string Color
        {
            get { return color; }
            set { color = value; }
        }
        [DataMember]
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        [DataMember]
        public string Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        [DataMember]
        public string Length
        {
            get { return length; }
            set { length = value; }
        }
        /*[DataMember]*/
        public int ArticleVersion
        {
            get { return articleVersion; }
            set { articleVersion = value; }
        }
        /*[DataMember]*/
        public string PriceInPoints
        {
            get { return priceInPoints; }
            set { priceInPoints = value; }
        }
        /*[DataMember]*/
        public string ProviderCode
        {
            get { return providerCode; }
            set { providerCode = value; }
        }
        /*[DataMember]*/
        public string Catalog
        {
            get { return catalog; }
            set { catalog = value; }
        }
        [DataMember]
        public string Family
        {
            get { return family; }
            set { family = value; }
        }
        [DataMember]
        public string SubFamily
        {
            get { return subFamily; }
            set { subFamily = value; }
        }
        [DataMember]
        public string Programa
        {
            get { return programa; }
            set { programa = value; }
        }
        [DataMember]
        public string ProductType
        {
            get { return productType; }
            set { productType = value; }
        }
        /*[DataMember]*/
        public double StockIndex
        {
            get { return stockIndex; }
            set { stockIndex = value; }
        }
        /*[DataMember]*/
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }
        [DataMember]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        [DataMember]
        public string DescripcionLarga
        {
            get { return descripcionLarga; }
            set { descripcionLarga = value; }
        }
        /*[DataMember]*/
        public string DescriptionCombo
        {
            get { return descriptionCombo; }
            set { descriptionCombo = value; }
        }
        /*[DataMember]*/
        public string ValueCombo
        {
            get { return valueCombo; }
            set { valueCombo = value; }
        }
        [DataMember]
        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        /*[DataMember]*/
        public string DiscountNet
        {
            get { return discountNet; }
            set { discountNet = value; }
        }

        /*[DataMember]*/
        /* 2019-06-21 - Aida Lucha - Shop in shop*/
        public string DiscountNetDecimal
        {
            get { return discountNetDecimal; }
            set { discountNetDecimal = value; }
        }

        /*[DataMember]*/
        public string DiscountPriceNet
        {
            get { return discounPriceNet; }
            set { discounPriceNet = value; }
        }
        [DataMember]
        public float Price
        {
            get { return price; }
            set { price = value; }
        }
        [DataMember]
        public string PriceString
        {
            get { return priceString; }
            set { priceString = value; }
        }
        [DataMember]
        public int PriceInt
        {
            get { return priceInt; }
            set { priceInt = value; }
        }
        /*[DataMember]*/
        public float PriceInDollars
        {
            get { return priceInDollars; }
            set { priceInDollars = value; }
        }
        [DataMember]
        public string Measures
        {
            get { return measures; }
            set { measures = value; }
        }
        [DataMember]
        public string Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        /*[DataMember]*/
        public string CatalogType
        {
            get { return catalogType; }
            set { catalogType = value; }
        }
        /*[DataMember]*/
        public string Destacado
        {
            get { return destacado; }
            set { destacado = value; }
        }
        [DataMember]
        public decimal UnidadMedida
        {
            get { return unidadMedida; }
            set { unidadMedida = value; }
        }
        [DataMember]
        public decimal Stock
        {
            get { return _stock; }
            set { }
        }
        /*[DataMember]*/
        public string CatalogPorDefecto
        {
            get { return catalogPorDefecto; }
            set { catalogPorDefecto = value; }
        }
        /*[DataMember]*/
        public string ImgUrl
        {
            get
            {
                //return "http://intranet.juliagrup.com/GestionDocumental/Documents/" + this.Code + "/" + this.Code + "·1.jpg";
                return JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                            JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/"
                                    + this.Code + "/" + this.Code + "·P.jpg";
                //return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + this.Code; 
            }
        }
        /*[DataMember]*/
        public string SmallImgUrl
        {
            get
            {
                //return "http://intranet.juliagrup.com/GestionDocumental/Documents/" + this.Code + "/" + this.Code + "·1.jpg";
                return JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                            JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/"
                                    + this.Code + "/" + this.Code + "·P.jpg";
                //return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + this.Code + "&type=1"; 
            }
        }
        /*[DataMember]*/
        public string MediumImgUrl
        {
            get { return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + this.Code + "&type=2"; }
        }
        /*[DataMember]*/
        public string LargeImgUrl
        {
            get { return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + this.Code + "&type=3"; }
        }
        [DataMember]
        public Decimal CantidadAzul
        {
            get { return cantidadAzul; }
            set { cantidadAzul = value; }
        }
        [DataMember]
        public Decimal CantidadVerde
        {
            get { return cantidaVerde; }
            set { cantidaVerde = value; }
        }
        [DataMember]
        public Decimal CantidadRojo
        {
            get { return cantidadRojo; }
            set { cantidadRojo = value; }
        }
        [DataMember]
        public Decimal NiveldeServicio
        {
            get { return niveldeservicio; }
            set { niveldeservicio = value; }
        }
        /*[DataMember]*/
        public string GDOTipoUnion
        {
            get { return gDOTipoUnion; }
            set { gDOTipoUnion = value; }
        }
        /*[DataMember]*/
        public string GDONoVendedor
        {
            get { return gDONoVendedor; }
            set { gDONoVendedor = value; }
        }
        /*[DataMember]*/
        public int GDOMinQty
        {
            get { return gDOMinQty; }
            set { gDOMinQty = value; }
        }
        /*[DataMember]*/
        public string CodColor
        {
            get { return codColor; }
            set { codColor = value; }
        }
        /*[DataMember]*/
        public string CodMedida
        {
            get { return codMedida; }
            set { codMedida = value; }
        }
        /*[DataMember]*/
        public string PatronAgrup
        {
            get { return patronAgrup; }
            set { patronAgrup = value; }
        }
        /*[DataMember]*/
        public int TipoAgrup
        {
            get { return tipoAgrup; }
            set { tipoAgrup = value; }
        }
        /*[DataMember]*/
        public string PrincipalAgrup
        {
            get { return principalAgrup; }
            set { principalAgrup = value; }
        }
        /*[DataMember]*/
        public string DetalleAgrup
        {
            get { return detalleAgrup; }
            set { detalleAgrup = value; }
        }

        public string DetalleAgrupColor
        {
            get { return detalleAgrupColor; }
            set { detalleAgrupColor = value; }
        }

        public int WebOrden
        {
            get { return webOrden; }
            set { webOrden = value; }
        }

        [DataMember]
        public string ItemMateriales
        {
            get { return itemMateriales; }
            set { itemMateriales = value; }
        }
        [DataMember]
        public string ItemMantenimiento
        {
            get { return itemMantenimiento; }
            set { itemMantenimiento = value; }
        }
        [DataMember]
        public string ItemMontaje
        {
            get { return itemMontaje; }
            set { itemMontaje = value; }
        }
        [DataMember]
        public string ProductNew
        {
            get { return productNew; }
            set { productNew = value; }
        }
        [DataMember]
        public string ComingSoon
        {
            get { return comingSoon; }
            set { comingSoon = value; }
        }
        [DataMember]
        public Decimal CantidadGris
        {
            get { return cantidadGris; }
            set { cantidadGris = value; }
        }
        [DataMember]
        public string DescripcionGris
        {
            get { return descripcionGris; }
            set { descripcionGris = value; }
        }
        [DataMember]
        public string NiveldeservicioDescripcion
        {
            get { return niveldeservicioDescripcion; }
            set { niveldeservicioDescripcion = value; }
        }

        [DataMember]
        public bool TieneDtoExclusivo
        {
            get { return tieneDtoExclusivo; }
            set { tieneDtoExclusivo = value; }
        }

        [DataMember]
        public string EANcode
        {
            get { return eancode; }
            set { eancode = value; }
        }

        [DataMember]
        public string CodigoArancelario
        {
            get { return codigoArancelario; }
            set { codigoArancelario = value; }
        }

        [DataMember]
        public string Apilable
        {
            get { return apilable; }
            set { apilable = value; }
        }
        public string MaxQtyContenedor
        {
            get { return maxQtyContenedor; }
            set { maxQtyContenedor = value; }
        }

        [DataMember]
        public string PaisOrigen
        {
            get { return paisOrigen; }
            set { paisOrigen = value; }
        }

        //REZ10062015 - Productes en liquidació
        public int TipusIcona
        {
            get { return tipusIcona; }
            set { tipusIcona = value; }
        }

        //REZ22032016 - Nova categoritzacio
        public string Categorizacion
        {
            get { return categorizacion; }
            set { categorizacion = value; }
        }
        public List<string> CategoriesList
        {
            get { return categoriesList; }
            set { categoriesList = value; }
        }
        public string Recomendado
        {
            get { return recomendado; }
            set { recomendado = value; }
        }
        public string VolumenBulto
        {
            get { return volumenBulto; }
            set { volumenBulto = value; }
        }
        public string PesoBulto
        {
            get { return pesoBulto; }
            set { pesoBulto = value; }
        }

        #endregion

        #region Constructor

        internal Article() { }

        public Article(JuliaWs.ProductoWEB p, decimal optionalTipoCambio = (decimal)1.3305)
        {
            this.code = p.ItemNo;
            this.ArticleVersion = p.ItemVersion;
            //providerCode
            this.description = p.ItemDescriprion;
            this.descripcionLarga = p.ItemDescripcionLarga;
            this.descriptionCombo = p.ItemNo + " " + p.ItemDescriprion + "(" + p.CatalogoDescripcion + ")";
            this.valueCombo = p.ItemNo + ";" + p.ItemVersion + ";" + p.CatalogueReal;
            //this.catalog = p.CatalogueName;
            this.catalog = p.CatalogoDescripcion;
            this.catalogNo = p.CatalogueNo;
            this.insertcatalogNo = p.CatalogueReal;
            this.color = p.ItemColorJulia;
            this.family = p.ItemCategoryProduct;
            this.subFamily = p.ProductGroupCode;
            this.programa = p.Programa;
            //measures
            this.measures = p.MedidasBultos;
            this.volume = p.ItemVolumen;
            this.height = p.ItemAltura;
            this.width = p.ItemAncho;
            this.weight = p.ItemPeso;
            this.length = p.ItemLargo;
            this.priceInPoints = p.ItemPuntos;
            this.businessLine = p.BusinessLine;
            this.numItems = p.ItemBultos.ToString();
            this.active = true;
            //stockIndex
            this.price = float.Parse(p.ItemPrecio, new System.Globalization.CultureInfo("es-ES")); //Debemos parsear la ,
            this.catalogType = p.TipoCatalogo;
            this.destacado = p.ItemDestacado;
            this.unidadMedida = (!String.IsNullOrEmpty(p.UnidadMedidCantidad)) ? Convert.ToDecimal(p.UnidadMedidCantidad, new System.Globalization.CultureInfo("es-ES")) : 1;
            this._stock = p.Stock;
            this.discount = p.ItemDescuento;
            this.discountNet = p.ItemPuntosNetos;
            /* 2019-06-21 - Aida Lucha - Shop in shop*/
            this.discountNetDecimal = p.ItemPuntosNetos;
            this.discounPriceNet = p.ItemPrecioNeto;
            this.catalogPorDefecto = p.CatalogoPorDefecto == 0 ? "false" : "true";
            this.cantidadAzul = Decimal.Parse(p.CantidadAzul, new System.Globalization.CultureInfo("es-ES"));
            this.niveldeservicio = p.NivelDeServicio;
            this.cantidadRojo = Decimal.Parse(p.CantidadRojo, new System.Globalization.CultureInfo("es-ES"));
            this.cantidaVerde = Decimal.Parse(p.CantidadVerde, new System.Globalization.CultureInfo("es-ES"));
            this.productNew = p.ProductoNew;
            
            this.cantidadGris = Decimal.Parse(p.CantidadGris, new System.Globalization.CultureInfo("es-ES"));
            this.descripcionGris = p.GrisDesc;
            this.niveldeservicioDescripcion = p.NivelServicioDesc;

            //REZ 25072014 - Filtre descomptes i atrubuts fitxa producte
            //Valors "No" y "Sí"
            this.tieneDtoExclusivo = (p.TieneDtoExclusivo == 1) ? true : false;

            this.eancode = p.EANcode;
            this.codigoArancelario = p.CodigoArancelario;
            this.apilable = p.Apilable;

            //GDO
            this.gDOMinQty = p.GDOMinQty;
            this.gDONoVendedor = p.GDOVendorNo;
            this.GDOTipoUnion = p.GDOTipoUnion;

            this.codColor = p.CodColor;
            this.codMedida = p.CodMedida;
            this.patronAgrup = p.PatronAgrup;
            this.tipoAgrup = p.TipoAgrup;
            this.principalAgrup = p.PrincipalAgrup;
            this.detalleAgrup = p.DetalleAgrup;
            this.itemMateriales = p.ItemMateriales;
            this.itemMantenimiento = p.ItemMantenimento;
            this.itemMontaje = p.ItemMontaje;
            this.priceInDollars = this.price * (float)optionalTipoCambio;//float.Parse("1,3305", new System.Globalization.CultureInfo("es-ES"));

            this.maxQtyContenedor = p.MaxQtyContenedor;
            this.paisOrigen = p.PaisOrigen;
            //Campo para montar minuaturas de agrupación de color
            DetalleAgrupColor = p.DetalleagrupView2; //Codis de producte separats per ;
            this.webOrden = p.Web_Orden;    //REZ19012015 - Orden Catalogos
            this.tipusIcona = p.TipusIcona;

            //REZ22032016 - Nova categorització

            this.categorizacion = p.Categorizacion;
            this.categoriesList = (!string.IsNullOrEmpty(categorizacion)) ? categorizacion.Split(';').ToList() : new List<string>();
            this.recomendado = p.Recomendado_1;
            this.volumenBulto = p.VolumenBulto;
            this.pesoBulto = p.PesoBulto;

        }

        public Article(System.Data.DataRow p, decimal optionalTipoCambio = (decimal)1.3305)
        {
            CultureInfo culture = new CultureInfo("es-Es", false);

            this.code = p["ItemNo"].ToString();
            this.ArticleVersion = int.Parse(p["ItemVersion"].ToString());
            //providerCode
            this.description = p["Item Description"].ToString();
            this.descripcionLarga = p["ItemLongDescription"].ToString();
            this.descriptionCombo = p["ItemNo"].ToString() + " " + p["Item Description"].ToString() + "(" + p["CatalogueDescription"].ToString() + ")";
            this.valueCombo = p["ItemNo"].ToString() + ";" + p["ItemVersion"].ToString() + ";" + p["CatalagoReal"].ToString();
            //this.catalog = p.CatalogueName;
            this.catalog = p["CatalogueDescription"].ToString();
            this.catalogNo = p["CatalogueNo"].ToString();
            this.insertcatalogNo = p["CatalagoReal"].ToString();
            this.color = p["ItemColorJulia"].ToString();
            this.family = p["Item Category Producct"].ToString();
            this.subFamily = p["Product Grupo Code"].ToString();
            this.programa = p["Programa"].ToString();
            this.productType = p["ItemProductType"].ToString();
            //measures
            this.measures = p["WEBMedidas"].ToString();
            this.volume = ((decimal)p["ItemVolumen"]).ToString("N3", culture);
            this.height = p["ItemAltura"].ToString();
            this.width = p["ItemAncho"].ToString();
            this.weight = ((decimal)p["ItemPeso"]).ToString("N2", culture);
            this.length = p["ItemLargo"].ToString();
            this.priceInPoints = ((decimal)p["ItemPuntos"]).ToString("N0", culture);
            this.businessLine = p["BusinessLine"].ToString();
            this.numItems = p["ItemBultos"].ToString();
            this.active = true;
            //stockIndex
            this.price = float.Parse(p["ItemPrecio"].ToString(), NumberStyles.Currency); //Debemos parsear la ,
            this.catalogType = GetCatalogTypeString(p["TipoCatalogo"].ToString());
            this.destacado = (p["ItemDestacado"].ToString().ToString() == "1") ? "Sí" : "No";
            this.unidadMedida = Math.Round((!String.IsNullOrEmpty(p["IUOM_Cantidad"].ToString())) ? Convert.ToDecimal(p["IUOM_Cantidad"].ToString(), new System.Globalization.CultureInfo("en-US")) : (decimal)1.00, 2);
            this._stock = int.Parse(p["Stock"].ToString());
            this.discount = p["ItemDescuento"].ToString().Replace('.', ',');
            this.discountNet =  //Es INT
                (!String.IsNullOrEmpty(p["ItemPuntosNetos"].ToString())) ?
                        Math.Round(Decimal.Parse(p["ItemPuntosNetos"].ToString(), NumberStyles.Currency), 0).ToString("N0", culture)
                        : "0";
            /* 2019-06-21 - Aida Lucha - Shop in shop*/
            this.discountNetDecimal = (!String.IsNullOrEmpty(p["ItemPuntosNetos"].ToString())) ? Decimal.Parse(p["ItemPuntosNetos"].ToString(), NumberStyles.Currency).ToString("N2", culture) : "0,00";  
            this.discounPriceNet =  //Con 2 decimales y coma
                (!String.IsNullOrEmpty(p["ItemPrecioNeto"].ToString())) ? Decimal.Parse(p["ItemPrecioNeto"].ToString(), NumberStyles.Currency).ToString("N2", culture) : "0,00";
            this.catalogPorDefecto = int.Parse(p["CatalogoDefecto"].ToString()) == 0 ? "false" : "true";
            this.cantidadAzul = Math.Round(Decimal.Parse(p["CantidadAzul"].ToString(), new System.Globalization.CultureInfo("en-US")), 2);
            this.niveldeservicio = Decimal.Parse(p["NivelDeServicio"].ToString());
            this.cantidadRojo = Math.Round(Decimal.Parse(p["CantidadRojo"].ToString(), new System.Globalization.CultureInfo("en-US")), 2);
            this.cantidaVerde = Math.Round(Decimal.Parse(p["CantidadVerde"].ToString(), new System.Globalization.CultureInfo("en-US")), 2);
            this.productNew = (p["ProductoNew"].ToString() == "1") ? "Sí" : "No";
            this.comingSoon = (p["ComingSoon"].ToString() == "1") ? "Sí" : "No";
            this.cantidadGris = Math.Round(Decimal.Parse(p["CantidadGris"].ToString(), new System.Globalization.CultureInfo("en-US")), 2);
            this.descripcionGris = p["GrisDesc"].ToString();
            this.niveldeservicioDescripcion = p["NivelServicioDesc"].ToString();

            //REZ 25072014 - Filtre descomptes i atrubuts fitxa producte
            //Valors "No" y "Sí"
            this.tieneDtoExclusivo = (int.Parse(p["TieneDtoExclusivo"].ToString()) == 1) ? true : false;

            this.eancode = p["EANcode"].ToString();
            this.codigoArancelario = p["CodigoArancelario"].ToString();
            this.apilable = (p["Apilable"].ToString().ToString() == "1") ? "Sí" : "No";

            //GDO
            this.gDOMinQty = int.Parse(p["GDOMinQty"].ToString());
            this.gDONoVendedor = p["GDOVendorNo"].ToString();
            this.GDOTipoUnion = p["GDOTipoUnion"].ToString();

            this.codColor = p["CodColor"].ToString();
            this.codMedida = p["CodMedida"].ToString();
            this.patronAgrup = p["PatronAgrup"].ToString();
            this.tipoAgrup = int.Parse(p["TipoAgrup"].ToString());
            this.principalAgrup = (p["PrincipalAgrup"].ToString() == "1") ? "Sí" : "No";
            this.detalleAgrup = p["DetalleAgrup"].ToString();
            this.itemMateriales = p["ItemMateriales"].ToString();
            this.itemMantenimiento = p["ItemMantenimiento"].ToString();
            this.itemMontaje = p["ItemMontaje"].ToString();
            this.priceInDollars = this.price * (float)optionalTipoCambio;//float.Parse("1,3305", new System.Globalization.CultureInfo("es-ES"));

            this.maxQtyContenedor = p["MaxQtyContenedor"].ToString();
            this.paisOrigen = p["PaisOrigen"].ToString();
            //Campo para montar minuaturas de agrupación de color
            this.DetalleAgrupColor = p["DetalleagrupView2"].ToString(); //Codis de producte separats per ;
            this.webOrden = int.Parse(p["Web_Orden"].ToString());    //REZ19012015 - Orden Catalogos
            this.tipusIcona = int.Parse(p["TipusIcona"].ToString());

            //REZ22032016 - Nova categorització

            this.categorizacion = p["Categorizacion"].ToString();
            this.categoriesList = (!string.IsNullOrEmpty(categorizacion)) ? categorizacion.Split(';').ToList() : new List<string>();
            this.recomendado = p["Recomendado_1"].ToString();
            this.volumenBulto = p["VolumenBulto"].ToString();
            this.pesoBulto = p["PesoBulto"].ToString();

        }


        protected string GetCatalogTypeString(string iCatalogType)
        {
            string output = String.Empty;
            switch (iCatalogType)
            {
                case "1": output = "Promociones"; break;
                case "2": output = "Liquidaciones Laforma"; break;
                case "3": output = "Liquidaciones Dorsuit"; break;
                case "4": output = "General"; break;
                case "5": output = "Inspirate"; break;
                case "6": output = "E-Commerce"; break;
                case "7": output = "GDO"; break;
                case "8": output = "Exclusivos"; break;
                case "9": output = "Anexo"; break;
                case "10": output = "Tarifa"; break;
            }
            return output;
        }

        //Articulo no producto web. Por ejemplo manipulación
        public Article(string code, string description)
        {
            this.code = code;
            //this.name = name;
            //this.price = price;
            this.description = description;
            //this.type = type;
        }

        #endregion

        #region Public Methods
       
        public void CalculatePriceWithDiscount(string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid, int tipoCompra, decimal dollarCoef, bool shopInShop)
        {
            
            CultureInfo culture = new CultureInfo("es-Es", false);
            
            this.priceString = String.Empty;
            this.priceInt = 0;
            this.priceWithDiscount = String.Empty;
            string oldprice = this.Price.ToString("N2", culture);
            if (tipoCompra == 0)
            {
                switch (iPriceSel)
                {
                    case "0":
                        this.priceString = this.priceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points", lcid);
                        this.priceInt = Convert.ToInt32(Math.Round(Convert.ToDouble(this.priceInPoints, culture)));
                        if (!(this.discount == "0,00") && !(this.discountNet == "0"))
                        {
                            this.priceWithDiscount = "  " + this.discountNet + " " + LanguageManager.GetLocalizedString("Points", lcid);
                            this.priceInt = Convert.ToInt32(Math.Round(Convert.ToDouble(this.discountNet, culture)));
                        }
                        break;
                    case "1":
                        this.PriceString = oldprice + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros", lcid);
                        this.PriceInt = (int)this.Price;
                        if (!(this.Discount == "0,00") && !(this.DiscountNet == "0"))
                        {
                            this.PriceWithDiscount = String.Format(this.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros", lcid);
                            this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountPriceNet, culture)));
                        }
                        break;
                    case "2":
                        this.PriceString = ((Convert.ToDouble(this.PriceInPoints, culture)) *
                                                    (Convert.ToDouble(coeficient, culture))).ToString("N2", culture) +
                                                    " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector", lcid);
                        
                        if (!(this.Discount == "0,00") && !(this.DiscountNet == "0"))
                        {/* 2019-06-04 - Aida Lucha - Shop in shop*/
                            if (!shopInShop)
                            {
                                this.priceWithDiscount = ((Convert.ToDouble(this.DiscountNet, culture)) * (Convert.ToDouble(coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector", lcid);
                                this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountNet, culture) * coeficient));
                                
                            }
                            else
                            {
                                this.priceWithDiscount = ((Convert.ToDouble(this.DiscountNetDecimal, culture)) * (Convert.ToDouble(coeficient, culture))).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("EurosPVPSelector", lcid);
                                this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountNetDecimal, culture) * coeficient));
                                
                            }
                        }
                        else {

                            if (!shopInShop)
                            {
                                this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.PriceInPoints, culture) * coeficient));
                            }
                            else {
                                this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountNetDecimal, culture) * coeficient));
                            }
                        }
                        break;
                    case "3":
                        this.PriceString = ((Convert.ToDouble(oldprice, culture)) *
                                                    (Convert.ToDouble(coefDivisaWEB, culture))).ToString("N2", culture)
                                                    + " " + literalDivisaWEB;
                        this.PriceInt = (int)(this.Price * (float)coefDivisaWEB);
                        if (!(this.Discount == "0,00") && !(this.DiscountNet == "0"))
                        {
                            this.priceWithDiscount = (
                                        (Convert.ToDouble(this.DiscountPriceNet, culture)) *
                                        (Convert.ToDouble(coefDivisaWEB, culture))
                                     ).ToString("N2", culture)
                                     + " " + literalDivisaWEB;
                            this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountPriceNet, culture) * coefDivisaWEB));
                        }
                        break;
                    case "4":
                        this.PriceString = (((Convert.ToDouble(this.PriceInPoints, culture)) *
                                                    (Convert.ToDouble(coefDivisaWEB, culture))) *
                                                    (Convert.ToDouble(coeficient, culture))).ToString("N2", culture)
                                                    + " " + literalDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo", lcid);
                        this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.PriceInPoints, culture) * coefDivisaWEB * coeficient));
                        if (!(this.Discount == "0,00") && !(this.DiscountNet == "0"))
                        {
                            this.priceWithDiscount = ((Convert.ToDouble(this.DiscountNet, culture)) *
                                        (Convert.ToDouble(coeficient, culture)) *
                                        (Convert.ToDouble(coefDivisaWEB, culture))
                                     ).ToString("N2", culture)
                                     + " " + literalDivisaWEB + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("DivisaPVPCombo", lcid);
                            this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDecimal(this.DiscountNet, culture) * coefDivisaWEB * coeficient));
                        }
                        break;
                    default:
                        this.PriceString = this.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points", lcid);
                        //this.PriceWithDiscount = this.PriceInPoints + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Points", lcid);
                        this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDouble(this.PriceInPoints, culture)));
                        if (!(this.Discount == "0,00") && !(this.DiscountNet == "0"))
                        {
                            this.PriceWithDiscount = "  " + this.DiscountNet + " " + LanguageManager.GetLocalizedString("Points", lcid);
                            this.PriceInt = Convert.ToInt32(Math.Round(Convert.ToDouble(this.DiscountNet, culture)));
                        }
                        break;
                }
            }
            else
            {
                switch (iPriceSel)
                {
                    case "0":
                        this.PriceString = oldprice + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Dollars");
                        this.PriceInt = (int)this.Price;
                        if (!(this.Discount == "0,00"))
                        {
                            this.PriceWithDiscount = " " + this.DiscountNet + " " + LanguageManager.GetLocalizedString("Dollars");
                        }
                        break;
                    case "1":
                        this.PriceString = (this.Price * Convert.ToDouble(dollarCoef)).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        this.PriceInt = (int)(this.Price * (float)dollarCoef);
                        if (!(this.Discount == "0,00"))
                        {
                            this.PriceWithDiscount = String.Format(this.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        }
                        break;
                    default:
                        this.PriceString = (this.Price * Convert.ToDouble(dollarCoef)).ToString("N2", culture) + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        this.PriceInt = (int)(this.Price * (float)dollarCoef);
                        if (!(this.Discount == "0,00"))
                        {
                            this.PriceWithDiscount = String.Format(this.DiscountPriceNet, "N2") + " " + JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("Euros");
                        }
                        break;
                }
            }
        }
        #endregion

    }


    // Custom comparer for the Article class 
    class ArticleComparer : IEqualityComparer<Article>
    {
        // Products are equal if their names and product numbers are equal. 
        public bool Equals(Article x, Article y)
        {

            //Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal. 
            return x.Code == y.Code;
        }

        // If Equals() returns true for a pair of objects  
        // then GetHashCode() must return the same value for these objects. 

        public int GetHashCode(Article product)
        {
            //Check whether the object is null 
            if (Object.ReferenceEquals(product, null)) return 0;

            //Get hash code for the Name field if it is not null. 
            int hashProductName = product.Description == null ? 0 : product.Description.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = product.Code.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }

    }

}
