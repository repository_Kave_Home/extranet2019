﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Bill
    {
        #region Private properties

        private string costumercode;
        private string costumername;
        private string code;
        private Client BillClient;
        private string description;
        private decimal  import;
        private DateTime date;
        private string referencia = string.Empty;

        private BillsDetailsWS.HistoricoFactura factura;

        private List<BillLine> lines;
        private string divisa = "EUR";

        #endregion

        #region Public Properties

        public string CostumerCode
        {
            get { return costumercode; }
        }
        public string CostumerName
        {
            get { return costumername; }
        }

        public string Code
        {
            get { return code; }        
        }

        public string Description
        {
            get { return description; }       
        }

        public decimal Import
        {
            get { return import; }        
        }

        public DateTime Date
        {
            get { return date; }       
        }
        public string Referencia
        {
            get { return referencia; }
        }


        public List<BillLine> Lines
        {
            get {                            
                GetProductsList();   
                return lines; }          
        }

        private  void GetProductsList()
        {
            try
            {
                ////load product list from the current order
                ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(this.BillClient.UserName, this.BillClient.Code,0);
                this.lines .Clear();

                foreach (BillsDetailsWS.Posted_Sales_Invoice_Line line in this.factura.SalesInvLines)
                {
                    this.AddLine(line, articleDAO);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG , JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public string Divisa
        {
            get
            {
                return divisa;
            }
            set
            {
                divisa = value;
            }
        }
        #endregion

        #region Constructor

        public Bill(BillsWS.ListaHistoricoFacturas bill)
        {
            this.costumername = bill.Bill_to_Name ;
            this.code = bill.No ;
            this.costumercode = bill.Bill_to_Customer_No;
            this.referencia = bill.Referencia;
            this.import = bill.Amount_Including_VAT;
            this.date = bill.Posting_Date;
            this.lines = new List<BillLine>();
            this.divisa = (bill.CurrencyCode == "USD") ? "USD" : "EUR";
        }

        public Bill(BillsDetailsWS.HistoricoFactura bill, Client c)
        {
            this.BillClient = c;
            this.factura = bill;
            this.costumername = bill.Bill_to_Name ;
            this.code = bill.No ;
            this.costumercode = bill.Bill_to_Customer_No;
            this.import = bill.Amount_Including_VAT;
            this.date = bill.Posting_Date;
            this.lines = new List<BillLine>();
        } 
        
        #endregion

        #region private functions

        private void AddLine(BillsDetailsWS.Posted_Sales_Invoice_Line line, ArticleDataAccessObject articleDAO)
        {
            try
            {
                if (line.No != null)
                {
                    //get the article
                    Article a = articleDAO.GetProductByCode(line.No);
                    //Article a = articleDAO.GetProductByCodeAndCatalog(line.No, "", line.catalogNo);

                    if (a != null)
                    {
                        BillLine ol = new BillLine((int)line.Quantity, a, (double)line.Line_Discount_Percent);
                        //ol.Price = (double)line.Line_Amount;
                        //ol.OrderLineKey = line.Key;
                        //ol.LineNo = line.Line_no;
                        //ol.LineDescription = line.Your_Reference;

                        this.lines.Add(ol);
                    }
                    else
                    {
                        Article p = new Article(line.No, line.Description);
                        BillLine ol = new BillLine((int)line.Quantity, p, (double)line.Line_Discount_Percent);
                        //ol.Price = (double)line.Line_Amount;
                        //ol.OrderLineKey = line.Key;
                        //ol.LineNo = line.Line_no;
                     //   ol.LineDescription = line.Your_Reference;

                        this.lines.Add(ol);
                    }

                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        #endregion
    }
}
