﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.Business
{
    public class BillLine
    {
        #region Private Properties

        private Article product;
        private int amount;
        private double discount;

        #endregion

        #region Public Properties

        public Article Product
        {
            get { return product; }
        }

        public int Amount
        {
            get { return amount; }
        }

        public double Discount
        {
            get { return discount; }
        }
 
        #endregion

        #region Constructor

        public BillLine(int _amount, Article _product, double _discount)
        {
            this.amount = _amount;
            this.product = _product;
            this.discount = _discount;
        }

        #endregion

        #region Methods

        public double GetProductPrice()
        {
            Double result = 0;
            try
            {
                double totalprice = (this.amount * this.product.Price);
                double auxTotal = totalprice - (totalprice * (this.discount / 100));
                string dd = auxTotal.ToString("f2");
                result= Double.Parse(dd);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG , JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public double GetProductDiscount()
        {
            double productDiscount = 0;
            try
            {
                productDiscount = (this.discount * this.amount);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
             return productDiscount;
        }

        #endregion
    }
}
