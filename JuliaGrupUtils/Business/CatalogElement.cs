﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class CatalogElement : SlideElement
    {
        private string onImageUrl;
        public string OnImageUrl
        {
            get { return onImageUrl; }
            set { onImageUrl = value; }
        }

        private string offImageUrl;
        public string OffImageUrl
        {
            get { return offImageUrl; }
            set { offImageUrl = value; }
        }

        private string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private DateTime novelty;
        public DateTime Novelty
        {
            get { return novelty; }
            set { novelty = value; }
        }

        public CatalogElement()
        {
            novelty = new DateTime();

            UpdateImageToShow();
        }

        public CatalogElement(string _code, string _offImageUrl, string _onImageUrl, DateTime _novelty)
        {
            code = _code;
            offImageUrl = _offImageUrl;
            onImageUrl = _onImageUrl;
            novelty = _novelty;

            UpdateImageToShow();
        }

        public void UpdateImageToShow()
        {
            if (this.novelty != null || this.novelty.CompareTo(DateTime.Now) <= 0)
                this.imageURL = onImageUrl;
            else
                this.imageURL = offImageUrl;
        }

        public void SetUrl()
        {
            this.url = "/loquesea?catalog=" + code;
        }
    }
}
