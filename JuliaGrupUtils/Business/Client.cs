﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Client : User
    {
        #region Private Properties
        
   
        private string name;
        private string searchname;
        private string phone;
        private string email;
        //0 code, 1 description, 2 city, 3 country
        private List<string[]> addresses = new List<string[]>();
        //0 code, 1 description
        private List<string[]> payMethods = new List<string[]>();
        private string direction;
        private string cp;
        private string portal;
        private string piso;
        private string city;
        private string country;
        private bool partialDelivery;
        private bool accesoGDO;
        private int headerImage;
        private bool puedecrearcampañas;
        private decimal coefDivisaWEB;
        private string literalDivisaWEB;
        private bool verDivisaWEB;
        private decimal tipoCambioDivisa;
        private decimal dtoWeb;
        private bool recogidaAlmacen;
        private bool shopInShop; /* 2019-06-04 - Aida Lucha - Shop in shop*/


        #endregion

        #region Creators
        public Client(ClientWs.Cliente cli, string username)
        {
            
            this.UserName = username;
            this.Code = cli.No;
            this.Type = "Client";
            this.Name = cli.Name;
            this.SearchName = cli.Search_Name;
            this.AddPayMethod(cli.Payment_Method_Code, ""); 
            this.phone = cli.Phone_No;
            this.direction = cli.Address;
            this.cp = cli.Post_Code;
            this.city = cli.City;
            this.country = cli.County;
            this.email = cli.E_Mail;
            this.AccesoGDO = cli.AccesoGDO;
            this.headerImage = cli.HeaderImage;
            this.puedecrearcampañas = cli.PuedeCrearCampañas;

            //this.coefDivisaWEB = cli.CoefDivisaWEB;
            //REZ 17082014 - Lo forzamos a 1 ya que no se quiere tener dos coeficientes
            this.coefDivisaWEB = 1;
            this.literalDivisaWEB = cli.LiteralDivisaWEB;
            this.verDivisaWEB = cli.VerDivisaWEB;
            this.tipoCambioDivisa = cli.TipoCambioDivisa;
            //REZ 21012015
            this.dtoWeb = cli.DtoWEB;

            this.recogidaAlmacen = cli.RecogidaAlmacen; //Indica si se muestra el selector de recogida almacen en la página de confirmación de oferta
            this.shopInShop = cli.Shop_in_shop; /* 2019-06-04 - Aida Lucha - Shop in shop*/

        }
        /* Constructor for user in Client WebServices call*/
        public Client(string clicode, string username)
        {
            this.UserName = username;
            this.Code = clicode;
        }  
        #endregion


        #region Public Properties



        //public string Email
        //{
        //    get { return email; }
        //    set { email = value; }
        //}

        public bool PartialDelivery
        {
            get { return partialDelivery; }
            set { partialDelivery = value; }
        }

     

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string SearchName
        {
            get { return searchname; }
            set { searchname = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }


        //key-value
        public List<string[]> Addresses
        {
            get { return addresses; }
            set { addresses = value; }
        }

        public List<string[]> PayMethods
        {
            get { return payMethods; }
            set { payMethods = value; }
        }

        public string Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        public string CodigoPostal
        {
            get { return cp; }
            set { cp = value; }
        }

        public string Portal
        {
            get { return portal; }
            set { portal = value; }
        }

        public string Piso
        {
            get { return piso; }
            set { piso = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public bool AccesoGDO
        {
            get { return accesoGDO; }
            set { accesoGDO = value; }
        }

        public int HeaderImage
        {
            get { return headerImage; }
            set { headerImage = value; }
        }

        public bool PuedeCrearCampañas
        {
            get { return this.puedecrearcampañas; }
            set { this.puedecrearcampañas = value; }
        }

        public virtual decimal CoefDivisaWEB
        {
            get { return this.coefDivisaWEB; }
            set { this.coefDivisaWEB = value; }
        }
        public string LiteralDivisaWEB
        {
            get { return this.literalDivisaWEB; }
            set { this.literalDivisaWEB = value; }
        }
        public bool VerDivisaWEB
        {
            get { return this.verDivisaWEB; }
            set { this.verDivisaWEB = value; }
        }

        public decimal TipoCambioDivisa {
            get { return this.tipoCambioDivisa; }
            set { this.tipoCambioDivisa = value; }        
        }

        public decimal DtoWeb
        {
            get { return this.dtoWeb; }
            set { this.dtoWeb = value; }
        }

        #endregion

        public void AddAddress(string code, string description, string city, string pais)
        {
            string[] aux = { code, description, city, pais };
            this.addresses.Add(aux);
        }

        public void AddPayMethod(string code, string description)
        {
            string[] aux = { code, description };
            this.PayMethods.Add(aux);
        }

        public bool RecogidaAlmacen
        {
            get { return this.recogidaAlmacen; }
            set { this.recogidaAlmacen = value; }
        }

        /* 2019-06-04 - Aida Lucha - Shop in shop*/
        public bool Shop_In_Shop
        {
            get { return this.Shop_In_Shop; }
            set { this.Shop_In_Shop = value; }
        }
    }
}
