﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{

    [Serializable]
    public class ClientGroup : User
    {
        #region Private properties
        private string name;
        private string phone;
        private string codVendedor;
        private string tracking; //estado del pedido
        private string deliveryDate;
        private string outDate;
        private string stat; //si la entrega es total o parcial
        private string codGrupo;
        private List<Client> listClients = new List<Client>();
        private  List<ClientHeader> _listClientsName = new List<ClientHeader>();
        #endregion

        #region Public Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string CodGrupo
        {
            get { return codGrupo; }
            set { codGrupo = value; }
        }
        public string CodVendedor
        {
            get { return codVendedor; }
            set { codVendedor = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string DeliveryDate
        {
            get { return deliveryDate; }
            set { deliveryDate = value; }
        }

        public string OutputDate
        {
            get { return outDate; }
            set { outDate = value; }
        }

        public string Tracking
        {
            get { return tracking; }
            set { tracking = value; }
        }

        public string Stat
        {
            get { return stat; }
            set { stat = value; }
        }

        public List<Client> ListClients
        {
            get { return listClients; }
            set { listClients = value; }
        }

        public List<ClientHeader> ListClientsName
        {
            get { return _listClientsName; }
            set { _listClientsName = value; }
        }
        #endregion

    }
}
