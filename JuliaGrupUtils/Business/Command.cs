﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Command
    {
        #region Private properties

            private string numCommand;
            private Client referenceClient;
            private List<Order> orderList;
            private string orderData;
            private string shippingData;
            private int stat;

        #endregion

        #region Public Properties

            public string NumCommand
            {
                get { return numCommand; }
                set { numCommand = value; }
            }

            public Client ReferenceClient
            {
                get { return referenceClient; }
            }

            public List<Order> OrderList
            {
                get { return orderList; }
                set { orderList = value; }
            }

            public string OrderData
            {
                get { return orderData; }
                set { orderData = value; }
            }

            public string ShippingData
            {
                get { return shippingData; }
                set { shippingData = value; }
            }

            public int Stat
            {
                get { return stat; }
                set { stat = value; }
            }

        #endregion

        #region Class creators

        public Command(string numCommand, Client referenceClient, string orderData, string shippingData, int stat)
        {
            this.numCommand = numCommand;
            this.referenceClient = referenceClient;
            this.orderData = orderData;
            this.shippingData = shippingData;
            this.stat = stat;
            this.orderList = new List<Order>();
           
        }
        #endregion

        #region Public Functions

        //Obtener el valor final de la comanda
        public double GetCommandFinalImport()
        {
            double finalImport = 0;
            try
            {
                foreach (Order or in orderList)
                {
                    finalImport =+ or.GetOrderFinalPrice();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return finalImport;
        }

        public List<Order> GetOrderList()
        {
            return this.orderList;
        }

        #endregion
    }
}


