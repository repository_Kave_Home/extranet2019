﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Communication
    {
        #region Private properties

        private string title;
        private string subtitle;
        private Client assignedTo;
        private string date;
        private string content;
        private string image;
        private string url;

        #endregion

        #region Public properties

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Subtitle
        {
            get { return subtitle; }
            set { subtitle = value; }
        }

        public Client AssignedTo
        {
            get { return assignedTo; }
            set { assignedTo = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public string Image
        {
            get { return image; }
            set { image = value; }
        }        
        
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        #endregion

        #region Constructor

        #endregion
    }
}
