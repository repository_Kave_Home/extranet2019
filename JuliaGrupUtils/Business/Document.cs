﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class Document
    {
        public int ObjectID=0;
        public string descriptionType = string.Empty;
        public string DocNo = string.Empty;
        public string DireccionEnvio = string.Empty;
        public string codiClient = string.Empty;

        public string itemNo = string.Empty;
        public int LineNo = 0;
        public string itemDescription = string.Empty;

        public DateTime FechaRegistro = new DateTime();
        
        public Document()
        {
          
        }
        public Document(JuliaWs.Documento p)
        {
            this.ObjectID = p.ObjectID;
            this.descriptionType = p.DescriptionType;
            this.DocNo = p.DocNo;
            this.FechaRegistro = p.FechaRegistro;
            this.DireccionEnvio = p.DireccionEnvio;
            this.codiClient = p.CodCliente;
        }


    }
}
