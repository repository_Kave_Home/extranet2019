﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Factura
    {
        #region Private properties
        private string name;
        private string code;
        private string description;
        private string document;
        private string date;

        #endregion

        #region Public Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Code
        {
            get { return code;}
            set { code = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Document
        {
            get { return document; }
            set { document = value; }
        }

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        #endregion

    }
}
