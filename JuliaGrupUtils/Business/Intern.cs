﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Intern
    {
        #region Private properties
        private string name;
        private string code;
        private string phone;
        private string tracking; //estado del pedido
        private string deliveryDate;
        private string outDate;
        private string stat; //si la entrega es total o parcial
        private List<Client> listClients;
        #endregion

        #region Public Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Code
        {
            get { return code;}
            set { code = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string DeliveryDate
        {
            get { return deliveryDate; }
            set { deliveryDate = value; }
        }

        public string OutputDate
        {
            get { return outDate; }
            set { outDate = value; }
        }

        public string Tracking
        {
            get { return tracking; }
            set { tracking = value; }
        }

        public string Stat
        {
            get { return stat; }
            set { stat = value; }
        }

        public List<Client> ListClients
        {
            get { return listClients; }
            set { listClients = value; }
        }
        #endregion

    }
}
