﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Issue
    {
        #region Private Properties



        #endregion

        #region Public Properties

        public string customerName = string.Empty;
        public string customerNumber = string.Empty;
        public string state = string.Empty;
        public DateTime StartDate = new DateTime();
        public DateTime EntranceDate = new DateTime();
        public DateTime EndDate = new DateTime();
        public DateTime CerradoDate = new DateTime();
        public string IssueNo = string.Empty;
        public string Reference = string.Empty;
        public string TipoResolucion = string.Empty;


        public string Descripcion = string.Empty;
        public string codDireccionEnvio = string.Empty;
        public decimal cantidad = 0;
        #endregion

        #region Constructor

        public Issue(){}
        public Issue(JuliaWs.Incidencia p)
        {
            this.customerName = p.CustomerName;
            this.customerNumber = p.CustomerNo;
            this.EndDate = p.FechaDecision;
            this.EntranceDate = p.FechaEntrada;
            this.IssueNo = p.NoIncidencia;
            this.Reference = p.Referencia;
            this.StartDate = p.FechaInicio;
            
            this.state = p.Estado;
            this.TipoResolucion = p.TipoResolucion;
            this.CerradoDate =p.FechaFinalizada;
        }
        
        

        #endregion
    }
}
