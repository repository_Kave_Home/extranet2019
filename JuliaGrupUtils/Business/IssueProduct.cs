﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class IssueProduct
    {
        public int ObjectID = 0;
        public string ItemNo = string.Empty;
        public string ItemDescription = string.Empty;
        public Double CantidadTotal = 0;
        public string Barcode = string.Empty;
        public string direccionEnvio = string.Empty;
        public int DocLine = 0;
        public IssueProduct(JuliaWs.Barcode Bar)
        {
            this.ObjectID = Bar.ObjectID;
            this.ItemNo = Bar.ItemNo;
            this.ItemDescription = Bar.ItemDescription;
            this.CantidadTotal = Convert.ToDouble(Bar.CantidadTotal);
            this.Barcode = Bar.Barcode1;
            this.direccionEnvio = Bar.DireccionEnvio;
            this.DocLine = Bar.DocLine;
        }

        public IssueProduct(JuliaWs.Producto Prod)
        {
            this.ItemNo = Prod.ItemNo;
            this.ItemDescription = Prod.ItemDescription;
            //this.CantidadTotal = Convert.ToDouble(Prod.CantidadTotal);
            //this.Barcode = Doc.Barcode1;
            //this.direccionEnvio = Doc.DireccionEnvio;
            //this.DocLine = Doc.DocLine;
        }

    }
}
