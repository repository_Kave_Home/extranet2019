﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
//using Microsoft.Office.Server.UserProfiles;
using Microsoft.SharePoint;
using JuliaGrupUtils.Utils;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class News
    {
        #region Private Properties

        private string title;
        private string subtitle;
        private string image;
        private string content;
        private string date;
        private string url;

        #endregion

        #region Public Properties

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Subtitle
        {
            get { return subtitle; }
            set { subtitle = value; }
        }
        
        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Image
        {
            get { return image; }
            set { image = value; }
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        #endregion
    }

    public class JuliaNews {
        public const string ContentTypeId = "JuliaNewsPage";

        public static DataTable GetLastNewsAsDataTable(uint news_number)
        {
            DataTable FilteredNewsdt;
            string viewfileds = "<FieldRef Name=\"Title\"/><FieldRef Name=\"ArticleStartDate\"/><FieldRef Name=\"PublishingPageImage\"/><FieldRef Name=\"EncodedAbsUrl\"/><FieldRef Name=\"FileRef\"/><FieldRef Name=\"PublishingContact\"/><FieldRef Name=\"AverageRating\"/>";
            viewfileds += "<FieldRef ID=\"{61cbb965-1e04-4273-b658-eedaa662f48d}\" />";
            //Campos añadidos para BPA
            viewfileds += "<FieldRef Name=\"PublishingRollupImage\" />";    //Página de Informe standard
            viewfileds += "<FieldRef Name=\"JuliaNewsRollupText\" />";    //Página de Informe standard
            viewfileds += "<FieldRef Name=\"{1390a86a-23da-45f0-8efe-ef36edadfb39}\" />";    //Enterprise Keywords

            string lists = "<Lists ServerTemplate=\"850\" />";
            FilteredNewsdt = SiteDataQuery.GetByContentTypeWholeSiteRecursive(news_number, viewfileds, "ArticleStartDate", false, lists, ContentTypeId, true, true);
            return FilteredNewsdt;
        }
    }
}
