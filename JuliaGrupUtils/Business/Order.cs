﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using Microsoft.SharePoint;

using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Order
    {
        #region private properties

        private string orderNumber = string.Empty;
        private Client orderClient;
        private List<OrderLine> productLists;
        private string customerName = string.Empty;
        private string orderObservations = string.Empty;
        private string referenciaPedido = string.Empty;
        private string paymentMethod = string.Empty;
        private string deliveryAddress = string.Empty;
        private string deliveryType = string.Empty;
        private decimal volumen = Decimal.Zero;
        private OfertasWs.Oferta navOferta;
        private DateTime fechaExpedicion;
        private string codFormaPago= string.Empty;
        private string codTerminosPago= string.Empty;
        private DateTime fechaRequerida;
        private string gdoBox = string.Empty;
        private Decimal gdoVolumenBox = decimal.Zero;
        private List<TipoContenedor> listaContenedores;
        private int TipoCompra = 0;
        private string divisa = "EUR";
        private string campaña = string.Empty;
        private string puntoRecogida = string.Empty;

        public OfertasWs.Oferta NavOferta
        {
            get { return navOferta; }
            set { navOferta = value; }
        }
        


        public int countProv = 0;

        #endregion

        #region Public Properties
        public decimal Volumen
        {
            get { return volumen; }
            set { volumen = value; }
        }
        public List<TipoContenedor> ListaContenedores
        {
            get { return listaContenedores; }
            set { listaContenedores = value; }
        }
      


        public string DeliveryType
        {
            get { return deliveryType; }
            set { deliveryType = value; }
        }
        public string DeliveryAddress
        {
            get { return deliveryAddress; }
            set { deliveryAddress = value; }
        }
        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        public string PaymentMethod
        {
            get { return paymentMethod; }
            set { paymentMethod = value; }
        }
   

        public string OrderNumber
        {
            get { return orderNumber; }
            set { orderNumber = value; }
        }

        public string ReferenciaPedido
        {
            get { return referenciaPedido; }
            set { referenciaPedido = value; }
        }
        public Client OrderClient
        {
            get { return orderClient; }
            set {
                /* Used in Client WebService calls */
                orderClient = value;
            }    
        }

        public string OrderObservations
        {
            get { return (!String.IsNullOrEmpty(orderObservations))?orderObservations:String.Empty; }
            set { this.orderObservations = value; }
        }
        public DateTime FechaExpedicion
        {
            get { return fechaExpedicion; }
            set { fechaExpedicion = value; }
        }
        public DateTime FechaRequerida
        {
            get { return fechaRequerida; }
            set { fechaRequerida = value; }
        }

        public string CodFormaPago
        {
            get { return codFormaPago; }
            set { codFormaPago = value; }
        }
        public string CodTerminosPago
        {
            get { return codTerminosPago; }
            set { codTerminosPago = value; }
        }
        public string GdoBox
        {
            get { return gdoBox; }
            set { gdoBox = value; }
        }
        public Decimal GdoVolumenBox
        {
            get { return gdoVolumenBox; }
            set { gdoVolumenBox = value; }
        }
   
      

        public List<OrderLine> ListOrderProducts
        {
            get {
                GetProductsList();
                return productLists; 
            }          
        }

        public string Divisa
        {
            get {
                return divisa;
            }
            set
            {
                divisa = value;
            }
        }

        public string Campaña
        {
            get {
                return campaña;
            }
            set
            {
                campaña = value;
            }
        }

        public string PuntoRecogida
        {
            get { return puntoRecogida; }
            set { this.puntoRecogida = value; }
        }

        public enum RecogidaAlmacen
        {
            almacen,
            cliente
        }

        #endregion

        #region Constructors

        public Order(OfertasWs.Oferta ofer, Client c)
        {
           
            //Check if we have some open order
            this.orderClient = c;
            this.navOferta = ofer;
            this.orderNumber = ofer.No;
            this.productLists = new List<OrderLine>();
            this.orderObservations = string.IsNullOrEmpty(ofer.Obsevaciones) ? string.Empty : ofer.Obsevaciones;
            this.referenciaPedido = string.IsNullOrEmpty(ofer.ReferenciaCliente) ? string.Empty : ofer.ReferenciaCliente;
            this.deliveryType = ofer.TipoEnvio.ToString();
            this.customerName = ofer.Sell_to_Customer_Name;
            //02012013 - REZ:: recuperamos Direccion Envio de la oferta
            this.deliveryAddress = ofer.DireccionEnvio;
            this.codFormaPago = ofer.CodFormaPago;
            this.codTerminosPago = ofer.CodTerminosPago;
            this.fechaRequerida = ofer.FechaRequerida;
            this.gdoBox = string.IsNullOrEmpty(ofer.GDOBox) ? string.Empty: ofer.GDOBox;
            this.GdoVolumenBox = ofer.GDOBoxVolumen;
            // RTP: Trasllado la carrega de les lines a la funció getlines
            ////load product list from the current order
            //ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(c.UserName, c.Code);

            OrderDataAccesObject orderDAO = new OrderDataAccesObject();
            this.listaContenedores = orderDAO.GetTipoContenedores();
            this.TipoCompra = (ofer.OrderType == OfertasWs.OrderType.GDO)?1:0;
            this.divisa = (ofer.CurrencyCode == "USD") ? "USD" : "EUR";
            //REZ 25072014 - Campaña externa
            this.campaña = string.IsNullOrEmpty(ofer.Campaña_externa) ? string.Empty : ofer.Campaña_externa;

            this.puntoRecogida = string.IsNullOrEmpty(ofer.PuntoRecogida) ? string.Empty : ofer.PuntoRecogida.ToLower();
        }

        #endregion

        #region Public functions

        public void AddProduct(Article p, int quantity, string linedescription, string username)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.AddOrderLine(this, p, quantity, linedescription, username);
              
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }

        }

        public void AddInspirate(string username, string catalog, string orderNo)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.InsertInspirate(this, username, catalog, orderNo);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }

        }

        //public void RefreshOrderProductList()
        //{
        //    ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(this.orderClient.UserName, this.orderClient.Code);
        //    this.productLists = new List<OrderLine>();
        //    foreach (OfertasWs.Sales_Quote_Line line in this.NavOferta.SalesLines)
        //    {
        //        this.AddLineToOrder(line, articleDAO);
        //    } 
        //}

        public void SetProductByLineNo(int orderlineNo, int Quantity, string linedescription, string username)
            //(Article product, int quantity, string linedescription, string username, int LineNumber)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                //orderDAO.UpdateOrderLine(this, orderLine, linedescription, username);
                orderDAO.UpdateOrderLineByLineNo(this, orderlineNo, Quantity, linedescription, username);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void SetProduct(Article product, int quantity, string linedescription, string username, int LineNumber)
        {
            try
            {
                //if (this.productLists.Count() < 1)
                //{
                //    List<OrderLine> ols = GetProductsList();
                //}
                //OrderLine orderLine = this.productLists.AsQueryable().First(ol => ol.LineNo == LineNumber);
                OrderLine orderLine = this.ListOrderProducts.AsQueryable().First(ol => ol.LineNo == LineNumber);
                orderLine.Quantity = quantity;

                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.UpdateOrderLine(this, orderLine, linedescription, username);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }


        public void DeleteProductByLineNo(int orderlineNo, string username)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.DeleteOrderLineByLineNo(this, orderlineNo, username);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
        }

        public void DeleteProduct(int position, string username)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();

                OrderLine op = this.ListOrderProducts[position];
                //if (this.productLists.Count() < (position + 1))
                //{
                //    List<OrderLine> ols = GetProductsList();
                //}
                //OrderLine op = this.productLists[position];
                orderDAO.DeleteOrderLine(this, op, username);
             }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
        }


        public void DeleteAllProducts(string username)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.DeleteOrderLines (this,username);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
        }

        public void Update(string username)
        {
            try
            {
                OrderDataAccesObject orderDAO = new OrderDataAccesObject();
                orderDAO.UpdateOrder(this, username);
            }
            catch (NavException ex)
            {
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw ex;
            }
        }
        public void IncreaseProduct(Article product)
        {
            try
            {
                //OrderLine orderLine = this.productLists.AsQueryable().First(ol => ol.Product.Code == product.Code);
                OrderLine orderLine = this.ListOrderProducts.AsQueryable().First(ol => ol.Product.Code == product.Code);
                orderLine.Quantity++;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public bool HasProduct(Article product)
        {
            //return (productLists.AsQueryable().First(ol => ol.Product.Code == product.Code) != null);
            return (ListOrderProducts.AsQueryable().First(ol => ol.Product.Code == product.Code) != null);
        }

        public double GetOrderPrice()
        {
            double result = 0;
            try
            {
                //var orderPrice = productLists.AsQueryable().Sum(p => p.GetProductPrice());
                var orderPrice = ListOrderProducts.AsQueryable().Sum(p => p.GetProductPrice());
                string sValue = string.Format("{0:#,###0.00}", orderPrice); // 2 decimales
                result= Convert.ToDouble(sValue);
             }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG , JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;   
        }

        public double getIVA()
        {
            double result = 0;
            try
            {
                double priceIVA = GetOrderPrice();
                priceIVA = (priceIVA * 18) / 100;

                string price = String.Format("{0:0.00}", priceIVA);
                result = double.Parse(price);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG , JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result; 
        }

        public double GetOrderFinalPrice()
        {
            double result = 0;
            try
            {
                double priceNoIva = GetOrderPrice();
                double priceDiscount = GetOrderDiscount();
                double priceIVA = getIVA();
                double finalPrice = priceNoIva - priceDiscount + priceIVA;

                string price = String.Format("{0:0.00}", finalPrice);
                result= double.Parse(price);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result; 
        }

        public double GetOrderDiscount()
        {
            double result = 0;
            try
            {
                //double discount = productLists.AsQueryable().Sum(p => p.GetProductDiscount());
                double discount = ListOrderProducts.AsQueryable().Sum(p => p.GetProductDiscount());

                string price = String.Format("{0:0.00}", discount);
                result = double.Parse(price);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result; 
        }

        public List<OrderLine> GetProductsList()
        {
            try 
            {
                ////load product list from the current order
                ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(this.orderClient.UserName, this.orderClient.Code, this.TipoCompra);
                this.productLists.Clear ();
                foreach (OfertasWs.Sales_Quote_Line line in this.navOferta.SalesLines)
                {
                    this.AddLineToOrder(line, articleDAO);
                }     
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return this.productLists;
        }

        public bool SendOrderToNAV()
        {
            return true;
        }

        #endregion

        #region private functions
        private void AddLineToOrder(OfertasWs.Sales_Quote_Line line, ArticleDataAccessObject articleDAO)
        {
            try
            {
                if (line.No != null)
                {
                    //get the article
                    //Article a = articleDAO.GetProductByCode(line.No);
                    Article a = articleDAO.GetProductByCodeAndCatalog(line.No, "", line.Catalogue_No);

                    if (a != null)
                    {
                        string type = line.Type != null ? line.Type.ToString() : string.Empty;
                        OrderLine ol = new OrderLine((int)line.Quantity, a, (double)line.Line_Discount_Percent, line.Description, line.CantidadPendiente, line.ListaAlbaranes, line.Volumen, type, line.Line_no,line.Semana,line.UnitPrice,line.PesoNeto);
                        ol.Price = (double)line.Line_Amount;
                        ol.Points = (decimal)line.Points;
                        ol.OrderLineKey = line.Key;
                        ol.LineNo = line.Line_no;
                        ol.LineDescription = line.Your_Reference;
                        ol.CantidadPendiente = line.CantidadPendiente;
                        ol.Volumen = line.Volumen;
                        ol.Albaranes = line.ListaAlbaranes;
                        ol.Type = line.Type != null ? line.Type.ToString() : string.Empty;
                        ol.UnitPrice = line.UnitPrice;
                        ol.PesoNeto = line.PesoNeto;
                  //line.UnitPrice  
                    //line.VolumenUnitario  
                        //line.PesoNeto
                        this.productLists.Add(ol);
                    }
                    else
                    {
                        Article p = new Article(line.No,line.Description);
                        OrderLine ol = new OrderLine((int)line.Quantity, p, (double)line.Line_Discount_Percent, line.Description, line.Line_no);
                        ol.Price = (double)line.Line_Amount;
                        ol.OrderLineKey = line.Key;
                        ol.LineNo = line.Line_no;
                        ol.LineDescription = line.Your_Reference;
                        ol.CantidadPendiente = line.CantidadPendiente;
                        ol.Albaranes = line.ListaAlbaranes;
                        ol.Points = line.Points;
                        ol.SemanasExpedicion = line.Semana;
                        this.productLists.Add(ol);
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        #endregion
    }
}
