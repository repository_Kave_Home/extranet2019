﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JuliaGrupUtils.Utils;
using System.Diagnostics;
using System.Runtime.Serialization;


namespace JuliaGrupUtils.Business
{
    [Serializable]
    [DataContract]
    public class OrderLine
    {
        #region Private Properties

        private int quantity;
        private Article product;
        private double discount;
        private string lineDescription;
        private int lineNo;
        private double price;
        private decimal points;
        private string type;
        private string orderLineKey;
        private decimal cantidadPendiente;
        private string albaranes;
        private decimal volumen;
        private Article a;
        private DateTime fechaExpedicion;
        private int semanasExpedicion;
        private Decimal unitPrice;
        private Decimal pesoNeto;
        private string grupoProveedor;

        #endregion

        #region Public Properties

        /* Client Binding Properties*/

        
        [DataMember]
        public string ArticleCode
        {
            get { return product.Code; }
            set { }
        }
        
        [DataMember]
        public string Description
        {
            get { return product.Description; }
            set {  }
        }
        
        [DataMember]
        public int Num
        {
            get { return quantity; }
            set { quantity = value; }
        }
        [DataMember]
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        /* End Client Properties */

        [DataMember]
        public int LineNo
        {
            get { return lineNo; }
            set { lineNo = value; }
        }
        
        public string OrderLineKey
        {
            get { return orderLineKey; }
            set { orderLineKey = value; }
        }
        
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        
        public decimal Points
        {
            get { return points; }
            set { points = value; }
        }
        /*
        [DataMember]
        public double Price
        {
            get { return price; }
            set { price = value; }
        }
        */
        
        public decimal Volumen
        {
            get { return volumen; }
            set { volumen = value; }
        }
        
        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        /*[DataMember]*/
        public Article Product
        {
            get { return product; }
            set { product = value; }
        }
        [DataMember]
        public double Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public string LineDescription
        {
            get { return lineDescription; }
            set { lineDescription = value; }
        }
        public decimal CantidadPendiente
        {
            get { return cantidadPendiente; }
            set { cantidadPendiente = value; }
        }
        public string Albaranes
        {
            get { return albaranes; }
            set { albaranes = value; }
        }
        public DateTime FechaExpedicion
        {
            get { return fechaExpedicion; }
            set { fechaExpedicion = value; }
        }
        public int SemanasExpedicion
        {
            get { return semanasExpedicion; }
            set { semanasExpedicion = value; }
        }
        public Decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        public Decimal PesoNeto
        {
            get { return pesoNeto; }
            set { pesoNeto = value; }
        }
 
        #endregion

        #region Constructor

        //public OrderLine(int quantity, Article product, double discountPct, string lineDescription,decimal CantidadPendiente, string Albaranes )
        //{
        //    this.quantity = quantity;
        //    this.product = product;
        //    this.discount = discountPct;
        //    this.lineDescription = lineDescription;
        //    this.albaranes = Albaranes;
        //    this.cantidadPendiente = CantidadPendiente;

        //}
        public OrderLine(int quantity, Article product, double discountPct, string lineDescription, decimal CantidadPendiente, string Albaranes, decimal Volumen,  string Type, int LineNo, Decimal UnitPrice,Decimal PesoNeto)
        {
            this.quantity = quantity;
            this.product = product;
            this.discount = discountPct;
            this.lineDescription = lineDescription;
            this.cantidadPendiente = CantidadPendiente;
            this.albaranes = Albaranes;
            this.volumen = Volumen;
            this.type = Type;
            this.lineNo = LineNo;
            this.pesoNeto = PesoNeto;
            this.unitPrice = UnitPrice;
        }
        public OrderLine(int quantity, Article product, double discountPct, string lineDescription, decimal CantidadPendiente, string Albaranes, decimal Volumen, string Type, int LineNo, int SemanasExpedicion,Decimal UnitPrice,Decimal PesoNeto)
        {
            this.quantity = quantity;
            this.product = product;
            this.discount = discountPct;
            this.lineDescription = lineDescription;
            this.cantidadPendiente = CantidadPendiente;
            this.albaranes = Albaranes;
            this.volumen = Volumen;
           
            this.type = Type;
            this.lineNo = LineNo;
            this.semanasExpedicion = SemanasExpedicion;
            this.pesoNeto = PesoNeto;
            this.unitPrice = UnitPrice;
            
        }
        public OrderLine(int quantity, Article product, double discountPct, string lineDescription, int LineNo)
        {
            this.quantity = quantity;
            this.product = product;
            this.discount = discountPct;
            this.lineDescription = lineDescription;
            this.lineNo = LineNo;
        }

        //public OrderLine(int p, Article a, double p_2, string p_3, decimal p_4, string p_5)
        //{
        //    // TODO: Complete member initialization
        //    this.p = p;
        //    this.a = a;
        //    this.p_2 = p_2;
        //    this.p_3 = p_3;
        //    this.p_4 = p_4;
        //    this.p_5 = p_5;
        //}

        #endregion

        #region Public methods

        public double GetProductPrice()
        {
            Double result = 0;
            try
            {
                double totalprice = (this.quantity * this.product.Price);
                double auxTotal = totalprice - (totalprice * (this.discount / 100));
                string dd = auxTotal.ToString("f2");
                result= Double.Parse(dd);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public double GetProductDiscount()
        {
            double productDiscount = 0;
            try
            {
                productDiscount = (this.discount * this.quantity);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return productDiscount;
        }

        #endregion
    }
}
