﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class OrderProduct
    {
        #region Private Properties
        private int quantity;
        private Product product;
        private double discount;
        #endregion

        #region Public Properties
        public int Quantity
        {
            get { return quantity; }
            //set { quantity = value; }
        }

        public Product Product
        {
            get { return product; }
            //set { product = value; }
        }

        public double Discount
        {
            get { return discount; }
        }
        #endregion

        #region Class Creator
        public OrderProduct(int quantity, Product product, double discountPct)
        {
            this.quantity = quantity;
            this.product = product;
            this.discount = discountPct;
        }
        #endregion

        #region Public methods
        public double GetProductPrice()
        {
            Double result =0;
            try
            {
                double totalprice = (this.quantity * this.product.Price);
                double auxTotal = totalprice - (totalprice * (this.discount / 100));
                string dd = auxTotal.ToString("f2");
                result= Double.Parse(dd);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        
        }

        public double GetProductDiscount()
        {
            double productDiscount = (this.discount * this.quantity);
            return productDiscount;
        }
        #endregion
    }
}
