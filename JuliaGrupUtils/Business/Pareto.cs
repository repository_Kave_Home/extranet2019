﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class Pareto
    {
        //FORMULARI DE NOVA INCIDENCIA
        //crear propietats segons Paretos p
        public bool Adjunto = false;
        public bool Descripcion = false;
        public bool Direccion = false;
        public bool Doc = false;
        public bool Matricula = false;
        public bool Product = false;
        public bool RefCliente = false;
        public bool Unidades = false;
        public bool ImagenDefecto = false;
        public int VistaFormID = 0;

        public string Type = string.Empty;
        public string TypeDescription = string.Empty;
        public string SubType = string.Empty;
        public string SubTypeDescription = string.Empty;
        public string ClientReference = string.Empty;
      

        public Pareto()
        {
          
        }
        public Pareto(JuliaWs.Paretos p)
        {
            this.Adjunto = p.Adjunto;
            this.Descripcion = p.CampoDescripcion;
            this.Direccion = p.CampoDireccion;
            this.Doc = p.CampoDoc;
            this.Matricula = p.CampoMatricula;
            this.Product = p.CampoProducto;
            this.RefCliente = p.CampoRefCliente;
            this.Unidades = p.CampoUnidades;

            this.VistaFormID = p.VistaFormID;

            this.Type = p.Type;
            this.TypeDescription = p.TypeDescription;
            this.SubType = p.SubType;
            this.SubTypeDescription = p.SubTypeDescription;
            this.ImagenDefecto = p.CampoImagenDefecto;
        }
    }
}
