﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Product
    {
        #region Private properties
        private string code;
        private double price;
        private string name;
        private string description;
        private string type;
        private bool principal;
        private string instruccions;
        private string image;
       // private  nformacionmetadata;
        #endregion

        #region Public Properties
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public bool Principal
        {
            get { return principal; }
            set { principal = value; }
        }

        public string Instruccions
        {
            get { return instruccions; }
            set { instruccions = value; }
        }
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        #endregion

     

        public Product(string code, string name, double price, string description, string type)
        {
            this.code = code;
            this.name = name;
            this.price = price;
            this.description = description;
            this.type = type;
        }
    }
}
