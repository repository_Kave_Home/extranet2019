﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;
using JuliaGrupUtils.Utils;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    [DataContract]
    public class ProductDocument 
    {        
        #region Private properties
            string icon = String.Empty;
            string link = String.Empty;
            string name = String.Empty;
            string contentype = String.Empty;
        #endregion

        #region Public Properties
        
        [DataMember]
        public string Icon
        {
            get { return this.icon; }
            set { }
        }
        [DataMember]
        public string Link
        {
            get { 
                    return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=document&id=" + link;
            }
            set { }
        }
        [DataMember]
        public string Name
        {
            get { 
                    return name;
            }
            set { }
        }
        [DataMember]
        public string ContentType
        {
            get
            {
                return contentype;
            }
            set { }
        }

        #endregion

        #region Constructor

        public ProductDocument(string stricon, string strlink, string strname, string strcontentype)
        {
            this.icon = stricon;
            this.link = strlink;
            this.name = strname;
            this.contentype = strcontentype;
        }


        #endregion
    }

}
