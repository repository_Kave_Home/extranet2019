﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class ProductElement : SlideElement
    {
        private Article article;
        public Article Article
        {
            get { return article; }
            set { article = value; }
        }

        public ProductElement()
        { }

        public ProductElement(Article _article)
        {
            this.article = _article;
        }

        public string GetCode()
        {
            if (this.article != null)
                return article.Code;
            return string.Empty;
        }

        public string Description()
        {
            if (this.article != null)
                return this.article.Description;
            return string.Empty;
        }

        public string Price()
        {
            if (this.article != null)
                return this.article.Price.ToString();
            return string.Empty;
        }

        public string Points()
        {
            if (this.article != null)
                return this.article.PriceInPoints.ToString();
            return string.Empty;
        }

        public void SetUrl()
        {
            if(this.article != null)
                this.url = "/loquesea?product=" + this.article.Code;
        }
    }
}
