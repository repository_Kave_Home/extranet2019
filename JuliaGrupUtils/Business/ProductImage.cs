﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;
using JuliaGrupUtils.Utils;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    [DataContract]
    public class ProductImage 
    {        
        #region Private properties
            string link = String.Empty;
            string name = String.Empty;
            string bigurl = String.Empty;
            string midurl = String.Empty;
            string _encodedurl = String.Empty;
        #endregion

        #region Public Properties
        
        [DataMember]
        public string Link
        {
            get { 
                    return link;
            }
            set { }
        }
        [DataMember]
        public string Name
        {
            get { 
                    return name;
            }
            set { }
        }

        [DataMember]
        public string BigUrl
        {
            get {
                return bigurl;
            }
            set { }
        }

        [DataMember]
        public string MidUrl
        {
            get {
                return midurl;
            }
            set { }
        }

        [DataMember]
        public string encodedurl
        {
            get {
                return _encodedurl;
            }
            set { }
        }


        #endregion

        #region Constructor

        public ProductImage(string strlink, string strname)
        {
            this.link = strlink;
            this.name = strname;
            this.bigurl = strlink.Replace(" ", "%20").Replace("·1", "·3");
            this.midurl = strlink.Replace(" ", "%20").Replace("·1", "·2");
            this._encodedurl = strlink.Replace(" ", "%20");
        }


        #endregion
    }

}
