﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class PromotionElement : SlideElement
    {
        private string onImageUrl;
        public string OnImageUrl
        {
            get { return onImageUrl; }
            set { onImageUrl = value; }
        }
        
        private string offImageUrl;
        public string OffImageUrl
        {
            get { return offImageUrl; }
            set { offImageUrl = value; }
        }

        private string code;
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private DateTime caducity;
        public DateTime Caducity
        {
          get { return caducity; }
          set { caducity = value; }
        }

        public PromotionElement()
        {
            caducity = new DateTime();

            UpdateImageToShow();
        }

        public PromotionElement(string _code, string _offImageUrl, string _onImageUrl, DateTime _caducity)
        {
            code = _code;
            offImageUrl = _offImageUrl;
            onImageUrl = _onImageUrl;
            caducity = _caducity;
            
            UpdateImageToShow();
        }

        public void UpdateImageToShow()
        {
            if(this.caducity != null || this.caducity.CompareTo(DateTime.Now) <= 0)
                this.imageURL = onImageUrl;
            else
                this.imageURL = offImageUrl;
        }

        public void SetUrl() 
        {
            this.url = "/loquesea?promotion=" + code;
        }
    }
}
