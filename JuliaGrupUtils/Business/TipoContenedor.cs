﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using Microsoft.SharePoint;

using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class TipoContenedor
    {
        private string code = string.Empty;
        private string description = string.Empty;
        private decimal height = decimal.Zero;
        private string key = string.Empty;
        private decimal lenght = decimal.Zero;
        private string volumen = string.Empty;
        private decimal weight = decimal.Zero;
        private decimal width = decimal.Zero;
        private string userDescription = string.Empty;
        private string messageContenedor = string.Empty;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public Decimal Height
        {
            get { return height; }
            set { height = value; }
        }
        public string Key
        {
            get { return key; }
            set { key = value; }
        }
        public Decimal Lenght
        {
            get { return lenght; }
            set { lenght = value; }
        }
        public string Volumen
        {
            get { return volumen; }
            set { volumen = value; }
        }
        public Decimal Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        public Decimal Width
        {
            get { return width; }
            set { width = value; }
        }
        public string UserDescription
        {
            get { return this.volumen + " - " + this.code; }
         
        }
        public string MessageContenedor
        {
            get { return messageContenedor; }

        }
        
         public TipoContenedor(ListaContenedoresWs.ListaContenedores tipo, string Lcid)
        {
            this.code = tipo.Code;
            this.description = tipo.Description;
            this.height = tipo.Height;
            this.key = tipo.Key;
            this.lenght = tipo.Length;
            this.volumen = tipo.Volumen;
            this.weight = tipo.Weight;
            this.width = tipo.Width;
            //this.userDescription = tipo.Code + " - " + tipo.Code;
            switch (Lcid)
            {
                case "1027":
                    this.messageContenedor = tipo.Item1027;
                    break;
                case "3082":
                    this.messageContenedor = tipo.Item3082;
                    break;
                case "1036":
                    this.messageContenedor = tipo.Item1036;
                    break;
                case "1040":
                    this.messageContenedor = tipo.Item1040;
                    break;
                case "1033":
                    this.messageContenedor = tipo.Item1033;
                    break;
                case "1031":
                    this.messageContenedor = tipo.Item1031;
                    break;
                default:
                    this.messageContenedor = tipo.Item3082;
                    break;
            }
            
        } 
    }
}
