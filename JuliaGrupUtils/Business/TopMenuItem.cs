﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;


namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class TopMenuItem
    {

        #region private properties
            string  _tipo;
            string  _codigo;
            string  _descripcion;
            string  _parentCode;
            int     _order;
        #endregion

        #region public properties
            [DataMember]
            public string Tipo 
            {
                get { return _tipo; }
                set {}
            }
            [DataMember]
            public string Codigo
            {
                get { return _codigo.ToUpper(); }
                set { }
            }
            [DataMember]
            public string Descripcion
            {
                get { return _descripcion; }
                set { }
            }
            [DataMember]
            public string ParentCode
            {
                get { return _parentCode; }
                set { }
            }
            [DataMember]
            public int Order
            {
                get { return _order; }
                set { }
            }
        #endregion



        public TopMenuItem(string tipo, string codigo, string descripcion, string parentCode, int order)
        {
            this._tipo = tipo;
            this._codigo = codigo;
            this._descripcion = descripcion;
            this._parentCode = parentCode;
            this._order = order;
        
        }
    }
}
