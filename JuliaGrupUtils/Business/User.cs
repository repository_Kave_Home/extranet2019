﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Data.SqlClient;
using System.Configuration;
using JuliaGrupUtils.DataAccessObjects;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class User
    {
        #region Private properties

        private string username;
        private string type;
        private string accesstype;
        private string language;
        private bool loaded = false;
        private string code;
        private string email;
        private decimal coeficient;
        private DateTime fechacaducidadpassword;
        private bool verCantidades;
        private decimal dolarCoef = decimal.Zero;
        private bool userhasgdoacces = false;
        private bool userhaslaformaacces = false;
        private bool gdoPVPUser = false;
        private decimal coeficientGDOPVP;
        private bool accesoComingSoon;
        private bool shopInShop;

        #endregion
        public bool GdoPVPUser
        {
            get { return gdoPVPUser; }
            set { gdoPVPUser = value; }
        }

        public bool AccesoComingSoon
        {
            get { return accesoComingSoon; }
            set { accesoComingSoon = value; }
        }

        public bool ShopInShop
        {
            get { return shopInShop; }
            set { shopInShop = value; }

        }
        #region virtual Public Properties


        public bool Loaded
        {
            get { return loaded; }
            set { loaded = value; }
        }
        public virtual string UserName
        {
            get { return username; }
            set { username = value; }
        }

        public virtual string Type
        {
            get { return type; }
            set { type = value; }
        }
        public virtual string AccessType
        {
            get { return accesstype; }
            set { accesstype = value; }
        }
        public virtual string Language
        {
            get { return language; }
            set { language = value; }
        }
        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }
        public virtual decimal Coeficient
        {
            get { return coeficient; }
            set { coeficient = value; }
        }

        
        public virtual decimal CoeficientGDOPVP
        {
            get { return coeficientGDOPVP; }
            set { coeficientGDOPVP = value; }
        }

        public virtual DateTime FechaCaducidadPassword
        {
            get { return fechacaducidadpassword; }
            set { fechacaducidadpassword = value; }
        }
        public virtual bool VerCantidades
        {
            get { return verCantidades; }
            set { verCantidades = value; }
        }
        public virtual Decimal DolarCoef
        {
            get { return dolarCoef; }
            set { dolarCoef = value; }
        }

        public virtual bool UserHasGdoAcces
        {
            get { return userhasgdoacces; }
            set { userhasgdoacces = value; }
        }

        public virtual bool UserHasLaFormaAcces
        {
            get {
                return !this.gdoPVPUser;
            }// userhaslaformaacces; }
            set { userhaslaformaacces = value; }
        }
        #endregion
    }


}
