﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    public class UsuariosWEB
    {
        public bool bloqueado = false;
        public string customerNo = string.Empty;
        public string customerName = string.Empty;
        public string mail = string.Empty;
        public DateTime fehcaCaducidadPsw = new DateTime();
        public DateTime fechaCreacion = new DateTime();
        public DateTime fechaUltimoRegistro = new DateTime();
        public string tipo = string.Empty;
        public string tipoAcceso = string.Empty;
        public string usuario = string.Empty;
        public decimal coeficiente = Decimal.Zero;
        public bool accesoGdo = false;
        public bool verStock = false;
        public bool shopInShop = false;  /* 2019-07-16 - Aida Lucha - Shop in shop*/

        public UsuariosWEB(JuliaWs.UsuarioWEB usuario)
        {
            this.bloqueado = usuario.Bloqueado;
            this.customerNo = usuario.CustomerNo;
            this.fehcaCaducidadPsw = usuario.FechaCaducidadPwd;
            this.fechaCreacion = usuario.FechaCreacion;
            this.fechaUltimoRegistro = usuario.FechaUltimoRegistro;
            this.mail = usuario.Mail;
            this.tipo = usuario.Tipo;
            this.tipoAcceso = usuario.TipoAcceso;
            this.usuario = usuario.Usuario;
            this.coeficiente =usuario.Coef;
            this.customerName = usuario.CustomerName;
            this.accesoGdo = usuario.AccesoGDO;
            this.verStock = usuario.VerCantidadesStock;
            this.shopInShop = usuario.ShopInShop; /* 2019-07-16 - Aida Lucha - Shop in shop*/


            //this.customerName = usuario.CustomerName;
        }
    }
}
