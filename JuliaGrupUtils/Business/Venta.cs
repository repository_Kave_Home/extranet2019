﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Venta
    {
        #region private properties
            private Client client;
            private string clientName;
            private string clientCode;
            private string number;
            private string observations;
            private string reference;
            private string paymentMethod;
            private string deliveryAddress;
            private string deliveryType;
            private ListaVentasWs.ListaVentas navVenta;
            private DateTime fecha;
            private string volumen;
            private decimal puntos;
            private string estado;
            private int type;
            private decimal importe;
            private string descripcion;
            private DateTime fechaExpedicion;
            private string divisa = "EUR";
        #endregion

        #region Public Properties
        public string ClientName 
        {
            get { return clientName; }
            set { clientName = value; }   
        }
        public string ClientCode
        {
            get { return clientCode; }
            set { clientCode = value; }
        } 
        public string Number
        {
            get { return  number; }
            set { number = value; }
        }
        public string DeliveryType
        {
            get { return deliveryType; }
            set { deliveryType = value; }
        }
        public string DeliveryAddress
        {
            get { return deliveryAddress; }
            set { deliveryAddress = value; }
        }    
        public string PaymentMethod
        {
            get { return paymentMethod; }
            set { paymentMethod = value; }
        }
        public string Reference
        {
            get { return reference; }
            set { reference = value; }
        }
        //public Client Client
        //{
        //    get { return client; }
        //}
        public string Observations
        {
            get { return observations; }
            set { this.observations = value; }
        }
        public DateTime FechaExpedicion
        {
            get { return fechaExpedicion; }
            set { this.fechaExpedicion = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set { this.fecha = value; }
        }
        public string Volumen
        {
            get { return volumen; }
            set { this.volumen = value; }
        }
        public decimal Puntos
        {
            get { return puntos; }
            set { this.puntos = value; }
        }
        public string Estado
        {
            get { return estado; }
            set { this.estado = value; }
        }
        public Decimal Importe
        {
            get { return importe; }
            set { importe = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public string VentaType()
        {
            string ventaType = "";

            switch (this.type)
            {
                case (int)Venta.Types.Oferta:
                    ventaType = "oferta";
                    break;
                case (int)Venta.Types.Pedido:
                    ventaType = "pedido";
                    break;
            }
            return ventaType;
        }
        public enum Types { Pedido, Oferta };

        public ListaVentasWs.ListaVentas NavVenta
        {
            get { return navVenta; }
            set { navVenta = value; }
        }

        public int countProv = 0;

        public string Divisa
        {
            get
            {
                return divisa;
            }
            set
            {
                divisa = value;
            }
        }
        #endregion

        #region Constructors

        public Venta(ListaVentasWs.ListaVentas venta)
        {
            //Check if we have some open order
            this.clientName = venta.Sell_to_Customer_Name;
            this.clientCode = venta.Sell_to_Customer_No;
            this.number = venta.No;
            this.observations = string.Empty; 
            this.reference = venta.ReferenciaCliente;
            this.paymentMethod = string.Empty;
            this.deliveryAddress = string.Empty;
            this.deliveryType = string.Empty;
            this.navVenta = venta;
            this.fecha = venta.OrderDate;    //REZ:: Ha de ser OrderDate    
            
            //necesito el volum
            this.volumen = (venta.TotalVolumen != null) ? venta.TotalVolumen.ToString() : "";
            //necesito els punts
            this.puntos = venta.Point;   //REZ
            //necesito l'estat
            this.estado = venta.StatusWEB.ToString();   //venta.Status.ToString();
            if (venta.Document_Type.ToString().ToLower().Trim() == "quote")
                this.type = (int)Types.Oferta;
            else if (venta.Document_Type.ToString().ToLower().Trim() == "order")
                this.type = (int)Types.Pedido;
            this.importe = (Decimal)venta.TotalImportesConIVA;
            this.descripcion = "";
            this.fechaExpedicion = venta.Fechaexpedicion;
            //decimal total = venta.SalesLines.Sum(a => a.Unit_Price);
            this.divisa = (venta.CurrencyCode == "USD") ? "USD" : "EUR";
           
        } 

        #endregion
    }
}
