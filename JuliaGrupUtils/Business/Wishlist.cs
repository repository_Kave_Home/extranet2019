﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using System.Web;
using Microsoft.SharePoint;

using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupUtils.Business
{
    [Serializable]
    public class Wishlist
    {
        #region Private Properties

        private List<WishlistLine> wishlistLines;
        private string customerName = string.Empty;
        private string clientCode;

        #endregion

        #region Public Properties

        public List<WishlistLine> WishlistLines
        {
            get { return wishlistLines; }
            set { }
        }

        public string custName
        {
            get { return customerName; }
            set { }
        }

        public string clCode
        {
            get { return clientCode; }
            set { }
        }

        #endregion

        #region Constructors

        public Wishlist()
        {
            this.wishlistLines = new List<WishlistLine>();
        }
        #endregion

        #region Public Methods

        public void AssignUserAndCode(string username, string code)
        {
            this.customerName = username;
            this.clientCode = code;
        }

        public void UpdateProductQuantity(string code, int quantity)
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            wishlistDAO.UpdateWishListLine(this.custName, this.clCode, code, quantity);
            this.ChangeProductQuantity(code, quantity);
            HttpContext.Current.Cache.Remove("Wishlist_" + this.custName + "_" + this.clCode);
            HttpContext.Current.Cache.Add("Wishlist_" + this.custName + "_" + this.clCode, this, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
        }

        public void AddProductToWishlist(Article p)
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            wishlistDAO.AddWishListLine(this, this.custName, this.clCode, p.Code, p.CatalogNo, (int)p.UnidadMedida);
        }

        public void AddToCart(string pOfertaNo)
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            wishlistDAO.Wish_AddCart(this.custName, this.clCode, pOfertaNo);
        }

        public void DeleteWishlistItem(string code)
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            wishlistDAO.DeleteWishListLine(this.custName, this.clCode, code);
            this.wishlistLines.Remove(this.wishlistLines.AsQueryable().FirstOrDefault(ol => ol.Product.Code == code));
        }

        public void DeleteWishlistAll()
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            wishlistDAO.DeleteWishList(this.custName, this.clCode);
            this.wishlistLines.Clear();
        }

        public void AddItemToCart(string currentOrderID, List<string> articleCodes)
        {
            WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
            foreach (string code in articleCodes)
            {
                wishlistDAO.Wish_AddItemToCart(this.custName, this.clCode, currentOrderID, code);
                DeleteWishlistItem(code);
            }
            wishlistDAO.Wish_EndInsertCart(this.custName, this.clCode, currentOrderID);
        }

        public List<WishlistLine> GetWishlist()
        {
            try
            {
                WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
                List<WishlistLine> list = wishlistDAO.GetCurrentNavWishlist(this.customerName, this.clientCode);
                this.wishlistLines = list;
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return this.wishlistLines;
        }

        #endregion

        #region Private Methods

        private void ChangeProductQuantity(string code, int quantity)
        {
            this.wishlistLines.AsQueryable().First(ol => ol.Product.Code == code).Quantity = quantity;
        }

        #endregion
    }
}
