﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JuliaGrupUtils.Utils;
using System.Diagnostics;
using System.Runtime.Serialization;
using JuliaGrupUtils.DataAccessObjects;

namespace JuliaGrupUtils.Business
{
    [Serializable]
    [DataContract]
    public class WishlistLine
    {
        #region Private Properties

        private int quantity;
        private Article product;
        private int lineNo;
        private DateTime insertDate;
        private bool verQty;
        private bool stock;
        private bool cantidadVerde;
        private bool cantidadAzul;
        private bool cantidadRojo;
        private bool cantidadGris;
        private bool niveldeServicio;
        private bool discount;
        private bool isNew;

        #endregion

        #region Public Properties

        [DataMember]
        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        [DataMember]
        public Article Product
        {
            get { return product; }
            set { }
        }

        [DataMember]
        public int LineNo
        {
            get { return lineNo; }
            set { lineNo = value; }
        }

        [DataMember]
        public DateTime InsertDate
        {
            get { return insertDate; }
            set { insertDate = value; }
        }

        [DataMember]
        public bool VerQty
        {
            get { return verQty; }
            set { verQty = value; }
        }

        [DataMember]
        public bool Stock
        {
            get { return stock; }
            set { stock = value; }
        }

        [DataMember]
        public bool CantidadVerde
        {
            get { return cantidadVerde; }
            set { cantidadVerde = value; }
        }

        [DataMember]
        public bool CantidadAzul
        {
            get { return cantidadAzul; }
            set { cantidadAzul = value; }
        }

        [DataMember]
        public bool CantidadRojo
        {
            get { return cantidadRojo; }
            set { cantidadRojo = value; }
        }

        [DataMember]
        public bool CantidadGris
        {
            get { return cantidadGris; }
            set { cantidadGris = value; }
        }

        [DataMember]
        public bool NiveldeServicio
        {
            get { return niveldeServicio; }
            set { niveldeServicio = value; }
        }

        [DataMember]
        public bool Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        [DataMember]
        public bool IsNew
        {
            get { return isNew; }
            set { isNew = value; }
        }

        #endregion

        #region Constructor
        
        public WishlistLine(Article product)
        {
            this.product = product;
            this.quantity = (int)product.UnidadMedida;
            this.insertDate = DateTime.Now;
        }

        public WishlistLine(WishListWS.WishList NavWishListLine, int TipoCompra=0)
        {
            //Ahora pasamos la key, pero necesitaremos CodCliente
            ArticleDataAccessObject articleDAO = new ArticleDataAccessObject(NavWishListLine.Usuario, NavWishListLine.CustomerNo, TipoCompra);
            this.product = articleDAO.GetProductByCodeAndCatalog(NavWishListLine.ItemNo, null, NavWishListLine.CatalogoNo);
            this.quantity = (int)NavWishListLine.Qty;
            this.insertDate = NavWishListLine.Fecha;
        }


        #endregion

        #region Public Methods

        #endregion
    }
}
