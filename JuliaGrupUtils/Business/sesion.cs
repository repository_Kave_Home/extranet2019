﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupUtils.Business
{
   public  class Sesion
    {


       public  User  GetUserWithUsername(string username)
       {
           User ret = null ;
           try
           {
               UserDataAccessObject DUser = new  UserDataAccessObject();
               ret= DUser.GetUserWithUsername(username);
           }
           catch (NavException ex)
           {
               throw ex;
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
        return ret;
        }

       public User GetUser(String LoginName)
       {
           #region LocalVars
                string currentLoginname;
                User ret = null;
           #endregion
           try
           {
                //Seleccionar el cliente logeado
                string[] loginparts = LoginName.Split('|');
                int membershipproviderparts = loginparts.Length;
                if (membershipproviderparts == 3)
                {
                    //NAV Provider
                    currentLoginname = loginparts[membershipproviderparts - 1];
                }
                else if (membershipproviderparts == 2)
                {
                    //Windows Provider
                    currentLoginname = loginparts[membershipproviderparts - 1].Split('\\')[1];
                }
                else
                {
                    //??? Error
                    currentLoginname = LoginName;
                }

                UserDataAccessObject userDAO = new UserDataAccessObject();

                ret = userDAO.GetUserWithUsername(currentLoginname);
              }
           catch (NavException ex)
           {
               throw ex;
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }

            return ret;
       }


       public string GetUserLanguageForUsername(String LoginName)
       {
           #region LocalVars
                string currentLoginname;
                string ret;
           #endregion
           try
           {
                //Seleccionar el cliente logeado
                string[] loginparts = LoginName.Split('|');
                int membershipproviderparts = loginparts.Length;
                if (membershipproviderparts == 3)
                {
                    //NAV Provider
                    currentLoginname = loginparts[membershipproviderparts - 1];
                }
                else if (membershipproviderparts == 2)
                {
                    //Windows Provider
                    currentLoginname = loginparts[membershipproviderparts - 1].Split('\\')[1];
                }
                else
                {
                    //??? Error
                    currentLoginname = LoginName;
                }

                UserDataAccessObject userDAO = new UserDataAccessObject();

                ret = userDAO.GetUserLanguageForUsername(currentLoginname);
              }
           catch (NavException ex)
           {
               throw ex;
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }

            return ret;
       }

       public Client  GetClient(String CodCliente, string username)
       {
           Client ret = null ;
           try
           {
               ClientDataAccessObject clientDAO = new ClientDataAccessObject();
               ret = clientDAO.GetClientNameByClientCode(CodCliente, username);
              }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
            return ret;
       }
       public ArticleDataAccessObject GetArticleGdo(String UserName, String ClientCode, string language, decimal optionalTipoCambio = (decimal)1.3305)
       {
           ArticleDataAccessObject ret = null;
           try
           {
               ret = new ArticleDataAccessObject(UserName, ClientCode, 1, optionalTipoCambio, language);
           }
           catch (NavException ex)
           {
               throw ex;
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
           return ret;
       }

       public ArticleDataAccessObject GetArticle(String UserName, String ClientCode, string language, decimal optionalTipoCambio = (decimal)1.3305)
       {
           ArticleDataAccessObject ret =null ;
           try
           {
               ret = new ArticleDataAccessObject(UserName, ClientCode, 0, optionalTipoCambio, language);
           }
           catch (NavException ex)
           {
               throw ex;
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
          return ret;
       }

       public void CleanArticleCache(String UserName, String ClientCode)
       {
          ArticleDataAccessObject. CleanArticleDataAccessObjectCache(UserName, ClientCode,0);
       }
       public void CleanArticleGdoCache(String UserName, String ClientCode)
       {
           ArticleDataAccessObject.CleanArticleDataAccessObjectCache(UserName, ClientCode,1);
       }

       public void UpdateClientLanguage(User usr)
       {
           try
           {
               UserDataAccessObject user = new UserDataAccessObject ();

               //06/11/2012 Enviamos siempre username
               //user.Updatelanguage(usr.Code , usr.Language);
               user.Updatelanguage(usr.UserName, usr.Language);
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
       }

       public Order  GetOrder(Client Client)
       {
           Order  ret = null ;
           try 
           {
               OrderDataAccesObject orderDAO = new OrderDataAccesObject();
               ret = orderDAO.GetCurrentNavOrder(Client, 0);
            }
           catch (NavException ex)
           {
               //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw ex;
           }
           return ret;
       }
       public Order GetOrderGdo(Client Client)
       {
           Order ret = null;
           try
           {
               OrderDataAccesObject orderDAO = new OrderDataAccesObject();
               ret = orderDAO.GetCurrentNavOrder(Client, 1);
           }
           catch (NavException ex)
           {
               //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw ex;
           }
           return ret;
       }

       public VentaDataAccessObject GetVentas(String  ClientCode)
       {
           VentaDataAccessObject ret = null ;
           try
           {
               ret = new VentaDataAccessObject();
               ret.SetCurrentNavVentas(ClientCode);
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
           return ret;
       }
       public VentaDataAccessObject GetVentasAgent(String AgentCode)
       {
           VentaDataAccessObject ret = null;
           try
           {
               ret = new VentaDataAccessObject();
               ret.SetCurrentAgentNavVentas(AgentCode);
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
           return ret;
       }
       public VentaDataAccessObject GetVentasClientGroup(String ClientGroupCode)
       {
           VentaDataAccessObject ret = null;
           try
           {
               ret = new VentaDataAccessObject();
               ret.SetCurrentClientGroupNavVentas(ClientGroupCode);
           }
           catch (Exception ex)
           {
               JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw new Exception(new StackFrame(1).GetMethod().Name, ex);
           }
           return ret;
       }
       
       public BillDataAccessObject  GetBills(String ClientCode)
       {
            BillDataAccessObject ret = null ;
            try
            {
               ret = new BillDataAccessObject();
               ret.SetBillsList (ClientCode);
              }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return ret;
       }

       public Wishlist GetWishlist(String Username, String ClientCode)
       {
           Wishlist ret = new Wishlist();
           try
           {
               WishListDataAccessObject wishlistDAO = new WishListDataAccessObject();
               ret.WishlistLines = wishlistDAO.GetCurrentNavWishlist(Username, ClientCode);
           }
           catch (NavException ex)
           {
               //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_LOG, JuliaGrupUtils.Log.Constantes.BUSINESS_AREA_CAT_LAYER_LOG);
               throw ex;
           }
           return ret;
       }

    }
}
