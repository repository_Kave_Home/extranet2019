﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupUtils.DataAccessObjects
{
    class AgentDataAccess
    {
        public Agent GetAgentWithUsername(string username)
        {
            Agent result = null;
            string aux = string.Empty;
            try
            {
                result = new Agent();
                result.UserName = username;

                UsuariosWs.UsuariosWEB_Service agent = new UsuariosWs.UsuariosWEB_Service();
                agent.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = agent.ReadMultiple(filters, string.Empty, 0);

                UsuariosWs.UsuariosWEB currentUser = usuarios[0];     //REZ: 26062012 -- Creo que no se utiliza

                JuliaWs.NAVCodeunitWS juliaWs = new JuliaWs.NAVCodeunitWS();
                juliaWs.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                JuliaWs.Customers customerLists = new JuliaWs.Customers();



                ClientWs.Cliente_Service service = new ClientWs.Cliente_Service();
                service.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ClientWs.Cliente cl = service.Read(currentUser.CustomerNo);

                Client ClientAgent = new Client(cl, username);
                result.ListClients.Add(ClientAgent);
                result.Code = ClientAgent.Code;
                result.Language = currentUser.Idioma;
                result.CodVendedor = currentUser.CodVendedor;
                result.Coeficient = currentUser.CoficientePVP;
                result.AccessType = currentUser.TipoAcceso.ToString();
                result.FechaCaducidadPassword = currentUser.Fecha_Caducidad_Password;
                result.VerCantidades = currentUser.VerCantidadesStock;
                result.DolarCoef = currentUser.CurrencyFactorUSDtoEUR;
                result.UserHasGdoAcces = currentUser.AccesoGDO;
                //result.DeliveryDate
                result.Email = currentUser.Mail;

                result.GdoPVPUser = currentUser.GDOPVPUser;
                result.CoeficientGDOPVP = currentUser.CoeficienteGDOPVP;
                result.AccesoComingSoon = currentUser.AccesoComingSoon;
                /* 2019-07-11 - Aida Lucha - Shop in shop*/
                result.ShopInShop = currentUser.ShopInShop;
                
                //result.Name
                //result.OutputDate 
                //result.Phone
                //result.Stat
                //result.Tracking
                //result.Type

                //get clientes by agente
                aux = juliaWs.GetClientesByAgente(username, ref customerLists);
                if (aux == string.Empty) 
                {
                    ClientDataAccessObject clientDAO = new ClientDataAccessObject();

                    var llistaClientsOrdernada = (from a in customerLists.Customer
                                                  orderby a.Name
                                                  select a);
                    int numclients = 0;

                    ClientHeader AGentClient = new ClientHeader();
                    AGentClient.Name = ClientAgent.Code + " - " + ClientAgent.Name;
                    AGentClient.Code = ClientAgent.Code;
                    result.ListClientsName.Add(AGentClient);

                    foreach (JuliaWs.Customer cus in llistaClientsOrdernada)
                    {
                        ClientHeader CH = new ClientHeader();
                        CH.Name = cus.CodCliente + " - " + cus.Name;
                        CH.Code = cus.CodCliente;
                        result.ListClientsName.Add(CH);
                        numclients++;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);                
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (aux != String.Empty)
            {
                NavException ex = new NavException(aux);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
            return result;
        }

    }
}
