﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Web;
using JuliaGrupUtils.Business;
using System.Runtime.Serialization;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using System.Web.UI;
using JuliaGrupUtils.ErrorHandler;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace JuliaGrupUtils.DataAccessObjects
{
    [Serializable]
    public class ArticleDataAccessObject
    {
        private string clientUsername = String.Empty;
        public List<Article> JuliaArticles;

        public ArticleDataAccessObject(string clientUsername, string codCliente,int TipoCompra, decimal optionalTipoCambio = (decimal)1.3305, string idioma = "ESP")
        {

            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.JuliaArticles = new List<Article>();
                string strTipoCompra = (TipoCompra == 0) ? "" : "Gdo";
                string strCacheConstant = string.Format("AllJuliaArticles{0}{1}{2}", strTipoCompra, clientUsername, codCliente);

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                if (HttpContext.Current.Cache.Get(strCacheConstant) == null)
                {
                    ////REZ22032016 - Pasar a funcion
                    UsuariosWs.UsuariosWEB_Service client = new UsuariosWs.UsuariosWEB_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                    UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                    filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                    filter.Criteria = this.clientUsername;

                    UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                    UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

                    UsuariosWs.UsuariosWEB currentUser = usuarios[0];

                    string accesoComing = "0";
                    if (currentUser.AccesoComingSoon)
                        accesoComing = "1";
                    //Stopwatch sw = Stopwatch.StartNew();
                    //MessageError = GetAllArticlesNAV(this.clientUsername, codCliente, TipoCompra, optionalTipoCambio);
                    //sw.Stop();
                    //TimeSpan elapsedTime = sw.Elapsed;
                    //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(),"GetAllArticlesNAV - Execution time = " + elapsedTime.Milliseconds, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                    ////MessageError = GetAllArticlesNAV(this.clientUsername, codCliente, TipoCompra, optionalTipoCambio);
                    ////printArticle(this.JuliaArticles.Where(p => p.Code == "S007EP01" && p.CatalogNo == "LFRELAX").FirstOrDefault(), "NAVArticle.xml");
                    //this.JuliaArticles.Clear();
                    //Stopwatch sw2 = Stopwatch.StartNew();
                    
                    MessageError = GetAllArticlesSQL(this.clientUsername, codCliente, TipoCompra, optionalTipoCambio, idioma, accesoComing);

                    
                    //sw2.Stop();
                    //TimeSpan elapsedTime2 = sw2.Elapsed;
                    //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "GetAllArticlesSQL - Execution time = " + elapsedTime2.Milliseconds, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                    //printArticle(this.JuliaArticles.Where(p => p.Code == "S007EP01" && p.CatalogNo == "LFRELAX").FirstOrDefault(), "SQLArticle.xml");
                    if (String.IsNullOrEmpty(MessageError))
                    {
                        HttpContext.Current.Cache.Add(strCacheConstant, this.JuliaArticles, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                }
                else
                {
                    this.JuliaArticles = (List<Article>)HttpContext.Current.Cache.Get(strCacheConstant);
                    //Refrescamos la cache para 20 minutos mas
                    HttpContext.Current.Cache.Remove(strCacheConstant);
                    HttpContext.Current.Cache.Add(strCacheConstant, this.JuliaArticles, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (MessageError != String.Empty)
            {
                NavException ex = new NavException(MessageError);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        private void printArticle(Article article, string filename)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Article));
            TextWriter tw = new StreamWriter(String.Format(@"c:\temp\{0}", filename));
            xs.Serialize(tw, article);
        }


        private string GetAllArticlesNAV(string clientUsername, string codCliente, int TipoCompra, decimal optionalTipoCambio) {
            string MessageError = String.Empty;

            JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
            client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
            JuliaWs.ProductosWEB prodWeb = new JuliaWs.ProductosWEB();

            //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception("Inicio llamada a NAV - (" + this.clientUsername + ", " + codCliente + ", ref " + prodWeb + ", " + TipoCompra + " )"), new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
            MessageError = client.GetTotalInfo(this.clientUsername, codCliente, ref prodWeb, TipoCompra);
            //JuliaGrupUtils.Log.Logger.WriteDebug(new Exception("Fin llamada a NAV - (" + this.clientUsername + ", " + codCliente + ", ref " + prodWeb + ", " + TipoCompra + " )"), new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

            if (MessageError == string.Empty)
            {
                for (int i = 0; i < prodWeb.ProductoWEB.Count(); i++)
                {
                    this.JuliaArticles.Add(new Article(prodWeb.ProductoWEB[i], optionalTipoCambio));
                }
            }
            return MessageError;
        }

        private string GetAllArticlesSQL(string clientUsername, string codCliente, int TipoCompra, decimal optionalTipoCambio, string idioma, string accesoComingSoon)
        {
            JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO CARGA DATOS [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

            string MessageError = "No data";
            //Me faltará el idioma. Lo podría obtener del username... 

            string whereTipoPortal = (TipoCompra == 0)?
                                        "and WTipoPortal = 1 and SalesActive = 1 and TipoCatalogo <> 7":    //LaForma
                                        "and WTipoPortal = 2 and PurchaseActive = 1 and TipoCatalogo = 7";  //GDO

            if (TipoCompra == 1 && accesoComingSoon == "0")
                whereTipoPortal += "and ComingSoon = 0";

            string articlesOrderBy = ConfigurationManager.AppSettings["ArticlesOrderBy"];

            string sqlQuery = String.Format(@"SELECT *
                                FROM [GET_TOTALS_ITEMS_SHP_v3]
                                where CustomerNo = '{0}'
                                  and idioma = '{1}'
                                  {2} {3}", codCliente, idioma, whereTipoPortal,articlesOrderBy);

            string connectionString = ConfigurationManager.AppSettings["NavisionDBConnectionString"];

                        
            using (SqlConnection sdwDBConnection = new SqlConnection(connectionString))
            {
               
                // Open the connection
                sdwDBConnection.Open();
                //Lanzamos la Query
                
                SqlCommand queryCommand = new SqlCommand(sqlQuery, sdwDBConnection);

                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO LOAD GET_TOTALS_ITEMS_SH_V3 [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                // Use the above SqlCommand object to create a SqlDataReader object.
                SqlDataReader queryCommandReader = queryCommand.ExecuteReader();

                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO LOAD DATOS V3 [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);

                // Create a DataTable object to hold all the data returned by the query.
                DataTable dataTable = new DataTable();

                
                dataTable.Load(queryCommandReader);

                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - INICIO TRATAMIENTO DATOS V3 [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);


                //Añadimos los resultados
                foreach (DataRow dtr in dataTable.Rows)
                {
                    this.JuliaArticles.Add(new Article(dtr, optionalTipoCambio));
                }

                JuliaGrupUtils.Log.Logger.WriteDebug(new Exception(), "JuliaGrup - Log traces - FIN LOAD GET_TOTALS_ITEMS_SH_V3 [alucha]", JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);


                MessageError = String.Empty;
            }
            return MessageError;
        }


        


        public static  void CleanArticleDataAccessObjectCache(string clientUsername, string codCliente, int TipoCompra)
        {       
            //Borramos toda la cache
            HttpContext.Current.Cache.Remove("AllJuliaArticles" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("AllJuliaArticlesGdo" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("ClientBusinessLine" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("ArticlesCatalog_Paging_cache_" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("ArticlesGdo_Paging_cache_" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("Inspirates" + clientUsername + codCliente);
            HttpContext.Current.Cache.Remove("Wishlist_" + clientUsername + "_" + codCliente);

            HttpContext.Current.Cache.Remove("TopMenu_" + clientUsername + "_" + codCliente);
            HttpContext.Current.Cache.Remove("TopMenuGdo_" + clientUsername + "_" + codCliente);
            
        }

        public List<Article> GetProductsReference(string code)
        {
            var products = from p in this.JuliaArticles
                           where p.Code.Substring(0, 6) == code.Substring(0, 6)
                           select p;

            return new List<Article>(products);

        }

        public List<Article> GetClientProductCodeBeginsWith(Client c, string code)
        {
            var products = from p in this.JuliaArticles
                           where p.Code.StartsWith(code)
                           select p;

            return new List<Article>(products);
        }

        public List<Article> GetProductCodeBeginsWith(string code)
        {
            var products = from p in this.JuliaArticles
                           where p.Code.StartsWith(code)
                           select p;

            return new List<Article>(products);
        }

        public List<Article> c(Client c, string code)
        {
            var products = from p in this.JuliaArticles
                           where p.Code.StartsWith(code)
                           select p;

            return new List<Article>(products);
        }
        public Article GetProductByCodeAndCatalog(string code,string version, string catalog)
        {
            try
            {
                var product = from p in this.JuliaArticles
                              where p.Code == code && p.InsertcatalogNo == catalog
                              select p;

                if (!(string.IsNullOrEmpty(version)))
                {

                   product = product.Where(p => p.ArticleVersion == Convert.ToInt32(version));

                }
                //else
                //{
                //    var product = from p in this.JuliaArticles
                //                  where p.Code == code && p.InsertcatalogNo == catalog 
                //                  select p;
                //}
                
                if (product.Count() == 1)
                {
                    //com a molt en tindrem un pq busquem per codi
                    return product.First();
                }
                else if (product.Count() > 0)
                {
                    NavException ex = new NavException(JuliaGrupUtils.Utils.LanguageManager.GetLocalizedString("ErrorDuplicateArticles"));
                    throw ex;
                } else return null;

            }
            catch(NavException ex)
            {
                throw ex;            
            }
           
        }
        public Article GetProductByCode(string code)
        {
            var product = from p in this.JuliaArticles
                          where p.Code.ToUpper() == code.ToUpper()
                          select p;

            if (product.Count() > 0)
            {
                //com a molt en tindrem un pq busquem per codi
                return product.First();
            }
            else
            {
                return null;
            }
        }

        public Article GetProductByCode(string code, string version)
        {//No la crida ningu, ara la funcio que s'utilitza es GetProductByCodeAndCatalog(string code,string version, string catalog)
            int versionInt = 0;
            Int32.TryParse(version,out versionInt);

            var product = from p in this.JuliaArticles
                          where p.Code == code && p.ArticleVersion == versionInt
                          select p;

            if (product.Count() > 0)
            {
                //com a molt en tindrem un pq busquem per codi
                return product.First();
            }
            else
            {
                return null;
            }
        }


        public string GetProductImagesIssuesByCode(string code, string version, string type)
        {
            List<SPFile> list = new List<SPFile>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {

                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    
                                                    <Eq>
                                                        <FieldRef Name=""ContentType"" />
                                                            <Value Type=""Computed"">Assembly instructions (image)</Value>
                                                    </Eq>
                                                     
                                                   
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.RowLimit = 500;
                            query.ViewFields = string.Concat(
                                  "<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                                  "<FieldRef Name='ContentType' />",
                                  "<FieldRef Name='FileLeafRef' />");

                            query.ViewFieldsOnly = true; // Fetch only the data that we need.
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = splist.GetItems(query);

                            foreach (SPListItem image in imagescollection)
                            {
                                list.Add(image.File);
                            }

                            //foreach (SPFile file in folder.Files)
                            //{
                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
                            //        && IsTypedImage(file.Item.Name, type))
                            //        list.Add(file);
                            //}
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (list.Count == 0)
            {
                return string.Empty;
            }
            else
            { 
             return list.First().ToString();
            }
           
        }
        public List<SPFile> GetProductImagesByCode(string code, string version, string type)
        {
            List<SPFile> list = new List<SPFile>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {

                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <And>
                                                        <Eq>
                                                            <FieldRef Name=""ContentType"" />
                                                                <Value Type=""Computed"">Product image</Value>
                                                        </Eq>
                                                        <Contains>
                                                            <FieldRef Name=""FileLeafRef"" />
                                                            <Value Type=""File"">" + type + @"</Value>
                                                        </Contains>
                                                    </And>
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.RowLimit = 500;
                            query.ViewFields = string.Concat(
                                  "<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                                  "<FieldRef Name='ContentType' />",
                                  "<FieldRef Name='FileLeafRef' />");

                            query.ViewFieldsOnly = true; // Fetch only the data that we need.

                            //query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = splist.GetItems(query);

                            foreach (SPListItem image in imagescollection)
                            {
                                list.Add(image.File);
                            }

                            //foreach (SPFile file in folder.Files)
                            //{
                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
                            //        && IsTypedImage(file.Item.Name, type))
                            //        list.Add(file);
                            //}
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return list;
        }

       

        private bool IsTypedImage(string name, string type)
        {
            if (!string.IsNullOrEmpty(name) && name.Contains(type))
                return true;
            return false;
        }

        private bool IsImgType(SPFolder folder, string doctype)
        {
            try{
            SPContentType docType = folder.DocumentLibrary.ContentTypes["Product image"];
            if (doctype == docType.Id.ToString())
                return true;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return false;
        }

        public List<SPListItem> GetProductDocumentsByCode(string code, string version)
        {
            List<SPListItem> list = new List<SPListItem>();
            try{
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {                            
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);

                            SPQuery query = new SPQuery();
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <Or>
                                                        <Eq>
                                                            <FieldRef Name=""ContentType"" />
                                                                <Value Type=""Computed"">Technical sheet</Value>
                                                        </Eq>
                                                        <Or>
                                                            <Eq>
                                                                <FieldRef Name=""ContentType"" />
                                                                    <Value Type=""Computed"">Assembly instructions (document)</Value>
                                                            </Eq>
                                                            <Or>
                                                                <Eq>
                                                                    <FieldRef Name=""ContentType"" />
                                                                        <Value Type=""Computed"">Composition sheet</Value>
                                                                </Eq>
                                                                <Eq>
                                                                    <FieldRef Name=""ContentType"" />
                                                                        <Value Type=""Computed"">Certificates</Value>
                                                                </Eq>
                                                            </Or>
                                                        </Or>
                                                    </Or>
                                                </And>
                                            </Where>
                                            <OrderBy Override=""TRUE""><FieldRef Name=""FileDirRef"" /><FieldRef Name=""FileLeafRef"" /></OrderBy>
                                            ";
                            query.RowLimit = 500;
                            query.Folder = folder;
                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            
                            
                            query.ViewFields = string.Concat(
                                  //"<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                                  "<FieldRef Name='ContentType' />",
                                  "<FieldRef Name='FileLeafRef' />",
                                  "<FieldRef Name='FileDirRef'/>");

                            //query.ViewFieldsOnly = true; // Fetch only the data that we need. //Peta... Porque se mapea a un SPListItem no descomentar

                            
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection docscollection = splist.GetItems(query);

                            foreach (SPListItem doc in docscollection)
                            {
                                list.Add(doc);
                            }

                            //foreach (SPFile file in folder.Files)
                            //{
                            //    //Per a la MV meva fer servir: Publish_x0020_on_x0020_client_x0020_portal0
                            //    if (IsDocType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true"))
                            //        list.Add(file.Item);
                            //}
                        }
                    }
                });
              }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return list;
        }

        public List<SPListItem> GetProductCertificateByCode(string code, string version)
        {
            List<SPListItem> list = new List<SPListItem>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);

                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                     <Eq>
                                                        <FieldRef Name=""ContentType"" />
                                                            <Value Type=""Computed"">Certificates</Value>
                                                    </Eq>                                                    
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;

                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.RowLimit = 500;
                            //query.ViewFields = string.Concat(
                            //      "<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                            //      "<FieldRef Name='ContentType' />",
                            //      "<FieldRef Name='FileLeafRef' />");

                            //query.ViewFieldsOnly = true; // Fetch only the data that we need.

                            //query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection docscollection = splist.GetItems(query);

                            foreach (SPListItem doc in docscollection)
                            {
                                list.Add(doc);
                            }

                            //foreach (SPFile file in folder.Files)
                            //{
                            //    //Per a la MV meva fer servir: Publish_x0020_on_x0020_client_x0020_portal0
                            //    if (IsDocType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true"))
                            //        list.Add(file.Item);
                            //}
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return list;
        }



        private bool CheckProperty(SPListItem item, string name, string value)
        {
            try
            {
                if (item != null)
                {
                    SPField field = item.Fields.GetFieldByInternalName(name);
                    if (field != null && item[field.Id] != null)
                    {
                        bool val;
                        bool.TryParse(item[field.Id].ToString(), out val);

                        if(val)
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

            return false;
        }

        private bool IsDocType(SPFolder folder, string doctype)
        {
            try{
                SPContentType docType = folder.DocumentLibrary.ContentTypes["Technical sheet"];
                SPContentType docType2 = folder.DocumentLibrary.ContentTypes["Assembly instructions (document)"];
                SPContentType docType3 = folder.DocumentLibrary.ContentTypes["Composition sheet"];
                SPContentType docType4 = folder.DocumentLibrary.ContentTypes["Certificates"];

                if (doctype == docType.Id.ToString() || doctype == docType2.Id.ToString()
                    || doctype == docType3.Id.ToString() || doctype == docType4.Id.ToString())
                    return true;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return false;
        }
        
        /***********************/

        public List<string> GetProductBigImagesByCode(string code, string version)
        {
            List<string> result = new List<string>();
                        
            for (int i = 0; i < 4; i++)
            {
                result.Add("/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + code);
            }
            return result;
        }

        public List<string> GetProductBigImagesByCode(string code)
        {
            List<string> result = new List<string>();

            for (int i = 0; i < 4; i++)
            {
                result.Add("/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + code);
            }
            return result;
        }

        public string GetProductMainImageByCode(string code, string version)
        {
            string image = @"~/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + code;
            return image;
        }

        public List<Article> GetCatalogProducts()
        {
            return this.JuliaArticles.Where(a => a.Active == true && !String.IsNullOrEmpty(a.Catalog)).ToList<Article>();
        }

        public List<Article> GetCatalogProducts(string code)
        {
            return this.JuliaArticles.Where(a => a.Active == true && !String.IsNullOrEmpty(a.Catalog) && a.CatalogNo == code).ToList<Article>();
        }

        public List<Article> GetAllProducts()
        {
            
            return this.GetAllProductsByQuery();
        }

        public List<Article> GetAllProductsByQuery()
        {
            return this.JuliaArticles;
        }

        public List<Article> 
            GetProductosDestacados()
        {
            //return this.JuliaArticles.Where(a => a.Destacado != "No").ToList();
            //Eliminamos artículos de Inspirate
            List<Article> articles = (from p in this.JuliaArticles where !p.CatalogType.Equals("Inspirate") select p).ToList();
            //Eliminamos artículos de promociones???
            //articles = (from p in articles where !p.CatalogType.Equals("Promociones") select p).ToList();
            //Funcion linq que devuelve artículos no repetidos por código (Code)
            return articles.Where(a => a.Destacado != "No").Distinct(new ArticleComparer()).ToList();
        }

        public string GetCatalogName(string code)
        {
            var catalogname = (from it in this.JuliaArticles
                    where it.Active == true && !String.IsNullOrEmpty(it.Catalog) && it.CatalogNo == code
                    select it.Catalog).Distinct().ToList();
            if (catalogname.Count > 0)
                return catalogname[0].ToString();
            return null;
        }

        public List<string> GetCatalogsCode(string tipo)
        {
            //return (from it in this.JuliaArticles
            //        where it.Active == true && !String.IsNullOrEmpty(it.Catalog) && it.CatalogType == tipo
            //        select it.CatalogNo).Distinct().ToList();
            //REZ19012015 - Orden Catalogos
            return (from it in this.JuliaArticles
                    where it.Active == true && !String.IsNullOrEmpty(it.Catalog) && it.CatalogType == tipo
                    orderby it.WebOrden ascending
                    select it.CatalogNo).Distinct().ToList();
        }

        public List<string> GetCatalogsCode(string tipo1, string tipo2, string tipo3)
        {
            //return (from it in this.JuliaArticles
            //        where it.Active == true && !String.IsNullOrEmpty(it.Catalog) && ((it.CatalogType == tipo1) || (it.CatalogType == tipo2) || (it.CatalogType == tipo3))
            //        select it.CatalogNo).Distinct().ToList();
            //REZ19012015 - Orden Catalogos
            return (from it in this.JuliaArticles
                    where it.Active == true && !String.IsNullOrEmpty(it.Catalog) && ((it.CatalogType == tipo1) || (it.CatalogType == tipo2) || (it.CatalogType == tipo3))
                    orderby it.WebOrden ascending
                    select it.CatalogNo).Distinct().ToList();
        }

        public List<string> GetCatalogsCode()
        {
            //return (from it in this.JuliaArticles
            //        where it.Active == true && !String.IsNullOrEmpty(it.Catalog)
            //        select it.CatalogNo).Distinct().ToList();
            //REZ19012015 - Orden Catalogos
            return (from it in this.JuliaArticles
                    where it.Active == true && !String.IsNullOrEmpty(it.Catalog)
                    orderby it.WebOrden ascending
                    select it.CatalogNo).Distinct().ToList();
        }
        public string GetInformeProductos(string codUser, string codClient, string codCatalogo, string familia, string subFamilia, string programa, string txtSearch, int vista, int filtroDto)
        {

            string value = string.Empty;
            string Doc = string.Empty;

            try
            {

                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //REZ 18/3/2013: temporalment desabilitat - 
                value = client2.GetInformeProductos(codUser, codClient, codCatalogo, familia, subFamilia, programa, txtSearch,vista ,ref Doc,filtroDto);
                //REZ 18/3/2013 -- Funció que funciona pero es d'una altra cosa...
                //value = client2.GetInformeClienteDetallado(codUser, codClient, ref Doc);

                if (value != String.Empty)
                {
                    return value;
                }
                else
                {
                    return Doc;
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            

        }


        public string GetInformeProductosGdo(string codUser, string codClient, string proveedor, string familia, string subFamilia, string programa, string txtSearch, string divisa, int filtroDto, string tipoUnion)
        {
            string value = string.Empty;
            string Doc = string.Empty;

            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //value = client2.GetInformeProductosGDO(codUser, codClient, proveedor, tipoUnion, familia, subFamilia, programa, txtSearch, vista, ref Doc);
                value = client2.GetInformeProductosGDO(codUser, codClient, proveedor, tipoUnion, familia, subFamilia, programa, txtSearch, divisa, ref Doc);

                if (value != String.Empty)
                {
                    return value;
                }
                else
                {
                    return Doc;
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
    }
}
