﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.DataAccessObjects
{
   
    [Serializable]
    public class BillDataAccessObject
    {
        private List<Bill > Bills;

        public BillDataAccessObject()
        {
            Bills = new List<Bill>();
        }

        #region Public methods


        public List<Bill> GetAllBills()
        {
            return this.Bills;
        }

      
        public void SetBillsList(String ClientCode)
        {
            try
            {
                if (this.Bills != null && this.Bills.Count > 0)
                    return;

                this.Bills = new List<Bill>();
                // GET ALL ORDERS
                BillsWS.ListaHistoricoFacturas_Service client = new BillsWS.ListaHistoricoFacturas_Service();

                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                BillsWS.ListaHistoricoFacturas_Filter filter = new BillsWS.ListaHistoricoFacturas_Filter();
                //filter.Field = BillsWS.ListaHistoricoFacturas_Fields.Sell_to_Customer_No; //No es correcte, ha de ser Bill-to-customerno
                filter.Field = BillsWS.ListaHistoricoFacturas_Fields.Bill_to_Customer_No;
                filter.Criteria = ClientCode;

                BillsWS.ListaHistoricoFacturas_Filter[] filters = { filter };

                BillsWS.ListaHistoricoFacturas[] bills = client.ReadMultiple(filters, string.Empty, 0);

                foreach (BillsWS.ListaHistoricoFacturas e in bills)
                {            
                    Bill bill = new Bill(e);
                    Bills.Add(bill );
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }


        public Bill GetBillByCode(string id, Client c)
        {
            Bill ret = null ;
            try
            {
                BillsDetailsWS.HistoricoFactura_Service client = new BillsDetailsWS.HistoricoFactura_Service();           

                //client.Credentials = new System.Net.NetworkCredential("Administrador", "l@f0rm@08", "julia");
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                BillsDetailsWS.HistoricoFactura bill = client.Read(id);
                if (bill == null)
                    return null;
            
                ret = new Bill(bill, c);
             
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return ret;
        }

        #endregion

        #region Private methods



        #endregion
    }
}
