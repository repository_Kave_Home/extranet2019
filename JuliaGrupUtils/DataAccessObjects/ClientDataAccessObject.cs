﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using System.Data.SqlClient;
using System.Configuration;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class ClientDataAccessObject
    {
        public ClientDataAccessObject()
        {
            
        }

        //returns if the user is an agent
        public bool IsUserAgent(string username)
        {
            return true;
        }
        public string GetInformeCliente(string codUser, string codCliente)
        {

            string value = string.Empty;
            string Doc = string.Empty;

            try
            {

                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.GetInformeClienteDetallado(codUser, codCliente, ref Doc);
                if (value != String.Empty)
                {
                    NavException ex = new NavException(value);
                    throw ex;
                }

            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        
            return Doc;

        }

        public Client GetClientByUsername(string username)
        {
            Client result = null;
            try
            {
                UsuariosWs.UsuariosWEB_Service client = new UsuariosWs.UsuariosWEB_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

                UsuariosWs.UsuariosWEB currentUser = usuarios[0];

                    

                if (currentUser.Tipo.ToString() == "Representante")
                {
                    return null;
                }
                ClientWs.Cliente_Service service = new ClientWs.Cliente_Service();
                service.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ClientWs.Cliente cli = service.Read(currentUser.CustomerNo);
                
                result = new Client(cli, username);
                result.Language = currentUser.Idioma;
                result.Coeficient = currentUser.CoficientePVP;
                result.AccessType = currentUser.TipoAcceso.ToString();
                result.FechaCaducidadPassword = currentUser.Fecha_Caducidad_Password;
                result.Email = currentUser.Mail;
                result.VerCantidades = currentUser.VerCantidadesStock;
                result.DolarCoef = currentUser.CurrencyFactorUSDtoEUR;
                result.UserHasGdoAcces = currentUser.AccesoGDO;

                result.GdoPVPUser = currentUser.GDOPVPUser;
                result.CoeficientGDOPVP = currentUser.CoeficienteGDOPVP;
                result.AccesoComingSoon = currentUser.AccesoComingSoon;
                result.ShopInShop = currentUser.ShopInShop;
                


                //Get direcciones envio
                DireccionEnvioWs.DireccionEnvio_Service direccionsService = new DireccionEnvioWs.DireccionEnvio_Service();
                direccionsService.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                DireccionEnvioWs.DireccionEnvio_Filter dirFilter = new DireccionEnvioWs.DireccionEnvio_Filter();
                dirFilter.Field = DireccionEnvioWs.DireccionEnvio_Fields.Customer_No;
                dirFilter.Criteria = cli.No;

                DireccionEnvioWs.DireccionEnvio_Filter[] dirFilters = { dirFilter };
                DireccionEnvioWs.DireccionEnvio[] direccionesEnvio = direccionsService.ReadMultiple(dirFilters, string.Empty, 0);

                foreach (DireccionEnvioWs.DireccionEnvio dir in direccionesEnvio)
                {
                    string completeAddress = dir.Address + " " + dir.Post_Code + " " + dir.City + " " + dir.Pais;
                    //result.AddAddress(dir.Code, dir.Address);
                    result.AddAddress(dir.Code, completeAddress, dir.City, dir.Pais);
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

            return result;
        }

        public Client GetClientNameByClientCode(string customerNo, string username)
        {
            Client result = null;
            try
            {
                ClientWs.Cliente_Service service = new ClientWs.Cliente_Service();
                service.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ClientWs.Cliente cliente = service.Read(customerNo);            

                if (cliente != null)
                {
                    result = new Client(cliente,  username);

                    //Get direcciones envio
                    DireccionEnvioWs.DireccionEnvio_Service direccionsService = new DireccionEnvioWs.DireccionEnvio_Service();
                    direccionsService.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                    DireccionEnvioWs.DireccionEnvio_Filter dirFilter = new DireccionEnvioWs.DireccionEnvio_Filter();
                    dirFilter.Field = DireccionEnvioWs.DireccionEnvio_Fields.Customer_No;
                    dirFilter.Criteria = cliente.No;

                    DireccionEnvioWs.DireccionEnvio_Filter[] dirFilters = { dirFilter };
                    DireccionEnvioWs.DireccionEnvio[] direccionesEnvio = direccionsService.ReadMultiple(dirFilters, string.Empty, 0);

                    foreach (DireccionEnvioWs.DireccionEnvio dir in direccionesEnvio)
                    {
                        string completeAddress = dir.Address + " " + dir.Post_Code + " " + dir.City + " " + dir.Pais;
                        //result.AddAddress(dir.Code, dir.Address);
                        result.AddAddress(dir.Code, completeAddress, dir.City, dir.Pais);
                    }

                    return result;
                }      

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public Client GetEntireClientByClientCode(string customerNo)
        {
            Client result = null;
            try
            {
            ClientWs.Cliente_Service service = new ClientWs.Cliente_Service();
            service.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

            ClientWs.Cliente cliente = service.Read(customerNo);

            if (cliente != null)
            {
                result = new Client(cliente, string.Empty);

                //Get direcciones envio
                DireccionEnvioWs.DireccionEnvio_Service direccionsService = new DireccionEnvioWs.DireccionEnvio_Service();
                direccionsService.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                DireccionEnvioWs.DireccionEnvio_Filter dirFilter = new DireccionEnvioWs.DireccionEnvio_Filter();
                dirFilter.Field = DireccionEnvioWs.DireccionEnvio_Fields.Customer_No;
                dirFilter.Criteria = cliente.No;

                DireccionEnvioWs.DireccionEnvio_Filter[] dirFilters = { dirFilter };
                DireccionEnvioWs.DireccionEnvio[] direccionesEnvio = direccionsService.ReadMultiple(dirFilters, string.Empty, 0);

                foreach (DireccionEnvioWs.DireccionEnvio dir in direccionesEnvio)
                {
                    string completeAddress = dir.Address + " " + dir.Post_Code + " " + dir.City + " " + dir.Pais;
                    result.AddAddress(dir.Code, dir.Address, dir.City, dir.Pais);
                }     
            }
  
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public List<Client> GetAgentClients()
        {
            List<Client> result = new List<Client>();

            return result;
        }

           public List<string> GetClientAddress(string codcliente)
        {
            List<string> result = new List<string>();            

            return result;
        }
    }
}
