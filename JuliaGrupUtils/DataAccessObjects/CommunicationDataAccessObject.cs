﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class CommunicationDataAccessObject
    {
        List<Communication> communications;

        #region Constructor

        /// <summary>
        /// Constructor Method
        /// </summary>
        public CommunicationDataAccessObject()
        {
            communications = new List<Communication>();

            this.GetSPCommunications();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Public method to get the Communications of the Portal Clientes
        /// </summary>
        public List<Communication> GetCommunications()
        {
            this.GetSPCommunications();

            return communications;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Internal method to get the Sharepoint Communications
        /// </summary>
        private void GetSPCommunications()
        {

            try{

                Guid site = SPContext.Current.Site.ID;

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite esite = new SPSite(site))
                    {
                        using (SPWeb web = esite.OpenWeb())
                        {
  
                                this.communications = new List<Communication>();
                                SPList list = web.Lists[ConstantManager.CommunicationsList_Name];

                                // Get the communications for the current user
                                SPQuery query = new SPQuery();
                                query.Query =
                                    "<OrderBy>" +
                                        "<FieldRef Name='" + ConstantManager.CommunicationsList_Field_Date + "' Ascending='FALSE'/>" +
                                    "</OrderBy>" +
                                    "<Where>" +
                                        "<Eq>" +
                                            "<FieldRef Name='" + ConstantManager.CommunicationsList_Field_AssignedTo + "' LookupId='TRUE'/>" +
                                            "<Value Type='Integer'><UserID></Value>" +
                                        "</Eq>" +
                                    "</Where>"
                                    ;

                                SPListItemCollection results = list.GetItems(query);

                                if (results.Count < 1) return;

                                foreach (SPListItem item in results)
                                {
                                    Communication communication = new Communication();
                                    communication.Title = item[ConstantManager.CommunicationsList_Field_Title].ToString();
                                    if (item[ConstantManager.CommunicationsList_Field_Subtitle] != null)
                                        communication.Subtitle = item[ConstantManager.CommunicationsList_Field_Subtitle].ToString();
                                    if (item[ConstantManager.CommunicationsList_Field_Date] != null)
                                        communication.Date = DateTime.Parse(item[ConstantManager.CommunicationsList_Field_Date].ToString()).ToShortDateString();
                                    if (item[ConstantManager.CommunicationsList_Field_Image] != null)
                                        communication.Image = item[ConstantManager.CommunicationsList_Field_Image].ToString();
                                    if (item[ConstantManager.CommunicationsList_Field_URL] != null)
                                        communication.Url = item[ConstantManager.CommunicationsList_Field_URL].ToString();

                                    this.communications.Add(communication);
                                }

                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_SHAREPOINT_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        #endregion
    }
}
