﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class DocumentDataAccessObject
    {
        private string clientUsername = String.Empty;
        List<Document> DocList;
        public DocumentDataAccessObject(string clientUsername, string codClient, string issueType)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup               
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();              
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocList = new JuliaWs.ListaDocumentos();

                MessageError = client.GetDocAlbaranesByCliente(clientUsername, codClient, ref DocList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocList.Documento[i]));
                    }
                }
                DocList = new JuliaWs.ListaDocumentos();
                //MessageError = client.GetDocFacturasByCliente(clientUsername, codClient, ref DocList);
                MessageError = client.GetDocFacturasByClienteTipo(clientUsername, codClient, ref DocList, issueType);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocList.Documento[i]));
                    }
                }
                DocList = new JuliaWs.ListaDocumentos();
                MessageError = client.GetDocPedidosByCliente(clientUsername, codClient, ref DocList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocList.Documento[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }          
        }
        //Funcion para devolver documentos por producto
        public List<Document> GetFacturasByClientandProduct(string clientUsername, string codCLient, string itemNo, string issueType) {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();
                List<Bill> BillList = new List<Bill>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                //JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : GetDocFacturasByClienteItem(clientUsername = {0}, codCLient = {1})", clientUsername, codCLient), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                //MessageError = client.GetDocFacturasByClienteItem(clientUsername, codCLient, ref DocumentosList,itemNo);
                JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : GetDocFacturasByClienteTipoItem(clientUsername = {0}, codCLient = {1})", clientUsername, codCLient), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                MessageError = client.GetDocFacturasByClientTipoItem(clientUsername, codCLient, ref DocumentosList, itemNo, issueType);               
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }
                }


            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }

        public List<Document> GetAlvaranByClientandProduct(string clientUsername, string codCLient, string itemNo)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : GetDocAlbaranesByClienteItem(clientUsername = {0}, codCLient = {1})", clientUsername, codCLient), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                MessageError = client.GetDocAlbaranesByClienteItem(clientUsername, codCLient, ref DocumentosList,itemNo);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }

                }



            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }

        public List<Document> GetFacturasByClient(string clientUsername, string codCLient, string issueType)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();
                List<Bill> BillList = new List<Bill>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                //MessageError = client.GetDocFacturasByCliente(clientUsername, codCLient, ref DocumentosList);
                MessageError = client.GetDocFacturasByClienteTipo(clientUsername, codCLient, ref DocumentosList, issueType);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }
                }
               
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }        
       
        public List<Document> GetAlvaranByClient(string clientUsername, string codCLient)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                MessageError = client.GetDocAlbaranesByCliente(clientUsername, codCLient, ref DocumentosList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }

                }



            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }

        public List<Document> GetPedidosByClientItem(string clientUsername, string codCLient, string itemNo)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();
                List<Order> BillList = new List<Order>();

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                JuliaGrupUtils.Log.Logger.WriteVerbose(String.Format("Llamada a WS : GetDocPedidosByCliente(clientUsername = {0}, codCLient = {1})", clientUsername, codCLient), JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                MessageError = client.GetDocPedidosByClienteItem(clientUsername, codCLient, ref DocumentosList, itemNo);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }

        public List<Document> GetPedidosByClient(string clientUsername, string codCLient)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.DocList = new List<Document>();
                List<Order> BillList = new List<Order>();

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaDocumentos DocumentosList = new JuliaWs.ListaDocumentos();
                MessageError = client.GetDocPedidosByCliente(clientUsername, codCLient, ref DocumentosList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < DocumentosList.Documento.Count(); i++)
                    {
                        this.DocList.Add(new Document(DocumentosList.Documento[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return DocList;
        }

        public int  GetObjectIDFromDoc(string DocNo)
        {
            return (from p in DocList
                    where p.DocNo== DocNo
                    select p.ObjectID).Distinct().ToList().First();
        }       
    }
}
