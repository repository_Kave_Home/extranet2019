﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;


namespace JuliaGrupUtils.DataAccessObjects
{
    public class IssueDataAccessObject
    {
        #region Fields

        public List<Issue> issues;
        
        private string clientUsername;
        public List<Albaran> allbaranList;

        private string codIncidencia = string.Empty;
        private string username = string.Empty;
        private string codClient = string.Empty;
        private string type = string.Empty;
        private string subType = string.Empty;
        private int objectID = 0;
        private string documentNo = string.Empty;
        private string itemNo = string.Empty;
        private string barcode = string.Empty;
        private decimal quantity = 0;
        private string RefClient = string.Empty;
        private string Observacioens = string.Empty;
        private string codiDireccionEnvio = string.Empty;

        #endregion

        #region Constructor

       

        #endregion

        #region Private Methods

        //private void GetSPIssues()
        //{
        //    issues = new List<Issue>();

        //    try
        //    {

        //        SPSecurity.RunWithElevatedPrivileges(delegate()
        //        {
        //            using (SPSite site = new SPSite(ConstantManager.ExtranetURL))
        //            {
        //                using (SPWeb web = site.RootWeb)
        //                {
        //                    SPList issuesList = web.Lists[ConstantManager.IssuesList_Name];

        //                    foreach (SPListItem item in issuesList.Items)
        //                    {
        //                        Issue issue = new Issue();

        //                        issue.CodeBill = item[ConstantManager.IssuesList_Field_Bill].ToString();
        //                        issue.CodeProduct = item[ConstantManager.IssuesList_Field_Product].ToString();
        //                        issue.Description = item[ConstantManager.IssuesList_Field_Comments].ToString();
        //                        issue.Coords = item[ConstantManager.IssuesList_Field_Points].ToString();

        //                        this.issues.Add(issue);
        //                    }
        //                }
        //            }
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_SHAREPOINT_LOG);
        //        throw new Exception(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //}
        public List<Issue> GetIssuesList(string clientUsername, string CodCustomer)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                issues = new List<Issue>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup
               
                    JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    JuliaWs.ListaIncidencias issuesList = new JuliaWs.ListaIncidencias();
                    MessageError = client.GetListaIncidencia(clientUsername,CodCustomer, ref issuesList);
                
                    if (MessageError == string.Empty)
                    {
                        if (issuesList.Incidencia != null)
                        {
                            for (int i = 0; i < issuesList.Incidencia.Count(); i++)
                            {
                                this.issues.Add(new Issue(issuesList.Incidencia[i]));
                            }
                        }
                    }                               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return issues;          
        }
       
      
        #endregion

        #region Public Methods



        public string AddIssue(string username, string codCLient, string type, string subtype, int objectID, string documentNo, string itemNo, string barcode, decimal quantity, string refClient, string observaciones, string codDireccionEnvio)
        {
          
            string MessageError = string.Empty;
            try
            {
                this.username = username;
                this.codClient = codCLient;
                this.type = type;
                this.subType = subtype;
                this.objectID = objectID;
                this.documentNo = documentNo;
                this.itemNo = itemNo;
                this.barcode = barcode;
                this.quantity = quantity;
                this.RefClient = refClient;
                this.Observacioens = observaciones;
                this.codiDireccionEnvio = codDireccionEnvio;
                issues = new List<Issue>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaIncidencias issuesList = new JuliaWs.ListaIncidencias();
                string logMsg = String.Format(@"Llamada a WS : AddIssue(username = {0}, codCLient = {1}, type = {2}, subType = {3}, objectID = {4}, documentNo = {5}, itemNo = {6}, barcode = {7},
                    quantity = {8}, RefClient = {9}, Observaciones = {10}, codiDireccionEnvio = {11})", this.username, this.codClient, this.type, this.subType, this.objectID, this.documentNo, this.itemNo, 
                    this.barcode, this.quantity, this.RefClient, this.Observacioens, this.codiDireccionEnvio);
                JuliaGrupUtils.Log.Logger.WriteVerbose(logMsg, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                string IssueMsg = client.AltaIncidencia(this.username, this.codClient, this.type, this.subType, this.objectID, this.documentNo, this.itemNo, this.barcode, this.quantity, this.RefClient, this.Observacioens, this.codiDireccionEnvio, ref codIncidencia);
                if (IssueMsg != string.Empty)
                {
                    throw new NavException(IssueMsg);
                }
            }
            catch (NavException ex)
            {
             throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

            return codIncidencia;
        }

        #endregion



       
    }
}
