﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using System.Collections;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class IssueProductDataAccessObject
    {
        private string clientUsername;
        public List<IssueProduct> productissue;
        
        public IssueProductDataAccessObject(string clientUsername, string codCliente)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                productissue = new List<IssueProduct>();

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaProductos ProductsList = new JuliaWs.ListaProductos();
                //Añadimos el 4 parametro para que compile...
                MessageError = client.GetItemListByCliente(clientUsername, codCliente, ref ProductsList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < ProductsList.Producto.Count(); i++)
                    {
                        this.productissue.Add(new IssueProduct(ProductsList.Producto[i]));
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public IssueProductDataAccessObject(string clientUsername, int objectID, string DocNo)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                productissue = new List<IssueProduct>();

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaBarcodes BarCodeList = new JuliaWs.ListaBarcodes();
                //Añadimos el 4 parametro para que compile...
                MessageError = client.GetBarcodesByDoc(clientUsername, objectID, DocNo, ref BarCodeList);
                if (MessageError == string.Empty)
                {
                    for (int i = 0; i < BarCodeList.Barcode.Count(); i++)
                    {
                        this.productissue.Add(new IssueProduct(BarCodeList.Barcode[i]));
                    }

                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
           
        }
        public List<IssueProduct> GetProductsFromDoc(string DocNo)
        {

            var Products = (from p in this.productissue
                            select p);
          
            return Products.ToList();
        }
        public IssueProduct GetProductFromItemNo(string ItemNo)
        {
            var Product = (from p in this.productissue
                            where p.ItemNo == ItemNo
                            select p);

            if (Product.Count() > 0)
            {
                //com a molt en tindrem un pq busquem per codi
                return Product.First();
            }
            else
            {
                return null;
            }

        }
        public List<IssueProduct> GetBarCodesFromProd(string ItemNo)
        {
            var BarCodes = (from p in this.productissue
                            where p.ItemNo == ItemNo
                            select p);
            return BarCodes.ToList();
        }
      
        public string GetProductImagesIssueByCode(string code, string version)
        {
            List<SPFile> list = new List<SPFile>();
            string ImageUrl = string.Empty;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {

                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                        <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <Eq>
                                                        <FieldRef Name=""ContentType"" />
                                                        <Value Type=""Computed"">Assembly instructions (image)</Value>
                                                    </Eq>
                                                </And>
                                            </Where>";

                            query.Folder = folder;
                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = splist.GetItems(query);
                            if (imagescollection.Count == 1)
                            {
                                ImageUrl = imagescollection[0].File.ServerRelativeUrl;
                            }



                            //foreach (SPFile file in folder.Files)
                            //{
                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
                            //        && IsTypedImage(file.Item.Name, type))
                            //        list.Add(file);
                            //}
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (!(string.IsNullOrEmpty(ImageUrl)))
            {
                return ImageUrl;
            }
            else
            {
                return "";
            }

        }
    }
}
