﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;


namespace JuliaGrupUtils.DataAccessObjects
{
    public class NewsDataAccessObject
    {
        public List<News> news;


        #region Constructor

        public NewsDataAccessObject()
        {
            GetSPNews();
        }

        #endregion

        #region Private Methods

        private void GetSPNews()
        {
            this.news = new List<News>();
                try{
                using (SPSite site = new SPSite(ConstantManager.ExtranetURL))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        SPList newsList = web.Lists[ConstantManager.NewsList_Name];

                        SPQuery query = new SPQuery();
                        query.Query = 
                            "<OrderBy>" +
                                "<FieldRef Name='" + ConstantManager.NewsList_Field_Date + "' Ascending='False' />" + 
                            "</OrderBy>";

                        SPListItemCollection results = newsList.GetItems(query);

                        foreach(SPListItem noticia in results)
                        {
                            News nuevaNoticia = new News();
                            if (noticia[ConstantManager.NewsList_Field_Title] != null)
                                nuevaNoticia.Title = noticia[ConstantManager.NewsList_Field_Title].ToString();
                            if (noticia[ConstantManager.NewsList_Field_SubTitle] != null)
                                nuevaNoticia.Subtitle = noticia[ConstantManager.NewsList_Field_SubTitle].ToString();
                            if (noticia[ConstantManager.NewsList_Field_Image] != null)
                                nuevaNoticia.Image = noticia[ConstantManager.NewsList_Field_Image].ToString();
                            if (noticia[ConstantManager.NewsList_Field_Date] != null)                                
                                nuevaNoticia.Date = DateTime.Parse(noticia[ConstantManager.NewsList_Field_Date].ToString()).ToShortDateString();
                            if (noticia[ConstantManager.NewsList_Field_URL] != null)
                                nuevaNoticia.Url = noticia[ConstantManager.NewsList_Field_URL].ToString();

                            this.news.Add(nuevaNoticia);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_SHAREPOINT_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        #endregion

        #region Public Methods

        public List<News> GetNews()
        {
            this.GetSPNews();

            return this.news;
        }

        #endregion
    }
}
