﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.OfertasWs;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using System.Globalization;

namespace JuliaGrupUtils.DataAccessObjects
{
    [Serializable]
    public class OrderDataAccesObject
    {
        private List<Order> orders;

        public OrderDataAccesObject()
        {
            orders = new List<Order>();
        }

        #region Public methods

        public List<Order> GetAllOrders()
        {
            return this.orders;
        }

        public Order GetCurrentNavOrder(Client c,int TipoCompra)
        {
            Order order = null;
            string result = string.Empty;
            try
            {                
                if (this.orders.Count == 0)
                {
                    this.SetOrdersList(c, TipoCompra);  
                    if (this.orders.Count > 0)
                        return orders.Last<Order>();

                    JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                    //no hi ha ofertes,  n'obrim una
                    result = client.CreateSales(c.Code, c.UserName, TipoCompra);

                    if (result == String.Empty)
                    {
                        //s'ha creat correctament, carreguem la order ultima
                        this.SetOrdersList(c, TipoCompra);
                        order = this.orders.Last<Order>();
                    }
                 
                }
                else
                {
                    return orders.Last<Order>();
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
          
            return order;
        }
        public decimal getPortes(string codUser,string OrderNum, string codCLient)
        {
            string value = string.Empty;
            decimal portesRef = decimal.Zero;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //int value = client2.InsertItem(order.NavOferta.No, p.Code, quantity, p.CatalogNo, p.BusinessLine, linedescription);
                value = client2.GetPortes(codUser, OrderNum, codCLient, ref portesRef);
                if (value != string.Empty)
                {
                    NavException ex = new NavException(value);
                    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    throw ex;
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
            return portesRef;
        }
        public string CheckTotalVolumenGDO(string codUser, string OrderNum, string codCLient)
        {
            string value = string.Empty;
            String CheckContenedor = String.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //int value = client2.InsertItem(order.NavOferta.No, p.Code, quantity, p.CatalogNo, p.BusinessLine, linedescription);
                value = client2.CheckTotalVolumenGDO(codUser, OrderNum, codCLient, ref CheckContenedor);
                if (value != string.Empty)
                {
                    NavException ex = new NavException(value);
                    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    throw ex;
                }

            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
           
            return CheckContenedor;
        }
        
        public void AddOrderLine(Order order, Article p, int quantity, string linedescription, string username)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //int value = client2.InsertItem(order.NavOferta.No, p.Code, quantity, p.CatalogNo, p.BusinessLine, linedescription);
                value = client2.InsertItem(order.NavOferta.No, p.Code, quantity, p.InsertcatalogNo, p.BusinessLine, linedescription, username);
                if (value == string.Empty)
                {
                OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);            
                order.NavOferta = client.Read(order.NavOferta.No);
                //Al añadir columna cambia el % de Volumen, lo actualizamos
                order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
              
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void UpdateOrderLineByLineNo(Order order, int lineNo, int Quantity, string linedescription, string username)
        {
            string value = String.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.UpdateQuantity(order.NavOferta.No, lineNo, Quantity, linedescription, username);
                if (value == String.Empty)
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                    //Al añadir columna cambia el % de Volumen, lo actualizamos
                    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void UpdateOrderLine(Order order, OrderLine line, string linedescription, string username)
        {
            string value = String.Empty;
            try{
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.UpdateQuantity(order.NavOferta.No, line.LineNo, line.Quantity, linedescription, username);
                if (value == String.Empty)
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                    //Al añadir columna cambia el % de Volumen, lo actualizamos
                    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }


        public void DeleteOrderLineByLineNo(Order order, int lineNo, string username) {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.DeleteItem(order.NavOferta.No, lineNo, username);
                if (value == string.Empty)
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                    //Al eliminar columna cambia el % de Volumen, lo actualizamos
                    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void DeleteOrderLine(Order order, OrderLine line, string username)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
            
                value = client2.DeleteItem(order.NavOferta.No, line.LineNo, username);
                if (value == string.Empty)
                {
                OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                order.NavOferta = client.Read(order.NavOferta.No);
                //Al eliminar columna cambia el % de Volumen, lo actualizamos
                order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }
           
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void DeleteOrderLines(Order order,string username)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.BorrarLineasCarrito (order.NavOferta.No,username);
                if (value == string.Empty)
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                    //Al eliminar columna cambia el % de Volumen, lo actualizamos
                    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }
        public void UpdateOrder(Order order, string username)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);


                value = client2.ModifySales(order.OrderNumber, order.PaymentMethod, order.DeliveryAddress, order.DeliveryType, order.ReferenciaPedido, order.OrderObservations, username,order.FechaRequerida,order.GdoBox, order.Campaña, order.PuntoRecogida);
                if (value == string.Empty)
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                    //Al añadir columna cambia el % de Volumen, lo actualizamos
                    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }

        }

        public bool SendOrderToNav(Order order, string username, int TipoCompra)
        {
        string value = string.Empty;
          bool ret;
            try
            {
               
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.ConfirmarSales(order.NavOferta.No, order.OrderClient.Code, order.OrderObservations, order.ReferenciaPedido, username,order.FechaRequerida,TipoCompra,order.GdoBox,order.Campaña, order.PuntoRecogida);
                if (value == string.Empty)
                {
                    ret = true;
                }
                else 
                {
                    ret = false;
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
            return ret;
        }

        public Order GetOrderById(string id, Client c)
        {
            Order order = null;
            try{

                OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                OfertasWs.Oferta oferta = client.Read(id);
                if (oferta == null)
                    return null;

                order = new Order(oferta, c);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return order;
        }

        public void SetOrdersList(Client c, int TipoCompra)
        {
           try
           {

                if (this.orders != null && this.orders.Count > 0)
                    return;

                this.orders = new List<Order>();

                // GET ALL ORDERS
                OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                OfertasWs.Oferta_Filter filter = new OfertasWs.Oferta_Filter();
                filter.Field = OfertasWs.Oferta_Fields.Sell_to_Customer_No;
                filter.Criteria = c.Code;

                OfertasWs.Oferta_Filter filter2 = new OfertasWs.Oferta_Filter();
                filter2.Field = OfertasWs.Oferta_Fields.Status;
                //filter2.Criteria = "Open";
                //REZ: Para cuando el WS esta en castellano
                filter2.Criteria = "Abierto";
                OfertasWs.Oferta_Filter filter3 = new OfertasWs.Oferta_Filter();
                filter3.Field = OfertasWs.Oferta_Fields.Channel;
                filter3.Criteria = "Pàgina Web";
                OfertasWs.Oferta_Filter filter4 = new OfertasWs.Oferta_Filter();
                filter4.Field = OfertasWs.Oferta_Fields.Borrador;
                filter4.Criteria = "true";
               //REZ 18/03/2013 - Filtro para oferta por usuario
                OfertasWs.Oferta_Filter filter5 = new OfertasWs.Oferta_Filter();
                filter5.Field = OfertasWs.Oferta_Fields.UsuarioCreacionWEB;
                filter5.Criteria = c.UserName;  //cuurentUser.UserName ?? 
                OfertasWs.Oferta_Filter filter6 = new OfertasWs.Oferta_Filter();
                if (TipoCompra == 1)
                {//Si TipoCompra no es GDO
                    filter6.Field = OfertasWs.Oferta_Fields.OrderType;
                    filter6.Criteria = "GDO";  //cuurentUser.UserName ?? 
                }
                else
                {
                    filter6.Field = OfertasWs.Oferta_Fields.OrderType;
                    filter6.Criteria = "<>GDO";  //cuurentUser.UserName ?? 
                }
                OfertasWs.Oferta_Filter[] filters = { filter, filter2, filter3, filter4, filter5, filter6 };

                OfertasWs.Oferta[] ofertas = client.ReadMultiple(filters, string.Empty, 0);
            
                foreach (OfertasWs.Oferta oferta in ofertas)
                {                
                    Order order = new Order(oferta, c);
                    orders.Add(order);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public string GetDescripcionPagos(string codUser,string CodFormaPago,string CodTerminosPago,string FieldToFill)
        {
            
            string value = string.Empty;
            string FormaPagotxt = string.Empty;
            string TerminosPagoTxt=  string.Empty;
         
            try
            {

                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.GetDescripcionPagos(codUser, CodFormaPago, CodTerminosPago, ref FormaPagotxt, ref TerminosPagoTxt);
                if (value != String.Empty)
                {
                    NavException ex = new NavException(value);
                    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    throw ex;
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (FieldToFill == "PaymentMethod")
                return FormaPagotxt;
            else {
                return TerminosPagoTxt;
            } 
        }
        public List<TipoContenedor> GetTipoContenedores()
        {

            List<TipoContenedor> ListaContenedores = new List<TipoContenedor>();
            try
            {
                /*ListaContenedoresWs.ListaContenedores contenedor = new ListaContenedoresWs.ListaContenedores();
                contenedor.
                TipoContenedor lisa = new TipoContenedor(contenedor);*/
                //Get direcciones envio
                ListaContenedoresWs.ListaContenedores_Service contenedoresService = new ListaContenedoresWs.ListaContenedores_Service();
                contenedoresService.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                
                 //ListaContenedoresWs.ListaContenedores_Filter contFilter = new ListaContenedoresWs.ListaContenedores_Filter();
                //contFilter.Field = ListaContenedoresWs.ListaContenedores_Fields.Code;
                //contFilter.Criteria = "";
                //ListaContenedoresWs.ListaContenedores_Filter[] contFilters = { contFilter };
                
                ListaContenedoresWs.ListaContenedores[] contenedores = contenedoresService.ReadMultiple(null, string.Empty, 0);

                foreach (ListaContenedoresWs.ListaContenedores cont in contenedores)
                {
                    ListaContenedores.Add(new TipoContenedor(cont, CultureInfo.CurrentUICulture.LCID.ToString()));
                }

                return ListaContenedores;

                
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
          

        }
        public string GetOfertaPdf(string codUser, string NoOferta, string vista,int TipoCompra)
        {

            string value = string.Empty;
            string Doc = string.Empty;

            try
            {

                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.GetOfertaPDF(codUser, NoOferta, ref Doc, Convert.ToInt32(vista),TipoCompra);
                if (value != String.Empty)
                {
                    NavException ex = new NavException(value);
                    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    throw ex;
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        
            return Doc;
           
        }
        public string InsertInspirate(Order order, string codUser, string codCatalog ,string NoOferta)
        {

            string value = string.Empty;


            try
            {

                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.InsertInspirate(codUser, codCatalog, NoOferta);
                if (value != String.Empty)
                {
                    NavException ex = new NavException(value);
                    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    throw ex;
                }
                else
                {
                    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    order.NavOferta = client.Read(order.NavOferta.No);
                }

            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return value;

        }
        #endregion

        #region Private methods

        

        #endregion
    }
}
