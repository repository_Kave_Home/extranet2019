﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class ParetoDataAccessObject
    {
        private string clientUsername = String.Empty;
        public List<Pareto> ParetosList;
   

        public ParetoDataAccessObject(string clientUsername, string codCliente)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.ParetosList = new List<Pareto>();

                ////els articles son diferents segons sigui un o altre usuari, es creara una cache per usuari i es carreguen
                ////els articles que ens envien des del webservice de JuliaGrup
               
                    JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    JuliaWs.ParetosList ParetosWeb = new JuliaWs.ParetosList();
                    MessageError = client.GetConfigIncidencias(clientUsername, ref ParetosWeb);
                    if (MessageError == string.Empty)
                    {
                        for (int i = 0; i < ParetosWeb.Paretos.Count(); i++)
                        {
                            this.ParetosList.Add(new Pareto(ParetosWeb.Paretos[i]));
                        }

                    }

                
               
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
          
        }

       
        public List<string> GetparetosType()
        {
            return (from p in this.ParetosList
                              select p.Type).Distinct().ToList();


        }
        public List<string> GetparetosTypeDescription()
        {
            return (from p in this.ParetosList
                    select p.TypeDescription).Distinct().ToList();
        }
        public List<string> GetParetoSubtype(string Type)
        {
            return (from p in this.ParetosList
                    where p.Type == Type
                    select p.SubType).ToList();
        }
        public List<string> GetParetoSubtypeDescription(string Type)
        {
            return (from p in this.ParetosList
                    where p.Type == Type
                    select p.SubTypeDescription).ToList();
        }

        public Pareto GetRequiredFields(string Type,string SubType)
        {
            var paretolist = (from p in this.ParetosList
                    where  p.Type == Type && p.SubType == SubType
                    select p);
            if (paretolist.Count() > 0)
                return paretolist.First();
            else return null;
        }

        public int GetFormId(string Type, string SubType)
        {
            var Formid = from p in this.ParetosList
                          where p.Type == Type && p.SubType == SubType
                          select p.VistaFormID;
            return Formid.First();
        }
       
    }
}
