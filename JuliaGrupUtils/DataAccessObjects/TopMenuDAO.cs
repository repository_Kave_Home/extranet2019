﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using System.Web;
using JuliaGrupUtils.Business;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;



namespace JuliaGrupUtils.DataAccessObjects
{
    [Serializable]
    public class TopMenuDAO
    {
        private string clientUsername = String.Empty;
        public List<TopMenuItem> MenuItems;

        public TopMenuDAO(string clientUsername, string codCliente,int TipoCompra, string idioma) {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try{
                this.MenuItems = new List<TopMenuItem>();
                string strTipoCompra = (TipoCompra == 0) ? "" : "Gdo";
                string strCacheConstant = string.Format("TopMenu{0}_{1}_{2}", strTipoCompra, clientUsername, codCliente);
                ////elmenú es diferent segons sigui un o altre usuari, es creara una cache per usuari i client es carreguen
                if (HttpContext.Current.Cache.Get(strCacheConstant) == null)
                {
                    //Obtener menu
                    GetTopMenuSQL(codCliente, TipoCompra, idioma);
                    //Añadir a cache
                    HttpContext.Current.Cache.Add(strCacheConstant, this.MenuItems, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                }
                else
                {
                    this.MenuItems = (List<TopMenuItem>)HttpContext.Current.Cache.Get(strCacheConstant);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (MessageError != String.Empty)
            {
                NavException ex = new NavException(MessageError);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        private void GetTopMenuSQL(string cliente, int tipoportal, string idioma)
        {
            //Hay un error y ahora 1 = LaForma, 0 = GDO
            tipoportal += 1;

            //Conectamos a SQL
            string connectionString = ConfigurationManager.AppSettings["NavisionDBConnectionString"];
            // Create a connection
            using (SqlConnection sdwDBConnection = new SqlConnection(connectionString))
            {
                // Open the connection
                sdwDBConnection.Open();

                //Lanzamos la Query
                string strTopmenuQuery = String.Format(@"
                                        select * from  WEB_Menu
                                        where 
                                            cliente='{0}' 
                                            and WTipoPortal={1}
                                            and idioma = '{2}'
                                        order by 
                                            WTipoPortal,cliente,Tipo,Codigo_rel, orden"
                                            , cliente, tipoportal, idioma);

                SqlCommand queryCommand = new SqlCommand(strTopmenuQuery, sdwDBConnection);

                // Use the above SqlCommand object to create a SqlDataReader object.
                SqlDataReader queryCommandReader = queryCommand.ExecuteReader();

                // Create a DataTable object to hold all the data returned by the query.
                DataTable dataTable = new DataTable();

                // Use the DataTable.Load(SqlDataReader) function to put the results of the query into a DataTable.
                dataTable.Load(queryCommandReader);

                //Añadimos los resultados
                foreach (DataRow dtr in dataTable.Rows)
                {
                    TopMenuItem MenuItem = new TopMenuItem(
                        dtr["Tipo"].ToString(),
                        dtr["Codigo"].ToString(),
                        //dtr["Descripcion"].ToString(),
                        dtr["DescripcionIdioma"].ToString(),
                        dtr["Codigo_rel"].ToString(),
                        (int)dtr["Orden"]);

                    this.MenuItems.Add(MenuItem);
                }
            }

        }

    }
}
