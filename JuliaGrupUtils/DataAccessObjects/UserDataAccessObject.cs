﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using Microsoft.SharePoint.Utilities;
using JuliaGrupUtils.Utils;
using Microsoft.SharePoint;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Data;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;
using Microsoft.Practices.SharePoint.Common;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class UserDataAccessObject
    {
        public UserDataAccessObject() { }


        public string PreRegisterUser(string username, string password, string clientmail, string clientID, string clientCIF, string clientCode, int language)
        {
            string result = LanguageManager.GetLocalizedString("RegistrationGenericErrorMsg");
            try
            {
                Guid guid = System.Guid.NewGuid();

                string passwordMD5 = Utilities.ConvertToMD5(password);
                string idioma = Utils.Utilities.ConvertLCIDtoNAVLanguages(language);

                string email = clientmail;
                string registrationSubject = LanguageManager.GetLocalizedString("RegistrationMailSubject");
                string body = LanguageManager.GetLocalizedString("RegistrationActivationMail");

                result = this.RegisterUserToNavision(username, passwordMD5, clientCode, clientCIF, idioma, clientmail, guid.ToString());
                //Get client info
                if (result == String.Empty)
                {
                    //preparemail with info and link
                    string linkToActivate = SPContext.Current.Site.Url + "/_layouts/login/Activation.aspx?token=" + guid;
                    body = body.Replace("$URL$", linkToActivate);
                    //send email
                    SPUtility.SendEmail(null, false, false, email, registrationSubject, body);
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                //throw new Exception(new StackFrame(1).GetMethod().Name, ex);
                result = LanguageManager.GetLocalizedString("RegistrationGenericErrorMsg");
            }
            return result;
        }
        public string RememberPass(string codUser, string email, string newPass)
        {
            string registrationSubject = LanguageManager.GetLocalizedString("RememberPassMailSubject");
            string body = LanguageManager.GetLocalizedString("RememberPassMail");

            JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
            client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

            string pass = string.Empty;
            pass = _ConvertToMD5(newPass);

            string MessageError = client.ResetPassword(codUser, email, pass);
            if (MessageError == string.Empty)
            {
                //send email
                body = body.Replace("$pass$", newPass);
                SPUtility.SendEmail(null, false, false, email, registrationSubject, body);
                return string.Empty;
            }
            else
            {
                return MessageError;
            }

        }

        //        //13/11/2012 - REZ:: Funcion previa
        //        public void PreRegisterUser(string username, string password, string clientmail, string clientID, string clientCIF)
        //        {
        //            try{
        //                Guid guid = System.Guid.NewGuid();
        //                int result = 0;

        //                string passwordMD5 = Utilities.ConvertToMD5(password);

        //                string insertquery = @"INSERT INTO [JuliaRegister].[dbo].[UserRegistration]
        //                                       ([Guid]
        //                                       ,[Username]
        //                                       ,[Password]
        //                                       ,[RegistrationMail]
        //                                       ,[ClientCode]
        //                                       ,[ClientCif]
        //                                       ,[RegistrationDate]
        //                                       ,[ActivationDate])
        //                                 VALUES
        //                                       ('" + guid + "','" + username + "','" + passwordMD5 + "','" + clientmail + "','" + clientID + "','" + clientCIF + "',getdate(), null)";

        //                string email = String.Empty;
        //                string registrationSubject = LanguageManager.GetLocalizedString("RegistrationMailSubject");
        //                string body = LanguageManager.GetLocalizedString("RegistrationActivationMail");
        //                //using (SPWeb web = SPContext.Current.Web)
        //                //{
        //                    string connectionString = ConfigurationManager.AppSettings["RegistrationDBConnectionString"];
        //                    using (SqlConnection sqlConn = new SqlConnection(connectionString))
        //                    {
        //                        //register new pre-registration
        //                        sqlConn.Open();
        //                        using (SqlCommand comm = new SqlCommand(insertquery, sqlConn))
        //                        {
        //                            result = comm.ExecuteNonQuery();
        //                        }
        //                        sqlConn.Close();
        //                    }
        //                    //Get client info

        //                    //preparemail with info and link
        //                    string linkToActivate = SPContext.Current.Site.Url + "/_login/Activation.aspx?token=" + guid;
        //                    body = body.Replace("$URL$", linkToActivate);


        //                    //send email
        //                    SPUtility.SendEmail(null, false, false, email, registrationSubject, body);
        //                //}

        //             }
        //            catch (Exception ex)
        //            {
        //                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG );
        //                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
        //            }
        //        }


        // 0 - ok
        // 1 - error user password incorrect


        public string ActivateRegisterUser(Guid activationguid, int idioma)
        {
            string resultat = string.Empty;
            try
            {
                resultat = this.EnableUserToNavision(activationguid.ToString(), idioma);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return resultat;
        }


        //19/11/2012 - REZ:: Solo pasamos el Guid y validamos al cargar la página
        //        public string ActivateRegisterUser(string username, string password, Guid activationguid, int idioma)
        //        {
        //            string resultat = string.Empty;
        //            try
        //            {
        //                string passwordMD5 = Utilities.ConvertToMD5(password);
        //                resultat = this.EnableUserToNavision(username, passwordMD5, activationguid.ToString());

        ////            string query = @"SELECT ClientCode,ClientCif,RegistrationMail
        ////                              FROM [JuliaRegister].[dbo].[UserRegistration]
        ////                              where [Guid] = '" + activationguid + "' and Username = '" + username + "' and [Password] = '" + passwordMD5 + "'";

        ////            string updateActivationDate = @"update [JuliaRegister].[dbo].[UserRegistration]
        ////                                            set ActivationDate = GETDATE()
        ////                                            where [Guid] = '" + activationguid + "' and Username = '" + username + "' and [Password] = '" + passwordMD5 + "'";



        ////                string connectionString = ConfigurationManager.AppSettings["RegistrationDBConnectionString"];
        ////                using (SqlConnection sqlConn = new SqlConnection(connectionString))
        ////                {
        ////                    sqlConn.Open();
        ////                    using(SqlCommand comm = new SqlCommand(query, sqlConn))
        ////                    {
        ////                        SqlDataReader dr = comm.ExecuteReader();
        ////                        if (!dr.HasRows)
        ////                        {
        ////                            resultat = "1";
        ////                        }
        ////                        else
        ////                        {
        ////                            string clientCode = string.Empty;
        ////                            string clientCif = string.Empty;
        ////                            string clientMail = string.Empty;
        ////                            while (dr.Read())
        ////                            {
        ////                                clientCode = (string)dr[0];
        ////                                clientCif = (string)dr[1];
        ////                                clientMail = (string)dr[2];
        ////                            }
        ////                            dr.Close();
        ////                            dr.Dispose();

        ////                            using (SqlCommand comm2 = new SqlCommand(updateActivationDate, sqlConn))
        ////                            {
        ////                               int aux = comm2.ExecuteNonQuery();
        ////                                if (aux==0)
        ////                                    return "1";
        ////                                else
        ////                                {
        ////                                    //TODO: Insertar l'usuari a NAVISION per tal d'activar-lo
        ////                                    //resultat = this.RegisterUserToNavision(username, passwordMD5, clientCode, clientCif, Utils.Utilities.ConvertLCIDtoNAVLanguages(idioma), clientMail);
        ////                                    //13112012 - REZ:: Se ha cambiado la logica, habrá que llamar a una funcion de activación (ActivarCuenta)
        ////                                }
        ////                            }
        ////                        }                                                
        ////                    }
        ////                }

        //            }
        //            catch (Exception ex)
        //            {
        //                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
        //                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
        //            }
        //            return resultat;
        //        }

        private string RegisterUserToNavision(string username, string password, string codeUser, string cif, string idioma, string mail, string activationGuid)
        {
            string result = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                result = client.AltaUsuari(username, password, codeUser, cif, idioma.ToString(), mail, activationGuid);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }
        public void DeleteUsuario(string userLogado, string user)
        {
            string result = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                result = client.DeleteUser(userLogado, user);
                if (result == string.Empty)
                {

                }
                else
                {
                    throw new NavException(result);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        ////REZ 10072014 - Sobrecarga para que no pete de momento. Indica false a AccesoGDO y VerStock
        //public void UpdateUsuario(string userLogado, string user, string pass, string codCliente, string pmail, decimal Coeficiente) {
        //    UpdateUsuario(userLogado, user, pass, codCliente, pmail, Coeficiente, false, false);
        //}
        public void UpdateUsuario(string userLogado, string user, string pass, string codCliente, string pmail, decimal Coeficiente, bool accesoGDO, bool verstock)
        {

            string result = string.Empty;
            try
            {
                String passwordmd5 = _ConvertToMD5(pass);
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                result = client.UpdateUser(userLogado, user, passwordmd5, codCliente, pmail, Coeficiente, accesoGDO, verstock);
                if (result == string.Empty)
                {

                }
                else
                {
                    throw new NavException(result);
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

        }

        ////REZ 10072014 - Sobrecarga para que no pete de momento. Indica false a AccesoGDO y VerStock
        //public void AddUsuario(string uselog, string user, string pass, string coclient, string mail, decimal coeficiente) { 
        //    AddUsuario( uselog, user, pass, coclient, mail,  coeficiente, false, false);
        //}

        public void AddUsuario(string uselog, string user, string pass, string coclient, string mail, decimal coeficiente, bool accesoGDO, bool verstock)
        {
            string result = string.Empty;
            try
            {
                string passwordmd5 = _ConvertToMD5(pass);
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                result = client.InsertUser(uselog, user, passwordmd5, coclient, mail, coeficiente, accesoGDO, verstock);
                if (result == string.Empty)
                {

                }
                else
                {
                    throw new NavException(result);
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

        }
        //19/11/2012 - REZ:: Solo enviamos el Guid
        //private string EnableUserToNavision(string username, string password, string activationGuid)
        //{
        //    string result = string.Empty;
        //    try
        //    {
        //        JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
        //        client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
        //        result = client.ActivarCuenta(username, password, activationGuid);
        //    }
        //    catch (Exception ex)
        //    {
        //        JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
        //        throw new Exception(new StackFrame(1).GetMethod().Name, ex);
        //    }
        //    return result;
        //}

        private string EnableUserToNavision(string activationGuid, int language)
        {
            string result = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                string idioma = JuliaGrupUtils.Utils.Utilities.ConvertLCIDtoNAVLanguages(language);
                result = client.ActivarCuenta(activationGuid, idioma);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }


        private static string _ConvertToMD5(string text)
        {
            //Declarations
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            String result = "";
            try
            {
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                md5 = new MD5CryptoServiceProvider();
                originalBytes = ASCIIEncoding.Default.GetBytes(text);
                encodedBytes = md5.ComputeHash(originalBytes);

                //Convert encoded bytes back to a 'readable' string
                result = BitConverter.ToString(encodedBytes);

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return result;
        }

        public string ValidateUser(string username, string password)
        {
            //Provider info at --> C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\WebServices\SecurityToken\web.config
            string result = "Wrong Userid or Password";
            int tipoUsuari = 0;

            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                string passwordmd5 = _ConvertToMD5(password);
                string idioma = string.Empty;
                result = client.ValidarUsuari(username, passwordmd5, ref tipoUsuari);
                //12/11/2012 - REZ: Queremos devolver el mensaje de error en caso que lo haya
                //if (result== string.Empty)
                //{
                //    result = "";
                //}

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            //12/11/2012 - REZ: Queremos devolver el mensaje de error en caso que lo haya
            //if (result != String.Empty)
            //{
            //    NavException ex = new NavException(result);
            //    //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
            //    throw ex;
            //}
            return result;
        }

        public List<User> FindUsersByEmail(string email)
        {
            string result = string.Empty;
            List<User> users = new List<User>();

            try
            {
                UsuariosWs.UsuariosWEB_Service usuariosws = new UsuariosWs.UsuariosWEB_Service();
                usuariosws.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields._x003C_Mail_x003E_;
                filter.Criteria = email;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = usuariosws.ReadMultiple(filters, string.Empty, 0);

                foreach (UsuariosWs.UsuariosWEB user in usuarios)
                {
                    User usr = new User();
                    usr.UserName = user.Usuario;
                    usr.Email = user.Mail;
                    users.Add(usr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return users;
        }


        public List<User> FindUsersByName(string username)
        {
            string result = string.Empty;
            List<User> users = new List<User>();

            try
            {
                UsuariosWs.UsuariosWEB_Service usuariosws = new UsuariosWs.UsuariosWEB_Service();
                usuariosws.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = usuariosws.ReadMultiple(filters, string.Empty, 0);

                foreach (UsuariosWs.UsuariosWEB user in usuarios)
                {
                    User usr = new User();
                    usr.UserName = user.Usuario;
                    usr.Email = user.Mail;
                    users.Add(usr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return users;
        }

        public List<User> GetUser(string username)
        {
            string result = string.Empty;
            List<User> users = new List<User>();

            try
            {
                UsuariosWs.UsuariosWEB_Service usuariosws = new UsuariosWs.UsuariosWEB_Service();
                usuariosws.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = usuariosws.ReadMultiple(filters, string.Empty, 0);

                foreach (UsuariosWs.UsuariosWEB user in usuarios)
                {
                    User usr = new User();
                    usr.UserName = user.Usuario;
                    usr.Email = user.Mail;
                    users.Add(usr);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return users;
        }

        public void ChangePass(String UserName, String oldPass, String newPass)
        {
            try
            {
                string oldpasswordMD5 = Utilities.ConvertToMD5(oldPass);
                string newpasswordMD5 = Utilities.ConvertToMD5(newPass);

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                string MessageError = client.CambiaPassword(UserName, oldpasswordMD5, newpasswordMD5);
                if (MessageError != string.Empty)
                {
                    NavException ex = new NavException(MessageError);
                    throw ex;
                }
                //RTP:

            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void Updatelanguage(String UserCode, String LanguageCode)
        {
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                client.ModifyIdioma(UserCode, LanguageCode);
                //RTP:

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }


        public string GetUserLanguageForUsername(string username)
        {
            string lang;
            try
            {
                UsuariosWs.UsuariosWEB_Service client = new UsuariosWs.UsuariosWEB_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

                UsuariosWs.UsuariosWEB currentUser = usuarios[0];
                lang = currentUser.Idioma;

            }
            catch (NavException ex)
            {
                throw ex;
            }
            return lang;
        }

        public User GetUserWithUsername(string username)
        {
            Agent a = null;
            try
            {

                //REZ :: 11/3/2013 -- Añadimos el tipo de cliente Grupo

                UsuariosWs.UsuariosWEB_Service client = new UsuariosWs.UsuariosWEB_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

                UsuariosWs.UsuariosWEB currentUser = usuarios[0];

                switch (currentUser.Tipo)
                {
                    case UsuariosWs.Tipo.Cliente:
                        ClientDataAccessObject clientDAO = new ClientDataAccessObject();
                        Client c = clientDAO.GetClientByUsername(username);
                        return c;
                        break;

                    case UsuariosWs.Tipo.Representante:
                        AgentDataAccess agentDAO = new AgentDataAccess();
                        a = agentDAO.GetAgentWithUsername(username);
                        return a;
                        break;

                    case UsuariosWs.Tipo.Grupo_Cliente:
                        ClientGroupDataAccessObject clientgroupDAO = new ClientGroupDataAccessObject();
                        ClientGroup cg = clientgroupDAO.GetAgentWithUsername(username); //REZ -- Hacer funcionaes correctamente
                        return cg;
                        break;

                }
                //EoF - REZ :: 11/3/2013 -- Añadimos el tipo de cliente Grupo

                //ClientDataAccessObject clientDAO = new ClientDataAccessObject();
                //Client c = clientDAO.GetClientByUsername(username);

                //if (c != null)
                //    return c;
                //else
                //{
                //    AgentDataAccess agentDAO = new AgentDataAccess();
                //    a = agentDAO.GetAgentWithUsername(username);

                //}
            }
            catch (NavException ex)
            {
                throw ex;
            }
            return a;
        }


        public string[] GetRolesForUser(string username)
        {
            string[] Roles = null;
            try
            {
                UsuariosWs.UsuariosWEB_Service client = new UsuariosWs.UsuariosWEB_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                UsuariosWs.UsuariosWEB_Filter filter = new UsuariosWs.UsuariosWEB_Filter();
                filter.Field = UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);
                //if (usuarios.Count() == 1)
                UsuariosWs.UsuariosWEB currentUser = usuarios[0];

                switch (currentUser.Tipo)
                {
                    case UsuariosWs.Tipo.Cliente:
                        if (currentUser.TipoAcceso == UsuariosWs.TipoAcceso.Admin)
                        {
                            Roles = new string[] { "client", "clientadmin" };
                        }
                        else
                        {
                            Roles = new string[] { "client", "clientbasic" };
                        }

                        break;

                    case UsuariosWs.Tipo.Representante:
                        Roles = new string[] { "representant" };
                        break;

                    case UsuariosWs.Tipo.Grupo_Cliente:
                        Roles = new string[] { "client", "clientgrup" };
                        break;

                }
                //EoF - REZ :: 11/3/2013 -- Añadimos el tipo de cliente Grupo

                //ClientDataAccessObject clientDAO = new ClientDataAccessObject();
                //Client c = clientDAO.GetClientByUsername(username);

                //if (c != null)
                //    return c;
                //else
                //{
                //    AgentDataAccess agentDAO = new AgentDataAccess();
                //    a = agentDAO.GetAgentWithUsername(username);

                //}
            }
            catch (NavException ex)
            {
                throw ex;
            }
            return Roles;
        }
        //        public string GetLogoUrl(string Code)
        //        {
        //            string URL = string.Empty;
        //            List<String> list = new List<String>();
        //            try
        //            {

        //                SPSecurity.RunWithElevatedPrivileges(delegate()
        //                {

        //                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
        //                    {
        //                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
        //                        {
        //                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
        //                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ClientDocumentSetList + "/" + Code);
        //                            SPQuery query = new SPQuery();
        //                            query.Query = @"<Where>
        //                                                    <Eq>
        //                                                        <FieldRef Name=""ContentType"" />
        //                                                            <Value Type=""Computed"">Imagen</Value>
        //                                                    </Eq>                                              
        //                                            </Where>";

        //                            query.Folder = folder;
        //                            query.ViewAttributes = "Scope=\"RecursiveAll\"";
        //                            query.QueryThrottleMode = SPQueryThrottleOption.Override;
        //                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
        //                            SPListItemCollection imagescollection = splist.GetItems(query);

        //                            foreach (SPListItem image in imagescollection)
        //                            {
        //                                list.Add(image.Url);
        //                            }

        //                            //foreach (SPFile file in folder.Files)
        //                            //{
        //                            //    if (IsImgType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true")
        //                            //        && IsTypedImage(file.Item.Name, type))
        //                            //        list.Add(file);
        //                            //}
        //                        }
        //                    }
        //                });
        //            }
        //            catch (Exception ex)
        //            {
        //                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
        //                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
        //            }
        //            if (list.Count == 0)
        //            {
        //                return string.Empty;
        //            }
        //            else
        //            {
        //                return list.First();
        //            }
    }
}