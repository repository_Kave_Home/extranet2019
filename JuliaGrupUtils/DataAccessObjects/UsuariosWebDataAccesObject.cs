﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupUtils.DataAccessObjects
{
    public class UsuariosWebDataAccesObject
    {
        public List<UsuariosWEB> UsuariosWebList;
        private string clientUsername = String.Empty;
        public UsuariosWebDataAccesObject(string clientUsername, string codCliente)
        {
            this.clientUsername = clientUsername;
            string MessageError = string.Empty;
            try
            {
                this.UsuariosWebList = new List<UsuariosWEB>();

                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaUsuariosWEB UsuariosWebListWS = new JuliaWs.ListaUsuariosWEB();
                
                MessageError = client.GetUsuariosWEB(clientUsername, codCliente, ref UsuariosWebListWS);
                if (MessageError == string.Empty)
                {
                    if (UsuariosWebListWS.UsuarioWEB != null)
                    {
                        foreach (JuliaWs.UsuarioWEB user in UsuariosWebListWS.UsuarioWEB)
                        {
                            this.UsuariosWebList.Add(new UsuariosWEB(user));
                        }
                    }
                    else
                    { 
                         
                    }

                   
                }
                else
                {
                    NavException ex = new NavException(MessageError);
                    throw ex;
                }
            }
            catch (NavException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        public void UpdateCoef(string UsernameLog,string Username,string codCliente,Decimal coeficiente,bool todos)
        {
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaUsuariosWEB UsuariosWebListWS = new JuliaWs.ListaUsuariosWEB();

                string MessageError = client.UpdateCoef(UsernameLog, Username, codCliente, coeficiente, todos);
                if (MessageError == string.Empty)
                {

                }
                else
                {
                    throw new NavException(MessageError);
                }
            }
            catch (NavException ex)
            {

                throw ex;
            }
            catch (Exception ex)
            {
                
                throw;
            }
          
        }
        public void UpdateMail(string UsernameLog, string Username, string email)
        {
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                JuliaWs.ListaUsuariosWEB UsuariosWebListWS = new JuliaWs.ListaUsuariosWEB();

                string MessageError = client.UpdateMail(UsernameLog, Username, email);
                if (MessageError == string.Empty)
                {

                }
                else
                {
                    throw new NavException(MessageError);
                }
            }
            catch (NavException ex)
            {

                throw ex;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<UsuariosWEB> GetUsuariosList()
        {
            return (from p in this.UsuariosWebList
                  select p ).ToList();


        }

        public void UpdateDivisa(string UsernameLog, string Username, string codCliente, bool verDivisa, Decimal coeficiente, string LiteralDivisa, bool todos) {
            try
            {
                JuliaWs.NAVCodeunitWS client = new JuliaWs.NAVCodeunitWS();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                string MessageError = client.UpdateDivisa(UsernameLog, Username, codCliente, verDivisa, coeficiente, LiteralDivisa, todos);
                if (MessageError == string.Empty)
                {

                }
                else
                {
                    throw new NavException(MessageError);
                }
            }
            catch (NavException ex)
            {

                throw ex;
            }
            catch (Exception ex)
            {

                throw;
            }
        
        }

    }
}
