﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using System.Diagnostics;

namespace JuliaGrupUtils.DataAccessObjects
{
    [Serializable]
    public class VentaDataAccessObject
    {
        private List<Venta> ventas;

        public VentaDataAccessObject()
        {
            ventas = new List<Venta>();
        }

        #region Public methods

        public List<Venta> Ventas()
        {
            return this.ventas;
        }

        public List<Venta> GetLastVentas(int numb)
        {
            var lastventas = (from e in ventas
                              orderby e.Fecha descending
                              select e).Take(numb).ToList<Venta>();
                
                
            return lastventas;
        }

        public int TotalVentas()
        {
            return this.ventas.Count();
        }

        public void SetCurrentNavVentas(String  ClientCode)
        {
            try
            {
                //OrderPedido orderpedido = null;
                //Get orders from NAV
                //just the open ones, we take the newest one
                ListaVentasWs.ListaVentas_Service client = new ListaVentasWs.ListaVentas_Service();            
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ListaVentasWs.ListaVentas_Filter filter = new ListaVentasWs.ListaVentas_Filter();
                filter.Field = ListaVentasWs.ListaVentas_Fields.Sell_to_Customer_No;
                filter.Criteria = ClientCode;

                //ListaVentasWs.ListaVentas_Filter filter2 = new ListaVentasWs.ListaVentas_Filter();
                //filter2.Field = ListaVentasWs.ListaVentas_Fields.Borrador_WEB;
                //filter2.Criteria = "false";

                //ListaVentasWs.ListaVentas_Filter filter3 = new ListaVentasWs.ListaVentas_Filter();
                //filter3.Field = ListaVentasWs.ListaVentas_Fields.EnviadoCompletamente;
                //filter3.Criteria = "false";

                ListaVentasWs.ListaVentas_Filter[] filters = { filter};

                ListaVentasWs.ListaVentas[] vnts = client.ReadMultiple(filters, string.Empty, 0);
                foreach (ListaVentasWs.ListaVentas venta in vnts)
                {
                    Venta v = new Venta(venta );
                    this.ventas.Add(v);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG );
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void SetCurrentAgentNavVentas(String AgentCode)
        {
            try
            {
                //OrderPedido orderpedido = null;
                //Get orders from NAV
                //just the open ones, we take the newest one
                ListaVentasWs.ListaVentas_Service client = new ListaVentasWs.ListaVentas_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ListaVentasWs.ListaVentas_Filter filter = new ListaVentasWs.ListaVentas_Filter();
                filter.Field = ListaVentasWs.ListaVentas_Fields.Salesperson_Code;
                filter.Criteria = AgentCode;

                //ListaVentasWs.ListaVentas_Filter filter2 = new ListaVentasWs.ListaVentas_Filter();
                //filter2.Field = ListaVentasWs.ListaVentas_Fields.Borrador_WEB;
                //filter2.Criteria = "false";

                //ListaVentasWs.ListaVentas_Filter filter3 = new ListaVentasWs.ListaVentas_Filter();
                //filter3.Field = ListaVentasWs.ListaVentas_Fields.EnviadoCompletamente;
                //filter3.Criteria = "false";

                ListaVentasWs.ListaVentas_Filter[] filters = { filter};

                ListaVentasWs.ListaVentas[] vnts = client.ReadMultiple(filters, string.Empty, 0);
                foreach (ListaVentasWs.ListaVentas venta in vnts)
                {
                    Venta v = new Venta(venta);
                    this.ventas.Add(v);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }
        public void SetCurrentClientGroupNavVentas(String ClientGroupCode)
        {
            try
            {
                //OrderPedido orderpedido = null;
                //Get orders from NAV
                //just the open ones, we take the newest one
                ListaVentasWs.ListaVentas_Service client = new ListaVentasWs.ListaVentas_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                ListaVentasWs.ListaVentas_Filter filter = new ListaVentasWs.ListaVentas_Filter();
                filter.Field = ListaVentasWs.ListaVentas_Fields.codGrupo;
                filter.Criteria = ClientGroupCode;

                //ListaVentasWs.ListaVentas_Filter filter2 = new ListaVentasWs.ListaVentas_Filter();
                //filter2.Field = ListaVentasWs.ListaVentas_Fields.Borrador_WEB;
                //filter2.Criteria = "false";

                //ListaVentasWs.ListaVentas_Filter filter3 = new ListaVentasWs.ListaVentas_Filter();
                //filter3.Field = ListaVentasWs.ListaVentas_Fields.EnviadoCompletamente;
                //filter3.Criteria = "false";

                ListaVentasWs.ListaVentas_Filter[] filters = { filter };

                ListaVentasWs.ListaVentas[] vnts = client.ReadMultiple(filters, string.Empty, 0);
                foreach (ListaVentasWs.ListaVentas venta in vnts)
                {
                    Venta v = new Venta(venta);
                    this.ventas.Add(v);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
        }

        //public void AddOrderPedidoLine(OrderPedido orderPedido, Article p, int quantity, string linedescription)
        //{
        //    JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
        //    client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

        //    int value = client2.InsertItem(orderPedido.NavOferta.No, p.Code, quantity, p.CatalogNo, p.BusinessLine);

        //    PedidosWs.Pedido_Service client = new PedidosWs.Pedido_Service();
        //    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
        //    orderPedido.NavOferta = client.Read(orderPedido.NavOferta.No);            
            
        //    //update el nostre objecte order amb la linia
        //    orderPedido.RefreshOrderProductList();
        //}

        //public void DeleteOrderPedidoLine(OrderPedido orderPedido, OrderLine line)
        //{
        //    JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
        //    client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

        //    int value = client2.DeleteItem(orderPedido.NavOferta.No, Decimal.Parse(line.LineNo));

        //    PedidosWs.Pedido_Service client = new PedidosWs.Pedido_Service();
        //    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
        //    orderPedido.NavOferta = client.Read(orderPedido.NavOferta.No);      
        //    //update el nostre objecte order amb la linia
        //    orderPedido.RefreshOrderProductList();
        //}


        //public Order GetOrderPedidoById(string id)
        //{
        //    this.GetInternalOrders();

        //    return null;
        //}

        #endregion
    }
}
