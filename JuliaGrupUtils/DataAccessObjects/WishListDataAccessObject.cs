﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.WishListWS;
using System.Diagnostics;
using JuliaGrupUtils.ErrorHandler;

namespace JuliaGrupUtils.DataAccessObjects
{
    [Serializable]
    class WishListDataAccessObject
    {


        #region constructor
        public WishListDataAccessObject()
        {

        }
        #endregion constructor

        #region Public methods

        public List<WishlistLine> GetCurrentNavWishlist(string username, string clientcode)
        {
            List<WishlistLine> output = null;
            output = SetWishList(username, clientcode);
            return output;
        }

        public void AddWishListLine(Wishlist wshlst, string username, string clientcode, string articleCode, string catalogCode, int quantity)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_Insert(username, articleCode, catalogCode, quantity, clientcode);
                if (value == string.Empty)
                {
                    //Obtenemos el resultado de la insercion de NAV
                    WishListWS.WishList_Service wishlistSvc = new WishListWS.WishList_Service();
                    wishlistSvc.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    WishListWS.WishList wsl = wishlistSvc.Read(username, clientcode, articleCode);

                    WishlistLine currentWsl = wshlst.WishlistLines.Where<WishlistLine>(p => p.Product.Code == wsl.ItemNo).FirstOrDefault();
                    if (currentWsl != null)
                    {
                        currentWsl.Product.CatalogNo = wsl.CatalogoNo;
                        currentWsl.Quantity = (int)wsl.Qty;
                    }
                    else
                    {
                        wshlst.WishlistLines.Add(new WishlistLine(wsl));
                    }
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void UpdateWishListLine(string username, string clientcode, string articleCode, int quantity)
        {
            string value = String.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_Modify(username, articleCode, quantity, clientcode);
                if (value == String.Empty)
                {
                    WishListWS.WishList_Service wishlistSvc = new WishListWS.WishList_Service();
                    wishlistSvc.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                    WishListWS.WishList wsl = wishlistSvc.Read(username, clientcode, articleCode);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void Wish_AddCart(string username, string clientcode, string pOfertaNo)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_AddCart(username, clientcode, pOfertaNo);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void Wish_AddItemToCart(string pUsuario, string pCliente, string pOfertaNo, string pItemNo)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_AddItemToCart(pUsuario, pCliente, pOfertaNo, pItemNo);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void Wish_EndInsertCart(string pUsuario, string pCliente, string pOfertaNo)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_EndInsertCart(pUsuario, pCliente, pOfertaNo);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void DeleteWishListLine(string username, string clientcode, string articleCode)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_Delete(username, articleCode, clientcode);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }

        public void DeleteWishList(string username, string clientcode)
        {
            string value = string.Empty;
            try
            {
                JuliaWs.NAVCodeunitWS client2 = new JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                value = client2.Wish_DeleteAll(username, clientcode);
            }

            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
        }



        #endregion Public methods

        #region Private methods
        private List<WishlistLine> SetWishList(string username, string clientcode)
        {
            List<WishlistLine> output = new List<WishlistLine>();
            try
            {
                // GET ALL WishListLines
                WishListWS.WishList_Service wishlistSvc = new WishListWS.WishList_Service();
                wishlistSvc.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                WishListWS.WishList_Filter Usernamefilter = new WishList_Filter();
                Usernamefilter.Field = WishListWS.WishList_Fields.Usuario;
                Usernamefilter.Criteria = username;
                WishListWS.WishList_Filter ClientCodefilter = new WishList_Filter();
                ClientCodefilter.Field = WishListWS.WishList_Fields.CustomerNo;
                ClientCodefilter.Criteria = clientcode;

                WishListWS.WishList_Filter[] filters = { Usernamefilter, ClientCodefilter };

                WishListWS.WishList[] wishlistlines = wishlistSvc.ReadMultiple(filters, string.Empty, 0);

                foreach (WishListWS.WishList wishlistline in wishlistlines)
                {
                    WishlistLine wshlline = new WishlistLine(wishlistline, 0);  //LaForma
                    output.Add(wshlline);
                }

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return output;

        }
        #endregion Private methods


    }
}
