﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;

namespace JuliaGrupUtils.DataTransferObjects
{
    [Serializable]
    [DataContract]
    public class ArticleDTO
    {
        #region Private properties

        private string code = String.Empty;
        private string description = String.Empty;
        private string catalogNo = String.Empty;
        private string color = String.Empty;
        private string programa = String.Empty;
        private string measures = String.Empty;
        private string volume = String.Empty;
        private string height = String.Empty;
        private string width = String.Empty;
        private string weight = String.Empty;
        private string length = String.Empty;
        private string priceInPoints = String.Empty;
        private string descripcionLarga = String.Empty;
        private string numItems = String.Empty;
        private float price = 0;
        private int priceInt = 0;
        private string priceString = String.Empty;
        private string priceWithDiscount = String.Empty;
        private string discount = String.Empty;
        private decimal unidadMedida = 0;
        private int _stock = 0;
        private Decimal niveldeservicio = Decimal.Zero;
        private string niveldeservicioDescripcion = String.Empty;
        private Decimal cantidadAzul = Decimal.Zero;
        private Decimal cantidaVerde = Decimal.Zero;
        private Decimal cantidadRojo = Decimal.Zero;
        private string productNew = String.Empty;
        private string comingSoon = String.Empty;
        private Decimal cantidadGris = Decimal.Zero;
        private string descripcionGris = String.Empty;
        private string itemMateriales = String.Empty;
        private string itemMantenimiento = String.Empty;
        private string itemMontaje = String.Empty;
        private bool tieneDtoExclusivo = false;
        private string eancode = String.Empty;
        private string codigoArancelario = String.Empty;
        private string apilable = String.Empty;
        private string paisOrigen = String.Empty;
        private int articleVersion = 0;
        private string insertcatalogNo = String.Empty;
        private int tipusIcona = 0;
        private string categorizacion = String.Empty;

        private bool verQty;
        private bool stockBool;
        private bool cantidadVerdeBool;
        private bool cantidadAzulBool;
        private bool cantidadRojoBool;
        private bool cantidadGrisBool;
        private bool niveldeServicioBool;
        private bool discountBool;
        private bool isNewBool;
        private bool isDiscountBool;

        #endregion

        #region Public Properties

        [DataMember]
        public string NavigateUrl
        {
            get { return "/Pages/ProductInfo.aspx?code=" + this.code + "&version=" + this.articleVersion + "&catalog=" + this.insertcatalogNo; }
            set { }
        }
        [DataMember]
        public string NavigateUrlGdo
        {
            get { return "/gdo/Pages/productGdoInfo.aspx?code=" + this.code + "&version=" + this.articleVersion + "&catalog=" + this.insertcatalogNo; }
            set { }
        }
        [DataMember]
        public string ImgDiscountUrl
        {
            get
            {
                if (this.discount.ToString() == "0,00")
                {
                    return "/Style%20Library/Julia/img/null_pixel.png";
                }
                else
                {
                    string output = String.Empty;
                    //REZ 13052015 - Producte en liquidació
                    //if (this.CatalogNo.StartsWith("PRELIQ"))
                    if (this.TipusIcona == 2)
                    {
                        output = "/Style%20Library/Julia/img/dto_liquidaciones.png";
                    }
                    else
                    {
                        output = "/Style%20Library/Julia/img/dto_promocion.png";
                    }
                    return output;
                }

            }
            set { }
        }
        public int TipusIcona
        {
            get { return tipusIcona; }
            set { tipusIcona = value; }
        }
        [DataMember]
        public string DiscountLabel
        {
            get
            {
                if (!(this.discount == "0,00"))
                {
                    return Convert.ToDouble(this.discount, new CultureInfo("es-ES")).ToString("N0") + "%";
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        [DataMember]
        public string ImageUrl
        {
            get
            {
                return this.SmallImgUrl;
            }
            set { }
        }
        public string SmallImgUrl
        {
            get
            {
                //return "http://intranet.juliagrup.com/GestionDocumental/Documents/" + this.Code + "/" + this.Code + "·1.jpg";
                return JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                            JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" +
                                JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/"
                                    + this.Code + "/" + this.Code + "·P.jpg";
                //return "/_layouts/JuliaGrupPortalClientes_v2/JuliaGrupPortalClientesDocuments.aspx?operation=image&id=" + this.Code + "&type=1"; 
            }
        }
        [DataMember]
        public string ImgErrorUrl
        {
            get
            {
                string strCultureName = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.Name) ? "es-ES" : Thread.CurrentThread.CurrentUICulture.Name;
                //return "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_1.jpg';";
                return "this.src='/Style Library/Julia/img/" + strCultureName + "/NODISPONIBLE_P.jpg';";
            }
            set { }
        }
        [DataMember]
        public int PriceInt
        {
            get { return priceInt; }
            set { priceInt = value; }
        }
        [DataMember]
        public string PriceWithDiscount
        {
            get { return priceWithDiscount; }
            set { priceWithDiscount = value; }
        }
        [DataMember]
        public string Height
        {
            get { return height; }
            set { height = value; }
        }
        [DataMember]
        public string Numitems
        {
            get { return numItems; }
            set { numItems = value; }
        }
        [DataMember]
        public string CatalogNo
        {
            get { return catalogNo; }
            set { catalogNo = value; }
        }
        [DataMember]
        public string InsertcatalogNo
        {
            get { return insertcatalogNo; }
            set { insertcatalogNo = value; }
        }
        [DataMember]
        public string Color
        {
            get { return color; }
            set { color = value; }
        }
        [DataMember]
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        [DataMember]
        public string Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        [DataMember]
        public string Length
        {
            get { return length; }
            set { length = value; }
        }
        [DataMember]
        public string Programa
        {
            get { return programa; }
            set { programa = value; }
        }
        [DataMember]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        [DataMember]
        public string DescripcionLarga
        {
            get { return descripcionLarga; }
            set { descripcionLarga = value; }
        }
        [DataMember]
        public string Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        [DataMember]
        public float Price
        {
            get { return price; }
            set { price = value; }
        }
        [DataMember]
        public string PriceString
        {
            get { return priceString; }
            set { priceString = value; }
        }
        [DataMember]
        public string Measures
        {
            get { return measures; }
            set { measures = value; }
        }
        [DataMember]
        public string Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        [DataMember]
        public decimal UnidadMedida
        {
            get { return unidadMedida; }
            set { unidadMedida = value; }
        }
        [DataMember]
        public decimal Stock
        {
            get { return _stock; }
            set { }
        }
        [DataMember]
        public Decimal CantidadAzul
        {
            get { return cantidadAzul; }
            set { cantidadAzul = value; }
        }
        [DataMember]
        public Decimal CantidadVerde
        {
            get { return cantidaVerde; }
            set { cantidaVerde = value; }
        }
        [DataMember]
        public Decimal CantidadRojo
        {
            get { return cantidadRojo; }
            set { cantidadRojo = value; }
        }
        [DataMember]
        public Decimal NiveldeServicio
        {
            get { return niveldeservicio; }
            set { niveldeservicio = value; }
        }
        [DataMember]
        public string NiveldeservicioDescripcion
        {
            get { return niveldeservicioDescripcion; }
            set { niveldeservicioDescripcion = value; }
        }
        [DataMember]
        public string ItemMateriales
        {
            get { return itemMateriales; }
            set { itemMateriales = value; }
        }
        [DataMember]
        public string ItemMantenimiento
        {
            get { return itemMantenimiento; }
            set { itemMantenimiento = value; }
        }
        [DataMember]
        public string ItemMontaje
        {
            get { return itemMontaje; }
            set { itemMontaje = value; }
        }
        [DataMember]
        public string ProductNew
        {
            get { return productNew; }
            set { productNew = value; }
        }
        [DataMember]
        public string ComingSoon
        {
            get { return comingSoon; }
            set { comingSoon = value; }
        }
        [DataMember]
        public Decimal CantidadGris
        {
            get { return cantidadGris; }
            set { cantidadGris = value; }
        }
        [DataMember]
        public string DescripcionGris
        {
            get { return descripcionGris; }
            set { descripcionGris = value; }
        }
        [DataMember]
        public bool TieneDtoExclusivo
        {
            get { return tieneDtoExclusivo; }
            set { tieneDtoExclusivo = value; }
        }
        [DataMember]
        public string EANcode
        {
            get { return eancode; }
            set { eancode = value; }
        }
        [DataMember]
        public string CodigoArancelario
        {
            get { return codigoArancelario; }
            set { codigoArancelario = value; }
        }
        [DataMember]
        public string Apilable
        {
            get { return apilable; }
            set { apilable = value; }
        }
        [DataMember]
        public string PaisOrigen
        {
            get { return paisOrigen; }
            set { paisOrigen = value; }
        }
        [DataMember]
        public string Categorizacion
        {
            get { return categorizacion; }
            set { categorizacion = value; }
        }
        [DataMember]
        public bool VerQty
        {
            get { return verQty; }
            set { verQty = value; }
        }
        [DataMember]
        public bool StockBool
        {
            get { return (this.Stock != 3); }
            set { }
        }
        [DataMember]
        public bool CantidadVerdeBool
        {
            get { return (this.CantidadVerde != 0); }
            set { }
        }
        [DataMember]
        public bool CantidadAzulBool
        {
            get { return (this.CantidadAzul != 0); }
            set { }
        }
        [DataMember]
        public bool CantidadRojoBool
        {
            get { return (this.CantidadRojo != 0); }
            set { }
        }
        [DataMember]
        public bool CantidadGrisBool
        {
            get { return (this.CantidadGris != 0); }
            set { }
        }
        [DataMember]
        public bool NiveldeServicioBool
        {
            get { return (this.NiveldeServicio != 0); }
            set { }
        }
        [DataMember]
        public bool DiscountBool
        {
            get
            {
                bool result = false;
                if (!(this.Discount == "0,00"))
                {
                    result = (this.TieneDtoExclusivo) ? false : true;
                }
                return result;
            }
            set { }
        }
        [DataMember]
        public bool IsNewBool
        {
            get
            {
                bool result = true;
                if (this.ProductNew == "No")
                {
                    result = false;
                }
                return result;
            }
            set { }
        }
        [DataMember]
        public bool IsComingSoon
        {
            get
            {
                bool result = true;
                if (this.ComingSoon == "No")
                {
                    result = false;
                }
                return result;
            }
            set { }
        }
        [DataMember]
        public bool IsDiscountBool
        {
            get
            {
                bool result = false;
                if (this.PriceWithDiscount != "")
                {
                    result = true;
                }
                return result;
            }
            set { }
        }


        #endregion

        #region Constructor

        public ArticleDTO(JuliaGrupUtils.Business.Article p)
        {
            this.code = p.Code;
            this.description = p.Description;
            this.descripcionLarga = p.DescripcionLarga;
            this.catalogNo = p.CatalogNo;
            this.insertcatalogNo = p.InsertcatalogNo;
            this.color = p.Color;
            this.programa = p.Programa;
            this.measures = p.Measures;
            this.volume = p.Volume;
            this.height = p.Height;
            this.width = p.Width;
            this.weight = p.Weight;
            this.length = p.Length;
            this.priceInPoints = p.PriceInPoints;
            this.numItems = p.Numitems;
            this.price = p.Price;
            this.unidadMedida = (p.UnidadMedida != 0) ? p.UnidadMedida : 1;
            this._stock = (int)p.Stock;
            this.discount = p.Discount;
            this.cantidadAzul = p.CantidadAzul;
            this.niveldeservicio = p.NiveldeServicio;
            this.cantidadRojo = p.CantidadRojo;
            this.cantidaVerde = p.CantidadVerde;
            this.productNew = p.ProductNew;
            this.comingSoon = p.ComingSoon;
            this.articleVersion = p.ArticleVersion;

            this.cantidadGris = p.CantidadGris;
            this.descripcionGris = p.DescripcionGris;
            this.tieneDtoExclusivo = p.TieneDtoExclusivo;

            this.eancode = p.EANcode;
            this.codigoArancelario = p.CodigoArancelario;
            this.apilable = p.Apilable;

            this.itemMateriales = p.ItemMateriales;
            this.itemMantenimiento = p.ItemMantenimiento;
            this.itemMontaje = p.ItemMontaje;
            this.paisOrigen = p.PaisOrigen;
            this.tipusIcona = p.TipusIcona;

            this.priceInt = p.PriceInt;
            this.priceString = p.PriceString;
            this.priceWithDiscount = p.PriceWithDiscount;
            this.categorizacion = p.Categorizacion;

            this.niveldeservicio = p.NiveldeServicio;
            this.niveldeservicioDescripcion = p.NiveldeservicioDescripcion;
        }

        #endregion
    }
}
