﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JuliaGrupUtils.ErrorHandler
{
    public class NavException : System.Exception
    {
        private string value;
        private string p;

        public NavException() : base() { }
        public NavException(string message) : base(message) { }
        public NavException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an 
        // exception propagates from a remoting server to the client.  
        protected NavException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }

       
    }
}
