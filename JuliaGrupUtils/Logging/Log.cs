﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.SharePoint.Common.ServiceLocation;
using Microsoft.Practices.SharePoint.Common.Logging;

namespace JuliaGrupUtils.Log
{
    public static class Constantes
    {
        public const String DATA_AREA_LOG = "DataLayer";
        public const String DATA_AREA_CAT_LAYER_LOG = "LayerObject";
        public const String DATA_AREA_CAT_SHAREPOINT_LOG = "Sharepoint";
        public const String DATA_AREA_CAT_WEBSERVICE_LOG = "WebService";

        public const String BUSINESS_AREA_LOG = "BusinessLayer";
        public const String BUSINESS_AREA_CAT_LAYER_LOG = "BusinessObject";

        public const String PRESENTATION_AREA_LOG = "DataLayer";
        public const String PRESENTATION_AREA_CAT_LAYER_LOG = "LayerObject";
    }
    public static class Logger
    {
        public static void WriteLogger(Exception ex, String Msg, String Area, String Catetgory)
        {
            ILogger logger = SharePointServiceLocator.GetCurrent().GetInstance<ILogger>();
            logger.LogToOperations(ex, Msg, 0, Microsoft.SharePoint.Administration.EventSeverity.Error, string.Format("{0}/{1}", Area, Catetgory));
        }

        public static void WriteDebug(Exception ex, String Msg, String Area, String Catetgory)
        {
            ILogger logger = SharePointServiceLocator.GetCurrent().GetInstance<ILogger>();
            logger.LogToOperations(ex, Msg, 0, Microsoft.SharePoint.Administration.EventSeverity.Error, string.Format("{0}/{1}", Area, Catetgory));
        }

        public static void WriteVerbose(String Msg, String Area, String Category)
        {
            ILogger logger = SharePointServiceLocator.GetCurrent().GetInstance<ILogger>();
            logger.LogToOperations(Msg, 0, Microsoft.SharePoint.Administration.EventSeverity.Verbose, string.Format("{0}/{1}", Area, Category));
        }

    }


    //public static class Logger
    //{
    //    public static void WriteLogUnexpectedError(string Message, string Stacktrace)
    //    {
    //        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Julia Grup", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, Message, Stacktrace);
    //    }

    //    public static void WriteLogInformation(string Message, string Stacktrace)
    //    {
    //        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Julia Grup", TraceSeverity.High, EventSeverity.Information), TraceSeverity.High, Message, Stacktrace);
    //    }
    //}
}
