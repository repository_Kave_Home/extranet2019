﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.BusinessData.Infrastructure.SecureStore;
using Microsoft.Office.SecureStoreService.Server;
using System.Security;
using Microsoft.SharePoint.Administration;
using System.Runtime.InteropServices;
using System.Configuration;

namespace JuliaGrupUtils.SecureStoreUtils
{
    public static class SecureStoreUtils
    {

        //public static string SECURESTORETARGETID = "NAVISION";       

        public static string DbConnectionStringFromAppSettings
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["JuliaGrupDbConnString"];
            }
        }

        public static string SecureStoreId
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["SecureStoreId"];
            }
        }

        public static string IntranetUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["IntranetSiteUrl"];
            }
        }

        public static string DocumentManagementUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["DocumentManagementUrl"];
            }
        }

        public static string ArticlesDocumentLibraryName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ArticlesDocumentLibraryName"];
            }
        }

        public static string IssueSiteUrl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["IssueSiteUrl"];
            }
        }

        public static string IssueListName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["IssueListName"];
            }
        }

        public static string[] GetSecureStoreCredentials(Guid siteId)
        {
            using (SPSite site = new SPSite(siteId))
            {
                // Get the default Secure Store Service provider.
                ISecureStoreProvider provider = SecureStoreProviderFactory.Create();
                if (provider == null)
                {
                    throw new InvalidOperationException("Unable to get an ISecureStoreProvider");
                }

                ISecureStoreServiceContext providerContext = provider as ISecureStoreServiceContext;
                providerContext.Context = SPServiceContext.GetContext(site);

                // Create the variables to hold the credentials.
                string userName = null;
                string password = null;
                string pin = null;
                
                try
                {
                    using (SecureStoreCredentialCollection creds = provider.GetCredentials(SecureStoreId))
                    {
                        if (creds != null)
                        {
                            foreach (SecureStoreCredential cred in creds)
                            {
                                if (cred == null)
                                {
                                    continue;
                                }

                                switch (cred.CredentialType)
                                {
                                    case SecureStoreCredentialType.UserName:
                                        if (userName == null)
                                        {
                                            userName = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                    case SecureStoreCredentialType.WindowsUserName:
                                        if (userName == null)
                                        {
                                            userName = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;

                                    case SecureStoreCredentialType.Password:
                                        if (password == null)
                                        {
                                            password = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                    case SecureStoreCredentialType.WindowsPassword:
                                        if (password == null)
                                        {
                                            password = GetStringFromSecureString(cred.Credential);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    if (userName == null || password == null)
                    {
                        throw new InvalidOperationException("Unable to get the credentials");
                    }

                    string[] result = { userName, password };
                    return result;
                }
                catch (SecureStoreException e)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("SPNavIntegrationJob_Families", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, e.Message + " - " + e.StackTrace, null);
                }
                string[] aux = { };
                return aux;
            }
        }

        private static string GetStringFromSecureString(SecureString secStr)
        {
            if (secStr == null)
            {
                return null;
            }

            IntPtr pPlainText = IntPtr.Zero;
            try
            {
                pPlainText = Marshal.SecureStringToBSTR(secStr);
                return Marshal.PtrToStringBSTR(pPlainText);
            }
            finally
            {
                if (pPlainText != IntPtr.Zero)
                {
                    Marshal.FreeBSTR(pPlainText);
                }
            }
        }
    }
}
