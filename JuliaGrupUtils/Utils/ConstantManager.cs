﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Configuration;

namespace JuliaGrupUtils.Utils
{
    public static class ConstantManager
    {
        

        public static string ContentType_Original_PagesLists
        {
            get
            {
                return "Document";
            }
        }

        public static string ExtranetURL
        {
            get
            {
                return ConfigurationManager.AppSettings["ExtranetURL"];
            }
        }

        public static string IntranetURL
        {
            get
            {
                return ConfigurationManager.AppSettings["IntranetURL"];
            }
        }

        #region Gestion documental
        public static string GestionDocumentalWeb
        {
            get
            {
                return ConfigurationManager.AppSettings["GestionDocumentalWebUrl"];
            }
        }

        public static string ArticleList
        {
            get
            {
                return ConfigurationManager.AppSettings["ArticleListName"];
            }
        }

        public static string ArticleListUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ArticleListUrl"];
            }
        }

        public static string ArticleDocumentSetList
        {
            get
            {
                return ConfigurationManager.AppSettings["ArticleDocumentSetList"];
            }
        }

        public static string ArticleDocumentSetListUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ArticleDocumentSetListUrl"];
            }
        }
            

        public static string ArticleDocumentSetContentTypeName
        {
            get
            {
                return ConfigurationManager.AppSettings["ArticleDocumentSetContentTypeName"];
            }
        }
        public static string ClientDocumentSetList
        {
            get
            {
                return ConfigurationManager.AppSettings["ClientDocumentSetList"];
            }
        }
        public static string ClientDocumentSetListUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ClientDocumentSetListUrl"];
            }
        }
        public static string ClientBillDocumentSetContentTypeName
        {
            get
            {
                return ConfigurationManager.AppSettings["ClientBillDocumentSetContentTypeName"];
            }
        }
         
        
        public static string ClientBillContentTypeName
        {
            get
            {
                return ConfigurationManager.AppSettings["ClientBillContentTypeName"];
            }
        }
       public static string ClientDeliveryNoteContentTypeName
        {
            get
            {
                return ConfigurationManager.AppSettings["ClientDeliveryNoteContentTypeName"];
            }
        }
        

        #endregion

        #region NEWS

        public static string NewsList_Name
        {
            get
            {
                return "News";
            }
        }

        public static string NewsList_Field_Title
        {
            get
            {
                return "Title";
            }
        }

        public static string NewsList_Field_SubTitle
        {
            get 
            {
                return "ArticleByLine";
            }
        }

        public static string NewsList_Field_Image
        {
            get
            {
                return "PublishingRollupImage";
            }
        }

        public static string NewsList_Field_URL
        {
            get
            {
                return "FileRef";
            }
        }

        public static string NewsList_Field_Content
        {
            get
            {
                return "PublishingPageContent";
            }
        }

        public static string NewsList_Field_Date
        {
            get
            {
                return "ArticleStartDate";
            }
        }

        #endregion

        #region COMMUNICATIONS
        
        public static string CommunicationsList_Name
        {
            get
            {
                return "Communications";
            }
        }

        public static string CommunicationsList_Field_Date
        {
            get
            {
                return "ArticleStartDate";
            }
        }

        public static string CommunicationsList_Field_Title
        {
            get
            {
                return "Title";
            }
        }

        public static string CommunicationsList_Field_Subtitle
        {
            get
            {
                return "ArticleByLine";
            }
        }

        public static string CommunicationsList_Field_Content
        {
            get
            {
                return "PublishingPageContet";
            }
        }

        public static string CommunicationsList_Field_Image
        {
            get
            {
                return "PublishingRollupImage";
            }
        }

        public static string CommunicationsList_Field_URL
        {
            get
            {
                return "FileRef";
            }
        }

        public static string CommunicationsList_Field_AssignedTo
        {
            get
            {
                return "AssignedTo";
            }
        }

        #endregion

        #region INCIDENCIAS

        public static string IssuesList_Name
        {
            get 
            {
                return "Issues";
            }
        }

        public static string IssuesList_Field_Bill
        {
            get
            {
                return "Bill";
            }
        }

        public static string IssuesList_Field_Product
        {
            get
            {
                return "Product";
            }
        }

        public static string IssuesList_Field_Comments
        {
            get
            {
                return "Issue Comments";
            }
        }

        public static string IssuesList_Field_Points
        {
            get
            {
                return "Points";
            }
        }

        #endregion

        #region JULIA WEB SERVICE
        
        public static string WebserviceUser
        {
            get
            {
                return ConfigurationManager.AppSettings["WebServiceUser"];
            }
        }

        public static string WebservicePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["WebServicePassword"];
            }
        }

        public static string WebserviceDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["WebServiceDomain"];
            }
        }
        #endregion

    }
}
