﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Utilities;

namespace JuliaGrupUtils.Utils
{
    public static class LanguageManager
    {
        public static string GetLocalizedString(string key)
        {
            uint currentLcId = (uint)System.Threading.Thread.CurrentThread.CurrentUICulture.LCID;
            return GetLocalizedString(key, currentLcId);
        }
        public static string GetLocalizedString(string key, uint lcID)
        {
            return SPUtility.GetLocalizedString("$Resources:JuliaGrupPortalClientesResources," + key, "JuliaGrupPortalClientesResources", lcID);
        }

        public static string GetColorToolTip(string key)
        {
            uint currentLcId = (uint)System.Threading.Thread.CurrentThread.CurrentUICulture.LCID;
            return GetColorToolTip(key, currentLcId);
        }
        public static string GetColorToolTip(string key, uint lcID)
        {
            return SPUtility.GetLocalizedString("$Resources:ColorsResources," + key, "ColorsResources", lcID);
        }

        
    }
}
