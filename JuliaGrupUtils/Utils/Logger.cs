﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;

namespace JuliaGrupUtils.Utils
{
    public static class Logger
    {
        public static void WiteLog(string message, string stackTrace)
        {
            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("JuliaGrup", TraceSeverity.Unexpected, EventSeverity.Error),
                TraceSeverity.Unexpected, message, stackTrace);
        }
    }
}
