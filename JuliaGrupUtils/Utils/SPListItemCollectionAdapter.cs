﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace JuliaGrupUtils.Utils
{
    /* http://asadewa.wordpress.com/2008/01/03/direct-linq-to-splistitemcollection/ */
    public class SPListItemCollectionAdapter : List<SPListItem>
    {
        private SPListItemCollection _listItemCollection;

        public SPListItemCollectionAdapter(SPListItemCollection listItemCollection)
        {
            _listItemCollection = listItemCollection;

            Refresh();
        }

        private void Refresh()
        {
            this.Clear();

            foreach (SPListItem item in _listItemCollection)
            {
                this.Add(item);
            }
        }

    }

}
