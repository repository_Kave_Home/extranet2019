﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.SharePoint;

using System.Data;
//using Microsoft.SharePoint.Search.Query;
//using Microsoft.Office.Server.Search.Administration;
//using Microsoft.Office.Server.Search.Query;
//using Microsoft.SharePoint.Administration;

//using Microsoft.Office.Server;
//using Microsoft.Office.Server.UserProfiles;
//using Microsoft.Office.Server.SocialData;
//using Microsoft.Office.Server.ActivityFeed;
using System.Web;
using Microsoft.SharePoint.Publishing;

//using Microsoft.SharePoint.Portal.WebControls;

//Se utiliza para acceder a datos de MyProfile (Colleagues, ...)
using System.Web.UI;


namespace JuliaGrupUtils.Utils
{
    class SiteDataQuery
    {

        //public static DataTable GetByContentTypeFromSite(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, string urlSite, string filter)
        //{
        //    using (SPWeb webSite = getNewWeb(urlSite))
        //    {
        //        DataTable output = GetByContentType(rowLimit, viewFields, sortField, ascending, lists, cType, webSite, false, filter);
        //        return output;
        //    }
        //}

        //public static DataTable GetByContentType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, SPWeb webSite, bool recursive, string filter)
        //{
        //    return GetByContentsType(rowLimit, viewFields, sortField, ascending, lists, new string[] { cType }, webSite, recursive, filter);
        //}

        //public static DataTable GetByContentsType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string[] cType, SPWeb webSite, bool recursive, string filter)
        //{
        //    DataTable output = new DataTable();
        //    string where = "";
        //    SPSiteDataQuery squery = new SPSiteDataQuery()
        //    {
        //        Lists = lists,
        //        QueryThrottleMode = SPQueryThrottleOption.Default,
        //        RowLimit = rowLimit,
        //        ViewFields = viewFields
        //    };
        //    if (recursive)
        //        squery.Webs = "<Webs Scope=\"Recursive\" />";

        //    if (!String.IsNullOrEmpty(filter))
        //        where = "<Where><And>{0}{1}</And></Where>{2}";
        //    else
        //    {
        //        where = "<Where>{0}{1}</Where>{2}";
        //        filter = "";
        //    }
        //    squery.Query = String.Format(where,
        //            getContentsTypesSubquery(cType, 0),
        //            filter,
        //            "<OrderBy><FieldRef Name=\"" + sortField + "\" Ascending=\"" + ((ascending) ? "TRUE" : "FALSE") + "\"/></OrderBy>");

        //    output = webSite.GetSiteData(squery);

        //    return output;
        //}

        //private static string getContentsTypesSubquery(string[] contsType, int indx)
        //{
        //    if (contsType.Length == 0)
        //        return null;
        //    else if (contsType.Length <= (indx + 1))
        //        return "<Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + contsType[indx] + "</Value></Eq>";
        //    else return "<Or><Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + contsType[indx] + "</Value></Eq>"
        //            + getContentsTypesSubquery(contsType, indx + 1) + "</Or>";
        //}

        //REZ - Começa aqui?
        //news.cs
        

        //Usando Audience Filtering
        public static DataTable GetByContentTypeWholeSiteRecursive(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, bool AudienceFiltering, bool ShowUntargetedItems)
        {
            DataTable output;
            if (AudienceFiltering == true)
            {
                output = GetByContentType(rowLimit, viewFields, sortField, ascending, lists, cType, SPContext.Current.Web, true, AudienceFiltering, ShowUntargetedItems);
            }
            else
            {
                output = GetByContentTypeWholeSiteRecursive(rowLimit, viewFields, sortField, ascending, lists, cType);
            }

            return output;
        }

        public static DataTable GetByContentTypeWholeSiteRecursive(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType)
        {
            DataTable output = GetByContentType(rowLimit, viewFields, sortField, ascending, lists, cType, SPContext.Current.Web, true, null);

            return output;
        }

        public static DataTable GetByContentType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, SPWeb webSite, bool recursive, string filter)
        {
            return GetByContentsType(rowLimit, viewFields, sortField, ascending, lists, new string[] { cType }, webSite, recursive, filter);
        }

        public static DataTable GetByContentsType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string[] cType, SPWeb webSite, bool recursive, string filter)
        {
            DataTable output = new DataTable();
            string where = "";
            SPSiteDataQuery squery = new SPSiteDataQuery()
            {
                Lists = lists,
                QueryThrottleMode = SPQueryThrottleOption.Default,
                RowLimit = rowLimit,
                ViewFields = viewFields
            };
            if (recursive)
                squery.Webs = "<Webs Scope=\"Recursive\" />";

            if (!String.IsNullOrEmpty(filter))
                where = "<Where><And>{0}{1}</And></Where>{2}";
            else
            {
                where = "<Where>{0}{1}</Where>{2}";
                filter = "";
            }
            squery.Query = String.Format(where,
                    getContentsTypesSubquery(cType, 0),
                    filter,
                    "<OrderBy><FieldRef Name=\"" + sortField + "\" Ascending=\"" + ((ascending) ? "TRUE" : "FALSE") + "\"/></OrderBy>");

            output = webSite.GetSiteData(squery);

            return output;
        }

        private static string getContentsTypesSubquery(string[] contsType, int indx)
        {
            if (contsType.Length == 0)
                return null;
            else if (contsType.Length <= (indx + 1))
                return "<Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + contsType[indx] + "</Value></Eq>";
            else return "<Or><Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + contsType[indx] + "</Value></Eq>"
                    + getContentsTypesSubquery(contsType, indx + 1) + "</Or>";
        }

        //Usando CrossListQueryInfo para AudienceFiltering
        public static DataTable GetByContentType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, SPWeb webSite, bool recursive, bool AudienceFiltering, bool ShowUntargetedItems, string filter)
        {
            DataTable output = new DataTable();
            DataTable alldata = new DataTable();
            string where = "";

            CrossListQueryInfo squery = new Microsoft.SharePoint.Publishing.CrossListQueryInfo()
            {
                //    RowLimit = rowLimit,  //No podemos aplicar RowLimit para filtrar por audiencia ya que despues aplicaria el filtro y obtendriamos menos resulatdos de los deseados
                RowLimit = 2000,
                WebUrl = webSite.ServerRelativeUrl,
                Lists = lists,
                ViewFields = viewFields,
                UseCache = true,
                FilterByAudience = AudienceFiltering,
                ShowUntargetedItems = ShowUntargetedItems,
            };

            if (!String.IsNullOrEmpty(filter))
                where = "<Where><And>{0}{1}</And></Where>{2}";
            else
            {
                where = "<Where>{0}{1}</Where>{2}";
                filter = "";
            }
            squery.Query = String.Format(where,
                //getContentsTypesSubquery(cType, 0),
                    "<Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + cType + "</Value></Eq>",
                    filter,
                    "<OrderBy><FieldRef Name=\"" + sortField + "\" Ascending=\"" + ((ascending) ? "TRUE" : "FALSE") + "\"/></OrderBy>");

            //where = "<Where>{0}{1}</Where>{2}";
            //squery.Query = String.Format(where,
            //        "<Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + cType + "</Value></Eq>",
            //        "",
            //        "<OrderBy><FieldRef Name=\"" + sortField + "\" Ascending=\"" + ((ascending) ? "TRUE" : "FALSE") + "\"/></OrderBy>");


            if (recursive)
                squery.Webs = "<Webs Scope=\"Recursive\" />";


            CrossListQueryCache cache = new CrossListQueryCache(squery);
            alldata = cache.GetSiteData(SPContext.Current.Site, squery.WebUrl);

            output = alldata.Clone();
            int rowcount = 0;
            foreach (DataRow dtr in alldata.Rows)
            {
                rowcount++;
                if (rowcount > rowLimit) break;
                output.ImportRow(dtr);
            }

            return output;
        }

        //Usando CrossListQueryInfo para AudienceFiltering
        public static DataTable GetByContentType(uint rowLimit, string viewFields, string sortField, bool ascending, string lists, string cType, SPWeb webSite, bool recursive, bool AudienceFiltering, bool ShowUntargetedItems)
        {
            DataTable output = new DataTable();
            DataTable alldata = new DataTable();
            string where = "";

            CrossListQueryInfo squery = new Microsoft.SharePoint.Publishing.CrossListQueryInfo()
            {
                //    RowLimit = rowLimit,  //No podemos aplicar RowLimit para filtrar por audiencia ya que despues aplicaria el filtro y obtendriamos menos resulatdos de los deseados
                RowLimit = 2000,
                WebUrl = webSite.ServerRelativeUrl,
                Lists = lists,
                ViewFields = viewFields,
                UseCache = true,
                FilterByAudience = AudienceFiltering,
                ShowUntargetedItems = ShowUntargetedItems,
            };

            where = "<Where>{0}{1}</Where>{2}";
            squery.Query = String.Format(where,
                    "<Eq><FieldRef Name=\"ContentType\"/><Value Type=\"String\">" + cType + "</Value></Eq>",
                    "",
                    "<OrderBy><FieldRef Name=\"" + sortField + "\" Ascending=\"" + ((ascending) ? "TRUE" : "FALSE") + "\"/></OrderBy>");


            if (recursive)
                squery.Webs = "<Webs Scope=\"Recursive\" />";


            CrossListQueryCache cache = new CrossListQueryCache(squery);
            alldata = cache.GetSiteData(webSite);

            output = alldata.Clone();
            int rowcount = 0;
            foreach (DataRow dtr in alldata.Rows)
            {
                rowcount++;
                if (rowcount > rowLimit) break;
                output.ImportRow(dtr);
            }

            return output;
        }
    }
}
