﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using JuliaGrupUtils.Business;
using JuliaGrupUtils.DataTransferObjects;
using Microsoft.SharePoint;

namespace JuliaGrupUtilsWebService.WebService
{
    [ServiceContract(Namespace = "JuliaGrupUtilsWebService.WebService")]
    public interface IJuliaClients
    {
        //Cada funcion de mi servicio se define aquí. El código de la función va en JuliaClients.cs. En la clase Article debemos definir los DataMembers i DataContracts
        [WebInvoke(UriTemplate = "/getArticles", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<Article> getArticles(int inipos, int itemCount, string username);

        [WebInvoke(UriTemplate = "/getCategoryTreeViewQty", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        int getCategoryTreeViewQty(string username, string clientcode, string value, string search, string color, string catalog, string program, string provider, string grupProv, string estance, string style, int minPrice, int maxPrice, string filterMin, string filterMax, string dto, int tipoCompra, string productEsEx);

        [WebInvoke(UriTemplate = "/searchProducts", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<ArticleDTO> searchProducts(string search, string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid, int tipoCompra, decimal dollarCoef);

        [WebInvoke(UriTemplate = "/loadCategoryList", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<TopMenuItem> loadCategoryList(string username, string clientcode, string language, int tipocompra);

        [WebInvoke(UriTemplate = "/loadComparation", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<WishlistLine> loadComparation(List<string> codes, string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid);

        [WebInvoke(UriTemplate = "/loadWishlist", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<WishlistLine> loadWishlist(string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid);

        [WebInvoke(UriTemplate = "/searchEANCode", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        Article searchEANCode(string eANCode, string iPriceSel, string username, string clientCode, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid, string catalog);

        [WebInvoke(UriTemplate = "/addToCart", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        void addToCart(string username, string clientCode, string pOfertaNo);

        [WebInvoke(UriTemplate = "/addToCartFromWishlist", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        void addToCartFromWishlist(List<string> codes, string username, string clientcode, string orderID);

        [WebInvoke(UriTemplate = "/deleteWishlistItem", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        string deleteWishlistItem(string code, string username, string clientCode);

        [WebInvoke(UriTemplate = "/deleteWishlistAll", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        void deleteWishlistAll(string username, string clientCode);

        [WebInvoke(UriTemplate = "/updateProductQuantity", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        void updateProductQuantity(string code, int quantity, string username, string clientCode);

        [WebInvoke(UriTemplate = "/test", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        string test();

        [WebInvoke(UriTemplate = "/buyArticle", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        string buyArticle(string OderNo, string ProdCode, int quantity, string InsertCat, string BLine, string LineDesc, string UserName);

        [WebInvoke(UriTemplate = "/getOrderLines", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<OrderLine> getOrderLines(string OrderNo, string Username, string ClientCode, int iniPos, int maxRows);

        [WebInvoke(UriTemplate = "/getOrderLinesCount", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        int getOrderLinesCount(string OrderNo, string Username, string ClientCode);

        //[WebInvoke(UriTemplate = "/getProductDocumentsByCode?productCode={productCode}&Version={Version}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml, RequestFormat = WebMessageFormat.Json)]
        [WebInvoke(UriTemplate = "/getProductDocumentsByCode", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<ProductDocument> getProductDocumentsByCode(string productCode, string Version);

        //[WebInvoke(UriTemplate = "/GetProductImagesByCode?code={code}&version={version}&type={type}", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Xml, RequestFormat = WebMessageFormat.Json)]
        [WebInvoke(UriTemplate = "/getProductImagesByCode", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<ProductImage> getProductImagesByCode(string code, string version, string type);

        [WebInvoke(UriTemplate = "/GetJuliaURL", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        String GetJuliaURL(String userName, String clientCode);
    }
}
