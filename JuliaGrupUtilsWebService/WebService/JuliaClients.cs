﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using JuliaGrupUtils.Business;
using System.Web;
using JuliaGrupUtils.Utils;
using JuliaGrupUtils.UsuariosWs;
using System.Diagnostics;
using System.Globalization;
using JuliaGrupUtils.ErrorHandler;
using JuliaGrupUtils.DataAccessObjects;
using JuliaGrupUtils.DataTransferObjects;
using Microsoft.SharePoint;
using System.Net;
using System.ServiceModel.Web;
using Microsoft.SharePoint.WebControls;



namespace JuliaGrupUtilsWebService.WebService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    class JuliaClients : IJuliaClients
    {
        public List<Article> getArticles(int inipos, int itemCount, string username)
        {
            List<Article> articles = HttpRuntime.Cache["Articles_" + username] as List<Article>;
            return articles.Skip(inipos).Take(itemCount).ToList();
        }

        public int getCategoryTreeViewQty(string username, string clientcode, string value, string search, string color, string catalog, string program, string provider, string grupProv, string estance, string style, int minPrice, int maxPrice, string filterMin, string filterMax, string dto, int tipoCompra, string productEsEx)
        {
            String strTipoCompra = (tipoCompra == 0) ? "" : "Gdo";
            List<Article> articles = HttpRuntime.Cache["AllJuliaArticles" + strTipoCompra + username + clientcode] as List<Article>;

            articles = articles.Where(p => p.CategoriesList.Contains(value)).ToList<Article>();
            if (search != String.Empty)
            {
                search = search.Trim().ToUpper();
                articles = articles.Where(p => p.Description.ToUpper().Contains(search) || p.Code.ToUpper().Contains(search)).ToList<Article>();
            }
            if (color != String.Empty)
                articles = articles.Where(p => p.Color == color).ToList<Article>();
            if (catalog != String.Empty)
            {
                articles = articles.Where(p => p.CatalogNo == catalog).ToList<Article>();
            }
            else
            {
                articles = (from p in articles where !p.CatalogType.Equals("Inspirate") select p).ToList();
                articles = (from p in articles
                            where
                                !p.CatalogType.Equals("Promociones")
                                ||
                                p.CatalogNo.StartsWith("PRELIQ")
                            select p).ToList();
            }
            if (productEsEx != String.Empty)
                articles = articles.Where(p => p.ProductType == productEsEx).ToList<Article>();          
            if (program != String.Empty)
                articles = articles.Where(p => p.Programa == program).ToList<Article>();
            if (estance != String.Empty)
                articles = articles.Where(p => p.CategoriesList.Contains(estance)).ToList<Article>();
            if (style != String.Empty)
                articles = articles.Where(p => p.CategoriesList.Contains(style)).ToList<Article>();
            if (!String.IsNullOrEmpty(filterMin) && !String.IsNullOrEmpty(filterMax))
            {
                articles = articles.Where(p => p.PriceInt >= minPrice).ToList<Article>();
                articles = articles.Where(p => p.PriceInt <= maxPrice).ToList<Article>();
            }
            List<Article> UnionProvcollection = articles;
            if (provider != String.Empty)
            {
                if (!String.IsNullOrEmpty(grupProv))
                {
                    UnionProvcollection = articles.Where(p => p.GDOTipoUnion == grupProv).ToList<Article>();
                }
                else
                {
                    UnionProvcollection = articles.Where(p => p.GDONoVendedor == provider).ToList<Article>();
                }
                if (provider != "AllAssociatedProducts")
                {
                    articles = UnionProvcollection.Where(p => p.GDONoVendedor == provider).ToList<Article>();
                }
                else
                {
                    articles = UnionProvcollection;
                }
            }

            switch (dto)
            {
                case "0":
                    //No filter
                    break;
                case "1":
                    articles = articles.Where(p => p.Discount != "0,00").ToList<Article>();
                    articles = articles.Where(p => p.TieneDtoExclusivo == true).ToList<Article>();
                    break;
                case "2":
                    articles = articles.Where(p => p.Discount != "0,00").ToList<Article>();
                    articles = articles.Where(p => p.TieneDtoExclusivo == false).ToList<Article>();
                    break;
                case "3":
                    //Todos los productos con descuento
                    articles = articles.Where(p => p.Discount != "0,00").ToList<Article>();
                    break;
            }
            articles = articles.Where(p => p.PrincipalAgrup == "Sí").ToList<Article>();
            articles = articles.GroupBy(p => p.Code).Select(grp => grp.First()).ToList<Article>();

            return articles.Count();
        }

        public List<ArticleDTO> searchProducts(string search, string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid, int tipoCompra, decimal dollarCoef)
        {
            
            string articlesCache = tipoCompra == 0 ? "AllJuliaArticles" : "AllJuliaArticlesGdo";
            List<Article> list = HttpRuntime.Cache[articlesCache + username + clientCode] as List<Article>;
            List<ArticleDTO> resultList = new List<ArticleDTO>();
            string[] searchList = search.Split(' ');
            var query1 = list.Where(n => searchList.All(x => n.Description.ToUpperInvariant().Contains(x.ToUpperInvariant())) || searchList.All(x => n.Code.ToUpperInvariant().Contains(x.ToUpperInvariant())));
            //var query1 = (from n in list.Where(n => n.Description.ToUpperInvariant().Contains(search.Trim().ToUpperInvariant()) || n.Code.ToUpperInvariant().Contains(search.Trim().ToUpperInvariant())) select n);
            List<Article> table = query1.ToList<Article>();
            
            //no hi ha catàlegs inspirate
            //table = (from p in table where !p.CatalogType.Equals("Inspirate") select p).ToList();
            table = (from p in table where !p.CatalogType.Equals("Promociones") || p.CatalogNo.StartsWith("PRELIQ") select p).ToList();

            List<String> duplicates = table.Select(a => a.Code).GroupBy(x => x).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
            List<Article> tableLast = new List<Article>(table);
            if (!(duplicates.Count == 0))
            {
                int cont = 0;

                foreach (string Codigorepetido in duplicates)
                {
                    foreach (Article Articulo in tableLast)
                    {
                        if ((Articulo.Code.ToString() == Codigorepetido && Articulo.CatalogPorDefecto != "true"))
                        {
                            table.RemoveAt(cont);
                            cont--;
                        }
                        cont++;
                    }
                    cont = 0;
                    tableLast = new List<Article>(table);
                }
            }
            
            //Obtenim usuari per saber si és shop in shop
            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service client = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service();
            client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter filter = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter();
            filter.Field = JuliaGrupUtils.UsuariosWs.UsuariosWEB_Fields.Usuario;
            filter.Criteria = username;

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
            JuliaGrupUtils.UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB currentUser = usuarios[0];
                                           

            try
            {
                foreach (Article Articulo in tableLast)
                {   /* 2019-06-04 - Aida Lucha - Shop in shop*/

                    Articulo.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, tipoCompra, dollarCoef, currentUser.ShopInShop);
                    ArticleDTO element = new ArticleDTO(Articulo);
                    resultList.Add(element);
                }
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }

            return resultList;
        }

        public List<TopMenuItem> loadCategoryList(string username, string clientcode, string language, int tipocompra)
        {
            List<TopMenuItem> list = new List<TopMenuItem>();
            List<TopMenuItem> topMenuItems;

            TopMenuDAO topMenuDAO = new TopMenuDAO(username, clientcode, tipocompra, language);
            topMenuItems = topMenuDAO.MenuItems;

            list = topMenuItems.Where(p => p.Tipo == "0").ToList();

            return list;
        }

        public List<WishlistLine> loadComparation(List<string> codes, string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid)
        {
            List<WishlistLine> list = new List<WishlistLine>();
            Wishlist wshlst = new Wishlist();
            wshlst = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);

            //Obtenim usuari per saber si és shop in shop
            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service client = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service();
            client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter filter = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter();
            filter.Field = JuliaGrupUtils.UsuariosWs.UsuariosWEB_Fields.Usuario;
            filter.Criteria = username;

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
            JuliaGrupUtils.UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB currentUser = usuarios[0];


            foreach (WishlistLine line in wshlst.WishlistLines)
            {
                foreach (string code in codes)
                {
                    if (code == line.Product.Code)
                    {   /* 2019-06-04 - Aida Lucha - Shop in shop*/
                        line.Product.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), currentUser.ShopInShop);
                        //line.Product.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), false);
                        list.Add(line);
                    }
                }
            }
            return list;
        }

        public List<WishlistLine> loadWishlist(string username, string clientCode, string iPriceSel, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid)
        {
            List<WishlistLine> list = new List<WishlistLine>();
            Wishlist wshlst = new Wishlist();
            wshlst = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);

            //Obtenim usuari per saber si és shop in shop
            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service client = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service();
            client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter filter = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter();
            filter.Field = JuliaGrupUtils.UsuariosWs.UsuariosWEB_Fields.Usuario;
            filter.Criteria = username;

            JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
            JuliaGrupUtils.UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

            JuliaGrupUtils.UsuariosWs.UsuariosWEB currentUser = usuarios[0];


            foreach (WishlistLine line in wshlst.WishlistLines)
            {   /* 2019-06-04 - Aida Lucha - Shop in shop*/
                //line.Product.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), false);
                line.Product.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), currentUser.ShopInShop);
                list.Add(line);
            }
            return list;
        }

        public Article searchEANCode(string eANCode, string iPriceSel, string username, string clientCode, decimal coefDivisaWEB, string literalDivisaWEB, decimal coeficient, uint lcid, string catalog)
        {
            Article result = null;
            OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
            try
            {
                List<Article> articlesList = HttpRuntime.Cache["AllJuliaArticles" + username + clientCode] as List<Article>;
                var query1 = (IEnumerable<Article>)null;
                if (String.IsNullOrEmpty(catalog))
                {
                    query1 = (from n in articlesList.Where(n => n.EANcode.ToUpperInvariant() == eANCode.ToUpperInvariant()) select n);
                }
                else
                {
                    query1 = (from n in articlesList.Where(n => (n.EANcode.ToUpperInvariant() == eANCode.ToUpperInvariant() && n.CatalogNo.ToUpperInvariant() == catalog.ToUpperInvariant())) select n);
                }

                //Obtenim usuari per saber si és shop in shop
                JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service client = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Service();
                client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter filter = new JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter();
                filter.Field = JuliaGrupUtils.UsuariosWs.UsuariosWEB_Fields.Usuario;
                filter.Criteria = username;

                JuliaGrupUtils.UsuariosWs.UsuariosWEB_Filter[] filters = { filter };
                JuliaGrupUtils.UsuariosWs.UsuariosWEB[] usuarios = client.ReadMultiple(filters, string.Empty, 0);

                JuliaGrupUtils.UsuariosWs.UsuariosWEB currentUser = usuarios[0];

                Wishlist wishlist;
                var wishlistcache = HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
                if (wishlistcache != null)
                {
                    wishlist = (Wishlist)wishlistcache;
                }
                else
                {
                    wishlist = new Wishlist();
                }
                wishlist.AssignUserAndCode(username, clientCode);
                result = query1.FirstOrDefault<Article>();
                /* 2019-06-04 - Aida Lucha - Shop in shop*/
                //result.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), false);
                result.CalculatePriceWithDiscount(iPriceSel, coefDivisaWEB, literalDivisaWEB, coeficient, lcid, 0, default(decimal), currentUser.ShopInShop);
                wishlist.AddProductToWishlist(result);

                HttpContext.Current.Cache.Remove("Wishlist_" + username + "_" + clientCode);
                HttpContext.Current.Cache.Add("Wishlist_" + username + "_" + clientCode, wishlist, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                outResponse.StatusCode = HttpStatusCode.InternalServerError;
                outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                result = null;
            }
            return result;
        }

        public void addToCart(string username, string clientCode, string pOfertaNo)
        {
            Wishlist wishlistcache = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
            if (wishlistcache.WishlistLines.Count != 0)
            {
                OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
                try
                {
                    Wishlist wishlist = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
                    wishlist.AssignUserAndCode(username, clientCode);
                    wishlist.AddToCart(pOfertaNo);
                    //Actualizamos la wishlist
                    wishlist = new Wishlist();
                    wishlist.AssignUserAndCode(username, clientCode);
                    wishlist.GetWishlist();
                    HttpContext.Current.Cache.Remove("Wishlist_" + username + "_" + clientCode);
                    HttpContext.Current.Cache.Add("Wishlist_" + username + "_" + clientCode, wishlist, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    outResponse.StatusCode = HttpStatusCode.InternalServerError;
                    outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                }
            }
        }

        public void addToCartFromWishlist(List<string> codes, string username, string clientcode, string orderID)
        {
            OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
            Wishlist wishlistcache = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientcode);
            try
            {
                wishlistcache.AddItemToCart(orderID, codes);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                outResponse.StatusCode = HttpStatusCode.InternalServerError;
                outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }
        }

        public string deleteWishlistItem(string code, string username, string clientCode)
        {
            Wishlist wishlistcache = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
            if (wishlistcache.WishlistLines.Count != 0)
            {
                OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
                try
                {
                    Wishlist wishlist = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
                    wishlist.AssignUserAndCode(username, clientCode);
                    wishlist.DeleteWishlistItem(code);
                    HttpContext.Current.Cache.Remove("Wishlist_" + username + "_" + clientCode);
                    HttpContext.Current.Cache.Add("Wishlist_" + username + "_" + clientCode, wishlist, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    outResponse.StatusCode = HttpStatusCode.InternalServerError;
                    outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                    code = null;
                }
            }
            return code;
        }

        public void deleteWishlistAll(string username, string clientCode)
        {
            Wishlist wishlistcache = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
            if (wishlistcache.WishlistLines.Count != 0)
            {
                OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
                try
                {
                    Wishlist wishlist = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
                    wishlist.AssignUserAndCode(username, clientCode);
                    wishlist.DeleteWishlistAll();
                    HttpContext.Current.Cache.Remove("Wishlist_" + username + "_" + clientCode);
                }
                catch (Exception ex)
                {
                    JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                    outResponse.StatusCode = HttpStatusCode.InternalServerError;
                    outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
                }
            }
        }

        public void updateProductQuantity(string code, int quantity, string username, string clientCode)
        {
            OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
            try
            {
                Wishlist wishlist = (Wishlist)HttpContext.Current.Cache.Get("Wishlist_" + username + "_" + clientCode);
                wishlist.AssignUserAndCode(username, clientCode);
                wishlist.UpdateProductQuantity(code, quantity);
                HttpContext.Current.Cache.Remove("Wishlist_" + username + "_" + clientCode);
                HttpContext.Current.Cache.Add("Wishlist_" + username + "_" + clientCode, wishlist, null, DateTime.MaxValue, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                outResponse.StatusCode = HttpStatusCode.InternalServerError;
                outResponse.StatusDescription = (ex.InnerException != null) ? ex.InnerException.Message : ex.Message;
            }
        }

        public string test()
        {
            return "Pepito tiene un grillito";
        }
        public string buyArticle(string OderNo, string ProdCode, int quantity, string InsertCat, string BLine, string LineDesc, string UserName)
        {
            string value = string.Empty;
            try
            {
                JuliaGrupUtils.JuliaWs.NAVCodeunitWS client2 = new JuliaGrupUtils.JuliaWs.NAVCodeunitWS();
                client2.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);

                //int value = client2.InsertItem(order.NavOferta.No, p.Code, quantity, p.CatalogNo, p.BusinessLine, linedescription);
                value = client2.InsertItem(OderNo, ProdCode, quantity, InsertCat, BLine, LineDesc, UserName);
                //Codigo que actualiza la oferta. No podemos hacerlo desde el WS, ver de crear un retoro que fuerce la recarga
                //if (value == string.Empty)
                //{
                //    OfertasWs.Oferta_Service client = new OfertasWs.Oferta_Service();
                //    client.Credentials = new System.Net.NetworkCredential(ConstantManager.WebserviceUser, ConstantManager.WebservicePassword, ConstantManager.WebserviceDomain);
                //    order.NavOferta = client.Read(order.NavOferta.No);
                //    //Al añadir columna cambia el % de Volumen, lo actualizamos
                //    order.GdoVolumenBox = order.NavOferta.GDOBoxVolumen;
                //}

            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            if (value != String.Empty)
            {
                NavException ex = new NavException(value);
                //JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_WEBSERVICE_LOG);
                throw ex;
            }
            return "";
        }
        public List<OrderLine> getOrderLines(string OrderNo, string Username, string ClientCode, int iniPos, int maxRows)
        {
            OrderDataAccesObject orderDAO = new OrderDataAccesObject();
            Order order = orderDAO.GetOrderById(OrderNo, null);
            order.OrderClient = new Client(ClientCode, Username);
            return order.ListOrderProducts.Skip(iniPos).Take(maxRows).ToList();
        }

        public int getOrderLinesCount(string OrderNo, string Username, string ClientCode)
        {
            OrderDataAccesObject orderDAO = new OrderDataAccesObject();
            Order order = orderDAO.GetOrderById(OrderNo, null);
            order.OrderClient = new Client(ClientCode, Username);

            return order.ListOrderProducts.Where(p => p.Type == "Item").ToList().Count;
        }

        public List<ProductDocument> getProductDocumentsByCode(string productCode, string Version)
        {
            List<ProductDocument> list = new List<ProductDocument>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            //RTP :SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + code);
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + productCode);

                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <Or>
                                                        <Eq>
                                                            <FieldRef Name=""ContentType"" />
                                                                <Value Type=""Computed"">Technical sheet</Value>
                                                        </Eq>
                                                        <Or>
                                                            <Eq>
                                                                <FieldRef Name=""ContentType"" />
                                                                    <Value Type=""Computed"">Assembly instructions (document)</Value>
                                                            </Eq>
                                                            <Or>
                                                                <Eq>
                                                                    <FieldRef Name=""ContentType"" />
                                                                        <Value Type=""Computed"">Composition sheet</Value>
                                                                </Eq>
                                                                <Eq>
                                                                    <FieldRef Name=""ContentType"" />
                                                                        <Value Type=""Computed"">Certificates</Value>
                                                                </Eq>
                                                            </Or>
                                                        </Or>
                                                    </Or>
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.RowLimit = 500;
                            //query.ViewFields = string.Concat(
                            //      "<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                            //      "<FieldRef Name='ContentType' />",
                            //      "<FieldRef Name='FileLeafRef' />");

                            //query.ViewFieldsOnly = true; // Fetch only the data that we need.

                            //query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection docscollection = splist.GetItems(query);

                            foreach (SPListItem doc in docscollection)
                            {
                                ProductDocument productdoc = new ProductDocument(
                                        "/Style Library/Julia/Img/pdf_ico.jpg",
                                        doc.Properties["_dlc_DocId"].ToString(),
                                        doc.Name,
                                        doc.ContentType.Name);
                                list.Add(productdoc);
                            }

                            //foreach (SPFile file in folder.Files)
                            //{
                            //    //Per a la MV meva fer servir: Publish_x0020_on_x0020_client_x0020_portal0
                            //    if (IsDocType(folder, file.Item["ContentTypeId"].ToString()) && CheckProperty(file.Item, "Publish_x0020_on_x0020_client_x0020_portal", "true"))
                            //        list.Add(file.Item);
                            //}
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return list;

        }

        public List<ProductImage> getProductImagesByCode(string code, string version, string type)
        {
            List<ProductImage> list = new List<ProductImage>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {

                    using (SPSite spsSitio = new SPSite(JuliaGrupUtils.Utils.ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = spsSitio.OpenWeb(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb))
                        {
                            SPFolder folder = web.GetFolder(JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/" + JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetListUrl + "/" + code);
                            SPQuery query = new SPQuery();
                            query.Query = @"<Where>
                                                <And>
                                                    <Eq>
                                                        <FieldRef Name=""Publish_x0020_on_x0020_client_x0020_portal"" />
                                                            <Value Type=""Integer"">1</Value>
                                                    </Eq>
                                                    <And>
                                                        <Eq>
                                                            <FieldRef Name=""ContentType"" />
                                                                <Value Type=""Computed"">Product image</Value>
                                                        </Eq>
                                                        <Contains>
                                                            <FieldRef Name=""FileLeafRef"" />
                                                            <Value Type=""File"">" + type + @"</Value>
                                                        </Contains>
                                                    </And>
                                                </And>
                                            </Where>
                                            <OrderBy><FieldRef Name=""FileLeafRef"" Ascending=""True"" /></OrderBy>";

                            query.Folder = folder;
                            //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                            //REZ 03032014 - Obtenemos solo los ficheros de ese folder...
                            query.ViewAttributes = "Scope=\"FilesOnly\"";
                            query.RowLimit = 500;
                            query.ViewFields = string.Concat(
                                  "<FieldRef Name='Publish_x0020_on_x0020_client_x0020_portal' />",
                                  "<FieldRef Name='ContentType' />",
                                  "<FieldRef Name='FileLeafRef' />");

                            query.ViewFieldsOnly = true; // Fetch only the data that we need.

                            //query.QueryThrottleMode = SPQueryThrottleOption.Override;
                            SPList splist = web.Lists[JuliaGrupUtils.Utils.ConstantManager.ArticleDocumentSetList];
                            SPListItemCollection imagescollection = splist.GetItems(query);

                            string UrlPath = JuliaGrupUtils.Utils.ConstantManager.IntranetURL +
                                JuliaGrupUtils.Utils.ConstantManager.GestionDocumentalWeb + "/";
                            foreach (SPListItem image in imagescollection)
                            {
                                list.Add(new ProductImage(UrlPath + image.File.Url, image.File.Name));
                            }

                        }
                    }
                });
            }
            catch (Exception ex)
            {
                JuliaGrupUtils.Log.Logger.WriteLogger(ex, new StackFrame(1).GetMethod().Name, JuliaGrupUtils.Log.Constantes.DATA_AREA_LOG, JuliaGrupUtils.Log.Constantes.DATA_AREA_CAT_LAYER_LOG);
                throw new Exception(new StackFrame(1).GetMethod().Name, ex);
            }
            return list;
        }
        public String GetJuliaURL(String userName, String clientCode)
        {
            String url = String.Empty;

            SPUser spUser = SPContext.Current.Web.CurrentUser;
            string strAdminUserName = spUser.LoginName.Split('|')[2];

            if (AdminUserForClientCode(strAdminUserName, clientCode) && UserNameExistsForClientCode(strAdminUserName, userName, clientCode))
            {
                String token = Utilities.Encrypt(String.Format("{0}#{1}#{2}", userName, clientCode, DateTime.Now.ToString("o")));
                url = SPContext.Current.Site.Url + "/_layouts/login/TokenValidator.aspx?token=" + HttpContext.Current.Server.UrlEncode(token);
            }
            else
            {
                url = "#";
            }
            return url;
        }

        private bool AdminUserForClientCode(string strUserName, String clientCode)
        {
            bool result = false;

            UserDataAccessObject UserDAO = new UserDataAccessObject();
            String[] roles = UserDAO.GetRolesForUser(strUserName);
            result = (roles.Contains("clientadmin")) ? true : false;
            return result;
        }

        private bool UserNameExistsForClientCode(string clientadminUser, String userName, String clientCode)
        {
            bool result = false;

            UserDataAccessObject UserDAO = new UserDataAccessObject();
            try
            {
                User user = UserDAO.GetUserWithUsername(clientCode + "_" + userName);
                result = true;
            }
            catch (Exception ex)
            {
                User Mainuser = UserDAO.GetUserWithUsername(clientadminUser);
                String pass = "prova";
                UserDAO.AddUsuario(Mainuser.UserName, clientCode + "_" + userName, pass, Mainuser.Code, Mainuser.Email, Mainuser.Coeficient, true, false);
                result = true;
            }
            return result;
        }
    }
}
