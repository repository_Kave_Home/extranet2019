﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using JuliaGrupUtils.Utils;
using Microsoft.SharePoint;
using Microsoft.Office.DocumentManagement.DocumentSets;
using System.Collections;

namespace JuliaGrupUtilsWebService.WebService
{
    [WebService(Namespace = "http://juliagrup.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    public class JuliaUtilWS : System.Web.Services.WebService
    {
        public enum DocumentTypes { 
            AsemblyInstructionsDoc, AssemblyInstructionsImg, CompositionSheet, ProductImage, TechnicalSheet, Bill, DeliveryNote,
            WeeklyOrders, QuarterReport, Document, Certificates
        };
        //"Pedidos de la semana" i "Informe de trimestre"

        /*Retornos:
         * 0 - OK
         * 1 - Error creating Article
         * 2 - Error Creating Document Sets, already exist
         * */
        [WebMethod]
        public string CreateArticle(string ArticleCode, string ArticleDescription, string ArticleVersion)
        {
            try
            {
                string result = String.Empty;

                SPListItem itemResult = this.CreateArticleInList(ArticleCode, ArticleDescription, ArticleVersion);
                if (itemResult == null)
                {
                    result = "Error Creating article";
                }
                else
                {
                    result = CreateDocumentSetArticle(itemResult, ArticleCode);
                }

                return result;
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }

        }


        [WebMethod]
        public string UploadArticleDocument(byte[] FileContent, string FileName, string FileTitle, string ArticleCode, DocumentTypes DocType, bool PublishOnClientPortal, bool PrincipalDoc)
        {
            try
            {
                int result = 0;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
                        {
                            web.AllowUnsafeUpdates = true;
                            SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + ArticleCode);
                            if (!folder.Exists)
                            {
                                throw new Exception("No se ha encontrado la carpeta para el Código de Artículo: " + ArticleCode);
                            }
                            SPDocumentLibrary library = folder.DocumentLibrary;
                            
                            //Comprobamos si ya existe el fichero
                            //SPFile oFile = folder.Files[FileName];
                            SPFile oFile = web.GetFile(folder.Url + "/" + FileName);
                            if (oFile.Exists)
                            {
                                if (oFile.CheckOutType == SPFile.SPCheckOutType.None) //Check the Status and Perform the Check Out
                                {
                                    oFile.CheckOut();
                                }
                            }

                            SPFile uploadedFile = folder.Files.Add(FileName, FileContent, true);
                            SPListItem itemUploadedFile = uploadedFile.Item;
                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Title").Id] = FileTitle;

                            SPContentType docType = null;
                            switch (DocType)
                            {
                                case DocumentTypes.AsemblyInstructionsDoc:
                                    docType = library.ContentTypes["Assembly instructions (document)"];
                                    break;
                                case DocumentTypes.AssemblyInstructionsImg:
                                    docType = library.ContentTypes["Assembly instructions (image)"];
                                    break;
                                case DocumentTypes.CompositionSheet:
                                    docType = library.ContentTypes["Composition sheet"];
                                    break;
                                case DocumentTypes.ProductImage:
                                    docType = library.ContentTypes["Product image"];
                                    itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Principal").Id] = PrincipalDoc;
                                    break;
                                case DocumentTypes.TechnicalSheet:
                                    docType = library.ContentTypes["Technical sheet"];
                                    break;
                                case DocumentTypes.Certificates:
                                    docType = library.ContentTypes["Certificates"];
                                    break;
                                default:
                                    throw new Exception("El Tipo de Documento especificado '" + DocType.ToString() + "' no está disponible en esta operación '" + "UploadArticleDocument" + "'.");
                            }

                            //set metadata to item, content type, publish on portal and principal
                            itemUploadedFile["ContentTypeId"] = docType.Id;
                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Publish_x0020_on_x0020_client_x0020_portal").Id] = PublishOnClientPortal;

                            itemUploadedFile.Update();
                            if (uploadedFile.CheckOutType != SPFile.SPCheckOutType.None){
                                uploadedFile.CheckIn("Uploaded by Ws");
                            }
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });


                return String.Empty;
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }
        }

        [WebMethod]
        public string UploadClientDocument(byte[] FileContent, string FileName, string FileTitle, string ClientCode, DocumentTypes DocType, bool PublishOnClientPortal)
        {
            try
            {
                int result = 0;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
                        {
                            web.AllowUnsafeUpdates = true;
                            SPList DSList = web.Lists[ConstantManager.ClientDocumentSetList];
                            
                            //SPFolder folder = web.GetFolder("/GestionDocumental/Documentacin%20Clientes/" + ClientCode);
                            SPFolder folder = web.GetFolder(DSList.RootFolder.ServerRelativeUrl+"/"  + ClientCode);
                            if (!folder.Exists)
                            {
                                CreateDocumentSetBill(ClientCode);
                                folder = web.GetFolder(DSList.RootFolder.ServerRelativeUrl + "/" + ClientCode);
                                //throw new Exception("No se ha encontrado la carpeta para el Código de Cliente: " + ClientCode);

                            }
                            SPDocumentLibrary library = folder.DocumentLibrary;

                            //Comprobamos si ya existe el fichero
                            //SPFile oFile = folder.Files[FileName];
                            SPFile oFile = web.GetFile(folder.Url + "/" + FileName);
                            if (oFile.Exists)
                            {
                                if (oFile.CheckOutType == SPFile.SPCheckOutType.None) //Check the Status and Perform the Check Out
                                {
                                    oFile.CheckOut();
                                }
                            }

                            SPFile uploadedFile = folder.Files.Add(FileName, FileContent, true);
                            SPListItem itemUploadedFile = uploadedFile.Item;
                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Title").Id] = FileTitle;

                            SPContentType docType = null;
                            switch (DocType)
                            {
                                case DocumentTypes.Bill :
                                    docType = library.ContentTypes["Client Bill"];
                                    break;
                                case DocumentTypes.DeliveryNote :
                                    docType = library.ContentTypes["Client DeliveryNote"];
                                    break;
                                case DocumentTypes.WeeklyOrders:
                                    docType = library.ContentTypes["Weekly Orders"];    //Pedidos de la semana
                                    break;
                                case DocumentTypes.QuarterReport:
                                    docType = library.ContentTypes["Quarter Report"];    //Informe de trimestre
                                    break;
                                case DocumentTypes.Document:
                                    docType = library.ContentTypes["Document"];    //Informe de trimestre
                                    break;
                                case DocumentTypes.Certificates:
                                    docType = library.ContentTypes["Certificates"];
                                    break;
                                default:
                                    throw new Exception("El Tipo de Documento especificado '" + DocType.ToString() + "' no está disponible en esta operación '" + "UploadClientDocument" + "'.");
                                    
                            }

                            //set metadata to item, content type, publish on portal and principal
                            itemUploadedFile["ContentTypeId"] = docType.Id;
                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Publish_x0020_on_x0020_client_x0020_portal").Id] = PublishOnClientPortal;

                            itemUploadedFile.Update();
                            if (uploadedFile.CheckOutType != SPFile.SPCheckOutType.None)
                            {
                                uploadedFile.CheckIn("Uploaded by Ws");
                            }
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });


                return String.Empty;
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }
        }



        /*PRIVATE FUNCTIONS*/
        private SPListItem CreateArticleInList(string ArticleCode, string ArticleDescription, string ArticleVersion)
        {
            //try
            //{
            SPListItem itemResult = null;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite site = new SPSite(ConstantManager.IntranetURL))
                {
                    SPWeb web = site.RootWeb;
                    web.AllowUnsafeUpdates = true;
                    SPList articleList = web.GetList(ConstantManager.ArticleListUrl);
                    SPContentType itemtype = articleList.ContentTypes["Article_bcs"];
                    //check if exist
                    SPQuery query = new SPQuery();
                    query.Query = "<Where><Eq><FieldRef Name=\"Article_x0020_code1\" /><Value Type=\"Text\">" + ArticleCode + "</Value></Eq></Where>";
                    SPListItemCollection collection = articleList.GetItems(query);

                    try
                    {
                        itemResult = collection[0];
                    }
                    catch (Exception e)
                    {
                    }

                    if (itemResult == null)
                    {
                        //add article to list
                        SPListItem item = articleList.Items.Add();
                        item["ContentTypeId"] = itemtype.Id;
                        item[articleList.Fields.GetFieldByInternalName("Article_x0020_code1").Id] = ArticleCode;
                        item[articleList.Fields.GetFieldByInternalName("Article_x0020_Description").Id] = ArticleDescription;
                        item[articleList.Fields.GetFieldByInternalName("Article_x0020_Active_x0020_versi").Id] = ArticleVersion;

                        item.Update();
                        itemResult = item;
                    }
                    web.AllowUnsafeUpdates = true;
                }
            });
            return itemResult;
            //}
            //catch (Exception ex)
            //{
            //    return null;
            //}
        }


        private string CreateDocumentSetArticle(SPListItem item, string articleCode)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
                        {
                            web.AllowUnsafeUpdates = true;
                            SPList articlesDSList = web.Lists[ConstantManager.ArticleDocumentSetList];
                            SPContentType articleDS = articlesDSList.ContentTypes[ConstantManager.ArticleDocumentSetContentTypeName];

                            //build document set properties
                            Hashtable documentSetProperties = new Hashtable();
                            documentSetProperties.Add("Title", item[item.Fields.GetFieldByInternalName("Article_x0020_code1").Id]);
                            documentSetProperties.Add("DocumentSetDescription", item[item.Fields.GetFieldByInternalName("Article_x0020_Description").Id]);
                            string codeVersion = item["Article code"] != null ? item[item.Fields.GetFieldByInternalName("Article_x0020_Active_x0020_versi").Id].ToString() : string.Empty;
                            SPFieldLookupValue lookup = new SPFieldLookupValue(item.ID, codeVersion);
                            documentSetProperties.Add("Article", lookup);

                            DocumentSet ds = DocumentSet.Create(articlesDSList.RootFolder, articleCode, articleDS.Id, documentSetProperties);
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });
                return String.Empty;
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }

        }
        private string CreateDocumentSetBill(string ClientCode)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
                    {
                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
                        {
                            web.AllowUnsafeUpdates = true;
                            //ClientDocumentSetList 
                            //ClientBillDocumentSetContentTypeName

                            SPList articlesDSList = web.Lists[ConstantManager.ClientDocumentSetList];
                            SPContentType BillDS = articlesDSList.ContentTypes[ConstantManager.ClientBillDocumentSetContentTypeName];

                            //build document set properties
                            Hashtable documentSetProperties = new Hashtable();
                            documentSetProperties.Add("Title", ClientCode);
                           // documentSetProperties.Add("Title", item[item.Fields.GetFieldByInternalName("Article_x0020_code1").Id]);
                            //documentSetProperties.Add("DocumentSetDescription", item[item.Fields.GetFieldByInternalName("Article_x0020_Description").Id]);
                            //string codeVersion = item["Article code"] != null ? item[item.Fields.GetFieldByInternalName("Article_x0020_Active_x0020_versi").Id].ToString() : string.Empty;
                            //SPFieldLookupValue lookup = new SPFieldLookupValue(item.ID, codeVersion);
                            //documentSetProperties.Add("Article", lookup);

                            DocumentSet ds = DocumentSet.Create(articlesDSList.RootFolder, ClientCode, BillDS.Id, documentSetProperties);
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });
                return String.Empty;
            }
            catch (Exception e)
            {
                return e.Message + " " + e.StackTrace;
            }

        }
    }
}




//Old Code - Por si acaso falla algo, para no perderlo
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.Services;
//using JuliaGrupUtils.Utils;
//using Microsoft.SharePoint;
//using Microsoft.Office.DocumentManagement.DocumentSets;
//using System.Collections;

//namespace JuliaGrupUtilsWebService.WebService
//{
//    [WebService(Namespace = "http://juliagrup.org/")]
//    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

//    public class JuliaUtilWS : System.Web.Services.WebService
//    {        
//        public enum DocumentTypes {AsemblyInstructionsDoc, AssemblyInstructionsImg, CompositionSheet, ProductImage, TechnicalSheet};

//        /*Retornos:
//         * 0 - OK
//         * 1 - Error creating Article
//         * 2 - Error Creating Document Sets, already exist
//         * */
//        [WebMethod]
//        public string CreateArticle(string ArticleCode, string ArticleDescription, string ArticleVersion)
//        {
//            try
//            {
//                string result = String.Empty;

//                SPListItem itemResult = this.CreateArticleInList(ArticleCode, ArticleDescription, ArticleVersion);
//                if (itemResult == null)
//                {
//                    result = "Error Creating article";
//                }
//                else
//                {
//                    result = CreateDocumentSetArticle(itemResult, ArticleCode);
//                }

//                return result;
//            }
//            catch (Exception e)
//            {
//                return e.Message + " " + e.StackTrace;
//            }

//        }


//        [WebMethod]
//        public string UploadDocument(byte[] FileContent, string FileName, string FileTitle, string ArticleCode, DocumentTypes DocType, bool PublishOnClientPortal, bool PrincipalDoc)
//        {
//            try
//            {
//                int result = 0;
//                SPSecurity.RunWithElevatedPrivileges(delegate()
//                {
//                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
//                    {
//                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
//                        {
//                            SPFolder folder = web.GetFolder("/GestionDocumental/Documents/" + ArticleCode);
//                            SPDocumentLibrary library = folder.DocumentLibrary;                           

//                            //Comprobamos si ya existe el fichero
//                            SPFile oFile = folder.Files[FileName];
//                            if(oFile.Exists) 
//                            {
//                                if (oFile.CheckOutType == SPFile.SPCheckOutType.None) //Check the Status and Perform the Check Out
//                                    {                        
//                                        oFile.CheckOut();
//                                    }
//                            }

//                            SPFile uploadedFile = folder.Files.Add(FileName, FileContent, true);
//                            SPListItem itemUploadedFile = uploadedFile.Item;
//                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Title").Id] = FileTitle;

//                            SPContentType docType = null;
//                            switch(DocType)
//                            {
//                                case DocumentTypes.AsemblyInstructionsDoc:
//                                     docType = library.ContentTypes["Assembly instructions (document)"];
//                                    break;
//                                case DocumentTypes.AssemblyInstructionsImg:
//                                    docType = library.ContentTypes["Assembly instructions (image)"];
//                                    break;
//                                case DocumentTypes.CompositionSheet:
//                                    docType = library.ContentTypes["Composition sheet"];
//                                    break;
//                                case DocumentTypes.ProductImage:
//                                    docType = library.ContentTypes["Product image"];
//                                    itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Principal").Id] = PrincipalDoc;
//                                    break;
//                                case DocumentTypes.TechnicalSheet:
//                                    docType = library.ContentTypes["Technical sheet"];
//                                    break;
//                            }

//                            //set metadata to item, content type, publish on portal and principal
//                            itemUploadedFile["ContentTypeId"] = docType.Id;
//                            itemUploadedFile[itemUploadedFile.Fields.GetFieldByInternalName("Publish_x0020_on_x0020_client_x0020_portal").Id] = PublishOnClientPortal;                            

//                            itemUploadedFile.Update();
//                            uploadedFile.CheckIn("Uploaded by Ws");

//                        }
//                    }
//                });


//                return String.Empty;
//            }
//            catch (Exception e)
//            {
//                return e.Message+" "+e.StackTrace;
//            }
//        }

//        /*PRIVATE FUNCTIONS*/
//        private SPListItem CreateArticleInList(string ArticleCode, string ArticleDescription, string ArticleVersion)
//        {
//            //try
//            //{
//                SPListItem itemResult = null;

//                SPSecurity.RunWithElevatedPrivileges(delegate()
//                {
//                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
//                    {
//                        SPList articleList = site.RootWeb.GetList(ConstantManager.ArticleListUrl);
//                        SPContentType itemtype = articleList.ContentTypes["Article_bcs"];
//                        //check if exist
//                        SPQuery query = new SPQuery();
//                        query.Query = "<Where><Eq><FieldRef Name=\"Article_x0020_code1\" /><Value Type=\"Text\">"+ArticleCode+"</Value></Eq></Where>";
//                        SPListItemCollection collection = articleList.GetItems(query);

//                        try
//                        {
//                            itemResult = collection[0];
//                        }
//                        catch(Exception e)
//                        {
//                        }

//                        if(itemResult == null)
//                        {
//                            //add article to list
//                            SPListItem item = articleList.Items.Add();
//                            item["ContentTypeId"] = itemtype.Id;
//                            item[articleList.Fields.GetFieldByInternalName("Article_x0020_code1").Id] = ArticleCode;
//                            item[articleList.Fields.GetFieldByInternalName("Article_x0020_Description").Id] = ArticleDescription;
//                            item[articleList.Fields.GetFieldByInternalName("Article_x0020_Active_x0020_versi").Id] = ArticleVersion;

//                            item.Update();
//                            itemResult = item;
//                        }

//                    }
//                });
//                return itemResult;
//            //}
//            //catch (Exception ex)
//            //{
//            //    return null;
//            //}
//        }


//        private string CreateDocumentSetArticle(SPListItem item, string articleCode)
//        {
//            try
//            {
//                SPSecurity.RunWithElevatedPrivileges(delegate()
//                {
//                    using (SPSite site = new SPSite(ConstantManager.IntranetURL))
//                    {
//                        using (SPWeb web = site.OpenWeb(ConstantManager.GestionDocumentalWeb))
//                        {
//                            SPList articlesDSList = web.Lists[ConstantManager.ArticleDocumentSetList];
//                            SPContentType articleDS = articlesDSList.ContentTypes[ConstantManager.ArticleDocumentSetContentTypeName];

//                            //build document set properties
//                            Hashtable documentSetProperties = new Hashtable();
//                            documentSetProperties.Add("Title", item[item.Fields.GetFieldByInternalName("Article_x0020_code1").Id]);
//                            documentSetProperties.Add("DocumentSetDescription", item[item.Fields.GetFieldByInternalName("Article_x0020_Description").Id]);
//                            string codeVersion = item["Article code"] != null ? item[item.Fields.GetFieldByInternalName("Article_x0020_Active_x0020_versi").Id].ToString() : string.Empty;
//                            SPFieldLookupValue lookup = new SPFieldLookupValue(item.ID, codeVersion);
//                            documentSetProperties.Add("Article", lookup);

//                            DocumentSet ds = DocumentSet.Create(articlesDSList.RootFolder, articleCode, articleDS.Id, documentSetProperties);                            
//                        }
//                    }
//                });
//                return String.Empty;
//            }
//            catch (Exception e)
//            {
//                return e.Message + " " + e.StackTrace;
//            }

//        }
//    }
//}
