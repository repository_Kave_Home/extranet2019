//
//	SPCategoryAttribute.cs - � Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;

using QuestechSystems.SharePoint;

namespace QuestechSystems.SharePoint.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class SPCategoryAttribute : System.ComponentModel.CategoryAttribute
    {
        public SPCategoryAttribute(string attribute) : base(attribute) { }

        protected override string GetLocalizedString(string value)
        {
            string locValue = Utility.GetResourceString(value);
            if (String.IsNullOrEmpty(locValue))
            {
                return base.GetLocalizedString(value);
            }
            else
            {
                return locValue;
            }
        }
    }
}