﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Questech Systems Picture Thumbnails")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Questech Systems")]
[assembly: AssemblyProduct("Questech Systems SharePoint")]
[assembly: AssemblyCopyright("Copyright © Questech Systems 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("A840C04C-CA3A-474f-8184-CCCCE0749AFA")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AllowPartiallyTrustedCallers()]
[assembly: System.Web.UI.WebResource("QuestechSystems.SharePoint.ClientScripts.litebox-1.0.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("QuestechSystems.SharePoint.ClientScripts.moo.fx.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("QuestechSystems.SharePoint.ClientScripts.prototype.lite.js", "text/javascript")]