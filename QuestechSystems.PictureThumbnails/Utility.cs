//
//	Utility.cs - � Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Resources;

namespace QuestechSystems.SharePoint
{
    public static class Utility
    {
        public const string DefaultNamespace = "QuestechSystems.SharePoint";
        public const string FilePathLitebox = "ClientScripts.litebox-1.0.js";
        public const string FilePathMooFx = "ClientScripts.moo.fx.js";
        public const string FilePathPrototypeLite = "ClientScripts.prototype.lite.js";
        public const string JSBlockKeyLitebox = "JSBlockKeyLitebox";
        public const string JSIncludeKeyLitebox = "JSIncludeKeyLitebox";
        public const string JSIncludeKeyMooFx = "JSIncludeKeyMooFx";
        public const string JSIncludeKeyPrototypeLite = "JSIncludeKeyPrototypeLite";

        public const string JSBlockLiteboxFormatString = @"fileLoadingImage = ""{0}loading.gif"";fileBottomNavCloseImage = ""{0}closelabel.gif"";_spBodyOnLoadFunctionNames.push('initLightbox');";
        public const string QueryCamlOrderByFormatString = "<FieldRef Name='{0}' Ascending='{1}' />";
        public const string QueryCamlOrderByTagFormatString = "<OrderBy>{0}</OrderBy>";

        public static ResourceManager ResourceManager = new ResourceManager(String.Format("{0}.Resource", DefaultNamespace), Assembly.GetCallingAssembly());


        public static string GetResourceString(string stringId)
        {
            return Utility.ResourceManager.GetString(stringId);
        }

        public static string GetResourceString(ResourceManager rm, string stringId)
        {
            if (rm == null)
            {
                return GetResourceString(stringId);
            }
            else
            {
                return rm.GetString(stringId);
            }
        }


        public static void AddOrderByCaml(ref string caml, string property, bool sortOrder)
        {
            if (String.IsNullOrEmpty(property))
            {
                return;
            }
            caml += String.Format(QueryCamlOrderByFormatString, property, sortOrder.ToString().ToUpper());
        }
    }
}