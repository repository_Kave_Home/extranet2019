//
//	PictureThumbnails.cs - � Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;

namespace QuestechSystems.SharePoint.WebControls
{
    public class PictureThumbnails : SPControl
    {
        public string SiteUrl
        {
            get
            {
                return _siteUrl;
            }
            set
            {
                _siteUrl = value;
            }
        }

        public string ListName
        {
            get
            {
                return _listName;
            }
            set
            {
                _listName = value;
            }
        }

        public bool RandomizeItems
        {
            get
            {
                return _randomizeItems;
            }
            set
            {
                _randomizeItems = value;
            }
        }

        public string SortField
        {
            // "Created"
            // "Modified"
            // "Title"
            // ...
            get
            {
                return _sortField;
            }
            set
            {
                _sortField = value;
            }
        }

        public bool SortAscending
        {
            get
            {
                return _sortAscending;
            }
            set
            {
                _sortAscending = value;
            }
        }

        public int RowLimit
        {
            get
            {
                return _rowLimit;
            }
            set
            {
                _rowLimit = value;
            }
        }

        public string ImageDirectoryUrl
        {
            get
            {
                return _imageDirectoryUrl;
            }
            set
            {
                _imageDirectoryUrl = value;
            }
        }

        public bool ShowAsImageSet
        {
            get
            {
                return _showAsImageSet;
            }
            set
            {
                _showAsImageSet = value;
            }
        }

        public string CssClassPictureThumbnails
        {
            get
            {
                return _cssClassPictureThumbnails;
            }
            set
            {
                _cssClassPictureThumbnails = value;
            }
        }

        private bool _randomizeItems = false;
        private bool _showAsImageSet = false;
        private bool _sortAscending = true;
        private int _rowLimit = 0;
        private string _cssClassPictureThumbnails;
        private string _imageDirectoryUrl;
        private string _listName;
        private string _siteUrl;
        private string _sortField;
        private SPList _pictureList;
        private SPWeb _site = null;
        private SPFolder _folder;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (String.IsNullOrEmpty(_listName))
            {
                return;
            }

            if (String.IsNullOrEmpty(_siteUrl))
            {
                _site = SPContext.Current.Web;
            }
            else
            {
                _site = SPContext.Current.Site.OpenWeb(_siteUrl);
            }

            if (_site == null)
            {
                return;
            }

            
            try
            {
                _folder = _site.GetFolder(_listName);
                _pictureList = _site.Lists[_folder.ParentListId];
            }
            catch (Exception ex) { }

            //foreach (SPList list in _site.Lists)
            //{
            //    if (list.BaseTemplate == SPListTemplateType.PictureLibrary && list.Title.Equals(_listName, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        _pictureList = list;
            //        break;
            //    }
            //}
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ClientScriptManager cs = Page.ClientScript;

            if (!cs.IsClientScriptIncludeRegistered(typeof(Page), Utility.JSIncludeKeyPrototypeLite))
            {
                string scriptUrl = cs.GetWebResourceUrl(this.GetType(), String.Format("{0}.{1}", Utility.DefaultNamespace, Utility.FilePathPrototypeLite));
                cs.RegisterClientScriptInclude(typeof(Page), Utility.JSIncludeKeyPrototypeLite, scriptUrl);
            }
            if (!cs.IsClientScriptIncludeRegistered(typeof(Page), Utility.JSIncludeKeyMooFx))
            {
                string scriptUrl = cs.GetWebResourceUrl(this.GetType(), String.Format("{0}.{1}", Utility.DefaultNamespace, Utility.FilePathMooFx));
                cs.RegisterClientScriptInclude(typeof(Page), Utility.JSIncludeKeyMooFx, scriptUrl);
            }
            if (!cs.IsClientScriptIncludeRegistered(typeof(Page), Utility.JSIncludeKeyLitebox))
            {
                string scriptUrl = cs.GetWebResourceUrl(this.GetType(), String.Format("{0}.{1}", Utility.DefaultNamespace, Utility.FilePathLitebox));
                cs.RegisterClientScriptInclude(typeof(Page), Utility.JSIncludeKeyLitebox, scriptUrl);
            }
            if (!cs.IsClientScriptBlockRegistered(typeof(Page), Utility.JSBlockKeyLitebox))
            {
                string scriptBlock = string.Format(Utility.JSBlockLiteboxFormatString,  _imageDirectoryUrl);
                cs.RegisterClientScriptBlock(typeof(Page), Utility.JSBlockKeyLitebox, scriptBlock, true);
            }
        }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);

            if (!String.IsNullOrEmpty(_siteUrl) && _site != null)
            {
                _site.Close();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (_pictureList == null)
            {
                return;
            }

            base.Render(writer);
        }

        protected override void CreateChildControls()
        {
            if (_pictureList == null)
            {
                return;
            }

            Panel pictureItemsPanel = new Panel();
            pictureItemsPanel.CssClass = _cssClassPictureThumbnails;
            PopulatePictureItemsPanel(pictureItemsPanel);
            this.Controls.Add(pictureItemsPanel);
        }

        private void PopulatePictureItemsPanel(Panel pictureItemsPanel)
        {
            List<SPListItem> pictureItems = GetPictureItems(_pictureList);

            if (pictureItems.Count == 0)
            {
                return;
            }

            string itemHtml;
            string baseUrl = this.Page.Request.Url.GetLeftPart(UriPartial.Authority);
            string siteUrl = _pictureList.ParentWebUrl.TrimEnd("/".ToCharArray());
            string galleryOption = String.Empty;
            string thumbnailUrl;
            if (_showAsImageSet)
            {
                galleryOption = "[Gallery]";
            }

            pictureItemsPanel.Controls.Add(new LiteralControl("<ul>"));

            foreach (SPListItem pictureItem in pictureItems) {
                thumbnailUrl = pictureItem["EncodedAbsThumbnailUrl"].ToString();
                if (thumbnailUrl.StartsWith(baseUrl))
                {
                    thumbnailUrl = thumbnailUrl.Substring(baseUrl.Length);
                }
                itemHtml = String.Format(@"<li style=""background-image:url('{0}')""><a href=""{1}/{2}"" rel=""lightbox{3}"" title=""{4}"">{4}</a></li>", thumbnailUrl, siteUrl, pictureItem.Url, galleryOption, SPEncode.HtmlEncode(pictureItem.Title));
                pictureItemsPanel.Controls.Add(new LiteralControl(itemHtml));
            }

            pictureItemsPanel.Controls.Add(new LiteralControl("</ul>"));
        }

        private List<SPListItem> GetPictureItems(SPList list)
        {
            List<SPListItem> items = new List<SPListItem>();

            SPListItem item;
            SPListItemCollection listItems = list.GetItems(GetDataQuery());

            foreach (SPListItem listItem in listItems)
            {
                item = list.GetItemById(listItem.ID);
                items.Add(item);
            }

            if (!_randomizeItems)
            {
                return items;
            }

            List<SPListItem> randomizedItems = new List<SPListItem>();
            int index;
            Random random = new Random();

            for (int i = items.Count; i > 0; i--)
            {
                if (_rowLimit != 0 && randomizedItems.Count >= _rowLimit)
                {
                    break;
                }
                index = random.Next(i);
                randomizedItems.Add(items[index]);
                items.RemoveAt(index);
            }

            return randomizedItems;
        }

        private SPQuery GetDataQuery()
        {
            SPQuery dataQuery = new SPQuery();

            dataQuery.ViewAttributes = "Scope=\"Recursive\"";

            if (!_randomizeItems)
            {
                string camlOrderBy = String.Empty;
                Utility.AddOrderByCaml(ref camlOrderBy, _sortField, _sortAscending);
                if (camlOrderBy.Length > 0)
                {
                    camlOrderBy = String.Format(Utility.QueryCamlOrderByTagFormatString, camlOrderBy);
                }

                if (camlOrderBy.Length > 0)
                {
                    dataQuery.Query = camlOrderBy;
                }

                if (_rowLimit > 0)
                {
                    dataQuery.RowLimit = (uint)_rowLimit;
                }
                
                dataQuery.Folder = _folder;

            }

            return dataQuery;
        }
    }
}