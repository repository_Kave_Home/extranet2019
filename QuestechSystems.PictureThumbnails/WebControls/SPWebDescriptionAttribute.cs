//
//	SPWebDescriptionAttribute.cs - � Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;

using QuestechSystems.SharePoint;

namespace QuestechSystems.SharePoint.WebControls.WebParts
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class SPWebDescriptionAttribute : System.Web.UI.WebControls.WebParts.WebDescriptionAttribute
    {
        private string _attribute;

        public SPWebDescriptionAttribute(string attribute)
        {
            _attribute = attribute;
        }
 
        public override string Description
        {
            get
            {
                string value = Utility.GetResourceString(_attribute);
                if (String.IsNullOrEmpty(value))
                {
                    value = _attribute;
                }
                return this.DescriptionValue = value;
            }
        }
    }
}