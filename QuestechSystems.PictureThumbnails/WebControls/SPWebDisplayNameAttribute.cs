//
//	SPWebDisplayNameAttribute.cs - � Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;

using QuestechSystems.SharePoint;

namespace QuestechSystems.SharePoint.WebControls.WebParts
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class SPWebDisplayNameAttribute : System.Web.UI.WebControls.WebParts.WebDisplayNameAttribute
    {
        private string _attribute;

        public SPWebDisplayNameAttribute(string attribute)
        {
            _attribute = attribute;
        }

        public override string DisplayName
        {
            get
            {
                string value = Utility.GetResourceString( _attribute);
                if (String.IsNullOrEmpty(value))
                {
                    value = _attribute;
                }
                return this.DisplayNameValue = value;
            }
        }
    }
}