﻿//
//	PictureThumbnailsWebPart.cs - © Questech Systems
//	This notice must stay intact for use. Not for resale.
//
using System;
using System.ComponentModel;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebPartPages;
using QuestechSystems.SharePoint.ComponentModel;

namespace QuestechSystems.SharePoint.WebControls.WebParts
{
    public class PictureThumbnailsWebPart : System.Web.UI.WebControls.WebParts.WebPart
    {
        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_SiteUrl")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_SiteUrl")]
        [SPCategory("SPCategory_Configuration")]
        public string SiteUrl
        {
            get
            {
                return _siteUrl;
            }
            set
            {
                _siteUrl = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_ListName")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_ListName")]
        [SPCategory("SPCategory_Configuration")]
        public string ListName
        {
            get
            {
                return _listName;
            }
            set
            {
                _listName = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_SortField")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_SortField")]
        [SPCategory("SPCategory_Configuration")]
        public SortFieldType SortField
        {
            get
            {
                return _sortField;
            }
            set
            {
                _sortField = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_SortOrder")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_SortOrder")]
        [SPCategory("SPCategory_Configuration")]
        public SortOrderType SortOrder
        {
            get
            {
                return _sortOrder;
            }
            set
            {
                _sortOrder = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_RowLimit")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_RowLimit")]
        [SPCategory("SPCategory_Configuration")]
        public int RowLimit
        {
            get
            {
                return _rowLimit;
            }
            set
            {
                _rowLimit = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_ShowAsImageSet")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_ShowAsImageSet")]
        [SPCategory("SPCategory_Configuration")]
        public bool ShowAsImageSet
        {
            get
            {
                return _showAsImageSet;
            }
            set
            {
                _showAsImageSet = value;
            }
        }

        [WebBrowsable(true)]
        [Personalizable(PersonalizationScope.User)]
        [SPWebDisplayName("PictureThumbnailsWebPartDisplayName_CssClassPictureThumbnails")]
        [SPWebDescription("PictureThumbnailsWebPartDescription_CssClassPictureThumbnails")]
        [SPCategory("SPCategory_Configuration")]
        public string CssClassPictureThumbnails
        {
            get
            {
                return _cssClassPictureThumbnails;
            }
            set
            {
                _cssClassPictureThumbnails = value;
            }
        }

        public enum SortFieldType
        {
            Name,
            Title,
            Created,
            Modified
        }

        public enum SortOrderType
        {
            Ascending,
            Descending,
            Random
        }

        private bool _showAsImageSet = false;
        private int _rowLimit = 0;
        private string _cssClassPictureThumbnails;
        private string _listName = String.Empty;
        private string _siteUrl = String.Empty;
        private SortFieldType _sortField;
        private SortOrderType _sortOrder;

        protected override void CreateChildControls()
        {
            try
            {
                if (String.IsNullOrEmpty(_listName))
                {
                    return;
                }
                PictureThumbnails pictureThumbnails = new PictureThumbnails();
                InitPictureThumbnails(pictureThumbnails);
                this.Controls.Add(pictureThumbnails);
            }
            catch (Exception ex)
            {
                Controls.Add(new LiteralControl(ex.Message));
            }
        }

        private void InitPictureThumbnails(PictureThumbnails pictureThumbnails)
        {
            pictureThumbnails.CssClassPictureThumbnails = _cssClassPictureThumbnails;
            pictureThumbnails.SiteUrl = _siteUrl.Trim();
            pictureThumbnails.ListName = _listName.Trim();
            if (_sortOrder == SortOrderType.Random)
            {
                pictureThumbnails.RandomizeItems = true;
            }
            else
            {
                if (_sortField == SortFieldType.Name)
                {
                    pictureThumbnails.SortField = "NameOrTitle";
                }
                else
                {
                    pictureThumbnails.SortField = _sortField.ToString("G");
                }
                if (_sortOrder == SortOrderType.Ascending)
                {
                    pictureThumbnails.SortAscending = true;
                }
                else
                {
                    pictureThumbnails.SortAscending = false;
                }
            }
            pictureThumbnails.RowLimit = _rowLimit;
            string baseUrl = this.Page.Request.Url.GetLeftPart(UriPartial.Authority);
            string imageDirectoryUrl = SPWebPartManager.GetClassResourcePath(SPContext.Current.Web, this.GetType());
            if (imageDirectoryUrl.StartsWith(baseUrl))
            {
                imageDirectoryUrl = imageDirectoryUrl.Substring(baseUrl.Length);
            }
            imageDirectoryUrl = String.Format("{0}/Images/", imageDirectoryUrl);
            pictureThumbnails.ImageDirectoryUrl = imageDirectoryUrl;
            pictureThumbnails.ShowAsImageSet = _showAsImageSet;
        }
    }
}